package com.zoo.zoomerchantadminweb.exception;

import com.zoo.web.bean.WebResult;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authz.UnauthorizedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import javax.servlet.http.HttpServletRequest;

/**
 * 异常处理
 *
 * @Author:LiuZhiMin
 */
@ControllerAdvice
public class GlobalExceptionHandler {

    private Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    @ExceptionHandler(Exception.class) // 所有的异常都是Exception子类
    public ModelAndView defaultErrorHandler(HttpServletRequest request, Exception e) { // 出现异常之后会跳转到此方法
        logger.error("异常捕获",e);
        ModelAndView mav = new ModelAndView(new MappingJackson2JsonView()); // 设置跳转路径
        WebResult result = WebResult.getErrorResult(WebResult.Code.ERROR);
        if (e instanceof UnknownAccountException) {
            //用户不存在
            result = WebResult.getErrorResult(WebResult.Code.LOGIN_USER_NO_EXIST_ERROR);
        } else if (e instanceof IncorrectCredentialsException) {
            //登录 用户名或密码错误
            result = WebResult.getErrorResult(WebResult.Code.LOGIN_AUTH_ERROR);
        } else if (e instanceof LockedAccountException) {
            //登录 账户无效
            result = WebResult.getErrorResult(WebResult.Code.LOGIN_USER_INVALID_ERROR);
        } else if (e instanceof UnauthorizedException) {
            //没有权限
            result = WebResult.getErrorResult(WebResult.Code.NO_PERMISSION);
        } else if (e instanceof com.alibaba.dubbo.rpc.RpcException) {
            //没有权限
            result = WebResult.getErrorResult(WebResult.Code.DUBBO_RPC_EXCEPTION);
        } else if( e instanceof AuthenticationException){
            result = WebResult.getErrorResult(WebResult.Code.LOGIN_AUTH_ERROR);
        }
        if (e != null) {
            e.printStackTrace();
        }

        //记录错误日志
        //LogExceptionHandler.logError(e);

        mav.addAllObjects(result);
        return mav;
    }

}
