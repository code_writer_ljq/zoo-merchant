package com.zoo.zoomerchantadminweb.finance.controller;

import com.zoo.activity.dao.model.Orders;
import com.zoo.activity.dao.model.Organizer;
import com.zoo.activity.service.OrdersService;
import com.zoo.activity.util.DateUtil;
import com.zoo.bean.util.ActivityBeanUtils;
import com.zoo.finance.dao.model.ZooBalanceDepositIndemnity;
import com.zoo.finance.dao.model.ZooBalanceDepositIndemnityExample;
import com.zoo.finance.service.*;
import com.zoo.ota.OtaConstant;
import com.zoo.rest.sms.sdk.utils.encoder.BASE64Encoder;
import com.zoo.zoomerchantadminweb.context.WebUpmsContext;
import com.zoo.zoomerchantadminweb.finance.controller.poi.util.FinanceExcelUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.models.auth.In;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.CellReference;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.util.CellRangeAddress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URLEncoder;
import java.util.*;

@Api(value = "api/finance/onlineCollect", tags = {"【财务系统】线上账务汇总"}, description = "线上账务汇总")
@RestController
@RequestMapping("api/finance/onlineCollect")
public class OnlineCollectController {

    private static final Logger LOGGER = LoggerFactory.getLogger(OnlineCollectController.class);

    @Autowired
    private ZooOperatorLoginService zooOperatorLoginService;

    @Autowired
    private ZooOperatorOrderStattisticsService zooOperatorOrderStattisticsService;

    @Autowired
    private ZooOperatorDepositService zooOperatorDepositService;

    @Autowired
    private ZooBalanceDepositIndemnityService zooBalanceDepositIndemnityService;

    @Autowired
    private OrdersService ordersService;

    @Autowired
    private ZooOnAccountOrdersService zooOnAccountOrdersService;

    @Autowired
    private ZooEntertainOrdersService zooEntertainOrdersService;

    /**
     * 导出线上财务报表
     */
    @ApiOperation(value = "「导出」导出订单信息", notes = "导出订单信息（按照操作员和产品分类进行导出")
    @RequestMapping(value = "/orders/export", method = RequestMethod.GET)
    public void exportOrdersWithOperatorAndEpisodeInfo(@RequestParam(value = "startTime", required = false) String startTime,
                                                       @RequestParam(value = "endTime", required = false) String endTime,
                                                       HttpServletRequest request, HttpServletResponse response) throws Exception {
        Organizer organizer = WebUpmsContext.getOrgnization(request);

        Date st = null;
        if (StringUtils.isNotEmpty(startTime)) {
            if (startTime.length() == 10) {
                startTime = startTime + " 00:00:00";
            }
            st = DateUtil.convertStringToDate(startTime, "yyyy-MM-dd HH:mm:ss");
        }

        Date et = null;
        if (StringUtils.isNotEmpty(endTime)) {
            if (endTime.length() == 10) {
                endTime = endTime + " 23:59:59";
            }
            et = DateUtil.convertStringToDate(endTime, "yyyy-MM-dd HH:mm:ss");
        }

        response.reset();
        response.setContentType("application/x-excel");
        String agent = request.getHeader("User-Agent");

        String outfile = "线上财务报表";
        if(StringUtils.isNotEmpty(startTime)){
            outfile += "_" + startTime.replace("-","").substring(0,8);
        }
        if(StringUtils.isNotEmpty(endTime)){
            outfile += "_" + endTime.replace("-","").substring(0,8);
        }

        String fileName = encodeDownloadFilename(outfile,agent);
        response.setHeader("Content-disposition", "attachment;filename=" + fileName + ".xls");
        ServletOutputStream outputStream = response.getOutputStream();
        createHSSFWorkbook(st, et,organizer, outputStream);
        outputStream.close();
    }

    public static String encodeDownloadFilename(String filename, String agent)
            throws IOException {
        if (agent.contains("Firefox")) { // 火狐浏览器
            filename = "=?UTF-8?B?"
                    + new BASE64Encoder().encode(filename.getBytes("utf-8"))
                    + "?=";
            filename = filename.replaceAll("\r\n", "");
        } else { // IE及其他浏览器
            filename = URLEncoder.encode(filename, "utf-8");
            filename = filename.replace("+"," ");
        }
        return filename;
    }

    private void createHSSFWorkbook(Date startTime, Date endTime, Organizer organizer, OutputStream out) throws IOException {

        /*不可退款订单（不包含分销商订单）（排出教练的课程）*/
        List<Orders> orderslist = ordersService.getOrdersOnline(startTime, endTime, null, null, organizer.getOrganizerId());
        if(orderslist == null){
            orderslist = new ArrayList<Orders>();
        }
        /*核销订单（可退款订单和分销商订单）（排出教练的课程）*/
        List<Orders> verifyOrdersOnlinelist = ordersService.getVerifyOrdersOnline(startTime, endTime, null, null, organizer.getOrganizerId());
        if(verifyOrdersOnlinelist != null){
            orderslist.addAll(verifyOrdersOnlinelist);
        }
        /*核销订单（教练的课程订单）*/
        List<Orders> verifyCoachCourseOrdersOnline = ordersService.getVerifyCoachCourseOrdersOnline(startTime, endTime, null, null, organizer.getOrganizerId());
        if(verifyCoachCourseOrdersOnline != null){
            orderslist.addAll(verifyCoachCourseOrdersOnline);
        }


        //押金总收入
        BigDecimal totalInDeposit = BigDecimal.ZERO;
        //押金总支出
        BigDecimal totalOutDeposit = BigDecimal.ZERO;
        //产品总金额
        BigDecimal totalOrderPrice = BigDecimal.ZERO;
        //退款总金额
        BigDecimal totalRefundPrice = BigDecimal.ZERO;
        //赔偿总金额
        BigDecimal totalIndemnityPrice = BigDecimal.ZERO;
        //赔偿
        Map<Integer,Map<String,Object>> indemnityProduct = new HashMap<Integer, Map<String, Object>>();
        //产品统计
        Map<Integer,Map<String,Map<String,Object>>> saleProductStastic = new HashMap<Integer,Map<String,Map<String,Object>>>();
        //产品
        Map<Integer,Map<String,Object>> orderProduct = new HashMap<Integer, Map<String, Object>>();
        //异常退款产品
        Map<Integer,Map<String,Object>> refundProduct = new HashMap<Integer, Map<String, Object>>();
        //支付方式汇总
        Map<Integer,Map<String,BigDecimal>> payTypeStastic = new HashMap<Integer, Map<String, BigDecimal>>();
        //处置支付
        BigDecimal totalStoragePayPrice = BigDecimal.ZERO;
        //押金收入汇总
        //Map<Integer,Map<String,BigDecimal>> depositStastic = new HashMap<Integer, Map<String, BigDecimal>>();
        //押金收入汇总
        List<Map<String,Object>> allStastics = new ArrayList<Map<String,Object>>();
        //租赁物明细查询
        Map<String,Map<String, Object>> rentListMap =new HashMap<String,Map<String, Object>>();
        Map<String,Map<String, Object>> rentIndemnityListMap =new HashMap<String,Map<String, Object>>();

        //增值服务统计
        Map<String,Map<String, Object>> addServiceListMap =new HashMap<String,Map<String, Object>>();

        //教练课程  ..产品名称》场次名称》票券名称
        Map<String,Map<String,Map<String,Map<String,Object>>>> coachCourseListMap= new HashMap<String,Map<String,Map<String,Map<String,Object>>>>();
        //教练课时卡
        Map<String,Map<String,Map<String,Map<String,Object>>>> coachCardListMap= new HashMap<String,Map<String,Map<String,Map<String,Object>>>>();
        //教练预约
        Map<String,Map<String,Map<String,Map<String,Object>>>> coachVisitListMap= new HashMap<String,Map<String,Map<String,Map<String,Object>>>>();
        //教练支付方式汇总
        Map<Integer,Map<String,BigDecimal>> coachPayTypeStastic = new HashMap<Integer, Map<String, BigDecimal>>();

        //分销商
        Map<String,Map<String,Map<String,Object>>> distributorMap = new HashMap<String,Map<String,Map<String,Object>>>();
        Map<String,Map<String,Map<String,Object>>> distributorProductMap = new HashMap<String,Map<String,Map<String,Object>>>();
        Map<String,Map<String,Object>> distributorStasticsMap = new HashMap<String,Map<String,Object>>();

        if(orderslist!=null){
            for (Orders orders : orderslist) {

                BigDecimal deposit = BigDecimal.ZERO;
                if(orders.getParentId() != null){
                    deposit = orders.getDeposit();
                }
                if(deposit==null){
                    deposit = BigDecimal.ZERO;
                }

                //分销商数据组装
                if(orders.getDistributorId()!=null){
                    //分销商统计
                    Map<String, Object> dsmp = distributorStasticsMap.get(orders.getDistributorName());
                    if(dsmp==null){
                        dsmp = new HashMap<String, Object>();
                        distributorStasticsMap.put(orders.getDistributorName(),dsmp);
                    }
                    Integer dsnum = MapUtils.getInteger(dsmp, "num", 0);
                    double price = MapUtils.getDoubleValue(dsmp, "price", 0);
                    dsmp.put("num",dsnum+orders.getChargeNum());
                    dsmp.put("price",price+orders.getTotalPrice().doubleValue()-deposit.doubleValue());


                    //分销商产品
                    String activityTypeIdName = FinanceExcelUtil.getActivityTypeIdName(orders.getActivityTypeId());
                    Map<String,Map<String,Object>> epMap = distributorProductMap.get(activityTypeIdName);
                    if(epMap == null){
                        epMap = new HashMap<String,Map<String,Object>>();
                        distributorProductMap.put(activityTypeIdName,epMap);
                    }

                    Map<String, Object> activityMap = epMap.get(orders.getActivityName());
                    if(activityMap == null){
                        activityMap = new HashMap<String, Object>();
                        epMap.put(orders.getActivityName(),activityMap);
                    }

                    Integer num = MapUtils.getInteger(activityMap, "num", 0);
                    double money = MapUtils.getDoubleValue(activityMap, "money", 0);
                    activityMap.put("num",num+orders.getChargeNum());
                    activityMap.put("money",money+orders.getTotalPrice().doubleValue()-deposit.doubleValue());

                    //分销商统计
                    Map<String,Map<String,Object>> epDistributorMap = distributorMap.get(orders.getDistributorName());
                    if(epDistributorMap == null){
                        epDistributorMap = new HashMap<String,Map<String,Object>>();
                        distributorMap.put(orders.getDistributorName(),epDistributorMap);
                    }

                    Map<String, Object> activityDistributorMap = epDistributorMap.get(orders.getActivityName());
                    if(activityDistributorMap == null){
                        activityDistributorMap = new HashMap<String, Object>();
                        epDistributorMap.put(orders.getActivityName(),activityDistributorMap);
                    }

                    Integer numDistributor = MapUtils.getInteger(activityDistributorMap, "num", 0);
                    double moneyDistributor = MapUtils.getDoubleValue(activityDistributorMap, "money", 0);
                    activityDistributorMap.put("num",numDistributor+orders.getChargeNum());
                    activityDistributorMap.put("money",moneyDistributor+orders.getTotalPrice().doubleValue()-deposit.doubleValue());
                }

                //押金收入
                /*if(orders.getParentId()!=null && orders.getDistributorId() == null){
                    Map<String,BigDecimal> depositStasticMap = depositStastic.get(orders.getPayTypeId());
                    if(depositStasticMap == null){
                        depositStasticMap=new HashMap<String,BigDecimal>();
                        depositStasticMap.put("0",deposit);
                        depositStastic.put(orders.getPayTypeId(),depositStasticMap);
                    }else{
                        BigDecimal totalPrice=depositStasticMap.get("0")==null?BigDecimal.ZERO:depositStasticMap.get("0");
                        totalPrice=totalPrice.add(deposit);
                        depositStasticMap.put("0",totalPrice);
                        depositStastic.put(orders.getPayTypeId(),depositStasticMap);
                    }
                    totalInDeposit = totalInDeposit.add(deposit);
                }*/

                //产品统计(不包含租赁物和增加值服务)
                if(orders.getDistributorId()==null && orders.getEquipmentId()==null && orders.getParentId()!=null && orders.getIncrement().intValue()!=1 && orders.getDistributorId()==null){

                    //筛选类型
                    Map<String, Map<String,Object>> map = saleProductStastic.get(orders.getActivityTypeId());
                    if(map == null){
                        map = new HashMap<String, Map<String,Object>>();
                        saleProductStastic.put(orders.getActivityTypeId(),map);
                    }

                    //商品
                    Map<String, Object> activityMap = map.get(orders.getActivityName());
                    if(activityMap==null){
                        activityMap = new HashMap<String, Object>();
                        map.put(orders.getActivityName(),activityMap);
                    }

                    int num = MapUtils.getIntValue(activityMap, "num", 0);
                    activityMap.put("num",num + orders.getChargeNum());

                    double price = MapUtils.getDoubleValue(activityMap, "price", 0);
                    activityMap.put("price",orders.getTotalPrice().doubleValue() + price-deposit.doubleValue());

                    //线上微信  0 线上储值支付 7

                    String keyPrice = orders.getPayTypeId().intValue() + "_price";
                    String keyNum = orders.getPayTypeId().intValue() + "_num";
                    double priceValue = MapUtils.getDoubleValue(activityMap, keyPrice, 0);
                    Integer numValue = MapUtils.getInteger(activityMap, keyNum, 0);
                    activityMap.put(keyPrice,orders.getTotalPrice().doubleValue() + priceValue);
                    activityMap.put(keyNum,orders.getChargeNum() + numValue);

                    /*if("1".equals(String.valueOf(orders.getPayTypeId()))){
                        double xianjinPrice = MapUtils.getDoubleValue(activityMap, "weixinPrice", 0);
                        Integer xianjinNum = MapUtils.getInteger(activityMap, "weixinNum", 0);
                        activityMap.put("weixinPrice",orders.getTotalPrice().doubleValue() + xianjinPrice);
                        activityMap.put("weixinNum",orders.getChargeNum() + xianjinNum);
                    }else if("7".equals(String.valueOf(orders.getPayTypeId()))){
                        double xianjinPrice = MapUtils.getDoubleValue(activityMap, "chuzhiPrice", 0);
                        Integer xianjinNum = MapUtils.getInteger(activityMap, "chuzhiNum", 0);
                        activityMap.put("chuzhiPrice",orders.getTotalPrice().doubleValue() + xianjinPrice);
                        activityMap.put("chuzhiNum",orders.getChargeNum() + xianjinNum);
                    }*/
                }

                //增值服务统计
                if(orders.getEquipmentId()==null && orders.getParentId()!=null && orders.getIncrement().intValue()==1 && orders.getDistributorId()==null){

                    Map<String, Object> addServiceMap = addServiceListMap.get(orders.getChargeName());
                    if(addServiceMap==null){
                        addServiceMap = new HashMap<String, Object>();
                        addServiceListMap.put(orders.getChargeName(),addServiceMap);
                    }

                    int num = MapUtils.getIntValue(addServiceMap, "num", 0);
                    addServiceMap.put("num",num + orders.getChargeNum());

                    double price = MapUtils.getDoubleValue(addServiceMap, "price", 0);
                    addServiceMap.put("price",orders.getTotalPrice().doubleValue() + price- deposit.doubleValue());

                    String keyPrice = orders.getPayTypeId().intValue() + "_price";
                    String keyNum = orders.getPayTypeId().intValue() + "_num";
                    double priceValue = MapUtils.getDoubleValue(addServiceMap, keyPrice, 0);
                    Integer numValue = MapUtils.getInteger(addServiceMap, keyNum, 0);
                    addServiceMap.put(keyPrice,orders.getTotalPrice().doubleValue() + priceValue);
                    addServiceMap.put(keyNum,orders.getChargeNum() + numValue);

                    //线下现金  0
                    /*if("1".equals(String.valueOf(orders.getPayTypeId()))){
                        double xianjinPrice = MapUtils.getDoubleValue(addServiceMap, "weixinPrice", 0);
                        Integer xianjinNum = MapUtils.getInteger(addServiceMap, "weixinNum", 0);
                        addServiceMap.put("weixinPrice",orders.getTotalPrice().doubleValue() + xianjinPrice);
                        addServiceMap.put("weixinNum",orders.getChargeNum() + xianjinNum);
                    }else if("7".equals(String.valueOf(orders.getPayTypeId()))){
                        double xianjinPrice = MapUtils.getDoubleValue(addServiceMap, "chuzhiPrice", 0);
                        Integer xianjinNum = MapUtils.getInteger(addServiceMap, "chuzhiNum", 0);
                        addServiceMap.put("chuzhiPrice",orders.getTotalPrice().doubleValue() + xianjinPrice);
                        addServiceMap.put("chuzhiNum",orders.getChargeNum() + xianjinNum);
                    }*/
                }

                //租赁物统计
                if(orders.getEquipmentId()!=null && orders.getParentId()!=null && orders.getDistributorId()==null){
                    //商品
                    Map<String, Object> rentMap = rentListMap.get(orders.getEquipmentName());
                    if(rentMap==null){
                        rentMap = new HashMap<String, Object>();
                        rentListMap.put(orders.getEquipmentName(),rentMap);
                    }

                    int num = MapUtils.getIntValue(rentMap, "num", 0);
                    rentMap.put("num",num + orders.getChargeNum());

                    double price = MapUtils.getDoubleValue(rentMap, "price", 0);
                    rentMap.put("price",orders.getTotalPrice().doubleValue() + price-deposit.doubleValue());

                    String keyPrice = orders.getPayTypeId().intValue() + "_price";
                    String keyNum = orders.getPayTypeId().intValue() + "_num";
                    double priceValue = MapUtils.getDoubleValue(rentMap, keyPrice, 0);
                    Integer numValue = MapUtils.getInteger(rentMap, keyNum, 0);
                    rentMap.put(keyPrice,orders.getTotalPrice().doubleValue() + priceValue);
                    rentMap.put(keyNum,orders.getChargeNum() + numValue);

                    /*if("1".equals(String.valueOf(orders.getPayTypeId()))){
                        double xianjinPrice = MapUtils.getDoubleValue(rentMap, "weixinPrice", 0);
                        Integer xianjinNum = MapUtils.getInteger(rentMap, "weixinNum", 0);
                        rentMap.put("weixinPrice",orders.getTotalPrice().doubleValue() + xianjinPrice);
                        rentMap.put("weixinNum",orders.getChargeNum() + xianjinNum);
                    }else if("7".equals(String.valueOf(orders.getPayTypeId()))){
                        double xianjinPrice = MapUtils.getDoubleValue(rentMap, "chuzhiPrice", 0);
                        Integer xianjinNum = MapUtils.getInteger(rentMap, "chuzhiNum", 0);
                        rentMap.put("chuzhiPrice",orders.getTotalPrice().doubleValue() + xianjinPrice);
                        rentMap.put("chuzhiNum",orders.getChargeNum() + xianjinNum);
                    }*/
                }
                //支付统计
                if(orders.getParentId() != null && orders.getDistributorId()==null){

                    BigDecimal totalOrdersPrice = orders.getTotalPrice();
                    /*if(orders.getParentId().intValue()==0){
                        totalOrdersPrice = orders.getTotalPrice();
                    }else{
                        totalOrdersPrice = orders.getTotalPrice();
                    }*/

                    Map<String,BigDecimal> payTypeMap = payTypeStastic.get(orders.getPayTypeId());
                    if(payTypeMap==null){
                        payTypeMap=new HashMap<String,BigDecimal>();
                        payTypeMap.put("0",totalOrdersPrice.setScale(2,RoundingMode.HALF_UP));
                        payTypeStastic.put(orders.getPayTypeId(),payTypeMap);
                    }else{
                        BigDecimal totalPrice=payTypeMap.get("0")==null?BigDecimal.ZERO:payTypeMap.get("0");
                        totalPrice=totalPrice.add(totalOrdersPrice).setScale(2,RoundingMode.HALF_UP);
                        payTypeMap.put("0",totalPrice);
                        payTypeStastic.put(orders.getPayTypeId(),payTypeMap);
                    }

                    totalOrderPrice = totalOrderPrice.add(orders.getTotalPrice()).setScale(2,RoundingMode.HALF_UP);
                    if(orders.getPayTypeId().intValue()==8 || orders.getPayTypeId().intValue()==7){
                        totalStoragePayPrice = totalStoragePayPrice.add(orders.getTotalPrice());
                    }
                }

                //教练预约
                if(orders.getParentId()!=null&&"2".equals(String.valueOf(orders.getActivityTypeId()))){
                    //产品
                    Map<String,Map<String, Map<String,Object>>> activityMap = coachVisitListMap.get("教练预约");
                    if(activityMap == null){
                        activityMap = new HashMap<String, Map<String,Map<String,Object>>>();
                        coachVisitListMap.put("教练预约",activityMap);
                    }

                    //场次
                    Map<String,Map<String, Object>> episodeNameMap = activityMap.get(orders.getActivityName());
                    if(episodeNameMap==null){
                        episodeNameMap = new HashMap<String,Map<String, Object>>();
                        activityMap.put(orders.getActivityName(),episodeNameMap);
                    }
                    //票券
                    Map<String, Object> chargeMap = episodeNameMap.get(orders.getChargeName());
                    if(chargeMap==null){
                        chargeMap = new HashMap<String, Object>();
                        episodeNameMap.put(orders.getChargeName(),chargeMap);
                    }

                    int num = MapUtils.getIntValue(chargeMap, "num", 0);
                    chargeMap.put("num",num + orders.getChargeNum());

                    double price = MapUtils.getDoubleValue(chargeMap, "price", 0);
                    chargeMap.put("price",orders.getTotalPrice().doubleValue() + price);
                }
                //教练课程
                if(orders.getParentId()!=null&&"8".equals(String.valueOf(orders.getActivityTypeId()))&&"0".equals(String.valueOf(orders.getActivityMode()))){
                    //产品
                    Map<String,Map<String, Map<String,Object>>> activityMap = coachCourseListMap.get("课程");
                    if(activityMap == null){
                        activityMap = new HashMap<String, Map<String,Map<String,Object>>>();
                        coachCourseListMap.put("课程",activityMap);
                    }

                    //场次
                    Map<String,Map<String, Object>> episodeNameMap = activityMap.get(orders.getActivityName());
                    if(episodeNameMap==null){
                        episodeNameMap = new HashMap<String,Map<String, Object>>();
                        activityMap.put(orders.getActivityName(),episodeNameMap);
                    }
                    //票券
                    Map<String, Object> chargeMap = episodeNameMap.get(orders.getChargeName());
                    if(chargeMap==null){
                        chargeMap = new HashMap<String, Object>();
                        episodeNameMap.put(orders.getChargeName(),chargeMap);
                    }

                    int num = MapUtils.getIntValue(chargeMap, "num", 0);
                    chargeMap.put("num",num + orders.getChargeNum());

                    double price = MapUtils.getDoubleValue(chargeMap, "price", 0);
                    chargeMap.put("price",orders.getTotalPrice().doubleValue() + price);
                }
                //教练课时卡
                if(orders.getParentId()!=null&&"8".equals(String.valueOf(orders.getActivityTypeId())) && "1".equals(String.valueOf(orders.getActivityMode()))){
                        //if("2".equals(String.valueOf(orders.getExtCanRefund()))){
                            //产品
                            Map<String,Map<String, Map<String,Object>>> activityMap = coachCardListMap.get("课时卡");
                            if(activityMap == null){
                                activityMap = new HashMap<String, Map<String,Map<String,Object>>>();
                                coachCardListMap.put("课时卡",activityMap);
                            }

                            //场次
                            Map<String,Map<String, Object>> episodeNameMap = activityMap.get(orders.getActivityName());
                            if(episodeNameMap==null){
                                episodeNameMap = new HashMap<String,Map<String, Object>>();
                                activityMap.put(orders.getActivityName(),episodeNameMap);
                            }
                            //票券
                            Map<String, Object> chargeMap = episodeNameMap.get(orders.getChargeName());
                            if(chargeMap==null){
                                chargeMap = new HashMap<String, Object>();
                                episodeNameMap.put(orders.getChargeName(),chargeMap);
                            }

                            int num = MapUtils.getIntValue(chargeMap, "num", 0);
                            chargeMap.put("num",num + orders.getChargeNum());

                            double price = MapUtils.getDoubleValue(chargeMap, "price", 0);
                            chargeMap.put("price",orders.getTotalPrice().doubleValue() + price);
                        //}

                }
                if(orders.getParentId()!=null&&("2".equals(String.valueOf(orders.getActivityTypeId()))||"8".equals(String.valueOf(orders.getActivityTypeId())))){
                    Map<String,BigDecimal> coachPayTypeMap = coachPayTypeStastic.get(orders.getPayTypeId());
                    if(coachPayTypeMap==null){
                        coachPayTypeMap=new HashMap<String,BigDecimal>();
                        coachPayTypeMap.put("0",orders.getTotalPrice().setScale(2,RoundingMode.HALF_UP));
                        coachPayTypeStastic.put(orders.getPayTypeId(),coachPayTypeMap);
                    }else{
                        BigDecimal totalPrice=coachPayTypeMap.get("0")==null?BigDecimal.ZERO:coachPayTypeMap.get("0");
                        totalPrice=totalPrice.add(orders.getTotalPrice()).setScale(2,RoundingMode.HALF_UP);
                        coachPayTypeMap.put("0",totalPrice);
                        coachPayTypeStastic.put(orders.getPayTypeId(),coachPayTypeMap);
                    }
                }
            }
        }
        //租赁物
        List<Map<String, Object>> rentOrders = ordersService.getOnlineStasticsRentByStartEndTime(startTime, endTime, organizer.getOrganizerId(), null);
        //超时租赁物
        List<Map<String, Object>> rentTimeOutOrders = ordersService.getOnlineStasticsRentByStartEndTime(startTime, endTime,organizer.getOrganizerId(),4);
        //租赁物和超时租赁物统计
        Map<String,Map<String, Object>> rentAndTimeOutOrders =new HashMap<String,Map<String, Object>>();
        if(rentOrders!=null&&rentOrders.size()>0){
            for (int i = 0; i < rentOrders.size(); i++) {
                Map<String, Object> map=rentOrders.get(i);
                String name=MapUtils.getString(map,"name","");
                Map<String,Object> rentAndTimeOutMap = rentAndTimeOutOrders.get(name);
                if(rentAndTimeOutMap==null){
                    rentAndTimeOutMap=new HashMap<String,Object>();
                    rentAndTimeOutMap.put("totalMoney",new BigDecimal(MapUtils.getDouble(map,"totalMoney",0.00)).setScale(2,RoundingMode.HALF_UP));
                    rentAndTimeOutMap.put("totalNum",MapUtils.getIntValue(map,"totalNum",0));
                    rentAndTimeOutMap.put("timeOutTotalMoney",BigDecimal.ZERO);
                    rentAndTimeOutMap.put("timeOutTotalNum",0);
                    rentAndTimeOutOrders.put(name,rentAndTimeOutMap);
                }else{
                    BigDecimal totalPrice=rentAndTimeOutMap.get("totalMoney")==null?BigDecimal.ZERO:new BigDecimal(MapUtils.getDouble(rentAndTimeOutMap,"totalMoney",0.00)).setScale(2,RoundingMode.HALF_UP);
                    totalPrice=totalPrice.add(new BigDecimal(MapUtils.getDouble(map,"totalMoney",0.00))).setScale(2,RoundingMode.HALF_UP);
                    rentAndTimeOutMap.put("totalMoney",totalPrice);
                    Integer totalNum=rentAndTimeOutMap.get("totalNum")==null?0:Integer.parseInt(rentAndTimeOutMap.get("totalNum").toString());
                    totalNum=totalNum+Integer.parseInt(map.get("totalNum").toString());
                    rentAndTimeOutMap.put("totalNum",totalNum);
                    rentAndTimeOutOrders.put(name,rentAndTimeOutMap);
                }
            }
        }
        if(rentTimeOutOrders!=null&&rentTimeOutOrders.size()>0){
            for (int i = 0; i < rentTimeOutOrders.size(); i++) {
                Map<String, Object> map=rentTimeOutOrders.get(i);
                String name=MapUtils.getString(map,"name","");
                Map<String,Object> rentAndTimeOutMap = rentAndTimeOutOrders.get(name);
                if(rentAndTimeOutMap==null){
                    rentAndTimeOutMap=new HashMap<String,Object>();
                    rentAndTimeOutMap.put("totalMoney",BigDecimal.ZERO);
                    rentAndTimeOutMap.put("totalNum",0);
                    rentAndTimeOutMap.put("timeOutTotalMoney",new BigDecimal(MapUtils.getDouble(map,"timeOutTotalMoney",0.00)).setScale(2,RoundingMode.HALF_UP));
                    rentAndTimeOutMap.put("timeOutTotalNum",MapUtils.getIntValue(map,"timeOutTotalNum",0));
                    rentAndTimeOutOrders.put(name,rentAndTimeOutMap);
                }else{
                    BigDecimal totalPrice=new BigDecimal(MapUtils.getDouble(rentAndTimeOutMap,"timeOutTotalMoney",0.00)).setScale(2,RoundingMode.HALF_UP);
                    totalPrice=totalPrice.add(new BigDecimal(MapUtils.getDouble(map,"totalMoney",0.00))).setScale(2,RoundingMode.HALF_UP);
                    rentAndTimeOutMap.put("timeOutTotalMoney",totalPrice);
                    Integer totalNum=MapUtils.getInteger(rentAndTimeOutMap,"timeOutTotalNum",0);
                    totalNum=totalNum+MapUtils.getInteger(map,"totalNum",0);
                    rentAndTimeOutMap.put("timeOutTotalNum",totalNum);
                    rentAndTimeOutOrders.put(name,rentAndTimeOutMap);
                }
            }
        }

        //增值服务
        //List<Map<String, Object>> addServiceOrders = ordersService.getOnlineStasticsAddServiceByTime(startTime, endTime, organizer.getOrganizerId());
        //分销渠道
        //List<Map<String, Object>> distributorOrders = ordersService.getStasticsDistributorByTime(startTime, endTime, organizer.getOrganizerId());
        //赔偿
        ZooBalanceDepositIndemnityExample indemnityExample = new ZooBalanceDepositIndemnityExample();
        ZooBalanceDepositIndemnityExample.Criteria indemnityExampleCriteria = indemnityExample.createCriteria();
        indemnityExampleCriteria.andOrganizerIdEqualTo(organizer.getOrganizerId());
        // indemnityExampleCriteria.andPayTypeEqualTo(1);
        indemnityExampleCriteria.andPayTypeIn(ActivityBeanUtils.OnlinePayNameEnum.getPayTypeIds());
        if(startTime!=null){
            indemnityExampleCriteria.andIndemnityTimeGreaterThanOrEqualTo(startTime);
        }
        if(endTime !=null){
            indemnityExampleCriteria.andIndemnityTimeLessThan(endTime);
        }
        List<ZooBalanceDepositIndemnity> depositIndemnityList = zooBalanceDepositIndemnityService.selectByExample(indemnityExample);
        for (ZooBalanceDepositIndemnity depositIndemnity : depositIndemnityList) {
            Integer equipmentId = depositIndemnity.getEquipmentId();
            Map<String, Object> p = indemnityProduct.get(equipmentId);

            if(p == null){
                p = new HashMap<String,Object>();
                indemnityProduct.put(equipmentId,p);
                p.put("money",BigDecimal.ZERO);
                p.put("name",depositIndemnity.getEquipmentName());
            }
            int num = MapUtils.getIntValue(p,"num",0);
            p.put("num",num+1);
            BigDecimal money = (BigDecimal)p.get("money");
            p.put("money",money.add(depositIndemnity.getIndemnity()).setScale(2,RoundingMode.HALF_UP));
            totalIndemnityPrice = totalIndemnityPrice.add(depositIndemnity.getIndemnity()).setScale(2,RoundingMode.HALF_UP);

            if("1".equals(String.valueOf(depositIndemnity.getPayType())) || "7".equals(String.valueOf(depositIndemnity.getPayType()))){
                //商品
                Map<String, Object> rentIndemnityMap = rentIndemnityListMap.get(depositIndemnity.getEquipmentName());
                if(rentIndemnityMap==null){
                    rentIndemnityMap = new HashMap<String, Object>();
                    rentIndemnityListMap.put(depositIndemnity.getEquipmentName(),rentIndemnityMap);
                }

                int indemnityNum = MapUtils.getIntValue(rentIndemnityMap, "indemnityNum", 0);
                rentIndemnityMap.put("indemnityNum",indemnityNum + 1);

                double price = MapUtils.getDoubleValue(rentIndemnityMap, "price", 0);
                rentIndemnityMap.put("price",depositIndemnity.getIndemnity().doubleValue() + price);

                String keyPrice = depositIndemnity.getPayType().intValue() + "_price";
                String keyNum = depositIndemnity.getPayType().intValue() + "_num";
                double priceValue = MapUtils.getDoubleValue(rentIndemnityMap, keyPrice, 0);
                Integer numValue = MapUtils.getInteger(rentIndemnityMap, keyNum, 0);
                rentIndemnityMap.put(keyPrice,depositIndemnity.getIndemnity().doubleValue() + priceValue);
                rentIndemnityMap.put(keyNum,numValue + 1);

                //线下微信 4
                /*double weixinPrice = MapUtils.getDoubleValue(rentIndemnityMap, "weixinPrice", 0);
                Integer weixinNum = MapUtils.getInteger(rentIndemnityMap, "weixinNum", 0);
                if("1".equals(String.valueOf(depositIndemnity.getPayType()))){
                    rentIndemnityMap.put("weixinPrice",depositIndemnity.getIndemnity().doubleValue() + weixinPrice);
                    rentIndemnityMap.put("weixinNum",weixinNum+1);
                }else{
                    if(rentIndemnityMap.get("weixinPrice")==null){
                        rentIndemnityMap.put("weixinPrice",weixinPrice);
                        rentIndemnityMap.put("weixinNum",weixinNum);
                    }
                }*/

            }
        }

        //押金支付方式汇总
        /*List<Map<String, Object>> stasticsDepositRefundOrders = ordersService.getOnlineStasticsDepositRefundByTime(startTime, endTime, organizer.getOrganizerId(),null);
        for (int i = 0; i < stasticsDepositRefundOrders.size(); i++) {
            Map<String, Object> map=stasticsDepositRefundOrders.get(i);
            Integer payTypeId=MapUtils.getInteger(map,"pay_type_id",0);
            Map<String,BigDecimal> depositStasticMap = depositStastic.get(payTypeId);
            if(depositStasticMap==null){
                depositStasticMap=new HashMap<String,BigDecimal>();
                depositStasticMap.put("1",new BigDecimal(MapUtils.getDouble(map,"totalMoney",0.00)).setScale(2,RoundingMode.HALF_UP));
                depositStastic.put(payTypeId,depositStasticMap);
            }else{
                BigDecimal totalPrice=depositStasticMap.get("1")==null?BigDecimal.ZERO:depositStasticMap.get("1");
                totalPrice=totalPrice.add(new BigDecimal(MapUtils.getDouble(map,"totalMoney",0.00))).setScale(2,RoundingMode.HALF_UP);
                depositStasticMap.put("1",totalPrice);
                depositStastic.put(payTypeId,depositStasticMap);
            }
            totalOutDeposit = totalOutDeposit.add(new BigDecimal(MapUtils.getDouble(map,"totalMoney",0.00))).setScale(2,RoundingMode.HALF_UP);

            //支付方式支出统计
            Map<String,BigDecimal> payTypeMap = payTypeStastic.get(payTypeId);
            if(payTypeMap==null){
                payTypeMap=new HashMap<String,BigDecimal>();
                payTypeMap.put("1",new BigDecimal(MapUtils.getDouble(map,"totalMoney",0.00)).setScale(2,RoundingMode.HALF_UP));
                payTypeStastic.put(payTypeId,payTypeMap);
            }else{
                BigDecimal totalPrice=payTypeMap.get("1")==null?BigDecimal.ZERO:payTypeMap.get("1");
                totalPrice=totalPrice.add(new BigDecimal(MapUtils.getDouble(map,"totalMoney",0.00))).setScale(2,RoundingMode.HALF_UP);
                payTypeMap.put("1",totalPrice);
                payTypeStastic.put(payTypeId,payTypeMap);
            }
        }*/

        //教练押金支出
        /*List<Map<String, Object>> coachDepositRefundOrders = ordersService.getCoachDepositRefundsByTime(startTime, endTime, organizer.getOrganizerId());
        if(coachDepositRefundOrders!=null&&coachDepositRefundOrders.size()>0){
            for (int i = 0; i < coachDepositRefundOrders.size(); i++) {
                Map<String, Object> map=coachDepositRefundOrders.get(i);
                Integer payTypeId=MapUtils.getInteger(map,"pay_type_id",0);
                Map<String,BigDecimal> coachPayTypeMap = coachPayTypeStastic.get(payTypeId);
                if(coachPayTypeMap==null){
                    coachPayTypeMap=new HashMap<String,BigDecimal>();
                    coachPayTypeMap.put("1",new BigDecimal(MapUtils.getDouble(map,"totalMoney",0.00)).setScale(2,RoundingMode.HALF_UP));
                    coachPayTypeStastic.put(payTypeId,coachPayTypeMap);
                }else{
                    BigDecimal totalPrice=coachPayTypeMap.get("1")==null?BigDecimal.ZERO:coachPayTypeMap.get("1");
                    totalPrice=totalPrice.add(new BigDecimal(MapUtils.getDouble(map,"totalMoney",0.00))).setScale(2,RoundingMode.HALF_UP);
                    coachPayTypeMap.put("1",totalPrice);
                    coachPayTypeStastic.put(payTypeId,coachPayTypeMap);
                }
            }
        }*/
        //汇总

        Map<String,Object> map1=new HashMap<String,Object>();
        map1.put("money",totalOrderPrice.subtract(totalStoragePayPrice).setScale(2,RoundingMode.HALF_UP));
        map1.put("name","收入金额");
        allStastics.add(map1);
        Map<String,Object> map2=new HashMap<String,Object>();
        map2.put("money",totalRefundPrice);
        map2.put("name","异常退款金额");
        allStastics.add(map2);
        /*Map<String,Object> map3=new HashMap<String,Object>();
        map3.put("money",totalOrderPrice.subtract(totalRefundPrice).setScale(2,RoundingMode.HALF_UP));
        map3.put("name","应收金额");
        allStastics.add(map3);*/
        Map<String,Object> map4=new HashMap<String,Object>();
        map4.put("money",totalIndemnityPrice);
        map4.put("name","赔偿金额");
        allStastics.add(map4);
        /*Map<String,Object> map5=new HashMap<String,Object>();
        map5.put("money",totalInDeposit.subtract(totalOutDeposit).setScale(2,RoundingMode.HALF_UP));
        map5.put("name","押金合计");
        allStastics.add(map5);*/
        Map<String,Object> map6=new HashMap<String,Object>();
        map6.put("money",totalOrderPrice.add(totalIndemnityPrice).subtract(totalStoragePayPrice).setScale(2,RoundingMode.HALF_UP));
        map6.put("name","实收金额");
        allStastics.add(map6);

        HSSFWorkbook wb = new HSSFWorkbook();
        //财务汇总表
        createStasticsSheet(startTime,endTime,totalStoragePayPrice,saleProductStastic,rentOrders,
                rentTimeOutOrders,addServiceListMap,distributorStasticsMap,indemnityProduct,
                refundProduct,payTypeStastic,null,allStastics,wb);

        //产品统计
        createProductStasticSheet(wb,saleProductStastic);

        //租赁物统计
        createRenStasticSheet(wb,rentAndTimeOutOrders,indemnityProduct,rentListMap,rentIndemnityListMap);

        //增值服务
        createAddServiceStasticSheet(wb,addServiceListMap);

        //分销商统计
        createDistributorStasticSheet(wb,distributorMap,distributorProductMap);

        //教练sheet，coachCourseListMap
        createCoachStasticSheet(wb,coachCourseListMap,coachCardListMap,coachVisitListMap,coachPayTypeStastic);

        wb.write(out);
    }

    /**
     * 分销商统计
     * @param wb
     * @return
     */
    private HSSFSheet createDistributorStasticSheet(HSSFWorkbook wb,Map<String,Map<String,Map<String,Object>>> distributorMap,Map<String,Map<String,Map<String,Object>>> distributorProductMap) {
        HSSFSheet sheet = wb.createSheet("分销商统计");
        int rowNum = 1;

        HSSFCellStyle headStyle = wb.createCellStyle(); // 样式对象
        HSSFFont font = wb.createFont();// 生成一个字体
        font.setColor(HSSFColor.BLACK.index);// HSSFColor.VIOLET.index // 字体颜色
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);// 字体增粗
        font.setFontHeightInPoints((short)16);
        headStyle.setFont(font);// 把字体应用到当前的样式
        headStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        headStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);//垂直居中
        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = row.createCell(1);
        cell.setCellStyle(headStyle);
        cell.setCellValue("分销商销售统计");

        int[] startIndex = new int[0];

        rowNum++;
        createProductPayStaticHead(wb,sheet,rowNum,new String[]{"分销商名称", "产品名称", "核销数量","收入金额"});
        if(distributorMap!=null){
            rowNum++;
            //分销商销售
            rowNum = FinanceExcelUtil.createOrderNumMoneyItem(wb,sheet,rowNum,distributorMap);
            rowNum++;
        }

        row = sheet.createRow(rowNum);
        cell = row.createCell(1);
        cell.setCellStyle(headStyle);
        cell.setCellValue("分销产品统计");
        rowNum++;
        createProductPayStaticHead(wb,sheet,rowNum,new String[]{"产品类型", "产品名称", "核销数量","收入金额"});
        if(distributorProductMap!=null){
            rowNum++;
            //分销产品统计
            rowNum = FinanceExcelUtil.createOrderNumMoneyItem(wb,sheet,rowNum,distributorProductMap);
            rowNum++;
        }
        rowNum++;
        return sheet;
    }

    /**
     * 教练课程统计
     * @param wb
     * @return
     */
    private HSSFSheet createCoachStasticSheet(HSSFWorkbook wb,Map<String,Map<String,Map<String,Map<String,Object>>>> coachCourseListMap,Map<String,Map<String,Map<String,Map<String,Object>>>> coachCardListMap,Map<String,Map<String,Map<String,Map<String,Object>>>> coachVisitListMap,Map<Integer,Map<String,BigDecimal>> coachPayTypeStastic) {
        HSSFSheet sheet = wb.createSheet("教练课程统计");
        String[] head = {"活动名称", "场次名称", "票券名称", "数量","金额"};

        int rowNum = 1;

        HSSFCellStyle headStyle = wb.createCellStyle(); // 样式对象
        HSSFFont font = wb.createFont();// 生成一个字体
        font.setColor(HSSFColor.BLACK.index);// HSSFColor.VIOLET.index // 字体颜色
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);// 字体增粗
        font.setFontHeightInPoints((short)16);
        headStyle.setFont(font);// 把字体应用到当前的样式
        headStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        headStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);//垂直居中
        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = row.createCell(1);
        cell.setCellStyle(headStyle);
        cell.setCellValue("教练课程统计");

        int[] startIndex = new int[0];

        rowNum++;
        createProductPayStaticHead(wb,sheet,rowNum,head);
        rowNum++;
        if(coachCourseListMap!=null && coachCourseListMap.size()>0){
            //教练课程
            rowNum = FinanceExcelUtil.createCoachItem(wb,sheet,rowNum,coachCourseListMap);
            rowNum++;
            startIndex =ArrayUtils.add(startIndex,rowNum);
        }
        if(coachCardListMap!=null && coachCardListMap.size()>0){
            //教练课时卡
            rowNum = FinanceExcelUtil.createCoachItem(wb,sheet,rowNum,coachCardListMap);
            rowNum++;
            startIndex =ArrayUtils.add(startIndex,rowNum);
        }
        if(coachVisitListMap!=null && coachVisitListMap.size()>0){
            //教练预约
            rowNum = FinanceExcelUtil.createCoachItem(wb,sheet,rowNum,coachVisitListMap);
            rowNum++;
            startIndex =ArrayUtils.add(startIndex,rowNum);
        }

        row = sheet.createRow(rowNum);
        cell = row.createCell(1);
        cell.setCellStyle(headStyle);
        cell.setCellValue("教练支付方式汇总");
        rowNum++;
        if(coachPayTypeStastic!=null){
            FinanceExcelUtil.createPayTypeHead(wb,sheet,rowNum);
            rowNum++;
            //支付方式
            rowNum = FinanceExcelUtil.createPayTypeStasticItem(wb,sheet,rowNum,coachPayTypeStastic);
            rowNum++;
        }
        rowNum++;
        return sheet;
    }

    /**
     * 增值服务统计
     * @param wb
     * @return
     */
    private HSSFSheet createAddServiceStasticSheet(HSSFWorkbook wb,Map<String,Map<String, Object>> addServiceListMap) {
        HSSFSheet sheet = wb.createSheet("增值服务统计");
        String[] addServiceHead = {"增值服务", "核销数量", "收入金额"};

        List<String> listHeadNames = new ArrayList<String>();
        listHeadNames.add("增值服务");
        List<Integer> payTypeIds = ActivityBeanUtils.OnlinePayNameEnum.getPayTypeIds();
        for (Integer payTypeId : payTypeIds) {
            listHeadNames.add(ActivityBeanUtils.getPayName(payTypeId));
            listHeadNames.add("核销数量");
        }
        listHeadNames.add("金额合计");
        listHeadNames.add("数量合计");
        // String[] addServiceHeadList = {"增值服务","微信支付", "核销数量","线上储值支付", "核销数量","金额合计", "数量合计"};

        String[] addServiceHeadList = listHeadNames.toArray(new String[listHeadNames.size()]);

        int rowNum=0;
        rowNum++;
        rowNum=createAddServiceStaticTable(rowNum,wb,sheet,addServiceListMap,addServiceHead);
        rowNum++;
        rowNum++;
        rowNum=createAddServicePayStaticTable(rowNum,wb,sheet,addServiceListMap,addServiceHeadList);
        return sheet;
    }

    /**
     * 创建增值服务支付方式表格
     * @param wb
     * @param sheet
     * @param head
     */
    private int createAddServicePayStaticTable(int rowNum,HSSFWorkbook wb,HSSFSheet sheet,Map<String,Map<String, Object>> addServiceHeadList,String[] head){
        rowNum++;
        HSSFCellStyle headStyle = wb.createCellStyle(); // 样式对象
        HSSFFont font = wb.createFont();// 生成一个字体
        font.setColor(HSSFColor.BLACK.index);// HSSFColor.VIOLET.index // 字体颜色
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);// 字体增粗
        font.setFontHeightInPoints((short)16);
        headStyle.setFont(font);// 把字体应用到当前的样式
        headStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        headStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);//垂直居中
        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = row.createCell(1);
        cell.setCellStyle(headStyle);
        cell.setCellValue("增值服务支付方式统计");
        rowNum++;
        createProductPayStaticHead(wb,sheet,rowNum,head);
        rowNum++;
        rowNum=createRentPayStaticItem(wb,sheet,rowNum,addServiceHeadList,false);
        return rowNum;
    }

    /**
     * 创建增值服务表格
     * @param wb
     * @param sheet
     * @param head
     */
    private int createAddServiceStaticTable(int rowNum,HSSFWorkbook wb,HSSFSheet sheet,Map<String,Map<String, Object>> addServiceListMap,String[] head){
        rowNum++;
        HSSFCellStyle headStyle = wb.createCellStyle(); // 样式对象
        HSSFFont font = wb.createFont();// 生成一个字体
        font.setColor(HSSFColor.BLACK.index);// HSSFColor.VIOLET.index // 字体颜色
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);// 字体增粗
        font.setFontHeightInPoints((short)16);
        headStyle.setFont(font);// 把字体应用到当前的样式
        headStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        headStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);//垂直居中
        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = row.createCell(1);
        cell.setCellStyle(headStyle);
        cell.setCellValue("增值服务销售统计");
        rowNum++;
        createProductPayStaticHead(wb,sheet,rowNum,head);
        rowNum++;
        rowNum=createAddServiceStaticItem(wb,sheet,rowNum,addServiceListMap);
        return rowNum;
    }

    /**
     * 创建增值服务项
     * @param wb
     * @param sheet
     * @param rowNum
     * @return
     */
    private int createAddServiceStaticItem(HSSFWorkbook wb,HSSFSheet sheet,int rowNum,Map<String,Map<String, Object>> addServiceListMap){
        int rowStartIndex = rowNum;

        Iterator<String> iterator = addServiceListMap.keySet().iterator();
        while(iterator.hasNext()){
            String name = iterator.next();
            Map<String, Object> value = addServiceListMap.get(name);
            sheet.addMergedRegion(new CellRangeAddress(rowStartIndex, rowNum, 1, 1));
            HSSFCellStyle headStyle = wb.createCellStyle(); // 样式对象
            headStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
            headStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
            headStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
            headStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
            headStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
            headStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);//垂直居中
            HSSFRow row = sheet.createRow(rowNum);
            HSSFCell cell = row.createCell(1);
            cell.setCellValue(name);
            cell.setCellStyle(headStyle);
            cell = row.createCell(2);
            cell.setCellValue(MapUtils.getIntValue(value,"num",0));
            cell.setCellStyle(headStyle);
            cell = row.createCell(3);
            cell.setCellValue(new BigDecimal(MapUtils.getDoubleValue(value,"price",0)).setScale(2,RoundingMode.HALF_UP).doubleValue());
            cell.setCellStyle(headStyle);
            rowNum++;
        }
        HSSFCellStyle hjStyle = wb.createCellStyle(); // 样式对象
        hjStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        hjStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        hjStyle.setFillForegroundColor(HSSFColor.GREY_40_PERCENT.index);
        hjStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        hjStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        hjStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        hjStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);

        HSSFCellStyle numStyle = wb.createCellStyle(); // 样式对象
        numStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        numStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        numStyle.setFillForegroundColor(HSSFColor.GREY_40_PERCENT.index);
        numStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        numStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        numStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        numStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);

        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = row.createCell(1);
        cell.setCellValue("合计");
        cell.setCellStyle(numStyle);
        cell = row.createCell(2);
        String sc1 = CellReference.convertNumToColString(2);
        cell.setCellFormula("SUM(" + sc1 + rowStartIndex + ":"+sc1+rowNum+")");
        cell.setCellStyle(numStyle);
        HSSFDataFormat df = wb.createDataFormat();
        hjStyle.setDataFormat(df.getBuiltinFormat("0.00"));
        cell = row.createCell(3);
        String sc = CellReference.convertNumToColString(3);
        cell.setCellFormula("SUM(" + sc + rowStartIndex + ":"+sc+rowNum+")");
        cell.setCellStyle(hjStyle);
        return rowNum;
    }

    /**
     * 产品统计
     * @param wb
     * @param saleProductStastic
     * @return
     */
    private HSSFSheet createProductStasticSheet(HSSFWorkbook wb,Map<Integer,Map<String,Map<String,Object>>> saleProductStastic) {
        HSSFSheet sheet = wb.createSheet("产品统计");
        List<String> listHeadNames = new ArrayList<String>();
        listHeadNames.add("产品类型");
        listHeadNames.add("产品名称");

        List<Integer> payTypeIds = ActivityBeanUtils.OnlinePayNameEnum.getPayTypeIds();
        for (Integer payTypeId : payTypeIds) {
            listHeadNames.add(ActivityBeanUtils.getPayName(payTypeId));
            listHeadNames.add("核销数量");
        }
        listHeadNames.add("金额合计");
        listHeadNames.add("数量合计");
        // String[] head = {"产品类型", "产品名称", "微信支付", "核销数量","线上储值支付", "核销数量","金额合计", "数量合计"};
        String[] head = listHeadNames.toArray(new String[listHeadNames.size()]);
        createProductPayStaticTable(0,wb,sheet,saleProductStastic,head);
        return sheet;
    }

    /**
     * 租赁物统计
     * @param wb
     * @param rentAndTimeOutOrders
     * @return
     */
    private HSSFSheet createRenStasticSheet(HSSFWorkbook wb, Map<String,Map<String, Object>> rentAndTimeOutOrders,Map<Integer,Map<String,Object>> indemnityProduct,Map<String,Map<String, Object>> rentListMap,Map<String,Map<String, Object>> rentIndemnityListMap) {
        HSSFSheet sheet = wb.createSheet("租赁物统计");
        String[] head = {"租赁物名称", "核销数量", "收入金额", "超时数量", "超时金额", "应收金额"};
        String[] indemnityHead = {"租赁物名称", "赔偿数量", "赔偿金额"};

        List<String> listHeadNames = new ArrayList<String>();
        List<String> listIndemnityHeadNames = new ArrayList<String>();
        listHeadNames.add("租赁物名称");
        listIndemnityHeadNames.add("租赁物名称");

        List<Integer> payTypeIds = ActivityBeanUtils.OnlinePayNameEnum.getPayTypeIds();
        for (Integer payTypeId : payTypeIds) {
            listHeadNames.add(ActivityBeanUtils.getPayName(payTypeId));
            listHeadNames.add("核销数量");
            listIndemnityHeadNames.add(ActivityBeanUtils.getPayName(payTypeId));
            listIndemnityHeadNames.add("核销数量");
        }
        listHeadNames.add("金额合计");
        listHeadNames.add("数量合计");
        listIndemnityHeadNames.add("金额合计");
        listIndemnityHeadNames.add("数量合计");
        // String[] headList = {"租赁物名称", "微信支付", "核销数量","线上储值支付", "核销数量","金额合计", "数量合计"};
        String[] headList = listHeadNames.toArray(new String[listHeadNames.size()]);
        // String[] indemnityHeadList = {"租赁物名称","微信支付", "核销数量"};
        String[] indemnityHeadList = listIndemnityHeadNames.toArray(new String[listIndemnityHeadNames.size()]);;

        int rowNum=0;
        rowNum=createRentStaticTable(rowNum,wb,sheet,rentAndTimeOutOrders,head);
        rowNum++;
        rowNum=createRentIndemnityStaticTable(rowNum,wb,sheet,indemnityProduct,indemnityHead);
        rowNum++;
        rowNum++;
        rowNum=createRentPayStaticTable(rowNum,wb,sheet,rentListMap,headList);
        rowNum++;
        rowNum=createRentIndemnityPayStaticTable(rowNum,wb,sheet,rentIndemnityListMap,indemnityHeadList);
        return sheet;
    }

    /**
     * 创建表格
     * @param wb
     * @param sheet
     * @param head
     */
    private void createProductPayStaticTable(int rowNum,HSSFWorkbook wb,HSSFSheet sheet,Map<Integer,Map<String,Map<String,Object>>> saleProductStastic,String[] head){
        rowNum++;
        HSSFCellStyle headStyle = wb.createCellStyle(); // 样式对象
        HSSFFont font = wb.createFont();// 生成一个字体
        font.setColor(HSSFColor.BLACK.index);// HSSFColor.VIOLET.index // 字体颜色
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);// 字体增粗
        font.setFontHeightInPoints((short)16);
        headStyle.setFont(font);// 把字体应用到当前的样式
        headStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        headStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);//垂直居中
        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = row.createCell(1);
        cell.setCellStyle(headStyle);
        cell.setCellValue("产品支付方式统计");
        rowNum++;
        createProductPayStaticHead(wb,sheet,rowNum,head);
        rowNum++;
        createProdctPayStaticItem(wb,sheet,rowNum,saleProductStastic);
    }

    /**
     * 创建产品项
     * @param wb
     * @param sheet
     * @param rowNum
     * @param saleProductStastic
     * @return
     */
    private int createProdctPayStaticItem(HSSFWorkbook wb,HSSFSheet sheet,int rowNum,Map<Integer,Map<String,Map<String,Object>>> saleProductStastic){
        int rowStartIndex = rowNum;

        List<String> scNumlist = null;
        List<String> sclist = null;

        HSSFCellStyle headStyle = wb.createCellStyle(); // 样式对象
        headStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        headStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        headStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        headStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        headStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
        headStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);//垂直居中

        Iterator<Integer> iterator = saleProductStastic.keySet().iterator();
        while (iterator.hasNext()){
            Integer activityTypeId = iterator.next();
            Map<String, Map<String, Object>> stringMapMap = saleProductStastic.get(activityTypeId);

            int rowMergedNum = rowNum;
            Iterator<Map.Entry<String, Map<String, Object>>> pIter = stringMapMap.entrySet().iterator();
            while (pIter.hasNext()){
                HSSFRow row = sheet.createRow(rowNum);
                HSSFCell cell = row.createCell( 1);
                cell.setCellValue(FinanceExcelUtil.getActivityTypeIdName(activityTypeId));
                cell.setCellStyle(headStyle);

                Map.Entry<String, Map<String, Object>> next = pIter.next();
                Map<String, Object> value = next.getValue();
                String activityName = next.getKey();

                cell = row.createCell(2);
                cell.setCellValue(activityName);
                cell.setCellStyle(headStyle);

                int index = 3;
                sclist = new ArrayList<String>();
                scNumlist = new ArrayList<String>();
                List<Integer> payTypeIds = ActivityBeanUtils.OnlinePayNameEnum.getPayTypeIds();
                for (Integer payTypeId : payTypeIds){
                    cell = row.createCell(index);
                    cell.setCellValue(new BigDecimal(MapUtils.getDoubleValue(value,payTypeId.intValue() + "_price",0)).setScale(2,RoundingMode.HALF_UP).doubleValue());
                    cell.setCellStyle(headStyle);
                    sclist.add(CellReference.convertNumToColString(index));
                    index++;
                    cell = row.createCell(index);
                    cell.setCellValue(MapUtils.getIntValue(value,payTypeId.intValue() + "_num",0));
                    cell.setCellStyle(headStyle);
                    scNumlist.add(CellReference.convertNumToColString(index));
                    index++;
                }

                /*cell = row.createCell(3);
                cell.setCellValue(new BigDecimal(MapUtils.getDoubleValue(value,"weixinPrice",0)).setScale(2,RoundingMode.HALF_UP).doubleValue());
                cell.setCellStyle(headStyle);
                cell = row.createCell(4);
                cell.setCellValue(MapUtils.getIntValue(value,"weixinNum",0));
                cell.setCellStyle(headStyle);
                cell = row.createCell(5);
                cell.setCellValue(new BigDecimal(MapUtils.getDoubleValue(value,"chuzhiPrice",0)).setScale(2,RoundingMode.HALF_UP).doubleValue());
                cell.setCellStyle(headStyle);
                cell = row.createCell(6);
                cell.setCellValue(MapUtils.getIntValue(value,"chuzhiNum",0));
                cell.setCellStyle(headStyle);*/

                cell = row.createCell(index);
                StringBuffer scBuffer = new StringBuffer();
                for (String sc : sclist){
                    if(StringUtils.isEmpty(scBuffer.toString())){
                        scBuffer.append(sc + (rowNum+1));
                    }else{
                        scBuffer.append("+" + sc + (rowNum+1));
                    }
                }
                cell.setCellFormula(scBuffer.toString());
                cell.setCellStyle(headStyle);
                index++;
                cell = row.createCell(index);
                StringBuffer scNumBuffer = new StringBuffer();
                for (String sc : scNumlist){
                    if(StringUtils.isEmpty(scBuffer.toString())){
                        scNumBuffer.append(sc + (rowNum+1));
                    }else{
                        scNumBuffer.append("+" + sc + (rowNum+1));
                    }
                }
                cell.setCellFormula(scNumBuffer.toString());
                cell.setCellStyle(headStyle);
/*
                cell = row.createCell(5);
                cell.setCellValue(new BigDecimal(MapUtils.getDoubleValue(value,"price",0)).setScale(2,RoundingMode.HALF_UP).doubleValue());
                cell.setCellStyle(headStyle);
                cell = row.createCell(6);
                cell.setCellValue(MapUtils.getIntValue(value,"num",0));
                cell.setCellStyle(headStyle);
*/
                rowNum++;
            }
            if(stringMapMap.size()>0){
                sheet.addMergedRegion(new CellRangeAddress( rowMergedNum,rowMergedNum + stringMapMap.size()-1,1, 1));
            }
        }

        HSSFCellStyle hjStyle = wb.createCellStyle(); // 样式对象
        HSSFDataFormat df = wb.createDataFormat();
        hjStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        hjStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        hjStyle.setFillForegroundColor(HSSFColor.GREY_40_PERCENT.index);
        hjStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        hjStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        hjStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        hjStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);

        HSSFCellStyle numStyle = wb.createCellStyle(); // 样式对象
        numStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        numStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        numStyle.setFillForegroundColor(HSSFColor.GREY_40_PERCENT.index);
        numStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        numStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        numStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        numStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);


        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = row.createCell(1);
        cell.setCellValue("合计");
        cell.setCellStyle(hjStyle);
        cell = row.createCell(2);
        cell.setCellValue("");
        cell.setCellStyle(hjStyle);

        hjStyle.setDataFormat(df.getBuiltinFormat("#,##0.00"));
        int index = 3;
        List<Integer> payTypeIds = ActivityBeanUtils.OnlinePayNameEnum.getPayTypeIds();
        for (Integer payTypeId : payTypeIds){
            cell = row.createCell(index);
            String sc = CellReference.convertNumToColString(index);
            cell.setCellFormula("SUM(" + sc + rowStartIndex + ":"+sc+rowNum+")");
            cell.setCellStyle(hjStyle);
            index++;
            cell = row.createCell( index);
            String pc = CellReference.convertNumToColString(index);
            cell.setCellFormula("SUM(" + pc + rowStartIndex + ":"+pc+rowNum+")");
            cell.setCellStyle(numStyle);
            index++;
        }

        /*cell = row.createCell(3);
        String sc = CellReference.convertNumToColString(3);
        cell.setCellFormula("SUM(" + sc + rowStartIndex + ":"+sc+rowNum+")");
        cell.setCellStyle(hjStyle);
        cell = row.createCell( 4);
        String pc = CellReference.convertNumToColString(4);
        cell.setCellFormula("SUM(" + pc + rowStartIndex + ":"+pc+rowNum+")");
        cell.setCellStyle(numStyle);
        cell = row.createCell( 5);
        String pc5 = CellReference.convertNumToColString(5);
        cell.setCellFormula("SUM(" + pc5 + rowStartIndex + ":"+pc5+rowNum+")");
        cell.setCellStyle(hjStyle);
        cell = row.createCell( 6);
        String pc6 = CellReference.convertNumToColString(6);
        cell.setCellFormula("SUM(" + pc6 + rowStartIndex + ":"+pc6+rowNum+")");
        cell.setCellStyle(numStyle);*/
        cell = row.createCell( index);
        String pc7 = CellReference.convertNumToColString(index);
        cell.setCellFormula("SUM(" + pc7 + rowStartIndex + ":"+pc7+rowNum+")");
        cell.setCellStyle(hjStyle);
        index++;
        cell = row.createCell( index);
        String pc8 = CellReference.convertNumToColString(index);
        cell.setCellFormula("SUM(" + pc8 + rowStartIndex + ":"+pc8+rowNum+")");
        cell.setCellStyle(numStyle);
        return rowNum;
    }

    /**
     * 创建租赁物表格
     * @param wb
     * @param sheet
     * @param head
     */
    private int createRentStaticTable(int rowNum,HSSFWorkbook wb,HSSFSheet sheet,Map<String,Map<String, Object>> rentAndTimeOutOrders,String[] head){
        rowNum++;
        HSSFCellStyle headStyle = wb.createCellStyle(); // 样式对象
        HSSFFont font = wb.createFont();// 生成一个字体
        font.setColor(HSSFColor.BLACK.index);// HSSFColor.VIOLET.index // 字体颜色
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);// 字体增粗
        font.setFontHeightInPoints((short)16);
        headStyle.setFont(font);// 把字体应用到当前的样式
        headStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        headStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);//垂直居中
        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = row.createCell(1);
        cell.setCellStyle(headStyle);
        cell.setCellValue("租赁物销售统计");
        rowNum++;
        createProductPayStaticHead(wb,sheet,rowNum,head);
        rowNum++;
        rowNum=createRenStaticItem(wb,sheet,rowNum,rentAndTimeOutOrders);
        return rowNum;
    }

    /**
     * 创建租赁物支付方式表格
     * @param wb
     * @param sheet
     * @param head
     */
    private int createRentPayStaticTable(int rowNum,HSSFWorkbook wb,HSSFSheet sheet,Map<String,Map<String, Object>> rentListMap,String[] head){
        rowNum++;
        HSSFCellStyle headStyle = wb.createCellStyle(); // 样式对象
        HSSFFont font = wb.createFont();// 生成一个字体
        font.setColor(HSSFColor.BLACK.index);// HSSFColor.VIOLET.index // 字体颜色
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);// 字体增粗
        font.setFontHeightInPoints((short)16);
        headStyle.setFont(font);// 把字体应用到当前的样式
        headStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        headStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);//垂直居中
        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = row.createCell(1);
        cell.setCellStyle(headStyle);
        cell.setCellValue("租赁物支付方式统计");
        rowNum++;
        createProductPayStaticHead(wb,sheet,rowNum,head);
        rowNum++;
        rowNum=createRentPayStaticItem(wb,sheet,rowNum,rentListMap,false);

        return rowNum;
    }
    /**
     * 创建租赁物赔偿支付方式表格
     * @param wb
     * @param sheet
     * @param head
     */
    private int createRentIndemnityPayStaticTable(int rowNum,HSSFWorkbook wb,HSSFSheet sheet,Map<String,Map<String, Object>> rentIndemnityListMap,String[] head){
        rowNum++;
        HSSFCellStyle headStyle = wb.createCellStyle(); // 样式对象
        HSSFFont font = wb.createFont();// 生成一个字体
        font.setColor(HSSFColor.BLACK.index);// HSSFColor.VIOLET.index // 字体颜色
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);// 字体增粗
        font.setFontHeightInPoints((short)16);
        headStyle.setFont(font);// 把字体应用到当前的样式
        headStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        headStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);//垂直居中
        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = row.createCell(1);
        cell.setCellStyle(headStyle);
        cell.setCellValue("租赁物赔偿支付方式统计");
        rowNum++;
        createProductPayStaticHead(wb,sheet,rowNum,head);
        rowNum++;
        rowNum=createRentPayStaticItem(wb,sheet,rowNum,rentIndemnityListMap,true);
        return rowNum;
    }
    /**
     * 创建租赁物赔偿表格
     * @param wb
     * @param sheet
     * @param head
     */
    private int createRentIndemnityStaticTable(int rowNum,HSSFWorkbook wb,HSSFSheet sheet,Map<Integer,Map<String,Object>> indemnityProduct,String[] head){
        rowNum++;
        HSSFCellStyle headStyle = wb.createCellStyle(); // 样式对象
        HSSFFont font = wb.createFont();// 生成一个字体
        font.setColor(HSSFColor.BLACK.index);// HSSFColor.VIOLET.index // 字体颜色
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);// 字体增粗
        font.setFontHeightInPoints((short)16);
        headStyle.setFont(font);// 把字体应用到当前的样式
        headStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        headStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);//垂直居中
        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = row.createCell(1);
        cell.setCellStyle(headStyle);
        cell.setCellValue("租赁物赔偿统计");
        rowNum++;
        createProductPayStaticHead(wb,sheet,rowNum,head);
        rowNum++;
        rowNum=createRenIndemnityStaticItem(wb,sheet,rowNum,indemnityProduct);
        return rowNum;
    }

    /**
     * 创建租赁物支付项
     * @param wb
     * @param sheet
     * @param rowNum
     * @param rentListMap
     * @return
     */
    private int createRentPayStaticItem(HSSFWorkbook wb,HSSFSheet sheet,int rowNum,Map<String,Map<String, Object>> rentListMap,boolean isIndeminity){
        int rowStartIndex = rowNum;

        List<String> scNumlist = null;
        List<String> sclist = null;

        Iterator<String> iterator = rentListMap.keySet().iterator();
        while (iterator.hasNext()){
            String name = iterator.next();
            Map<String, Object> value = rentListMap.get(name);

            HSSFCellStyle headStyle = wb.createCellStyle(); // 样式对象
            headStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
            headStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
            headStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
            headStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
            headStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
            headStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);//垂直居中



            HSSFRow row = sheet.createRow(rowNum);
            HSSFCell cell = row.createCell( 1);
            cell.setCellValue(name);
            cell.setCellStyle(headStyle);

            int index = 2;
            sclist = new ArrayList<String>();
            scNumlist = new ArrayList<String>();
            List<Integer> payTypeIds = ActivityBeanUtils.OnlinePayNameEnum.getPayTypeIds();
            for (Integer payTypeId : payTypeIds) {
                cell = row.createCell(index);
                cell.setCellValue(new BigDecimal(MapUtils.getDoubleValue(value,payTypeId.intValue() + "_price",0)).setScale(2,RoundingMode.HALF_UP).doubleValue());
                cell.setCellStyle(headStyle);
                sclist.add(CellReference.convertNumToColString(index));
                index++;
                cell = row.createCell(index);
                cell.setCellValue(MapUtils.getIntValue(value,payTypeId.intValue() + "_num",0));
                cell.setCellStyle(headStyle);
                scNumlist.add(CellReference.convertNumToColString(index));
                index++;
            }

            /*cell = row.createCell(2);
            cell.setCellValue(new BigDecimal(MapUtils.getDoubleValue(value,"weixinPrice",0)).setScale(2,RoundingMode.HALF_UP).doubleValue());
            cell.setCellStyle(headStyle);
            cell = row.createCell(3);
            cell.setCellValue(MapUtils.getIntValue(value,"weixinNum",0));
            cell.setCellStyle(headStyle);

            if(isIndeminity){
               rowNum++;
               continue;
            }

            cell = row.createCell(4);
            cell.setCellValue(new BigDecimal(MapUtils.getDoubleValue(value,"chuzhiPrice",0)).setScale(2,RoundingMode.HALF_UP).doubleValue());
            cell.setCellStyle(headStyle);
            cell = row.createCell(5);
            cell.setCellValue(MapUtils.getIntValue(value,"chuzhiNum",0));
            cell.setCellStyle(headStyle);*/

            cell = row.createCell(index);
            StringBuffer scBuffer = new StringBuffer();
            for (String sc : sclist){
                if(StringUtils.isEmpty(scBuffer.toString())){
                    scBuffer.append(sc + (rowNum+1));
                }else{
                    scBuffer.append("+" + sc + (rowNum+1));
                }
            }
            cell.setCellFormula(scBuffer.toString());
            cell.setCellStyle(headStyle);
            index++;
            cell = row.createCell(index);
            StringBuffer scNumBuffer = new StringBuffer();
            for (String sc : scNumlist){
                if(StringUtils.isEmpty(scBuffer.toString())){
                    scNumBuffer.append(sc + (rowNum+1));
                }else{
                    scNumBuffer.append("+" + sc + (rowNum+1));
                }
            }
            cell.setCellFormula(scNumBuffer.toString());
            cell.setCellStyle(headStyle);

            rowNum++;
        }

        HSSFCellStyle hjStyle = wb.createCellStyle(); // 样式对象
        HSSFDataFormat df = wb.createDataFormat();
        hjStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        hjStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        hjStyle.setFillForegroundColor(HSSFColor.GREY_40_PERCENT.index);
        hjStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        hjStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        hjStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        hjStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);

        HSSFCellStyle numStyle = wb.createCellStyle(); // 样式对象
        numStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        numStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        numStyle.setFillForegroundColor(HSSFColor.GREY_40_PERCENT.index);
        numStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        numStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        numStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        numStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
        numStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);//垂直居中

        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = row.createCell(1);
        cell.setCellValue("合计");
        cell.setCellStyle(hjStyle);

        hjStyle.setDataFormat(df.getBuiltinFormat("#,##0.00"));

        int index = 2;
        int rowSumIndex = rowStartIndex + 1;
        List<Integer> payTypeIds = ActivityBeanUtils.OnlinePayNameEnum.getPayTypeIds();
        for (Integer payTypeId : payTypeIds) {
            cell = row.createCell(index);
            String sc = CellReference.convertNumToColString(index);
            cell.setCellFormula("SUM(" + sc + rowSumIndex + ":"+sc+rowNum+")");
            cell.setCellStyle(hjStyle);
            index++;
            cell = row.createCell( index);
            String pc = CellReference.convertNumToColString(index);
            cell.setCellFormula("SUM(" + pc + rowSumIndex + ":"+pc+rowNum+")");
            cell.setCellStyle(numStyle);
            index++;
        }

        /*cell = row.createCell(2);
        String sc = CellReference.convertNumToColString(2);
        cell.setCellFormula("SUM(" + sc + rowStartIndex + ":"+sc+rowNum+")");
        cell.setCellStyle(hjStyle);
        cell = row.createCell( 3);
        String pc = CellReference.convertNumToColString(3);
        cell.setCellFormula("SUM(" + pc + rowStartIndex + ":"+pc+rowNum+")");
        cell.setCellStyle(numStyle);*/

        // if(!isIndeminity){
            /*cell = row.createCell( 4);
            String pc4 = CellReference.convertNumToColString(4);
            cell.setCellFormula("SUM(" + pc4 + rowStartIndex + ":"+pc4+rowNum+")");
            cell.setCellStyle(hjStyle);
            cell = row.createCell( 5);
            String pc5 = CellReference.convertNumToColString(5);
            cell.setCellFormula("SUM(" + pc5 + rowStartIndex + ":"+pc5+rowNum+")");
            cell.setCellStyle(numStyle);*/

            cell = row.createCell( index);
            String pc6 = CellReference.convertNumToColString(index);
            cell.setCellFormula("SUM(" + pc6 + rowSumIndex + ":"+pc6+rowNum+")");
            cell.setCellStyle(hjStyle);
            index++;
            cell = row.createCell( index);
            String pc7 = CellReference.convertNumToColString(index);
            cell.setCellFormula("SUM(" + pc7 + rowSumIndex + ":"+pc7+rowNum+")");
            cell.setCellStyle(numStyle);
        // }


        return rowNum;
    }

    /**
     * 创建租赁物项
     * @param wb
     * @param sheet
     * @param rowNum
     * @param rentAndTimeOutOrders
     * @return
     */
    private int createRenStaticItem(HSSFWorkbook wb,HSSFSheet sheet,int rowNum,Map<String,Map<String, Object>> rentAndTimeOutOrders){
        int rowStartIndex = rowNum;

        Iterator<String> iterator = rentAndTimeOutOrders.keySet().iterator();
        while (iterator.hasNext()){
            String name = iterator.next();
            Map<String, Object> stringMapMap = rentAndTimeOutOrders.get(name);

            HSSFCellStyle headStyle = wb.createCellStyle(); // 样式对象
            headStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
            headStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
            headStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
            headStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
            headStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
            headStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);//垂直居中



            HSSFRow row = sheet.createRow(rowNum);
            HSSFCell cell = row.createCell( 1);
            cell.setCellValue(name);
            cell.setCellStyle(headStyle);

            cell = row.createCell(2);
            cell.setCellValue(MapUtils.getIntValue(stringMapMap,"totalNum",0));
            cell.setCellStyle(headStyle);
            cell = row.createCell(3);
            cell.setCellValue(new BigDecimal(MapUtils.getDoubleValue(stringMapMap,"totalMoney",0)).setScale(2,RoundingMode.HALF_UP).doubleValue());
            cell.setCellStyle(headStyle);
            cell = row.createCell(4);
            cell.setCellValue(MapUtils.getIntValue(stringMapMap,"timeOutTotalNum",0));
            cell.setCellStyle(headStyle);
            cell = row.createCell(5);
            cell.setCellValue(new BigDecimal(MapUtils.getDoubleValue(stringMapMap,"timeOutTotalMoney",0)).setScale(2,RoundingMode.HALF_UP).doubleValue());
            cell.setCellStyle(headStyle);
            cell = row.createCell(6);
            BigDecimal totalMoney=new BigDecimal(MapUtils.getDoubleValue(stringMapMap,"totalMoney",0)).setScale(2,RoundingMode.HALF_UP);
            BigDecimal timeOutTotalMoney=new BigDecimal(MapUtils.getDoubleValue(stringMapMap,"timeOutTotalMoney",0)).setScale(2,RoundingMode.HALF_UP);
            timeOutTotalMoney=timeOutTotalMoney.add(totalMoney);
            cell.setCellValue(timeOutTotalMoney.doubleValue());
            cell.setCellStyle(headStyle);
            rowNum++;
        }

        HSSFCellStyle hjStyle = wb.createCellStyle(); // 样式对象
        HSSFDataFormat df = wb.createDataFormat();
        hjStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        hjStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        hjStyle.setFillForegroundColor(HSSFColor.GREY_40_PERCENT.index);
        hjStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        hjStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        hjStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        hjStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);

        HSSFCellStyle numStyle = wb.createCellStyle(); // 样式对象
        numStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        numStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        numStyle.setFillForegroundColor(HSSFColor.GREY_40_PERCENT.index);
        numStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        numStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        numStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        numStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);


        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = row.createCell(1);
        cell.setCellValue("合计");
        cell.setCellStyle(hjStyle);

        hjStyle.setDataFormat(df.getBuiltinFormat("#,##0.00"));
        cell = row.createCell(2);
        String sc = CellReference.convertNumToColString(2);
        cell.setCellFormula("SUM(" + sc + rowStartIndex + ":"+sc+rowNum+")");
        cell.setCellStyle(numStyle);
        cell = row.createCell( 3);
        String pc = CellReference.convertNumToColString(3);
        cell.setCellFormula("SUM(" + pc + rowStartIndex + ":"+pc+rowNum+")");
        cell.setCellStyle(hjStyle);
        cell = row.createCell( 4);
        String pc5 = CellReference.convertNumToColString(4);
        cell.setCellFormula("SUM(" + pc5 + rowStartIndex + ":"+pc5+rowNum+")");
        cell.setCellStyle(numStyle);
        cell = row.createCell( 5);
        String pc6 = CellReference.convertNumToColString(5);
        cell.setCellFormula("SUM(" + pc6 + rowStartIndex + ":"+pc6+rowNum+")");
        cell.setCellStyle(hjStyle);
        cell = row.createCell( 6);
        String pc7 = CellReference.convertNumToColString(6);
        cell.setCellFormula("SUM(" + pc7 + rowStartIndex + ":"+pc7+rowNum+")");
        cell.setCellStyle(hjStyle);
        return rowNum;
    }

    /**
     * 创建租赁物赔偿项
     * @param wb
     * @param sheet
     * @param rowNum
     * @param indemnityProduct
     * @return
     */
    private int createRenIndemnityStaticItem(HSSFWorkbook wb,HSSFSheet sheet,int rowNum,Map<Integer,Map<String,Object>> indemnityProduct){
        int rowStartIndex = rowNum;

        Iterator<Map.Entry<Integer,Map<String,Object>>> iterator = indemnityProduct.entrySet().iterator();
        while(iterator.hasNext()){
            Map.Entry<Integer, Map<String,Object>> next = iterator.next();
            Integer key = next.getKey();
            Map<String,Object> map = (Map<String,Object>)next.getValue();
            sheet.addMergedRegion(new CellRangeAddress(rowStartIndex, rowNum, 1, 1));
            HSSFCellStyle headStyle = wb.createCellStyle(); // 样式对象
            headStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
            headStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
            headStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
            headStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
            headStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
            headStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);//垂直居中
            HSSFRow row = sheet.createRow(rowNum);
            HSSFCell cell = row.createCell(1);
            cell.setCellValue(MapUtils.getString(map,"name",""));
            cell.setCellStyle(headStyle);
            cell = row.createCell(2);
            cell.setCellValue(MapUtils.getIntValue(map,"num",0));
            cell.setCellStyle(headStyle);
            cell = row.createCell(3);
            cell.setCellValue(new BigDecimal(MapUtils.getDoubleValue(map,"money",0)).setScale(2,RoundingMode.HALF_UP).doubleValue());
            cell.setCellStyle(headStyle);
            rowNum++;
        }
        HSSFCellStyle hjStyle = wb.createCellStyle(); // 样式对象
        hjStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        hjStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        hjStyle.setFillForegroundColor(HSSFColor.GREY_40_PERCENT.index);
        hjStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        hjStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        hjStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        hjStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);

        HSSFCellStyle numStyle = wb.createCellStyle(); // 样式对象
        numStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        numStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        numStyle.setFillForegroundColor(HSSFColor.GREY_40_PERCENT.index);
        numStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        numStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        numStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        numStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);

        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = row.createCell(1);
        cell.setCellValue("合计");
        cell.setCellStyle(numStyle);
        cell = row.createCell(2);
        String sc1 = CellReference.convertNumToColString(2);
        cell.setCellFormula("SUM(" + sc1 + rowStartIndex + ":"+sc1+rowNum+")");
        cell.setCellStyle(numStyle);
        HSSFDataFormat df = wb.createDataFormat();
        hjStyle.setDataFormat(df.getBuiltinFormat("#,##0.00"));
        cell = row.createCell(3);
        String sc = CellReference.convertNumToColString(3);
        cell.setCellFormula("SUM(" + sc + rowStartIndex + ":"+sc+rowNum+")");
        cell.setCellStyle(hjStyle);
        return rowNum;
    }

    /**
     * 创建租赁物
     * @param wb
     * @param sheet
     * @param rowNum
     * @return
     */
    private int createRentItem(HSSFWorkbook wb,HSSFSheet sheet,int rowNum,List<Map<String, Object>> rentOrders,List<Map<String, Object>> rentTimeOutOrders,Map<String,Object> map){

        int rowStartIndex = rowNum;
        for(int i=0;i<rentOrders.size();i++){
            Map<String,Object> rentMap=rentOrders.get(i);

            HSSFCellStyle headStyle = wb.createCellStyle(); // 样式对象
            headStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
            headStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
            headStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
            headStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
            headStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
            headStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);//垂直居中
            HSSFRow row = sheet.createRow(rowNum);
            HSSFCell cell = row.createCell(1);
            cell.setCellValue("租赁物");
            cell.setCellStyle(headStyle);
            cell = row.createCell(2);
            cell.setCellValue(String.valueOf(rentMap.get("name")));
            cell.setCellStyle(headStyle);
            cell = row.createCell(3);
            cell.setCellValue(MapUtils.getIntValue(rentMap,"totalNum",0));
            cell.setCellStyle(headStyle);
            cell = row.createCell(4);
            cell.setCellValue(new BigDecimal(MapUtils.getDoubleValue(rentMap,"totalMoney",0)).setScale(2,RoundingMode.HALF_UP).doubleValue());
            cell.setCellStyle(headStyle);
            rowNum++;
        }
        if(rentOrders.size()>0){
            sheet.addMergedRegion(new CellRangeAddress(rowStartIndex, rowStartIndex-1+rentOrders.size(), 1, 1));
        }
        int rowStartIndext = rowNum;
        for(int i=0;i<rentTimeOutOrders.size();i++){
            Map<String,Object> rentTimeOutMap=rentTimeOutOrders.get(i);
            HSSFCellStyle headStyle = wb.createCellStyle(); // 样式对象
            headStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
            headStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
            headStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
            headStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
            headStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
            headStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);//垂直居中
            HSSFRow row = sheet.createRow(rowNum);
            HSSFCell cell = row.createCell(1);
            cell.setCellValue("超时租赁");
            cell.setCellStyle(headStyle);
            cell = row.createCell(2);
            cell.setCellValue(String.valueOf(rentTimeOutMap.get("name")));
            cell.setCellStyle(headStyle);
            cell = row.createCell(3);
            cell.setCellValue(MapUtils.getIntValue(rentTimeOutMap,"totalNum",0));
            cell.setCellStyle(headStyle);
            cell = row.createCell(4);
            cell.setCellValue(new BigDecimal(MapUtils.getDoubleValue(rentTimeOutMap,"totalMoney",0)).setScale(2,RoundingMode.HALF_UP).doubleValue());
            cell.setCellStyle(headStyle);
            rowNum++;
        }
        if(rentTimeOutOrders.size()>0){
            sheet.addMergedRegion(new CellRangeAddress(rowStartIndext, rowStartIndext-1+rentOrders.size(), 1, 1));
        }

        HSSFCellStyle hjStyle = wb.createCellStyle(); // 样式对象
        hjStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        hjStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        hjStyle.setFillForegroundColor(HSSFColor.GREY_40_PERCENT.index);
        hjStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        hjStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        hjStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        hjStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);

        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = row.createCell(1);
        cell.setCellValue("合计");
        cell.setCellStyle(hjStyle);
        cell = row.createCell(2);
        cell.setCellValue("");
        cell.setCellStyle(hjStyle);

        HSSFDataFormat df = wb.createDataFormat();
        hjStyle.setDataFormat(df.getBuiltinFormat("#,##0.00"));
        cell = row.createCell(3);
        String sc = CellReference.convertNumToColString(3);
        rowStartIndex=rowStartIndex+1;
        cell.setCellFormula("SUM(" + sc +  (rowStartIndex)+ ":"+sc+rowNum+")");
        cell.setCellStyle(hjStyle);
        cell = row.createCell(4);
        String pc = CellReference.convertNumToColString(4);
        cell.setCellFormula("SUM(" + pc + (rowStartIndex) + ":"+pc+rowNum+")");
        cell.setCellStyle(hjStyle);
        return rowNum;
    }

    /**
     * 创建表头
     * @param wb
     * @param sheet
     * @param rowNum
     * @param head
     */
    private void createProductPayStaticHead(HSSFWorkbook wb,HSSFSheet sheet,int rowNum,String[] head){
        HSSFCellStyle style = wb.createCellStyle(); // 样式对象
        HSSFFont font = wb.createFont();// 生成一个字体
        font.setColor(HSSFColor.BLACK.index);// HSSFColor.VIOLET.index // 字体颜色
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);// 字体增粗
        style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        style.setFillForegroundColor(HSSFColor.GREY_40_PERCENT.index);
        style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        style.setBorderRight(HSSFCellStyle.BORDER_THIN);
        style.setBorderTop(HSSFCellStyle.BORDER_THIN);
        style.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);//垂直居中
        style.setFont(font);// 把字体应用到当前的样式
        //style.setFillBackgroundColor(HSSFColor.BLACK.index);
        HSSFRow row = sheet.createRow(rowNum);
        int index = 1;
        for (String s : head) {
            HSSFCell cell = row.createCell(index);
            cell.setCellValue(s);
            cell.setCellStyle(style);
            sheet.setColumnWidth(index, 16 * 172);
            index++;
        }
    }

    private HSSFCellStyle getCellStyle(HSSFWorkbook wb){
        HSSFCellStyle headStyle = wb.createCellStyle(); // 样式对象
        headStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        headStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        headStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        headStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        headStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
        headStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);//垂直居中
        return headStyle;
    }

    //生成收银员支付方式汇总sheet
    private HSSFSheet createStasticsSheet(Date startTime, Date endTime,BigDecimal totalStoragePayPrice,Map<Integer,Map<String,Map<String,Object>>> saleProductStastic,
                                          List<Map<String, Object>> rentOrders,List<Map<String, Object>> rentTimeOutOrders,
                                          Map<String, Map<String,Object>> addServiceOrders,Map<String, Map<String,Object>> distributorStasticsMap,Map<Integer,Map<String,Object>> indemnityProduct,
                                          Map<Integer,Map<String,Object>> refundProduct,Map<Integer,Map<String,BigDecimal>> payTypeStastic,
                                          Map<Integer,Map<String,BigDecimal>> depositStastic,List<Map<String,Object>> allStastic,
                                          HSSFWorkbook wb){
        HSSFSheet sheet = wb.createSheet("财务汇总表");

        int rowNum = 2;

        HSSFCellStyle headStyle = wb.createCellStyle(); // 样式对象
        HSSFFont font = wb.createFont();// 生成一个字体
        font.setColor(HSSFColor.BLACK.index);// HSSFColor.VIOLET.index // 字体颜色
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);// 字体增粗
        font.setFontHeightInPoints((short)16);
        headStyle.setFont(font);// 把字体应用到当前的样式
        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = row.createCell(1);
        cell.setCellValue("时间：" + DateUtil.convertDateToString(startTime,"yyyy/MM/dd HH:mm:ss") + "-" + DateUtil.convertDateToString(endTime,"yyyy/MM/dd HH:mm:ss"));
        cell.setCellStyle(headStyle);

        rowNum++;
        rowNum++;

        headStyle = wb.createCellStyle(); // 样式对象
        font = wb.createFont();// 生成一个字体
        font.setColor(HSSFColor.BLACK.index);// HSSFColor.VIOLET.index // 字体颜色
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);// 字体增粗
        font.setFontHeightInPoints((short)16);
        headStyle.setFont(font);// 把字体应用到当前的样式
        headStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        headStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);//垂直居中
        row = sheet.createRow(rowNum);
        cell = row.createCell(1);
        cell.setCellStyle(headStyle);
        cell.setCellValue("线上销售汇总表格");

        Map<String,Object> map =new HashMap<String,Object>();
        map.put("zjMoney",BigDecimal.ZERO);

        int[] startIndex = new int[0];

        rowNum++;
        if(saleProductStastic!=null){
            FinanceExcelUtil.createProductHead(wb,sheet,rowNum);
            rowNum++;
            //产品
            rowNum = FinanceExcelUtil.createProdctItem(wb,sheet,rowNum,saleProductStastic);
            rowNum++;

            startIndex =ArrayUtils.add(startIndex,rowNum);

        }

        //租赁物//超时租赁物
        if((rentOrders!=null||rentTimeOutOrders!=null) && (rentOrders.size()>0 || rentTimeOutOrders.size()>0)){
            //租赁物
            rowNum = FinanceExcelUtil.createRentItem(wb,sheet,rowNum,rentOrders,rentTimeOutOrders);
            rowNum++;

            startIndex =ArrayUtils.add(startIndex,rowNum);
        }

        //增值服务
        if(addServiceOrders!=null && addServiceOrders.size()>0){
            //增值服务
            rowNum = createAddServiceItem(wb,sheet,rowNum,addServiceOrders);
            rowNum++;

            startIndex=ArrayUtils.add(startIndex,rowNum);
        }
        //分销渠道
        if(distributorStasticsMap!=null && distributorStasticsMap.size()>0){
            //分销渠道
            rowNum = createDistributorItem(wb,sheet,rowNum,distributorStasticsMap,map);
            rowNum++;

            startIndex=ArrayUtils.add(startIndex,rowNum);
        }

        if(startIndex.length>0){
            HSSFCellStyle hjStyle = wb.createCellStyle(); // 样式对象
            hjStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
            hjStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
            hjStyle.setFillForegroundColor(HSSFColor.GREY_40_PERCENT.index);
            hjStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
            hjStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
            hjStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
            hjStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);

            row = sheet.createRow(rowNum);
            cell = row.createCell(1);
            cell.setCellValue("收入总金额");
            cell.setCellStyle(hjStyle);
            cell = row.createCell(2);
            cell.setCellValue("");
            cell.setCellStyle(hjStyle);

            HSSFDataFormat df = wb.createDataFormat();
            hjStyle.setDataFormat(df.getBuiltinFormat("#,##0.00"));
            cell = row.createCell(3);
            String sc = CellReference.convertNumToColString(3);
            String sumStr = "";
            for (int index : startIndex) {
                if(sumStr.equals("")){
                    sumStr += sc + index ;
                }else{
                    sumStr += "+"+sc + index ;
                }
            }
            cell.setCellFormula(sumStr);
            cell.setCellStyle(hjStyle);
            cell = row.createCell(4);
            String pc = CellReference.convertNumToColString(4);
            sumStr = "";
            for (int index : startIndex) {
                if(sumStr.equals("")){
                    sumStr += pc + index ;
                }else{
                    sumStr += "+"+pc + index ;
                }
            }
            cell.setCellFormula(sumStr);
            cell.setCellStyle(hjStyle);
            rowNum++;
        }

        if(indemnityProduct!=null&&indemnityProduct.size()>0){
            //赔偿
            rowNum = FinanceExcelUtil.createDepositIndemnityItem(wb,sheet,rowNum,indemnityProduct);
            rowNum++;
        }

        rowNum++;
        rowNum++;
        //支付方式汇总 Map<Integer,Map<String,BigDecimal>> payTypeStastic
        row = sheet.createRow(rowNum);
        cell = row.createCell(1);
        cell.setCellStyle(headStyle);
        cell.setCellValue("支付方式汇总");
        rowNum++;
        if(payTypeStastic!=null){
            createPayTypeHead(wb,sheet,rowNum);
            rowNum++;
            //支付方式
            rowNum = createPayTypeStasticItem(wb,sheet,rowNum,payTypeStastic);
            rowNum++;
        }
        rowNum++;
        rowNum++;
        //押金汇总
        /*row = sheet.createRow(rowNum);
        cell = row.createCell(1);
        cell.setCellStyle(headStyle);
        cell.setCellValue("押金支付方式汇总");
        rowNum++;
        if(payTypeStastic!=null){
            FinanceExcelUtil.createDepositStasticHead(wb,sheet,rowNum);
            rowNum++;
            //押金支付方式
            rowNum = FinanceExcelUtil.createDepositStasticItem(wb,sheet,rowNum,depositStastic);
            rowNum++;
        }
        rowNum++;
        rowNum++;*/
        //汇总
        row = sheet.createRow(rowNum);
        cell = row.createCell(1);
        cell.setCellStyle(headStyle);
        cell.setCellValue("汇总(储值支付不计算在实收金额中)");
        rowNum++;
        if(payTypeStastic!=null){
            //汇总x
            rowNum = FinanceExcelUtil.createEndItem(wb,sheet,rowNum,allStastic);
            rowNum++;
        }

        return sheet;
    }

    /**
     * 支付方式汇总
     * @param wb
     * @param sheet
     * @param rowNum
     * @return
     */
    public static int createPayTypeStasticItem(HSSFWorkbook wb,HSSFSheet sheet,int rowNum,Map<Integer,Map<String,BigDecimal>> payTypeStastic){

        int rowStartIndex = rowNum;
        Iterator<Map.Entry<Integer,Map<String,BigDecimal>>> iterator = payTypeStastic.entrySet().iterator();
        HSSFCellStyle headStyle = FinanceExcelUtil.getCellStyle(wb);
        while(iterator.hasNext()){
            Map.Entry<Integer, Map<String,BigDecimal>> next = iterator.next();
            Integer key = next.getKey();
            Map<String,BigDecimal> map = (Map<String,BigDecimal>)next.getValue();

            HSSFDataFormat df = wb.createDataFormat();
            headStyle.setDataFormat(df.getBuiltinFormat("0.00"));

            HSSFRow row = sheet.createRow(rowNum);
            HSSFCell cell = row.createCell(1);
            if(key!=null){
                cell.setCellValue(FinanceExcelUtil.getPayName(key));
            }else{
                cell.setCellValue("");
            }
            cell.setCellStyle(headStyle);
            cell = row.createCell(2);
            BigDecimal srje=new BigDecimal(MapUtils.getDoubleValue(map,"0",0)).setScale(2,RoundingMode.HALF_UP);
            cell.setCellValue(srje.doubleValue());
            cell.setCellStyle(headStyle);
            rowNum++;
        }
        HSSFCellStyle hjStyle = FinanceExcelUtil.getFootCellStyle(wb);

        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = row.createCell(1);
        cell.setCellValue("合计");
        cell.setCellStyle(hjStyle);

        HSSFDataFormat df = wb.createDataFormat();
        hjStyle.setDataFormat(df.getBuiltinFormat("0.00"));
        cell = row.createCell(2);
        String qc = CellReference.convertNumToColString(2);
        cell.setCellFormula("SUM(" + qc + rowStartIndex + ":"+qc+rowNum+")");
        cell.setCellValue("");
        cell.setCellStyle(hjStyle);
        return rowNum;
    }

    /**
     * 支付方式统计表头
     * @param wb
     * @param sheet
     * @param rowNum
     */
    public static void createPayTypeHead(HSSFWorkbook wb,HSSFSheet sheet,int rowNum){
        HSSFCellStyle style = wb.createCellStyle(); // 样式对象
        HSSFFont font = wb.createFont();// 生成一个字体
        font.setColor(HSSFColor.BLACK.index);// HSSFColor.VIOLET.index // 字体颜色
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);// 字体增粗
        style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        style.setFillForegroundColor(HSSFColor.GREY_40_PERCENT.index);
        style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        style.setBorderRight(HSSFCellStyle.BORDER_THIN);
        style.setBorderTop(HSSFCellStyle.BORDER_THIN);
        style.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);//垂直居中
        style.setFont(font);// 把字体应用到当前的样式
        //style.setFillBackgroundColor(HSSFColor.BLACK.index);
        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = row.createCell(1);
        cell.setCellValue("支付方式");
        cell.setCellStyle(style);
        cell = row.createCell(2);
        cell.setCellValue("收入金额");
        cell.setCellStyle(style);
    }

    /**
     * 增值服务
     * @param wb
     * @param sheet
     * @param rowNum
     * @param distributorStasticsMap
     * @return
     */
    private int createAddServiceItem(HSSFWorkbook wb,HSSFSheet sheet,int rowNum,Map<String, Map<String,Object>> distributorStasticsMap){

        int rowStartIndex = rowNum;
        Iterator<Map.Entry<String, Map<String, Object>>> iterator = distributorStasticsMap.entrySet().iterator();

        sheet.addMergedRegion(new CellRangeAddress(rowNum, rowNum+distributorStasticsMap.size()-1, 1, 1));
        HSSFCellStyle headStyle = wb.createCellStyle(); // 样式对象
        headStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        headStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        headStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        headStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        headStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
        headStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);//垂直居中

        while(iterator.hasNext()){
            Map.Entry<String, Map<String, Object>> next = iterator.next();
            String name = next.getKey();
            Map<String, Object> value = next.getValue();

            HSSFRow row = sheet.createRow(rowNum);
            HSSFCell cell = row.createCell(1);
            cell.setCellValue("增值服务");
            cell.setCellStyle(headStyle);
            cell = row.createCell(2);
            cell.setCellValue(name);
            cell.setCellStyle(headStyle);
            cell = row.createCell(3);
            cell.setCellValue(MapUtils.getIntValue(value,"num",0));
            cell.setCellStyle(headStyle);
            cell = row.createCell(4);
            cell.setCellValue(new BigDecimal(MapUtils.getDoubleValue(value,"price",0)).setScale(2,RoundingMode.HALF_UP).doubleValue());
            cell.setCellStyle(headStyle);
            rowNum++;
        }
        HSSFCellStyle hjStyle = wb.createCellStyle(); // 样式对象
        hjStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        hjStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        hjStyle.setFillForegroundColor(HSSFColor.GREY_40_PERCENT.index);
        hjStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        hjStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        hjStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        hjStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);

        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = row.createCell(1);
        cell.setCellValue("合计");
        cell.setCellStyle(hjStyle);
        cell = row.createCell(2);
        cell.setCellValue("");
        cell.setCellStyle(hjStyle);

        HSSFDataFormat df = wb.createDataFormat();
        hjStyle.setDataFormat(df.getBuiltinFormat("#,##0.00"));
        cell = row.createCell(3);
        String sc = CellReference.convertNumToColString(3);
        cell.setCellFormula("SUM(" + sc + (rowStartIndex+1) + ":"+sc+rowNum+")");
        cell.setCellStyle(hjStyle);
        cell = row.createCell(4);
        String pc = CellReference.convertNumToColString(4);
        cell.setCellFormula("SUM(" + pc + (rowStartIndex+1) + ":"+pc+rowNum+")");
        cell.setCellStyle(hjStyle);
        return rowNum;
    }

    /**
     * 创建分销商
     * @param wb
     * @param sheet
     * @param rowNum
     * @return
     */
    private int createDistributorItem(HSSFWorkbook wb,HSSFSheet sheet,int rowNum,Map<String, Map<String,Object>> distributorStasticsMap,Map<String,Object> map){

        int rowStartIndex = rowNum;
        Iterator<Map.Entry<String, Map<String, Object>>> iterator = distributorStasticsMap.entrySet().iterator();

        sheet.addMergedRegion(new CellRangeAddress(rowNum, rowNum+distributorStasticsMap.size()-1, 1, 1));
        HSSFCellStyle headStyle = wb.createCellStyle(); // 样式对象
        headStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        headStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        headStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        headStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        headStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
        headStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);//垂直居中

        while(iterator.hasNext()){
            Map.Entry<String, Map<String, Object>> next = iterator.next();
            String name = next.getKey();
            Map<String, Object> value = next.getValue();

            HSSFRow row = sheet.createRow(rowNum);
            HSSFCell cell = row.createCell(1);
            cell.setCellValue("分销渠道");
            cell.setCellStyle(headStyle);
            cell = row.createCell(2);
            cell.setCellValue(name);
            cell.setCellStyle(headStyle);
            cell = row.createCell(3);
            cell.setCellValue(MapUtils.getIntValue(value,"num",0));
            cell.setCellStyle(headStyle);
            cell = row.createCell(4);
            cell.setCellValue(new BigDecimal(MapUtils.getDoubleValue(value,"price",0)).setScale(2,RoundingMode.HALF_UP).doubleValue());
            cell.setCellStyle(headStyle);
            rowNum++;
        }
        HSSFCellStyle hjStyle = wb.createCellStyle(); // 样式对象
        hjStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        hjStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        hjStyle.setFillForegroundColor(HSSFColor.GREY_40_PERCENT.index);
        hjStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        hjStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        hjStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        hjStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);

        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = row.createCell(1);
        cell.setCellValue("合计");
        cell.setCellStyle(hjStyle);
        cell = row.createCell(2);
        cell.setCellValue("");
        cell.setCellStyle(hjStyle);

        HSSFDataFormat df = wb.createDataFormat();
        hjStyle.setDataFormat(df.getBuiltinFormat("#,##0.00"));
        cell = row.createCell(3);
        String sc = CellReference.convertNumToColString(3);
        cell.setCellFormula("SUM(" + sc + (rowStartIndex+1) + ":"+sc+rowNum+")");
        cell.setCellStyle(hjStyle);
        cell = row.createCell(4);
        String pc = CellReference.convertNumToColString(4);
        cell.setCellFormula("SUM(" + pc + (rowStartIndex+1) + ":"+pc+rowNum+")");
        cell.setCellStyle(hjStyle);
        return rowNum;
    }
    /**
     * 创建赔偿
     * @param wb
     * @param sheet
     * @param rowNum
     * @return
     */
    private int createDepositIndemnityItem(HSSFWorkbook wb,HSSFSheet sheet,int rowNum,Map<Integer,Map<String,Object>> indemnityProduct){

        int rowStartIndex = rowNum;
        Iterator<Map.Entry<Integer,Map<String,Object>>> iterator = indemnityProduct.entrySet().iterator();
        while(iterator.hasNext()){
            Map.Entry<Integer, Map<String,Object>> next = iterator.next();
            Integer key = next.getKey();
            Map<String,Object> map = (Map<String,Object>)next.getValue();
            sheet.addMergedRegion(new CellRangeAddress(rowStartIndex, rowNum, 1, 1));
            HSSFCellStyle headStyle = wb.createCellStyle(); // 样式对象
            headStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
            headStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
            headStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
            headStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
            headStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
            headStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);//垂直居中
            HSSFRow row = sheet.createRow(rowNum);
            HSSFCell cell = row.createCell(1);
            cell.setCellValue("赔偿");
            cell.setCellStyle(headStyle);
            cell = row.createCell(2);
            cell.setCellValue(MapUtils.getString(map,"name",""));
            cell.setCellStyle(headStyle);
            cell = row.createCell(3);
            cell.setCellValue(MapUtils.getIntValue(map,"num",0));
            cell.setCellStyle(headStyle);
            cell = row.createCell(4);
            cell.setCellValue(new BigDecimal(MapUtils.getDoubleValue(map,"money",0)).setScale(2,RoundingMode.HALF_UP).doubleValue());
            cell.setCellStyle(headStyle);
            rowNum++;
        }
        HSSFCellStyle hjStyle = wb.createCellStyle(); // 样式对象
        hjStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        hjStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        hjStyle.setFillForegroundColor(HSSFColor.GREY_40_PERCENT.index);
        hjStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        hjStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        hjStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        hjStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);

        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = row.createCell(1);
        cell.setCellValue("合计");
        cell.setCellStyle(hjStyle);
        cell = row.createCell(2);
        cell.setCellValue("");
        cell.setCellStyle(hjStyle);

        HSSFDataFormat df = wb.createDataFormat();
        hjStyle.setDataFormat(df.getBuiltinFormat("#,##0.00"));
        cell = row.createCell(3);
        String sc = CellReference.convertNumToColString(3);
        cell.setCellFormula("SUM(" + sc + (rowStartIndex+1) + ":"+sc+rowNum+")");
        cell.setCellStyle(hjStyle);
        cell = row.createCell(4);
        String pc = CellReference.convertNumToColString(4);
        cell.setCellFormula("SUM(" + pc + (rowStartIndex+1) + ":"+pc+rowNum+")");
        cell.setCellStyle(hjStyle);
        return rowNum;
    }

    /**
     * 创建异常退款
     * @param wb
     * @param sheet
     * @param rowNum
     * @return
     */
    private int createRefundProductItem(HSSFWorkbook wb,HSSFSheet sheet,int rowNum,Map<Integer,Map<String,Object>> refundProduct){

        int rowStartIndex = rowNum;
        Iterator<Map.Entry<Integer,Map<String,Object>>> iterator = refundProduct.entrySet().iterator();
        while(iterator.hasNext()){
            Map.Entry<Integer, Map<String,Object>> next = iterator.next();
            Integer key = next.getKey();
            Map<String,Object> map = (Map<String,Object>)next.getValue();
            sheet.addMergedRegion(new CellRangeAddress(rowStartIndex, rowNum, 1, 1));
            HSSFCellStyle headStyle = wb.createCellStyle(); // 样式对象
            headStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
            headStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
            headStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
            headStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
            headStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
            headStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);//垂直居中
            HSSFRow row = sheet.createRow(rowNum);
            HSSFCell cell = row.createCell(1);
            String cellName=FinanceExcelUtil.getActivityTypeIdName(key);
            cell.setCellValue(cellName);
            cell.setCellStyle(headStyle);
            cell = row.createCell(2);
            cell.setCellValue(MapUtils.getString(map,"name",""));
            cell.setCellStyle(headStyle);
            cell = row.createCell(3);
            cell.setCellValue(MapUtils.getIntValue(map,"num",0));
            cell.setCellStyle(headStyle);
            cell = row.createCell(4);
            cell.setCellValue(new BigDecimal(MapUtils.getDoubleValue(map,"money",0)).setScale(2,RoundingMode.HALF_UP).doubleValue());
            cell.setCellStyle(headStyle);
            rowNum++;
        }
        HSSFCellStyle hjStyle = wb.createCellStyle(); // 样式对象
        hjStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        hjStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        hjStyle.setFillForegroundColor(HSSFColor.GREY_40_PERCENT.index);
        hjStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        hjStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        hjStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        hjStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);

        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = row.createCell(1);
        cell.setCellValue("合计");
        cell.setCellStyle(hjStyle);
        cell = row.createCell(2);
        cell.setCellValue("");
        cell.setCellStyle(hjStyle);

        HSSFDataFormat df = wb.createDataFormat();
        hjStyle.setDataFormat(df.getBuiltinFormat("#,##0.00"));
        cell = row.createCell(3);
        String sc = CellReference.convertNumToColString(3);
        cell.setCellFormula("SUM(" + sc + (rowStartIndex+1) + ":"+sc+rowNum+")");
        cell.setCellStyle(hjStyle);
        cell = row.createCell(4);
        String pc = CellReference.convertNumToColString(4);
        cell.setCellFormula("SUM(" + pc + (rowStartIndex+1) + ":"+pc+rowNum+")");
        cell.setCellStyle(hjStyle);
        return rowNum;
    }

    private void createrefundProductHead(HSSFWorkbook wb,HSSFSheet sheet,int rowNum){
        HSSFCellStyle style = wb.createCellStyle(); // 样式对象
        HSSFFont font = wb.createFont();// 生成一个字体
        font.setColor(HSSFColor.BLACK.index);// HSSFColor.VIOLET.index // 字体颜色
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);// 字体增粗
        style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        style.setFillForegroundColor(HSSFColor.GREY_40_PERCENT.index);
        style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        style.setBorderRight(HSSFCellStyle.BORDER_THIN);
        style.setBorderTop(HSSFCellStyle.BORDER_THIN);
        style.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);//垂直居中
        style.setFont(font);// 把字体应用到当前的样式
        //style.setFillBackgroundColor(HSSFColor.BLACK.index);
        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = row.createCell(1);
        cell.setCellValue("项目类型");
        cell.setCellStyle(style);
        cell = row.createCell(2);
        cell.setCellValue("名称");
        cell.setCellStyle(style);
        cell = row.createCell(3);
        cell.setCellValue("异常退款数量");
        cell.setCellStyle(style);
        cell = row.createCell(4);
        cell.setCellValue("异常退款金额");
        cell.setCellStyle(style);
    }


}