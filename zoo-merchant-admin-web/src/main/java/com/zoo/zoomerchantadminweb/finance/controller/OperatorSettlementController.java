package com.zoo.zoomerchantadminweb.finance.controller;

import com.zoo.activity.dao.model.Activity;
import com.zoo.activity.dao.model.LimitCardIndemnity;
import com.zoo.activity.dao.model.Orders;
import com.zoo.activity.dao.model.Organizer;
import com.zoo.activity.dao.nutz.pojo.CommonAccount;
import com.zoo.activity.service.LimitCardService;
import com.zoo.activity.service.OrdersService;
import com.zoo.activity.util.DateUtil;
import com.zoo.bean.util.ActivityBeanUtils;
import com.zoo.finance.dao.model.*;
import com.zoo.finance.service.*;
import com.zoo.web.bean.WebResult;
import com.zoo.zoomerchantadminweb.context.WebUpmsContext;
import com.zoo.zoomerchantadminweb.finance.controller.poi.util.FinanceExcelUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import jodd.util.collection.SortedArrayList;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.CellReference;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.hssf.util.Region;
import org.apache.poi.ss.usermodel.CellStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

@Api(value = "api/finance/operator", tags = {"【财务系统】操作员账务结算"}, description = "操作员账务结算")
@RestController
@RequestMapping("api/finance/operator")
public class OperatorSettlementController {

    private static final Logger LOGGER = LoggerFactory.getLogger(OperatorSettlementController.class);

    @Autowired
    private ZooOperatorLoginService zooOperatorLoginService;

    @Autowired
    private ZooOperatorOrderStattisticsService zooOperatorOrderStattisticsService;

    @Autowired
    private ZooOperatorDepositService zooOperatorDepositService;

    @Autowired
    private ZooBalanceDepositIndemnityService zooBalanceDepositIndemnityService;

    @Autowired
    private ZooOnAccountOrdersService zooOnAccountOrdersService;

    @Autowired
    private ZooEntertainOrdersService zooEntertainOrdersService;

    @Autowired
    private LimitCardService limitCardService;

    @Autowired
    private OrdersService ordersService;

    @ApiOperation(value = "「导出」导出收银结算订单信息",notes = "导出导出收银结算订单信息")
    @ApiImplicitParam(name = "zooOperatorLoginId", value = "登录日志ID", required = true, dataType = "int", paramType = "query")
    @RequestMapping(value = "/orders/operator/export", method = RequestMethod.GET)
    public void exportByZooOperatorLoginId(@RequestParam(value = "zooOperatorLoginId", required = true) Long zooOperatorLoginId,HttpServletRequest request, HttpServletResponse response) throws Exception {
        ZooOperatorLoginExample loginExample = new ZooOperatorLoginExample();
        loginExample.createCriteria().andIdEqualTo(zooOperatorLoginId);
        ZooOperatorLogin operatorLogin = zooOperatorLoginService.selectFirstByExample(loginExample);
        String startTime = DateUtil.convertDateToString(operatorLogin.getStartTime(),"yyyy-MM-dd HH:mm:ss");
        String endTime = DateUtil.convertDateToString(operatorLogin.getEndTime(),"yyyy-MM-dd HH:mm:ss");
        exportOrdersWithOperatorAndEpisodeInfo(startTime,endTime,null,null,null,operatorLogin.getOperatorId(),zooOperatorLoginId,false,request,response);
    }

    /**
     * 导出订单信息（按照操作员和产品分类进行导出）
     */
    @ApiOperation(value = "「导出」导出当天收银员订单信息",notes = "导出当天收银员订单信息")
    @RequestMapping(value = "/orders/day/export", method = RequestMethod.GET)
    public void exportToday(HttpServletRequest request, HttpServletResponse response) throws Exception {
        CommonAccount account = WebUpmsContext.getAccount(request);
        String now = DateUtil.convertDateToString(new Date(),"yyyy-MM-dd");
        String startTime = now + " 00:00:00";
        String endTime = now + " 23:59:59";
        exportOrdersWithOperatorAndEpisodeInfo(startTime,endTime,null,null,null,account.getCustomerId(),null,false,request,response);
    }

    /**
     * 导出订单信息（按照操作员和产品分类进行导出）
     */
    @ApiOperation(value = "「导出」导出订单信息",notes = "导出订单信息（按照操作员和产品分类进行导出")
    @RequestMapping(value = "/orders/export", method = RequestMethod.GET)
    public void exportOrdersWithOperatorAndEpisodeInfo(@RequestParam(value = "startTime", required = false) String startTime,
                                                       @RequestParam(value = "endTime", required = false) String endTime,
                                                       @RequestParam(value = "activityId", required = false) Integer activityId,
                                                       @RequestParam(value = "episodeId", required = false) Integer episodeId,
                                                       @RequestParam(value = "cashierName", required = false) String cashierName, // 操作员名称
                                                       @RequestParam(value = "cashierId", required = false) Integer cashierId,
                                                       @RequestParam(value = "zooOperatorLoginId", required = false) Long zooOperatorLoginId,
                                                       @RequestParam(value = "isAllCashier", required = false,defaultValue = "true") Boolean isAllCashier,
                                                       HttpServletRequest request, HttpServletResponse response) throws Exception {
        Organizer organizer = WebUpmsContext.getOrgnization(request);

        Date st = null;
        if(StringUtils.isNotEmpty(startTime)){
            if(startTime.length()==10){
                startTime = startTime + " 00:00:00";
            }
            st = DateUtil.convertStringToDate(startTime,"yyyy-MM-dd HH:mm:ss");
        }

        Date et = null;
        if(StringUtils.isNotEmpty(endTime)){
            if(endTime.length()==10){
                endTime = endTime + " 23:59:59";
            }
            et = DateUtil.convertStringToDate(endTime,"yyyy-MM-dd HH:mm:ss");
        }

        //退款数据
        //List<Map> refundslist = ordersService.selectRefundByOrgnizerAndCashierId(st, et, activityId, episodeId, cashierName, cashierId, organizer.getOrganizerId(),zooOperatorLoginId);

        //押金退款数据
        List<Map> depositRefundslist = ordersService.selectDepositRefundByOrgnizerAndCashierId(st, et, activityId, episodeId, cashierName, cashierId, organizer.getOrganizerId());

        //押金赔款
        ZooBalanceDepositIndemnityExample indemnityExample = new ZooBalanceDepositIndemnityExample();
        ZooBalanceDepositIndemnityExample.Criteria indemnityExampleCriteria = indemnityExample.createCriteria();
        indemnityExampleCriteria.andOrganizerIdEqualTo(organizer.getOrganizerId());
        // indemnityExampleCriteria.andPayTypeNotEqualTo(1);
        indemnityExampleCriteria.andPayTypeIn(ActivityBeanUtils.OfflinePayNameEnum.getPayTypeIds());
        if(st!=null){
            indemnityExampleCriteria.andIndemnityTimeGreaterThanOrEqualTo(st);
        }
        if(et !=null){
            indemnityExampleCriteria.andIndemnityTimeLessThan(et);
        }
        if(cashierName!=null){
            indemnityExampleCriteria.andOperatorNameLike("%"+cashierName+"%");
        }
        if(cashierId!=null){
            indemnityExampleCriteria.andOperatorIdEqualTo(cashierId);
        }
        if(zooOperatorLoginId!=null){
            indemnityExampleCriteria.andZooOperatorLoginIdEqualTo(zooOperatorLoginId);
        }
        List<ZooBalanceDepositIndemnity> depositIndemnityList = zooBalanceDepositIndemnityService.selectByExample(indemnityExample);

        //订单数据
        List<Orders> orderslist = ordersService.getOrdersByCashierAndPayTypeIds(st, et, activityId, episodeId, cashierName, cashierId, organizer.getOrganizerId(),zooOperatorLoginId, ActivityBeanUtils.OfflinePayNameEnum.getPayTypeIds());

        //异常退款订单
        List<Orders> abnormalOrderslist = ordersService.getAbnormalOrdersByCashier(st, et, activityId, episodeId, cashierName, cashierId, organizer.getOrganizerId(),zooOperatorLoginId);

        //租赁物
        List<Map<String, Object>> rentOrders = ordersService.getStasticsRentByStartEndTime(st, et, organizer.getOrganizerId(), null,null,1);
        //超时租赁物
        List<Map<String, Object>> rentTimeOutOrders = ordersService.getStasticsRentByStartEndTime(st, et,organizer.getOrganizerId(),4,null,1);

        //增值服务
        //List<Map<String, Object>> addServiceOrders = ordersService.getStasticsAddServiceByTime(st, et, organizer.getOrganizerId(),1);

        //操作人挂账统计
        List<Map> hungUpCashierList = zooOnAccountOrdersService.hungUpCashierStastic(st, et, organizer.getOrganizerId(), null);

        //招待统计
        List<Map> entertainCashierList = zooEntertainOrdersService.entertainCashierStastic(st, et, organizer.getOrganizerId(), null);

        //时限卡赔偿
        List<LimitCardIndemnity> limitCardIndemnityList = limitCardService.getLimitCardIndemnityList(st, et, zooOperatorLoginId);

        response.reset();
        response.setContentType("application/x-excel");
        String agent = request.getHeader("User-Agent");
        String outfile = "收银员报表";
        if(StringUtils.isNotEmpty(startTime)){
            outfile += "_" + startTime.replace("-","").substring(0,8);
        }
        if(StringUtils.isNotEmpty(endTime)){
            outfile += "_" + endTime.replace("-","").substring(0,8);
        }
        String fileName = encodeDownloadFilename(outfile,agent);
        response.setHeader("Content-disposition", "attachment;filename=" + fileName + ".xls");

        ServletOutputStream outputStream = response.getOutputStream();
        createHSSFWorkbook(startTime,endTime,isAllCashier,orderslist,abnormalOrderslist,depositRefundslist,depositIndemnityList,outputStream,
                rentOrders,rentTimeOutOrders,hungUpCashierList,
                entertainCashierList,limitCardIndemnityList);
        outputStream.close();
    }

    public static void main(String[] args) {
        String s = "2018-10-11 00:00:00";
        System.out.println(s.replace("-","").substring(0,8));
    }

    public static String encodeDownloadFilename(String filename, String agent)
            throws IOException {
        /*if (agent.contains("Firefox")) { // 火狐浏览器
            filename = "=?UTF-8?B?"
                    + new BASE64Encoder().encode(filename.getBytes("utf-8"))
                    + "?=";
            filename = filename.replaceAll("\r\n", "");
        } else { // IE及其他浏览器
            filename = URLEncoder.encode(filename, "utf-8");
            filename = filename.replace("+"," ");
        }*/
        filename = new String(filename.getBytes("UTF-8"), "iso-8859-1");
        return filename;
    }

    private void createHSSFWorkbook(String startTime,String endTime,Boolean isAllCashier,List<Orders> orderslist,List<Orders> abnormalOrderslist,
                                    List<Map> depositRefundslist,List<ZooBalanceDepositIndemnity> depositIndemnityList,
                                    OutputStream out,List<Map<String, Object>> rentOrders,List<Map<String, Object>> rentTimeOutOrders,
                                    List<Map> hungUpCashierList,List<Map> entertainCashierList,
                                    List<LimitCardIndemnity> limitCardIndemnityList) throws IOException {

        //收银员订单总金额
        Map<String,BigDecimal> cashOrdersMoney = new HashMap<String,BigDecimal>();
        //押金收入
        Map<String,BigDecimal> cashDepositMoney = new HashMap<String,BigDecimal>();
        //押金支出
        Map<String,BigDecimal> cashOutDepositMoney = new HashMap<String,BigDecimal>();
        //赔偿金额
        Map<String,BigDecimal> indemnityMoney = new HashMap<String,BigDecimal>();
        //异常退款金额
        Map<String,BigDecimal> abnormalOrdersMoney = new HashMap<String,BigDecimal>();
        Map<String,Integer> abnormalOrdersNum = new HashMap<String,Integer>();
        //产品数量
        Map<String,Integer> productNum = new HashMap<String,Integer>();
        //储值金额
        BigDecimal totalStoragePayPrice = BigDecimal.ZERO;
        Map<String,BigDecimal> totalStorageMoney = new HashMap<String,BigDecimal>();

        //押金收入
        Map<String,Map<Integer,BigDecimal>> cashierInPayDepositMoney = new HashMap<String,Map<Integer,BigDecimal>>();
        //押金支出
        Map<String,Map<Integer,BigDecimal>> cashierOutPayDepositMoney = new HashMap<String,Map<Integer,BigDecimal>>();
        //订单金额
        Map<String,Map<Integer,BigDecimal>> cashierOrderMoney = new HashMap<String,Map<Integer,BigDecimal>>();

        //order product 个人产品统计
        Map<String,Object> cashierOrderProductStastics = new HashMap<String,Object>();

        //时限卡换卡
        Map<String,BigDecimal> cashlimitCardIndemnityMoney = new HashMap<String,BigDecimal>();
        Map<String,List<LimitCardIndemnity>> limitCardIndemnitsta = new HashMap<String,List<LimitCardIndemnity>>();
        if(limitCardIndemnityList != null){
            int size = limitCardIndemnityList.size();
            for (int i = 0 ; i < size ; i++) {
                LimitCardIndemnity limit = limitCardIndemnityList.get(i);
                String operator_name = limit.getOperatorName();
                List<LimitCardIndemnity> ilist = limitCardIndemnitsta.get(operator_name);
                if(ilist == null){
                    ilist = new ArrayList<LimitCardIndemnity>();
                    limitCardIndemnitsta.put(operator_name,ilist);
                }
                ilist.add(limit);

                //金额
                BigDecimal dmoney = cashlimitCardIndemnityMoney.get(operator_name);
                if(dmoney == null){
                    dmoney = BigDecimal.ZERO;
                }
                cashlimitCardIndemnityMoney.put(operator_name,dmoney.add(limit.getAmount()));
            }
        }

        //挂账统计
        Map<String,List<Map>> hungUpsta = new HashMap<String,List<Map>>();
        if(hungUpCashierList != null){
            int size = hungUpCashierList.size();
            for (int i = 0 ; i < size ; i++) {
                Map map = hungUpCashierList.get(i);
                String operator_name = MapUtils.getString(map, "operator_name");
                List<Map> hungUplist = hungUpsta.get(operator_name);
                if(hungUplist == null){
                    hungUplist = new ArrayList<Map>();
                    hungUpsta.put(operator_name,hungUplist);
                }
                hungUplist.add(map);
            }

        }

        //招待统计
        Map<String,List<Map>> entertainsta = new HashMap<String,List<Map>>();
        if(entertainCashierList != null){
            for (Map map : entertainCashierList) {
                String operator_name = MapUtils.getString(map, "operator_name");
                List<Map> entertainlist = entertainsta.get(operator_name);
                if(entertainlist == null){
                    entertainlist = new ArrayList<Map>();
                    entertainsta.put(operator_name,entertainlist);
                }
                entertainlist.add(map);
            }
        }

        //支付方式
        Set<Integer> paySet = new HashSet<Integer>();

        //收银员
        Set<String> cashier = new HashSet<String>();

        //租赁物
        Map<String,List<Map<String, Object>>> rentOrdersMap = new HashMap<String,List<Map<String, Object>>>();
        if(rentOrders!=null){
            for (Map<String, Object> rentOrder : rentOrders) {
                String cashierName = MapUtils.getString(rentOrder, "cashier_name");
                List<Map<String, Object>> list = rentOrdersMap.get(cashierName);
                if(list == null){
                    list = new ArrayList<Map<String, Object>>();
                    rentOrdersMap.put(cashierName,list);
                }
                list.add(rentOrder);
            }
        }

        //超时租赁物
        Map<String,List<Map<String, Object>>> rentTimeOutOrdersMap = new HashMap<String,List<Map<String, Object>>>();
        if(rentOrders!=null){
            for (Map<String, Object> rentOrder : rentTimeOutOrders) {
                String cashierName = MapUtils.getString(rentOrder, "cashier_name");
                List<Map<String, Object>> list = rentTimeOutOrdersMap.get(cashierName);
                if(list == null){
                    list = new ArrayList<Map<String, Object>>();
                    rentTimeOutOrdersMap.put(cashierName,list);
                }
                list.add(rentOrder);
            }

        }

        //异常退款订单
        Map<String,List<Orders>> abnormalOrdersCashierMap = new HashMap<String,List<Orders>>();
        if(abnormalOrderslist!=null){
            for (Orders orders : abnormalOrderslist){
                if(orders.getExtAbnormalRefundMoney()==null){
                    continue;
                }
                if(orders.getExtAbnormalCashierName()==null){
                    continue;
                }
                cashier.add(orders.getExtAbnormalCashierName());
                if(orders.getParentId()==null||orders.getParentId().intValue()==0){
                    BigDecimal payBigDecimal = abnormalOrdersMoney.get(orders.getExtAbnormalCashierName());
                    if(payBigDecimal == null){
                        payBigDecimal =  BigDecimal.ZERO;
                    }
                    abnormalOrdersMoney.put(orders.getExtAbnormalCashierName(),payBigDecimal.add(orders.getExtAbnormalRefundMoney()).setScale(2,RoundingMode.HALF_UP));

                    List<Orders> list = abnormalOrdersCashierMap.get(orders.getExtAbnormalCashierName());
                    if(list==null){
                        list = new ArrayList<Orders>();
                        abnormalOrdersCashierMap.put(orders.getExtAbnormalCashierName(),list);
                    }
                    list.add(orders);

                    //数量统计
                    Integer num = MapUtils.getIntValue(abnormalOrdersNum,orders.getExtAbnormalCashierName(),0);
                    num = num + 1;
                    abnormalOrdersNum.put(orders.getExtAbnormalCashierName(),num);
                }
            }
        }


        Map<String,List<Orders>> parentCashierMap = new HashMap<String,List<Orders>>();
        Map<String,List<Orders>> childCashierMap = new HashMap<String,List<Orders>>();


        //增值服务
        Map<String,Map<String,Map<String, Object>>> addServiceOrders = new HashMap<String,Map<String,Map<String, Object>>>();

        if(orderslist!=null){

            for (Orders orders : orderslist){

                String cashierName = orders.getCashierName();

                if(cashierName==null){
                    continue;
                }
                cashier.add(cashierName);

                paySet.add(orders.getPayTypeId());

                //增值服务
                if(orders.getEquipmentId()==null && orders.getParentId()!=null && orders.getIncrement().intValue()==1){
                    Map<String,Map<String, Object>> maps = addServiceOrders.get(cashierName);
                    if(maps == null){
                        maps = new HashMap<String,Map<String, Object>>();
                        addServiceOrders.put(cashierName,maps);
                    }

                    String chargeName = orders.getChargeName();
                    Map<String, Object> item = maps.get(chargeName);
                    if(item == null){
                        item = new HashMap<String, Object>();
                        maps.put(chargeName,item);
                    }

                    item.put("charge_name",chargeName);

                    int totalNum = MapUtils.getIntValue(item, "totalNum", 0);
                    item.put("totalNum",totalNum+orders.getChargeNum());

                    double totalMoney = MapUtils.getDoubleValue(item, "totalMoney", 0);
                    item.put("totalMoney",new BigDecimal(totalMoney+orders.getChargeNum()*orders.getChargePrice().doubleValue()).setScale(2,RoundingMode.HALF_UP));

                }

                if(orders.getParentId()!=null){
                    //个人产品统计
                    cashierStastics(orders,cashierOrderProductStastics);

                    Map<Integer, BigDecimal> cashierMoneyMap = cashierOrderMoney.get(orders.getCashierName());
                    if(cashierMoneyMap == null){
                        cashierMoneyMap = new HashMap<Integer, BigDecimal>();
                        cashierOrderMoney.put(orders.getCashierName(),cashierMoneyMap);
                    }
                    BigDecimal money = cashierMoneyMap.get(orders.getPayTypeId());
                    if(money == null){
                        money = BigDecimal.ZERO;
                    }
                    BigDecimal deposit = orders.getDeposit();
                    if(deposit == null){
                        deposit = BigDecimal.ZERO;
                    }
                    cashierMoneyMap.put(orders.getPayTypeId(),money.add(orders.getTotalPrice()).add(deposit));
                }

                //按支付方式（押金收入）
                if(orders.getParentId()==null||orders.getParentId().intValue()==0){
                    //押金
                    Map<Integer, BigDecimal> payBigDecimal = cashierInPayDepositMoney.get(orders.getCashierName());
                    if(payBigDecimal == null){
                        payBigDecimal = new HashMap<Integer, BigDecimal>();
                        cashierInPayDepositMoney.put(orders.getCashierName(),payBigDecimal);
                    }
                    BigDecimal deposit = orders.getDeposit();
                    if(deposit == null){
                        deposit = BigDecimal.ZERO;
                    }
                    BigDecimal payPrice = payBigDecimal.get(orders.getPayTypeId());
                    if(payPrice == null){
                        payPrice =  BigDecimal.ZERO;
                    }
                    payBigDecimal.put(orders.getPayTypeId(),payPrice.add(deposit).setScale(2,RoundingMode.HALF_UP));

                    //押金总金额
                    BigDecimal depositMoney = cashDepositMoney.get(orders.getCashierName());
                    if(depositMoney==null){
                        depositMoney = BigDecimal.ZERO;
                    }
                    cashDepositMoney.put(orders.getCashierName(),depositMoney.add(deposit).setScale(2,RoundingMode.HALF_UP));

                    //订单总额
                    BigDecimal totalMoney = cashOrdersMoney.get(orders.getCashierName());
                    if(totalMoney==null){
                        totalMoney = BigDecimal.ZERO;
                    }
                    cashOrdersMoney.put(orders.getCashierName(),totalMoney.add(orders.getTotalPrice()));
                }

                //1、按收银员进行分类，并区分所有父订单
                List<Orders> list = null;
                if(orders.getParentId()==null || orders.getParentId().intValue()==0){
                    list = parentCashierMap.get(orders.getCashierName());
                    if(list==null){
                        list = new ArrayList<Orders>();
                        parentCashierMap.put(orders.getCashierName(),list);
                    }
                }else{
                    list = childCashierMap.get(orders.getCashierName());
                    if(list==null){
                        list = new ArrayList<Orders>();
                        childCashierMap.put(orders.getCashierName(),list);
                    }
                }
                list.add(orders);

                //2、每个收银员订单和押金总金额计算，产品总数统计
                /*if(orders.getParentId()==null || orders.getParentId().intValue()==0){
                    //订单总额
                    BigDecimal totalMoney = cashOrdersMoney.get(orders.getCashierName());
                    if(totalMoney==null){
                        totalMoney = BigDecimal.ZERO;
                    }
                    cashOrdersMoney.put(orders.getCashierName(),totalMoney.add(orders.getTotalPrice()));

                    //押金总额
                    BigDecimal depositMoney = cashDepositMoney.get(orders.getCashierName());
                    if(depositMoney==null){
                        depositMoney = BigDecimal.ZERO;
                    }
                    BigDecimal orderDeposit = orders.getDeposit();
                    if(orderDeposit==null){
                        orderDeposit = BigDecimal.ZERO;
                    }
                    cashDepositMoney.put(orders.getCashierName(),depositMoney.add(orderDeposit));
                }*/
                if(orders.getParentId()!=null){
                    //产品数量
                    Integer num = productNum.get(orders.getCashierName());
                    if(num==null){
                        num = 0;
                    }
                    productNum.put(orders.getCashierName(),num.intValue()+orders.getChargeNum().intValue());

                    //储值支付累计金额
                    if(orders.getPayTypeId().intValue()==8 || orders.getPayTypeId().intValue()==7){
                        totalStoragePayPrice = totalStoragePayPrice.add(orders.getTotalPrice());

                        BigDecimal storagePrice = totalStorageMoney.get(orders.getCashierName());
                        if(storagePrice==null){
                            storagePrice = BigDecimal.ZERO;
                        }
                        totalStorageMoney.put(orders.getCashierName(),storagePrice.add(orders.getTotalPrice()));
                    }
                }

            }
        }

        //押金退款
        Map<String,List<Map>> depositRefundsMap = new HashMap<String,List<Map>>();

        //3、处理押金退款数据
        if(depositRefundslist!=null){
            for (Map refunds : depositRefundslist){

                String name = MapUtils.getString(refunds,"realname");

                if(name==null){
                    name = MapUtils.getString(refunds,"nickname");
                }

                if(name == null){
                    continue;
                }
                cashier.add(name);

                List<Map> l = depositRefundsMap.get(name);
                if(l == null){
                    l = new ArrayList<Map>();
                    depositRefundsMap.put(name,l);
                }
                l.add(refunds);

                //押金退款总额
                BigDecimal depositRefundsMoney = cashOutDepositMoney.get(name);
                if(depositRefundsMoney==null){
                    depositRefundsMoney = BigDecimal.ZERO;
                }
                BigDecimal refundAmount = new BigDecimal(MapUtils.getDoubleValue(refunds,"refund_amount",0));
                cashOutDepositMoney.put(name,depositRefundsMoney.add(refundAmount));

                //支付方式
                Integer pay_type_id = MapUtils.getInteger(refunds, "pay_type_id");
                paySet.add(pay_type_id);

                //押金支出
                Map<Integer, BigDecimal> payBigDecimal = cashierOutPayDepositMoney.get(name);
                if(payBigDecimal == null){
                    payBigDecimal = new HashMap<Integer, BigDecimal>();
                    cashierOutPayDepositMoney.put(name,payBigDecimal);
                }
                BigDecimal payPrice = payBigDecimal.get(pay_type_id);
                if(payPrice == null){
                    payPrice = BigDecimal.ZERO;
                }
                payBigDecimal.put(pay_type_id,payPrice.add(refundAmount));
            }
        }

        //赔款
        Map<String,List<ZooBalanceDepositIndemnity>> depositIndemnityMap = new HashMap<String,List<ZooBalanceDepositIndemnity>>();
        if(depositIndemnityList!=null){
            for (ZooBalanceDepositIndemnity depositIndemnity : depositIndemnityList) {

                String operatorName = depositIndemnity.getOperatorName();

                cashier.add(operatorName);

                BigDecimal bigDecimal = indemnityMoney.get(operatorName);
                if(bigDecimal==null){
                    bigDecimal = BigDecimal.ZERO;
                }
                indemnityMoney.put(operatorName,bigDecimal.add(depositIndemnity.getIndemnity()));

                List<ZooBalanceDepositIndemnity> l = depositIndemnityMap.get(depositIndemnity.getOperatorName());
                if(l == null){
                    l = new ArrayList<ZooBalanceDepositIndemnity>();
                    depositIndemnityMap.put(depositIndemnity.getOperatorName(),l);
                }
                l.add(depositIndemnity);
            }
        }

        HSSFWorkbook wb = new HSSFWorkbook();
        //生成收银员支付方式汇总sheet
        if(isAllCashier){
            createCashierPayStasticsSheet(totalStoragePayPrice,startTime,endTime,wb,orderslist,cashierInPayDepositMoney,cashierOutPayDepositMoney,paySet,cashier,abnormalOrdersMoney,abnormalOrdersNum);
        }

        Iterator<String> iterator = cashier.iterator();
        while (iterator.hasNext()){
            String cashierName = iterator.next();
            if(StringUtils.isBlank(cashierName)){
                continue;
            }
            List<Orders> childOrders = childCashierMap.get(cashierName);
            List<Orders> parentOrders = parentCashierMap.get(cashierName);
            //押金退款数据
            List<Map> depositRefunds = depositRefundsMap.get(cashierName);
            //赔款
            List<ZooBalanceDepositIndemnity> depositIndemnities = depositIndemnityMap.get(cashierName);
            BigDecimal orderTotalMoney = cashOrdersMoney.get(cashierName);
            BigDecimal depositInMoney = cashDepositMoney.get(cashierName);
            BigDecimal depositOutMoney =cashOutDepositMoney.get(cashierName);
            BigDecimal abnormalMoney =abnormalOrdersMoney.get(cashierName);
            int pNum = MapUtils.getIntValue(productNum,cashierName,0);

            //异常退款订单
            List<Orders> abnormalOrders = abnormalOrdersCashierMap.get(cashierName);

            BigDecimal storageMoney = totalStorageMoney.get(cashierName);

            //时限卡
            List<LimitCardIndemnity> limitCardIndemnities = limitCardIndemnitsta.get(cashierName);
            BigDecimal limitCardIndemnityMoney = cashlimitCardIndemnityMoney.get(cashierName);

            createOrderDetailSheet(wb,cashierName,parentOrders,childOrders,abnormalOrders,orderTotalMoney,depositInMoney,depositOutMoney,pNum,depositRefunds,depositIndemnities,limitCardIndemnities,limitCardIndemnityMoney);
            //createStasticsSheet(wb,cashierName,parentOrders,childOrders,orderTotalMoney,depositInMoney,depositOutMoney,pNum,depositRefunds,depositIndemnities,abnormalMoney,storageMoney);

            //个人统计
            Map<Integer,Map<String,Map<String,Object>>> cashierOrderProductItem = (Map)cashierOrderProductStastics.get(cashierName);
            List<Map<String, Object>> rentOrderslist = rentOrdersMap.get(cashierName);
            List<Map<String, Object>> rentTimeOutOrderslist = rentTimeOutOrdersMap.get(cashierName);
            Map<String,Map<String, Object>> addServiceCashieMap = addServiceOrders.get(cashierName);
            List<Map<String, Object>> addServiceCashielist = null;
            if(addServiceCashieMap!=null){
                addServiceCashielist = new ArrayList<Map<String, Object>>(addServiceCashieMap.values());
            }else{
                addServiceCashielist = new ArrayList<Map<String, Object>>();
            }

            //押金收入
            Map<Integer, BigDecimal> cashierInPayDepositMap = cashierInPayDepositMoney.get(cashierName);
            Map<Integer, BigDecimal> cashierOutPayDepositMap = cashierOutPayDepositMoney.get(cashierName);

            //订单支付方式汇总
            Map<Integer, BigDecimal> cashierMoneyMap = cashierOrderMoney.get(cashierName);

            //押金收入汇总
            Map<Integer,Map<String,BigDecimal>> depositStastic = new HashMap<Integer, Map<String, BigDecimal>>();

            for (Integer pay : paySet) {
                Map<String,BigDecimal> depositPayMap = new HashMap<String,BigDecimal>();
                //押金收入
                if(cashierInPayDepositMap!=null){
                    depositPayMap.put("0",cashierInPayDepositMap.get(pay));
                }
                //押金支出
                if(cashierOutPayDepositMap!=null){
                    depositPayMap.put("1",cashierOutPayDepositMap.get(pay));
                }
                depositStastic.put(pay,depositPayMap);
            }

            //订单支付方式金额汇总
            Map<Integer,Map<String,BigDecimal>> orderPayStastic = new HashMap<Integer, Map<String, BigDecimal>>();

            for (Integer pay : paySet) {
                Map<String,BigDecimal>  orderPayMap = new HashMap<String,BigDecimal>();
                //收入
                if(cashierMoneyMap!=null){
                    orderPayMap.put("0",cashierMoneyMap.get(pay));
                }
                //支出
                if(cashierOutPayDepositMap!=null){
                    orderPayMap.put("1",cashierOutPayDepositMap.get(pay));
                }
                orderPayStastic.put(pay,orderPayMap);
            }

            //赔偿
            BigDecimal indemnityTotalMoney = indemnityMoney.get(cashierName);

            //挂账
            List<Map> hungUpcashierlist = hungUpsta.get(cashierName);
            List<Map> entertainlist = entertainsta.get(cashierName);


            createCashierStasticsSheet(wb,cashierName,cashierOrderProductItem,rentOrderslist,rentTimeOutOrderslist,addServiceCashielist,
                    depositIndemnities,depositStastic,orderPayStastic,orderTotalMoney,depositInMoney,depositOutMoney,abnormalMoney,storageMoney,
                    indemnityTotalMoney,hungUpcashierlist,entertainlist,limitCardIndemnities,limitCardIndemnityMoney);
        }

        if(cashier.size()==0){
            HSSFSheet sheet = wb.createSheet("收银员报表");
            int rowNum = 0;

            HSSFCellStyle headStyle = wb.createCellStyle(); // 样式对象
            HSSFFont font = wb.createFont();// 生成一个字体
            font.setColor(HSSFColor.BLACK.index);// HSSFColor.VIOLET.index // 字体颜色
            font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);// 字体增粗
            font.setFontHeightInPoints((short)16);
            headStyle.setFont(font);// 把字体应用到当前的样式
            HSSFRow row = sheet.createRow(rowNum);
            HSSFCell cell = row.createCell(0);
            cell.setCellValue("时间：" + startTime + "-" + endTime);
            cell.setCellStyle(headStyle);

            rowNum++;
            row = sheet.createRow(rowNum);
            cell = row.createCell(0);
            cell.setCellValue("没有任何交易记录");
            cell.setCellStyle(headStyle);
        }

        wb.write(out);
    }

    /**
     * 统计分类产品
     * @param orders
     * @param cashierOrderProductStastics
     */
    private void cashierStastics(Orders orders,Map<String,Object> cashierOrderProductStastics){
        if(orders.getEquipmentId() == null && orders.getIncrement().intValue()!=1){
            Map<Integer,Map<String,Map<String,Object>>> activityTypeIdCashierMap = (Map)cashierOrderProductStastics.get(orders.getCashierName());
            if(activityTypeIdCashierMap == null){
                activityTypeIdCashierMap = new HashMap<Integer,Map<String,Map<String,Object>>>();
                cashierOrderProductStastics.put(orders.getCashierName(),activityTypeIdCashierMap);
            }
            FinanceExcelUtil.createProductStasticsData(orders,activityTypeIdCashierMap);
        }
    }

    /**
     * 押金退款头
     * @param wb
     * @param sheet
     * @param rowNum
     */
    private void creatAllPersonHead(HSSFWorkbook wb,HSSFSheet sheet,int rowNum){
        HSSFCellStyle style = wb.createCellStyle(); // 样式对象
        HSSFFont font = wb.createFont();// 生成一个字体
        font.setColor(HSSFColor.GREEN.index);// HSSFColor.VIOLET.index // 字体颜色
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);// 字体增粗
        style.setFont(font);// 把字体应用到当前的样式
        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = row.createCell((short) 0);
        cell.setCellValue("订单号");
        cell.setCellStyle(style);
        cell = row.createCell((short) 1);
        cell.setCellValue("产品");
        cell.setCellStyle(style);
        cell = row.createCell((short) 2);
        cell.setCellValue("场次");
        cell.setCellStyle(style);
        cell = row.createCell((short) 3);
        cell.setCellValue("票券名");
        cell.setCellStyle(style);
        cell = row.createCell((short) 4);
        cell.setCellValue("押金支出");
        cell.setCellStyle(style);
        cell = row.createCell((short) 5);
        cell.setCellValue("支出时间");
        cell.setCellStyle(style);
    }

    /**
     * 设置金额格式
     * @param wb
     * @param cell
     */
    private void setDecimal(HSSFWorkbook wb,HSSFCell cell){
        HSSFCellStyle style = wb.createCellStyle(); // 样式对象
        HSSFDataFormat df = wb.createDataFormat();
        style.setDataFormat(df.getBuiltinFormat("0.00"));
        cell.setCellStyle(style);
    }

    //生成收银员支付方式汇总sheet
    private HSSFSheet createCashierPayStasticsSheet(BigDecimal totalStoragePayPrice,String startTime,String endTime,HSSFWorkbook wb,
                                                    List<Orders> orderslist,Map<String,Map<Integer,BigDecimal>> cashierInPayDepositMoney,Map<String,Map<Integer,BigDecimal>> cashierOutPayDepositMoney,
                                                    Set<Integer> paySet,Set<String> cashier,Map<String,BigDecimal> abnormalOrdersMoney,Map<String,Integer> abnormalOrdersNum){
        HSSFSheet sheet = wb.createSheet("收银员汇总");
        Set<Integer> payList = new HashSet<Integer>();
        Map<String,Map<Integer,BigDecimal>> orderstastic = new HashMap<String,Map<Integer,BigDecimal>>();
        if(orderslist!=null){
            for (Orders child : orderslist) {
                if(child.getParentId()!=null){
                    Map<Integer, BigDecimal> integerBigDecimalMap = orderstastic.get(child.getCashierName());
                    if(integerBigDecimalMap == null){
                        integerBigDecimalMap = new HashMap<Integer, BigDecimal>();
                        orderstastic.put(child.getCashierName(),integerBigDecimalMap);
                    }
                    BigDecimal bigDecimal = integerBigDecimalMap.get(child.getPayTypeId());
                    if(bigDecimal == null){
                        bigDecimal =  BigDecimal.ZERO;
                    }
                    integerBigDecimalMap.put(child.getPayTypeId(),bigDecimal.add(child.getTotalPrice()));
                    payList.add(child.getPayTypeId());
                }
            }
        }

        int rowNum = 0;

        HSSFCellStyle headStyle = wb.createCellStyle(); // 样式对象
        HSSFFont font = wb.createFont();// 生成一个字体
        font.setColor(HSSFColor.BLACK.index);// HSSFColor.VIOLET.index // 字体颜色
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);// 字体增粗
        font.setFontHeightInPoints((short)16);
        headStyle.setFont(font);// 把字体应用到当前的样式
        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = row.createCell(0);
        cell.setCellValue("时间：" + startTime + "-" + endTime);
        cell.setCellStyle(headStyle);

        rowNum++;
        row = sheet.createRow(rowNum);
        cell = row.createCell(0);
        cell.setCellValue("收银员支付方式汇总：");
        cell.setCellStyle(headStyle);

        rowNum++;

        HSSFCellStyle style = wb.createCellStyle(); // 样式对象
        font = wb.createFont();// 生成一个字体
        font.setColor(HSSFColor.GREEN.index);// HSSFColor.VIOLET.index // 字体颜色
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);// 字体增粗
        style.setFont(font);// 把字体应用到当前的样式

        row = sheet.createRow(rowNum);
        cell = row.createCell(0);
        cell.setCellValue("收银员");
        cell.setCellStyle(style);
        int count = 1;
        for (Integer pay : payList) {
            cell = row.createCell(count);
            cell.setCellValue(FinanceExcelUtil.getPayName(pay));
            cell.setCellStyle(style);
            count++;
        }

        cell = row.createCell(count);
        cell.setCellValue("总计");
        cell.setCellStyle(style);

        rowNum++;

        //组装订单统计
        Iterator<String> iter1 = cashier.iterator();
        while (iter1.hasNext()){
            String name = iter1.next();
            if(StringUtils.isBlank(name)){
                continue;
            }
            Map<Integer, BigDecimal> pls = orderstastic.get(name);
            count = 0;
            row = sheet.createRow(rowNum);
            cell = row.createCell(count);
            cell.setCellValue(name);

            for (Integer pay : payList) {
                count++;
                cell = row.createCell(count);
                BigDecimal bigDecimal = BigDecimal.ZERO;
                if(pls!=null){
                    bigDecimal = pls.get(pay);
                    if(bigDecimal == null){
                        bigDecimal = BigDecimal.ZERO;
                    }
                }
                cell.setCellValue(bigDecimal.doubleValue());
            }

            cell = row.createCell(count+1);
            String sc = CellReference.convertNumToColString(1);
            String ec = CellReference.convertNumToColString(count);
            setDecimal(wb,cell);
            cell.setCellFormula("SUM(" + sc + (rowNum+1) + ":"+ec+(rowNum+1)+")");

            rowNum++;
        }
        row = sheet.createRow(rowNum);
        cell = row.createCell(0);
        cell.setCellValue("总计");
        int size = payList.size()+1;
        if(size > 1){
            for (int i = 0;i<size;i++) {
                cell = row.createCell(i+1);
                String sc = CellReference.convertNumToColString(i+1);
                setDecimal(wb,cell);
                cell.setCellFormula("SUM(" + sc + 4 + ":"+sc+rowNum+")");
            }
        }

        int endOrderIndex = rowNum + 1;

        rowNum++;
        rowNum = rowNum + 2;

        //押金
        row = sheet.createRow(rowNum);
        cell = row.createCell(0);
        cell.setCellValue("收银押金汇总");
        cell.setCellStyle(headStyle);
        rowNum++;
        creaDepositStasticsHead(wb,sheet,rowNum,paySet);
        rowNum = rowNum + 2;

        int depositIndexStart = rowNum;

        Iterator<String> iter = cashier.iterator();
        while (iter.hasNext()){
            String name = iter.next();

            if(StringUtils.isBlank(name)){
                continue;
            }

            row = sheet.createRow(rowNum);
            cell = row.createCell(0);
            cell.setCellValue(name);

            Map<Integer, BigDecimal> cashierInPayDeposit = cashierInPayDepositMoney.get(name);
            Map<Integer, BigDecimal> cashierOutPayDeposit = cashierOutPayDepositMoney.get(name);
            int index = 1;
            for (Integer pay : paySet) {
                BigDecimal price = BigDecimal.ZERO;
                if(cashierInPayDeposit != null){
                    price = cashierInPayDeposit.get(pay);
                    if(price == null){
                        price = BigDecimal.ZERO;
                    }
                }
                cell = row.createCell(index);
                cell.setCellValue(price.doubleValue());
                index++;
            }

            for (Integer pay : paySet) {
                BigDecimal price = BigDecimal.ZERO;
                if(cashierOutPayDeposit != null){
                    price = cashierOutPayDeposit.get(pay);
                    if(price == null){
                        price = BigDecimal.ZERO;
                    }
                }
                cell = row.createCell(index);
                cell.setCellValue(price.doubleValue());
                index++;
            }

            int s = paySet.size();

            for (Integer pay : paySet) {
                cell = row.createCell(index);
                String sc = CellReference.convertNumToColString(index- 2 * s);
                String ec = CellReference.convertNumToColString(index - s);
                setDecimal(wb,cell);
                cell.setCellFormula(sc + (rowNum+1) + "-"+ec+(rowNum+1));
                index++;
            }

            cell = row.createCell(index);
            String sc = CellReference.convertNumToColString(2 * s+1);
            String ec = CellReference.convertNumToColString(3 * s);
            setDecimal(wb,cell);
            cell.setCellFormula("SUM("+sc + (rowNum+1) + ":"+ec+(rowNum+1)+")");
            rowNum++;
        }
        size = paySet.size();
        row = sheet.createRow(rowNum);
        cell = row.createCell(0);
        cell.setCellValue("总计");
        if(size > 1){
            for (int i = 0;i <= 3 * size ;i++) {
                cell = row.createCell(i+1);
                String sc = CellReference.convertNumToColString(i+1);
                setDecimal(wb,cell);
                cell.setCellFormula("SUM(" + sc + (depositIndexStart+1) + ":"+sc+rowNum+")");
            }
        }

        int depositOrderIndex = rowNum + 1;

        //异常退款
        rowNum = rowNum + 2;
        row = sheet.createRow(rowNum);
        cell = row.createCell(0);
        cell.setCellValue("异常退款汇总");
        cell.setCellStyle(headStyle);
        rowNum++;
        createAbnormalStasticsHead(wb,sheet,rowNum);
        Iterator<String> iterAbnormal = cashier.iterator();
        rowNum++;
        int abnormalIndex = rowNum;
        while (iterAbnormal.hasNext()){
            String name = iterAbnormal.next();
            BigDecimal money = abnormalOrdersMoney.get(name);
            if(money==null){
                money = BigDecimal.ZERO;
            }
            int num = MapUtils.getIntValue(abnormalOrdersNum,name,0);
            row = sheet.createRow(rowNum);
            cell = row.createCell((short) 0);
            cell.setCellValue(name);
            cell = row.createCell((short) 1);
            cell.setCellValue(num);
            cell = row.createCell((short) (2));
            cell.setCellValue(money.setScale(2,RoundingMode.HALF_UP).doubleValue());
            rowNum++;
        }
        row = sheet.createRow(rowNum);
        cell = row.createCell((short) 0);
        cell.setCellValue("总计");
        cell = row.createCell((short) 1);
        String sc = CellReference.convertNumToColString(1);
        cell.setCellFormula("SUM(" + sc + abnormalIndex + ":"+sc+rowNum+")");
        cell = row.createCell((short) 2);
        sc = CellReference.convertNumToColString(2);
        setDecimal(wb,cell);
        cell.setCellFormula("SUM(" + sc + abnormalIndex + ":"+sc+rowNum+")");

        int abnormalEndIndex = rowNum + 1;

        rowNum = rowNum + 2;
        row = sheet.createRow(rowNum);
        cell = row.createCell((short) 0);
        cell.setCellValue("结算总计(不计储值支付金额)");
        cell = row.createCell((short) 1);
        String oc = CellReference.convertNumToColString(size+1);
        String tc = CellReference.convertNumToColString(3*size+1);
        String fc = CellReference.convertNumToColString(2);
        setDecimal(wb,cell);
        cell.setCellFormula(oc + endOrderIndex + "+"+tc+depositOrderIndex+"-" + fc + abnormalEndIndex+"-"+totalStoragePayPrice.doubleValue());

        return sheet;
    }

    /**
     * 异常退款金额
     * @param wb
     * @param sheet
     * @param rowNum
     */
    private void createAbnormalStasticsHead(HSSFWorkbook wb,HSSFSheet sheet,int rowNum){
        HSSFCellStyle style = wb.createCellStyle(); // 样式对象
        HSSFFont font = wb.createFont();// 生成一个字体
        font.setColor(HSSFColor.GREEN.index);// HSSFColor.VIOLET.index // 字体颜色
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);// 字体增粗
        style.setFont(font);// 把字体应用到当前的样式
        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = row.createCell((short) 0);
        cell.setCellValue("收银员");
        cell.setCellStyle(style);
        cell = row.createCell((short) 1);
        cell.setCellValue("订单数量");
        cell.setCellStyle(style);
        cell = row.createCell((short) (2));
        cell.setCellValue("退款金额");
        cell.setCellStyle(style);
    }

    private void creaDepositStasticsHead(HSSFWorkbook wb,HSSFSheet sheet,int rowNum,Set<Integer> paySet){
        HSSFCellStyle style = wb.createCellStyle(); // 样式对象
        HSSFFont font = wb.createFont();// 生成一个字体
        font.setColor(HSSFColor.GREEN.index);// HSSFColor.VIOLET.index // 字体颜色
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);// 字体增粗
        style.setFont(font);// 把字体应用到当前的样式
        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = row.createCell((short) 0);
        cell.setCellValue("收银员");
        cell.setCellStyle(style);

        int size = paySet.size();

        if(size > 0){
            sheet.addMergedRegion(new Region(rowNum,(short) 1,rowNum,(short) size));
            sheet.addMergedRegion(new Region(rowNum,(short)(size+1), rowNum,(short)(2*size)));
            sheet.addMergedRegion(new Region(rowNum,(short)(2 * size+1), rowNum,(short)(3*size)));
        }

        cell = row.createCell((short) 1);
        cell.setCellValue("押金收入");
        cell.setCellStyle(style);
        cell = row.createCell((short) (size+1));
        cell.setCellValue("押金支出");
        cell.setCellStyle(style);
        cell = row.createCell((short) (2 * size+1));
        cell.setCellValue("押金总计");
        cell.setCellStyle(style);

        rowNum++;
        if(size>0){
            row = sheet.createRow(rowNum);
        }
        for(int i = 0; i < size; i++){
            Integer pay = (Integer) paySet.toArray()[i];
            String payName = FinanceExcelUtil.getPayName(pay);

            cell = row.createCell((short) i + 1);
            cell.setCellValue(payName);
            cell.setCellStyle(style);

            cell = row.createCell((short) size + i + 1);
            cell.setCellValue(payName);
            cell.setCellStyle(style);

            cell = row.createCell((short) 2 * size + i + 1);
            cell.setCellValue(payName);
            cell.setCellStyle(style);
        }

        cell = row.createCell((short) (3 * size+1));
        cell.setCellValue("总计");
        cell.setCellStyle(style);

    }

    /**
     * 收银员统计
     * @param wb
     * @return
     */
    private HSSFSheet createCashierStasticsSheet(HSSFWorkbook wb,String cashierName,Map<Integer,Map<String,Map<String,Object>>> cashierOrderProductStastics,
                                                 List<Map<String, Object>> rentOrderslist,List<Map<String, Object>> rentTimeOutOrderslist,
                                                 List<Map<String, Object>> addServiceCashielist,List<ZooBalanceDepositIndemnity> depositIndemnities,
                                                 Map<Integer,Map<String,BigDecimal>> depositStastic,Map<Integer,Map<String,BigDecimal>> orderPayStastic,
                                                 BigDecimal orderTotalMoney,BigDecimal depositInMoney,BigDecimal depositOutMone,BigDecimal abnormalMoney,BigDecimal storageMoney,
                                                 BigDecimal indemnityTotalMoney,List<Map> hungUpcashierlist,List<Map> entertainlist,
                                                 List<LimitCardIndemnity> limitCardIndemnities,BigDecimal limitCardIndemnitiesMoney){
        if(cashierName.contains("小ZOO")){
            cashierName = "总管理员";
        }
        if(wb.getSheet(cashierName + "汇总")!=null){
            cashierName += "-" + DateUtil.convertDateToString(new Date(),"ss");
        }
        HSSFSheet sheet = wb.createSheet(cashierName + "汇总");

        int rowNum = 2;

        HSSFCellStyle headStyle = wb.createCellStyle(); // 样式对象
        HSSFFont font = wb.createFont();// 生成一个字体
        font.setColor(HSSFColor.BLACK.index);// HSSFColor.VIOLET.index // 字体颜色
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);// 字体增粗
        font.setFontHeightInPoints((short)16);
        headStyle.setFont(font);// 把字体应用到当前的样式
        headStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        headStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);//垂直居中
        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = row.createCell(1);
        cell.setCellStyle(headStyle);
        cell.setCellValue(cashierName+"汇总表");

        rowNum++;

        int[] startIndex = new int[0];

        if(cashierOrderProductStastics!=null){
            FinanceExcelUtil.createProductHead(wb,sheet,rowNum);
            rowNum++;
            //产品
            rowNum = FinanceExcelUtil.createProdctItem(wb,sheet,rowNum,cashierOrderProductStastics);
            rowNum++;

            startIndex =ArrayUtils.add(startIndex,rowNum);
        }

        //租赁物//超时租赁物
        if(rentOrderslist!=null||rentTimeOutOrderslist!=null){
            //租赁物
            rowNum = FinanceExcelUtil.createRentItem(wb,sheet,rowNum,rentOrderslist,rentTimeOutOrderslist);
            rowNum++;

            startIndex =ArrayUtils.add(startIndex,rowNum);
        }

        //增值服务
        if(addServiceCashielist!=null && addServiceCashielist.size()>0){
            //增值服务
            rowNum = FinanceExcelUtil.createAddServiceItem(wb,sheet,rowNum,addServiceCashielist);
            rowNum++;

            startIndex =ArrayUtils.add(startIndex,rowNum);
        }

        if(startIndex.length>0){
            HSSFCellStyle hjStyle = wb.createCellStyle(); // 样式对象
            hjStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
            hjStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
            hjStyle.setFillForegroundColor(HSSFColor.GREY_40_PERCENT.index);
            hjStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
            hjStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
            hjStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
            hjStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);

            row = sheet.createRow(rowNum);
            cell = row.createCell(1);
            cell.setCellValue("收入总金额");
            cell.setCellStyle(hjStyle);
            cell = row.createCell(2);
            cell.setCellValue("");
            cell.setCellStyle(hjStyle);

            HSSFDataFormat df = wb.createDataFormat();
            hjStyle.setDataFormat(df.getBuiltinFormat("#,##0.00"));
            cell = row.createCell(3);
            String sc = CellReference.convertNumToColString(3);
            String sumStr = "";
            for (int index : startIndex) {
                if(sumStr.equals("")){
                    sumStr += sc + index ;
                }else{
                    sumStr += "+"+sc + index ;
                }
            }
            cell.setCellFormula(sumStr);
            cell.setCellStyle(hjStyle);
            cell = row.createCell(4);
            String pc = CellReference.convertNumToColString(4);
            sumStr = "";
            for (int index : startIndex) {
                if(sumStr.equals("")){
                    sumStr += pc + index ;
                }else{
                    sumStr += "+"+pc + index ;
                }
            }
            cell.setCellFormula(sumStr);
            cell.setCellStyle(hjStyle);
            rowNum++;
        }

        //赔偿
        if(depositIndemnities!=null&&depositIndemnities.size()>0){
            Map<Integer,Map<String,Object>> indemnityProduct = new HashMap<Integer, Map<String, Object>>();
            for (ZooBalanceDepositIndemnity depositIndemnity : depositIndemnities) {
                Integer equipmentId = depositIndemnity.getEquipmentId();
                Map<String, Object> p = indemnityProduct.get(equipmentId);

                if(p == null){
                    p = new HashMap<String,Object>();
                    indemnityProduct.put(equipmentId,p);
                    p.put("money",BigDecimal.ZERO);
                    p.put("name",depositIndemnity.getEquipmentName());
                }
                int num = MapUtils.getIntValue(p,"num",0);
                p.put("num",num+1);
                BigDecimal money = (BigDecimal)p.get("money");
                p.put("money",money.add(depositIndemnity.getIndemnity()).setScale(2,RoundingMode.HALF_UP));
            }

            //赔偿
            rowNum = FinanceExcelUtil.createDepositIndemnityItem(wb,sheet,rowNum,indemnityProduct);
            rowNum++;
        }

        if(hungUpcashierlist!=null&&hungUpcashierlist.size()>0){
            //挂账统计
            rowNum = FinanceExcelUtil.createHungUpItem(wb,sheet,rowNum,hungUpcashierlist);
            rowNum++;
        }
        if(entertainlist!=null&&entertainlist.size()>0){
            //招待统计
            rowNum = FinanceExcelUtil.createEntertainItem(wb,sheet,rowNum,entertainlist);
            rowNum++;
        }

        rowNum+=2;

        //支付方式汇总 Map<Integer,Map<String,BigDecimal>> payTypeStastic
        row = sheet.createRow(rowNum);
        cell = row.createCell(1);
        cell.setCellStyle(headStyle);
        cell.setCellValue("支付方式汇总");
        rowNum++;
        if(orderPayStastic!=null){
            FinanceExcelUtil.createPayTypeHead(wb,sheet,rowNum);
            rowNum++;
            //支付方式
            rowNum = FinanceExcelUtil.createPayTypeStasticItem(wb,sheet,rowNum,orderPayStastic);
            rowNum++;
        }
        rowNum++;
        rowNum++;

        row = sheet.createRow(rowNum);
        cell = row.createCell(1);
        cell.setCellStyle(headStyle);
        cell.setCellValue("押金支付方式汇总");
        rowNum++;
        FinanceExcelUtil.createDepositStasticHead(wb,sheet,rowNum);
        if(depositStastic!=null){
            rowNum++;
            //押金支付方式
            rowNum = FinanceExcelUtil.createDepositStasticItem(wb,sheet,rowNum,depositStastic);
            rowNum++;
        }
        rowNum++;

        if(orderTotalMoney == null){
            orderTotalMoney = BigDecimal.ZERO;
        }
        if(storageMoney == null){
            storageMoney = BigDecimal.ZERO;
        }
        if(abnormalMoney == null){
            abnormalMoney = BigDecimal.ZERO;
        }
        if(depositOutMone == null){
            depositOutMone = BigDecimal.ZERO;
        }
        if(depositInMoney == null){
            depositInMoney = BigDecimal.ZERO;
        }
        if(indemnityTotalMoney == null){
            indemnityTotalMoney = BigDecimal.ZERO;
        }

        List<Map<String,Object>> allStastics = new ArrayList<Map<String,Object>>();
        Map<String,Object> map1=new HashMap<String,Object>();
        map1.put("money",orderTotalMoney.subtract(depositInMoney).setScale(2,RoundingMode.HALF_UP));
        map1.put("name","收入金额");
        allStastics.add(map1);
        Map<String,Object> map2=new HashMap<String,Object>();
        map2.put("money",abnormalMoney);
        map2.put("name","异常退款金额");
        allStastics.add(map2);
        Map<String,Object> map3=new HashMap<String,Object>();
        map3.put("money",orderTotalMoney.subtract(abnormalMoney).subtract(depositInMoney).setScale(2,RoundingMode.HALF_UP));
        map3.put("name","应收金额");
        allStastics.add(map3);
        Map<String,Object> map4=new HashMap<String,Object>();
        map4.put("money",indemnityTotalMoney);
        map4.put("name","赔偿金额");
        allStastics.add(map4);
        Map<String,Object> map5=new HashMap<String,Object>();
        map5.put("money",depositInMoney.subtract(depositOutMone).setScale(2,RoundingMode.HALF_UP));
        map5.put("name","押金合计");
        allStastics.add(map5);
        Map<String,Object> map6=new HashMap<String,Object>();
        map6.put("money",limitCardIndemnitiesMoney.setScale(2,RoundingMode.HALF_UP));
        map6.put("name","时限卡换卡合计");
        allStastics.add(map5);
        Map<String,Object> map7=new HashMap<String,Object>();
        map7.put("money",orderTotalMoney.subtract(storageMoney).subtract(abnormalMoney).subtract(depositOutMone).add(limitCardIndemnitiesMoney).setScale(2,RoundingMode.HALF_UP));
        map7.put("name","实收金额");
        allStastics.add(map7);

        rowNum++;
        rowNum++;
        //汇总
        row = sheet.createRow(rowNum);
        cell = row.createCell(1);
        cell.setCellStyle(headStyle);
        cell.setCellValue("汇总(储值支付不计算在实收金额中)");
        rowNum++;
        if(allStastics!=null){
            //汇总
            rowNum = FinanceExcelUtil.createEndItem(wb,sheet,rowNum,allStastics);
            rowNum++;
        }

        return sheet;
    }

    private void createHungUp(){

    }

    private HSSFSheet createStasticsSheet(HSSFWorkbook wb,String cashierName,List<Orders> parentOrders,List<Orders> childOrders,
                                          BigDecimal orderTotalMoney,BigDecimal depositInMoney,BigDecimal depositOutMone,Integer pNum,
                                          List<Map> depositRefunds,List<ZooBalanceDepositIndemnity> depositIndemnities,BigDecimal abnormalMoney,BigDecimal storageMoney){
        if(depositOutMone == null){
            depositOutMone = BigDecimal.ZERO;
        }
        if(depositInMoney == null){
            depositInMoney = BigDecimal.ZERO;
        }
        if(orderTotalMoney == null){
            orderTotalMoney = BigDecimal.ZERO;
        }
        if(storageMoney == null){
            storageMoney = BigDecimal.ZERO;
        }

        if(parentOrders!=null && childOrders == null){
            childOrders = parentOrders;
        }

        HSSFSheet sheet = wb.createSheet(cashierName + "汇总");
        Set<Integer> payList = new HashSet<Integer>();
        Map<Integer,Map<Integer,BigDecimal>> orderstastic = new HashMap<Integer,Map<Integer,BigDecimal>>();
        Map<Integer,BigDecimal> depositInMap = new HashMap<Integer,BigDecimal>();//押金进账
        if(childOrders!=null){
            for (Orders child : childOrders) {
                Map<Integer, BigDecimal> integerBigDecimalMap = orderstastic.get(child.getActivityTypeId());
                if(integerBigDecimalMap == null){
                    integerBigDecimalMap = new HashMap<Integer, BigDecimal>();
                    orderstastic.put(child.getActivityTypeId(),integerBigDecimalMap);
                }
                BigDecimal bigDecimal = integerBigDecimalMap.get(child.getPayTypeId());
                if(bigDecimal == null){
                    bigDecimal =  BigDecimal.ZERO;
                }
                integerBigDecimalMap.put(child.getPayTypeId(),bigDecimal.add(child.getTotalPrice()));
                payList.add(child.getPayTypeId());
            }
        }

        //押金收入
        if(parentOrders!=null){
            for (Orders orders : parentOrders) {
                BigDecimal depositMoney = depositInMap.get(orders.getPayTypeId());
                if(depositMoney == null){
                    depositMoney = BigDecimal.ZERO;
                }
                BigDecimal deposit = orders.getDeposit();
                if(deposit == null){
                    deposit = BigDecimal.ZERO;
                }
                depositInMap.put(orders.getPayTypeId(),depositMoney.add(deposit));
            }
        }


        //押金出账
        Map<Integer,BigDecimal> depositOutMap = new HashMap<Integer,BigDecimal>();
        if(depositRefunds!=null){
            for (Map depositRefund : depositRefunds) {
                Integer pay_type_id = MapUtils.getInteger(depositRefund, "pay_type_id");
                payList.add(pay_type_id);
                double refund_amount = MapUtils.getDoubleValue(depositRefund, "refund_amount", 0);
                BigDecimal depositMoney = depositOutMap.get(pay_type_id);
                if(depositMoney == null){
                    depositMoney = BigDecimal.ZERO;
                }
                depositOutMap.put(pay_type_id,depositMoney.add(new BigDecimal(refund_amount)));
            }
        }

        int rowNum = 0;

        HSSFCellStyle style = wb.createCellStyle(); // 样式对象
        HSSFFont font = wb.createFont();// 生成一个字体
        font.setColor(HSSFColor.GREEN.index);// HSSFColor.VIOLET.index // 字体颜色
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);// 字体增粗
        style.setFont(font);// 把字体应用到当前的样式
        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = null;

        int count = 1;
        for (Integer pay : payList) {
            cell = row.createCell(count);
            cell.setCellValue(FinanceExcelUtil.getPayName(pay));
            cell.setCellStyle(style);
            count++;
        }

        cell = row.createCell(count);
        cell.setCellValue("总计");
        cell.setCellStyle(style);

        rowNum++;

        //组装订单统计
        Iterator<Map.Entry<Integer, Map<Integer, BigDecimal>>> iterator = orderstastic.entrySet().iterator();
        while (iterator.hasNext()){
            Map.Entry<Integer, Map<Integer, BigDecimal>> next = iterator.next();
            Map<Integer, BigDecimal> value = next.getValue();
            Integer key = next.getKey();

            count = 0;
            row = sheet.createRow(rowNum);
            cell = row.createCell(count);
            cell.setCellValue(getActivityTypeIdName(key));

            for (Integer pay : payList) {
                count++;
                cell = row.createCell(count);
                BigDecimal bigDecimal = value.get(pay);
                if(bigDecimal == null){
                    bigDecimal = BigDecimal.ZERO;
                }
                cell.setCellValue(bigDecimal.doubleValue());
            }

            cell = row.createCell(count+1);
            String sc = CellReference.convertNumToColString(1);
            String ec = CellReference.convertNumToColString(count);
            setDecimal(wb,cell);
            cell.setCellFormula("SUM(" + sc + (rowNum+1) + ":"+ec+(rowNum+1)+")");

            rowNum++;
        }
        row = sheet.createRow(rowNum);
        cell = row.createCell(0);
        cell.setCellValue("总计");
        int size = payList.size()+1;
        if(size > 1){
            for (int i = 0;i<size;i++) {
                cell = row.createCell(i+1);
                String sc = CellReference.convertNumToColString(i+1);
                setDecimal(wb,cell);
                cell.setCellFormula("SUM(" + sc + 2 + ":"+sc+rowNum+")");
            }
        }

        rowNum++;
        rowNum++;

        int depositTotalIndex = rowNum+2;

        //组装押金入账与退款
        row = sheet.createRow(rowNum);
        cell = row.createCell(1);
        cell.setCellValue("押金收入");
        cell.setCellStyle(style);
        cell = row.createCell(2);
        cell.setCellValue("押金退还");
        cell.setCellStyle(style);
        cell = row.createCell(3);
        cell.setCellValue("总计");
        cell.setCellStyle(style);

        rowNum++;
        int dsize = depositOutMap.size()>=depositInMap.size()?depositOutMap.size():depositInMap.size();
        for (Integer pay : payList) {
            row = sheet.createRow(rowNum);
            cell = row.createCell(0);
            cell.setCellValue(FinanceExcelUtil.getPayName(pay));
            cell = row.createCell(1);
            BigDecimal in = depositInMap.get(pay);
            if(in == null){
                cell.setCellValue(0);
            }else{
                cell.setCellValue(in.doubleValue());
            }

            cell = row.createCell(2);
            BigDecimal out = depositOutMap.get(pay);
            if(out == null){
                cell.setCellValue(0);
            }else{
                cell.setCellValue(out.doubleValue());
            }

            cell = row.createCell(3);
            String sc = CellReference.convertNumToColString(1);
            String ec = CellReference.convertNumToColString(2);
            setDecimal(wb,cell);
            cell.setCellFormula(sc + (rowNum+1) + "-"+ec+(rowNum+1));

            rowNum++;
        }

        row = sheet.createRow(rowNum);
        cell = row.createCell(0);
        cell.setCellValue("总计");
        if(size>1){
            for (int i = 1;i<=3;i++) {
                cell = row.createCell(i);
                String sc = CellReference.convertNumToColString(i);
                setDecimal(wb,cell);
                cell.setCellFormula("SUM(" + sc + depositTotalIndex + ":"+sc+rowNum+")");
            }
        }

        rowNum++;
        rowNum++;

        row = sheet.createRow(rowNum);
        cell = row.createCell(0);
        cell.setCellValue("异常退款");
        cell = row.createCell(1);
        if(abnormalMoney == null){
            abnormalMoney = BigDecimal.ZERO;
        }
        cell.setCellValue(abnormalMoney.setScale(2,RoundingMode.HALF_UP).doubleValue());


        return sheet;
    }

    private String getActivityTypeIdName(Integer activityTypeId){
        if(activityTypeId == null){
            return "";
        }
        switch (activityTypeId.intValue()){
            case 0:
                return "最新活动";
            case 1:
                return "滑雪度假";
            case 2:
                return "教练预约";
            case 3:
                return  "超值票务";
            case 4:
                return  "会员卡";
            case 5:
                return "训练营";
            case 6:
                return "自营票务";
            case 7:
                return "酒店预定";
            case 8:
                return "教练课程";
            case 9:
                return "储值卡";
            case 10:
                return "续时产品";
        }
        return "";
    }


    /**
     * 创建订单详情sheet
     * @param wb
     * @param cashierName
     * @param parentOrders
     * @param childOrders
     * @return
     */
    private HSSFSheet createOrderDetailSheet(HSSFWorkbook wb,String cashierName,List<Orders> parentOrders,List<Orders> childOrders,List<Orders> abnormalOrders,
                                             BigDecimal orderTotalMoney,BigDecimal depositInMoney,BigDecimal depositOutMone,Integer pNum,
                                             List<Map> depositRefunds,List<ZooBalanceDepositIndemnity> depositIndemnities,
                                             List<LimitCardIndemnity> limitCardIndemnities,BigDecimal limitIndemnitiesMoney){

        if(depositOutMone == null){
            depositOutMone = BigDecimal.ZERO;
        }
        if(depositInMoney == null){
            depositInMoney = BigDecimal.ZERO;
        }
        if(orderTotalMoney == null){
            orderTotalMoney = BigDecimal.ZERO;
        }

        if(parentOrders!=null && childOrders!=null){
            for (Orders parentOrder : parentOrders) {
                if(parentOrder.getParentId()!=null){
                    childOrders.add(parentOrder);
                }
            }
        }

        if(cashierName.contains("小ZOO")){
            cashierName = "总管理员";
        }
        if(wb.getSheet(cashierName)!=null){
            cashierName = cashierName + "-" + DateUtil.dateToDateString(new Date(),"ss");
        }
        HSSFSheet sheet = wb.createSheet(cashierName);

        //按产品场次进行分类
        Map<String,List<Orders>> episodeOrders = getOrdersByProductEpisode(parentOrders);
        //订单下商品数量
        Map<Integer,Integer> orderProductNum = new HashMap<Integer,Integer>();

        HSSFCellStyle style = wb.createCellStyle(); // 样式对象
        HSSFFont font = wb.createFont();// 生成一个字体
        font.setColor(HSSFColor.RED.index);// HSSFColor.VIOLET.index // 字体颜色
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);// 字体增粗
        style.setFont(font);// 把字体应用到当前的样式
        int rowNum = 0;
        String operatorStr = "操作员统计------订单金额:" + new BigDecimal(orderTotalMoney.doubleValue()).setScale(2,RoundingMode.HALF_DOWN) +
                "|押金收入:" + new BigDecimal(depositInMoney.doubleValue()).setScale(2,RoundingMode.HALF_DOWN) +
                "|押金支出(当前操作员的订单):" + new BigDecimal(depositOutMone.doubleValue()).setScale(2,RoundingMode.HALF_DOWN) +
                "|时限卡赔偿金额:" + new BigDecimal(limitIndemnitiesMoney.doubleValue()).setScale(2,RoundingMode.HALF_DOWN) +
                "|产品数量:" + pNum;
        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = row.createCell((short) 0);
        cell.setCellValue(operatorStr);
        cell.setCellStyle(style);
        rowNum += 3;

        HSSFCellStyle style2 = wb.createCellStyle(); // 样式对象
        HSSFFont font2 = wb.createFont();// 生成一个字体
        font2.setColor(HSSFColor.GREEN.index);// HSSFColor.VIOLET.index // 字体颜色
        font2.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);// 字体增粗
        style2.setFont(font2);// 把字体应用到当前的样式

        HSSFCellStyle styleMain = wb.createCellStyle(); // 样式对象
        HSSFFont fontMain = wb.createFont();// 生成一个字体
        fontMain.setColor(HSSFColor.DARK_YELLOW.index);// HSSFColor.VIOLET.index // 字体颜色
        fontMain.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);// 字体增粗
        styleMain.setFont(fontMain);// 把字体应用到当前的样式

        Iterator<Map.Entry<String, List<Orders>>> iterator = episodeOrders.entrySet().iterator();
        while(iterator.hasNext()){
            Map.Entry<String, List<Orders>> next = iterator.next();
            String key = next.getKey();
            List<Orders> value = next.getValue();

            if(value==null){
                continue;
            }

            double totalPrice = 0;
            double depositTotalPrice = 0;
            int totalNum = 0;
            for (Orders orders : value) {
                totalPrice += orders.getTotalPrice().doubleValue();
                if(orders.getDeposit()!=null){
                    depositTotalPrice += orders.getDeposit().doubleValue();
                }
                if(orders.getParentId()==null && childOrders!=null){
                    int ptn = 0;
                    for (Orders childOrder : childOrders) {
                        if(childOrder.getParentId().intValue() == orders.getOrderId().intValue()){
                            totalNum += childOrder.getChargeNum().intValue();
                            ptn += childOrder.getChargeNum().intValue();;
                        }
                    }
                    orderProductNum.put(orders.getOrderId(),ptn);
                }else if(orders.getParentId()!=null && orders.getParentId().intValue()==0){
                    totalNum += orders.getChargeNum().intValue();
                    orderProductNum.put(orders.getOrderId(),orders.getChargeNum());
                }
            }

            row = sheet.createRow(rowNum);
            cell = row.createCell((short) 0);
            String episodeStr = key;
            episodeStr += "|场次订单金额:" + new BigDecimal(totalPrice).setScale(2,RoundingMode.HALF_DOWN);
            episodeStr += "|押金收入:" + new BigDecimal(depositTotalPrice).setScale(2,RoundingMode.HALF_DOWN);
            episodeStr += "|产品数量:" + totalNum;
            cell.setCellValue(episodeStr);// 以后加上场次的统计
            rowNum++;

            // 创建订单列表的 头 信息
            creaOrderDetailHead(wb,sheet,rowNum,style2);
            rowNum++;

            // 创建产品订单
            for (Orders orders : value) {
                createOrderDetailCell("主订单",wb,sheet,rowNum,orders,orderProductNum.get(orders.getOrderId()),styleMain);
                rowNum++;

                if(childOrders!=null){
                    for (Orders childOrder : childOrders) {
                        if(childOrder.getParentId().intValue() == orders.getOrderId().intValue()){
                            createOrderDetailCell("子订单",wb,sheet,rowNum,childOrder,childOrder.getChargeNum(),styleMain);
                            rowNum++;
                        }
                    }
                }
                rowNum++;
            }

            rowNum++;
            rowNum++;

        }

        rowNum++;

        //押金退款操作
        row = sheet.createRow(rowNum);
        cell = row.createCell((short) 0);
        cell.setCellValue("押金退款");// 以后加上场次的统计
        rowNum++;
        creaDepositRefundHead(wb,sheet,rowNum);
        rowNum++;
        if(depositRefunds!=null){
            //HSSFCellStyle styleMain = wb.createCellStyle(); // 样式对象
            //HSSFFont fontMain = wb.createFont();// 生成一个字体
            //fontMain.setColor(HSSFColor.DARK_YELLOW.index);// HSSFColor.VIOLET.index // 字体颜色
            //fontMain.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);// 字体增粗
            //styleMain.setFont(fontMain);// 把字体应用到当前的样式
            for (Map depositRefund : depositRefunds) {
                creaDepositRefundCell(wb,sheet,rowNum,depositRefund,styleMain);
                rowNum++;
            }
        }

        rowNum++;
        rowNum++;

        //赔偿
        row = sheet.createRow(rowNum);
        cell = row.createCell((short) 0);
        cell.setCellValue("赔偿");
        rowNum++;
        creatIndemnitiesHead(wb,sheet,rowNum);
        rowNum++;
        if(depositIndemnities!=null){
            //HSSFCellStyle styleMain = wb.createCellStyle(); // 样式对象
            //HSSFFont fontMain = wb.createFont();// 生成一个字体
            //fontMain.setColor(HSSFColor.DARK_YELLOW.index);// HSSFColor.VIOLET.index // 字体颜色
            //fontMain.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);// 字体增粗
            //styleMain.setFont(fontMain);// 把字体应用到当前的样式
            for (ZooBalanceDepositIndemnity depositIndemnity : depositIndemnities) {
                creatIndemnitiesCell(wb,sheet,rowNum,depositIndemnity,styleMain);
                rowNum++;
            }
        }

        rowNum++;

        //异常退款订单
        row = sheet.createRow(rowNum);
        cell = row.createCell((short) 0);
        cell.setCellValue("异常退款订单");
        rowNum++;
        creatAbnormalHead(wb,sheet,rowNum);
        rowNum++;
        if(abnormalOrders!=null){
            //HSSFCellStyle styleMain = wb.createCellStyle(); // 样式对象
            //HSSFFont fontMain = wb.createFont();// 生成一个字体
            //fontMain.setColor(HSSFColor.DARK_YELLOW.index);// HSSFColor.VIOLET.index // 字体颜色
            //fontMain.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);// 字体增粗
            //styleMain.setFont(fontMain);// 把字体应用到当前的样式
            for (Orders abnormalOrder : abnormalOrders) {
                creatAbnormalCell(wb,sheet,rowNum,abnormalOrder,styleMain);
                rowNum++;
            }
        }

        rowNum++;

        row = sheet.createRow(rowNum);
        cell = row.createCell((short) 0);
        cell.setCellValue("时限卡换卡赔偿");
        rowNum++;
        creatLimitCardHead(wb,sheet,rowNum);
        rowNum++;
        if(limitCardIndemnities!=null){
            for (LimitCardIndemnity limitCardIndemnity : limitCardIndemnities) {
                creatLimitCardCell(wb,sheet,rowNum,limitCardIndemnity,styleMain);
            }
        }

        return sheet;
    }

    /**
     * 异常退款明细
     * @param wb
     * @param sheet
     * @param rowNum
     * @param abnormalOrder
     */
    private void creatAbnormalCell(HSSFWorkbook wb,HSSFSheet sheet,int rowNum,Orders abnormalOrder,HSSFCellStyle styleMain){
        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = row.createCell((short) 0);
        cell.setCellValue(abnormalOrder.getCode());
        cell = row.createCell((short) 1);
        cell.setCellValue(FinanceExcelUtil.getPayName(abnormalOrder.getPayTypeId()));
        cell = row.createCell((short) 2);
        cell.setCellValue(abnormalOrder.getActivityName());
        cell = row.createCell((short) 3);
        cell.setCellValue(abnormalOrder.getExtAbnormalRefundMoney().setScale(2,RoundingMode.HALF_UP).doubleValue());
        cell = row.createCell((short) 4);
        cell.setCellValue(abnormalOrder.getRemark());
    }

    /**
     * 时限卡明细
     * @param wb
     * @param sheet
     * @param rowNum
     * @param limitCardIndemnity
     */
    private void creatLimitCardCell(HSSFWorkbook wb,HSSFSheet sheet,int rowNum,LimitCardIndemnity limitCardIndemnity,HSSFCellStyle styleMain){
        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = row.createCell((short) 0);
        cell.setCellValue(DateUtil.convertDateToString(limitCardIndemnity.getCreateTime(),"yyyy-MM-dd HH:mm:ss"));
        cell = row.createCell((short) 1);
        cell.setCellValue(ActivityBeanUtils.getPayName(limitCardIndemnity.getPayType()));
        cell = row.createCell((short) 2);
        cell.setCellValue(limitCardIndemnity.getAmount().setScale(2,RoundingMode.HALF_UP).doubleValue());
        cell = row.createCell((short) 3);
        cell.setCellValue(limitCardIndemnity.getInfo());
    }

    /**
     * 时限卡换卡
     * @param wb
     * @param sheet
     * @param rowNum
     */
    private void creatLimitCardHead(HSSFWorkbook wb,HSSFSheet sheet,int rowNum){
        HSSFCellStyle style = wb.createCellStyle(); // 样式对象
        HSSFFont font = wb.createFont();// 生成一个字体
        font.setColor(HSSFColor.GREEN.index);// HSSFColor.VIOLET.index // 字体颜色
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);// 字体增粗
        style.setFont(font);// 把字体应用到当前的样式
        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = row.createCell((short) 0);
        cell.setCellValue("时间");
        cell.setCellStyle(style);
        cell = row.createCell((short) 1);
        cell.setCellValue("支付方式");
        cell.setCellStyle(style);
        cell = row.createCell((short) 2);
        cell.setCellValue("金额");
        cell.setCellStyle(style);
        cell = row.createCell((short) 3);
        cell.setCellValue("备注");
        cell.setCellStyle(style);
    }

    /**
     * 异常退款订单头
     * @param wb
     * @param sheet
     * @param rowNum
     */
    private void creatAbnormalHead(HSSFWorkbook wb,HSSFSheet sheet,int rowNum){
        HSSFCellStyle style = wb.createCellStyle(); // 样式对象
        HSSFFont font = wb.createFont();// 生成一个字体
        font.setColor(HSSFColor.GREEN.index);// HSSFColor.VIOLET.index // 字体颜色
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);// 字体增粗
        style.setFont(font);// 把字体应用到当前的样式
        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = row.createCell((short) 0);
        cell.setCellValue("订单号");
        cell.setCellStyle(style);
        cell = row.createCell((short) 1);
        cell.setCellValue("支付方式");
        cell.setCellStyle(style);
        cell = row.createCell((short) 2);
        cell.setCellValue("产品名称");
        cell.setCellStyle(style);
        cell = row.createCell((short) 3);
        cell.setCellValue("异常退款金额");
        cell.setCellStyle(style);
        cell = row.createCell((short) 4);
        cell.setCellValue("备注");
        cell.setCellStyle(style);
    }

    /**
     * 赔偿明细
     * @param wb
     * @param sheet
     * @param rowNum
     * @param depositIndemnity
     */
    private void creatIndemnitiesCell(HSSFWorkbook wb,HSSFSheet sheet,int rowNum,ZooBalanceDepositIndemnity depositIndemnity,HSSFCellStyle styleMain){
        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = row.createCell((short) 0);
        cell.setCellValue(depositIndemnity.getEquipmentName());
        cell.setCellStyle(styleMain);
        cell = row.createCell((short) 1);
        cell.setCellValue(DateUtil.convertDateToString(depositIndemnity.getIndemnityTime(),"yyyy-MM-dd HH:mm:ss"));
        cell = row.createCell((short) 2);
        cell.setCellValue(depositIndemnity.getIndemnity().toString());
    }

    /**
     * 押金退款头
     * @param wb
     * @param sheet
     * @param rowNum
     */
    private void creatIndemnitiesHead(HSSFWorkbook wb,HSSFSheet sheet,int rowNum){
        HSSFCellStyle style = wb.createCellStyle(); // 样式对象
        HSSFFont font = wb.createFont();// 生成一个字体
        font.setColor(HSSFColor.GREEN.index);// HSSFColor.VIOLET.index // 字体颜色
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);// 字体增粗
        style.setFont(font);// 把字体应用到当前的样式
        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = row.createCell((short) 0);
        cell.setCellValue("设备名称");
        cell.setCellStyle(style);
        cell = row.createCell((short) 1);
        cell.setCellValue("赔款时间");
        cell.setCellStyle(style);
        cell = row.createCell((short) 2);
        cell.setCellValue("赔偿金额");
        cell.setCellStyle(style);
    }

    private void creaDepositRefundCell(HSSFWorkbook wb,HSSFSheet sheet,int rowNum,Map depositRefund){
        HSSFCellStyle styleMain = wb.createCellStyle(); // 样式对象
        HSSFFont fontMain = wb.createFont();// 生成一个字体
        fontMain.setColor(HSSFColor.DARK_YELLOW.index);// HSSFColor.VIOLET.index // 字体颜色
        fontMain.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);// 字体增粗
        styleMain.setFont(fontMain);// 把字体应用到当前的样式
        creaDepositRefundCell(wb,sheet,rowNum,depositRefund,styleMain);
    }

    /**
     * 押金退款明细
     * @param wb
     * @param sheet
     * @param rowNum
     * @param depositRefund
     */
    private void creaDepositRefundCell(HSSFWorkbook wb,HSSFSheet sheet,int rowNum,Map depositRefund,HSSFCellStyle styleMain){

        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = row.createCell((short) 0);
        cell.setCellValue(MapUtils.getString(depositRefund,"code",""));
        cell.setCellStyle(styleMain);
        cell = row.createCell((short) 1);
        cell.setCellValue(MapUtils.getString(depositRefund,"activity_title",""));
        cell = row.createCell((short) 2);
        cell.setCellValue(MapUtils.getString(depositRefund,"episode_title",""));
        cell = row.createCell((short) 3);
        cell.setCellValue(MapUtils.getString(depositRefund,"charge_name",""));
        cell = row.createCell((short) 4);
        cell.setCellValue(MapUtils.getDoubleValue(depositRefund,"refund_amount",0));
        cell = row.createCell((short) 5);
        cell.setCellValue(MapUtils.getString(depositRefund,"time"));
        cell = row.createCell((short) 6);
        cell.setCellValue(FinanceExcelUtil.getPayName(MapUtils.getInteger(depositRefund,"pay_type_id")));
    }

    /**
     * 押金退款头
     * @param wb
     * @param sheet
     * @param rowNum
     */
    private void creaDepositRefundHead(HSSFWorkbook wb,HSSFSheet sheet,int rowNum){
        HSSFCellStyle style = wb.createCellStyle(); // 样式对象
        HSSFFont font = wb.createFont();// 生成一个字体
        font.setColor(HSSFColor.GREEN.index);// HSSFColor.VIOLET.index // 字体颜色
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);// 字体增粗
        style.setFont(font);// 把字体应用到当前的样式
        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = row.createCell((short) 0);
        cell.setCellValue("订单号");
        cell.setCellStyle(style);
        cell = row.createCell((short) 1);
        cell.setCellValue("产品");
        cell.setCellStyle(style);
        cell = row.createCell((short) 2);
        cell.setCellValue("场次");
        cell.setCellStyle(style);
        cell = row.createCell((short) 3);
        cell.setCellValue("票券名");
        cell.setCellStyle(style);
        cell = row.createCell((short) 4);
        cell.setCellValue("押金支出");
        cell.setCellStyle(style);
        cell = row.createCell((short) 5);
        cell.setCellValue("支出时间");
        cell.setCellStyle(style);
        cell = row.createCell((short) 6);
        cell.setCellValue("支付方式");
        cell.setCellStyle(style);
    }

    /**
     * 订单Cell
     * @param orderType
     * @param wb
     * @param sheet
     * @param rowNum
     * @param orders
     * @param ptotalNum 产品总数
     */
    private void createOrderDetailCell(String orderType,HSSFWorkbook wb,HSSFSheet sheet,int rowNum,Orders orders,Integer ptotalNum,HSSFCellStyle styleMain){
        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = row.createCell((short) 0);
        cell.setCellValue(orderType);
        cell.setCellStyle(styleMain);
        cell = row.createCell((short) 1);
        cell.setCellValue(orders.getCode());
        cell = row.createCell((short) 2);
        cell.setCellValue(orders.getActivityName());
        cell = row.createCell((short) 3);
        cell.setCellValue(orders.getEpisodeName());
        cell = row.createCell((short) 4);
        cell.setCellValue(orders.getChargeName());
        cell = row.createCell((short) 6);
        BigDecimal deposit = orders.getDeposit();
        double productMoney = 0;
        if(deposit == null){
            cell.setCellValue(0.00);
            deposit =BigDecimal.ZERO;
            productMoney = orders.getTotalPrice().doubleValue();
        }else{
            cell.setCellValue(deposit.doubleValue());
            if(orders.getParentId()==null){
                productMoney = orders.getTotalPrice().doubleValue()-deposit.doubleValue();
            }else{
                productMoney = orders.getTotalPrice().doubleValue();
            }

        }
        cell = row.createCell((short) 7);
        cell.setCellValue(productMoney);
        cell = row.createCell((short) 5);
        BigDecimal orderPrice = deposit.add(new BigDecimal(productMoney)).setScale(2,RoundingMode.HALF_DOWN);
        cell.setCellValue(orderPrice.doubleValue());

        if(orders.getPayTime()!=null){
            cell = row.createCell((short) 8);
            cell.setCellValue(DateUtil.convertDateToString(orders.getPayTime(),"yyyy-MM-dd HH:mm:ss"));
        }
        cell = row.createCell((short) 9);
        /*double cashMoney = 0.00;
        double zhifubaoMoney = 0.00;
        double paybyCard = 0.00;
        double weixin = 0.00;
        double preSale = 0.00;
        double hungup = 0.00;
        double entertain = 0.00;
        double sweepCode = 0.00;*/
        if(orders.getPayTypeId()!=null){
            cell.setCellValue(FinanceExcelUtil.getPayName(orders.getPayTypeId().intValue()));
            /*switch (orders.getPayTypeId().intValue()){
                case 0:
                    //cell.setCellValue("线下现金");
                    cashMoney = orders.getTotalPrice().doubleValue();
                    break;
                case 2:
                    //cell.setCellValue("线下支付宝");
                    zhifubaoMoney = orders.getTotalPrice().doubleValue();
                    break;
                case 3:
                    //cell.setCellValue("线下刷卡");
                    paybyCard = orders.getTotalPrice().doubleValue();
                    break;
                case 4:
                    //cell.setCellValue("线下微信");
                    weixin = orders.getTotalPrice().doubleValue();
                    break;
                case 8:
                    //cell.setCellValue("储值支付");
                    preSale = orders.getTotalPrice().doubleValue();
                    break;
                case 9:
                    //cell.setCellValue("线下挂账");
                    hungup = orders.getTotalPrice().doubleValue();
                    break;
                case 10:
                    //cell.setCellValue("招待支付");
                    entertain =orders.getTotalPrice().doubleValue();
                    break;
                case 13:
                    //cell.setCellValue("招待支付");
                    sweepCode =orders.getTotalPrice().doubleValue();
                    break;
            }*/
        }else{
            cell.setCellValue("");
        }


        cell = row.createCell((short) 10);
        if(ptotalNum == null){
            ptotalNum = 0;
        }
        cell.setCellValue(ptotalNum);


        int index = 11;
        ActivityBeanUtils.OfflinePayNameEnum[] values = ActivityBeanUtils.OfflinePayNameEnum.values();
        for (ActivityBeanUtils.OfflinePayNameEnum v : values){
            cell = row.createCell((short) index);
            double money = 0.00;
            if(orders.getPayTypeId().intValue() == v.getValue()){
                money = orders.getTotalPrice().doubleValue();
            }
            cell.setCellValue(new BigDecimal(money).setScale(2,RoundingMode.HALF_DOWN).doubleValue());
            index++;
        }

        /*cell = row.createCell((short) 11);
        cell.setCellValue(new BigDecimal(cashMoney).setScale(2,RoundingMode.HALF_DOWN).doubleValue());

        cell = row.createCell((short) 14);
        cell.setCellValue(new BigDecimal(zhifubaoMoney).setScale(2,RoundingMode.HALF_DOWN).doubleValue());

        cell = row.createCell((short) 12);
        cell.setCellValue(new BigDecimal(paybyCard).setScale(2,RoundingMode.HALF_DOWN).doubleValue());

        cell = row.createCell((short) 13);
        cell.setCellValue(new BigDecimal(weixin).setScale(2,RoundingMode.HALF_DOWN).doubleValue());

        cell = row.createCell((short) 15);
        cell.setCellValue(new BigDecimal(preSale).setScale(2,RoundingMode.HALF_DOWN).doubleValue());

        cell = row.createCell((short) 16);
        cell.setCellValue(new BigDecimal(hungup).setScale(2,RoundingMode.HALF_DOWN).doubleValue());

        cell = row.createCell((short) 17);
        cell.setCellValue(new BigDecimal(entertain).setScale(2,RoundingMode.HALF_DOWN).doubleValue());

        cell = row.createCell((short) 18);
        cell.setCellValue(new BigDecimal(sweepCode).setScale(2,RoundingMode.HALF_DOWN).doubleValue());*/



    }

/** 0：线下现金
* 1：线上
* 2：线下支付宝
* 3：线下刷卡
* 4：线下微信
* 5：线下预售
* 6：线下挂账
* 7：线上储值支付
* 8：线下储值支付
* 999：混合支付
* 9：线下挂账支付
* 10：线下招待支付*/

    /**
     * 创建表头
     * @param wb
     * @param sheet
     * @param rowNum
     * @return
     */
    private void creaOrderDetailHead(HSSFWorkbook wb,HSSFSheet sheet,int rowNum,HSSFCellStyle style){
        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = row.createCell((short) 0);
        cell.setCellValue("订单类型");
        cell.setCellStyle(style);
        cell = row.createCell((short) 1);
        cell.setCellValue("订单号");
        cell.setCellStyle(style);
        cell = row.createCell((short) 2);
        cell.setCellValue("产品");
        cell.setCellStyle(style);
        cell = row.createCell((short) 3);
        cell.setCellValue("场次");
        cell.setCellStyle(style);
        cell = row.createCell((short) 4);
        cell.setCellValue("票券名");
        cell.setCellStyle(style);
        cell = row.createCell((short) 5);
        cell.setCellValue("订单总价");
        cell.setCellStyle(style);
        cell = row.createCell((short) 6);
        cell.setCellValue("押金收入");
        cell.setCellStyle(style);
        cell = row.createCell((short) 7);
        cell.setCellValue("产品金额");
        cell.setCellStyle(style);
        cell = row.createCell((short) 8);
        cell.setCellValue("支付时间");
        cell.setCellStyle(style);
        cell = row.createCell((short) 9);
        cell.setCellValue("支付方式");
        cell.setCellStyle(style);
        cell = row.createCell((short) 10);
        cell.setCellValue("产品数量");
        cell.setCellStyle(style);

        int index = 11;
        ActivityBeanUtils.OfflinePayNameEnum[] values = ActivityBeanUtils.OfflinePayNameEnum.values();
        for (ActivityBeanUtils.OfflinePayNameEnum v : values){
            cell = row.createCell((short) index);
            cell.setCellValue(v.getName());
            cell.setCellStyle(style);
            index++;
        }
        /*cell = row.createCell((short) 11);
        cell.setCellValue("现金");
        cell.setCellStyle(style);
        cell = row.createCell((short) 12);
        cell.setCellValue("银行卡");
        cell.setCellStyle(style);
        cell = row.createCell((short) 13);
        cell.setCellValue("微信");
        cell.setCellStyle(style);
        cell = row.createCell((short) 14);
        cell.setCellValue("支付宝");
        cell.setCellStyle(style);
        cell = row.createCell((short) 15);
        cell.setCellValue("储值支付");
        cell.setCellStyle(style);
        cell = row.createCell((short) 16);
        cell.setCellValue("挂账");
        cell.setCellStyle(style);
        cell = row.createCell((short) 17);
        cell.setCellValue("招待");
        cell.setCellStyle(style);
        cell = row.createCell((short) 18);
        cell.setCellValue("扫码支付");
        cell.setCellStyle(style);*/
    }

    /**
     * 按产品场次进行分类
     * @param parentOrders
     * @return
     */
    private Map<String,List<Orders>> getOrdersByProductEpisode(List<Orders> parentOrders){
        Map<String,List<Orders>> map = new HashMap<String,List<Orders>>();

        if(parentOrders!=null){
            for (Orders orders : parentOrders){
                String key = "产品:"+orders.getActivityName()+"|场次:" + orders.getEpisodeName();
                List<Orders> pOrdlis = map.get(key);
                if(pOrdlis==null){
                    pOrdlis = new ArrayList<Orders>();
                    map.put(key,pOrdlis);
                }
                pOrdlis.add(orders);
            }
        }

        return map;
    }

    @ApiOperation(value = "操作员结算",notes = "支付类型，0：线下现金，1：线上， 2：线下支付宝， 3：线下刷卡， 4：线下微信，5：线下预售，6：线下挂账，7：线上储值支付，8：线下储值支付，999：混合支付，9：下线挂账支付，10：挂账")
    @RequestMapping(value = "/settlement", method = RequestMethod.GET)
    @ResponseBody
    public WebResult settlement(HttpServletRequest request) {
        CommonAccount commonAccount = WebUpmsContext.getAccount(request);
        //WebResult result = preview(request);

        //Map<String,BigDecimal>  orderTotal = (Map<String,BigDecimal>)result.get("orderTotal");
        //Map<String,BigDecimal>  depositOrder = (Map<String,BigDecimal>)result.get("depositTotal");

        //BigDecimal total = orderTotal.get("total").add(depositOrder.get("total"));

        Date date = new Date();
        ordersService.orderCashierSettment(date,commonAccount.getZooOperatorLoginId());

        ZooBalanceDepositIndemnityExample example = new ZooBalanceDepositIndemnityExample();
        example.createCriteria().andZooOperatorLoginIdEqualTo(commonAccount.getZooOperatorLoginId());
        ZooBalanceDepositIndemnity depositIndemnity = new ZooBalanceDepositIndemnity();
        depositIndemnity.setOperatorSettlementTime(date);
        zooBalanceDepositIndemnityService.updateByExampleSelective(depositIndemnity,example);

        //BigDecimal refundPrice = ordersService.selectRefundTotalPriceByOrgnizerAndCashierId(null, null, null, null, null, null, null, commonAccount.getZooOperatorLoginId());

        WebResult preview = previewPrint(request,commonAccount.getZooOperatorLoginId());
        BigDecimal totalActual = (BigDecimal)preview.get("totalActual");

        zooOperatorLoginService.settlement(date,commonAccount.getCustomerId(),totalActual);

        return WebResult.getSuccessResult();
    }

    @ApiOperation(value = "操作员结算预览",notes = "支付类型，0：线下现金，1：线上， 2：线下支付宝， 3：线下刷卡， 4：线下微信，5：线下预售，6：线下挂账，7：线上储值支付，8：线下储值支付，999：混合支付，9：下线挂账支付，10：挂账")
    @ApiImplicitParam(name = "id", value = "登录日志ID", required = false, dataType = "int", paramType = "query")
    @RequestMapping(value = "/previewPrint", method = RequestMethod.GET)
    @ResponseBody
    public WebResult previewPrint(HttpServletRequest request,Long id) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);

        WebResult result = WebResult.getSuccessResult();

        CommonAccount commonAccount = WebUpmsContext.getAccount(request);

        zooOperatorOrderStattisticsService.statistics(id);

        //押金总收入
        BigDecimal totalInDeposit = BigDecimal.ZERO;
        //押金总支出
        BigDecimal totalOutDeposit = BigDecimal.ZERO;
        //订单总金额
        BigDecimal totalOrderPrice = BigDecimal.ZERO;
        //储值支付
        BigDecimal totalStoragePayPrice = BigDecimal.ZERO;
        //退款总金额
        BigDecimal totalRefundPrice = BigDecimal.ZERO;
        //赔偿总金额
        BigDecimal totalIndemnityPrice = BigDecimal.ZERO;

        //支付方式
        Map<Integer,BigDecimal> payMap = new HashMap<Integer, BigDecimal>();

        //产品
        Map<Integer,Map<String,Object>> orderProduct = new HashMap<Integer, Map<String, Object>>();

        //异常退款产品
        Map<Integer,Map<String,Object>> refundProduct = new HashMap<Integer, Map<String, Object>>();

        //赔偿
        Map<Integer,Map<String,Object>> indemnityProduct = new HashMap<Integer, Map<String, Object>>();

        //异常退款订单
        List<Orders> abnormalOrderslist = ordersService.getAbnormalOrdersByCashier(null, null, null, null, null, null, organizer.getOrganizerId(),id);

        //异常退款订单
        for (Orders orders : abnormalOrderslist) {
            Map<Integer,Map<String,Object>> product = null;
            if(orders.getExtAbnormalRefundMoney()!=null){
                product = refundProduct;
            }else{
                continue;
            }

            /*if(orders.getParentId()!=null && orders.getParentId()!=0){
                continue;
            }*/

            Integer pId = orders.getActivityId();
            if(orders.getEquipmentId()!=null){
                pId = new Integer(orders.getEquipmentId());
            }
            Map<String, Object> p = product.get(pId);

            if(p == null){
                p = new HashMap<String,Object>();
                product.put(pId,p);
                p.put("money",BigDecimal.ZERO);
                if(orders.getEquipmentId()==null){
                    p.put("name",orders.getActivityName());
                }else{
                    p.put("name",orders.getEquipmentName());
                }
            }
            int num = MapUtils.getIntValue(p,"num",0);
            p.put("num",num + 1);
            BigDecimal money = (BigDecimal)p.get("money");
            p.put("money",money.add(orders.getExtAbnormalRefundMoney()).setScale(2,RoundingMode.HALF_UP));

            if(orders.getExtAbnormalRefundMoney()!=null){
                totalRefundPrice = totalRefundPrice.add(orders.getExtAbnormalRefundMoney()).setScale(2,RoundingMode.HALF_UP);
            }
        }

        //订单数据
        List<Orders> orderslist = ordersService.getOrdersByCashier(null, null, null, null, null, null, null,id);

        for (Orders orders : orderslist) {

            Map<Integer,Map<String,Object>> product = orderProduct;

            if(orders.getParentId()==null){
                continue;
            }

            Integer pId = orders.getActivityId();
            if(orders.getEquipmentId()!=null){
                pId = new Integer(orders.getEquipmentId());
            }
            Map<String, Object> p = product.get(pId);

            if(p == null){
                p = new HashMap<String,Object>();
                product.put(pId,p);
                p.put("money",BigDecimal.ZERO);
                if(orders.getEquipmentId()==null){
                    p.put("name",orders.getActivityName());
                }else{
                    p.put("name",orders.getEquipmentName());
                }
            }
            int num = MapUtils.getIntValue(p,"num",0);
            p.put("num",num+orders.getChargeNum());
            BigDecimal money = (BigDecimal)p.get("money");
            p.put("money",money.add(orders.getTotalPrice()).setScale(2,RoundingMode.HALF_UP));

            BigDecimal orderTotalPrice = orders.getTotalPrice();
            if(orders.getParentId().intValue() == 0 && orders.getDeposit()!=null){
                //如果父订单，减去押金金额
                orderTotalPrice = orderTotalPrice.subtract(orders.getDeposit());
            }
            totalOrderPrice = totalOrderPrice.add(orderTotalPrice).setScale(2,RoundingMode.HALF_UP);

            /*if(orders.getExtAbnormalRefundMoney()!=null){
                totalRefundPrice = totalRefundPrice.add(orders.getTotalPrice()).setScale(2,RoundingMode.HALF_UP);
            }*/

            //储值支付累计金额
            if(orders.getPayTypeId().intValue()==8 || orders.getPayTypeId().intValue()==7){
                totalStoragePayPrice = totalStoragePayPrice.add(orders.getTotalPrice());
            }

            BigDecimal deposit = BigDecimal.ZERO;
            if(orders.getParentId()!=null && orders.getParentId().intValue()==0){
                deposit = orders.getDeposit();
            }
            if(deposit==null){
                deposit = BigDecimal.ZERO;
            }

            //支付统计
            BigDecimal payPrice = payMap.get(orders.getPayTypeId());
            if(payPrice == null){
                payMap.put(orders.getPayTypeId(),orders.getTotalPrice().add(deposit).setScale(2,RoundingMode.HALF_UP));
            }else{
                payMap.put(orders.getPayTypeId(),payPrice.add(orders.getTotalPrice()).add(deposit).setScale(2,RoundingMode.HALF_UP));
            }
        }

        //赔偿
        ZooBalanceDepositIndemnityExample indemnityExample = new ZooBalanceDepositIndemnityExample();
        ZooBalanceDepositIndemnityExample.Criteria indemnityExampleCriteria = indemnityExample.createCriteria();
        if(id != null){
            indemnityExampleCriteria.andZooOperatorLoginIdEqualTo(id);
        }else{
            indemnityExampleCriteria.andOperatorSettlementTimeIsNull();
        }
        List<ZooBalanceDepositIndemnity> depositIndemnitieList = zooBalanceDepositIndemnityService.selectByExample(indemnityExample);
        for (ZooBalanceDepositIndemnity depositIndemnity : depositIndemnitieList) {
            Integer equipmentId = depositIndemnity.getEquipmentId();
            Map<String, Object> p = indemnityProduct.get(equipmentId);

            if(p == null){
                p = new HashMap<String,Object>();
                indemnityProduct.put(equipmentId,p);
                p.put("money",BigDecimal.ZERO);
                p.put("name",depositIndemnity.getEquipmentName());
            }
            int num = MapUtils.getIntValue(p,"num",0);
            p.put("num",num+1);
            BigDecimal money = (BigDecimal)p.get("money");
            p.put("money",money.add(depositIndemnity.getIndemnity()).setScale(2,RoundingMode.HALF_UP));
            totalIndemnityPrice = totalIndemnityPrice.add(depositIndemnity.getIndemnity()).setScale(2,RoundingMode.HALF_UP);
        }

        //押金
        ZooOperatorDepositExample depositExample = new ZooOperatorDepositExample();
        ZooOperatorDepositExample.Criteria depositExampleCriteria = depositExample.createCriteria();
        depositExampleCriteria.andOperatorIdEqualTo(commonAccount.getCustomerId());
        depositExampleCriteria.andPayTypeNotEqualTo(1);
        if(id!=null){
            depositExampleCriteria.andZooOperatorLoginIdEqualTo(id);
        }else{
            depositExampleCriteria.andStatusEqualTo(1);
        }
        List<ZooOperatorDeposit> zooOperatorDeposits = zooOperatorDepositService.selectByExample(depositExample);
        for (ZooOperatorDeposit deposit : zooOperatorDeposits) {
            //类型：1、押金进账，2、押金退款
            if(deposit.getType() == 1){
                totalInDeposit = totalInDeposit.add(deposit.getDeposit());
            }else{
                totalOutDeposit = totalOutDeposit.add(deposit.getDeposit());
            }
        }

        //时限卡换卡
        //时限卡赔偿
        List<LimitCardIndemnity> limitCardIndemnityList = limitCardService.getLimitCardIndemnityList(null, null, id);
        BigDecimal limitCardIndemnityTotalMoney = BigDecimal.ZERO;
        if(limitCardIndemnityList != null){
            int size = limitCardIndemnityList.size();
            for (int i = 0 ; i < size ; i++) {
                limitCardIndemnityTotalMoney = limitCardIndemnityTotalMoney.add(limitCardIndemnityList.get(i).getAmount());
            }
        }

        result.put("product",orderProduct.values());
        result.put("refundProduct",refundProduct.values());
        result.put("indemnityProduct",indemnityProduct.values());
        result.put("payMap",payMap);
        result.put("totalInDeposit",totalInDeposit);
        result.put("totalOutDeposit",totalOutDeposit);
        BigDecimal deposit = totalInDeposit.subtract(totalOutDeposit).setScale(2,RoundingMode.HALF_UP);
        result.put("totalDeposit",deposit);
        result.put("totalRefundPrice",totalRefundPrice);
        result.put("totalIndemnityPrice",totalIndemnityPrice);
        result.put("totalOrderPrice",totalOrderPrice);
        result.put("limitCardIndemnityTotalMoney",limitCardIndemnityTotalMoney);
        totalOrderPrice = totalOrderPrice.subtract(totalRefundPrice).setScale(2,RoundingMode.HALF_UP);
        //应收金额
        result.put("total",totalOrderPrice);
        //实收金额
        result.put("totalActual",totalOrderPrice.add(deposit).add(limitCardIndemnityTotalMoney).subtract(totalStoragePayPrice).setScale(2,RoundingMode.HALF_UP));

        ZooOperatorLoginExample accountExample = new ZooOperatorLoginExample();
        ZooOperatorLoginExample.Criteria accountExampleCriteria = accountExample.createCriteria();
        accountExampleCriteria.andOperatorIdEqualTo(commonAccount.getCustomerId());
        if(id!=null){
            accountExampleCriteria.andIdEqualTo(id);
        }else{
            accountExampleCriteria.andEndTimeIsNull();
        }

        ZooOperatorLogin login = zooOperatorLoginService.selectFirstByExample(accountExample);
        if(login != null){
            result.put("startTime",DateUtil.convertDateToString(login.getStartTime(),"yyyy-MM-dd HH:mm:ss"));
            Date end = login.getEndTime();
            if(end == null){
                end = new Date();
            }
            result.put("endTime",DateUtil.convertDateToString(end,"yyyy-MM-dd HH:mm:ss"));
        }
        result.put("operatorName",login.getOperatorName());

        return result;
    }

    @ApiOperation(value = "操作员结算预览",notes = "支付类型，0：线下现金，1：线上， 2：线下支付宝， 3：线下刷卡， 4：线下微信，5：线下预售，6：线下挂账，7：线上储值支付，8：线下储值支付，999：混合支付，9：下线挂账支付，10：挂账")
    @ApiImplicitParam(name = "id", value = "登录日志ID", required = false, dataType = "int", paramType = "query")
    @RequestMapping(value = "/preview", method = RequestMethod.GET)
    @ResponseBody
    public WebResult preview(HttpServletRequest request,Long id) {

        CommonAccount commonAccount = WebUpmsContext.getAccount(request);

        zooOperatorOrderStattisticsService.statistics(id);

        //押金总收入
        BigDecimal totalInDeposit = BigDecimal.ZERO;
        //押金总支出
        BigDecimal totalOutDeposit = BigDecimal.ZERO;
        //订单总金额
        BigDecimal totalOrderPrice = BigDecimal.ZERO;
        //退款总金额
        BigDecimal totalRefundPrice = BigDecimal.ZERO;

        //合计金额
        Map<String,BigDecimal> totalOrder = new LinkedHashMap<String,BigDecimal>();
        Map<String,BigDecimal> depositOrder = new LinkedHashMap<String,BigDecimal>();

        //支付方式列表
        Set<String> payTypeList = new TreeSet<String>();

        //订单
        ZooOperatorOrderStattisticsExample example = new ZooOperatorOrderStattisticsExample();
        ZooOperatorOrderStattisticsExample.Criteria criteria = example.createCriteria();
        if(id!=null){
            criteria.andZooOperatorLoginIdEqualTo(id);
        }else{
            criteria.andStatusEqualTo(1);
        }
        criteria.andOperatorIdEqualTo(commonAccount.getCustomerId());
        example.setOrderByClause("activity_type_id desc");
        List<ZooOperatorOrderStattistics> list = zooOperatorOrderStattisticsService.selectByExample(example);

        Map<String,Map<String,BigDecimal>> listResult = new LinkedHashMap<String,Map<String,BigDecimal>>();
        if(list != null && list.size()>0){
            for (ZooOperatorOrderStattistics stattistics : list){
                payTypeList.add(stattistics.getPayType().toString());
                Map<String,BigDecimal> l = listResult.get(stattistics.getActivityTypeId().toString());
                if(l == null){
                    l = new LinkedHashMap<String,BigDecimal>();
                    l.put(stattistics.getPayType().toString(),stattistics.getTotalPrice());
                    listResult.put(stattistics.getActivityTypeId().toString(),l);
                }else{
                    BigDecimal bigDecimal = l.get(stattistics.getPayType().toString());
                    if(bigDecimal == null){
                        bigDecimal = BigDecimal.ZERO;
                    }
                    BigDecimal add = bigDecimal.add(stattistics.getTotalPrice());
                    l.put(stattistics.getPayType().toString(),add);
                }

                BigDecimal bigDecimal = totalOrder.get(stattistics.getPayType().toString());
                if(bigDecimal == null){
                    bigDecimal = BigDecimal.ZERO;
                    totalOrder.put(stattistics.getPayType().toString(),stattistics.getTotalPrice());
                }else{
                    totalOrder.put(stattistics.getPayType().toString(),bigDecimal.add(stattistics.getTotalPrice()));
                }
                totalOrderPrice = totalOrderPrice.add(stattistics.getTotalPrice());

            }
        }

        //押金
        ZooOperatorDepositExample depositExample = new ZooOperatorDepositExample();
        ZooOperatorDepositExample.Criteria depositExampleCriteria = depositExample.createCriteria();
        depositExampleCriteria.andOperatorIdEqualTo(commonAccount.getCustomerId());
        if(id!=null){
            depositExampleCriteria.andZooOperatorLoginIdEqualTo(id);
        }else{
            depositExampleCriteria.andStatusEqualTo(1);
        }
        List<ZooOperatorDeposit> zooOperatorDeposits = zooOperatorDepositService.selectByExample(depositExample);

        Map<String,Map<String,BigDecimal>> depositsResult = new LinkedHashMap<String,Map<String,BigDecimal>>();
        depositsResult.put("inDeposit",new LinkedHashMap<String,BigDecimal>());
        depositsResult.put("outDeposit",new LinkedHashMap<String,BigDecimal>());

        if(zooOperatorDeposits != null && zooOperatorDeposits.size()>0){
            for (ZooOperatorDeposit deposit : zooOperatorDeposits){
                //类型：1、押金进账，2、押金退款
                payTypeList.add(deposit.getPayType().toString());
                BigDecimal bigDecimal = depositOrder.get(deposit.getPayType().toString());
                if(bigDecimal == null){
                    if(deposit.getType() == 1){
                        depositOrder.put(deposit.getPayType().toString(),deposit.getDeposit());
                    }else{
                        depositOrder.put(deposit.getPayType().toString(),deposit.getDeposit().multiply(new BigDecimal(-1)));
                    }
                    //depositOrder.put(deposit.getPayType().toString(),deposit.getDeposit());
                }else{
                    if(deposit.getType() == 1){
                        depositOrder.put(deposit.getPayType().toString(),deposit.getDeposit().add(bigDecimal));
                    }else{
                        depositOrder.put(deposit.getPayType().toString(),bigDecimal.subtract(deposit.getDeposit()));
                    }
                    //depositOrder.put(deposit.getPayType().toString(),deposit.getDeposit().add(bigDecimal));
                }

                Map<String,BigDecimal> l = null;
                if(deposit.getType() == 1){
                    l = depositsResult.get("inDeposit");
                    if(l == null){
                        l = new LinkedHashMap<String,BigDecimal>();
                        depositsResult.put("inDeposit",l);
                    }
                }else{
                    l = depositsResult.get("outDeposit");
                    if(l == null){
                        l = new LinkedHashMap<String,BigDecimal>();
                        depositsResult.put("outDeposit",l);
                    }
                }
                BigDecimal dPrice = deposit.getDeposit();
                if(deposit.getType() == 2){
                    //dPrice = dPrice;
                }
                BigDecimal p = l.get(deposit.getPayType().toString());
                if(p == null){
                    p = BigDecimal.ZERO;
                }
                BigDecimal add = p.add(dPrice);
                l.put(deposit.getPayType().toString(),add);
                if(deposit.getType() == 1){
                    //押金收入
                    totalInDeposit = totalInDeposit.add(dPrice);
                }else{
                    totalOutDeposit = totalOutDeposit.add(dPrice);
                }
            }
        }

        //赔款
        Map<String,Map<String,BigDecimal>> depositsIndemnityResult = new LinkedHashMap<String,Map<String,BigDecimal>>();
        ZooBalanceDepositIndemnityExample indemnityExample = new ZooBalanceDepositIndemnityExample();
        ZooBalanceDepositIndemnityExample.Criteria indemnityExampleCriteria = indemnityExample.createCriteria();
        if(id != null){
            indemnityExampleCriteria.andZooOperatorLoginIdEqualTo(id);
        }else{
            indemnityExampleCriteria.andOperatorSettlementTimeIsNull();
        }
        List<Map> maps = zooBalanceDepositIndemnityService.selectStatisticGroupByPayTypeByExample(indemnityExample);
        LinkedHashMap<String,BigDecimal> indemnityMap = new LinkedHashMap<String,BigDecimal>();
        if(maps!=null && maps.size()>0){

            for (Map map : maps) {
                String payType = MapUtils.getString(map, "pay_type");
                payTypeList.add(payType);
                BigDecimal indmnityPrice = indemnityMap.get(payType);
                BigDecimal indemnity = new BigDecimal(MapUtils.getDouble(map, "indemnity", 0d));
                if(indmnityPrice != null){
                    indemnity = indemnity.add(indmnityPrice);
                }
                indemnityMap.put(payType,indemnity);
            }
        }
        depositsIndemnityResult.put("indemnity",indemnityMap);

        ZooOperatorLoginExample accountExample = new ZooOperatorLoginExample();
        ZooOperatorLoginExample.Criteria accountExampleCriteria = accountExample.createCriteria();
        accountExampleCriteria.andOperatorIdEqualTo(commonAccount.getCustomerId());
        if(id!=null){
            accountExampleCriteria.andIdEqualTo(id);
        }else{
            accountExampleCriteria.andEndTimeIsNull();
        }

        ZooOperatorLogin login = zooOperatorLoginService.selectFirstByExample(accountExample);

        //退款数据
        List<Map> refundslist = ordersService.selectRefundByOrgnizerAndCashierId(null, null, null, null, null, null, null,id);
        Map<String,Map<String,BigDecimal>> refundsResult = new LinkedHashMap<String,Map<String,BigDecimal>>();
        LinkedHashMap<String,BigDecimal> refundsMap = new LinkedHashMap<String,BigDecimal>();
        if(refundslist!=null){
            for (Map map : refundslist) {
                String payType = MapUtils.getString(map, "pay_type_id");
                payTypeList.add(payType);
                BigDecimal price = refundsMap.get(payType);
                if(price == null){
                    price = BigDecimal.ZERO;
                }
                BigDecimal refundPrice = new BigDecimal(MapUtils.getDouble(map, "price", 0d));
                if(refundPrice != null){
                    refundPrice = refundPrice.add(price);
                }
                refundsMap.put(payType,refundPrice);
                totalRefundPrice = totalRefundPrice.add(price);
            }
        }
        refundsResult.put("refunds",refundsMap);


        totalPriceCal(listResult);
        totalPriceCal(depositsResult);
        totalPriceCal(refundsResult);
        totalPriceCal(depositsIndemnityResult);

        TreeMap<String,String> typeMap = new TreeMap<String,String>();
        SortedArrayList<String> patyTypeArrayList = new SortedArrayList<String>();
        if(payTypeList!=null && payTypeList.size()>0){
            for(String pay : payTypeList){
                //0：线下现金，1：线上， 2：线下支付宝， 3：线下刷卡， 4：线下微信，5：线下预售，7：线上储值支付，8：线下储值支付，999：混合支付，9：下线挂账支付，10：招待
                String name = typeMap.put(pay,ActivityBeanUtils.getPayName(Integer.valueOf(pay)));
                typeMap.put(pay,name);
                patyTypeArrayList.add(pay);
            }
            patyTypeArrayList.sort(new Comparator<String>() {
                @Override
                public int compare(String o1, String o2) {
                    int i1 = new Integer(o1).intValue();
                    int i2 = new Integer(o2).intValue();
                    if(i1 > i2){
                        return 1;
                    }else if(i1 == i2){
                        return 0;
                    }else{
                        return -1;
                    }
                }
            });
        }

        Map<String,BigDecimal> totalPrice = new HashMap<String,BigDecimal>();
        totalInDeposit = totalInDeposit.setScale(2,RoundingMode.HALF_UP);
        totalOutDeposit = totalOutDeposit.setScale(2,RoundingMode.HALF_UP);
        totalRefundPrice = totalRefundPrice.setScale(2,RoundingMode.HALF_UP);
        totalOrderPrice = totalOrderPrice.setScale(2,RoundingMode.HALF_UP);
        totalPrice.put("inDeposit",totalInDeposit);
        totalPrice.put("outDeposit",totalOutDeposit);
        totalPrice.put("depositTotal",totalInDeposit.add(totalOutDeposit.multiply(new BigDecimal(-1))).setScale(2,RoundingMode.HALF_UP));
        totalPrice.put("refunds",totalRefundPrice);
        totalPrice.put("orderPrice",totalOrderPrice);
        totalPrice.put("total",totalOrderPrice.add(totalInDeposit).add(totalOutDeposit.multiply(new BigDecimal(-1))).add(totalRefundPrice.multiply(new BigDecimal(-1))).setScale(2,RoundingMode.HALF_UP));

        //异常退款金额
        BigDecimal extAbnormalRefundTotalPrice = ordersService.selectExtAbnormalRefundTotalPrice(null, null, null, null, null, null, id);

        WebResult result = WebResult.getSuccessResult();
        result.put("extAbnormalRefundTotalPrice",extAbnormalRefundTotalPrice);
        result.put("typeMap",typeMap);
        result.put("totalPrice",totalPrice);

        List<Map> order = new ArrayList<Map>();
        List<Map> deposit = new ArrayList<Map>();
        List<Map> indemnity = new ArrayList<Map>();
        List<Map> refunds = new ArrayList<Map>();
        createResultList(patyTypeArrayList,order,listResult);
        createResultList(patyTypeArrayList,deposit,depositsResult);
        createResultList(patyTypeArrayList,indemnity,depositsIndemnityResult);
        createResultList(patyTypeArrayList,refunds,refundsResult);
        //订单列表
        result.put("order",order);
        //押金列表
        result.put("deposit",deposit);
        //押金赔款
        result.put("indemnity",indemnity);
        //退款
        result.put("refunds",refunds);

        //总计
        Map<String, Object> depositTotal = createResultMap("deposit", patyTypeArrayList, depositOrder);
        Map<String, Object> orderTotal = createResultMap("order", patyTypeArrayList, totalOrder);
        result.put("depositTotal",depositTotal);
        result.put("orderTotal",orderTotal);
        if(login != null){
            result.put("startTime",DateUtil.convertDateToString(login.getStartTime(),"yyyy-MM-dd HH:mm:ss"));
            Date end = login.getEndTime();
            if(end == null){
                end = new Date();
            }
            result.put("endTime",DateUtil.convertDateToString(end,"yyyy-MM-dd HH:mm:ss"));
        }
        result.put("operatorName",login.getOperatorName());
        result.put("payTypeList",patyTypeArrayList);
        return result;
    }

    private void createResultList(List<String> payTypeList,List<Map> result,Map<String,Map<String,BigDecimal>> listResult){
        if(payTypeList!=null && payTypeList.size()>0){
            Iterator<String> iterator = listResult.keySet().iterator();
            while(iterator.hasNext()){
                String typeId = iterator.next();
                result.add(createResultMap(typeId,payTypeList,listResult.get(typeId)));
            }
        }
    }

    private Map<String,Object> createResultMap(String typeId,List<String> payTypeList,Map<String,BigDecimal> mapResult){
        Map<String,Object> item = new LinkedHashMap<String,Object>();
        item.put("typeId",typeId);

        List<Map> payValueList = new ArrayList<Map>();
        item.put("paylist",payValueList);
        BigDecimal total = BigDecimal.ZERO;
        for(String pay : payTypeList){
            Map<String,Object> pItem = new LinkedHashMap<String,Object>();
            payValueList.add(pItem);
            pItem.put("payType",pay);
            BigDecimal bigDecimal = mapResult.get(pay);
            if(bigDecimal == null){
                bigDecimal = BigDecimal.ZERO;
            }
            pItem.put("Money",bigDecimal);
            total = total.add(bigDecimal);
        }
        item.put("total",total);
        return item;
    }

    private BigDecimal mapTotalPriceCal(Map<String,BigDecimal> map){
        Iterator<BigDecimal> bigDecimalIterator = map.values().iterator();
        BigDecimal total = BigDecimal.ZERO;
        while (bigDecimalIterator.hasNext()){
            BigDecimal v = bigDecimalIterator.next();
            total.add(v);
        }
        return total;
    }

    private void totalPriceCal(Map<String,Map<String,BigDecimal>> listResult){
        Iterator<Map<String, BigDecimal>> iterator = listResult.values().iterator();
        while (iterator.hasNext()){
            Map<String, BigDecimal> next = iterator.next();
            next.put("total",mapTotalPriceCal(next));
        }
    }

    @ApiOperation(value = "操作员结算列表")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ResponseBody
    public WebResult list(HttpServletRequest request,
            @RequestParam(required = false, defaultValue = "0", value = "page") int page,
            @RequestParam(required = false, defaultValue = "10", value = "size") int size) {

        CommonAccount commonAccount = WebUpmsContext.getAccount(request);

        ZooOperatorLoginExample example = new ZooOperatorLoginExample();
        example.createCriteria().andOperatorIdEqualTo(commonAccount.getCustomerId());
        example.setOrderByClause("start_time desc");

        List<ZooOperatorLogin> list = zooOperatorLoginService.selectByExampleForStartPage(example, page, size);
        int count = zooOperatorLoginService.countByExample(example);
        WebResult result = WebResult.getSuccessResult();
        result.put("list",list);
        result.put("count",count);
        return result;
    }

}
