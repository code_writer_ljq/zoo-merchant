package com.zoo.zoomerchantadminweb.finance.controller.poi.util;

import com.zoo.activity.dao.model.Orders;
import com.zoo.bean.util.ActivityBeanUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.CellReference;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.util.CellRangeAddress;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

/**
 * 财务报表工具类
 */
public class FinanceExcelUtil {

    /**
     * 创建订单数量和金额统计（通用方法）
     * @param wb
     * @param sheet
     * @param rowNum
     * @param map
     * @return
     */
    public static int createOrderNumMoneyItem(HSSFWorkbook wb,HSSFSheet sheet,int rowNum,Map<String,Map<String,Map<String,Object>>> map){

        if(map==null){
            return rowNum;
        }

        int rowStartIndex = rowNum;

        Iterator<Map.Entry<String, Map<String, Map<String, Object>>>> iterator = map.entrySet().iterator();
        while (iterator.hasNext()){
            Map.Entry<String, Map<String, Map<String, Object>>> next = iterator.next();
            String key = next.getKey();
            Map<String, Map<String, Object>> value = next.getValue();

            int size = value.size();

            if(size>0){
                sheet.addMergedRegion(new CellRangeAddress(rowNum, rowNum-1+size, 1, 1));
            }

            Iterator<Map.Entry<String, Map<String, Object>>> iterator1 = value.entrySet().iterator();
            while (iterator1.hasNext()){
                Map.Entry<String, Map<String, Object>> next1 = iterator1.next();
                String name = next1.getKey();
                Map<String, Object> value1 = next1.getValue();
                Integer num = MapUtils.getInteger(value1, "num", 0);
                double money = MapUtils.getDoubleValue(value1, "money", 0);

                HSSFCellStyle cellStyle = getCellStyle(wb);

                HSSFRow row = sheet.createRow(rowNum);
                HSSFCell cell = row.createCell(1);
                cell.setCellValue(key);
                cell.setCellStyle(cellStyle);

                cell = row.createCell(2);
                cell.setCellValue(name);
                cell.setCellStyle(cellStyle);

                cell = row.createCell(3);
                cell.setCellValue(num);
                cell.setCellStyle(cellStyle);

                HSSFDataFormat df = wb.createDataFormat();
                cellStyle.setDataFormat(df.getBuiltinFormat("0.00"));
                cell = row.createCell(4);
                cell.setCellValue(money);
                cell.setCellStyle(cellStyle);
                rowNum++;
            }

        }

        HSSFCellStyle hjStyle = getFootCellStyle(wb);

        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = row.createCell(1);
        cell.setCellValue("合计");
        cell.setCellStyle(hjStyle);

        cell = row.createCell(2);
        cell.setCellValue("");
        cell.setCellStyle(hjStyle);

        HSSFDataFormat df = wb.createDataFormat();
        hjStyle.setDataFormat(df.getBuiltinFormat("0"));
        cell = row.createCell(3);
        String sc = CellReference.convertNumToColString(3);
        cell.setCellFormula("SUM(" + sc + rowStartIndex + ":"+sc+rowNum+")");
        cell.setCellStyle(hjStyle);

        hjStyle = getFootCellStyle(wb);
        df = wb.createDataFormat();
        hjStyle.setDataFormat(df.getBuiltinFormat("0.00"));
        cell = row.createCell(4);
        String pc = CellReference.convertNumToColString(4);
        cell.setCellFormula("SUM(" + pc + rowStartIndex + ":"+pc+rowNum+")");
        cell.setCellStyle(hjStyle);


        return rowNum;
    }

    /**
     * 创建汇总
     * @param wb
     * @param sheet
     * @param rowNum
     * @return
     */
    public static int createEndItem(HSSFWorkbook wb,HSSFSheet sheet,int rowNum,List<Map<String,Object>> allStastic){
        for (int i = 0; i < allStastic.size(); i++) {
            Map<String,Object> map=allStastic.get(i);
            HSSFCellStyle headStyle = getCellStyle(wb);
            HSSFDataFormat df = wb.createDataFormat();
            headStyle.setDataFormat(df.getBuiltinFormat("0.00"));

            HSSFRow row = sheet.createRow(rowNum);
            HSSFCell cell = row.createCell(1);
            cell.setCellValue(MapUtils.getString(map,"name"));
            cell.setCellStyle(headStyle);
            cell = row.createCell(2);
            BigDecimal yjsr=new BigDecimal(MapUtils.getDoubleValue(map,"money",0)).setScale(2,RoundingMode.HALF_UP);
            cell.setCellValue(yjsr.doubleValue());
            cell.setCellStyle(headStyle);
            rowNum++;
        }
        return rowNum;
    }

    /**
     * 支付方式汇总
     * @param wb
     * @param sheet
     * @param rowNum
     * @return
     */
    public static int createPayTypeStasticItem(HSSFWorkbook wb,HSSFSheet sheet,int rowNum,Map<Integer,Map<String,BigDecimal>> payTypeStastic){

        int rowStartIndex = rowNum;
        Iterator<Map.Entry<Integer,Map<String,BigDecimal>>> iterator = payTypeStastic.entrySet().iterator();
        while(iterator.hasNext()){
            Map.Entry<Integer, Map<String,BigDecimal>> next = iterator.next();
            Integer key = next.getKey();
            Map<String,BigDecimal> map = (Map<String,BigDecimal>)next.getValue();
            HSSFCellStyle headStyle = getCellStyle(wb);

            HSSFDataFormat df = wb.createDataFormat();
            headStyle.setDataFormat(df.getBuiltinFormat("0.00"));

            HSSFRow row = sheet.createRow(rowNum);
            HSSFCell cell = row.createCell(1);
            if(key!=null){
                cell.setCellValue(FinanceExcelUtil.getPayName(key));
            }else{
                cell.setCellValue("");
            }
            cell.setCellStyle(headStyle);
            cell = row.createCell(2);
            BigDecimal srje=new BigDecimal(MapUtils.getDoubleValue(map,"0",0)).setScale(2,RoundingMode.HALF_UP);
            cell.setCellValue(srje.doubleValue());
            cell.setCellStyle(headStyle);
            cell = row.createCell(3);
            BigDecimal zcje=new BigDecimal(MapUtils.getDoubleValue(map,"1",0)).setScale(2,RoundingMode.HALF_UP);
            cell.setCellValue(zcje.doubleValue());
            cell.setCellStyle(headStyle);
            cell = row.createCell(4);
            cell.setCellValue(srje.subtract(zcje).doubleValue());
            cell.setCellStyle(headStyle);
            rowNum++;
        }
        HSSFCellStyle hjStyle = getFootCellStyle(wb);

        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = row.createCell(1);
        cell.setCellValue("合计");
        cell.setCellStyle(hjStyle);

        HSSFDataFormat df = wb.createDataFormat();
        hjStyle.setDataFormat(df.getBuiltinFormat("0.00"));
        cell = row.createCell(2);
        String qc = CellReference.convertNumToColString(2);
        cell.setCellFormula("SUM(" + qc + rowStartIndex + ":"+qc+rowNum+")");
        cell.setCellValue("");
        cell.setCellStyle(hjStyle);
        cell = row.createCell(3);
        String sc = CellReference.convertNumToColString(3);
        cell.setCellFormula("SUM(" + sc + rowStartIndex + ":"+sc+rowNum+")");
        cell.setCellStyle(hjStyle);
        cell = row.createCell(4);
        String pc = CellReference.convertNumToColString(4);
        cell.setCellFormula("SUM(" + pc + rowStartIndex + ":"+pc+rowNum+")");
        cell.setCellStyle(hjStyle);
        return rowNum;
    }


    /**
     * 支付方式统计表头
     * @param wb
     * @param sheet
     * @param rowNum
     */
    public static void createPayTypeHead(HSSFWorkbook wb,HSSFSheet sheet,int rowNum){
        HSSFCellStyle style = wb.createCellStyle(); // 样式对象
        HSSFFont font = wb.createFont();// 生成一个字体
        font.setColor(HSSFColor.BLACK.index);// HSSFColor.VIOLET.index // 字体颜色
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);// 字体增粗
        style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        style.setFillForegroundColor(HSSFColor.GREY_40_PERCENT.index);
        style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        style.setBorderRight(HSSFCellStyle.BORDER_THIN);
        style.setBorderTop(HSSFCellStyle.BORDER_THIN);
        style.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);//垂直居中
        style.setFont(font);// 把字体应用到当前的样式
        //style.setFillBackgroundColor(HSSFColor.BLACK.index);
        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = row.createCell(1);
        cell.setCellValue("支付方式");
        cell.setCellStyle(style);
        cell = row.createCell(2);
        cell.setCellValue("收入金额");
        cell.setCellStyle(style);
        cell = row.createCell(3);
        cell.setCellValue("支出金额");
        cell.setCellStyle(style);
        cell = row.createCell(4);
        cell.setCellValue("应收金额");
        cell.setCellStyle(style);
    }

    public static String getPayName(Integer pay){
        return ActivityBeanUtils.getPayName(pay);
        /*if(pay == null){
            return "";
        }
        switch (pay.intValue()){
            case 0:
                return "线下现金";
            case 1:
                return "线上微信";
            case 2:
                return "线下支付宝";
            case 3:
                return "线下刷卡";
            case 4:
                return  "线下微信";
            case 5:
                return  "线下预售";
            case 6:
                return  "线下挂账";//已被废除
            case 7:
                return  "线上储值支付";
            case 8:
                return  "线下储值支付";
            case 9:
                return "线下挂账";
            case 10:
                return "招待支付";
            case 999:
                return "混合支付";
            case 11:
                return "线下免费";
            case 12:
                return "线上免费";
            case 13:
                return "扫码支付";
        }
        return "";*/
    }

    /**
     * 订单状态：0 未支付，1 可使用 ，2 退款中，3 已退款， 4 已关闭，5 已完成
     * @param status
     * @return
     */
    public static String getOrderStatus(Integer status){
        if(status == null){
            return "";
        }
        switch (status.intValue()){
            case 0:
                return "未支付";
            case 1:
                return "可使用";
            case 2:
                return "退款中";
            case 3:
                return "已退款";
            case 4:
                return  "已关闭";
            case 5:
                return  "已完成";
        }
        return "";
    }

    /**
     * 押金统计头
     * @param wb
     * @param sheet
     * @param rowNum
     */
    public static void createDepositStasticHead(HSSFWorkbook wb,HSSFSheet sheet,int rowNum){
        HSSFCellStyle style = wb.createCellStyle(); // 样式对象
        HSSFFont font = wb.createFont();// 生成一个字体
        font.setColor(HSSFColor.BLACK.index);// HSSFColor.VIOLET.index // 字体颜色
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);// 字体增粗
        style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        style.setFillForegroundColor(HSSFColor.GREY_40_PERCENT.index);
        style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        style.setBorderRight(HSSFCellStyle.BORDER_THIN);
        style.setBorderTop(HSSFCellStyle.BORDER_THIN);
        style.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);//垂直居中
        style.setFont(font);// 把字体应用到当前的样式
        //style.setFillBackgroundColor(HSSFColor.BLACK.index);
        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = row.createCell(1);
        cell.setCellValue("支付方式");
        cell.setCellStyle(style);
        cell = row.createCell(2);
        cell.setCellValue("押金收入");
        cell.setCellStyle(style);
        cell = row.createCell(3);
        cell.setCellValue("押金支出");
        cell.setCellStyle(style);
        cell = row.createCell(4);
        cell.setCellValue("押金合计");
        cell.setCellStyle(style);
    }

    /**
     * 创建押金支付汇总
     * @param wb
     * @param sheet
     * @param rowNum
     * @return
     */
    public static int createDepositStasticItem(HSSFWorkbook wb,HSSFSheet sheet,int rowNum,Map<Integer,Map<String,BigDecimal>> depositStastic){
        int rowStartIndex = rowNum;
        Iterator<Map.Entry<Integer,Map<String,BigDecimal>>> iterator = depositStastic.entrySet().iterator();
        while(iterator.hasNext()){
            Map.Entry<Integer, Map<String,BigDecimal>> next = iterator.next();
            Integer key = next.getKey();
            Map<String,BigDecimal> map = (Map<String,BigDecimal>)next.getValue();

            HSSFCellStyle headStyle = getCellStyle(wb);
            HSSFDataFormat df = wb.createDataFormat();
            headStyle.setDataFormat(df.getBuiltinFormat("0.00"));

            HSSFRow row = sheet.createRow(rowNum);
            HSSFCell cell = row.createCell(1);
            if(key!=null){
                cell.setCellValue(FinanceExcelUtil.getPayName(key));
            }else{
                cell.setCellValue("");
            }
            cell.setCellStyle(headStyle);
            cell = row.createCell(2);
            BigDecimal yjsr=new BigDecimal(MapUtils.getDoubleValue(map,"0",0)).setScale(2,RoundingMode.HALF_UP);
            cell.setCellValue(yjsr.doubleValue());
            cell.setCellStyle(headStyle);
            cell = row.createCell(3);
            BigDecimal yjzc=new BigDecimal(MapUtils.getDoubleValue(map,"1",0)).setScale(2,RoundingMode.HALF_UP);
            cell.setCellValue(yjzc.doubleValue());
            cell.setCellStyle(headStyle);
            cell = row.createCell(4);
            cell.setCellValue(yjsr.subtract(yjzc).doubleValue());
            cell.setCellStyle(headStyle);
            rowNum++;
        }
        HSSFCellStyle hjStyle = getFootCellStyle(wb);

        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = row.createCell(1);
        cell.setCellValue("合计");
        cell.setCellStyle(hjStyle);

        HSSFDataFormat df = wb.createDataFormat();
        hjStyle.setDataFormat(df.getBuiltinFormat("0.00"));
        cell = row.createCell(2);
        String qc = CellReference.convertNumToColString(2);
        cell.setCellFormula("SUM(" + qc + (rowStartIndex+1) + ":"+qc+rowNum+")");
        cell.setCellStyle(hjStyle);
        cell = row.createCell(3);
        String sc = CellReference.convertNumToColString(3);
        cell.setCellFormula("SUM(" + sc + (rowStartIndex+1) + ":"+sc+rowNum+")");
        cell.setCellStyle(hjStyle);
        cell = row.createCell(4);
        String pc = CellReference.convertNumToColString(4);
        cell.setCellFormula("SUM(" + pc + (rowStartIndex+1) + ":"+pc+rowNum+")");
        cell.setCellStyle(hjStyle);
        return rowNum;
    }

    /**
     * 招待项
     * @param wb
     * @param sheet
     * @param rowNum
     * @param entertainlist
     * @return
     */
    public static int createEntertainItem(HSSFWorkbook wb,HSSFSheet sheet,int rowNum,List<Map> entertainlist){

        int rowStartIndex = rowNum;
        Iterator<Map> iterator = entertainlist.iterator();
        while(iterator.hasNext()){
            Map entertain = iterator.next();
            sheet.addMergedRegion(new CellRangeAddress(rowStartIndex, rowNum, 1, 1));
            HSSFCellStyle headStyle = wb.createCellStyle(); // 样式对象
            headStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
            headStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
            headStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
            headStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
            headStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
            headStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);//垂直居中
            HSSFRow row = sheet.createRow(rowNum);
            HSSFCell cell = row.createCell(1);
            cell.setCellValue("招待");
            cell.setCellStyle(headStyle);
            cell = row.createCell(2);
            cell.setCellValue(MapUtils.getString(entertain,"company_name",""));
            cell.setCellStyle(headStyle);
            cell = row.createCell(3);
            cell.setCellValue(MapUtils.getIntValue(entertain,"num",0));
            cell.setCellStyle(headStyle);
            cell = row.createCell(4);
            cell.setCellValue(new BigDecimal(MapUtils.getDoubleValue(entertain,"money",0)).setScale(2,RoundingMode.HALF_UP).doubleValue());
            cell.setCellStyle(headStyle);
            rowNum++;
        }
        HSSFCellStyle hjStyle = getFootCellStyle(wb);

        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = row.createCell(1);
        cell.setCellValue("合计");
        cell.setCellStyle(hjStyle);
        cell = row.createCell(2);
        cell.setCellValue("");
        cell.setCellStyle(hjStyle);

        hjStyle = getFootCellStyle(wb);
        HSSFDataFormat df = wb.createDataFormat();
        hjStyle.setDataFormat(df.getBuiltinFormat("0"));
        cell = row.createCell(3);
        String sc = CellReference.convertNumToColString(3);
        cell.setCellStyle(hjStyle);
        cell.setCellFormula("SUM(" + sc + (rowStartIndex+1) + ":"+sc+rowNum+")");


        hjStyle = getFootCellStyle(wb);
        df = wb.createDataFormat();
        hjStyle.setDataFormat(df.getBuiltinFormat("0.00"));
        cell = row.createCell(4);
        String pc = CellReference.convertNumToColString(4);
        cell.setCellStyle(hjStyle);
        cell.setCellFormula("SUM(" + pc + (rowStartIndex+1) + ":"+pc+rowNum+")");

        return rowNum;
    }

    /**
     * 创建表头
     * @param wb
     * @param sheet
     * @param rowNum
     * @param head
     */
    public static void createExcellHead(HSSFWorkbook wb,HSSFSheet sheet,int rowNum,String[] head){
        HSSFCellStyle style = wb.createCellStyle(); // 样式对象
        HSSFFont font = wb.createFont();// 生成一个字体
        font.setColor(HSSFColor.BLACK.index);// HSSFColor.VIOLET.index // 字体颜色
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);// 字体增粗
        style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        style.setFillForegroundColor(HSSFColor.GREY_40_PERCENT.index);
        style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        style.setBorderRight(HSSFCellStyle.BORDER_THIN);
        style.setBorderTop(HSSFCellStyle.BORDER_THIN);
        style.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);//垂直居中
        style.setFont(font);// 把字体应用到当前的样式
        //style.setFillBackgroundColor(HSSFColor.BLACK.index);
        HSSFRow row = sheet.createRow(rowNum);
        int index = 1;
        for (String s : head) {
            HSSFCell cell = row.createCell(index);
            cell.setCellValue(s);
            cell.setCellStyle(style);
            sheet.setColumnWidth(index, 16 * 172);
            index++;
        }
    }

    /**
     * 挂账项
     * @param wb
     * @param sheet
     * @param rowNum
     * @param hungUpcashierlist
     * @return
     */
    public static int createHungUpItem(HSSFWorkbook wb,HSSFSheet sheet,int rowNum,List<Map> hungUpcashierlist){

        int rowStartIndex = rowNum;
        Iterator<Map> iterator = hungUpcashierlist.iterator();
        while(iterator.hasNext()){
            Map hungUp = iterator.next();
            sheet.addMergedRegion(new CellRangeAddress(rowStartIndex, rowNum, 1, 1));
            HSSFCellStyle headStyle = wb.createCellStyle(); // 样式对象
            headStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
            headStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
            headStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
            headStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
            headStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
            headStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);//垂直居中
            HSSFRow row = sheet.createRow(rowNum);
            HSSFCell cell = row.createCell(1);
            cell.setCellValue("挂账");
            cell.setCellStyle(headStyle);
            cell = row.createCell(2);
            cell.setCellValue(MapUtils.getString(hungUp,"company_name",""));
            cell.setCellStyle(headStyle);
            cell = row.createCell(3);
            cell.setCellValue(MapUtils.getIntValue(hungUp,"num",0));
            cell.setCellStyle(headStyle);
            cell = row.createCell(4);
            cell.setCellValue(new BigDecimal(MapUtils.getDoubleValue(hungUp,"money",0)).setScale(2,RoundingMode.HALF_UP).doubleValue());
            cell.setCellStyle(headStyle);
            rowNum++;
        }
        HSSFCellStyle hjStyle = getFootCellStyle(wb);

        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = row.createCell(1);
        cell.setCellValue("合计");
        cell.setCellStyle(hjStyle);
        cell = row.createCell(2);
        cell.setCellValue("");
        cell.setCellStyle(hjStyle);

        hjStyle = getFootCellStyle(wb);
        HSSFDataFormat df = wb.createDataFormat();
        hjStyle.setDataFormat(df.getBuiltinFormat("0"));

        cell = row.createCell(3);
        String sc = CellReference.convertNumToColString(3);
        cell.setCellStyle(hjStyle);
        cell.setCellFormula("SUM(" + sc + (rowStartIndex+1) + ":"+sc+rowNum+")");

        hjStyle = getFootCellStyle(wb);
        df = wb.createDataFormat();
        hjStyle.setDataFormat(df.getBuiltinFormat("0.00"));
        cell = row.createCell(4);
        String pc = CellReference.convertNumToColString(4);
        cell.setCellStyle(hjStyle);
        cell.setCellFormula("SUM(" + pc + (rowStartIndex+1) + ":"+pc+rowNum+")");

        return rowNum;
    }

    /**
     * 创建招待
     * @param wb
     * @param sheet
     * @param rowNum
     * @return
     */
    public static int createentertainItem(HSSFWorkbook wb,HSSFSheet sheet,int rowNum,Map<String,Map<String,Object>> hungUpMap){

        int rowStartIndex = rowNum;
        Iterator<Map.Entry<String,Map<String,Object>>> iterator = hungUpMap.entrySet().iterator();
        while(iterator.hasNext()){
            Map.Entry<String, Map<String,Object>> next = iterator.next();
            String key = next.getKey();
            Map<String,Object> map = (Map<String,Object>)next.getValue();
            sheet.addMergedRegion(new CellRangeAddress(rowStartIndex, rowNum, 1, 1));
            HSSFCellStyle headStyle = getCellStyle(wb);
            HSSFRow row = sheet.createRow(rowNum);
            HSSFCell cell = row.createCell(1);
            cell.setCellValue("招待");
            cell.setCellStyle(headStyle);
            cell = row.createCell(2);
            cell.setCellValue(MapUtils.getString(map,"name",""));
            cell.setCellStyle(headStyle);
            cell = row.createCell(3);
            cell.setCellValue(MapUtils.getIntValue(map,"num",0));
            cell.setCellStyle(headStyle);
            cell = row.createCell(4);
            cell.setCellValue(new BigDecimal(MapUtils.getDoubleValue(map,"money",0)).setScale(2,RoundingMode.HALF_UP).doubleValue());
            cell.setCellStyle(headStyle);
            rowNum++;
        }

        HSSFCellStyle hjStyle = getFootCellStyle(wb);

        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = row.createCell(1);
        cell.setCellValue("合计");
        cell.setCellStyle(hjStyle);
        cell = row.createCell(2);
        cell.setCellValue("");
        cell.setCellStyle(hjStyle);

        hjStyle = getFootCellStyle(wb);
        HSSFDataFormat df = wb.createDataFormat();
        hjStyle.setDataFormat(df.getBuiltinFormat("0"));
        cell = row.createCell(3);
        String sc = CellReference.convertNumToColString(3);
        cell.setCellStyle(hjStyle);
        cell.setCellFormula("SUM(" + sc + (rowStartIndex+1) + ":"+sc+rowNum+")");

        hjStyle = getFootCellStyle(wb);
        df = wb.createDataFormat();
        hjStyle.setDataFormat(df.getBuiltinFormat("0.00"));
        cell = row.createCell(4);
        String pc = CellReference.convertNumToColString(4);
        cell.setCellFormula("SUM(" + pc + (rowStartIndex+1) + ":"+pc+rowNum+")");
        cell.setCellStyle(hjStyle);
        return rowNum;
    }

    /**
     * 创建挂账
     * @param wb
     * @param sheet
     * @param rowNum
     * @return
     */
    public static int createHungUpItem(HSSFWorkbook wb,HSSFSheet sheet,int rowNum,Map<String,Map<String,Object>> hungUpMap){

        int rowStartIndex = rowNum;
        Iterator<Map.Entry<String,Map<String,Object>>> iterator = hungUpMap.entrySet().iterator();
        while(iterator.hasNext()){
            Map.Entry<String, Map<String,Object>> next = iterator.next();
            String key = next.getKey();
            Map<String,Object> map = (Map<String,Object>)next.getValue();
            sheet.addMergedRegion(new CellRangeAddress(rowStartIndex, rowNum, 1, 1));
            HSSFCellStyle headStyle = getCellStyle(wb);
            HSSFRow row = sheet.createRow(rowNum);
            HSSFCell cell = row.createCell(1);
            cell.setCellValue("挂账");
            cell.setCellStyle(headStyle);
            cell = row.createCell(2);
            cell.setCellValue(MapUtils.getString(map,"name",""));
            cell.setCellStyle(headStyle);
            cell = row.createCell(3);
            cell.setCellValue(MapUtils.getIntValue(map,"num",0));
            cell.setCellStyle(headStyle);
            cell = row.createCell(4);
            cell.setCellValue(new BigDecimal(MapUtils.getDoubleValue(map,"money",0)).setScale(2,RoundingMode.HALF_UP).doubleValue());
            cell.setCellStyle(headStyle);
            rowNum++;
        }

        HSSFCellStyle hjStyle = getFootCellStyle(wb);

        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = row.createCell(1);
        cell.setCellValue("合计");
        cell.setCellStyle(hjStyle);
        cell = row.createCell(2);
        cell.setCellValue("");
        cell.setCellStyle(hjStyle);

        hjStyle = getFootCellStyle(wb);
        HSSFDataFormat df = wb.createDataFormat();
        hjStyle.setDataFormat(df.getBuiltinFormat("0"));
        cell = row.createCell(3);
        String sc = CellReference.convertNumToColString(3);
        cell.setCellStyle(hjStyle);
        cell.setCellFormula("SUM(" + sc + (rowStartIndex+1) + ":"+sc+rowNum+")");

        hjStyle = getFootCellStyle(wb);
        df = wb.createDataFormat();
        hjStyle.setDataFormat(df.getBuiltinFormat("0.00"));
        cell = row.createCell(4);
        String pc = CellReference.convertNumToColString(4);
        cell.setCellFormula("SUM(" + pc + (rowStartIndex+1) + ":"+pc+rowNum+")");
        cell.setCellStyle(hjStyle);
        return rowNum;
    }

    /**
     * 创建赔偿
     * @param wb
     * @param sheet
     * @param rowNum
     * @return
     */
    public static int createDepositIndemnityItem(HSSFWorkbook wb,HSSFSheet sheet,int rowNum,Map<Integer,Map<String,Object>> indemnityProduct){

        HSSFCellStyle headStyle = wb.createCellStyle(); // 样式对象
        headStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        headStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        headStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        headStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        headStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
        headStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);//垂直居中

        int rowStartIndex = rowNum;
        Iterator<Map.Entry<Integer,Map<String,Object>>> iterator = indemnityProduct.entrySet().iterator();
        while(iterator.hasNext()){
            Map.Entry<Integer, Map<String,Object>> next = iterator.next();
            Integer key = next.getKey();
            Map<String,Object> map = (Map<String,Object>)next.getValue();
            sheet.addMergedRegion(new CellRangeAddress(rowStartIndex, rowNum, 1, 1));

            HSSFRow row = sheet.createRow(rowNum);
            HSSFCell cell = row.createCell(1);
            cell.setCellValue("赔偿");
            cell.setCellStyle(headStyle);
            cell = row.createCell(2);
            cell.setCellValue(MapUtils.getString(map,"name",""));
            cell.setCellStyle(headStyle);
            cell = row.createCell(3);
            cell.setCellValue(MapUtils.getIntValue(map,"num",0));
            cell.setCellStyle(headStyle);
            cell = row.createCell(4);
            cell.setCellValue(new BigDecimal(MapUtils.getDoubleValue(map,"money",0)).setScale(2,RoundingMode.HALF_UP).doubleValue());
            cell.setCellStyle(headStyle);
            rowNum++;
        }

        HSSFCellStyle hjStyle = getFootCellStyle(wb);

        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = row.createCell(1);
        cell.setCellValue("合计");
        cell.setCellStyle(hjStyle);
        cell = row.createCell(2);
        cell.setCellValue("");
        cell.setCellStyle(hjStyle);

        hjStyle = getFootCellStyle(wb);
        HSSFDataFormat df = wb.createDataFormat();
        hjStyle.setDataFormat(df.getBuiltinFormat("0"));
        cell = row.createCell(3);
        String sc = CellReference.convertNumToColString(3);
        cell.setCellStyle(hjStyle);
        cell.setCellFormula("SUM(" + sc + (rowStartIndex+1) + ":"+sc+rowNum+")");

        hjStyle = getFootCellStyle(wb);
        df = wb.createDataFormat();
        hjStyle.setDataFormat(df.getBuiltinFormat("0.00"));
        cell = row.createCell(4);
        String pc = CellReference.convertNumToColString(4);
        cell.setCellFormula("SUM(" + pc + (rowStartIndex+1) + ":"+pc+rowNum+")");
        cell.setCellStyle(hjStyle);
        return rowNum;
    }

    /**
     * 创建增值服务
     * @param wb
     * @param sheet
     * @param rowNum
     * @return
     */
    public static int createAddServiceItem(HSSFWorkbook wb,HSSFSheet sheet,int rowNum,List<Map<String, Object>> addServiceOrders){

        HSSFCellStyle headStyle = wb.createCellStyle(); // 样式对象
        headStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        headStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        headStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        headStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        headStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
        headStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);//垂直居中

        int rowStartIndex = rowNum;
        for(int i=0;i<addServiceOrders.size();i++){
            Map<String,Object> rentMap=addServiceOrders.get(i);

            HSSFRow row = sheet.createRow(rowNum);
            HSSFCell cell = row.createCell(1);
            cell.setCellValue("增值服务");
            cell.setCellStyle(headStyle);
            cell = row.createCell(2);
            cell.setCellValue(String.valueOf(rentMap.get("charge_name")));
            cell.setCellStyle(headStyle);
            cell = row.createCell(3);
            cell.setCellValue(MapUtils.getIntValue(rentMap,"totalNum",0));
            cell.setCellStyle(headStyle);
            cell = row.createCell(4);
            cell.setCellValue(new BigDecimal(MapUtils.getDoubleValue(rentMap,"totalMoney",0)).setScale(2,RoundingMode.HALF_UP).doubleValue());
            cell.setCellStyle(headStyle);
            rowNum++;
        }
        if(addServiceOrders.size()>0){
            sheet.addMergedRegion(new CellRangeAddress(rowStartIndex, rowStartIndex-1+addServiceOrders.size(), 1, 1));
        }


        HSSFCellStyle hjStyle = getFootCellStyle(wb);

        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = row.createCell(1);
        cell.setCellValue("合计");
        cell.setCellStyle(hjStyle);
        cell = row.createCell(2);
        cell.setCellValue("");
        cell.setCellStyle(hjStyle);

        hjStyle = getFootCellStyle(wb);
        HSSFDataFormat df = wb.createDataFormat();
        hjStyle.setDataFormat(df.getBuiltinFormat("0"));

        cell = row.createCell(3);
        String sc = CellReference.convertNumToColString(3);
        cell.setCellFormula("SUM(" + sc + (rowStartIndex+1) + ":"+sc+rowNum+")");
        cell.setCellStyle(hjStyle);
        cell = row.createCell(4);

        hjStyle = getFootCellStyle(wb);
        df = wb.createDataFormat();
        hjStyle.setDataFormat(df.getBuiltinFormat("0.00"));
        String pc = CellReference.convertNumToColString(4);
        cell.setCellFormula("SUM(" + pc + (rowStartIndex+1) + ":"+pc+rowNum+")");
        cell.setCellStyle(hjStyle);
        return rowNum;
    }

    /**
     * 创建租赁物
     * @param wb
     * @param sheet
     * @param rowNum
     * @return
     */
    public static int createRentItem(HSSFWorkbook wb, HSSFSheet sheet, int rowNum, List<Map<String, Object>> rentOrders, List<Map<String, Object>> rentTimeOutOrders){

        HSSFCellStyle headStyle = wb.createCellStyle(); // 样式对象
        headStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        headStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        headStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        headStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        headStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
        headStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);//垂直居中

        int rowStartIndex = rowNum;
        if(rentOrders!=null){
            for(int i=0;i<rentOrders.size();i++){
                Map<String,Object> rentMap=rentOrders.get(i);


                HSSFRow row = sheet.createRow(rowNum);
                HSSFCell cell = row.createCell(1);
                cell.setCellValue("租赁物");
                cell.setCellStyle(headStyle);
                cell = row.createCell(2);
                cell.setCellValue(String.valueOf(rentMap.get("name")));
                cell.setCellStyle(headStyle);
                cell = row.createCell(3);
                cell.setCellValue(MapUtils.getIntValue(rentMap,"totalNum",0));
                cell.setCellStyle(headStyle);
                cell = row.createCell(4);
                cell.setCellValue(new BigDecimal(MapUtils.getDoubleValue(rentMap,"totalMoney",0)).setScale(2,RoundingMode.HALF_UP).doubleValue());
                cell.setCellStyle(headStyle);
                rowNum++;
            }
            if(rentOrders.size()>0){
                sheet.addMergedRegion(new CellRangeAddress(rowStartIndex, rowStartIndex-1+rentOrders.size(), 1, 1));
            }
        }

        int rowStartIndext = rowNum;
        if(rentTimeOutOrders!=null){
            for(int i=0;i<rentTimeOutOrders.size();i++){
                Map<String,Object> rentTimeOutMap=rentTimeOutOrders.get(i);
                HSSFRow row = sheet.createRow(rowNum);
                HSSFCell cell = row.createCell(1);
                cell.setCellValue("超时租赁");
                cell.setCellStyle(headStyle);
                cell = row.createCell(2);
                cell.setCellValue(String.valueOf(rentTimeOutMap.get("name")));
                cell.setCellStyle(headStyle);
                cell = row.createCell(3);
                cell.setCellValue(MapUtils.getIntValue(rentTimeOutMap,"totalNum",0));
                cell.setCellStyle(headStyle);
                cell = row.createCell(4);
                cell.setCellValue(new BigDecimal(MapUtils.getDoubleValue(rentTimeOutMap,"totalMoney",0)).setScale(2,RoundingMode.HALF_UP).doubleValue());
                cell.setCellStyle(headStyle);
                rowNum++;
            }
            if(rentTimeOutOrders.size()>0){
                sheet.addMergedRegion(new CellRangeAddress(rowStartIndext, rowStartIndext-1+rentTimeOutOrders.size(), 1, 1));
            }
        }


        HSSFCellStyle hjStyle = getFootCellStyle(wb);

        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = row.createCell(1);
        cell.setCellValue("合计");
        cell.setCellStyle(hjStyle);
        cell = row.createCell(2);
        cell.setCellValue("");
        cell.setCellStyle(hjStyle);

        hjStyle = getFootCellStyle(wb);
        HSSFDataFormat df = wb.createDataFormat();
        hjStyle.setDataFormat(df.getBuiltinFormat("0"));
        cell = row.createCell(3);
        String sc = CellReference.convertNumToColString(3);
        rowStartIndex=rowStartIndex+1;
        cell.setCellFormula("SUM(" + sc +  (rowStartIndex)+ ":"+sc+rowNum+")");
        cell.setCellStyle(hjStyle);

        hjStyle = getFootCellStyle(wb);
        df = wb.createDataFormat();
        hjStyle.setDataFormat(df.getBuiltinFormat("0.00"));
        cell = row.createCell(4);
        String pc = CellReference.convertNumToColString(4);
        cell.setCellFormula("SUM(" + pc + (rowStartIndex) + ":"+pc+rowNum+")");
        cell.setCellStyle(hjStyle);
        return rowNum;
    }

    public static int createProdctItem(HSSFWorkbook wb, HSSFSheet sheet, int rowNum, Map<Integer,Map<String,Map<String,Object>>> saleProductStastic){
        HSSFCellStyle headStyle = wb.createCellStyle(); // 样式对象
        headStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        headStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        headStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        headStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        headStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
        headStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);//垂直居中
        return createProdctItem(wb,sheet,rowNum,saleProductStastic,headStyle);
    }


    /**
     * 创建产品项
     * @param wb
     * @param sheet
     * @param rowNum
     * @param saleProductStastic
     * @return
     */
    public static int createProdctItem(HSSFWorkbook wb, HSSFSheet sheet, int rowNum, Map<Integer,Map<String,Map<String,Object>>> saleProductStastic,HSSFCellStyle headStyle){

        int rowStartIndex = rowNum;

        HSSFCellStyle headStyle1 = getCellStyle(wb);

        HSSFCellStyle headStyle2 = getCellStyle(wb);
        HSSFDataFormat df = wb.createDataFormat();
        headStyle2.setDataFormat(df.getBuiltinFormat("0"));

        HSSFCellStyle headStyle3 = getCellStyle(wb);
        HSSFDataFormat df2 = wb.createDataFormat();
        headStyle3.setDataFormat(df2.getBuiltinFormat("0.00"));

        Iterator<Integer> iterator = saleProductStastic.keySet().iterator();
        while (iterator.hasNext()){
            Integer activityTypeId = iterator.next();
            Map<String, Map<String, Object>> stringMapMap = saleProductStastic.get(activityTypeId);

            HSSFRow row = sheet.createRow(rowNum);
            HSSFCell cell = row.createCell( 1);
            cell.setCellValue(getActivityTypeIdName(activityTypeId));
            cell.setCellStyle(headStyle);
            cell = row.createCell( 2);
            cell.setCellValue(getActivityTypeIdName(activityTypeId));
            cell.setCellStyle(headStyle);
            cell = row.createCell( 3);
            cell.setCellValue(getActivityTypeIdName(activityTypeId));
            cell.setCellStyle(headStyle);
            cell = row.createCell( 4);
            cell.setCellValue(getActivityTypeIdName(activityTypeId));
            cell.setCellStyle(headStyle);
            sheet.addMergedRegion(new CellRangeAddress( rowNum,rowNum + stringMapMap.size()-1,1, 1));

            boolean isFirst = true;
            Iterator<Map.Entry<String, Map<String, Object>>> pIter = stringMapMap.entrySet().iterator();
            while (pIter.hasNext()){

                if(!isFirst){
                    row = sheet.createRow(rowNum);
                }
                isFirst = false;

                Map.Entry<String, Map<String, Object>> next = pIter.next();
                Map<String, Object> value = next.getValue();
                String activityName = next.getKey();

                cell = row.createCell(2);
                cell.setCellValue(activityName);
                cell.setCellStyle(headStyle1);

                cell = row.createCell(3);
                cell.setCellStyle(headStyle2);
                cell.setCellValue(MapUtils.getIntValue(value,"num",0));

                cell = row.createCell(4);
                cell.setCellStyle(headStyle3);
                cell.setCellValue(new BigDecimal(MapUtils.getDoubleValue(value,"price",0)).setScale(2,RoundingMode.HALF_UP).doubleValue());

                rowNum++;
            }
        }

        HSSFCellStyle hjStyle = getFootCellStyle(wb);
        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = row.createCell(1);
        cell.setCellValue("合计");
        cell.setCellStyle(hjStyle);
        cell = row.createCell(2);
        cell.setCellValue("");
        cell.setCellStyle(hjStyle);

        HSSFDataFormat df3 = wb.createDataFormat();
        hjStyle.setDataFormat(df3.getBuiltinFormat("0"));
        cell = row.createCell(3);
        String sc = CellReference.convertNumToColString(3);
        cell.setCellStyle(hjStyle);
        cell.setCellFormula("SUM(" + sc + rowStartIndex + ":"+sc+rowNum+")");


        hjStyle = getFootCellStyle(wb);
        hjStyle.setDataFormat(df.getBuiltinFormat("0.00"));
        cell = row.createCell( 4);
        String pc = CellReference.convertNumToColString(4);
        cell.setCellStyle(hjStyle);
        cell.setCellFormula("SUM(" + pc + rowStartIndex + ":"+pc+rowNum+")");

        return rowNum;
    }

    /**
     * 创建教练支付方式汇总
     * @param wb
     * @param sheet
     * @param rowNum
     * @param coachPayListMap
     * @param payNameSet
     * @return
     */
    public static int createPayCoachItem(HSSFWorkbook wb, HSSFSheet sheet, int rowNum, Map<String,Map<String,Map<String,Map<String,Map<String,Object>>>>> coachPayListMap,Set<String> payNameSet){
        int rowStartIndex = rowNum;

        String[] head = new String[2*payNameSet.size()+5];
        head[0] = "类型";
        head[1] = "产品名称";
        head[2] = "票券名称";

        HSSFCellStyle cellStyle = getCellStyle(wb);
        HSSFCellStyle bigDecimalcellStyle = getCellStyle(wb);
        HSSFDataFormat df = wb.createDataFormat();
        bigDecimalcellStyle.setDataFormat(df.getBuiltinFormat("0.00"));

        int index = 3;
        Iterator<String> payNameIterator = payNameSet.iterator();
        while (payNameIterator.hasNext()){
            String payName = payNameIterator.next();
            head[index++] = payName;
            head[index++] = "核销数量";
        }
        head[index++] = "金额总计";
        head[index++] = "核销总计";

        createExcellHead(wb,sheet,rowNum,head);
        rowNum++;

        Iterator<Map.Entry<String, Map<String, Map<String, Map<String, Map<String, Object>>>>>> coachPayListMapIterator = coachPayListMap.entrySet().iterator();
        while (coachPayListMapIterator.hasNext()){
            Map.Entry<String, Map<String, Map<String, Map<String, Map<String, Object>>>>> coachPayListMapEntry = coachPayListMapIterator.next();
            //类型
            String typeName = coachPayListMapEntry.getKey();

            int episodRowSize = 0;

            //产品
            Map<String, Map<String, Map<String, Map<String, Object>>>> activityPayListMap = coachPayListMapEntry.getValue();
            Iterator<Map.Entry<String, Map<String, Map<String, Map<String, Object>>>>> activityPayListMapIterator = activityPayListMap.entrySet().iterator();

            while (activityPayListMapIterator.hasNext()){

                int activityRowSize = 0;
                int activityRowStart = rowNum;

                Map.Entry<String, Map<String, Map<String, Map<String, Object>>>> activityPayListMapEntry = activityPayListMapIterator.next();
                //产品名称
                String activityName = activityPayListMapEntry.getKey();

                Map<String, Map<String, Map<String, Object>>> episodPayListMap = activityPayListMapEntry.getValue();
                Iterator<Map.Entry<String, Map<String, Map<String, Object>>>> episodPayListMapIterator = episodPayListMap.entrySet().iterator();
                while (episodPayListMapIterator.hasNext()){

                    episodRowSize++;
                    activityRowSize++;

                    Map.Entry<String, Map<String, Map<String, Object>>> episodPayListMapEntry = episodPayListMapIterator.next();

                    //场次名称
                    String episodName = episodPayListMapEntry.getKey();

                    int colNum = 1;
                    HSSFRow row = sheet.createRow(rowNum++);
                    HSSFCell cell = row.createCell(colNum++);
                    cell.setCellValue(typeName);
                    cell.setCellStyle(cellStyle);

                    cell = row.createCell(colNum++);
                    cell.setCellValue(activityName);
                    cell.setCellStyle(cellStyle);

                    cell = row.createCell(colNum++);
                    cell.setCellValue(episodName);
                    cell.setCellStyle(cellStyle);

                    Map<String, Map<String, Object>> payListMap = episodPayListMapEntry.getValue();

                    double totalPrice = 0;
                    int totalNum = 0;

                    Iterator<String> pnItertor = payNameSet.iterator();
                    while (pnItertor.hasNext()){

                        String payName = pnItertor.next();
                        Map<String, Object> payMap = payListMap.get(payName);

                        Double price = MapUtils.getDouble(payMap, "price",0.0);
                        totalPrice += price;

                        cell = row.createCell(colNum++);
                        cell.setCellValue(price);
                        cell.setCellStyle(bigDecimalcellStyle);

                        int num = MapUtils.getIntValue(payMap, "num",0);
                        totalNum+=num;
                        cell = row.createCell(colNum++);
                        cell.setCellValue(num);
                        cell.setCellStyle(cellStyle);
                    }

                    cell = row.createCell(colNum++);
                    cell.setCellValue(totalPrice);
                    cell.setCellStyle(cellStyle);

                    cell = row.createCell(colNum++);
                    cell.setCellValue(totalNum);
                    cell.setCellStyle(cellStyle);

                }

                //产品合并
                if(activityRowSize>1){
                    sheet.addMergedRegion(new CellRangeAddress( activityRowStart,activityRowStart + activityRowSize-1,2, 2));
                }
            }

            //创建类型合并
            if(episodRowSize>1){
                sheet.addMergedRegion(new CellRangeAddress( rowStartIndex+1,rowStartIndex + episodRowSize,1, 1));
            }

        }

        if(coachPayListMap.size()>0){

            int colNum = 1;

            HSSFCellStyle hjStyle = getFootCellStyle(wb);

            HSSFCellStyle bigFootCellStyle = getFootCellStyle(wb);
            HSSFDataFormat dfFoot = wb.createDataFormat();
            bigFootCellStyle.setDataFormat(dfFoot.getBuiltinFormat("0.00"));

            HSSFRow row = sheet.createRow(rowNum);
            HSSFCell cell = row.createCell(1);
            cell.setCellValue("合计");
            cell.setCellStyle(hjStyle);
            cell = row.createCell(colNum++);
            cell.setCellValue("");
            cell.setCellStyle(hjStyle);
            cell = row.createCell(colNum++);
            cell.setCellValue("");
            cell.setCellStyle(hjStyle);
            cell = row.createCell(colNum++);
            cell.setCellValue("");
            cell.setCellStyle(hjStyle);

            int size = payNameSet.size();
            for (int i = 0 ;i < 2 * size + 2;i++){
                cell = row.createCell( colNum);
                String pc = CellReference.convertNumToColString(colNum);
                if(colNum%2==0){
                    cell.setCellStyle(bigFootCellStyle);
                }else{
                    cell.setCellStyle(hjStyle);
                }

                cell.setCellFormula("SUM(" + pc + (rowStartIndex+1) + ":"+pc+rowNum+")");
                colNum++;
            }
        }

        return rowNum;
    }

    /**
     * 创建教练课程项
     * @param wb
     * @param sheet
     * @param rowNum
     * @param coachCourseListMap
     * @return
     */
    public static int createCoachItem(HSSFWorkbook wb, HSSFSheet sheet, int rowNum, Map<String,Map<String,Map<String,Map<String,Object>>>> coachCourseListMap){
        int rowStartIndex = rowNum;

        Iterator<String> iterator = coachCourseListMap.keySet().iterator();
        while (iterator.hasNext()){
            String activityName = iterator.next();
            Map<String,Map<String, Map<String, Object>>> activityMap = coachCourseListMap.get(activityName);

            HSSFCellStyle headStyle = wb.createCellStyle(); // 样式对象
            headStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
            headStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
            headStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
            headStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
            headStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
            headStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);//垂直居中

            int oneIndex = rowNum;

            Iterator<String> pIter = activityMap.keySet().iterator();
            int size = 0;
            while (pIter.hasNext()){
                String epName = pIter.next();
                Map<String,Map<String, Object>> epMap = activityMap.get(epName);
                size+=epMap.size();
                Iterator<String> chIter = epMap.keySet().iterator();
                int twoIndex = rowNum;
                while (chIter.hasNext()){
                    HSSFRow row = sheet.createRow(rowNum);
                    HSSFCell cell = row.createCell( 1);
                    cell.setCellValue(activityName);
                    cell.setCellStyle(headStyle);

                    headStyle = getCellStyle(wb);
                    cell = row.createCell(2);
                    cell.setCellValue(epName);
                    cell.setCellStyle(headStyle);

                    String chName = chIter.next();
                    Map<String,Object> value=epMap.get(chName);

                    headStyle = getCellStyle(wb);
                    cell = row.createCell(3);
                    cell.setCellStyle(headStyle);
                    cell.setCellValue(chName);

                    headStyle = getCellStyle(wb);
                    HSSFDataFormat df = wb.createDataFormat();
                    headStyle.setDataFormat(df.getBuiltinFormat("0"));
                    cell = row.createCell(4);
                    cell.setCellStyle(headStyle);
                    cell.setCellValue(MapUtils.getIntValue(value,"num",0));

                    headStyle = getCellStyle(wb);
                    df = wb.createDataFormat();
                    headStyle.setDataFormat(df.getBuiltinFormat("0.00"));
                    cell = row.createCell(5);
                    cell.setCellStyle(headStyle);
                    cell.setCellValue(new BigDecimal(MapUtils.getDoubleValue(value,"price",0)).setScale(2,RoundingMode.HALF_UP).doubleValue());

                    rowNum++;
                }
                if(epMap.size()>0){
                    sheet.addMergedRegion(new CellRangeAddress( twoIndex,twoIndex + epMap.size()-1,2, 2));
                }
            }

            if(size>0){
                sheet.addMergedRegion(new CellRangeAddress( oneIndex,oneIndex + size - 1,1, 1));
            }

        }
        if(coachCourseListMap.size()>0){
            HSSFCellStyle hjStyle = getFootCellStyle(wb);
            HSSFRow row = sheet.createRow(rowNum);
            HSSFCell cell = row.createCell(1);
            cell.setCellValue("合计");
            cell.setCellStyle(hjStyle);
            cell = row.createCell(2);
            cell.setCellValue("");
            cell.setCellStyle(hjStyle);
            cell = row.createCell(3);
            cell.setCellValue("");
            cell.setCellStyle(hjStyle);
            HSSFDataFormat df = wb.createDataFormat();
            hjStyle.setDataFormat(df.getBuiltinFormat("0"));
            cell = row.createCell(4);
            String sc = CellReference.convertNumToColString(4);
            cell.setCellStyle(hjStyle);
            cell.setCellFormula("SUM(" + sc + (rowStartIndex+1) + ":"+sc+rowNum+")");


            hjStyle = getFootCellStyle(wb);
            hjStyle.setDataFormat(df.getBuiltinFormat("#,##0.00"));
            cell = row.createCell( 5);
            String pc = CellReference.convertNumToColString(5);
            cell.setCellStyle(hjStyle);
            cell.setCellFormula("SUM(" + pc + (rowStartIndex+1) + ":"+pc+rowNum+")");
        }
        return rowNum;
    }
    /**
     * 底部结算样式
     * @param wb
     * @return
     */
    public static HSSFCellStyle getFootCellStyle(HSSFWorkbook wb){
        HSSFCellStyle hjStyle = wb.createCellStyle(); // 样式对象
        hjStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        hjStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        hjStyle.setFillForegroundColor(HSSFColor.GREY_40_PERCENT.index);
        hjStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        hjStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        hjStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        hjStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
        return hjStyle;
    }

    public static HSSFCellStyle getCellStyle(HSSFWorkbook wb){
        HSSFCellStyle headStyle = wb.createCellStyle(); // 样式对象
        headStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        headStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        headStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        headStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        headStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
        headStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);//垂直居中
        return headStyle;
    }

    /**
     * 创建商品类型统计
     * @param orders
     * @param saleProductStastic
     */
    public static void createProductStasticsData(Orders orders,Map<Integer,Map<String,Map<String,Object>>> saleProductStastic){
        //筛选类型
        Map<String, Map<String,Object>> map = saleProductStastic.get(orders.getActivityTypeId());
        if(map == null){
            map = new HashMap<String, Map<String,Object>>();
            saleProductStastic.put(orders.getActivityTypeId(),map);
        }

        //商品
        Map<String, Object> activityMap = map.get(orders.getActivityName());
        if(activityMap==null){
            activityMap = new HashMap<String, Object>();
            map.put(orders.getActivityName(),activityMap);
        }

        int num = MapUtils.getIntValue(activityMap, "num", 0);
        activityMap.put("num",num + orders.getChargeNum());

        double price = MapUtils.getDoubleValue(activityMap, "price", 0);
        activityMap.put("price",orders.getTotalPrice().doubleValue() + price);
    }

    public static void createProductHead(HSSFWorkbook wb,HSSFSheet sheet,int rowNum){
        HSSFCellStyle style = wb.createCellStyle(); // 样式对象
        HSSFFont font = wb.createFont();// 生成一个字体
        font.setColor(HSSFColor.BLACK.index);// HSSFColor.VIOLET.index // 字体颜色
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);// 字体增粗
        style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        style.setFillForegroundColor(HSSFColor.GREY_40_PERCENT.index);
        style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        style.setBorderRight(HSSFCellStyle.BORDER_THIN);
        style.setBorderTop(HSSFCellStyle.BORDER_THIN);
        style.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);//垂直居中
        style.setFont(font);// 把字体应用到当前的样式
        //style.setFillBackgroundColor(HSSFColor.BLACK.index);
        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = row.createCell(1);
        cell.setCellValue("项目类型");
        cell.setCellStyle(style);
        sheet.setColumnWidth(1, 22 * 172);
        cell = row.createCell(2);
        cell.setCellValue("名称");
        cell.setCellStyle(style);
        sheet.setColumnWidth(2, 22 * 172);
        cell = row.createCell(3);
        cell.setCellValue("核销数量");
        cell.setCellStyle(style);
        sheet.setColumnWidth(3, 22 * 172);
        cell = row.createCell(4);
        cell.setCellValue("收入金额");
        sheet.setColumnWidth(4, 22 * 172);
        cell.setCellStyle(style);
    }
    public static void createCoachHead(HSSFWorkbook wb,HSSFSheet sheet,int rowNum){
        HSSFCellStyle style = wb.createCellStyle(); // 样式对象
        HSSFFont font = wb.createFont();// 生成一个字体
        font.setColor(HSSFColor.BLACK.index);// HSSFColor.VIOLET.index // 字体颜色
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);// 字体增粗
        style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        style.setFillForegroundColor(HSSFColor.GREY_40_PERCENT.index);
        style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        style.setBorderRight(HSSFCellStyle.BORDER_THIN);
        style.setBorderTop(HSSFCellStyle.BORDER_THIN);
        style.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);//垂直居中
        style.setFont(font);// 把字体应用到当前的样式
        //style.setFillBackgroundColor(HSSFColor.BLACK.index);
        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = row.createCell(1);
        cell.setCellValue("活动名称");
        cell.setCellStyle(style);
        sheet.setColumnWidth(1, 22 * 172);
        cell = row.createCell(2);
        cell.setCellValue("场次名称");
        cell.setCellStyle(style);
        sheet.setColumnWidth(2, 22 * 172);
        cell = row.createCell(3);
        cell.setCellValue("票券名称");
        cell.setCellStyle(style);
        sheet.setColumnWidth(3, 22 * 172);
        cell = row.createCell(4);
        cell.setCellValue("数量");
        sheet.setColumnWidth(4, 22 * 172);
        cell.setCellStyle(style);
        cell = row.createCell(5);
        cell.setCellValue("金额");
        sheet.setColumnWidth(5, 22 * 172);
        cell.setCellStyle(style);

    }
    public static String getActivityTypeIdName(Integer activityTypeId){
        if(activityTypeId == null){
            return "";
        }
        switch (activityTypeId.intValue()){
            case 0:
                return "最新活动";
            case 1:
                return "滑雪度假";
            case 2:
                return "教练预约";
            case 3:
                return  "超值票务";
            case 4:
                return  "会员卡";
            case 5:
                return "训练营";
            case 6:
                return "自营票务";
            case 7:
                return "酒店预定";
            case 8:
                return "教练课程";
            case 9:
                return "储值卡";
            case 10:
                return "续时产品";
        }
        return "";
    }

}
