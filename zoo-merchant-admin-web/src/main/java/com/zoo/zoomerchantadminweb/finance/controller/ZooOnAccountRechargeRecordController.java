package com.zoo.zoomerchantadminweb.finance.controller;

import com.zoo.activity.dao.model.Organizer;
import com.zoo.activity.dao.nutz.pojo.CommonAccount;
import com.zoo.common.util.StringUtil;
import com.zoo.finance.dao.model.ZooOnAccountCompany;
import com.zoo.finance.dao.model.ZooOnAccountCompanyExample;
import com.zoo.finance.dao.model.ZooOnAccountRechargeRecord;
import com.zoo.finance.dao.model.ZooOnAccountRechargeRecordExample;
import com.zoo.finance.service.ZooOnAccountCompanyService;
import com.zoo.finance.service.ZooOnAccountRechargeRecordService;
import com.zoo.web.bean.WebResult;
import com.zoo.zoomerchantadminweb.context.WebUpmsContext;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Api(value = "api/finance/hangup/recharge", tags = {"【财务系统】挂帐单位充值管理"}, description = "挂帐单位充值管理接口描述")
@RestController
@RequestMapping("api/finance/hangup/recharge")
public class ZooOnAccountRechargeRecordController {

    @Autowired
    private ZooOnAccountRechargeRecordService zooOnAccountRechargeRecordService;

    @Autowired
    private ZooOnAccountCompanyService zooOnAccountCompanyService;

    @ApiOperation(value = "充值流水列表")
    //@RequiresPermissions("zoo:onAccountRechargeRecord:read")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "size", value = "分页大小", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "companyId", value = "企业ID", required = true, dataType = "int", paramType = "query"),
    })
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ResponseBody
    public Object list(HttpServletRequest request,
            @RequestParam(required = false, defaultValue = "0", value = "page") int page,
            @RequestParam(required = false, defaultValue = "10", value = "size") int size,
            @RequestParam(required = false, value = "sort") String sort,
            @RequestParam(required = false, value = "order") String order,
            @RequestParam(required = true, value = "companyId") Integer companyId) {
        ZooOnAccountRechargeRecordExample zooOnAccountRechargeRecordExample = new ZooOnAccountRechargeRecordExample();
        if (!StringUtils.isBlank(sort) && !StringUtils.isBlank(order)) {
            zooOnAccountRechargeRecordExample.setOrderByClause(StringUtil.humpToLine(sort) + " " + order);
        }
        ZooOnAccountRechargeRecordExample.Criteria criteria = zooOnAccountRechargeRecordExample.createCriteria();
        CommonAccount account = WebUpmsContext.getAccount(request);
        Organizer orgnization = WebUpmsContext.getOrgnization(request);
        criteria.andOrganizerIdEqualTo(orgnization.getOrganizerId());
        criteria.andCompanyIdEqualTo(companyId);
        zooOnAccountRechargeRecordExample.setOrderByClause("create_time desc");
        List<ZooOnAccountRechargeRecord> rows = zooOnAccountRechargeRecordService.selectByExampleForStartPage(zooOnAccountRechargeRecordExample, page, size);
        if(rows!=null){
            for (ZooOnAccountRechargeRecord row : rows) {
                String operatorName = row.getOperatorName();
                if(operatorName == null || operatorName.contains("小ZOO")){
                    row.setOperatorName("总管理员");
                }
            }

        }
        long total = zooOnAccountRechargeRecordService.countByExample(zooOnAccountRechargeRecordExample);
        WebResult result = WebResult.getSuccessResult();
        result.put("list", rows);
        result.put("count", total);
        return result;
    }

    @ApiOperation(value = "充值")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "ID", required = false, dataType = "long", paramType = "query"),
            @ApiImplicitParam(name = "money", value = "金额", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "remark", value = "备注", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "type", value = "类型(1充值，2扣款)", required = false, dataType = "string", paramType = "query"),
    })
    //@RequiresPermissions("zoo:onAccountCompany:create")
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @ResponseBody
    public WebResult recharge(HttpServletRequest request,Long id, String money,String remark,Integer type){
        double moneyDouble = NumberUtils.toDouble(money,-1);
        if(moneyDouble <= 0){
            return WebResult.getErrorResult(WebResult.Code.RECHANGER_MONEY_FAIL);
        }
        if(id == null){
            return WebResult.getErrorResult(WebResult.Code.RECHANGER_MONEY_FAIL);
        }
        if(type == null){
            return WebResult.getErrorResult(WebResult.Code.RECHANGER_MONEY_FAIL);
        }
        CommonAccount account = WebUpmsContext.getAccount(request);
        Organizer orgnization = WebUpmsContext.getOrgnization(request);

        ZooOnAccountCompanyExample example = new ZooOnAccountCompanyExample();
        example.createCriteria().andIdEqualTo(id);
        ZooOnAccountCompany company = zooOnAccountCompanyService.selectFirstByExample(example);
        if(type.intValue() == 2 && company.getBalance().compareTo(new BigDecimal(money))<0){
            return WebResult.getErrorResult(WebResult.Code.HUNG_UP_NO_BALANCE);
        }

        ZooOnAccountRechargeRecord rechargeRecord = new ZooOnAccountRechargeRecord();
        rechargeRecord.setOrganizerId(orgnization.getOrganizerId());
        rechargeRecord.setBeforeMoney(company.getBalance());
        rechargeRecord.setMoney(new BigDecimal(money));
        rechargeRecord.setCreateTime(new Date());
        rechargeRecord.setCompanyId(id.intValue());
        rechargeRecord.setOperatorId(account.getCustomerId());
        rechargeRecord.setOperatorName(org.apache.commons.lang3.StringUtils.isEmpty(account.getRealname())?account.getNickname():account.getRealname());
        rechargeRecord.setOrganizerName(orgnization.getName());
        rechargeRecord.setType(type);
        rechargeRecord.setRemark(remark);
        if(zooOnAccountCompanyService.recharge(rechargeRecord)){
            return WebResult.getSuccessResult();
        }else{
            return WebResult.getErrorResult(WebResult.Code.RECHANGER_FAIL);
        }
    }

}
