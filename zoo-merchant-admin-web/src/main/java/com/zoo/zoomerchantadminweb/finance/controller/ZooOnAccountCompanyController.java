package com.zoo.zoomerchantadminweb.finance.controller;


import com.zoo.activity.dao.model.Organizer;
import com.zoo.activity.dao.nutz.pojo.CommonAccount;
import com.zoo.common.util.StringUtil;
import com.zoo.finance.common.constant.ZooHangupConstant;
import com.zoo.finance.dao.model.ZooOnAccountCompany;
import com.zoo.finance.dao.model.ZooOnAccountCompanyExample;
import com.zoo.finance.service.ZooOnAccountCompanyService;
import com.zoo.web.bean.WebResult;
import com.zoo.zoomerchantadminweb.context.WebUpmsContext;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.*;


@Api(value = "api/finance/hangup/company", tags = {"【财务系统】挂帐单位管理"}, description = "挂帐单位接口描述")
@RestController
@RequestMapping("api/finance/hangup/company")
public class ZooOnAccountCompanyController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ZooOnAccountCompanyController.class);

    @Autowired
    private ZooOnAccountCompanyService zooOnAccountCompanyService;

    @ApiOperation(value = "挂帐单位列表（查询的是启用/停用两种状态的单位列表）")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "size", value = "分页大小", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "name", value = "挂账单位名称", required = false, dataType = "string", paramType = "query"),
    })
    //@RequiresPermissions("zoo:onAccountCompany:read")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ResponseBody
    public WebResult list(HttpServletRequest request,
                          @RequestParam(required = false, defaultValue = "0", value = "page") int page,
                          @RequestParam(required = false, defaultValue = "10", value = "size") int size,
                          @RequestParam(required = false, value = "sort") String sort,
                          @RequestParam(required = false, value = "order") String order,
                          @RequestParam(required = false, value = "name") String name) {
        ZooOnAccountCompanyExample zooOnAccountCompanyExample = new ZooOnAccountCompanyExample();
        if (!org.apache.commons.lang.StringUtils.isBlank(sort) && !org.apache.commons.lang.StringUtils.isBlank(order)) {
            zooOnAccountCompanyExample.setOrderByClause(StringUtil.humpToLine(sort) + " " + order);
        }
        ZooOnAccountCompanyExample.Criteria criteria = zooOnAccountCompanyExample.createCriteria();
        if (StringUtils.isNotEmpty(name)) {
            criteria.andCompanyNameLike("%" + name + "%");
        }
        zooOnAccountCompanyExample.setOrderByClause("create_time desc");
//        CommonAccount account = WebUpmsContext.getAccount(request);
        Organizer orgnization = WebUpmsContext.getOrgnization(request);
        criteria.andOrganizerIdEqualTo(orgnization.getOrganizerId());

        List statusList = new ArrayList<>();
        statusList.add(0);
        statusList.add(1);
        criteria.andStatusIn(statusList);
        
        List<ZooOnAccountCompany> rows = zooOnAccountCompanyService.selectByExampleForStartPage(zooOnAccountCompanyExample, page, size);
        if (rows != null) {
            for (ZooOnAccountCompany row : rows) {
                String operatorName = row.getOperatorName();
                if (operatorName == null || operatorName.contains("小ZOO")) {
                    row.setOperatorName("总管理员");
                }
            }
        }
        long total = zooOnAccountCompanyService.countByExample(zooOnAccountCompanyExample);
        WebResult result = WebResult.getSuccessResult();
        result.put("count", total);
        result.put("list", rows);
        return result;
    }

    @ApiOperation(value = "下单时挂账单位,此接口只能查询一种状态的单位列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "size", value = "分页大小", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "status", value = "挂账单位状态", required = false, dataType = "string", paramType = "query"),
    })
    //@RequiresPermissions("zoo:onAccountCompany:read")
    @RequestMapping(value = "/list.json", method = RequestMethod.POST)
    @ResponseBody
    public WebResult companyList(HttpServletRequest request,
                                 @RequestParam(required = false, defaultValue = "0", value = "page") int page,
                                 @RequestParam(required = false, defaultValue = "0", value = "size") int size,
                                 @RequestParam(required = false, value = "status", defaultValue = "1") Integer status,
                                 @RequestParam(required = false, value = "sort") String sort,
                                 @RequestParam(required = false, value = "order") String order,
                                 @RequestParam(required = false, value = "name") String name) {
        ZooOnAccountCompanyExample zooOnAccountCompanyExample = new ZooOnAccountCompanyExample();
        if (!org.apache.commons.lang.StringUtils.isBlank(sort) && !org.apache.commons.lang.StringUtils.isBlank(order)) {
            zooOnAccountCompanyExample.setOrderByClause(StringUtil.humpToLine(sort) + " " + order);
        }
        ZooOnAccountCompanyExample.Criteria criteria = zooOnAccountCompanyExample.createCriteria();
        if (StringUtils.isNotEmpty(name)) {
            criteria.andCompanyNameLike("%" + name + "%");
        }
        zooOnAccountCompanyExample.setOrderByClause("create_time desc");
//        CommonAccount account = WebUpmsContext.getAccount(request);
        Organizer orgnization = WebUpmsContext.getOrgnization(request);
        criteria.andOrganizerIdEqualTo(orgnization.getOrganizerId());

//        List statusList = new ArrayList<>();
//        statusList.add(status);
        criteria.andStatusEqualTo(status);

        List<ZooOnAccountCompany> rows = zooOnAccountCompanyService.selectByExampleForStartPage(zooOnAccountCompanyExample, page, size);
        if (rows != null) {
            for (ZooOnAccountCompany row : rows) {
                String operatorName = row.getOperatorName();
                if (operatorName == null || operatorName.contains("小ZOO")) {
                    row.setOperatorName("总管理员");
                }
            }
        }
        long total = zooOnAccountCompanyService.countByExample(zooOnAccountCompanyExample);
        WebResult result = WebResult.getSuccessResult();
        result.put("count", total);
        result.put("list", rows);
        return result;
    }

    @ApiOperation(value = "新增挂帐单位")
    @RequiresPermissions(value = {"sys_manager:hungup:add"}, logical = Logical.OR)
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @ResponseBody
    public WebResult create(HttpServletRequest request, ZooOnAccountCompany zooOnAccountCompany) {
        if (zooOnAccountCompany == null || StringUtils.isBlank(zooOnAccountCompany.getCompanyName())) {
            return WebResult.getErrorResult(WebResult.Code.COMPANY_NAME_IS_MUST);
        }
        CommonAccount account = WebUpmsContext.getAccount(request);
        Organizer orgnization = WebUpmsContext.getOrgnization(request);
        zooOnAccountCompany.setStatus(1);
        zooOnAccountCompany.setOrganizerId(orgnization.getOrganizerId());
        zooOnAccountCompany.setOrganizerName(orgnization.getName());
        zooOnAccountCompany.setOperatorId(account.getCustomerId());
        zooOnAccountCompany.setOperatorName(StringUtils.isEmpty(account.getNickname()) ? account.getRealname() : account.getNickname());
        if (zooOnAccountCompany.getBalance() == null || zooOnAccountCompany.getBalance().intValue() == 0) {
            zooOnAccountCompany.setBalance(new BigDecimal(0));
        }
        int count = zooOnAccountCompanyService.insertSelective(zooOnAccountCompany);
        if (count <= 0) {
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }
        return WebResult.getSuccessResult();
    }

    @ApiOperation(value = "删除挂帐单位")
    @ApiImplicitParam(name = "id", value = "ID", required = false, dataType = "int", paramType = "query")
    //@RequiresPermissions("zoo:onAccountCompany:delete")
    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    @ResponseBody
    public WebResult delete(HttpServletRequest request, @RequestParam(required = false, value = "id") Long id) {
        if (id == null) {
            return WebResult.getErrorResult(WebResult.Code.ID_CAN_NOT_NULL);
        }
        ZooOnAccountCompany zooOnAccountCompany = new ZooOnAccountCompany();
        zooOnAccountCompany.setId(id);
        zooOnAccountCompany.setStatus(ZooHangupConstant.zoo_on_account_company_status_delete);
        int count = zooOnAccountCompanyService.updateByPrimaryKeySelective(zooOnAccountCompany);
        if (count <= 0) {
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }
        return WebResult.getSuccessResult();
    }

    @ApiOperation(value = "启用or停用挂帐单位装填")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "id", value = "ID", required = false, dataType = "int", paramType = "query"),
        @ApiImplicitParam(name = "status", value = "status", required = false, dataType = "int", paramType = "query")
    })
    //@RequiresPermissions("zoo:onAccountCompany:status")
    @RequestMapping(value = "/status", method = RequestMethod.GET)
    @ResponseBody
    public WebResult status(HttpServletRequest request, @RequestParam(required = false, value = "id") Long id
            , @RequestParam(required = false, value = "status") Integer status) {
        if (id == null) {
            return WebResult.getErrorResult(WebResult.Code.ID_CAN_NOT_NULL);
        }
        CommonAccount account = WebUpmsContext.getAccount(request);
        if (account == null) {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
        ZooOnAccountCompany zooOnAccountCompany = new ZooOnAccountCompany();
        zooOnAccountCompany.setId(id);
        zooOnAccountCompany.setStatus(status);
        zooOnAccountCompany.setOperatorId(account.getCustomerId());
        zooOnAccountCompany.setOperatorName(account.getRealname() != null ? account.getRealname() : account.getNickname());
        int count = zooOnAccountCompanyService.updateByPrimaryKeySelective(zooOnAccountCompany);
        if (count <= 0) {
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }
        return WebResult.getSuccessResult();
    }

    @ApiOperation(value = "查询挂帐单位")
    @ApiImplicitParam(name = "id", value = "ID", required = false, dataType = "int", paramType = "query")
    //@RequiresPermissions("zoo:onAccountCompany:read")
    @RequestMapping(value = "/queryById", method = RequestMethod.GET)
    public WebResult queryById(@RequestParam(required = false, value = "id") Long id, ModelMap modelMap) {
        if (id == null) {
            return WebResult.getErrorResult(WebResult.Code.ID_CAN_NOT_NULL);
        }
        ZooOnAccountCompanyExample example = new ZooOnAccountCompanyExample();
        example.createCriteria().andIdEqualTo(id);
        ZooOnAccountCompany zooOnAccountCompany = zooOnAccountCompanyService.selectFirstByExample(example);

        WebResult result = WebResult.getSuccessResult();
        result.putResult("data", zooOnAccountCompany);

        return result;
    }

    @ApiOperation(value = "修改挂帐单位")
    //@RequiresPermissions("zoo:onAccountCompany:update")
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ResponseBody
    public WebResult update(ZooOnAccountCompany zooOnAccountCompany) {
        if (zooOnAccountCompany == null || zooOnAccountCompany.getId() == null) {
            return WebResult.getErrorResult(WebResult.Code.ID_CAN_NOT_NULL);
        }
        int count = zooOnAccountCompanyService.updateByPrimaryKeySelective(zooOnAccountCompany);
        WebResult result = WebResult.getSuccessResult();
        result.putResult("count", count);
        return result;
    }

}
