package com.zoo.zoomerchantadminweb.finance.controller;

import com.github.pagehelper.PageInfo;
import com.zoo.activity.dao.model.Organizer;
import com.zoo.activity.service.OrdersService;
import com.zoo.activity.util.DateUtil;
import com.zoo.activity.util.MapUtil;
import com.zoo.finance.dao.mapper.ZooSettlementTransferAccountsMapper;
import com.zoo.finance.dao.model.*;
import com.zoo.finance.service.*;
import com.zoo.web.bean.WebResult;
import com.zoo.zoomerchantadminweb.context.WebUpmsContext;
import io.swagger.annotations.*;
import jodd.util.StringUtil;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.*;

@Api(value = "api/finance/balance", tags = {"【财务系统】对账管理"}, description = "对账管理接口描述")
@RestController
@RequestMapping("api/finance/balance")
public class BalanceAccountsController {

    private static final Logger LOGGER = LoggerFactory.getLogger(BalanceAccountsController.class);

    @Autowired
    private ZooSettlementDetailService zooSettlementDetailService;

    @Autowired
    private ZooSettlementTransferAccountsService zooSettlementTransferAccountsService;

    @Autowired
    private ZooSettlementInvoiceService zooSettlementInvoiceService;

    @Autowired
    private ZooBalanceStatisticsService zooBalanceStatisticsService;

    @Autowired
    private ZooRentExceptionService zooRentExceptionService;

    @Autowired
    private ZooOnAccountDayDetailService zooOnAccountDayDetailService;

    @Autowired
    private ZooEntertainOrdersService zooEntertainOrdersService;

    @Autowired
    private ZooOtaStatisticsService zooOtaStatisticsService;

    @Autowired
    private ZooPayStattisticsService zooPayStattisticsService;

    @Autowired
    private ZooRefundStatisticsService zooRefundStatisticsService;

    @Autowired
    private ZooBalanceDepositIndemnityService zooBalanceDepositIndemnityService;

    @Autowired
    private OrdersService ordersService;

    @ApiOperation(value = "对账列表金额、订单数统计",notes = "线上统计：lineMap，线下统计：offlineMap，不可退款统计：noRefundMap，订单总金额：totalPrice，订单总笔数：orderNum")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "startTime", value = "开始时间-(yyyy-MM-dd)", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "endTime", value = "结束时间-(yyyy-MM-dd)", required = false, dataType = "string", paramType = "query")
    })
    //@RequiresPermissions("zoo:onAccountHangupDetail:read")
    @RequestMapping(value = "/balanceListMoney", method = RequestMethod.GET)
    @ResponseBody
    public WebResult balanceListMoney(HttpServletRequest request,
                                      @RequestParam(required = false, value = "status",defaultValue = "1") Integer status,
                                      @RequestParam(required = false, value = "startTime") String startTime,
                                      @RequestParam(required = false, value = "endTime") String endTime) {
        startTime = formartDate(startTime);
        endTime = formartDate(endTime);

        Map lineMap = selectSum(request,startTime,endTime,1);
        Map offlineMap = selectSum(request,startTime,endTime,2);
        Map noRefundMap = selectSum(request,startTime,endTime,3);


        WebResult result = WebResult.getSuccessResult();

        if(lineMap == null){
            lineMap = new HashMap<String,Object>();
            lineMap.put("totalPrice",new BigDecimal(0.0));
            lineMap.put("orderNum",0);
        }
        if(offlineMap == null){
            offlineMap = new HashMap<String,Object>();
            offlineMap.put("totalPrice",new BigDecimal(0.0));
            offlineMap.put("orderNum",0);
        }
        if(noRefundMap == null){
            noRefundMap = new HashMap<String,Object>();
            noRefundMap.put("totalPrice",new BigDecimal(0.0));
            noRefundMap.put("orderNum",0);
        }

        result.put("lineMap",lineMap);
        result.put("offlineMap",offlineMap);
        result.put("noRefundMap",noRefundMap);

        BigDecimal totalPrice = BigDecimal.ZERO;
        if(lineMap!=null && lineMap.get("totalPrice")!=null){
            totalPrice=totalPrice.add(new BigDecimal(lineMap.get("totalPrice").toString()));
        }
        if(offlineMap!=null && offlineMap.get("totalPrice")!=null){
            totalPrice=totalPrice.add(new BigDecimal(offlineMap.get("totalPrice").toString()));
        }
        if(noRefundMap!=null && noRefundMap.get("totalPrice")!=null){
            totalPrice=totalPrice.add(new BigDecimal(noRefundMap.get("totalPrice").toString()));
        }

        int orderNum = 0;
        if(lineMap!=null && lineMap.get("orderNum")!=null){
            orderNum += new Integer(lineMap.get("orderNum").toString());
        }
        if(offlineMap!=null && offlineMap.get("orderNum")!=null){
            orderNum += new Integer(offlineMap.get("orderNum").toString());
        }
        if(noRefundMap !=null && noRefundMap.get("orderNum")!=null){
            orderNum += new Integer(noRefundMap.get("orderNum").toString());
        }

        result.put("orderNum",orderNum);
        result.put("totalPrice",totalPrice);

        return result;
    }

    private Map selectSum(HttpServletRequest request,String startTime,String endTime,Integer status){
        ZooBalanceStatisticsExample example = new ZooBalanceStatisticsExample();
        ZooBalanceStatisticsExample.Criteria criteria = example.createCriteria();
        try {
            if(StringUtils.isNotEmpty(startTime) && StringUtils.isNotEmpty(endTime)){
                criteria.andDayBetween(DateUtil.convertStringToDate(startTime + " 00:00:00","yyyy-MM-dd HH:mm:ss"),DateUtil.convertStringToDate(endTime + " 23:59:59","yyyy-MM-dd HH:mm:ss"));
            }else if(StringUtils.isNotEmpty(startTime) && StringUtils.isEmpty(endTime)){
                criteria.andDayGreaterThanOrEqualTo(DateUtil.convertStringToDate(startTime + " 00:00:00","yyyy-MM-dd HH:mm:ss"));
            }else if(StringUtils.isNotEmpty(endTime) && StringUtils.isEmpty(startTime)){
                criteria.andDayLessThanOrEqualTo(DateUtil.convertStringToDate(endTime + " 00:00:00","yyyy-MM-dd HH:mm:ss"));
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if(status == 1){
            //线上
            criteria.andLineOrOfflineNotEqualTo(2);
            criteria.andStatusRefundEqualTo(1);
        }else if(status == 2){
            //线下
            criteria.andLineOrOfflineEqualTo(2);
        }else if(status == 3){
            //不可退款（只有线上订单才计算不可退款）
            criteria.andLineOrOfflineNotEqualTo(2);
            criteria.andStatusRefundEqualTo(0);
        }
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        criteria.andOrganizerIdEqualTo(organizer.getOrganizerId());

        Map map = zooBalanceStatisticsService.selectSumPriceAndNumByExample(example);
        return map;
    }

    private String formartDate(String startTime){
        try {
            if(StringUtil.isEmpty(startTime)){
                return startTime;
            }
            DateUtil.convertStringToDate(startTime,"yyyy-MM-dd");
            return startTime;
        } catch (ParseException e) {
            Date d = new Date(Long.valueOf(startTime));
            return DateUtil.convertDateToString(d,"yyyy-MM-dd");
        }
    }

    @ApiOperation(value = "查询产品统计详情列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "size", value = "分页大小", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "startTime", value = "开始时间-(yyyy-MM-dd)", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "endTime", value = "结束时间-(yyyy-MM-dd)", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "status", value = "状态：1、线上，2、线下，3、不可退款", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "activityTypeId", value = "产品类型ID:0.最新活动; 1.滑雪度假;2.教练预约；3.超值票务；4.秒杀相关；5.训练营；6.自营票务；7.酒店；8.教练课程；9.储值卡；10.续时产品", required = false, dataType = "int", paramType = "query"),
    })
    //@RequiresPermissions("zoo:onAccountHangupDetail:read")
    @RequestMapping(value = "/detailStatistics", method = RequestMethod.GET)
    @ResponseBody
    public WebResult detailStatistics(HttpServletRequest request,
                          @RequestParam(required = false, defaultValue = "0", value = "page") int page,
                          @RequestParam(required = false, defaultValue = "0", value = "size") int size,
                          @RequestParam(required = false, value = "status",defaultValue = "1") Integer status,
                          @RequestParam(required = false, value = "activityTypeId") Integer activityTypeId,
                          @RequestParam(required = false, value = "startTime") String startTime,
                          @RequestParam(required = false, value = "endTime") String endTime) {
        ZooBalanceStatisticsExample example = new ZooBalanceStatisticsExample();
        ZooBalanceStatisticsExample.Criteria criteria = example.createCriteria();
        try {
            startTime = formartDate(startTime);
            endTime = formartDate(endTime);
            if(StringUtils.isNotEmpty(startTime) && StringUtils.isNotEmpty(endTime)){
                criteria.andDayBetween(DateUtil.convertStringToDate(startTime + " 00:00:00","yyyy-MM-dd HH:mm:ss"),DateUtil.convertStringToDate(endTime + " 23:59:59","yyyy-MM-dd HH:mm:ss"));
            }else if(StringUtils.isNotEmpty(startTime) && StringUtils.isEmpty(endTime)){
                criteria.andDayGreaterThanOrEqualTo(DateUtil.convertStringToDate(startTime + " 00:00:00","yyyy-MM-dd HH:mm:ss"));
            }else if(StringUtils.isNotEmpty(endTime) && StringUtils.isEmpty(startTime)){
                criteria.andDayLessThanOrEqualTo(DateUtil.convertStringToDate(endTime + " 00:00:00","yyyy-MM-dd HH:mm:ss"));
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if(activityTypeId!=null){
            criteria.andActivityTypeIdEqualTo(activityTypeId);
        }
        if(status == 1){
            //线上
            criteria.andLineOrOfflineNotEqualTo(2);
            criteria.andStatusRefundEqualTo(1);
        }else if(status == 2){
            //线下
            criteria.andLineOrOfflineEqualTo(2);
        }else if(status == 3){
            //不可退款（只有线上订单才计算不可退款）
            criteria.andLineOrOfflineNotEqualTo(2);
            criteria.andStatusRefundEqualTo(0);
        }

        Organizer organizer = WebUpmsContext.getOrgnization(request);
        criteria.andOrganizerIdEqualTo(organizer.getOrganizerId());
        criteria.andEquipmentIdIsNull();
        PageInfo<Map> list = zooBalanceStatisticsService.selectDetailStatisticsByExample(example,page,size);
        WebResult result = WebResult.getSuccessResult();
        result.put("list",list.getList());
        result.put("count",list.getTotal());
        return result;
    }


    private ZooBalanceStatisticsExample.Criteria createActivityExampleCriteria(ZooBalanceStatisticsExample activityExample,String startTime,String endTime,Integer status,Organizer organizer){
        ZooBalanceStatisticsExample.Criteria activityExampleCriteria = activityExample.createCriteria();
        try {
            if(StringUtils.isNotEmpty(startTime) && StringUtils.isNotEmpty(endTime)){
                activityExampleCriteria.andDayBetween(DateUtil.convertStringToDate(startTime + " 00:00:00","yyyy-MM-dd HH:mm:ss"),DateUtil.convertStringToDate(endTime + " 23:59:59","yyyy-MM-dd HH:mm:ss"));
            }else if(StringUtils.isNotEmpty(startTime) && StringUtils.isEmpty(endTime)){
                activityExampleCriteria.andDayGreaterThanOrEqualTo(DateUtil.convertStringToDate(startTime + " 00:00:00","yyyy-MM-dd HH:mm:ss"));
            }else if(StringUtils.isNotEmpty(endTime) && StringUtils.isEmpty(startTime)){
                activityExampleCriteria.andDayLessThanOrEqualTo(DateUtil.convertStringToDate(endTime + " 00:00:00","yyyy-MM-dd HH:mm:ss"));
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if(status == 1){
            //线上
            activityExampleCriteria.andLineOrOfflineNotEqualTo(2);
            activityExampleCriteria.andStatusRefundEqualTo(1);
        }else if(status == 2){
            //线下
            activityExampleCriteria.andLineOrOfflineEqualTo(2);
        }else if(status == 3){
            //不可退款（只有线上订单才计算不可退款）
            activityExampleCriteria.andLineOrOfflineNotEqualTo(2);
            activityExampleCriteria.andStatusRefundEqualTo(0);
        }
        activityExampleCriteria.andOrganizerIdEqualTo(organizer.getOrganizerId());
        return activityExampleCriteria;
    }

    @ApiOperation(value = "对账列表",notes = "对账列表返回值说明，产品对账列表：product，滑雪学校：schoolMaps,租赁物统计:rentMaps,其他：otherMaps ,支付统计：payMaps,退款数据/异常数据：refundMaps,<br/>产品类型:0.最新活动; 1.滑雪度假;2.教练预约；3.超值票务；4.秒杀相关；5.训练营；6.自营票务；7.酒店；8.教练课程；9.储值卡；10.续时产品",response = WebResult.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "status", value = "状态：1、线上，2、线下，3、不可退款", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "startTime", value = "开始时间-(yyyy-MM-dd)", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "endTime", value = "结束时间-(yyyy-MM-dd)", required = false, dataType = "string", paramType = "query"),
    })
    //@RequiresPermissions("zoo:onAccountHangupDetail:read")
    @RequestMapping(value = "/balanceList", method = RequestMethod.GET)
    @ResponseBody
    public WebResult balanceList(HttpServletRequest request,
                                 @RequestParam(required = false, value = "status",defaultValue = "1") Integer status,
                                 @RequestParam(required = false, value = "startTime") String startTime,
                                 @RequestParam(required = false, value = "endTime") String endTime) {

        startTime = formartDate(startTime);
        endTime = formartDate(endTime);

        Organizer organizer = WebUpmsContext.getOrgnization(request);
        ZooBalanceStatisticsExample activityExample = new ZooBalanceStatisticsExample();
        ZooBalanceStatisticsExample.Criteria activityExampleCriteria = createActivityExampleCriteria(activityExample, startTime, endTime, status, organizer);
        List<Integer> activityTypeIds = new ArrayList<Integer>();
        activityTypeIds.add(2);
        activityTypeIds.add(8);

        /**查询产品统计**/
        activityExampleCriteria.andActivityTypeIdNotIn(activityTypeIds);
        activityExampleCriteria.andEquipmentIdIsNull();
        List<Map> prodMaps = zooBalanceStatisticsService.selectStatisticsGroupActivityTypeIdByExample(activityExample);

        /**滑雪学校产品统计**/
        ZooBalanceStatisticsExample coachExample = new ZooBalanceStatisticsExample();
        ZooBalanceStatisticsExample.Criteria coachExampleCriteria = createActivityExampleCriteria(coachExample, startTime, endTime, status, organizer);
        coachExampleCriteria.andActivityTypeIdIn(activityTypeIds);
        coachExampleCriteria.andEquipmentIdIsNull();
        List<Map> schoolMaps = zooBalanceStatisticsService.selectStatisticsGroupActivityTypeIdByExample(coachExample);

        /**租赁物统计**/
        ZooRentExceptionExample zooRentExceptionExample = new ZooRentExceptionExample();
        ZooRentExceptionExample.Criteria zooRentExceptionExampleCriteria = zooRentExceptionExample.createCriteria();
        zooRentExceptionExampleCriteria.andOrganizerIdEqualTo(organizer.getOrganizerId());
        try {
            if(StringUtils.isNotEmpty(startTime) && StringUtils.isNotEmpty(endTime)){
                zooRentExceptionExampleCriteria.andDayBetween(DateUtil.convertStringToDate(startTime + " 00:00:00","yyyy-MM-dd HH:mm:ss"),DateUtil.convertStringToDate(endTime + " 23:59:59","yyyy-MM-dd HH:mm:ss"));
            }else if(StringUtils.isNotEmpty(startTime) && StringUtils.isEmpty(endTime)){
                zooRentExceptionExampleCriteria.andDayGreaterThanOrEqualTo(DateUtil.convertStringToDate(startTime + " 00:00:00","yyyy-MM-dd HH:mm:ss"));
            }else if(StringUtils.isNotEmpty(endTime) && StringUtils.isEmpty(startTime)){
                zooRentExceptionExampleCriteria.andDayLessThanOrEqualTo(DateUtil.convertStringToDate(endTime + " 00:00:00","yyyy-MM-dd HH:mm:ss"));
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if(status == 1){
            //线上
            zooRentExceptionExampleCriteria.andLineOrOfflineNotEqualTo(2);
            zooRentExceptionExampleCriteria.andStatusRefundEqualTo(1);
        }else if(status == 2){
            //线下
            zooRentExceptionExampleCriteria.andLineOrOfflineEqualTo(2);
        }else if(status == 3){
            //不可退款（只有线上订单才计算不可退款）
            zooRentExceptionExampleCriteria.andLineOrOfflineNotEqualTo(2);
            zooRentExceptionExampleCriteria.andStatusRefundEqualTo(0);
        }
        List<Map> rentMaps = zooRentExceptionService.selectStatisticByExample(zooRentExceptionExample);

        /**挂帐**/
        ZooOnAccountDayDetailExample dayDetailExample = new ZooOnAccountDayDetailExample();
        ZooOnAccountDayDetailExample.Criteria zooOnAccountDayDetailExampleCriteria = dayDetailExample.createCriteria();
        zooOnAccountDayDetailExampleCriteria.andOrganizerIdEqualTo(organizer.getOrganizerId());
        try {
            if(StringUtils.isNotEmpty(startTime) && StringUtils.isNotEmpty(endTime)){
                zooOnAccountDayDetailExampleCriteria.andDayBetween(DateUtil.convertStringToDate(startTime + " 00:00:00","yyyy-MM-dd HH:mm:ss"),DateUtil.convertStringToDate(endTime + " 23:59:59","yyyy-MM-dd HH:mm:ss"));
            }else if(StringUtils.isNotEmpty(startTime) && StringUtils.isEmpty(endTime)){
                zooOnAccountDayDetailExampleCriteria.andDayGreaterThanOrEqualTo(DateUtil.convertStringToDate(startTime + " 00:00:00","yyyy-MM-dd HH:mm:ss"));
            }else if(StringUtils.isNotEmpty(endTime) && StringUtils.isEmpty(startTime)){
                zooOnAccountDayDetailExampleCriteria.andDayLessThanOrEqualTo(DateUtil.convertStringToDate(endTime + " 00:00:00","yyyy-MM-dd HH:mm:ss"));
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        List<Map> hungUpMaps = zooOnAccountDayDetailService.selectStatisticByExample(dayDetailExample);

        /**招待**/
        ZooEntertainOrdersExample ordersExample = new ZooEntertainOrdersExample();
        ZooEntertainOrdersExample.Criteria ordersExampleCriteria = ordersExample.createCriteria();
        ordersExampleCriteria.andOrganizerIdEqualTo(organizer.getOrganizerId());
        try {
            if(StringUtils.isNotEmpty(startTime) && StringUtils.isNotEmpty(endTime)){
                ordersExampleCriteria.andOrderTimeBetween(DateUtil.convertStringToDate(startTime + " 00:00:00","yyyy-MM-dd HH:mm:ss"),DateUtil.convertStringToDate(endTime + " 23:59:59","yyyy-MM-dd HH:mm:ss"));
            }else if(StringUtils.isNotEmpty(startTime) && StringUtils.isEmpty(endTime)){
                ordersExampleCriteria.andOrderTimeGreaterThanOrEqualTo(DateUtil.convertStringToDate(startTime + " 00:00:00","yyyy-MM-dd HH:mm:ss"));
            }else if(StringUtils.isNotEmpty(endTime) && StringUtils.isEmpty(startTime)){
                ordersExampleCriteria.andOrderTimeLessThanOrEqualTo(DateUtil.convertStringToDate(endTime + " 00:00:00","yyyy-MM-dd HH:mm:ss"));
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        List<Map> entertainMaps = zooEntertainOrdersService.selectStatisticByExample(ordersExample);

        /**渠道（ota）**/
        ZooOtaStatisticsExample otaStatisticsExample = new ZooOtaStatisticsExample();
        ZooOtaStatisticsExample.Criteria otaStatisticsExampleCriteria = otaStatisticsExample.createCriteria();
        otaStatisticsExampleCriteria.andOrganizerIdEqualTo(organizer.getOrganizerId());
        try {
            if(StringUtils.isNotEmpty(startTime) && StringUtils.isNotEmpty(endTime)){
                otaStatisticsExampleCriteria.andDayBetween(DateUtil.convertStringToDate(startTime + " 00:00:00","yyyy-MM-dd HH:mm:ss"),DateUtil.convertStringToDate(endTime + " 23:59:59","yyyy-MM-dd HH:mm:ss"));
            }else if(StringUtils.isNotEmpty(startTime) && StringUtils.isEmpty(endTime)){
                otaStatisticsExampleCriteria.andDayGreaterThanOrEqualTo(DateUtil.convertStringToDate(startTime + " 00:00:00","yyyy-MM-dd HH:mm:ss"));
            }else if(StringUtils.isNotEmpty(endTime) && StringUtils.isEmpty(startTime)){
                otaStatisticsExampleCriteria.andDayLessThanOrEqualTo(DateUtil.convertStringToDate(endTime + " 00:00:00","yyyy-MM-dd HH:mm:ss"));
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        List<Map> otaMaps = zooOtaStatisticsService.selectStatisticByExample(otaStatisticsExample);

        List<Map> otherMaps = new ArrayList<Map>();
        if(hungUpMaps!=null && hungUpMaps.size()>0 && hungUpMaps.get(0)!=null){
            hungUpMaps.get(0).put("name","挂账");
            otherMaps.add(hungUpMaps.get(0));
        }
        if(entertainMaps!=null && entertainMaps.size()>0 && entertainMaps.get(0)!=null){
            entertainMaps.get(0).put("name","招待");
            otherMaps.add(entertainMaps.get(0));
        }
        if(otaMaps!=null && otaMaps.size()>0 && otaMaps.get(0)!=null){
            otaMaps.get(0).put("name","ota");
            otherMaps.add(otaMaps.get(0));
        }

        /**支付统计**/
        ZooPayStattisticsExample payStattisticsExample = new ZooPayStattisticsExample();
        ZooPayStattisticsExample.Criteria payStattisticsExampleCriteria = payStattisticsExample.createCriteria();
        payStattisticsExampleCriteria.andOrganizerIdEqualTo(organizer.getOrganizerId());
        try {
            if(StringUtils.isNotEmpty(startTime) && StringUtils.isNotEmpty(endTime)){
                payStattisticsExampleCriteria.andDayBetween(DateUtil.convertStringToDate(startTime + " 00:00:00","yyyy-MM-dd HH:mm:ss"),DateUtil.convertStringToDate(endTime + " 23:59:59","yyyy-MM-dd HH:mm:ss"));
            }else if(StringUtils.isNotEmpty(startTime) && StringUtils.isEmpty(endTime)){
                payStattisticsExampleCriteria.andDayGreaterThanOrEqualTo(DateUtil.convertStringToDate(startTime + " 00:00:00","yyyy-MM-dd HH:mm:ss"));
            }else if(StringUtils.isNotEmpty(endTime) && StringUtils.isEmpty(startTime)){
                payStattisticsExampleCriteria.andDayLessThanOrEqualTo(DateUtil.convertStringToDate(endTime + " 00:00:00","yyyy-MM-dd HH:mm:ss"));
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        List<Map> payMaps = zooPayStattisticsService.selectStatisticByExample(payStattisticsExample);

        /**产品退款统计**/
        ZooRefundStatisticsExample refundStatisticsExample = new ZooRefundStatisticsExample();
        ZooRefundStatisticsExample.Criteria refundStatisticsExampleCriteria = refundStatisticsExample.createCriteria();
        refundStatisticsExampleCriteria.andOrganizerIdEqualTo(organizer.getOrganizerId());
        refundStatisticsExampleCriteria.andEquipmentIdIsNull();
        try {
            if(StringUtils.isNotEmpty(startTime) && StringUtils.isNotEmpty(endTime)){
                refundStatisticsExampleCriteria.andDayBetween(DateUtil.convertStringToDate(startTime + " 00:00:00","yyyy-MM-dd HH:mm:ss"),DateUtil.convertStringToDate(endTime + " 23:59:59","yyyy-MM-dd HH:mm:ss"));
            }else if(StringUtils.isNotEmpty(startTime) && StringUtils.isEmpty(endTime)){
                refundStatisticsExampleCriteria.andDayGreaterThanOrEqualTo(DateUtil.convertStringToDate(startTime + " 00:00:00","yyyy-MM-dd HH:mm:ss"));
            }else if(StringUtils.isNotEmpty(endTime) && StringUtils.isEmpty(startTime)){
                refundStatisticsExampleCriteria.andDayLessThanOrEqualTo(DateUtil.convertStringToDate(endTime + " 00:00:00","yyyy-MM-dd HH:mm:ss"));
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        List<Map> proRefundMaps = zooRefundStatisticsService.selectStatisticByExample(refundStatisticsExample);

        /**租赁退款统计**/
        ZooRefundStatisticsExample rentRefundStatisticsExample = new ZooRefundStatisticsExample();
        ZooRefundStatisticsExample.Criteria rentRefundStatisticsExampleCriteria = rentRefundStatisticsExample.createCriteria();
        rentRefundStatisticsExampleCriteria.andOrganizerIdEqualTo(organizer.getOrganizerId());
        rentRefundStatisticsExampleCriteria.andEquipmentIdIsNotNull();
        try {
            if(StringUtils.isNotEmpty(startTime) && StringUtils.isNotEmpty(endTime)){
                rentRefundStatisticsExampleCriteria.andDayBetween(DateUtil.convertStringToDate(startTime + " 00:00:00","yyyy-MM-dd HH:mm:ss"),DateUtil.convertStringToDate(endTime + " 23:59:59","yyyy-MM-dd HH:mm:ss"));
            }else if(StringUtils.isNotEmpty(startTime) && StringUtils.isEmpty(endTime)){
                rentRefundStatisticsExampleCriteria.andDayGreaterThanOrEqualTo(DateUtil.convertStringToDate(startTime + " 00:00:00","yyyy-MM-dd HH:mm:ss"));
            }else if(StringUtils.isNotEmpty(endTime) && StringUtils.isEmpty(startTime)){
                rentRefundStatisticsExampleCriteria.andDayLessThanOrEqualTo(DateUtil.convertStringToDate(endTime + " 00:00:00","yyyy-MM-dd HH:mm:ss"));
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        List<Map> rentRefundMaps = zooRefundStatisticsService.selectStatisticByExample(refundStatisticsExample);

        /**押金未退统计**/
        ZooBalanceDepositIndemnityExample balanceDepositIndemnityExample = new ZooBalanceDepositIndemnityExample();
        ZooBalanceDepositIndemnityExample.Criteria balanceDepositIndemnityExampleCriteria = balanceDepositIndemnityExample.createCriteria();
        balanceDepositIndemnityExampleCriteria.andOrganizerIdEqualTo(organizer.getOrganizerId());
        balanceDepositIndemnityExampleCriteria.andEquipmentIdIsNotNull();
        try {
            if(StringUtils.isNotEmpty(startTime) && StringUtils.isNotEmpty(endTime)){
                balanceDepositIndemnityExampleCriteria.andIndemnityTimeBetween(DateUtil.convertStringToDate(startTime + " 00:00:00","yyyy-MM-dd HH:mm:ss"),DateUtil.convertStringToDate(endTime + " 23:59:59","yyyy-MM-dd HH:mm:ss"));
            }else if(StringUtils.isNotEmpty(startTime) && StringUtils.isEmpty(endTime)){
                balanceDepositIndemnityExampleCriteria.andIndemnityTimeGreaterThanOrEqualTo(DateUtil.convertStringToDate(startTime + " 00:00:00","yyyy-MM-dd HH:mm:ss"));
            }else if(StringUtils.isNotEmpty(endTime) && StringUtils.isEmpty(startTime)){
                balanceDepositIndemnityExampleCriteria.andIndemnityTimeLessThanOrEqualTo(DateUtil.convertStringToDate(endTime + " 00:00:00","yyyy-MM-dd HH:mm:ss"));
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        List<Map> depositNoRefundMaps = zooBalanceDepositIndemnityService.selectStatisticByExample(balanceDepositIndemnityExample);

        List<Map> refundMaps = new ArrayList<Map>();
        if(proRefundMaps!=null && proRefundMaps.size()>0 && proRefundMaps.get(0)!=null){
            proRefundMaps.get(0).put("name","产品退款");
            refundMaps.add(proRefundMaps.get(0));
        }
        if(rentRefundMaps!=null && rentRefundMaps.size()>0 && rentRefundMaps.get(0)!=null){
            rentRefundMaps.get(0).put("name","租赁物退款");
            refundMaps.add(rentRefundMaps.get(0));
        }
        if(depositNoRefundMaps!=null && depositNoRefundMaps.size()>0 && depositNoRefundMaps.get(0)!=null){
            depositNoRefundMaps.get(0).put("name","押金赔偿");
            depositNoRefundMaps.get(0).put("money",depositNoRefundMaps.get(0).get("indemnity"));
            refundMaps.add(depositNoRefundMaps.get(0));
        }

        WebResult<Object> result = WebResult.getSuccessResult();

        sumNumAndMoney(result,"product",prodMaps);
        sumNumAndMoney(result,"school",schoolMaps);
        sumRentNumAndMoney(result,"rentMaps",rentMaps);

        result.put("product",prodMaps);
        result.put("schoolMaps",schoolMaps);
        result.put("rentMaps",rentMaps);
        result.put("otherMaps",otherMaps);
        result.put("payMaps",payMaps);
        result.put("refundMaps",refundMaps);

        return result;
    }

    private void sumRentNumAndMoney(WebResult<Object> result,String modName,List<Map> list){
        if(list!=null){
            BigDecimal bgd = new BigDecimal(0);
            int num = 0;
            for(Map m : list){
                bgd = bgd.add(new BigDecimal(MapUtils.getDoubleValue(m,"totalMoney")));
                if(MapUtils.getIntValue(m,"totalNum",-1)==-1){
                    num += MapUtils.getIntValue(m,"totalNum");
                }else{
                    num += MapUtils.getIntValue(m,"totalNum");
                }
            }
            result.put(modName+"_num",num);
            result.put(modName+"_money",bgd.setScale(2,BigDecimal.ROUND_HALF_DOWN));
        }else{
            result.put(modName+"_num",0);
            result.put(modName+"_money","0.00");
        }
    }

    private void sumNumAndMoney(WebResult<Object> result,String modName,List<Map> list){
        if(list!=null){
            BigDecimal bgd = new BigDecimal(0);
            int num = 0;
            for(Map m : list){
                bgd = bgd.add(new BigDecimal(MapUtils.getDoubleValue(m,"totalPrice")));
                if(MapUtils.getIntValue(m,"orderNum",-1)==-1){
                    num += MapUtils.getIntValue(m,"orderNum");
                }else{
                    num += MapUtils.getIntValue(m,"orderNum");
                }
            }
            result.put(modName+"_num",num);
            result.put(modName+"_money",bgd.setScale(2,BigDecimal.ROUND_HALF_DOWN));
        }else{
            result.put(modName+"_num",0);
            result.put(modName+"_money","0.00");
        }
    }

    @ApiOperation(value = "申请发票",response = WebResult.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "settlementIds", value = "对账ID，多个ID之间用逗号隔开", required = false, dataType = "string", paramType = "query"),
    })
    //@RequiresPermissions("zoo:onAccountHangupDetail:read")
    @RequestMapping(value = "/applyInvoices", method = RequestMethod.GET)
    @ResponseBody
    public WebResult applyInvoices(HttpServletRequest request,String settlementIds) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);

        String[] split = settlementIds.split(",");

        if(split==null || split.length==0){
            return null;
        }
        ArrayList ids = new ArrayList(split.length);

        for (String id : split){
            ids.add(id);
        }

        List<Integer> invoiceStatusList = new ArrayList<Integer>();
        invoiceStatusList.add(1);
        invoiceStatusList.add(3);
        ZooSettlementDetailExample example = new ZooSettlementDetailExample();
        ZooSettlementDetailExample.Criteria criteria = example.createCriteria();
        criteria.andIdIn(ids);
        criteria.andInvoiceStatusIn(invoiceStatusList);
        List<ZooSettlementDetail> zooSettlementDetails = zooSettlementDetailService.selectByExample(example);
        if(zooSettlementDetails!=null  && zooSettlementDetails.size()>0){
            return WebResult.getErrorResult(WebResult.Code.HAVE_APPLY_INVOICE);
        }

        BigDecimal bigDecimal = zooSettlementInvoiceService.applyInvoices(settlementIds, organizer.getOrganizerId(), organizer.getName());
        if(bigDecimal!=null){
            WebResult result = WebResult.getSuccessResult();
            result.put("money",bigDecimal);
            return result;
        }
        return WebResult.getErrorResult(WebResult.Code.FINANCE_APPLY_INVOICE);
    }
    @ApiOperation(value = "完成对账",response = WebResult.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "settlementIds", value = "对账ID，多个ID之间用逗号隔开", required = false, dataType = "string", paramType = "query"),
    })
    @RequestMapping(value = "/finishBalances", method = RequestMethod.GET)
    @ResponseBody
    public WebResult finishBalances(HttpServletRequest request,String settlementIds) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);

        String[] split = settlementIds.split(",");

        if(split==null || split.length==0){
            return null;
        }
        ArrayList ids = new ArrayList(split.length);

        for (String id : split){
            ids.add(id);
        }
        //1、出账中，2、已出账，3、完成对账，4、转账中，5、转账完成
        List<Integer> statusList = new ArrayList<Integer>();
        statusList.add(3);
        statusList.add(4);
        statusList.add(5);
        ZooSettlementDetailExample example = new ZooSettlementDetailExample();
        ZooSettlementDetailExample.Criteria criteria = example.createCriteria();
        criteria.andIdIn(ids);
        criteria.andStatusIn(statusList);
        List<ZooSettlementDetail> zooSettlementDetails = zooSettlementDetailService.selectByExample(example);
        if(zooSettlementDetails!=null  && zooSettlementDetails.size()>0){
            return WebResult.getErrorResult(WebResult.Code.HAVE_FINISH_BALANCE);
        }
        //1、出账中，2、已出账，3、完成对账，4、转账中，5、转账完成
        ZooSettlementDetailExample example2 = new ZooSettlementDetailExample();
        ZooSettlementDetailExample.Criteria criteria2 = example2.createCriteria();
        criteria2.andIdIn(ids);
        criteria2.andStatusEqualTo(1);
        List<ZooSettlementDetail> zooSettlementDetails2 = zooSettlementDetailService.selectByExample(example2);
        if(zooSettlementDetails2!=null  && zooSettlementDetails2.size()>0){
            return WebResult.getErrorResult(WebResult.Code.HAVE_UN_FINISH_BALANCE);
        }

        ZooSettlementDetailExample example1 = new ZooSettlementDetailExample();
        ZooSettlementDetailExample.Criteria criteria1 = example1.createCriteria();
        criteria1.andIdIn(ids);
        criteria1.andStatusEqualTo(2);
        ZooSettlementDetail zooSettlementDetail=new ZooSettlementDetail();
        zooSettlementDetail.setStatus(3);
        zooSettlementDetail.setLastUpdateTime(new Date());
        zooSettlementDetailService.updateByExampleSelective(zooSettlementDetail,example1);

        WebResult result = WebResult.getSuccessResult();
        result.put("CODE",0);
        return result;
    }

    @ApiOperation(value = "转账列表统计金额",notes = "返回：累计转账金额-totalTransMoney，转账次数-transCount，未转账金额-noTotalTransMoney",response = WebResult.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "isHotel", value = "是否酒店(1、非酒店，2、酒店)", required = false, dataType = "int", paramType = "query")
    })
    //@RequiresPermissions("zoo:onAccountHangupDetail:read")
    @RequestMapping(value = "/staticsMoney", method = RequestMethod.GET)
    @ResponseBody
    public WebResult staticsMoney(HttpServletRequest request,@RequestParam(required = false, value = "isHotel",defaultValue = "1") Integer isHotel) {
        ZooSettlementDetailExample example = new ZooSettlementDetailExample();
        ZooSettlementDetailExample.Criteria criteria = example.createCriteria();

        Organizer organizer = WebUpmsContext.getOrgnization(request);
        criteria.andOrganizerIdEqualTo(organizer.getOrganizerId());
        criteria.andStatusEqualTo(5);
        criteria.andIsHotelEqualTo(isHotel);//非酒店

        //累计转账金额
        BigDecimal totalTransMoney = zooSettlementDetailService.totalTransferMoney(example);
        if(totalTransMoney==null){
            totalTransMoney = new BigDecimal(0);
        }

        //转账次数
        ZooSettlementTransferAccountsExample transferAccountsExample = new ZooSettlementTransferAccountsExample();
        transferAccountsExample.createCriteria().andOrganizerIdEqualTo(organizer.getOrganizerId()).andStatusEqualTo(1).andIsHotelEqualTo(isHotel);
        int count = zooSettlementTransferAccountsService.countByExample(transferAccountsExample);

        //未转账金额
        ZooSettlementDetailExample noTransExample = new ZooSettlementDetailExample();
        ZooSettlementDetailExample.Criteria criteria1 = noTransExample.createCriteria();
        criteria1.andOrganizerIdEqualTo(organizer.getOrganizerId()).andStatusNotEqualTo(5).andIsHotelEqualTo(isHotel);
        /*String day = DateUtil.convertDateToString(new Date(),"yyyy-MM-dd");
        try {
            criteria1.andStartTimeLessThan(DateUtil.convertStringToDate(day+" 00:00:00","yyyy-MM-dd HH:mm:ss"));
        } catch (ParseException e) {
            e.printStackTrace();
        }*/
        BigDecimal noTotalTransMoney = zooSettlementDetailService.totalTransferMoney(noTransExample);
        if(noTotalTransMoney==null){
            noTotalTransMoney = new BigDecimal(0);
        }

        WebResult result = WebResult.getSuccessResult();
        result.put("totalTransMoney",totalTransMoney.setScale(2,BigDecimal.ROUND_HALF_DOWN));
        result.put("transCount",count);
        result.put("noTotalTransMoney",noTotalTransMoney.setScale(2,BigDecimal.ROUND_HALF_DOWN));
        return result;
    }
    @ApiOperation(value = "转账列表备注",notes = "返回：备注",response = WebResult.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "isHotel", value = "是否酒店(1、非酒店，2、酒店)", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "id", value = "转账记录id", required = false, dataType = "int", paramType = "query")
    })
    //@RequiresPermissions("zoo:onAccountHangupDetail:read")
    @RequestMapping(value = "/queryRemark", method = RequestMethod.GET)
    @ResponseBody
    public WebResult queryRemark(HttpServletRequest request,@RequestParam(required = false, value = "isHotel",defaultValue = "1") Integer isHotel
            ,@RequestParam(required = false, value = "id",defaultValue = "1") Integer id) {
        ZooSettlementDetailExample zooSettlementDetailExample =new ZooSettlementDetailExample();
        zooSettlementDetailExample.createCriteria().andIdEqualTo(id);
        List<ZooSettlementDetail> zooSettlementDetails= zooSettlementDetailService.selectByExample(zooSettlementDetailExample);

        WebResult result = WebResult.getSuccessResult();
        result.put("remark", zooSettlementDetails.get(0).getRemark());
        return result;
    }
    @ApiOperation(value = "转账列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "size", value = "分页大小", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "startTime", value = "开始时间-(yyyy-MM-dd)", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "endTime", value = "结束时间-(yyyy-MM-dd)", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "status", value = "转账状态-(0:全部，2:转款中，3:转款完成，4:转账失败)", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "invoiceStatus", value = "发票状态(-1、全部,0、未开发发票，1、已申请，2、驳回，3、已处理)", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "isHotel", value = "是否酒店(1、非酒店，2、酒店)", required = false, dataType = "int", paramType = "query")
    })
    //@RequiresPermissions("zoo:onAccountHangupDetail:read")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ResponseBody
    public WebResult list(HttpServletRequest request,
            @RequestParam(required = false, defaultValue = "0", value = "page") int page,
            @RequestParam(required = false, defaultValue = "10", value = "size") int size,
            @RequestParam(required = false, value = "startTime") String startTime,
            @RequestParam(required = false, value = "endTime") String endTime,
            @RequestParam(required = false, value = "status",defaultValue = "0") Integer status,
            @RequestParam(required = false, value = "invoiceStatus",defaultValue = "-1") Integer invoiceStatus,
            @RequestParam(required = false, value = "isHotel",defaultValue = "1") Integer isHotel) {
        startTime = formartDate(startTime);
        endTime = formartDate(endTime);

        ZooSettlementDetailExample example = new ZooSettlementDetailExample();
        example.setOrderByClause("start_time desc,end_time asc");
        ZooSettlementDetailExample.Criteria criteria = example.createCriteria();
        criteria.andIsHotelEqualTo(isHotel);
        try {
            if(StringUtils.isNotEmpty(startTime) && StringUtils.isNotEmpty(endTime)){
                criteria.andStartTimeBetween(DateUtil.convertStringToDate(startTime + " 00:00:00","yyyy-MM-dd HH:mm:ss"),DateUtil.convertStringToDate(endTime + " 23:59:59","yyyy-MM-dd HH:mm:ss"));
                criteria.andEndTimeBetween(DateUtil.convertStringToDate(startTime + " 00:00:00","yyyy-MM-dd HH:mm:ss"),DateUtil.convertStringToDate(endTime + " 23:59:59","yyyy-MM-dd HH:mm:ss"));
            }else if(StringUtils.isNotEmpty(startTime) && StringUtils.isEmpty(endTime)){
                criteria.andStartTimeGreaterThanOrEqualTo(DateUtil.convertStringToDate(startTime + " 00:00:00","yyyy-MM-dd HH:mm:ss"));
            }else if(StringUtils.isNotEmpty(endTime) && StringUtils.isEmpty(startTime)){
                criteria.andEndTimeLessThanOrEqualTo(DateUtil.convertStringToDate(endTime + " 00:00:00","yyyy-MM-dd HH:mm:ss"));
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if(status!=0){
            criteria.andStatusEqualTo(status);
        }

        if(invoiceStatus!=-1){
            criteria.andInvoiceStatusEqualTo(invoiceStatus);
        }

        Organizer organizer = WebUpmsContext.getOrgnization(request);
        criteria.andOrganizerIdEqualTo(organizer.getOrganizerId());

        List<ZooSettlementDetail> list = zooSettlementDetailService.selectByExampleForStartPage(example, page, size);
        if(list!=null&&list.size()>0){
            for (int i = 0; i < list.size(); i++) {
                ZooSettlementDetail ZooSettlementDetail=list.get(i);
                Date endCyleDate=ZooSettlementDetail.getEndTime();
                ZooSettlementDetail.setEndTime(DateUtil.addSecond(endCyleDate,-1));
            }
        }
        int count = zooSettlementDetailService.countByExample(example);
        WebResult result = WebResult.getSuccessResult();
        result.put("list",list);
        result.put("count",count);
        return result;
    }


    @ApiOperation(value = "消费记录")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "startTime", value = "开始时间-(yyyy-MM-dd HH:mm:ss)", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "endTime", value = "结束时间-(yyyy-MM-dd HH:mm:ss)", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "cashierId", value = "收银员ID", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "cashierName", value = "收银员名称", required = false, dataType = "string", paramType = "query"),
    })
    @RequestMapping(value = "/consumelist", method = RequestMethod.GET)
    @ResponseBody
    public WebResult consumelist(HttpServletRequest request,
                          @RequestParam(required = false, value = "startTime") String startTime,
                          @RequestParam(required = false, value = "endTime") String endTime,
                          @RequestParam(required = false, value = "cashierId") Integer cashierId,
                          @RequestParam(required = false, value = "cashierName") String cashierName) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        Integer orgId=organizer.getOrganizerId();
        Date startDate=null;
        Date endDate=null;
        try {
            if(startTime!=null){
                startDate=DateUtil.convertStringToDate(startTime,"yyyy-MM-dd HH:mm:ss");
            }
            if(endTime!=null){
                endDate=DateUtil.convertStringToDate(endTime,"yyyy-MM-dd HH:mm:ss");
            }
        } catch (ParseException e) {
            e.printStackTrace();
            startDate=null;
            endDate=null;
        }
        //产品统计
        List<Map<String,Object>> ordersB=ordersService.selectOrdersGroupByActivity(startDate, endDate, orgId, cashierId,cashierName);
        List<Map<String,Object>> orders=new ArrayList<Map<String,Object>>();
        //初始化
        orders.add(getProductInit("最新活动", "0"));
        orders.add(getProductInit("滑雪度假", "1"));
        orders.add(getProductInit("教练预约", "2"));
        orders.add(getProductInit("超值票务", "3"));
        orders.add(getProductInit("会员卡", "4"));
        orders.add(getProductInit("训练营", "5"));
        orders.add(getProductInit("自营票务", "6"));
        orders.add(getProductInit("酒店预定", "7"));
        orders.add(getProductInit("教练课程", "8"));
        orders.add(getProductInit("储值卡", "9"));
        orders.add(getProductInit("续时产品", "10"));
        if(ordersB!=null&&ordersB.size()>0){
            for (int i = 0; i < ordersB.size(); i++) {
                Map<String,Object> map=ordersB.get(i);
                String typeId=map.get("type_id").toString();
                ((List<Map<String,Object>>)orders.get(Integer.parseInt(typeId)).get("lists")).add(map);
            }
        }
        //租赁物统计
        List<Map<String,Object>> rentOrders=ordersService.selectRentOrdersGroupByActivity(startDate, endDate, orgId, cashierId,cashierName);

        //押金赔偿
        ZooBalanceDepositIndemnityExample indemnityExample = new ZooBalanceDepositIndemnityExample();
        ZooBalanceDepositIndemnityExample.Criteria indemnityExampleCriteria = indemnityExample.createCriteria();
        if(startDate!=null){
            indemnityExampleCriteria.andIndemnityTimeGreaterThanOrEqualTo(startDate);
        }
        if(endDate !=null){
            indemnityExampleCriteria.andIndemnityTimeLessThan(endDate);
        }
        if(cashierName!=null){
            indemnityExampleCriteria.andOperatorNameLike("%"+cashierName+"%");
        }
        if(cashierId!=null){
            indemnityExampleCriteria.andOperatorIdEqualTo(cashierId);
        }
        if(cashierName!=null){
            indemnityExampleCriteria.andOperatorNameEqualTo(cashierName);
        }
        //List<ZooBalanceDepositIndemnity> depositIndemnityList = zooBalanceDepositIndemnityService.selectByExample(indemnityExample);
        List<Map> depositPayOrders = zooBalanceDepositIndemnityService.selectStatisticByExample(indemnityExample);

        //押金未退
        List<Map<String,Object>> depositUnRefundOrders=ordersService.selectTotalMoneyFromDeposit(startDate, endDate, orgId, cashierId,cashierName);

        List<Map<String,Object>> exceptionOrders=new ArrayList<Map<String,Object>>();
        if(depositPayOrders!=null&&depositPayOrders.size()>0){
            Map<String,Object> map=new HashMap<String,Object>();
            map.put("name","押金赔偿");
            map.put("totalMoney",depositPayOrders.get(0).get("indemnity"));
            exceptionOrders.add(map);
        }else{
            Map<String,Object> map=new HashMap<String,Object>();
            map.put("name","押金赔偿");
            map.put("totalMoney",0);
            exceptionOrders.add(map);
        }
        if(depositUnRefundOrders!=null&&depositUnRefundOrders.size()>0){
            Map<String,Object> map=new HashMap<String,Object>();
            map.put("name","未退押金");
            map.put("totalMoney",depositUnRefundOrders.get(0).get("totalMoney"));
            exceptionOrders.add(map);
        }else{
            Map<String,Object> map=new HashMap<String,Object>();
            map.put("name","未退押金");
            map.put("totalMoney",0);
            exceptionOrders.add(map);
        }
        List<Map> ordersDistributionStatictis = ordersService.getOrdersPayStatictis(startDate, endDate, 100, cashierName, cashierId, orgId);
        List<Map> ordersPayStatictis = ordersService.getOrdersPayStatictis(startDate, endDate, null, cashierName, cashierId, orgId);
        //组装线上支付方式List
        List<Map> payOrders=new ArrayList<Map>();
        Map<String,Object> resultMap=new HashMap<String,Object>();

        List<Map<String,Object>> onlineList=new ArrayList<Map<String,Object>>();
        onlineList.add(getinitPayMap("线上微信", BigDecimal.ZERO));
        onlineList.add(getinitPayMap("线上储值支付", BigDecimal.ZERO));
        onlineList.add(getinitPayMap("分销商支付", BigDecimal.ZERO));
        List<Map<String,Object>> offlineList=new ArrayList<Map<String,Object>>();
        offlineList.add(getinitPayMap("线下现金", BigDecimal.ZERO));
        offlineList.add(getinitPayMap("线下支付宝", BigDecimal.ZERO));
        offlineList.add(getinitPayMap("线下刷卡", BigDecimal.ZERO));
        offlineList.add(getinitPayMap("线下微信", BigDecimal.ZERO));
        offlineList.add(getinitPayMap("线下预售", BigDecimal.ZERO));
        //offlineList.add(getinitPayMap("线下挂账", BigDecimal.ZERO));
        offlineList.add(getinitPayMap("线下储值支付", BigDecimal.ZERO));
        offlineList.add(getinitPayMap("线下挂账支付", BigDecimal.ZERO));
        offlineList.add(getinitPayMap("线下招待支付", BigDecimal.ZERO));


        BigDecimal onlineMoney=BigDecimal.ZERO;
        BigDecimal offlineMoney=BigDecimal.ZERO;
        BigDecimal totalMoney=BigDecimal.ZERO;
        if(ordersPayStatictis!=null&&ordersPayStatictis.size()>0){
            for (int i = 0; i < ordersPayStatictis.size(); i++) {
                if(ordersPayStatictis.get(i).get("pay_type_id")==null){
                    continue;
                }
                String payType=ordersPayStatictis.get(i).get("pay_type_id").toString();
                BigDecimal money=(BigDecimal)ordersPayStatictis.get(i).get("totalMoney");
                if("1".equals(payType)){//   有问题
                    onlineList.get(0).put("money",money);
                    onlineMoney=onlineMoney.add(money);
                }
                if("7".equals(payType)){
                    onlineList.get(1).put("money",money);
                    onlineMoney=onlineMoney.add(money);
                }


                if("0".equals(payType)){
                    offlineList.get(0).put("money",money);
                    offlineMoney=offlineMoney.add(money);
                }
                if("2".equals(payType)){
                    offlineList.get(1).put("money",money);
                    offlineMoney=offlineMoney.add(money);
                }
                if("3".equals(payType)){
                    offlineList.get(2).put("money",money);
                    offlineMoney=offlineMoney.add(money);
                }
                if("4".equals(payType)){
                    offlineList.get(3).put("money",money);
                    offlineMoney=offlineMoney.add(money);
                }
                if("5".equals(payType)){
                    offlineList.get(4).put("money",money);
                    offlineMoney=offlineMoney.add(money);
                }
//                if("6".equals(payType)){
//                    offlineList.get(5).put("money",money);
//                    offlineMoney=offlineMoney.add(money);
//                }
                if("8".equals(payType)){
                    offlineList.get(5).put("money",money);
                    offlineMoney=offlineMoney.add(money);
                }
                if("9".equals(payType)){
                    offlineList.get(6).put("money",money);
                    offlineMoney=offlineMoney.add(money);
                }
                if("10".equals(payType)){
                    offlineList.get(7).put("money",money);
                    offlineMoney=offlineMoney.add(money);
                }
            }

        }
        if(ordersDistributionStatictis!=null&&ordersDistributionStatictis.size()>0){
            BigDecimal money=(BigDecimal)ordersDistributionStatictis.get(0).get("totalMoney");
            onlineList.get(2).put("money",money);
            onlineMoney=onlineMoney.add(money);
        }

        totalMoney=totalMoney.add(onlineMoney);
        totalMoney=totalMoney.add(offlineMoney);
        Map<String,Object> onlineMap=new HashMap<String,Object>();
        Map<String,Object> offlineMap=new HashMap<String,Object>();
        onlineMap.put("total",onlineMoney);
        onlineMap.put("list",onlineList);
        offlineMap.put("total",offlineMoney);
        offlineMap.put("list",offlineList);
        resultMap.put("online",onlineMap);
        resultMap.put("offline",offlineMap);
        resultMap.put("totalMoney",totalMoney);
        payOrders.add(resultMap);

        WebResult result = WebResult.getSuccessResult();
        result.put("orders",orders);
        result.put("rentOrders",rentOrders);
        result.put("exceptionOrders",exceptionOrders);
        result.put("payOrders",payOrders);
        return result;
    }
    private Map getProductInit(String typeName, String typeId) {
        //0	最新活动-----指企业举办的相关活动（比如：举办滑雪锦标赛等），最新活动没有入园凭证，不需要生成二维码，产品自动核销
        //* 1	滑雪度假
        //* 2	教练预约：只能单次预约，不指定教练
        //* 3	超值票务
        //* 4	装备秒杀
        //* 5	训练营
        //* 6	自营票务
        //* 7	酒店预定
        //* 8	教练课程——(课时卡多次、课程单次)，课程与教练绑定
        //* 9	储值卡
        //* 10	续时产品
        Map initMap=new HashMap();
        initMap.put("type_id",typeId);
        initMap.put("typeName",typeName);
        initMap.put("lists",new ArrayList());
        return  initMap;
    }
    private Map<String, Object> getinitPayMap(String payTypeStr, BigDecimal payMoney) {
        Map<String,Object> inintMap=new HashMap<String,Object>();
        inintMap.put("name",payTypeStr);
        inintMap.put("money",payMoney);
        return inintMap;
    }

    private String getTypeName(String typeId) {
        //* 0	最新活动
        //* 1	滑雪度假
        //* 2	教练预约
        //* 3	超值票务
        //* 4	装备秒杀
        //* 5	训练营
        //* 6	自营票务
        //* 7	酒店预定
        //* 8	教练课程
        //* 9	储值卡
        //* 10	续时产品
        String typeName = "";
        switch(typeId){
            case "0":
                typeName="最新活动";
                break;
            case "1":
                typeName="滑雪度假";
                break;
            case "2":
                typeName="教练预约";
                break;
            case "3":
                typeName="超值票务";
                break;
            case "4":
                typeName="会员卡";
                break;
            case "5":
                typeName="训练营";
                break;
            case "6":
                typeName="自营票务";
                break;
            case "7":
                typeName="酒店预定";
                break;
            case "8":
                typeName="教练课程";
                break;
            case "9":
                typeName="储值卡";
                break;
            case "10":
                typeName="续时产品";
                break;
            default:
                typeName="";
                break;
        }
        return typeName;
    }


}
