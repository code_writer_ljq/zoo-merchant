package com.zoo.zoomerchantadminweb.finance.controller;

import com.itextpdf.text.*;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.zoo.activity.dao.model.Organizer;
import com.zoo.common.util.StringUtil;
import com.zoo.finance.dao.model.ZooSettlementDetail;
import com.zoo.finance.dao.model.ZooSettlementDetailExample;
import com.zoo.finance.dao.model.ZooSettlementTransferAccounts;
import com.zoo.finance.dao.model.ZooSettlementTransferAccountsExample;
import com.zoo.finance.service.ZooSettlementDetailService;
import com.zoo.finance.service.ZooSettlementTransferAccountsService;
import com.zoo.rest.sms.sdk.utils.encoder.BASE64Encoder;
import com.zoo.util.data.DateUtil;
import com.zoo.web.bean.WebResult;
import com.zoo.zoomerchantadminweb.context.WebUpmsContext;
import com.zoo.zoomerchantadminweb.dowload.controller.ExcelExportUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.jsoup.helper.DataUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.io.*;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Api(value = "api/finance/getimage", tags = {"【财务系统】获取银行电子回单图片"}, description = "这是为前台UE提供服务获取图片接口")
@RestController
@RequestMapping("api/finance/getimage")
public class ZooGetImageController {
    private static final Logger LOGGER = LoggerFactory.getLogger(ZooGetImageController.class);

    @Autowired
    private ZooSettlementTransferAccountsService zooSettlementTransferAccountsService;

    @Autowired
    private ZooSettlementDetailService zooSettlementDetailService;

    @ApiOperation(value = "获取银行电子回单图片", httpMethod = "GET")
    @RequestMapping(value = "/electronicReceipt", method = RequestMethod.GET)
    @ResponseBody
    public Object export(HttpServletResponse response, HttpServletRequest request,
                       @RequestParam(required = false, value = "id") Integer id) throws IOException {
        //Organizer organizer = WebUpmsContext.getOrgnization(request);
        if(id==null){
            return WebResult.getErrorResult(WebResult.Code.SETTLEMENT_TRANSFER_ID_NULL);
        }
        ZooSettlementTransferAccountsExample example = new ZooSettlementTransferAccountsExample();
        example.createCriteria().andIdEqualTo(id);
        List<ZooSettlementTransferAccounts> zooSettlementTransferAccountss = zooSettlementTransferAccountsService.selectByExample(example);
        ZooSettlementTransferAccounts zooSettlementTransferAccounts=zooSettlementTransferAccountss.get(0);
        String filePath = zooSettlementTransferAccounts.getElectronicReceipt();

        //String filename = filePath.substring(filePath.lastIndexOf("/") + 1, filePath.length());
//        response.setContentType("application/image/*;charset=UTF-8");
//        response.setCharacterEncoding("UTF-8");
//        response.setHeader("Cache-Control", "no-cache");
//        response.setHeader("Content-Disposition", "attachment;filename=" + new String(filename.getBytes("gbk"), "iso8859-1"));
//        File file = new File(filePath);
//        FileInputStream in = new FileInputStream(file);
//        byte[] b = new byte[1024];
//        int len = 0;
//        while ((len = in.read(b)) != -1) {
//            response.getOutputStream().write(b, 0, len);
//        }
//        response.flushBuffer();

        String imgStr = "";
        try {
            WebResult result = WebResult.getSuccessResult();
            if(StringUtils.isNotEmpty(filePath)){
                File file = new File(filePath);
                if(!file.exists()){
                    return WebResult.getErrorMsgResult(WebResult.Code.ERROR,"电子回单还未上传，工作人员正在马不停蹄的处理中");
                }
                FileInputStream fis = new FileInputStream(file);
                byte[] buffer = new byte[(int) file.length()];
                int offset = 0;
                int numRead = 0;
                while (offset < buffer.length && (numRead = fis.read(buffer, offset, buffer.length - offset)) >= 0) {
                    offset += numRead;
                }
                if (offset != buffer.length) {
                    LOGGER.error("获取回执单失败，文件可能被损坏");
                    return WebResult.getErrorResult(WebResult.Code.BLANCE_IMAGE_FAIL);
                }
                fis.close();
                BASE64Encoder encoder = new BASE64Encoder();
                imgStr = encoder.encode(buffer);
                result.put("imgStr",imgStr);
            }

            SimpleDateFormat sim =new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String datad="";
            if(zooSettlementTransferAccounts.getTransferTime()!=null){
                datad=sim.format(zooSettlementTransferAccounts.getTransferTime());
            }
            ZooSettlementDetailExample zooSettlementDetailExample = new ZooSettlementDetailExample();
            zooSettlementDetailExample.createCriteria().andZooSettlementTransferAccountsIdEqualTo(id);
            //查询对账表
            List<ZooSettlementDetail> zooSettlementDetails = zooSettlementDetailService.selectByExample(zooSettlementDetailExample);
            //查询合同
            BigDecimal transferMoney = new BigDecimal(0);
            BigDecimal platformRateMoney = new BigDecimal(0);
            BigDecimal payRateMoney = new BigDecimal(0);
            BigDecimal depositIndemnityMoney = new BigDecimal(0);//押金退款金额
            BigDecimal orderTotalMoney = new BigDecimal(0);
            BigDecimal poundageTotalMoney = new BigDecimal(0);
            for (int i = 0; i < zooSettlementDetails.size(); i++) {
                ZooSettlementDetail zoo = zooSettlementDetails.get(i);
                transferMoney = transferMoney.add(zoo.getTransferMoney());
                platformRateMoney = platformRateMoney.add(zoo.getPlatformRateMoney());
                payRateMoney = payRateMoney.add(zoo.getPayRateMoney());
                depositIndemnityMoney = depositIndemnityMoney.add(zoo.getDepositIndemnityMoney());
                orderTotalMoney = orderTotalMoney.add(zoo.getOrderTotalMoney());
                poundageTotalMoney=poundageTotalMoney.add(zoo.getPoundageTotalMoney());
            }
            result.put("transData",datad);
            result.put("accountBank",zooSettlementTransferAccounts.getAccountBank());
            result.put("accountName",zooSettlementTransferAccounts.getAccountName());
            result.put("accountNumber",zooSettlementTransferAccounts.getAccountNumber());
            result.put("orderTotalMoney",orderTotalMoney);
            result.put("transferMoney",transferMoney);
            result.put("poundageTotalMoney",poundageTotalMoney);
            result.put("voucher",zooSettlementTransferAccounts.getVoucher());
            result.put("organizerName",zooSettlementTransferAccounts.getOrganizerName());

            result.setCode(0);
            result.setMsg("成功");
            return result;
        } catch (Exception e) {
            LOGGER.error("获取回执单失败：",e);
            return WebResult.getErrorResult(WebResult.Code.BLANCE_IMAGE_FAIL);
        }
    }

    @ApiOperation(value = "获取转账信息pdf", httpMethod = "GET")
    @RequestMapping(value = "/balancePdf", method = RequestMethod.GET)
    @ResponseBody
    public void balancePdf(HttpServletResponse response, HttpServletRequest request,
                         @RequestParam(required = true, value = "id") Integer id) throws IOException {
        ZooSettlementTransferAccountsExample example = new ZooSettlementTransferAccountsExample();
        example.createCriteria().andIdEqualTo(id);
        List<ZooSettlementTransferAccounts> zooSettlementTransferAccountss = zooSettlementTransferAccountsService.selectByExample(example);
        ZooSettlementTransferAccounts zooSettlementTransferAccounts=zooSettlementTransferAccountss.get(0);
        ZooSettlementDetailExample zooSettlementDetailExample = new ZooSettlementDetailExample();
        zooSettlementDetailExample.createCriteria().andZooSettlementTransferAccountsIdEqualTo(id);
        //查询对账表
        List<ZooSettlementDetail> zooSettlementDetails = zooSettlementDetailService.selectByExample(zooSettlementDetailExample);
        //查询合同
        BigDecimal transferMoney = new BigDecimal(0);
        BigDecimal platformRateMoney = new BigDecimal(0);
        BigDecimal payRateMoney = new BigDecimal(0);
        BigDecimal depositIndemnityMoney = new BigDecimal(0);//押金退款金额
        BigDecimal orderTotalMoney = new BigDecimal(0);
        BigDecimal poundageTotalMoney = new BigDecimal(0);
        for (int i = 0; i < zooSettlementDetails.size(); i++) {
            ZooSettlementDetail zoo = zooSettlementDetails.get(i);
            transferMoney = transferMoney.add(zoo.getTransferMoney());
            platformRateMoney = platformRateMoney.add(zoo.getPlatformRateMoney());
            payRateMoney = payRateMoney.add(zoo.getPayRateMoney());
            depositIndemnityMoney = depositIndemnityMoney.add(zoo.getDepositIndemnityMoney());
            orderTotalMoney = orderTotalMoney.add(zoo.getOrderTotalMoney());
            poundageTotalMoney=poundageTotalMoney.add(zoo.getPoundageTotalMoney());
        }
        String filePath = zooSettlementTransferAccounts.getElectronicReceipt();
        Document document = new Document();
        response.setContentType("application/pdf;charset=UTF-8");
        try{
            response.setContentType("application/pdf");
            PdfWriter.getInstance(document, response.getOutputStream());
            document.open();
            BaseFont bf = BaseFont.createFont("STSong-Light", "UniGB-UCS2-H", BaseFont.NOT_EMBEDDED);
            Font FontChinese = new Font(bf, 12, Font.NORMAL);
            document.add(new Paragraph("转账记录",FontChinese));
            SimpleDateFormat sim =new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//            String createTime=sim.format(new Date());
//            document.add(new Paragraph("当前时间"+createTime,FontChinese));
            document.add(new Paragraph("\n"));


            float[] widths = {100, 400}; //设置三列表格的宽度
            PdfPTable table = new PdfPTable(widths);
            table.setLockedWidth(true);
            table.setTotalWidth(500);
            table.setHorizontalAlignment(Element.ALIGN_LEFT);
            String datad="";
            if(zooSettlementTransferAccounts.getTransferTime()!=null){
                datad=sim.format(zooSettlementTransferAccounts.getTransferTime());
            }
            Object[] datas = {"转账日期",datad,
                    "结算账户行", zooSettlementTransferAccounts.getAccountBank(),
                    "结算账户名", zooSettlementTransferAccounts.getAccountName(),
                    "结算账号", zooSettlementTransferAccounts.getAccountNumber(),
                    "订单金额",orderTotalMoney,
                    "到账金额",transferMoney,
                    "手续费",poundageTotalMoney,
                    "电子回执单凭证号", zooSettlementTransferAccounts.getVoucher(),"主办方名称", zooSettlementTransferAccounts.getOrganizerName()};
            for(int i = 0; i < datas.length; i++) {
                PdfPCell pdfCell = new PdfPCell();
                pdfCell.setMinimumHeight(30);//设置表格行高
                Paragraph paragraph = new Paragraph(StringUtil.getString(datas[i]), getPdfChineseFont());
                pdfCell.setPhrase(paragraph);
                table.addCell(pdfCell);
            }
            PdfPCell pdfCell1 = new PdfPCell();
            pdfCell1.setMinimumHeight(30);//设置表格行高
            Paragraph paragraph = new Paragraph("电子回执单凭证图片", getPdfChineseFont());
            pdfCell1.setPhrase(paragraph);
            table.addCell(pdfCell1);
            //单元格插入图片
            byte[] bt = FileUtils.readFileToByteArray(new File(filePath));
            PdfPCell pdfCell2 = new PdfPCell();
            Image img=Image.getInstance(bt);
            //img.scaleAbsolute(2,3);
            img.scalePercent(10);
            pdfCell2.setImage(img);//插入图片
            table.addCell(pdfCell2);
            document.add(table);
        }catch(Exception e){
            e.printStackTrace();
        }
        document.close();

        response.setCharacterEncoding("UTF-8");
        response.setHeader("Cache-Control", "no-cache");
        String filename = "TransferAccount" + "_" + DateUtil.getToday("yyyyMMddHHmmss") + ".pdf";
        response.setHeader("Content-Disposition", "attachment;filename=" + new String(filename.getBytes("gbk"), "iso8859-1"));
        response.flushBuffer();

    }

    public static Font getPdfChineseFont() throws Exception {
        BaseFont bfChinese = BaseFont.createFont("STSongStd-Light", "UniGB-UCS2-H",
                BaseFont.NOT_EMBEDDED);
        Font fontChinese = new Font(bfChinese, 12, Font.NORMAL);
        return fontChinese;
    }

    public static void MyDrawString(String str,int x,int y,double rate,Graphics g){
        String tempStr=new String();
        int orgStringWight=g.getFontMetrics().stringWidth(str);
        int orgStringLength=str.length();
        int tempx=x;
        int tempy=y;
        while(str.length()>0)
        {
            tempStr=str.substring(0, 1);
            str=str.substring(1, str.length());
            g.drawString(tempStr, tempx, tempy);
            tempx=(int)(tempx+(double)orgStringWight/(double)orgStringLength*rate);
        }
    }
}
