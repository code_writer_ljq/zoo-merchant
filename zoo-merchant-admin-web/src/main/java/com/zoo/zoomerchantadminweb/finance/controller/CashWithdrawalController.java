package com.zoo.zoomerchantadminweb.finance.controller;

import com.alibaba.rocketmq.client.exception.MQBrokerException;
import com.alibaba.rocketmq.client.exception.MQClientException;
import com.alibaba.rocketmq.remoting.exception.RemotingException;
import com.zoo.activity.dao.model.Organizer;
import com.zoo.activity.dao.nutz.pojo.CommonAccount;
import com.zoo.common.job.util.FinanceJobUtil;
import com.zoo.common.page.PageList;
import com.zoo.finance.dao.model.ZooSettlementDetail;
import com.zoo.finance.dao.model.ZooSettlementDetailExample;
import com.zoo.finance.dao.model.ZooSettlementTransferAccounts;
import com.zoo.finance.dao.model.ZooSettlementTransferAccountsExample;
import com.zoo.finance.service.*;
import com.zoo.sms.mq.rocketmq.producter.SmsProducter;
import com.zoo.sms.mq.rocketmq.producter.SmsTypeEnum;
import com.zoo.util.data.DateUtil;
import com.zoo.web.bean.WebResult;
import com.zoo.zoomerchantadminweb.context.WebUpmsContext;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Api(value = "api/finance/cash/withdrawal", tags = {"【财务系统】提现"}, description = "提现")
@RestController
@RequestMapping("api/finance/cash/withdrawal")
public class CashWithdrawalController {

    private static final Logger LOGGER = LoggerFactory.getLogger(CashWithdrawalController.class);

    @Autowired
    private ZooSettlementDetailService zooSettlementDetailService;

    @Autowired
    private ZooSettlementTransferAccountsService zooSettlementTransferAccountsService;

    @Autowired
    private JavaMailSender mailSender;

    @Value("${spring.mail.username}")
    private String fromMail;

    /**
     * 提现金额和次数统计
     */
    @ApiOperation(value = "提现金额和次数统计", notes = "提现金额和次数统计，返回字段：(noTransferMoney-未提现金额，transferMoney-已提现金额，count-提现次数)")
    @RequestMapping(value = "/statistics", method = RequestMethod.GET)
    public WebResult statistics(HttpServletRequest request){

        Organizer orgnization = WebUpmsContext.getOrgnization(request);

        //-------------------未转账金额---------------------
        ZooSettlementDetailExample noDetailExample = new ZooSettlementDetailExample();
        ZooSettlementDetailExample.Criteria nocriteria = noDetailExample.createCriteria();
        //1、出账中，2、已出账，3、完成对账，4、转账中，5、转账完成
        List<Integer> nostatus = new ArrayList<Integer>();
        nostatus.add(1);
        nostatus.add(2);
        nostatus.add(3);
        nocriteria.andStatusIn(nostatus);
        nocriteria.andOrganizerIdEqualTo(orgnization.getOrganizerId());
        // nocriteria.andTransferTypeEqualTo(2);
        BigDecimal noTransferMoney = zooSettlementDetailService.totalTransferMoney(noDetailExample);
        if(noTransferMoney == null){
            noTransferMoney = BigDecimal.ZERO;
        }

        //-------------------提现金额---------------------
        ZooSettlementDetailExample detailExample = new ZooSettlementDetailExample();
        ZooSettlementDetailExample.Criteria criteria = detailExample.createCriteria();
        //1、出账中，2、已出账，3、完成对账，4、转账中，5、转账完成
        List<Integer> status = new ArrayList<Integer>();
        status.add(4);
        status.add(5);
        criteria.andStatusIn(status);
        criteria.andOrganizerIdEqualTo(orgnization.getOrganizerId());
        // criteria.andTransferTypeEqualTo(2);
        BigDecimal transferMoney = zooSettlementDetailService.totalTransferMoney(detailExample);
        if(transferMoney == null){
            transferMoney = BigDecimal.ZERO;
        }

        //-------------------提现次数---------------------
        ZooSettlementTransferAccountsExample transferAccountsExample = new ZooSettlementTransferAccountsExample();
        ZooSettlementTransferAccountsExample.Criteria accountsExampleCriteria = transferAccountsExample.createCriteria();
        accountsExampleCriteria.andOrganizerIdEqualTo(orgnization.getOrganizerId());
        accountsExampleCriteria.andStatusNotEqualTo(2);
        // accountsExampleCriteria.andTransferTypeEqualTo(2);
        int count = zooSettlementTransferAccountsService.countByExample(transferAccountsExample);

        WebResult<Object> successResult = WebResult.getSuccessResult();
        //未转金额
        successResult.put("noTransferMoney",noTransferMoney.setScale(2, RoundingMode.HALF_UP).doubleValue());
        //提现金额
        successResult.put("transferMoney",transferMoney.setScale(2, RoundingMode.HALF_UP).doubleValue());
        //提现数量
        successResult.put("count",count);

        return successResult;
    }

    @ApiOperation(value = "查询待转列表", notes = "查询待转列表：返回状态balanceStatus（1：未出账，2：待提现-已对账），transferMoney订单金额，时间startTime", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页", required = false, dataType = "int", paramType = "query", defaultValue = "1"),
            @ApiImplicitParam(name = "size", value = "条", required = false, dataType = "int", paramType = "query", defaultValue = "10"),
            @ApiImplicitParam(name = "startTime", value = "开始时间", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "endTime", value = "结束时间", required = false, dataType = "string", paramType = "query"),
    })
    @RequestMapping(value = "/noTransfer", method = RequestMethod.GET)
    public Object noTransfer(@RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
                                  @RequestParam(value = "size", required = false, defaultValue = "10") Integer size,
                                  @RequestParam(value = "startTime", required = false) String startTime,
                                  @RequestParam(value = "endTime", required = false) String endTime,
                                  HttpServletRequest request) {

        // 主办方信息
        Organizer organizer = WebUpmsContext.getOrgnization(request);

        PageList pageList = null;
        try {
            ZooSettlementDetailExample noDetailExample = new ZooSettlementDetailExample();
            noDetailExample.setOrderByClause("start_time desc");
            ZooSettlementDetailExample.Criteria nocriteria = noDetailExample.createCriteria();
            //1、出账中，2、已出账，3、完成对账，4、转账中，5、转账完成
            List<Integer> nostatus = new ArrayList<Integer>();
            nostatus.add(1);
            nostatus.add(2);
            nostatus.add(3);
            nocriteria.andStatusIn(nostatus);
            nocriteria.andOrganizerIdEqualTo(organizer.getOrganizerId());
            //转账模式：1：周期结算模式，2：自助提现模式
            // nocriteria.andTransferTypeEqualTo(2);

            if(StringUtils.isNotEmpty(startTime)){
                startTime = startTime.substring(0,10) + " 00:00:00";
                nocriteria.andStartTimeGreaterThanOrEqualTo(DateUtil.convertStringToDate(startTime,"yyyy-MM-dd HH:mm:ss"));
            }
            if(StringUtils.isNotEmpty(endTime)){
                endTime = endTime.substring(0,10) + " 23:59:59";
                nocriteria.andStartTimeLessThanOrEqualTo(DateUtil.convertStringToDate(endTime,"yyyy-MM-dd HH:mm:ss"));
            }
            List<ZooSettlementDetail> list = zooSettlementDetailService.selectByExampleForStartPage(noDetailExample, page, size);
            int count = zooSettlementDetailService.countByExample(noDetailExample);

            pageList = new PageList();
            pageList.setCount(count);
            pageList.setDataList(list);

        } catch (Exception e) {
            e.printStackTrace();
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }

        return WebResult.getSuccessResult("data", pageList);
    }

    @ApiOperation(value = "查询已转列表", notes = "查询已转列表", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页", required = false, dataType = "int", paramType = "query", defaultValue = "1"),
            @ApiImplicitParam(name = "size", value = "条", required = false, dataType = "int", paramType = "query", defaultValue = "10"),
            @ApiImplicitParam(name = "startTime", value = "开始时间", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "endTime", value = "结束时间", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "status", value = "状态：0、全部，1、转款完成，2、撤销，3、转账中，4、转账异常，可以不传值，默认值0", required = false , defaultValue = "0" ,dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "invoiceStatus", value = "发票状态（-1 、全部, 0、未开发发票，1、已申请，2、驳回，3、已处理）,可以不传值，默认值-1", required = false , defaultValue = "-1",dataType = "int", paramType = "query"),
    })
    @RequestMapping(value = "/transfer", method = RequestMethod.GET)
    public Object transfer(@RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
                             @RequestParam(value = "size", required = false, defaultValue = "10") Integer size,
                             @RequestParam(value = "startTime", required = false) String startTime,
                             @RequestParam(value = "endTime", required = false) String endTime,
                             @RequestParam(value = "status", required = false,defaultValue = "0") Integer status,
                             @RequestParam(value = "invoiceStatus", required = false,defaultValue = "-1") Integer invoiceStatus,
                             HttpServletRequest request) {

        // 主办方信息
        Organizer organizer = WebUpmsContext.getOrgnization(request);

        PageList pageList = null;
        try {
            ZooSettlementTransferAccountsExample transferAccountsExample = new ZooSettlementTransferAccountsExample();
            transferAccountsExample.setOrderByClause("create_time desc");
            ZooSettlementTransferAccountsExample.Criteria accountsExampleCriteria = transferAccountsExample.createCriteria();
            accountsExampleCriteria.andOrganizerIdEqualTo(organizer.getOrganizerId());
            //转账模式：1：周期结算模式，2：自助提现模式
            // accountsExampleCriteria.andTransferTypeEqualTo(2);
            accountsExampleCriteria.andStatusNotEqualTo(2);
            if(status != 0){
                //数据库状态：1、转款完成，2、撤销，3、转账中，4、转账异常
                accountsExampleCriteria.andStatusEqualTo(status);
            }
            if(invoiceStatus != -1){
                accountsExampleCriteria.andInvoiceStatusEqualTo(invoiceStatus);
            }
            if(StringUtils.isNotEmpty(startTime)){
                startTime = startTime.substring(0,10) + " 00:00:00";
                accountsExampleCriteria.andCreateTimeGreaterThanOrEqualTo(DateUtil.convertStringToDate(startTime,"yyyy-MM-dd HH:mm:ss"));
            }
            if(StringUtils.isNotEmpty(endTime)){
                endTime = endTime.substring(0,10) + " 23:59:59";
                accountsExampleCriteria.andCreateTimeLessThanOrEqualTo(DateUtil.convertStringToDate(endTime,"yyyy-MM-dd HH:mm:ss"));
            }
            List<ZooSettlementTransferAccounts> list = zooSettlementTransferAccountsService.selectByExampleForStartPage(transferAccountsExample, page, size);
            int count = zooSettlementTransferAccountsService.countByExample(transferAccountsExample);

            pageList = new PageList();
            pageList.setCount(count);
            pageList.setDataList(list);
        } catch (Exception e) {
            e.printStackTrace();
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }

        return WebResult.getSuccessResult("data", pageList);
    }

    @ApiOperation(value = "提现信息", notes = "提现信息，返回内容：orderTotalMoney(订单金额)，poundageTotalMoney(手续费),platformRateMoney(平台服务费)，payRateMoney(支付通道费),accountBank(开户银行),accountName（开户名称）,accountNumber（开户账号）", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", value = "需要提现的记录ID，ID之间使用中横线'-'分隔", required = false, dataType = "string", paramType = "query"),
    })
    @RequestMapping(value = "/getTransferInfo", method = RequestMethod.GET)
    public WebResult getTransferInfo(HttpServletRequest request,@RequestParam(value = "ids", required = false) String ids) {

        Organizer orgnization = WebUpmsContext.getOrgnization(request);

        Map<String,Object> map=zooSettlementTransferAccountsService.getZooSettlementTransferAccountsByZooSettlementDetailIds(orgnization.getOrganizerId(),ids);
        if(map == null){
            return WebResult.getErrorResult(WebResult.Code.CASHIER_NO_CASH_ORDER);
        }

        //允许最高提现次数
        int maxCashWithdrawalCount = FinanceJobUtil.getMaxCashWithdrawalCount();
        map.put("maxCashWithdrawalCount", maxCashWithdrawalCount);

        //已经提现次数
        Integer nowCashWithdrawalCount = FinanceJobUtil.getNowCashWithdrawalCount(orgnization.getOrganizerId());
        map.put("nowCashWithdrawalCount",nowCashWithdrawalCount);
        map.put("remainderCashWithdrawalCount",maxCashWithdrawalCount-nowCashWithdrawalCount);

        return WebResult.getSuccessResult("data",map);
    }

    @ApiOperation(value = "提交提现申请", notes = "提交提现申请", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", value = "需要提现的记录ID，ID之间使用中横线'-'分隔", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "remark", value = "备注", required = false, dataType = "string", paramType = "query"),
    })
    @RequestMapping(value = "/commitTransferInfo", method = RequestMethod.GET)
    public WebResult commitTransferInfo(HttpServletRequest request,@RequestParam(value = "ids", required = false) String ids,String remark) {

        CommonAccount account = WebUpmsContext.getAccount(request);
        Organizer orgnization = WebUpmsContext.getOrgnization(request);

        //允许最高提现次数
        int maxCashWithdrawalCount = FinanceJobUtil.getMaxCashWithdrawalCount();

        //已经提现次数
        int nowCashWithdrawalCount = FinanceJobUtil.getNowCashWithdrawalCount(orgnization.getOrganizerId());
        if(maxCashWithdrawalCount-nowCashWithdrawalCount<=0){
            return WebResult.getErrorResult(WebResult.Code.CASHIER_NO_CASH_COUNT);
        }

        Map<String,Object> map=zooSettlementTransferAccountsService.getZooSettlementTransferAccountsByZooSettlementDetailIds(orgnization.getOrganizerId(),ids);
        if(map == null){
            return WebResult.getErrorMsgResult(WebResult.Code.ERROR,"请刷新页面后，重试");
        }

        if(MapUtils.getDouble(map,"transferMoney",0.0) == 0){
            return WebResult.getErrorResult(WebResult.Code.CASHIER_NO_CASH_MONEY);
        }

        ZooSettlementTransferAccounts zooSettlementTransferAccounts = new ZooSettlementTransferAccounts();
        zooSettlementTransferAccounts.setOrganizerId(MapUtils.getInteger(map,"organizerId"));
        zooSettlementTransferAccounts.setOrganizerName(MapUtils.getString(map,"organizerName"));
        zooSettlementTransferAccounts.setIsHotel(MapUtils.getInteger(map,"isHotel"));
        zooSettlementTransferAccounts.setTransferMoney(new BigDecimal(MapUtils.getDouble(map,"transferMoney")));
        zooSettlementTransferAccounts.setAccountBank(MapUtils.getString(map,"accountBank"));
        zooSettlementTransferAccounts.setAccountName(MapUtils.getString(map,"accountName"));
        zooSettlementTransferAccounts.setAccountNumber(MapUtils.getString(map,"accountNumber"));
        Date d = new Date();
        zooSettlementTransferAccounts.setCreateTime(d);
        zooSettlementTransferAccounts.setLastUpdateTime(d);
        zooSettlementTransferAccounts.setTransferType(MapUtils.getInteger(map,"transferType"));
        zooSettlementTransferAccounts.setPoundageTotalMoney(new BigDecimal(MapUtils.getDouble(map,"poundageTotalMoney")));
        zooSettlementTransferAccounts.setOfflineOrderTotalMoney(new BigDecimal(MapUtils.getDouble(map,"offlineOrderTotalMoney")));
        zooSettlementTransferAccounts.setOfflinePlatformRateMoney(new BigDecimal(MapUtils.getDouble(map,"offlinePlatformRateMoney")));
        zooSettlementTransferAccounts.setPayRateMoney(new BigDecimal(MapUtils.getDouble(map,"payRateMoney")));
        zooSettlementTransferAccounts.setPlatformRateMoney(new BigDecimal(MapUtils.getDouble(map,"platformRateMoney")));
        zooSettlementTransferAccounts.setOperType(1);
        zooSettlementTransferAccounts.setRemark(remark);
        zooSettlementTransferAccounts.setOrderTotalMoney(new BigDecimal(MapUtils.getDouble(map,"orderTotalMoney")));
        zooSettlementTransferAccounts.setOperatorId(account.getCustomerId());
        zooSettlementTransferAccounts.setOperatorName(StringUtils.isEmpty(account.getRealname())?account.getNickname():account.getRealname());

        zooSettlementTransferAccountsService.insertZooSettlementTransferAccountsUpdateDetail(ids, zooSettlementTransferAccounts);

        //记录提现次数
        FinanceJobUtil.incrCashWithdrawalCount(orgnization.getOrganizerId());

        new Thread(()->{
            try{
                sendMessage(orgnization.getName(),zooSettlementTransferAccounts.getTransferMoney().doubleValue());
            } catch (Exception e){
            }
        }).start();

        return WebResult.getSuccessResult("data",map);
    }

    private void sendMessage(String organizerName,double money){

        List<Map<String, String>> financer = FinanceJobUtil.getFinancer();
        for (Map<String, String> item : financer) {

            String phone = MapUtils.getString(item, "phone");
            String email = MapUtils.getString(item, "email");

            //发送短信
            if(StringUtils.isNotEmpty(phone)){
                try {
                    SmsProducter.send(null, phone ,SmsTypeEnum.system_notification_falcon.toString(),organizerName + "申请提现" + money + ",请尽快处理");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (RemotingException e) {
                    e.printStackTrace();
                } catch (MQClientException e) {
                    e.printStackTrace();
                } catch (MQBrokerException e) {
                    e.printStackTrace();
                }
            }

            if(StringUtils.isNotEmpty(email)){
                SimpleMailMessage message = new SimpleMailMessage();
                message.setFrom(fromMail);
                message.setTo(email);
                message.setSubject("提现申请：" + organizerName);
                message.setText(organizerName + "申请提现，提现金额：" + money + ",请尽快处理");
                mailSender.send(message);
            }
        }
    }


}