package com.zoo.zoomerchantadminweb.finance.controller;

import com.github.pagehelper.PageInfo;
import com.zoo.activity.dao.model.Organizer;
import com.zoo.activity.dao.nutz.pojo.CommonAccount;
import com.zoo.activity.util.DateUtil;
import com.zoo.common.util.StringUtil;
import com.zoo.finance.dao.model.*;
import com.zoo.finance.service.ZooOnAccountCompanyService;
import com.zoo.finance.service.ZooOnAccountDayDetailService;
import com.zoo.finance.service.ZooOnAccountOrdersService;
import com.zoo.web.bean.WebResult;
import com.zoo.zoomerchantadminweb.context.WebUpmsContext;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

@Api(value = "api/finance/hangup/statistic", tags = {"【财务系统】挂帐统计"}, description = "挂帐统计接口描述")
@RestController
@RequestMapping("api/finance/hangup/statistic")
public class HangUpStatisticController {

    private static final Logger LOGGER = LoggerFactory.getLogger(HangUpStatisticController.class);

    @Autowired
    private ZooOnAccountDayDetailService zooOnAccountDayDetailService;

    @Autowired
    private ZooOnAccountOrdersService zooOnAccountOrdersService;

    @Autowired
    private ZooOnAccountCompanyService zooOnAccountCompanyService;

    @ApiOperation(value = "挂帐统计列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "size", value = "分页大小", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "companyId", value = "挂账单位ID", required = false, dataType = "int", paramType = "query"),
    })
    //@RequiresPermissions("zoo:onAccountHangupDetail:read")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ResponseBody
    public WebResult list(
            HttpServletRequest request,
            @RequestParam(required = false, defaultValue = "0", value = "page") int page,
            @RequestParam(required = false, defaultValue = "10", value = "size") int size,
            @RequestParam(required = false, value = "companyId") Long companyId,
            @RequestParam(required = false, value = "startTime") String startTime,
            @RequestParam(required = false, value = "endTime") String endTime) {

        Organizer orgnization = WebUpmsContext.getOrgnization(request);

        if(StringUtils.isNotEmpty(startTime) && StringUtils.isNotEmpty(endTime)){
            startTime = startTime + " 00:00:00";
            endTime = endTime + " 23:59:59";
        }else if(StringUtils.isNotEmpty(startTime) && StringUtils.isEmpty(endTime)){
            startTime = startTime + " 00:00:00";
        }else if(StringUtils.isNotEmpty(endTime) && StringUtils.isEmpty(startTime)){
            endTime = endTime + " 23:59:59";
        }

        PageInfo<Map> maps = zooOnAccountDayDetailService.selectGroupByCompanyId(orgnization.getOrganizerId(),companyId, startTime, endTime, page, size);
        WebResult result = WebResult.getSuccessResult();
        result.put("detail",maps.getList());
        result.put("count",maps.getTotal());
        return result;
    }

    @ApiOperation(value = "挂帐统计明细列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "size", value = "分页大小", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "companyId", value = "挂账单位ID", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "startTime", value = "统计开始时间（yyyy-MM-dd）", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "endTime", value = "统计结束时间（yyyy-MM-dd）", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "orderCode", value = "主订单编号", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "activityId", value = "产品ID", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "episodeId", value = "场次ID", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "hangupPersonName", value = "挂账人姓名", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "hangupPersonPhone", value = "挂账人手机号", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "trustees", value = "经办人", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "hangupStartTime", value = "挂账开始时间（yyyy-MM-dd HH:mm:ss）", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "hangupEndTime", value = "挂账结束时间（yyyy-MM-dd HH:mm:ss）", required = false, dataType = "string", paramType = "query"),
    })
    //@RequiresPermissions("zoo:onAccountHangupDetail:read")
    @RequestMapping(value = "/detail", method = RequestMethod.GET)
    @ResponseBody
    public WebResult detail(HttpServletRequest request,
            @RequestParam(required = false, defaultValue = "0", value = "page") int page,
            @RequestParam(required = false, defaultValue = "10", value = "size") int size,
            @RequestParam(required = false, value = "companyId") Integer companyId,
            @RequestParam(required = false, value = "orderCode") String orderCode,
            @RequestParam(required = false, value = "activityId") Integer activityId,
            @RequestParam(required = false, value = "episodeId") Integer episodeId,
            @RequestParam(required = false, value = "hangupPersonName") String hangupPersonName,
            @RequestParam(required = false, value = "hangupPersonPhone") String hangupPersonPhone,
            @RequestParam(required = false, value = "trustees") String trustees,
            @RequestParam(required = false, value = "startTime") String startTime,
            @RequestParam(required = false, value = "endTime") String endTime,
            @RequestParam(required = false, value = "hangupStartTime") String hangupStartTime,
            @RequestParam(required = false, value = "hangupEndTime") String hangupEndTime) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        ZooOnAccountOrdersExample example = new ZooOnAccountOrdersExample();
        ZooOnAccountOrdersExample.Criteria criteria = example.createCriteria();
        try {
            if(StringUtils.isNotEmpty(startTime) && StringUtils.isNotEmpty(endTime)){
                criteria.andOrderTimeBetween(DateUtil.convertStringToDate(startTime + " 00:00:00","yyyy-MM-dd HH:mm:ss"),DateUtil.convertStringToDate(endTime + " 23:59:59","yyyy-MM-dd HH:mm:ss"));
            }else if(StringUtils.isNotEmpty(startTime) && StringUtils.isEmpty(endTime)){
                criteria.andOrderTimeGreaterThanOrEqualTo(DateUtil.convertStringToDate(startTime + " 00:00:00","yyyy-MM-dd HH:mm:ss"));
            }else if(StringUtils.isNotEmpty(endTime) && StringUtils.isEmpty(startTime)){
                criteria.andOrderTimeLessThanOrEqualTo(DateUtil.convertStringToDate(endTime + " 00:00:00","yyyy-MM-dd HH:mm:ss"));
            }

            if(StringUtils.isNotEmpty(hangupStartTime) && StringUtils.isNotEmpty(hangupEndTime)){
                criteria.andCreateTimeBetween(DateUtil.convertStringToDate(hangupStartTime,"yyyy-MM-dd HH:mm:ss"),DateUtil.convertStringToDate(hangupEndTime,"yyyy-MM-dd HH:mm:ss"));
            }else if(StringUtils.isNotEmpty(hangupStartTime) && StringUtils.isEmpty(hangupEndTime)){
                criteria.andCreateTimeGreaterThanOrEqualTo(DateUtil.convertStringToDate(hangupStartTime,"yyyy-MM-dd HH:mm:ss"));
            }else if(StringUtils.isNotEmpty(hangupEndTime) && StringUtils.isEmpty(hangupStartTime)){
                criteria.andCreateTimeLessThanOrEqualTo(DateUtil.convertStringToDate(hangupEndTime,"yyyy-MM-dd HH:mm:ss"));
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }
        if(companyId!=null){
            criteria.andCompanyIdEqualTo(companyId);
        }
        if(orderCode!=null){
            criteria.andOrderParentCodeEqualTo(orderCode);
        }
        if(activityId!=null){
            criteria.andActivityIdEqualTo(activityId);
        }
        if(episodeId!=null){
            criteria.andEpisodeIdEqualTo(episodeId);
        }
        if(hangupPersonName!=null){
            criteria.andHangupPersonNameEqualTo(hangupPersonName);
        }
        if(hangupPersonPhone!=null){
            criteria.andHangupPersonPhoneEqualTo(hangupPersonPhone);
        }
        if(trustees!=null){
            criteria.andTrusteesEqualTo(trustees);
        }
        criteria.andOrganizerIdEqualTo(organizer.getOrganizerId());
        example.setOrderByClause(" order_parent_code desc, order_time desc ");
        List<ZooOnAccountOrders> ordersList = zooOnAccountOrdersService.selectByExampleForStartPage(example, page, size);
        if(ordersList!=null&&ordersList.size()>0){
            for (int i = 0; i < ordersList.size(); i++) {
                ZooOnAccountOrders zooOnAccountOrders=ordersList.get(i);

                ZooOnAccountCompanyExample zooOnAccountCompanyExample=new ZooOnAccountCompanyExample();
                zooOnAccountCompanyExample.createCriteria().andIdEqualTo(Long.parseLong(String.valueOf(zooOnAccountOrders.getCompanyId())));
                List<ZooOnAccountCompany> zooOnAccountCompanys=zooOnAccountCompanyService.selectByExample(zooOnAccountCompanyExample);
                if(zooOnAccountCompanys!=null&&zooOnAccountCompanys.size()>0){
                    zooOnAccountOrders.setCompanyName(zooOnAccountCompanys.get(0).getCompanyName());
                }
                if(zooOnAccountOrders.getOperatorName()!=null && zooOnAccountOrders.getOperatorName().contains("小ZOO")){
                    zooOnAccountOrders.setOperatorName("总管理员");
                }
            }
        }
        int count = zooOnAccountOrdersService.countByExample(example);
        WebResult result = WebResult.getSuccessResult();
        result.put("list",ordersList);
        result.put("count",count);
        return result;
    }


}
