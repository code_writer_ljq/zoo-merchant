package com.zoo.zoomerchantadminweb.finance.controller;

import com.zoo.activity.dao.model.Organizer;
import com.zoo.activity.dao.nutz.pojo.CommonAccount;
import com.zoo.activity.util.DateUtil;
import com.zoo.finance.dao.model.*;
import com.zoo.finance.service.ZooEntertainDetailService;
import com.zoo.finance.service.ZooEntertainOrdersService;
import com.zoo.finance.service.ZooOnAccountDayDetailService;
import com.zoo.finance.service.ZooOnAccountOrdersService;
import com.zoo.web.bean.WebResult;
import com.zoo.zoomerchantadminweb.context.WebUpmsContext;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

@Api(value = "api/finance/entertain/statistic", tags = {"【财务系统】招待统计"}, description = "招待统计接口描述")
@RestController
@RequestMapping("api/finance/entertain/statistic")
public class EntertainStatisticController {

    private static final Logger LOGGER = LoggerFactory.getLogger(EntertainStatisticController.class);

    @Autowired
    private ZooEntertainDetailService zooEntertainDetailService;

    @ApiOperation(value = "招待列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "size", value = "分页大小", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "trustees", value = "经办人姓名", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "trusteesPhone", value = "经办人电话", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "entertainPersonName", value = "招待人姓名", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "entertainPersonPhone", value = "招待人电话", required = false, dataType = "string", paramType = "query"),
    })
    //@RequiresPermissions("zoo:onAccountHangupDetail:read")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ResponseBody
    public WebResult list(HttpServletRequest request,
            @RequestParam(required = false, defaultValue = "0", value = "page") int page,
            @RequestParam(required = false, defaultValue = "10", value = "size") int size,
            @RequestParam(required = false, value = "startTime") String startTime,
            @RequestParam(required = false, value = "endTime") String endTime,
            @RequestParam(required = false, value = "entertainPersonName") String entertainPersonName,
            @RequestParam(required = false, value = "trusteesPhone") String trusteesPhone,
            @RequestParam(required = false, value = "entertainPersonPhone") String entertainPersonPhone,
            @RequestParam(required = false, value = "trustees") String trustees) {
        ZooEntertainDetailExample example = new ZooEntertainDetailExample();
        ZooEntertainDetailExample.Criteria criteria = example.createCriteria();
        try {
            if(StringUtils.isNotEmpty(startTime) && StringUtils.isNotEmpty(endTime)){
                criteria.andOrderTimeBetween(DateUtil.convertStringToDate(startTime + " 00:00:00","yyyy-MM-dd HH:mm:ss"),DateUtil.convertStringToDate(endTime + " 23:59:59","yyyy-MM-dd HH:mm:ss"));
            }else if(StringUtils.isNotEmpty(startTime) && StringUtils.isEmpty(endTime)){
                criteria.andOrderTimeGreaterThanOrEqualTo(DateUtil.convertStringToDate(startTime + " 00:00:00","yyyy-MM-dd HH:mm:ss"));
            }else if(StringUtils.isNotEmpty(endTime) && StringUtils.isEmpty(startTime)){
                criteria.andOrderTimeLessThanOrEqualTo(DateUtil.convertStringToDate(endTime + " 00:00:00","yyyy-MM-dd HH:mm:ss"));
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        criteria.andOrderCodeIsNotNull();

        if(StringUtils.isNotEmpty(entertainPersonName)){
            criteria.andEntertainPersonNameLike("%"+entertainPersonName+"%");
        }
        if(StringUtils.isNotEmpty(trusteesPhone)){
            criteria.andTrusteesEqualTo(trusteesPhone);
        }
        if(StringUtils.isNotEmpty(entertainPersonPhone)){
            criteria.andEntertainPersonPhoneEqualTo(entertainPersonPhone);
        }
        if(StringUtils.isNotEmpty(trustees)){
            criteria.andTrusteesLike("%"+trustees+"%");
        }

        Organizer orgnization = WebUpmsContext.getOrgnization(request);
        criteria.andOrganizerIdEqualTo(orgnization.getOrganizerId());
        example.setOrderByClause(" order_time desc ");
        List<ZooEntertainDetail> list = zooEntertainDetailService.selectByExampleForStartPage(example, page, size);
        int count = zooEntertainDetailService.countByExample(example);
        WebResult result = WebResult.getSuccessResult();
        result.put("list",list);
        result.put("count",count);
        return result;
    }

}
