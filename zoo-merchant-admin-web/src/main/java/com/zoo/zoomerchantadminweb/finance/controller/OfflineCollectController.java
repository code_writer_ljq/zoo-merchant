package com.zoo.zoomerchantadminweb.finance.controller;

import com.zoo.activity.dao.model.Activity;
import com.zoo.activity.dao.model.Orders;
import com.zoo.activity.dao.model.Organizer;
import com.zoo.activity.dao.nutz.pojo.CommonAccount;
import com.zoo.activity.service.OrdersService;
import com.zoo.activity.util.DateUtil;
import com.zoo.bean.util.ActivityBeanUtils;
import com.zoo.finance.dao.model.*;
import com.zoo.finance.service.*;
import com.zoo.ota.OtaConstant;
import com.zoo.rest.sms.sdk.utils.encoder.BASE64Encoder;
import com.zoo.web.bean.WebResult;
import com.zoo.zoomerchantadminweb.context.WebUpmsContext;
import com.zoo.zoomerchantadminweb.finance.controller.poi.util.FinanceExcelUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import jodd.util.collection.SortedArrayList;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.CellReference;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.hssf.util.Region;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.util.CellRangeAddress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URLEncoder;
import java.util.*;

@Api(value = "api/finance/offlineCollect", tags = {"【财务系统】线下账务汇总"}, description = "线下账务汇总")
@RestController
@RequestMapping("api/finance/offlineCollect")
public class OfflineCollectController {

    private static final Logger LOGGER = LoggerFactory.getLogger(OfflineCollectController.class);

    @Autowired
    private ZooOperatorLoginService zooOperatorLoginService;

    @Autowired
    private ZooOperatorOrderStattisticsService zooOperatorOrderStattisticsService;

    @Autowired
    private ZooOperatorDepositService zooOperatorDepositService;

    @Autowired
    private ZooBalanceDepositIndemnityService zooBalanceDepositIndemnityService;

    @Autowired
    private OrdersService ordersService;

    @Autowired
    private ZooOnAccountOrdersService zooOnAccountOrdersService;

    @Autowired
    private ZooEntertainOrdersService zooEntertainOrdersService;

    /**
     * 导出线下财务报表
     */
    @ApiOperation(value = "「导出」导出订单信息", notes = "导出订单信息（按照操作员和产品分类进行导出")
    @RequestMapping(value = "/orders/export", method = RequestMethod.GET)
    public void exportOrdersWithOperatorAndEpisodeInfo(@RequestParam(value = "startTime", required = false) String startTime,
                                                       @RequestParam(value = "endTime", required = false) String endTime,
                                                       HttpServletRequest request, HttpServletResponse response) throws Exception {
        Organizer organizer = WebUpmsContext.getOrgnization(request);

        Date st = null;
        if (StringUtils.isNotEmpty(startTime)) {
            if (startTime.length() == 10) {
                startTime = startTime + " 00:00:00";
            }
            st = DateUtil.convertStringToDate(startTime, "yyyy-MM-dd HH:mm:ss");
        }

        Date et = null;
        if (StringUtils.isNotEmpty(endTime)) {
            if (endTime.length() == 10) {
                endTime = endTime + " 23:59:59";
            }
            et = DateUtil.convertStringToDate(endTime, "yyyy-MM-dd HH:mm:ss");
        }

        response.reset();
        response.setContentType("application/x-excel");
        String agent = request.getHeader("User-Agent");

        String outfile = "线下财务报表";
        if(StringUtils.isNotEmpty(startTime)){
            outfile += "_" + startTime.replace("-","").substring(0,8);
        }
        if(StringUtils.isNotEmpty(endTime)){
            outfile += "_" + endTime.replace("-","").substring(0,8);
        }

        String fileName = encodeDownloadFilename(outfile,agent);
        response.setHeader("Content-disposition", "attachment;filename=" + fileName + ".xls");
        ServletOutputStream outputStream = response.getOutputStream();
        createHSSFWorkbook(st, et,organizer, outputStream);
        outputStream.close();
    }

    public static String encodeDownloadFilename(String filename, String agent)
            throws IOException {
        if (agent.contains("Firefox")) { // 火狐浏览器
            filename = "=?UTF-8?B?"
                    + new BASE64Encoder().encode(filename.getBytes("utf-8"))
                    + "?=";
            filename = filename.replaceAll("\r\n", "");
        } else { // IE及其他浏览器
            filename = URLEncoder.encode(filename, "utf-8");
            filename = filename.replace("+"," ");
        }
        return filename;
    }

    private void createHSSFWorkbook(Date startTime, Date endTime, Organizer organizer, OutputStream out) throws IOException {

        //订单数据
        List<Orders> orderslist = ordersService.getOrdersByCashierAndPayTypeIds(startTime, endTime, null, null, null, null, organizer.getOrganizerId(), null,ActivityBeanUtils.OfflinePayNameEnum.getPayTypeIds());

        //押金总收入
        BigDecimal totalInDeposit = BigDecimal.ZERO;
        //押金总支出
        BigDecimal totalOutDeposit = BigDecimal.ZERO;
        //产品总金额
        BigDecimal totalOrderPrice = BigDecimal.ZERO;
        //退款总金额
        BigDecimal totalRefundPrice = BigDecimal.ZERO;
        //赔偿总金额
        BigDecimal totalIndemnityPrice = BigDecimal.ZERO;
        //赔偿
        Map<Integer,Map<String,Object>> indemnityProduct = new HashMap<Integer, Map<String, Object>>();
        //产品统计
        Map<Integer,Map<String,Map<String,Object>>> saleProductStastic = new HashMap<Integer,Map<String,Map<String,Object>>>();
        //产品
        Map<Integer,Map<String,Object>> orderProduct = new HashMap<Integer, Map<String, Object>>();
        //异常退款产品
        Map<Integer,Map<String,Object>> refundProduct = new HashMap<Integer, Map<String, Object>>();
        //支付方式汇总
        Map<Integer,Map<String,BigDecimal>> payTypeStastic = new HashMap<Integer, Map<String, BigDecimal>>();
        //处置支付
        BigDecimal totalStoragePayPrice = BigDecimal.ZERO;
        //押金收入汇总
        Map<Integer,Map<String,BigDecimal>> depositStastic = new HashMap<Integer, Map<String, BigDecimal>>();
        //押金收入汇总
        List<Map<String,Object>> allStastics = new ArrayList<Map<String,Object>>();
        //租赁物明细查询
        Map<String,Map<String, Object>> rentListMap =new HashMap<String,Map<String, Object>>();
        //租赁物赔偿统计
        Map<String,Map<String, Object>> rentIndemnityListMap =new HashMap<String,Map<String, Object>>();
        //增值服务统计
        Map<String,Map<String, Object>> addServiceListMap =new HashMap<String,Map<String, Object>>();
        //教练课程  ..产品名称》场次名称》票券名称
        Map<String,Map<String,Map<String,Map<String,Object>>>> coachCourseListMap= new HashMap<String,Map<String,Map<String,Map<String,Object>>>>();
        //教练课时卡
        Map<String,Map<String,Map<String,Map<String,Object>>>> coachCardListMap= new HashMap<String,Map<String,Map<String,Map<String,Object>>>>();
        //教练预约
        Map<String,Map<String,Map<String,Map<String,Object>>>> coachVisitListMap= new HashMap<String,Map<String,Map<String,Map<String,Object>>>>();

        //教练支付方式统计
        Map<String,Map<String,Map<String,Map<String,Map<String,Object>>>>> coachPayListMap= new HashMap<String,Map<String,Map<String,Map<String,Map<String,Object>>>>>();

        Set<String> payNameSet = new HashSet<String>();

        //教练支付统计
        //支付方式汇总
        Map<Integer,Map<String,BigDecimal>> coachPayTypeStastic = new HashMap<Integer, Map<String, BigDecimal>>();
        if(orderslist!=null){
            for (Orders orders : orderslist) {

                //设置支付方式
                payNameSet.add(FinanceExcelUtil.getPayName(orders.getPayTypeId()));

                //押金收入
                if(orders.getParentId()==null||orders.getParentId().intValue()==0){
                    Map<String,BigDecimal> depositStasticMap = depositStastic.get(orders.getPayTypeId());
                    if(depositStasticMap == null){
                        depositStasticMap=new HashMap<String,BigDecimal>();
                        BigDecimal deposit=orders.getDeposit()==null?BigDecimal.ZERO:orders.getDeposit().setScale(2,RoundingMode.HALF_UP);
                        depositStasticMap.put("0",deposit);
                        depositStastic.put(orders.getPayTypeId(),depositStasticMap);
                    }else{
                        BigDecimal totalPrice=depositStasticMap.get("0")==null?BigDecimal.ZERO:depositStasticMap.get("0");
                        BigDecimal deposit=orders.getDeposit()==null?BigDecimal.ZERO:orders.getDeposit().setScale(2,RoundingMode.HALF_UP);
                        totalPrice=totalPrice.add(deposit);
                        depositStasticMap.put("0",totalPrice);
                        depositStastic.put(orders.getPayTypeId(),depositStasticMap);
                    }
                    totalInDeposit = totalInDeposit.add(orders.getDeposit()==null?BigDecimal.ZERO:orders.getDeposit().setScale(2,RoundingMode.HALF_UP));
                }

                if(orders.getEquipmentId()==null && orders.getParentId()!=null && (orders.getIncrement() == null || orders.getIncrement().intValue()!=1)){

                    //筛选类型
                    Map<String, Map<String,Object>> map = saleProductStastic.get(orders.getActivityTypeId());
                    if(map == null){
                        map = new HashMap<String, Map<String,Object>>();
                        saleProductStastic.put(orders.getActivityTypeId(),map);
                    }

                    //商品
                    Map<String, Object> activityMap = map.get(orders.getActivityName());
                    if(activityMap==null){
                        activityMap = new HashMap<String, Object>();
                        map.put(orders.getActivityName(),activityMap);
                    }

                    int num = MapUtils.getIntValue(activityMap, "num", 0);
                    activityMap.put("num",num + orders.getChargeNum());

                    double price = MapUtils.getDoubleValue(activityMap, "price", 0);
                    activityMap.put("price",orders.getTotalPrice().doubleValue() + price);


                    double payPrice = MapUtils.getDoubleValue(activityMap,orders.getPayTypeId() + "_price",0);
                    activityMap.put(orders.getPayTypeId() + "_price",orders.getTotalPrice().doubleValue() + payPrice);

                    int payNum = MapUtils.getInteger(activityMap, orders.getPayTypeId() + "_num", 0);
                    activityMap.put(orders.getPayTypeId() + "_num",orders.getChargeNum() + payNum);

                    //线下现金  0
                    /*double xianjinPrice = MapUtils.getDoubleValue(activityMap, "xianjinPrice", 0);
                    Integer xianjinNum = MapUtils.getInteger(activityMap, "xianjinNum", 0);
                    if("0".equals(String.valueOf(orders.getPayTypeId()))){
                        activityMap.put("xianjinPrice",orders.getTotalPrice().doubleValue() + xianjinPrice);
                        activityMap.put("xianjinNum",orders.getChargeNum() + xianjinNum);
                    }else{
                        if(activityMap.get("xianjinPrice")==null){
                            activityMap.put("xianjinPrice",xianjinPrice);
                            activityMap.put("xianjinNum",xianjinNum);
                        }
                    }*/

                    //线下支付宝 2
                    /*double zhifubaoPrice = MapUtils.getDoubleValue(activityMap, "zhifubaoPrice", 0);
                    Integer zhifubaoNum = MapUtils.getInteger(activityMap, "zhifubaoNum", 0);
                    if("2".equals(String.valueOf(orders.getPayTypeId()))){
                        activityMap.put("zhifubaoPrice",orders.getTotalPrice().doubleValue() + zhifubaoPrice);
                        activityMap.put("zhifubaoNum",orders.getChargeNum() + zhifubaoNum);
                    }else{
                        if(activityMap.get("zhifubaoPrice")==null){
                            activityMap.put("zhifubaoPrice",zhifubaoPrice);
                            activityMap.put("zhifubaoNum",zhifubaoNum);
                        }
                    }*/

                    //线下微信 4
                    /*double weixinPrice = MapUtils.getDoubleValue(activityMap, "weixinPrice", 0);
                    Integer weixinNum = MapUtils.getInteger(activityMap, "weixinNum", 0);
                    if("4".equals(String.valueOf(orders.getPayTypeId()))){
                        activityMap.put("weixinPrice",orders.getTotalPrice().doubleValue() + weixinPrice);
                        activityMap.put("weixinNum",orders.getChargeNum() + weixinNum);
                    }else{
                        if(activityMap.get("weixinPrice")==null){
                            activityMap.put("weixinPrice",weixinPrice);
                            activityMap.put("weixinNum",weixinNum);
                        }
                    }*/

                    //线下银行卡 3
                    /*double yinhangkaPrice = MapUtils.getDoubleValue(activityMap, "yinhangkaPrice", 0);
                    Integer yinhangkaNum = MapUtils.getInteger(activityMap, "yinhangkaNum", 0);
                    if("3".equals(String.valueOf(orders.getPayTypeId()))){
                        activityMap.put("yinhangkaPrice",orders.getTotalPrice().doubleValue() + yinhangkaPrice);
                        activityMap.put("yinhangkaNum",orders.getChargeNum() + yinhangkaNum);
                    }else{
                        if(activityMap.get("yinhangkaPrice")==null){
                            activityMap.put("yinhangkaPrice",yinhangkaPrice);
                            activityMap.put("yinhangkaNum",yinhangkaNum);
                        }
                    }*/

                    //线下招待 10
                    /*double zhaodaiPrice = MapUtils.getDoubleValue(activityMap, "zhaodaiPrice", 0);
                    Integer zhaodaiNum = MapUtils.getInteger(activityMap, "zhaodaiNum", 0);
                    if("10".equals(String.valueOf(orders.getPayTypeId()))){
                        activityMap.put("zhaodaiPrice",orders.getTotalPrice().doubleValue() + zhaodaiPrice);
                        activityMap.put("zhaodaiNum",orders.getChargeNum() + zhaodaiNum);
                    }else{
                        if(activityMap.get("zhaodaiPrice")==null){
                            activityMap.put("zhaodaiPrice",zhaodaiPrice);
                            activityMap.put("zhaodaiNum",zhaodaiNum);
                        }
                    }*/
                    //线下挂账 9
                    /*double guazhangPrice = MapUtils.getDoubleValue(activityMap, "guazhangPrice", 0);
                    Integer guazhangNum = MapUtils.getInteger(activityMap, "guazhangNum", 0);
                    if("9".equals(String.valueOf(orders.getPayTypeId()))){
                        activityMap.put("guazhangPrice",orders.getTotalPrice().doubleValue() + guazhangPrice);
                        activityMap.put("guazhangNum",orders.getChargeNum() + guazhangNum);
                    }else{
                        if(activityMap.get("guazhangPrice")==null){
                            activityMap.put("guazhangPrice",guazhangPrice);
                            activityMap.put("guazhangNum",guazhangNum);
                        }
                    }*/
                    //线上储值支付 7

                    //线下储值支付 8
                    /*double chuzhiPrice = MapUtils.getDoubleValue(activityMap, "chuzhiPrice", 0);
                    Integer chuzhiNum = MapUtils.getInteger(activityMap, "chuzhiNum", 0);
                    if("8".equals(String.valueOf(orders.getPayTypeId()))){
                        activityMap.put("chuzhiPrice",orders.getTotalPrice().doubleValue() + chuzhiPrice);
                        activityMap.put("chuzhiNum",orders.getChargeNum() + chuzhiNum);
                    }else{
                        if(activityMap.get("chuzhiPrice")==null){
                            activityMap.put("chuzhiPrice",chuzhiPrice);
                            activityMap.put("chuzhiNum",chuzhiNum);
                        }
                    }*/
                }
                //租赁物统计
                if(orders.getEquipmentId()!=null && orders.getParentId()!=null){
                    if(!"1".equals(String.valueOf(orders.getPayTypeId()))&&!"7".equals(String.valueOf(orders.getPayTypeId()))){
                        //商品
                        Map<String, Object> rentMap = rentListMap.get(orders.getEquipmentName());
                        if(rentMap==null){
                            rentMap = new HashMap<String, Object>();
                            rentListMap.put(orders.getEquipmentName(),rentMap);
                        }

                        int num = MapUtils.getIntValue(rentMap, "num", 0);
                        rentMap.put("num",num + orders.getChargeNum());

                        double price = MapUtils.getDoubleValue(rentMap, "price", 0);
                        rentMap.put("price",orders.getTotalPrice().doubleValue() + price);

                        double payPrice = MapUtils.getDoubleValue(rentMap,orders.getPayTypeId() + "_price",0);
                        rentMap.put(orders.getPayTypeId() + "_price",orders.getTotalPrice().doubleValue() + payPrice);

                        int payNum = MapUtils.getInteger(rentMap, orders.getPayTypeId() + "_num", 0);
                        rentMap.put(orders.getPayTypeId() + "_num",orders.getChargeNum() + payNum);

                        //线下现金  0
                        /*double xianjinPrice = MapUtils.getDoubleValue(rentMap, "xianjinPrice", 0);
                        Integer xianjinNum = MapUtils.getInteger(rentMap, "xianjinNum", 0);
                        if("0".equals(String.valueOf(orders.getPayTypeId()))){
                            rentMap.put("xianjinPrice",orders.getTotalPrice().doubleValue() + xianjinPrice);
                            rentMap.put("xianjinNum",orders.getChargeNum() + xianjinNum);
                        }else{
                            if(rentMap.get("xianjinPrice")==null){
                                rentMap.put("xianjinPrice",xianjinPrice);
                                rentMap.put("xianjinNum",xianjinNum);
                            }
                        }*/
                        //线下支付宝 2
                        /*double zhifubaoPrice = MapUtils.getDoubleValue(rentMap, "zhifubaoPrice", 0);
                        Integer zhifubaoNum = MapUtils.getInteger(rentMap, "zhifubaoNum", 0);
                        if("2".equals(String.valueOf(orders.getPayTypeId()))){
                            rentMap.put("zhifubaoPrice",orders.getTotalPrice().doubleValue() + zhifubaoPrice);
                            rentMap.put("zhifubaoNum",orders.getChargeNum() + zhifubaoNum);
                        }else{
                            if(rentMap.get("zhifubaoPrice")==null){
                                rentMap.put("zhifubaoPrice",zhifubaoPrice);
                                rentMap.put("zhifubaoNum",zhifubaoNum);
                            }
                        }*/
                        //线下微信 4
                        /*double weixinPrice = MapUtils.getDoubleValue(rentMap, "weixinPrice", 0);
                        Integer weixinNum = MapUtils.getInteger(rentMap, "weixinNum", 0);
                        if("4".equals(String.valueOf(orders.getPayTypeId()))){
                            rentMap.put("weixinPrice",orders.getTotalPrice().doubleValue() + weixinPrice);
                            rentMap.put("weixinNum",orders.getChargeNum() + weixinNum);
                        }else{
                            if(rentMap.get("weixinPrice")==null){
                                rentMap.put("weixinPrice",weixinPrice);
                                rentMap.put("weixinNum",weixinNum);
                            }
                        }*/
                        //线下银行卡 3
                        /*double yinhangkaPrice = MapUtils.getDoubleValue(rentMap, "yinhangkaPrice", 0);
                        Integer yinhangkaNum = MapUtils.getInteger(rentMap, "yinhangkaNum", 0);
                        if("3".equals(String.valueOf(orders.getPayTypeId()))){
                            rentMap.put("yinhangkaPrice",orders.getTotalPrice().doubleValue() + yinhangkaPrice);
                            rentMap.put("yinhangkaNum",orders.getChargeNum() + yinhangkaNum);
                        }else{
                            if(rentMap.get("yinhangkaPrice")==null){
                                rentMap.put("yinhangkaPrice",yinhangkaPrice);
                                rentMap.put("yinhangkaNum",yinhangkaNum);
                            }
                        }*/
                        //线下招待 10
                        /*double zhaodaiPrice = MapUtils.getDoubleValue(rentMap, "zhaodaiPrice", 0);
                        Integer zhaodaiNum = MapUtils.getInteger(rentMap, "zhaodaiNum", 0);
                        if("10".equals(String.valueOf(orders.getPayTypeId()))){
                            rentMap.put("zhaodaiPrice",orders.getTotalPrice().doubleValue() + zhaodaiPrice);
                            rentMap.put("zhaodaiNum",orders.getChargeNum() + zhaodaiNum);
                        }else{
                            if(rentMap.get("zhaodaiPrice")==null){
                                rentMap.put("zhaodaiPrice",zhaodaiPrice);
                                rentMap.put("zhaodaiNum",zhaodaiNum);
                            }
                        }*/
                        //线下挂账 9
                        /*double guazhangPrice = MapUtils.getDoubleValue(rentMap, "guazhangPrice", 0);
                        Integer guazhangNum = MapUtils.getInteger(rentMap, "guazhangNum", 0);
                        if("9".equals(String.valueOf(orders.getPayTypeId()))){
                            rentMap.put("guazhangPrice",orders.getTotalPrice().doubleValue() + guazhangPrice);
                            rentMap.put("guazhangNum",orders.getChargeNum() + guazhangNum);
                        }else{
                            if(rentMap.get("guazhangPrice")==null){
                                rentMap.put("guazhangPrice",guazhangPrice);
                                rentMap.put("guazhangNum",guazhangNum);
                            }
                        }*/
                        //线上储值支付 7

                        //线下储值支付 8
                        /*double chuzhiPrice = MapUtils.getDoubleValue(rentMap, "chuzhiPrice", 0);
                        Integer chuzhiNum = MapUtils.getInteger(rentMap, "chuzhiNum", 0);
                        if("8".equals(String.valueOf(orders.getPayTypeId()))){
                            rentMap.put("chuzhiPrice",orders.getTotalPrice().doubleValue() + chuzhiPrice);
                            rentMap.put("chuzhiNum",orders.getChargeNum() + chuzhiNum);
                        }else{
                            if(rentMap.get("chuzhiPrice")==null){
                                rentMap.put("chuzhiPrice",chuzhiPrice);
                                rentMap.put("chuzhiNum",chuzhiNum);
                            }
                        }*/
                    }
                }
                //支付统计
                if(orders.getParentId() == null || orders.getParentId()==0){
                    Map<String,BigDecimal> payTypeMap = payTypeStastic.get(orders.getPayTypeId());
                    if(payTypeMap==null){
                        payTypeMap=new HashMap<String,BigDecimal>();
                        payTypeMap.put("0",orders.getTotalPrice().setScale(2,RoundingMode.HALF_UP));
                        payTypeStastic.put(orders.getPayTypeId(),payTypeMap);
                    }else{
                        BigDecimal totalPrice=payTypeMap.get("0")==null?BigDecimal.ZERO:payTypeMap.get("0");
                        totalPrice=totalPrice.add(orders.getTotalPrice()).setScale(2,RoundingMode.HALF_UP);
                        payTypeMap.put("0",totalPrice);
                        payTypeStastic.put(orders.getPayTypeId(),payTypeMap);
                    }

                    totalOrderPrice = totalOrderPrice.add(orders.getTotalPrice()).setScale(2,RoundingMode.HALF_UP);

                    if(orders.getPayTypeId().intValue()==8 || orders.getPayTypeId().intValue()==7){
                        totalStoragePayPrice = totalStoragePayPrice.add(orders.getTotalPrice());
                    }
                }
                //增值服务统计
                if(orders.getEquipmentId()==null && orders.getParentId()!=null && orders.getIncrement().intValue()==1){
                    if(!"1".equals(String.valueOf(orders.getPayTypeId()))&&!"7".equals(String.valueOf(orders.getPayTypeId()))){
                        //商品
                        Map<String, Object> addServiceMap = addServiceListMap.get(orders.getChargeName());
                        if(addServiceMap==null){
                            addServiceMap = new HashMap<String, Object>();
                            addServiceListMap.put(orders.getChargeName(),addServiceMap);
                        }

                        int num = MapUtils.getIntValue(addServiceMap, "num", 0);
                        addServiceMap.put("num",num + orders.getChargeNum());

                        double price = MapUtils.getDoubleValue(addServiceMap, "price", 0);
                        addServiceMap.put("price",orders.getTotalPrice().doubleValue() + price);

                        //支付方式金额和数量
                        double payPrice = MapUtils.getDoubleValue(addServiceMap,orders.getPayTypeId() + "_price",0);
                        addServiceMap.put(orders.getPayTypeId() + "_price",orders.getTotalPrice().doubleValue() + payPrice);

                        int payNum = MapUtils.getInteger(addServiceMap, orders.getPayTypeId() + "_num", 0);
                        addServiceMap.put(orders.getPayTypeId() + "_num",orders.getChargeNum() + payNum);

                        //线下现金  0
                        /*double xianjinPrice = MapUtils.getDoubleValue(addServiceMap, "xianjinPrice", 0);
                        Integer xianjinNum = MapUtils.getInteger(addServiceMap, "xianjinNum", 0);
                        if("0".equals(String.valueOf(orders.getPayTypeId()))){
                            addServiceMap.put("xianjinPrice",orders.getTotalPrice().doubleValue() + xianjinPrice);
                            addServiceMap.put("xianjinNum",orders.getChargeNum() + xianjinNum);
                        }else{
                            if(addServiceMap.get("xianjinPrice")==null){
                                addServiceMap.put("xianjinPrice",xianjinPrice);
                                addServiceMap.put("xianjinNum",xianjinNum);
                            }
                        }*/
                        //线下支付宝 2
                        /*double zhifubaoPrice = MapUtils.getDoubleValue(addServiceMap, "zhifubaoPrice", 0);
                        Integer zhifubaoNum = MapUtils.getInteger(addServiceMap, "zhifubaoNum", 0);
                        if("2".equals(String.valueOf(orders.getPayTypeId()))){
                            addServiceMap.put("zhifubaoPrice",orders.getTotalPrice().doubleValue() + zhifubaoPrice);
                            addServiceMap.put("zhifubaoNum",orders.getChargeNum() + zhifubaoNum);
                        }else{
                            if(addServiceMap.get("zhifubaoPrice")==null){
                                addServiceMap.put("zhifubaoPrice",zhifubaoPrice);
                                addServiceMap.put("zhifubaoNum",zhifubaoNum);
                            }
                        }*/
                        //线下微信 4
                        /*double weixinPrice = MapUtils.getDoubleValue(addServiceMap, "weixinPrice", 0);
                        Integer weixinNum = MapUtils.getInteger(addServiceMap, "weixinNum", 0);
                        if("4".equals(String.valueOf(orders.getPayTypeId()))){
                            addServiceMap.put("weixinPrice",orders.getTotalPrice().doubleValue() + weixinPrice);
                            addServiceMap.put("weixinNum",orders.getChargeNum() + weixinNum);
                        }else{
                            if(addServiceMap.get("weixinPrice")==null){
                                addServiceMap.put("weixinPrice",weixinPrice);
                                addServiceMap.put("weixinNum",weixinNum);
                            }
                        }*/
                        //线下银行卡 3
                        /*double yinhangkaPrice = MapUtils.getDoubleValue(addServiceMap, "yinhangkaPrice", 0);
                        Integer yinhangkaNum = MapUtils.getInteger(addServiceMap, "yinhangkaNum", 0);
                        if("3".equals(String.valueOf(orders.getPayTypeId()))){
                            addServiceMap.put("yinhangkaPrice",orders.getTotalPrice().doubleValue() + yinhangkaPrice);
                            addServiceMap.put("yinhangkaNum",orders.getChargeNum() + yinhangkaNum);
                        }else{
                            if(addServiceMap.get("yinhangkaPrice")==null){
                                addServiceMap.put("yinhangkaPrice",yinhangkaPrice);
                                addServiceMap.put("yinhangkaNum",yinhangkaNum);
                            }
                        }*/
                        //线下招待 10
                        /*double zhaodaiPrice = MapUtils.getDoubleValue(addServiceMap, "zhaodaiPrice", 0);
                        Integer zhaodaiNum = MapUtils.getInteger(addServiceMap, "zhaodaiNum", 0);
                        if("10".equals(String.valueOf(orders.getPayTypeId()))){
                            addServiceMap.put("zhaodaiPrice",orders.getTotalPrice().doubleValue() + zhaodaiPrice);
                            addServiceMap.put("zhaodaiNum",orders.getChargeNum() + zhaodaiNum);
                        }else{
                            if(addServiceMap.get("zhaodaiPrice")==null){
                                addServiceMap.put("zhaodaiPrice",zhaodaiPrice);
                                addServiceMap.put("zhaodaiNum",zhaodaiNum);
                            }
                        }*/
                        //线下挂账 9
                        /*double guazhangPrice = MapUtils.getDoubleValue(addServiceMap, "guazhangPrice", 0);
                        Integer guazhangNum = MapUtils.getInteger(addServiceMap, "guazhangNum", 0);
                        if("9".equals(String.valueOf(orders.getPayTypeId()))){
                            addServiceMap.put("guazhangPrice",orders.getTotalPrice().doubleValue() + guazhangPrice);
                            addServiceMap.put("guazhangNum",orders.getChargeNum() + guazhangNum);
                        }else{
                            if(addServiceMap.get("guazhangPrice")==null){
                                addServiceMap.put("guazhangPrice",guazhangPrice);
                                addServiceMap.put("guazhangNum",guazhangNum);
                            }
                        }*/
                        //线上储值支付 7

                        //线下储值支付 8
                        /*double chuzhiPrice = MapUtils.getDoubleValue(addServiceMap, "chuzhiPrice", 0);
                        Integer chuzhiNum = MapUtils.getInteger(addServiceMap, "chuzhiNum", 0);
                        if("8".equals(String.valueOf(orders.getPayTypeId()))){
                            addServiceMap.put("chuzhiPrice",orders.getTotalPrice().doubleValue() + chuzhiPrice);
                            addServiceMap.put("chuzhiNum",orders.getChargeNum() + chuzhiNum);
                        }else{
                            if(addServiceMap.get("chuzhiPrice")==null){
                                addServiceMap.put("chuzhiPrice",chuzhiPrice);
                                addServiceMap.put("chuzhiNum",chuzhiNum);
                            }
                        }*/
                    }
                }
                //教练预约
                if(orders.getParentId()!=null&&"2".equals(String.valueOf(orders.getActivityTypeId()))){

                    Map<String, Map<String, Map<String, Map<String, Object>>>> coachPayMap = coachPayListMap.get("教练预约");
                    //产品
                    if(coachPayMap == null){
                        coachPayMap = new HashMap<String, Map<String, Map<String, Map<String, Object>>>>();
                        coachPayListMap.put("教练预约",coachPayMap);
                    }
                    payCoachInitMap(orders,coachPayMap);


                    //产品
                    Map<String,Map<String, Map<String,Object>>> activityMap = coachVisitListMap.get("教练预约");
                    if(activityMap == null){
                        activityMap = new HashMap<String, Map<String,Map<String,Object>>>();
                        coachVisitListMap.put("教练预约",activityMap);
                    }

                    //场次
                    Map<String,Map<String, Object>> episodeNameMap = activityMap.get(orders.getActivityName());
                    if(episodeNameMap==null){
                        episodeNameMap = new HashMap<String,Map<String, Object>>();
                        activityMap.put(orders.getActivityName(),episodeNameMap);
                    }
                    //票券
                    Map<String, Object> chargeMap = episodeNameMap.get(orders.getChargeName());
                    if(chargeMap==null){
                        chargeMap = new HashMap<String, Object>();
                        episodeNameMap.put(orders.getChargeName(),chargeMap);
                    }

                    int num = MapUtils.getIntValue(chargeMap, "num", 0);
                    chargeMap.put("num",num + orders.getChargeNum());

                    double price = MapUtils.getDoubleValue(chargeMap, "price", 0);
                    chargeMap.put("price",orders.getTotalPrice().doubleValue() + price);
                }
                //教练课程
                if(orders.getParentId()!=null&&"8".equals(String.valueOf(orders.getActivityTypeId()))&&"0".equals(String.valueOf(orders.getActivityMode()))){

                    Map<String, Map<String, Map<String, Map<String, Object>>>> coachPayMap = coachPayListMap.get("课程");
                    //产品
                    if(coachPayMap == null){
                        coachPayMap = new HashMap<String, Map<String, Map<String, Map<String, Object>>>>();
                        coachPayListMap.put("课程",coachPayMap);
                    }
                    payCoachInitMap(orders,coachPayMap);

                    //产品
                    Map<String,Map<String, Map<String,Object>>> activityMap = coachCourseListMap.get("课程");
                    if(activityMap == null){
                        activityMap = new HashMap<String, Map<String,Map<String,Object>>>();
                        coachCourseListMap.put("课程",activityMap);
                    }

                    //场次
                    Map<String,Map<String, Object>> episodeNameMap = activityMap.get(orders.getActivityName());
                    if(episodeNameMap==null){
                        episodeNameMap = new HashMap<String,Map<String, Object>>();
                        activityMap.put(orders.getActivityName(),episodeNameMap);
                    }
                    //票券
                    Map<String, Object> chargeMap = episodeNameMap.get(orders.getChargeName());
                    if(chargeMap==null){
                        chargeMap = new HashMap<String, Object>();
                        episodeNameMap.put(orders.getChargeName(),chargeMap);
                    }

                    int num = MapUtils.getIntValue(chargeMap, "num", 0);
                    chargeMap.put("num",num + orders.getChargeNum());

                    double price = MapUtils.getDoubleValue(chargeMap, "price", 0);
                    chargeMap.put("price",orders.getTotalPrice().doubleValue() + price);
                }
                //教练课时卡
                if(orders.getParentId()!=null&&"8".equals(String.valueOf(orders.getActivityTypeId()))&&"1".equals(String.valueOf(orders.getActivityMode()))){

                    Map<String, Map<String, Map<String, Map<String, Object>>>> coachPayMap = coachPayListMap.get("课时卡");
                    //产品
                    if(coachPayMap == null){
                        coachPayMap = new HashMap<String, Map<String, Map<String, Map<String, Object>>>>();
                        coachPayListMap.put("课时卡",coachPayMap);
                    }
                    payCoachInitMap(orders,coachPayMap);

                    //产品
                    Map<String,Map<String, Map<String,Object>>> activityMap = coachCardListMap.get("课时卡");
                    if(activityMap == null){
                        activityMap = new HashMap<String, Map<String,Map<String,Object>>>();
                        coachCardListMap.put("课时卡",activityMap);
                    }

                    //场次
                    Map<String,Map<String, Object>> episodeNameMap = activityMap.get(orders.getActivityName());
                    if(episodeNameMap==null){
                        episodeNameMap = new HashMap<String,Map<String, Object>>();
                        activityMap.put(orders.getActivityName(),episodeNameMap);
                    }
                    //票券
                    Map<String, Object> chargeMap = episodeNameMap.get(orders.getChargeName());
                    if(chargeMap==null){
                        chargeMap = new HashMap<String, Object>();
                        episodeNameMap.put(orders.getChargeName(),chargeMap);
                    }

                    int num = MapUtils.getIntValue(chargeMap, "num", 0);
                    chargeMap.put("num",num + orders.getChargeNum());

                    double price = MapUtils.getDoubleValue(chargeMap, "price", 0);
                    chargeMap.put("price",orders.getTotalPrice().doubleValue() + price);
                }
                if(orders.getParentId()!=null&&("2".equals(String.valueOf(orders.getActivityTypeId()))||"8".equals(String.valueOf(orders.getActivityTypeId())))){
                    Map<String,BigDecimal> coachPayTypeMap = coachPayTypeStastic.get(orders.getPayTypeId());
                    if(coachPayTypeMap==null){
                        coachPayTypeMap=new HashMap<String,BigDecimal>();
                        coachPayTypeMap.put("0",orders.getTotalPrice().setScale(2,RoundingMode.HALF_UP));
                        coachPayTypeMap.put("1",BigDecimal.ZERO);
                        coachPayTypeStastic.put(orders.getPayTypeId(),coachPayTypeMap);
                    }else{
                        BigDecimal totalPrice=coachPayTypeMap.get("0")==null?BigDecimal.ZERO:coachPayTypeMap.get("0");
                        totalPrice=totalPrice.add(orders.getTotalPrice()).setScale(2,RoundingMode.HALF_UP);
                        coachPayTypeMap.put("0",totalPrice);
                        coachPayTypeMap.put("1",BigDecimal.ZERO);
                        coachPayTypeStastic.put(orders.getPayTypeId(),coachPayTypeMap);
                    }
                }
            }
        }
        //租赁物
        List<Map<String, Object>> rentOrders = ordersService.getStasticsRentByStartEndTime(startTime, endTime, organizer.getOrganizerId(), null,null,null);
        //超时租赁物
        List<Map<String, Object>> rentTimeOutOrders = ordersService.getStasticsRentByStartEndTime(startTime, endTime,organizer.getOrganizerId(),4,null,null);
        //租赁物和超时租赁物统计
        Map<String,Map<String, Object>> rentAndTimeOutOrders =new HashMap<String,Map<String, Object>>();
        if(rentOrders!=null&&rentOrders.size()>0){
            for (int i = 0; i < rentOrders.size(); i++) {
                Map<String, Object> map=rentOrders.get(i);
                String name=MapUtils.getString(map,"name","");
                Map<String,Object> rentAndTimeOutMap = rentAndTimeOutOrders.get(name);
                if(rentAndTimeOutMap==null){
                    rentAndTimeOutMap=new HashMap<String,Object>();
                    rentAndTimeOutMap.put("totalMoney",new BigDecimal(MapUtils.getDouble(map,"totalMoney",0.00)).setScale(2,RoundingMode.HALF_UP));
                    rentAndTimeOutMap.put("totalNum",MapUtils.getIntValue(map,"totalNum",0));
                    rentAndTimeOutMap.put("timeOutTotalMoney",BigDecimal.ZERO);
                    rentAndTimeOutMap.put("timeOutTotalNum",0);
                    rentAndTimeOutOrders.put(name,rentAndTimeOutMap);
                }else{
                    BigDecimal totalPrice=rentAndTimeOutMap.get("totalMoney")==null?BigDecimal.ZERO:new BigDecimal(MapUtils.getDouble(rentAndTimeOutMap,"totalMoney",0.00)).setScale(2,RoundingMode.HALF_UP);
                    totalPrice=totalPrice.add(new BigDecimal(MapUtils.getDouble(map,"totalMoney",0.00))).setScale(2,RoundingMode.HALF_UP);
                    rentAndTimeOutMap.put("totalMoney",totalPrice);
                    Integer totalNum=rentAndTimeOutMap.get("totalNum")==null?0:Integer.parseInt(rentAndTimeOutMap.get("totalNum").toString());
                    totalNum=totalNum+Integer.parseInt(map.get("totalNum").toString());
                    rentAndTimeOutMap.put("totalNum",totalNum);
                    rentAndTimeOutOrders.put(name,rentAndTimeOutMap);
                }
            }
        }
        if(rentTimeOutOrders!=null&&rentTimeOutOrders.size()>0){
            for (int i = 0; i < rentTimeOutOrders.size(); i++) {
                Map<String, Object> map=rentTimeOutOrders.get(i);
                String name=MapUtils.getString(map,"name","");
                Map<String,Object> rentAndTimeOutMap = rentAndTimeOutOrders.get(name);
                if(rentAndTimeOutMap==null){
                    rentAndTimeOutMap=new HashMap<String,Object>();
                    rentAndTimeOutMap.put("totalMoney",BigDecimal.ZERO);
                    rentAndTimeOutMap.put("totalNum",0);
                    rentAndTimeOutMap.put("timeOutTotalMoney",new BigDecimal(MapUtils.getDouble(map,"timeOutTotalMoney",0.00)).setScale(2,RoundingMode.HALF_UP));
                    rentAndTimeOutMap.put("timeOutTotalNum",MapUtils.getIntValue(map,"timeOutTotalNum",0));
                    rentAndTimeOutOrders.put(name,rentAndTimeOutMap);
                }else{
                    BigDecimal totalPrice=rentAndTimeOutMap.get("timeOutTotalMoney")==null?BigDecimal.ZERO:new BigDecimal(MapUtils.getDouble(rentAndTimeOutMap,"timeOutTotalMoney",0.00)).setScale(2,RoundingMode.HALF_UP);
                    totalPrice=totalPrice.add(new BigDecimal(MapUtils.getDouble(map,"totalMoney",0.00))).setScale(2,RoundingMode.HALF_UP);
                    rentAndTimeOutMap.put("timeOutTotalMoney",totalPrice);
                    Integer totalNum=rentAndTimeOutMap.get("timeOutTotalNum")==null?0:Integer.parseInt(rentAndTimeOutMap.get("timeOutTotalNum").toString());
                    totalNum=totalNum+Integer.parseInt(map.get("totalNum").toString());
                    rentAndTimeOutMap.put("timeOutTotalNum",totalNum);
                    rentAndTimeOutOrders.put(name,rentAndTimeOutMap);
                }
            }
        }

        //增值服务
        List<Map<String, Object>> addServiceOrders = ordersService.getStasticsAddServiceByTime(startTime, endTime, organizer.getOrganizerId(),null);
        //分销渠道
        List<Map<String, Object>> distributorOrders = ordersService.getStasticsDistributorByTime(startTime, endTime, organizer.getOrganizerId());
        //赔偿
        ZooBalanceDepositIndemnityExample indemnityExample = new ZooBalanceDepositIndemnityExample();
        ZooBalanceDepositIndemnityExample.Criteria indemnityExampleCriteria = indemnityExample.createCriteria();
        indemnityExampleCriteria.andOrganizerIdEqualTo(organizer.getOrganizerId());
        // indemnityExampleCriteria.andPayTypeNotEqualTo(1);
        indemnityExampleCriteria.andPayTypeIn(ActivityBeanUtils.OfflinePayNameEnum.getPayTypeIds());
        if(startTime!=null){
            indemnityExampleCriteria.andIndemnityTimeGreaterThanOrEqualTo(startTime);
        }
        if(endTime !=null){
            indemnityExampleCriteria.andIndemnityTimeLessThan(endTime);
        }
        List<ZooBalanceDepositIndemnity> depositIndemnityList = zooBalanceDepositIndemnityService.selectByExample(indemnityExample);
        for (ZooBalanceDepositIndemnity depositIndemnity : depositIndemnityList) {
            Integer equipmentId = depositIndemnity.getEquipmentId();
            Map<String, Object> p = indemnityProduct.get(equipmentId);

            if(p == null){
                p = new HashMap<String,Object>();
                indemnityProduct.put(equipmentId,p);
                p.put("money",BigDecimal.ZERO);
                p.put("name",depositIndemnity.getEquipmentName());
            }
            int num = MapUtils.getIntValue(p,"num",0);
            p.put("num",num+1);
            BigDecimal money = (BigDecimal)p.get("money");
            p.put("money",money.add(depositIndemnity.getIndemnity()).setScale(2,RoundingMode.HALF_UP));
            totalIndemnityPrice = totalIndemnityPrice.add(depositIndemnity.getIndemnity()).setScale(2,RoundingMode.HALF_UP);

            if(!"1".equals(String.valueOf(depositIndemnity.getPayType()))&&!"7".equals(String.valueOf(depositIndemnity.getPayType()))){
                //商品
                Map<String, Object> rentIndemnityMap = rentIndemnityListMap.get(depositIndemnity.getEquipmentName());
                if(rentIndemnityMap==null){
                    rentIndemnityMap = new HashMap<String, Object>();
                    rentIndemnityListMap.put(depositIndemnity.getEquipmentName(),rentIndemnityMap);
                }

                int indemnityNum = MapUtils.getIntValue(rentIndemnityMap, "num", 0);
                rentIndemnityMap.put("num",indemnityNum + 1);

                double price = MapUtils.getDoubleValue(rentIndemnityMap, "price", 0);
                rentIndemnityMap.put("price",depositIndemnity.getIndemnity().doubleValue() + price);

                //支付方式金额和数量
                double payPrice = MapUtils.getDoubleValue(rentIndemnityMap,depositIndemnity.getPayType() + "_price",0);
                rentIndemnityMap.put(depositIndemnity.getPayType() + "_price",depositIndemnity.getIndemnity().doubleValue() + payPrice);

                int payNum = MapUtils.getInteger(rentIndemnityMap, depositIndemnity.getPayType() + "_num", 0);
                rentIndemnityMap.put(depositIndemnity.getPayType() + "_num",depositIndemnity.getIndemnity().doubleValue() + payNum);

                //线下现金  0
                /*double xianjinPrice = MapUtils.getDoubleValue(rentIndemnityMap, "xianjinPrice", 0);
                Integer xianjinNum = MapUtils.getInteger(rentIndemnityMap, "xianjinNum", 0);
                if("0".equals(String.valueOf(depositIndemnity.getPayType()))){
                    rentIndemnityMap.put("xianjinPrice",depositIndemnity.getIndemnity().doubleValue() + xianjinPrice);
                    rentIndemnityMap.put("xianjinNum",xianjinNum+1);
                }else{
                    if(rentIndemnityMap.get("xianjinPrice")==null){
                        rentIndemnityMap.put("xianjinPrice",xianjinPrice);
                        rentIndemnityMap.put("xianjinNum",xianjinNum);
                    }
                }*/
                //线下支付宝 2
                /*double zhifubaoPrice = MapUtils.getDoubleValue(rentIndemnityMap, "zhifubaoPrice", 0);
                Integer zhifubaoNum = MapUtils.getInteger(rentIndemnityMap, "zhifubaoNum", 0);
                if("2".equals(String.valueOf(depositIndemnity.getPayType()))){
                    rentIndemnityMap.put("zhifubaoPrice",depositIndemnity.getIndemnity().doubleValue() + zhifubaoPrice);
                    rentIndemnityMap.put("zhifubaoNum",zhifubaoNum+1);
                }else{
                    if(rentIndemnityMap.get("zhifubaoPrice")==null){
                        rentIndemnityMap.put("zhifubaoPrice",zhifubaoPrice);
                        rentIndemnityMap.put("zhifubaoNum",zhifubaoNum);
                    }
                }*/
                //线下微信 4
                /*double weixinPrice = MapUtils.getDoubleValue(rentIndemnityMap, "weixinPrice", 0);
                Integer weixinNum = MapUtils.getInteger(rentIndemnityMap, "weixinNum", 0);
                if("4".equals(String.valueOf(depositIndemnity.getPayType()))){
                    rentIndemnityMap.put("weixinPrice",depositIndemnity.getIndemnity().doubleValue() + weixinPrice);
                    rentIndemnityMap.put("weixinNum",weixinNum+1);
                }else{
                    if(rentIndemnityMap.get("weixinPrice")==null){
                        rentIndemnityMap.put("weixinPrice",weixinPrice);
                        rentIndemnityMap.put("weixinNum",weixinNum);
                    }
                }*/
                //线下银行卡 3
                /*double yinhangkaPrice = MapUtils.getDoubleValue(rentIndemnityMap, "yinhangkaPrice", 0);
                Integer yinhangkaNum = MapUtils.getInteger(rentIndemnityMap, "yinhangkaNum", 0);
                if("3".equals(String.valueOf(depositIndemnity.getPayType()))){
                    rentIndemnityMap.put("yinhangkaPrice",depositIndemnity.getIndemnity().doubleValue() + yinhangkaPrice);
                    rentIndemnityMap.put("yinhangkaNum",yinhangkaNum+1);
                }else{
                    if(rentIndemnityMap.get("yinhangkaPrice")==null){
                        rentIndemnityMap.put("yinhangkaPrice",yinhangkaPrice);
                        rentIndemnityMap.put("yinhangkaNum",yinhangkaNum);
                    }
                }*/
                //线下招待 10
                /*double zhaodaiPrice = MapUtils.getDoubleValue(rentIndemnityMap, "zhaodaiPrice", 0);
                Integer zhaodaiNum = MapUtils.getInteger(rentIndemnityMap, "zhaodaiNum", 0);
                if("10".equals(String.valueOf(depositIndemnity.getPayType()))){
                    rentIndemnityMap.put("zhaodaiPrice",depositIndemnity.getIndemnity().doubleValue() + zhaodaiPrice);
                    rentIndemnityMap.put("zhaodaiNum",zhaodaiNum+1);
                }else{
                    if(rentIndemnityMap.get("zhaodaiPrice")==null){
                        rentIndemnityMap.put("zhaodaiPrice",zhaodaiPrice);
                        rentIndemnityMap.put("zhaodaiNum",zhaodaiNum);
                    }
                }*/
                //线下挂账 9
                /*double guazhangPrice = MapUtils.getDoubleValue(rentIndemnityMap, "guazhangPrice", 0);
                Integer guazhangNum = MapUtils.getInteger(rentIndemnityMap, "guazhangNum", 0);
                if("9".equals(String.valueOf(depositIndemnity.getPayType()))){
                    rentIndemnityMap.put("guazhangPrice",depositIndemnity.getIndemnity().doubleValue() + guazhangPrice);
                    rentIndemnityMap.put("guazhangNum",guazhangNum+1);
                }else{
                    if(rentIndemnityMap.get("guazhangPrice")==null){
                        rentIndemnityMap.put("guazhangPrice",guazhangPrice);
                        rentIndemnityMap.put("guazhangNum",guazhangNum);
                    }
                }*/
                //线上储值支付 7

                //线下储值支付 8
                /*double chuzhiPrice = MapUtils.getDoubleValue(rentIndemnityMap, "chuzhiPrice", 0);
                Integer chuzhiNum = MapUtils.getInteger(rentIndemnityMap, "chuzhiNum", 0);
                if("8".equals(String.valueOf(depositIndemnity.getPayType()))){
                    rentIndemnityMap.put("chuzhiPrice",depositIndemnity.getIndemnity().doubleValue() + chuzhiPrice);
                    rentIndemnityMap.put("chuzhiNum",chuzhiNum+1);
                }else{
                    if(rentIndemnityMap.get("chuzhiPrice")==null){
                        rentIndemnityMap.put("chuzhiPrice",chuzhiPrice);
                        rentIndemnityMap.put("chuzhiNum",chuzhiNum);
                    }
                }*/
            }
        }
        //异常退款订单汇总
        Set<String> abnormalPayNameSet = new HashSet<String>();
        Map<String,Map<String,Map<String,Map<String,Object>>>> abnormalPayStasticsList = new HashMap<String,Map<String,Map<String,Map<String,Object>>>>();
        List<Orders> abnormalOrderslist = ordersService.getAbnormalOrdersByCashier(startTime, endTime, null, null, null, null, organizer.getOrganizerId(),null);
        for (Orders orders : abnormalOrderslist) {
            if(orders.getExtAbnormalRefundMoney()!=null){
                Integer pId = orders.getActivityTypeId();
                if(orders.getEquipmentId()!=null){
                    pId = new Integer(orders.getEquipmentId());
                }
                Map<String, Object> p = refundProduct.get(pId);
                if(p == null){
                    p = new HashMap<String,Object>();
                    refundProduct.put(pId,p);
                    p.put("money",BigDecimal.ZERO);
                    if(orders.getEquipmentId()==null){
                        p.put("name",orders.getActivityName());
                    }else{
                        p.put("name",orders.getEquipmentName());
                    }
                }
                int num = MapUtils.getIntValue(p,"num",0);
                p.put("num",num+orders.getChargeNum());
                BigDecimal money = (BigDecimal)p.get("money");
                p.put("money",money.add(orders.getExtAbnormalRefundMoney()).setScale(2,RoundingMode.HALF_UP));

                if(orders.getExtAbnormalRefundMoney()!=null){
                    totalRefundPrice = totalRefundPrice.add(orders.getExtAbnormalRefundMoney()).setScale(2,RoundingMode.HALF_UP);
                }

                //按支付方式统计
                int payTypeId = orders.getPayTypeId();
                Integer extActivityTypeId = orders.getActivityTypeId();
                abnormalPayNameSet.add(FinanceExcelUtil.getPayName(payTypeId));
                Map<String,Map<String, Map<String,Object>>> abnormalActivityTypeMap = abnormalPayStasticsList.get(FinanceExcelUtil.getActivityTypeIdName(extActivityTypeId));
                if(abnormalActivityTypeMap == null){
                    abnormalActivityTypeMap = new HashMap<String,Map<String, Map<String,Object>>>();
                    abnormalPayStasticsList.put(FinanceExcelUtil.getActivityTypeIdName(extActivityTypeId),abnormalActivityTypeMap);
                }
                Map<String, Map<String,Object>> abnormalPayMap = abnormalActivityTypeMap.get(orders.getActivityName());
                if(abnormalPayMap == null){
                    abnormalPayMap = new HashMap<String, Map<String,Object>>();
                    abnormalActivityTypeMap.put(orders.getActivityName(),abnormalPayMap);
                }
                Map<String, Object> stringObjectMap = abnormalPayMap.get(FinanceExcelUtil.getPayName(payTypeId));
                if(stringObjectMap == null){
                    stringObjectMap = new HashMap<String,Object>();
                    abnormalPayMap.put(FinanceExcelUtil.getPayName(payTypeId),stringObjectMap);
                    stringObjectMap.put("money",BigDecimal.ZERO);
                    if(orders.getEquipmentId()==null){
                        stringObjectMap.put("name",orders.getActivityName());
                    }else{
                        stringObjectMap.put("name",orders.getEquipmentName());
                    }
                }
                int numPay = MapUtils.getIntValue(stringObjectMap,"num",0);
                stringObjectMap.put("num",numPay+orders.getChargeNum());
                BigDecimal moneyPay = (BigDecimal)stringObjectMap.get("money");
                stringObjectMap.put("money",moneyPay.add(orders.getExtAbnormalRefundMoney()).setScale(2,RoundingMode.HALF_UP));

            }else{
                continue;
            }
        }
        //支付方式汇总 payTypeStastic

        //押金支付方式汇总
        List<Map<String, Object>> stasticsDepositRefundOrders = ordersService.getStasticsDepositRefundByTime(startTime, endTime, organizer.getOrganizerId(),null);
        for (int i = 0; i < stasticsDepositRefundOrders.size(); i++) {
            Map<String, Object> map=stasticsDepositRefundOrders.get(i);
            Integer payTypeId=MapUtils.getInteger(map,"pay_type_id",0);
            Map<String,BigDecimal> depositStasticMap = depositStastic.get(payTypeId);
            if(depositStasticMap==null){
                depositStasticMap=new HashMap<String,BigDecimal>();
                depositStasticMap.put("1",new BigDecimal(MapUtils.getDouble(map,"totalMoney",0.00)).setScale(2,RoundingMode.HALF_UP));
                depositStastic.put(payTypeId,depositStasticMap);
            }else{
                BigDecimal totalPrice=depositStasticMap.get("1")==null?BigDecimal.ZERO:depositStasticMap.get("1");
                totalPrice=totalPrice.add(new BigDecimal(MapUtils.getDouble(map,"totalMoney",0.00))).setScale(2,RoundingMode.HALF_UP);
                depositStasticMap.put("1",totalPrice);
                depositStastic.put(payTypeId,depositStasticMap);
            }
            totalOutDeposit = totalOutDeposit.add(new BigDecimal(MapUtils.getDouble(map,"totalMoney",0.00))).setScale(2,RoundingMode.HALF_UP);

            //支付方式支出统计
            Map<String,BigDecimal> payTypeMap = payTypeStastic.get(payTypeId);
            if(payTypeMap==null){
                payTypeMap=new HashMap<String,BigDecimal>();
                payTypeMap.put("1",new BigDecimal(MapUtils.getDouble(map,"totalMoney",0.00)).setScale(2,RoundingMode.HALF_UP));
                payTypeStastic.put(payTypeId,payTypeMap);
            }else{
                BigDecimal totalPrice=payTypeMap.get("1")==null?BigDecimal.ZERO:payTypeMap.get("1");
                totalPrice=totalPrice.add(new BigDecimal(MapUtils.getDouble(map,"totalMoney",0.00))).setScale(2,RoundingMode.HALF_UP);
                payTypeMap.put("1",totalPrice);
                payTypeStastic.put(payTypeId,payTypeMap);
            }
        }

        //招待
        List<Map> entertainList = zooEntertainOrdersService.entertainStastic(startTime, endTime, organizer.getOrganizerId(), null);

        //挂账
        List<Map> hungUpList = zooOnAccountOrdersService.hungUpStastic(startTime, endTime, organizer.getOrganizerId(), null);


        //教练押金支出
//        List<Map<String, Object>> coachDepositRefundOrders = ordersService.getCoachDepositRefundsByTime(startTime, endTime, organizer.getOrganizerId());
//        if(coachDepositRefundOrders!=null&&coachDepositRefundOrders.size()>0){
//            for (int i = 0; i < coachDepositRefundOrders.size(); i++) {
//                Map<String, Object> map=coachDepositRefundOrders.get(i);
//                Integer payTypeId=MapUtils.getInteger(map,"pay_type_id",0);
//                Map<String,BigDecimal> coachPayTypeMap = coachPayTypeStastic.get(payTypeId);
//                if(coachPayTypeMap==null){
//                    coachPayTypeMap=new HashMap<String,BigDecimal>();
//                    coachPayTypeMap.put("1",new BigDecimal(MapUtils.getDouble(map,"totalMoney",0.00)).setScale(2,RoundingMode.HALF_UP));
//                    coachPayTypeStastic.put(payTypeId,coachPayTypeMap);
//                }else{
//                    BigDecimal totalPrice=coachPayTypeMap.get("1")==null?BigDecimal.ZERO:coachPayTypeMap.get("1");
//                    totalPrice=totalPrice.add(new BigDecimal(MapUtils.getDouble(map,"totalMoney",0.00))).setScale(2,RoundingMode.HALF_UP);
//                    coachPayTypeMap.put("1",totalPrice);
//                    coachPayTypeStastic.put(payTypeId,coachPayTypeMap);
//                }
//            }
//        }

        //汇总

        Map<String,Object> map1=new HashMap<String,Object>();
        map1.put("money",totalOrderPrice.subtract(totalInDeposit).subtract(totalStoragePayPrice).setScale(2,RoundingMode.HALF_UP));
        map1.put("name","收入金额");
        allStastics.add(map1);
        Map<String,Object> map2=new HashMap<String,Object>();
        map2.put("money",totalRefundPrice);
        map2.put("name","异常退款金额");
        allStastics.add(map2);
        Map<String,Object> map3=new HashMap<String,Object>();
        map3.put("money",totalOrderPrice.subtract(totalInDeposit).subtract(totalRefundPrice).setScale(2,RoundingMode.HALF_UP));
        map3.put("name","应收金额");
        allStastics.add(map3);
        Map<String,Object> map4=new HashMap<String,Object>();
        map4.put("money",totalIndemnityPrice);
        map4.put("name","赔偿金额");
        allStastics.add(map4);
        Map<String,Object> map5=new HashMap<String,Object>();
        map5.put("money",totalInDeposit.subtract(totalOutDeposit).setScale(2,RoundingMode.HALF_UP));
        map5.put("name","押金合计");
        allStastics.add(map5);
        Map<String,Object> map6=new HashMap<String,Object>();
        map6.put("money",totalOrderPrice.subtract(totalInDeposit).subtract(totalRefundPrice).subtract(totalStoragePayPrice).setScale(2,RoundingMode.HALF_UP).add(totalInDeposit.subtract(totalOutDeposit).setScale(2,RoundingMode.HALF_UP)));
        map6.put("name","实收金额");
        allStastics.add(map6);

        HSSFWorkbook wb = new HSSFWorkbook();
        createStasticsSheet(startTime,endTime,totalStoragePayPrice,saleProductStastic,rentOrders,
                rentTimeOutOrders,addServiceOrders,distributorOrders,indemnityProduct,
                refundProduct,payTypeStastic,depositStastic,allStastics,wb,entertainList,hungUpList,abnormalPayStasticsList,abnormalPayNameSet);

        //产品统计
        createProductStasticSheet(wb,saleProductStastic);

        //租赁物统计
        createRenStasticSheet(wb,rentAndTimeOutOrders,indemnityProduct,rentListMap,rentIndemnityListMap);
        //增值服务
        createAddServiceStasticSheet(wb,addServiceListMap);

        //indemnityProduct
        //挂账汇总sheet
        createHungStasticsSheet(wb,startTime,endTime,organizer,hungUpList);

        //招待汇总sheet
        createEntertainStasticSheet(wb,startTime,endTime,organizer,entertainList);

        //教练sheet，coachCourseListMap
        createCoachStasticSheet(wb,coachCourseListMap,coachCardListMap,coachVisitListMap,coachPayTypeStastic,coachPayListMap,payNameSet);

        wb.write(out);
    }

    /**
     * 教练支付数据组装
     * @param orders
     * @param coachPayMap
     */
    private void payCoachInitMap(Orders orders,Map<String, Map<String, Map<String, Map<String, Object>>>> coachPayMap){

        //场次
        Map<String, Map<String, Map<String, Object>>> episodeNamePayMap = coachPayMap.get(orders.getActivityName());
        if(episodeNamePayMap==null){
            episodeNamePayMap = new HashMap<String, Map<String, Map<String, Object>>>();
            coachPayMap.put(orders.getActivityName(),episodeNamePayMap);
        }

        //票券
        Map<String, Map<String, Object>> chargePayMap = episodeNamePayMap.get(orders.getChargeName());
        if(chargePayMap==null){
            chargePayMap = new HashMap<String, Map<String, Object>>();
            episodeNamePayMap.put(orders.getChargeName(),chargePayMap);
        }

        //支付方式
        Map<String, Object> payMap = chargePayMap.get(FinanceExcelUtil.getPayName(orders.getPayTypeId()));
        if(payMap==null){
            payMap = new HashMap<String, Object>();
            chargePayMap.put(FinanceExcelUtil.getPayName(orders.getPayTypeId()),payMap);
        }

        int payNum = MapUtils.getIntValue(payMap, "num", 0);
        payMap.put("num" , payNum + orders.getChargeNum());
        double payPrice = MapUtils.getDoubleValue(payMap, "price", 0);
        payMap.put("price",new BigDecimal(payPrice + orders.getChargeNum()*orders.getChargePrice().doubleValue()).setScale(2,RoundingMode.HALF_UP));

    }

    /**
     * 产品统计
     * @param wb
     * @param saleProductStastic
     * @return
     */
    private HSSFSheet createProductStasticSheet(HSSFWorkbook wb,Map<Integer,Map<String,Map<String,Object>>> saleProductStastic) {
        HSSFSheet sheet = wb.createSheet("产品统计");
        List<String> headList = new ArrayList<String>();
        headList.add("产品类型");
        headList.add("产品名称");
        ActivityBeanUtils.OfflinePayNameEnum[] offlinePayNameEnums = ActivityBeanUtils.OfflinePayNameEnum.values();
        for (ActivityBeanUtils.OfflinePayNameEnum payEnum:offlinePayNameEnums){
            headList.add(payEnum.getName());
            headList.add("核销数量");
        }
        headList.add("金额合计");
        headList.add("数量合计");

        String[] head = headList.toArray(new String[headList.size()]);

        //String[] head = {"产品类型", "产品名称", "线下现金", "核销数量", "线下支付宝", "核销数量", "线下微信","核销数量", "线下银行卡", "核销数量", "线下招待", "核销数量", "线下挂账", "核销数量", "线下储值支付", "核销数量", "金额合计", "数量合计"};

        createProductPayStaticTable(0,wb,sheet,saleProductStastic,head);
        return sheet;
    }
    /**
     * 租赁物统计
     * @param wb
     * @param rentAndTimeOutOrders
     * @return
     */
    private HSSFSheet createRenStasticSheet(HSSFWorkbook wb, Map<String,Map<String, Object>> rentAndTimeOutOrders,Map<Integer,Map<String,Object>> indemnityProduct,Map<String,Map<String, Object>> rentListMap,Map<String,Map<String, Object>> rentIndemnityListMap) {
        HSSFSheet sheet = wb.createSheet("租赁物统计");
        String[] head = {"租赁物名称", "核销数量", "收入金额", "超时数量", "超时金额", "应收金额"};
        String[] indemnityHead = {"租赁物名称", "赔偿数量", "赔偿金额", "应收金额"};
        //String[] headList = {"租赁物名称", "线下现金", "核销数量", "线下支付宝", "核销数量", "线下微信","核销数量", "线下银行卡", "核销数量", "线下招待", "核销数量", "线下挂账", "核销数量", "线下储值支付", "核销数量", "金额合计", "数量合计"};

        List<String> rentPayheadList = new ArrayList<String>();
        List<String> indemnityPayheadList = new ArrayList<String>();
        indemnityPayheadList.add("租赁物名称");

        rentPayheadList.add("租赁物名称");
        ActivityBeanUtils.OfflinePayNameEnum[] offlinePayNameEnums = ActivityBeanUtils.OfflinePayNameEnum.values();
        for (ActivityBeanUtils.OfflinePayNameEnum payEnum:offlinePayNameEnums){
            //租赁物
            rentPayheadList.add(payEnum.getName());
            rentPayheadList.add("核销数量");

            //赔偿
            indemnityPayheadList.add(payEnum.getName());
            indemnityPayheadList.add("核销数量");
        }
        rentPayheadList.add("金额合计");
        rentPayheadList.add("数量合计");
        //赔偿
        indemnityPayheadList.add("金额合计");
        indemnityPayheadList.add("数量合计");

        String[] headList = rentPayheadList.toArray(new String[rentPayheadList.size()]);
        String[] indemnityHeadList = rentPayheadList.toArray(new String[indemnityPayheadList.size()]);

        //String[] indemnityHeadList = {"租赁物名称","线下现金", "核销数量", "线下支付宝", "核销数量", "线下微信","核销数量", "线下银行卡", "核销数量", "线下招待", "核销数量", "线下挂账", "核销数量", "线下储值支付", "核销数量", "金额合计", "数量合计"};

        int rowNum=0;
        rowNum=createRentStaticTable(rowNum,wb,sheet,rentAndTimeOutOrders,head);
        rowNum++;
        rowNum=createRentIndemnityStaticTable(rowNum,wb,sheet,indemnityProduct,indemnityHead);
        rowNum++;
        rowNum++;
        rowNum=createRentPayStaticTable(rowNum,wb,sheet,rentListMap,headList);
        rowNum++;
        rowNum=createRentIndemnityPayStaticTable(rowNum,wb,sheet,rentIndemnityListMap,indemnityHeadList);
        return sheet;
    }

    /**
     * 增值服务统计
     * @param wb
     * @return
     */
    private HSSFSheet createAddServiceStasticSheet(HSSFWorkbook wb,Map<String,Map<String, Object>> addServiceListMap) {
        HSSFSheet sheet = wb.createSheet("增值服务统计");
        String[] addServiceHead = {"增值服务", "核销数量", "收入金额"};

        List<String> headList = new ArrayList<String>();
        headList.add("增值服务");
        ActivityBeanUtils.OfflinePayNameEnum[] offlinePayNameEnums = ActivityBeanUtils.OfflinePayNameEnum.values();
        for (ActivityBeanUtils.OfflinePayNameEnum payEnum:offlinePayNameEnums){
            //租赁物
            headList.add(payEnum.getName());
            headList.add("核销数量");
        }
        headList.add("金额合计");
        headList.add("数量合计");

        String[] addServiceHeadList = headList.toArray(new String[headList.size()]);

        //String[] addServiceHeadList = {"增值服务","线下现金", "核销数量", "线下支付宝", "核销数量", "线下微信","核销数量", "线下银行卡", "核销数量", "线下招待", "核销数量", "线下挂账", "核销数量", "线下储值支付", "核销数量", "金额合计", "数量合计"};

        int rowNum=0;
        rowNum++;
        rowNum=createAddServiceStaticTable(rowNum,wb,sheet,addServiceListMap,addServiceHead);
        rowNum++;
        rowNum++;
        rowNum=createAddServicePayStaticTable(rowNum,wb,sheet,addServiceListMap,addServiceHeadList);
        return sheet;
    }
    /**
     * 教练课程统计
     * @param wb
     * @return
     */
    private HSSFSheet createCoachStasticSheet(HSSFWorkbook wb,Map<String,Map<String,Map<String,Map<String,Object>>>> coachCourseListMap,Map<String,Map<String,Map<String,Map<String,Object>>>> coachCardListMap,Map<String,Map<String,Map<String,Map<String,Object>>>> coachVisitListMap,Map<Integer
            ,Map<String,BigDecimal>> coachPayTypeStastic
            ,Map<String,Map<String,Map<String,Map<String,Map<String,Object>>>>> coachPayListMap
            ,Set<String> payNameSet) {
        HSSFSheet sheet = wb.createSheet("教练课程统计");
        String[] head = {"活动名称", "场次名称", "票券名称", "数量","金额"};

        int rowNum = 1;

        HSSFCellStyle headStyle = wb.createCellStyle(); // 样式对象
        HSSFFont font = wb.createFont();// 生成一个字体
        font.setColor(HSSFColor.BLACK.index);// HSSFColor.VIOLET.index // 字体颜色
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);// 字体增粗
        font.setFontHeightInPoints((short)16);
        headStyle.setFont(font);// 把字体应用到当前的样式
        headStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        headStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);//垂直居中
        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = row.createCell(1);
        cell.setCellStyle(headStyle);
        cell.setCellValue("教练课程统计");

        int[] startIndex = new int[0];

        rowNum++;
        FinanceExcelUtil.createExcellHead(wb,sheet,rowNum,head);
        rowNum++;
        if(coachCourseListMap!=null && coachCourseListMap.size()>0){
            //教练课程
            rowNum = FinanceExcelUtil.createCoachItem(wb,sheet,rowNum,coachCourseListMap);
            rowNum++;
            startIndex =ArrayUtils.add(startIndex,rowNum);
        }
        if(coachCardListMap!=null && coachCardListMap.size()>0){
            //教练课时卡
            rowNum = FinanceExcelUtil.createCoachItem(wb,sheet,rowNum,coachCardListMap);
            rowNum++;
            startIndex =ArrayUtils.add(startIndex,rowNum);
        }
        if(coachVisitListMap!=null && coachVisitListMap.size()>0){
            //教练预约
            rowNum = FinanceExcelUtil.createCoachItem(wb,sheet,rowNum,coachVisitListMap);
            rowNum++;
            startIndex =ArrayUtils.add(startIndex,rowNum);
        }

        /*if(coachPayTypeStastic!=null){
            FinanceExcelUtil.createPayTypeHead(wb,sheet,rowNum);
            rowNum++;
            //支付方式
            rowNum = FinanceExcelUtil.createPayTypeStasticItem(wb,sheet,rowNum,coachPayTypeStastic);
            rowNum++;
        }
        rowNum++;*/

        if(coachPayListMap!=null){
            row = sheet.createRow(rowNum);
            cell = row.createCell(1);
            cell.setCellStyle(headStyle);
            cell.setCellValue("教练支付方式汇总");
            rowNum++;
            rowNum = FinanceExcelUtil.createPayCoachItem(wb,sheet,rowNum,coachPayListMap,payNameSet);
            rowNum++;
        }


        return sheet;
    }
    /**
     * 创建表格
     * @param wb
     * @param sheet
     * @param head
     */
    private void createProductPayStaticTable(int rowNum,HSSFWorkbook wb,HSSFSheet sheet,Map<Integer,Map<String,Map<String,Object>>> saleProductStastic,String[] head){
        rowNum++;
        HSSFCellStyle headStyle = wb.createCellStyle(); // 样式对象
        HSSFFont font = wb.createFont();// 生成一个字体
        font.setColor(HSSFColor.BLACK.index);// HSSFColor.VIOLET.index // 字体颜色
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);// 字体增粗
        font.setFontHeightInPoints((short)16);
        headStyle.setFont(font);// 把字体应用到当前的样式
        headStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        headStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);//垂直居中
        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = row.createCell(1);
        cell.setCellStyle(headStyle);
        cell.setCellValue("产品支付方式统计");
        rowNum++;
        FinanceExcelUtil.createExcellHead(wb,sheet,rowNum,head);
        rowNum++;
        createProdctPayStaticItem(wb,sheet,rowNum,saleProductStastic);
    }
    /**
     * 创建教练课程表格
     * @param wb
     * @param sheet
     * @param head
     */
    private void createCoachStaticTable(int rowNum,HSSFWorkbook wb,HSSFSheet sheet,Map<Integer,Map<String,Map<String,Object>>> saleProductStastic,String[] head){
        rowNum++;
        HSSFCellStyle headStyle = wb.createCellStyle(); // 样式对象
        HSSFFont font = wb.createFont();// 生成一个字体
        font.setColor(HSSFColor.BLACK.index);// HSSFColor.VIOLET.index // 字体颜色
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);// 字体增粗
        font.setFontHeightInPoints((short)16);
        headStyle.setFont(font);// 把字体应用到当前的样式
        headStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        headStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);//垂直居中
        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = row.createCell(1);
        cell.setCellStyle(headStyle);
        cell.setCellValue("产品支付方式统计");
        rowNum++;
        FinanceExcelUtil.createExcellHead(wb,sheet,rowNum,head);
        rowNum++;
        createProdctPayStaticItem(wb,sheet,rowNum,saleProductStastic);
    }
    /**
     * 创建产品项
     * @param wb
     * @param sheet
     * @param rowNum
     * @param saleProductStastic
     * @return
     */
    private int createProdctPayStaticItem(HSSFWorkbook wb,HSSFSheet sheet,int rowNum,Map<Integer,Map<String,Map<String,Object>>> saleProductStastic){
        int rowStartIndex = rowNum;

        HSSFCellStyle headStyle = wb.createCellStyle(); // 样式对象
        headStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        headStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        headStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        headStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        headStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
        headStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);//垂直居中

        Iterator<Integer> iterator = saleProductStastic.keySet().iterator();
        while (iterator.hasNext()){
            Integer activityTypeId = iterator.next();
            Map<String, Map<String, Object>> stringMapMap = saleProductStastic.get(activityTypeId);

            int rowMergedNum = rowNum;
            Iterator<Map.Entry<String, Map<String, Object>>> pIter = stringMapMap.entrySet().iterator();
            while (pIter.hasNext()){
                HSSFRow row = sheet.createRow(rowNum);
                HSSFCell cell = row.createCell( 1);
                cell.setCellValue(FinanceExcelUtil.getActivityTypeIdName(activityTypeId));
                cell.setCellStyle(headStyle);

                Map.Entry<String, Map<String, Object>> next = pIter.next();
                Map<String, Object> value = next.getValue();
                String activityName = next.getKey();

                cell = row.createCell(2);
                cell.setCellValue(activityName);
                cell.setCellStyle(headStyle);

                int index = 2;
                ActivityBeanUtils.OfflinePayNameEnum[] offlinePayNameEnums = ActivityBeanUtils.OfflinePayNameEnum.values();
                for (ActivityBeanUtils.OfflinePayNameEnum payEnum:offlinePayNameEnums){
                    index++;
                    cell = row.createCell(index);
                    cell.setCellValue(new BigDecimal(MapUtils.getDoubleValue(value,payEnum.getValue() + "_price",0)).setScale(2,RoundingMode.HALF_UP).doubleValue());
                    cell.setCellStyle(headStyle);
                    index++;
                    cell = row.createCell(index);
                    cell.setCellValue(MapUtils.getIntValue(value,payEnum.getValue() + "_num",0));
                    cell.setCellStyle(headStyle);
                }

                /*cell = row.createCell(3);
                cell.setCellValue(new BigDecimal(MapUtils.getDoubleValue(value,"xianjinPrice",0)).setScale(2,RoundingMode.HALF_UP).doubleValue());
                cell.setCellStyle(headStyle);
                cell = row.createCell(4);
                cell.setCellValue(MapUtils.getIntValue(value,"xianjinNum",0));
                cell.setCellStyle(headStyle);
                cell = row.createCell(5);
                cell.setCellValue(new BigDecimal(MapUtils.getDoubleValue(value,"zhifubaoPrice",0)).setScale(2,RoundingMode.HALF_UP).doubleValue());
                cell.setCellStyle(headStyle);
                cell = row.createCell(6);
                cell.setCellValue(MapUtils.getIntValue(value,"zhifubaoNum",0));
                cell.setCellStyle(headStyle);
                cell = row.createCell(7);
                cell.setCellValue(new BigDecimal(MapUtils.getDoubleValue(value,"weixinPrice",0)).setScale(2,RoundingMode.HALF_UP).doubleValue());
                cell.setCellStyle(headStyle);
                cell = row.createCell(8);
                cell.setCellValue(MapUtils.getIntValue(value,"weixinNum",0));
                cell.setCellStyle(headStyle);
                cell = row.createCell(9);
                cell.setCellValue(new BigDecimal(MapUtils.getDoubleValue(value,"yinhangkaPrice",0)).setScale(2,RoundingMode.HALF_UP).doubleValue());
                cell.setCellStyle(headStyle);
                cell = row.createCell(10);
                cell.setCellValue(MapUtils.getIntValue(value,"yinhangkaNum",0));
                cell.setCellStyle(headStyle);
                cell = row.createCell(11);
                cell.setCellValue(new BigDecimal(MapUtils.getDoubleValue(value,"zhaodaiPrice",0)).setScale(2,RoundingMode.HALF_UP).doubleValue());
                cell.setCellStyle(headStyle);
                cell = row.createCell(12);
                cell.setCellValue(MapUtils.getIntValue(value,"zhaodaiNum",0));
                cell.setCellStyle(headStyle);
                cell = row.createCell(13);
                cell.setCellValue(new BigDecimal(MapUtils.getDoubleValue(value,"guazhangPrice",0)).setScale(2,RoundingMode.HALF_UP).doubleValue());
                cell.setCellStyle(headStyle);
                cell = row.createCell(14);
                cell.setCellValue(MapUtils.getIntValue(value,"guazhangNum",0));
                cell.setCellStyle(headStyle);
                cell = row.createCell(15);
                cell.setCellValue(new BigDecimal(MapUtils.getDoubleValue(value,"chuzhiPrice",0)).setScale(2,RoundingMode.HALF_UP).doubleValue());
                cell.setCellStyle(headStyle);
                cell = row.createCell(16);
                cell.setCellValue(MapUtils.getIntValue(value,"chuzhiNum",0));
                cell.setCellStyle(headStyle);*/
                index++;
                cell = row.createCell(index);
                cell.setCellValue(new BigDecimal(MapUtils.getDoubleValue(value,"price",0)).setScale(2,RoundingMode.HALF_UP).doubleValue());
                cell.setCellStyle(headStyle);
                index++;
                cell = row.createCell(index);
                cell.setCellValue(MapUtils.getIntValue(value,"num",0));
                cell.setCellStyle(headStyle);
                rowNum++;
            }
            if(stringMapMap.size()>0){
                sheet.addMergedRegion(new CellRangeAddress( rowMergedNum,rowMergedNum + stringMapMap.size()-1,1, 1));
            }
        }

        HSSFCellStyle hjStyle = wb.createCellStyle(); // 样式对象
        HSSFDataFormat df = wb.createDataFormat();
        hjStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        hjStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        hjStyle.setFillForegroundColor(HSSFColor.GREY_40_PERCENT.index);
        hjStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        hjStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        hjStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        hjStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);

        HSSFCellStyle numStyle = wb.createCellStyle(); // 样式对象
        numStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        numStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        numStyle.setFillForegroundColor(HSSFColor.GREY_40_PERCENT.index);
        numStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        numStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        numStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        numStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);


        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = row.createCell(1);
        cell.setCellValue("合计");
        cell.setCellStyle(hjStyle);
        cell = row.createCell(2);
        cell.setCellValue("");
        cell.setCellStyle(hjStyle);

        hjStyle.setDataFormat(df.getBuiltinFormat("0.00"));

        int index = 2;
        ActivityBeanUtils.OfflinePayNameEnum[] offlinePayNameEnums = ActivityBeanUtils.OfflinePayNameEnum.values();
        for (ActivityBeanUtils.OfflinePayNameEnum payEnum:offlinePayNameEnums){
            index++;
            cell = row.createCell(index);
            String sc = CellReference.convertNumToColString(index);
            cell.setCellFormula("SUM(" + sc + rowStartIndex + ":"+sc+rowNum+")");
            cell.setCellStyle(hjStyle);
            index++;
            cell = row.createCell( index);
            String pc = CellReference.convertNumToColString(index);
            cell.setCellFormula("SUM(" + pc + rowStartIndex + ":"+pc+rowNum+")");
            cell.setCellStyle(numStyle);
        }
        /*cell = row.createCell(3);
        String sc = CellReference.convertNumToColString(3);
        cell.setCellFormula("SUM(" + sc + rowStartIndex + ":"+sc+rowNum+")");
        cell.setCellStyle(hjStyle);
        cell = row.createCell( 4);
        String pc = CellReference.convertNumToColString(4);
        cell.setCellFormula("SUM(" + pc + rowStartIndex + ":"+pc+rowNum+")");
        cell.setCellStyle(numStyle);
        cell = row.createCell( 5);
        String pc5 = CellReference.convertNumToColString(5);
        cell.setCellFormula("SUM(" + pc5 + rowStartIndex + ":"+pc5+rowNum+")");
        cell.setCellStyle(hjStyle);
        cell = row.createCell( 6);
        String pc6 = CellReference.convertNumToColString(6);
        cell.setCellFormula("SUM(" + pc6 + rowStartIndex + ":"+pc6+rowNum+")");
        cell.setCellStyle(numStyle);
        cell = row.createCell( 7);
        String pc7 = CellReference.convertNumToColString(7);
        cell.setCellFormula("SUM(" + pc7 + rowStartIndex + ":"+pc7+rowNum+")");
        cell.setCellStyle(hjStyle);
        cell = row.createCell( 8);
        String pc8 = CellReference.convertNumToColString(8);
        cell.setCellFormula("SUM(" + pc8 + rowStartIndex + ":"+pc8+rowNum+")");
        cell.setCellStyle(numStyle);
        cell = row.createCell( 9);
        String pc9 = CellReference.convertNumToColString(9);
        cell.setCellFormula("SUM(" + pc9 + rowStartIndex + ":"+pc9+rowNum+")");
        cell.setCellStyle(hjStyle);
        cell = row.createCell( 10);
        String pc10 = CellReference.convertNumToColString(10);
        cell.setCellFormula("SUM(" + pc10 + rowStartIndex + ":"+pc10+rowNum+")");
        cell.setCellStyle(numStyle);
        cell = row.createCell( 11);
        String pc11 = CellReference.convertNumToColString(11);
        cell.setCellFormula("SUM(" + pc11 + rowStartIndex + ":"+pc11+rowNum+")");
        cell.setCellStyle(hjStyle);
        cell = row.createCell( 12);
        String pc12 = CellReference.convertNumToColString(12);
        cell.setCellFormula("SUM(" + pc12 + rowStartIndex + ":"+pc12+rowNum+")");
        cell.setCellStyle(numStyle);
        cell = row.createCell( 13);
        String pc13 = CellReference.convertNumToColString(13);
        cell.setCellFormula("SUM(" + pc13 + rowStartIndex + ":"+pc13+rowNum+")");
        cell.setCellStyle(hjStyle);
        cell = row.createCell( 14);
        String pc14 = CellReference.convertNumToColString(14);
        cell.setCellFormula("SUM(" + pc14 + rowStartIndex + ":"+pc14+rowNum+")");
        cell.setCellStyle(numStyle);
        cell = row.createCell( 15);
        String pc15 = CellReference.convertNumToColString(15);
        cell.setCellFormula("SUM(" + pc15 + rowStartIndex + ":"+pc15+rowNum+")");
        cell.setCellStyle(hjStyle);
        cell = row.createCell( 16);
        String pc16 = CellReference.convertNumToColString(16);
        cell.setCellFormula("SUM(" + pc16 + rowStartIndex + ":"+pc16+rowNum+")");
        cell.setCellStyle(numStyle);*/
        index++;
        cell = row.createCell( index);
        String pc17 = CellReference.convertNumToColString(index);
        cell.setCellFormula("SUM(" + pc17 + rowStartIndex + ":"+pc17+rowNum+")");
        cell.setCellStyle(hjStyle);
        index++;
        cell = row.createCell( index);
        String pc18 = CellReference.convertNumToColString(index);
        cell.setCellFormula("SUM(" + pc18 + rowStartIndex + ":"+pc18+rowNum+")");
        cell.setCellStyle(numStyle);
        return rowNum;
    }

    /**
     * 创建租赁物表格
     * @param wb
     * @param sheet
     * @param head
     */
    private int createRentStaticTable(int rowNum,HSSFWorkbook wb,HSSFSheet sheet,Map<String,Map<String, Object>> rentAndTimeOutOrders,String[] head){
        rowNum++;
        HSSFCellStyle headStyle = wb.createCellStyle(); // 样式对象
        HSSFFont font = wb.createFont();// 生成一个字体
        font.setColor(HSSFColor.BLACK.index);// HSSFColor.VIOLET.index // 字体颜色
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);// 字体增粗
        font.setFontHeightInPoints((short)16);
        headStyle.setFont(font);// 把字体应用到当前的样式
        headStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        headStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);//垂直居中
        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = row.createCell(1);
        cell.setCellStyle(headStyle);
        cell.setCellValue("租赁物销售统计");
        rowNum++;
        FinanceExcelUtil.createExcellHead(wb,sheet,rowNum,head);
        rowNum++;
        rowNum=createRenStaticItem(wb,sheet,rowNum,rentAndTimeOutOrders);
        return rowNum;
    }

    /**
     * 创建租赁物支付方式表格
     * @param wb
     * @param sheet
     * @param head
     */
    private int createRentPayStaticTable(int rowNum,HSSFWorkbook wb,HSSFSheet sheet,Map<String,Map<String, Object>> rentListMap,String[] head){
        rowNum++;
        HSSFCellStyle headStyle = wb.createCellStyle(); // 样式对象
        HSSFFont font = wb.createFont();// 生成一个字体
        font.setColor(HSSFColor.BLACK.index);// HSSFColor.VIOLET.index // 字体颜色
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);// 字体增粗
        font.setFontHeightInPoints((short)16);
        headStyle.setFont(font);// 把字体应用到当前的样式
        headStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        headStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);//垂直居中
        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = row.createCell(1);
        cell.setCellStyle(headStyle);
        cell.setCellValue("租赁物支付方式统计");
        rowNum++;
        FinanceExcelUtil.createExcellHead(wb,sheet,rowNum,head);
        rowNum++;
        rowNum=createRentPayStaticItem(wb,sheet,rowNum,rentListMap);

        return rowNum;
    }
    /**
     * 创建租赁物赔偿支付方式表格
     * @param wb
     * @param sheet
     * @param head
     */
    private int createRentIndemnityPayStaticTable(int rowNum,HSSFWorkbook wb,HSSFSheet sheet,Map<String,Map<String, Object>> rentIndemnityListMap,String[] head){
        rowNum++;
        HSSFCellStyle headStyle = wb.createCellStyle(); // 样式对象
        HSSFFont font = wb.createFont();// 生成一个字体
        font.setColor(HSSFColor.BLACK.index);// HSSFColor.VIOLET.index // 字体颜色
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);// 字体增粗
        font.setFontHeightInPoints((short)16);
        headStyle.setFont(font);// 把字体应用到当前的样式
        headStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        headStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);//垂直居中
        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = row.createCell(1);
        cell.setCellStyle(headStyle);
        cell.setCellValue("租赁物赔偿支付方式统计");
        rowNum++;
        FinanceExcelUtil.createExcellHead(wb,sheet,rowNum,head);
        rowNum++;
        rowNum=createRentPayStaticItem(wb,sheet,rowNum,rentIndemnityListMap);
        return rowNum;
    }
    /**
     * 创建增值服务支付方式表格
     * @param wb
     * @param sheet
     * @param head
     */
    private int createAddServicePayStaticTable(int rowNum,HSSFWorkbook wb,HSSFSheet sheet,Map<String,Map<String, Object>> addServiceHeadList,String[] head){
        rowNum++;
        HSSFCellStyle headStyle = wb.createCellStyle(); // 样式对象
        HSSFFont font = wb.createFont();// 生成一个字体
        font.setColor(HSSFColor.BLACK.index);// HSSFColor.VIOLET.index // 字体颜色
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);// 字体增粗
        font.setFontHeightInPoints((short)16);
        headStyle.setFont(font);// 把字体应用到当前的样式
        headStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        headStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);//垂直居中
        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = row.createCell(1);
        cell.setCellStyle(headStyle);
        cell.setCellValue("增值服务支付方式统计");
        rowNum++;
        FinanceExcelUtil.createExcellHead(wb,sheet,rowNum,head);
        rowNum++;
        rowNum=createRentPayStaticItem(wb,sheet,rowNum,addServiceHeadList);
        return rowNum;
    }
    /**
     * 创建租赁物赔偿表格
     * @param wb
     * @param sheet
     * @param head
     */
    private int createRentIndemnityStaticTable(int rowNum,HSSFWorkbook wb,HSSFSheet sheet,Map<Integer,Map<String,Object>> indemnityProduct,String[] head){
        rowNum++;
        HSSFCellStyle headStyle = wb.createCellStyle(); // 样式对象
        HSSFFont font = wb.createFont();// 生成一个字体
        font.setColor(HSSFColor.BLACK.index);// HSSFColor.VIOLET.index // 字体颜色
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);// 字体增粗
        font.setFontHeightInPoints((short)16);
        headStyle.setFont(font);// 把字体应用到当前的样式
        headStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        headStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);//垂直居中
        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = row.createCell(1);
        cell.setCellStyle(headStyle);
        cell.setCellValue("租赁物赔偿统计");
        rowNum++;
        FinanceExcelUtil.createExcellHead(wb,sheet,rowNum,head);
        rowNum++;
        rowNum=createRenIndemnityStaticItem(wb,sheet,rowNum,indemnityProduct);
        return rowNum;
    }
    /**
     * 创建增值服务表格
     * @param wb
     * @param sheet
     * @param head
     */
    private int createAddServiceStaticTable(int rowNum,HSSFWorkbook wb,HSSFSheet sheet,Map<String,Map<String, Object>> addServiceListMap,String[] head){
        rowNum++;
        HSSFCellStyle headStyle = wb.createCellStyle(); // 样式对象
        HSSFFont font = wb.createFont();// 生成一个字体
        font.setColor(HSSFColor.BLACK.index);// HSSFColor.VIOLET.index // 字体颜色
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);// 字体增粗
        font.setFontHeightInPoints((short)16);
        headStyle.setFont(font);// 把字体应用到当前的样式
        headStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        headStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);//垂直居中
        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = row.createCell(1);
        cell.setCellStyle(headStyle);
        cell.setCellValue("增值服务销售统计");
        rowNum++;
        FinanceExcelUtil.createExcellHead(wb,sheet,rowNum,head);
        rowNum++;
        rowNum=createAddServiceStaticItem(wb,sheet,rowNum,addServiceListMap);
        return rowNum;
    }

    /**
     * 创建租赁物支付项
     * @param wb
     * @param sheet
     * @param rowNum
     * @param rentListMap
     * @return
     */
    private int createRentPayStaticItem(HSSFWorkbook wb,HSSFSheet sheet,int rowNum,Map<String,Map<String, Object>> rentListMap){
        int rowStartIndex = rowNum;

        HSSFCellStyle headStyle = wb.createCellStyle(); // 样式对象
        headStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        headStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        headStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        headStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        headStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
        headStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);//垂直居中

        ActivityBeanUtils.OfflinePayNameEnum[] offlinePayNameEnums = ActivityBeanUtils.OfflinePayNameEnum.values();

        Iterator<String> iterator = rentListMap.keySet().iterator();
        while (iterator.hasNext()){
            String name = iterator.next();
            Map<String, Object> value = rentListMap.get(name);

            HSSFRow row = sheet.createRow(rowNum);
            HSSFCell cell = row.createCell( 1);
            cell.setCellValue(name);
            cell.setCellStyle(headStyle);

            int index = 1;
            for (ActivityBeanUtils.OfflinePayNameEnum payEnum : offlinePayNameEnums){
                index++;
                cell = row.createCell(index);
                cell.setCellValue(new BigDecimal(MapUtils.getDoubleValue(value,payEnum.getValue() + "_price",0)).setScale(2,RoundingMode.HALF_UP).doubleValue());
                cell.setCellStyle(headStyle);
                index++;
                cell = row.createCell(index);
                cell.setCellValue(MapUtils.getIntValue(value,payEnum.getValue() + "_num",0));
                cell.setCellStyle(headStyle);
            }

            /*cell = row.createCell(2);
            cell.setCellValue(new BigDecimal(MapUtils.getDoubleValue(value,"xianjinPrice",0)).setScale(2,RoundingMode.HALF_UP).doubleValue());
            cell.setCellStyle(headStyle);
            cell = row.createCell(3);
            cell.setCellValue(MapUtils.getIntValue(value,"xianjinNum",0));
            cell.setCellStyle(headStyle);
            cell = row.createCell(4);
            cell.setCellValue(new BigDecimal(MapUtils.getDoubleValue(value,"zhifubaoPrice",0)).setScale(2,RoundingMode.HALF_UP).doubleValue());
            cell.setCellStyle(headStyle);
            cell = row.createCell(5);
            cell.setCellValue(MapUtils.getIntValue(value,"zhifubaoNum",0));
            cell.setCellStyle(headStyle);
            cell = row.createCell(6);
            cell.setCellValue(new BigDecimal(MapUtils.getDoubleValue(value,"weixinPrice",0)).setScale(2,RoundingMode.HALF_UP).doubleValue());
            cell.setCellStyle(headStyle);
            cell = row.createCell(7);
            cell.setCellValue(MapUtils.getIntValue(value,"weixinNum",0));
            cell.setCellStyle(headStyle);
            cell = row.createCell(8);
            cell.setCellValue(new BigDecimal(MapUtils.getDoubleValue(value,"yinhangkaPrice",0)).setScale(2,RoundingMode.HALF_UP).doubleValue());
            cell.setCellStyle(headStyle);
            cell = row.createCell(9);
            cell.setCellValue(MapUtils.getIntValue(value,"yinhangkaNum",0));
            cell.setCellStyle(headStyle);
            cell = row.createCell(10);
            cell.setCellValue(new BigDecimal(MapUtils.getDoubleValue(value,"zhaodaiPrice",0)).setScale(2,RoundingMode.HALF_UP).doubleValue());
            cell.setCellStyle(headStyle);
            cell = row.createCell(11);
            cell.setCellValue(MapUtils.getIntValue(value,"zhaodaiNum",0));
            cell.setCellStyle(headStyle);
            cell = row.createCell(12);
            cell.setCellValue(new BigDecimal(MapUtils.getDoubleValue(value,"guazhangPrice",0)).setScale(2,RoundingMode.HALF_UP).doubleValue());
            cell.setCellStyle(headStyle);
            cell = row.createCell(13);
            cell.setCellValue(MapUtils.getIntValue(value,"guazhangNum",0));
            cell.setCellStyle(headStyle);
            cell = row.createCell(14);
            cell.setCellValue(new BigDecimal(MapUtils.getDoubleValue(value,"chuzhiPrice",0)).setScale(2,RoundingMode.HALF_UP).doubleValue());
            cell.setCellStyle(headStyle);
            cell = row.createCell(15);
            cell.setCellValue(MapUtils.getIntValue(value,"chuzhiNum",0));
            cell.setCellStyle(headStyle);*/
            index++;
            cell = row.createCell(index);
            cell.setCellValue(new BigDecimal(MapUtils.getDoubleValue(value,"price",0)).setScale(2,RoundingMode.HALF_UP).doubleValue());
            cell.setCellStyle(headStyle);
            index++;
            cell = row.createCell(index);
            cell.setCellValue(MapUtils.getIntValue(value,"num",0));
            cell.setCellStyle(headStyle);
            rowNum++;
        }

        HSSFCellStyle hjStyle = wb.createCellStyle(); // 样式对象
        HSSFDataFormat df = wb.createDataFormat();
        hjStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        hjStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        hjStyle.setFillForegroundColor(HSSFColor.GREY_40_PERCENT.index);
        hjStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        hjStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        hjStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        hjStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);

        HSSFCellStyle numStyle = wb.createCellStyle(); // 样式对象
        numStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        numStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        numStyle.setFillForegroundColor(HSSFColor.GREY_40_PERCENT.index);
        numStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        numStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        numStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        numStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
        numStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);//垂直居中

        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = row.createCell(1);
        cell.setCellValue("合计");
        cell.setCellStyle(hjStyle);

        hjStyle.setDataFormat(df.getBuiltinFormat("0.00"));


        int index = 1;
        for (ActivityBeanUtils.OfflinePayNameEnum payEnum : offlinePayNameEnums){
            index++;
            cell = row.createCell(index);
            String sc = CellReference.convertNumToColString(index);
            cell.setCellFormula("SUM(" + sc + rowStartIndex + ":"+sc+rowNum+")");
            cell.setCellStyle(hjStyle);
            index++;
            cell = row.createCell( index);
            String pc = CellReference.convertNumToColString(index);
            cell.setCellFormula("SUM(" + pc + rowStartIndex + ":"+pc+rowNum+")");
            cell.setCellStyle(numStyle);
        }
        /*cell = row.createCell(2);
        String sc = CellReference.convertNumToColString(2);
        cell.setCellFormula("SUM(" + sc + rowStartIndex + ":"+sc+rowNum+")");
        cell.setCellStyle(hjStyle);
        cell = row.createCell( 3);
        String pc = CellReference.convertNumToColString(3);
        cell.setCellFormula("SUM(" + pc + rowStartIndex + ":"+pc+rowNum+")");
        cell.setCellStyle(numStyle);
        cell = row.createCell( 4);
        String pc5 = CellReference.convertNumToColString(4);
        cell.setCellFormula("SUM(" + pc5 + rowStartIndex + ":"+pc5+rowNum+")");
        cell.setCellStyle(hjStyle);
        cell = row.createCell( 5);
        String pc6 = CellReference.convertNumToColString(5);
        cell.setCellFormula("SUM(" + pc6 + rowStartIndex + ":"+pc6+rowNum+")");
        cell.setCellStyle(numStyle);
        cell = row.createCell( 6);
        String pc7 = CellReference.convertNumToColString(6);
        cell.setCellFormula("SUM(" + pc7 + rowStartIndex + ":"+pc7+rowNum+")");
        cell.setCellStyle(hjStyle);
        cell = row.createCell( 7);
        String pc8 = CellReference.convertNumToColString(7);
        cell.setCellFormula("SUM(" + pc8 + rowStartIndex + ":"+pc8+rowNum+")");
        cell.setCellStyle(numStyle);
        cell = row.createCell( 8);
        String pc9 = CellReference.convertNumToColString(8);
        cell.setCellFormula("SUM(" + pc9 + rowStartIndex + ":"+pc9+rowNum+")");
        cell.setCellStyle(hjStyle);
        cell = row.createCell( 9);
        String pc10 = CellReference.convertNumToColString(9);
        cell.setCellFormula("SUM(" + pc10 + rowStartIndex + ":"+pc10+rowNum+")");
        cell.setCellStyle(numStyle);
        cell = row.createCell( 10);
        String pc11 = CellReference.convertNumToColString(10);
        cell.setCellFormula("SUM(" + pc11 + rowStartIndex + ":"+pc11+rowNum+")");
        cell.setCellStyle(hjStyle);
        cell = row.createCell( 11);
        String pc12 = CellReference.convertNumToColString(11);
        cell.setCellFormula("SUM(" + pc12 + rowStartIndex + ":"+pc12+rowNum+")");
        cell.setCellStyle(numStyle);
        cell = row.createCell( 12);
        String pc13 = CellReference.convertNumToColString(12);
        cell.setCellFormula("SUM(" + pc13 + rowStartIndex + ":"+pc13+rowNum+")");
        cell.setCellStyle(hjStyle);
        cell = row.createCell( 13);
        String pc14 = CellReference.convertNumToColString(13);
        cell.setCellFormula("SUM(" + pc14 + rowStartIndex + ":"+pc14+rowNum+")");
        cell.setCellStyle(numStyle);
        cell = row.createCell( 14);
        String pc15 = CellReference.convertNumToColString(14);
        cell.setCellFormula("SUM(" + pc15 + rowStartIndex + ":"+pc15+rowNum+")");
        cell.setCellStyle(hjStyle);
        cell = row.createCell( 15);
        String pc16 = CellReference.convertNumToColString(15);
        cell.setCellFormula("SUM(" + pc16 + rowStartIndex + ":"+pc16+rowNum+")");
        cell.setCellStyle(numStyle);*/
        index++;
        cell = row.createCell( index);
        String pc17 = CellReference.convertNumToColString(index);
        cell.setCellFormula("SUM(" + pc17 + rowStartIndex + ":"+pc17+rowNum+")");
        cell.setCellStyle(hjStyle);
        index++;
        cell = row.createCell( index);
        String pc18 = CellReference.convertNumToColString(index);
        cell.setCellFormula("SUM(" + pc18 + rowStartIndex + ":"+pc18+rowNum+")");
        cell.setCellStyle(numStyle);
        return rowNum;
    }

    /**
     * 创建租赁物项
     * @param wb
     * @param sheet
     * @param rowNum
     * @param rentAndTimeOutOrders
     * @return
     */
    private int createRenStaticItem(HSSFWorkbook wb,HSSFSheet sheet,int rowNum,Map<String,Map<String, Object>> rentAndTimeOutOrders){
        int rowStartIndex = rowNum;

        HSSFCellStyle headStyle = wb.createCellStyle(); // 样式对象
        headStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        headStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        headStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        headStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        headStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
        headStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);//垂直居中

        Iterator<String> iterator = rentAndTimeOutOrders.keySet().iterator();
        while (iterator.hasNext()){
            String name = iterator.next();
            Map<String, Object> stringMapMap = rentAndTimeOutOrders.get(name);

            HSSFRow row = sheet.createRow(rowNum);
            HSSFCell cell = row.createCell( 1);
            cell.setCellValue(name);
            cell.setCellStyle(headStyle);

            cell = row.createCell(2);
            cell.setCellValue(MapUtils.getIntValue(stringMapMap,"totalNum",0));
            cell.setCellStyle(headStyle);
            cell = row.createCell(3);
            cell.setCellValue(new BigDecimal(MapUtils.getDoubleValue(stringMapMap,"totalMoney",0)).setScale(2,RoundingMode.HALF_UP).doubleValue());
            cell.setCellStyle(headStyle);
            cell = row.createCell(4);
            cell.setCellValue(MapUtils.getIntValue(stringMapMap,"timeOutTotalNum",0));
            cell.setCellStyle(headStyle);
            cell = row.createCell(5);
            cell.setCellValue(new BigDecimal(MapUtils.getDoubleValue(stringMapMap,"timeOutTotalMoney",0)).setScale(2,RoundingMode.HALF_UP).doubleValue());
            cell.setCellStyle(headStyle);
            cell = row.createCell(6);
            BigDecimal totalMoney=new BigDecimal(MapUtils.getDoubleValue(stringMapMap,"totalMoney",0)).setScale(2,RoundingMode.HALF_UP);
            BigDecimal timeOutTotalMoney=new BigDecimal(MapUtils.getDoubleValue(stringMapMap,"timeOutTotalMoney",0)).setScale(2,RoundingMode.HALF_UP);
            timeOutTotalMoney=timeOutTotalMoney.add(totalMoney);
            cell.setCellValue(timeOutTotalMoney.doubleValue());
            cell.setCellStyle(headStyle);
            rowNum++;
        }

        HSSFCellStyle hjStyle = wb.createCellStyle(); // 样式对象
        HSSFDataFormat df = wb.createDataFormat();
        hjStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        hjStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        hjStyle.setFillForegroundColor(HSSFColor.GREY_40_PERCENT.index);
        hjStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        hjStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        hjStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        hjStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);

        HSSFCellStyle numStyle = wb.createCellStyle(); // 样式对象
        numStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        numStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        numStyle.setFillForegroundColor(HSSFColor.GREY_40_PERCENT.index);
        numStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        numStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        numStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        numStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);


        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = row.createCell(1);
        cell.setCellValue("合计");
        cell.setCellStyle(hjStyle);

        hjStyle.setDataFormat(df.getBuiltinFormat("0.00"));
        cell = row.createCell(2);
        String sc = CellReference.convertNumToColString(2);
        cell.setCellFormula("SUM(" + sc + rowStartIndex + ":"+sc+rowNum+")");
        cell.setCellStyle(numStyle);
        cell = row.createCell( 3);
        String pc = CellReference.convertNumToColString(3);
        cell.setCellFormula("SUM(" + pc + rowStartIndex + ":"+pc+rowNum+")");
        cell.setCellStyle(hjStyle);
        cell = row.createCell( 4);
        String pc5 = CellReference.convertNumToColString(4);
        cell.setCellFormula("SUM(" + pc5 + rowStartIndex + ":"+pc5+rowNum+")");
        cell.setCellStyle(numStyle);
        cell = row.createCell( 5);
        String pc6 = CellReference.convertNumToColString(5);
        cell.setCellFormula("SUM(" + pc6 + rowStartIndex + ":"+pc6+rowNum+")");
        cell.setCellStyle(hjStyle);
        cell = row.createCell( 6);
        String pc7 = CellReference.convertNumToColString(6);
        cell.setCellFormula("SUM(" + pc7 + rowStartIndex + ":"+pc7+rowNum+")");
        cell.setCellStyle(hjStyle);
        return rowNum;
    }

    /**
     * 创建增值服务项
     * @param wb
     * @param sheet
     * @param rowNum
     * @return
     */
    private int createAddServiceStaticItem(HSSFWorkbook wb,HSSFSheet sheet,int rowNum,Map<String,Map<String, Object>> addServiceListMap){
        int rowStartIndex = rowNum;

        HSSFCellStyle headStyle = wb.createCellStyle(); // 样式对象
        headStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        headStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        headStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        headStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        headStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
        headStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);//垂直居中

        Iterator<String> iterator = addServiceListMap.keySet().iterator();
        while(iterator.hasNext()){
            String name = iterator.next();
            Map<String, Object> value = addServiceListMap.get(name);
            //sheet.addMergedRegion(new CellRangeAddress(rowStartIndex, rowNum, 1, 1));

            HSSFRow row = sheet.createRow(rowNum);
            HSSFCell cell = row.createCell(1);
            cell.setCellValue(name);
            cell.setCellStyle(headStyle);
            cell = row.createCell(2);
            cell.setCellValue(MapUtils.getIntValue(value,"num",0));
            cell.setCellStyle(headStyle);
            cell = row.createCell(3);
            cell.setCellValue(new BigDecimal(MapUtils.getDoubleValue(value,"price",0)).setScale(2,RoundingMode.HALF_UP).doubleValue());
            cell.setCellStyle(headStyle);
            rowNum++;
        }
        HSSFCellStyle hjStyle = wb.createCellStyle(); // 样式对象
        hjStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        hjStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        hjStyle.setFillForegroundColor(HSSFColor.GREY_40_PERCENT.index);
        hjStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        hjStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        hjStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        hjStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);

        HSSFCellStyle numStyle = wb.createCellStyle(); // 样式对象
        numStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        numStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        numStyle.setFillForegroundColor(HSSFColor.GREY_40_PERCENT.index);
        numStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        numStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        numStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        numStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);

        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = row.createCell(1);
        cell.setCellValue("合计");
        cell.setCellStyle(numStyle);
        cell = row.createCell(2);
        String sc1 = CellReference.convertNumToColString(3);
        cell.setCellFormula("SUM(" + sc1 + (rowStartIndex+1) + ":"+sc1+rowNum+")");
        cell.setCellStyle(numStyle);
        HSSFDataFormat df = wb.createDataFormat();
        hjStyle.setDataFormat(df.getBuiltinFormat("0.00"));
        cell = row.createCell(3);
        String sc = CellReference.convertNumToColString(3);
        cell.setCellFormula("SUM(" + sc + (rowStartIndex+1) + ":"+sc+rowNum+")");
        cell.setCellStyle(hjStyle);
        return rowNum;
    }

    /**
     * 创建租赁物赔偿项
     * @param wb
     * @param sheet
     * @param rowNum
     * @param indemnityProduct
     * @return
     */
    private int createRenIndemnityStaticItem(HSSFWorkbook wb,HSSFSheet sheet,int rowNum,Map<Integer,Map<String,Object>> indemnityProduct){
        int rowStartIndex = rowNum;

        HSSFCellStyle headStyle = wb.createCellStyle(); // 样式对象
        headStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        headStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        headStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        headStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        headStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
        headStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);//垂直居中

        Iterator<Map.Entry<Integer,Map<String,Object>>> iterator = indemnityProduct.entrySet().iterator();
        while(iterator.hasNext()){
            Map.Entry<Integer, Map<String,Object>> next = iterator.next();
            Integer key = next.getKey();
            Map<String,Object> map = (Map<String,Object>)next.getValue();
            //sheet.addMergedRegion(new CellRangeAddress(rowStartIndex, rowNum, 1, 1));

            HSSFRow row = sheet.createRow(rowNum);
            HSSFCell cell = row.createCell(1);
            cell.setCellValue(MapUtils.getString(map,"name",""));
            cell.setCellStyle(headStyle);
            cell = row.createCell(2);
            cell.setCellValue(MapUtils.getIntValue(map,"num",0));
            cell.setCellStyle(headStyle);
            cell = row.createCell(3);
            cell.setCellValue(new BigDecimal(MapUtils.getDoubleValue(map,"money",0)).setScale(2,RoundingMode.HALF_UP).doubleValue());
            cell.setCellStyle(headStyle);
            cell = row.createCell(4);
            cell.setCellValue(new BigDecimal(MapUtils.getDoubleValue(map,"money",0)).setScale(2,RoundingMode.HALF_UP).doubleValue());
            cell.setCellStyle(headStyle);
            rowNum++;
        }
        HSSFCellStyle hjStyle = wb.createCellStyle(); // 样式对象
        hjStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        hjStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        hjStyle.setFillForegroundColor(HSSFColor.GREY_40_PERCENT.index);
        hjStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        hjStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        hjStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        hjStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);

        HSSFCellStyle numStyle = wb.createCellStyle(); // 样式对象
        numStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        numStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        numStyle.setFillForegroundColor(HSSFColor.GREY_40_PERCENT.index);
        numStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        numStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        numStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        numStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);

        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = row.createCell(1);
        cell.setCellValue("合计");
        cell.setCellStyle(numStyle);
        cell = row.createCell(2);
        String sc1 = CellReference.convertNumToColString(2);
        cell.setCellFormula("SUM(" + sc1 + (rowStartIndex+1) + ":"+sc1+rowNum+")");
        cell.setCellStyle(numStyle);
        HSSFDataFormat df = wb.createDataFormat();
        hjStyle.setDataFormat(df.getBuiltinFormat("0.00"));
        cell = row.createCell(3);
        String sc = CellReference.convertNumToColString(3);
        cell.setCellFormula("SUM(" + sc + (rowStartIndex+1) + ":"+sc+rowNum+")");
        cell.setCellStyle(hjStyle);
        cell = row.createCell(4);
        String pc = CellReference.convertNumToColString(4);
        cell.setCellFormula("SUM(" + pc + (rowStartIndex+1) + ":"+pc+rowNum+")");
        cell.setCellStyle(hjStyle);
        return rowNum;
    }
    /**
     * 创建租赁物
     * @param wb
     * @param sheet
     * @param rowNum
     * @return
     */
    private int createRentItem(HSSFWorkbook wb,HSSFSheet sheet,int rowNum,List<Map<String, Object>> rentOrders,List<Map<String, Object>> rentTimeOutOrders,Map<String,Object> map){

        int rowStartIndex = rowNum;

        HSSFCellStyle headStyle = wb.createCellStyle(); // 样式对象
        headStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        headStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        headStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        headStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        headStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
        headStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);//垂直居中

        for(int i=0;i<rentOrders.size();i++){
            Map<String,Object> rentMap=rentOrders.get(i);

            HSSFRow row = sheet.createRow(rowNum);
            HSSFCell cell = row.createCell(1);
            cell.setCellValue("租赁物");
            cell.setCellStyle(headStyle);
            cell = row.createCell(2);
            cell.setCellValue(String.valueOf(rentMap.get("name")));
            cell.setCellStyle(headStyle);
            cell = row.createCell(3);
            cell.setCellValue(MapUtils.getIntValue(rentMap,"totalNum",0));
            cell.setCellStyle(headStyle);
            cell = row.createCell(4);
            cell.setCellValue(new BigDecimal(MapUtils.getDoubleValue(rentMap,"totalMoney",0)).setScale(2,RoundingMode.HALF_UP).doubleValue());
            cell.setCellStyle(headStyle);
            rowNum++;
        }
        if(rentOrders.size()>0){
            sheet.addMergedRegion(new CellRangeAddress(rowStartIndex, rowStartIndex-1+rentOrders.size(), 1, 1));
        }
        int rowStartIndext = rowNum;

        headStyle = wb.createCellStyle(); // 样式对象
        headStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        headStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        headStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        headStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        headStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
        headStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);//垂直居中

        for(int i=0;i<rentTimeOutOrders.size();i++){
            Map<String,Object> rentTimeOutMap=rentTimeOutOrders.get(i);
            HSSFRow row = sheet.createRow(rowNum);
            HSSFCell cell = row.createCell(1);
            cell.setCellValue("超时租赁");
            cell.setCellStyle(headStyle);
            cell = row.createCell(2);
            cell.setCellValue(String.valueOf(rentTimeOutMap.get("name")));
            cell.setCellStyle(headStyle);
            cell = row.createCell(3);
            cell.setCellValue(MapUtils.getIntValue(rentTimeOutMap,"totalNum",0));
            cell.setCellStyle(headStyle);
            cell = row.createCell(4);
            cell.setCellValue(new BigDecimal(MapUtils.getDoubleValue(rentTimeOutMap,"totalMoney",0)).setScale(2,RoundingMode.HALF_UP).doubleValue());
            cell.setCellStyle(headStyle);
            rowNum++;
        }
        if(rentTimeOutOrders.size()>0){
            sheet.addMergedRegion(new CellRangeAddress(rowStartIndext, rowStartIndext-1+rentOrders.size(), 1, 1));
        }

        HSSFCellStyle hjStyle = wb.createCellStyle(); // 样式对象
        hjStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        hjStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        hjStyle.setFillForegroundColor(HSSFColor.GREY_40_PERCENT.index);
        hjStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        hjStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        hjStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        hjStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);

        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = row.createCell(1);
        cell.setCellValue("合计");
        cell.setCellStyle(hjStyle);
        cell = row.createCell(2);
        cell.setCellValue("");
        cell.setCellStyle(hjStyle);

        HSSFDataFormat df = wb.createDataFormat();
        hjStyle.setDataFormat(df.getBuiltinFormat("0.00"));
        cell = row.createCell(3);
        String sc = CellReference.convertNumToColString(3);
        rowStartIndex=rowStartIndex+1;
        cell.setCellFormula("SUM(" + sc +  (rowStartIndex)+ ":"+sc+rowNum+")");
        cell.setCellStyle(hjStyle);
        cell = row.createCell(4);
        String pc = CellReference.convertNumToColString(4);
        cell.setCellFormula("SUM(" + pc + (rowStartIndex) + ":"+pc+rowNum+")");
        cell.setCellStyle(hjStyle);
        return rowNum;
    }

    /**
     * 招待统计
     * @param wb
     * @return
     */
    private HSSFSheet createEntertainStasticSheet(HSSFWorkbook wb,Date startTime, Date endTime, Organizer organizer,List<Map> entertainList){
        HSSFSheet sheet = wb.createSheet("招待统计");

        int rowNum = 1;

        /*招待（招待人）*/
        rowNum = entertainPersonStastics(wb,sheet,startTime,endTime,organizer,rowNum,entertainList);

        rowNum = rowNum + 3;

        /*招待（产品）*/
        entertainProductStastics(wb,sheet,startTime,endTime,organizer,rowNum);

        return sheet;
    }

    /**
     * 招待统计(招待人)
     * @param wb
     * @param sheet
     * @param startTime
     * @param endTime
     * @param organizer
     * @param rowNum
     * @return
     */
    private int entertainProductStastics(HSSFWorkbook wb,HSSFSheet sheet,Date startTime, Date endTime, Organizer organizer,int rowNum){
        List<Map> entertainList = zooEntertainOrdersService.entertainProductStastic(startTime, endTime, organizer.getOrganizerId(), null);
        Map<String,List<Map>> entertainMap = new HashMap<String,List<Map>>();
        if(entertainList!=null){
            for (Map map : entertainList) {
                String companyName = MapUtils.getString(map,"company_name","");
                List<Map> list = entertainMap.get(companyName);
                if(list==null){
                    list = new ArrayList<Map>();
                    entertainMap.put(companyName,list);
                }
                list.add(map);
            }
        }

        HSSFCellStyle headStyle = wb.createCellStyle(); // 样式对象
        HSSFFont font = wb.createFont();// 生成一个字体
        font.setColor(HSSFColor.BLACK.index);// HSSFColor.VIOLET.index // 字体颜色
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);// 字体增粗
        font.setFontHeightInPoints((short)16);
        headStyle.setFont(font);// 把字体应用到当前的样式
        headStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        headStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);//垂直居中
        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = row.createCell(1);
        cell.setCellStyle(headStyle);
        cell.setCellValue("招待汇总(产品)");

        HSSFCellStyle cellStyle = getCellStyle(wb);

        rowNum++;
        createEntertainProductStasticsHead(wb,sheet,rowNum,entertainList.size());
        rowNum++;

        int startIndex = rowNum;

        if(entertainList != null){
            Iterator<Map.Entry<String, List<Map>>> iterator = entertainMap.entrySet().iterator();

            HSSFCellStyle cellStyleNormal = getCellStyle(wb);
            HSSFDataFormat df = wb.createDataFormat();
            cellStyleNormal.setDataFormat(df.getBuiltinFormat("0"));

            HSSFCellStyle cellStyleDecimal = getCellStyle(wb);
            df = wb.createDataFormat();
            cellStyleDecimal.setDataFormat(df.getBuiltinFormat("0.00"));

            while (iterator.hasNext()) {
                Map.Entry<String, List<Map>> next = iterator.next();
                List<Map> list = next.getValue();
                if(list.size()>0){
                    sheet.addMergedRegion(new CellRangeAddress( rowNum,rowNum + list.size()-1,2, 2));
                }
                for (Map map : list) {
                    String companyName = MapUtils.getString(map,"company_name","");
                    String personName = MapUtils.getString(map,"activity_name","");
                    int num = MapUtils.getIntValue(map,"num",0);
                    double money = new BigDecimal(MapUtils.getDoubleValue(map,"money",0)).setScale(2,RoundingMode.HALF_UP).doubleValue();

                    row = sheet.createRow(rowNum);
                    cell = row.createCell( 1);
                    cell.setCellValue("招待汇总");
                    cell.setCellStyle(cellStyle);
                    cell = row.createCell( 2);
                    cell.setCellValue(companyName);
                    cell.setCellStyle(cellStyle);
                    cell = row.createCell( 3);
                    cell.setCellValue(personName);
                    cell.setCellStyle(cellStyle);

                    cell = row.createCell( 4);
                    cell.setCellStyle(cellStyleNormal);
                    cell.setCellValue(num);

                    cell = row.createCell( 5);
                    cell.setCellStyle(cellStyleDecimal);
                    cell.setCellValue(money);
                    rowNum++;
                }
            }
        }

        row = sheet.createRow(rowNum);
        cell = row.createCell( 1);
        cell.setCellValue("总计");
        cell.setCellStyle(cellStyle);
        cell = row.createCell( 2);
        cell.setCellValue("");
        cell.setCellStyle(cellStyle);
        cell = row.createCell( 3);
        cell.setCellValue("");
        cell.setCellStyle(cellStyle);

        cellStyle = getCellStyle(wb);
        HSSFDataFormat df = wb.createDataFormat();
        cellStyle.setDataFormat(df.getBuiltinFormat("0"));
        cell = row.createCell( 4);
        String sc4 = CellReference.convertNumToColString(4);
        cell.setCellStyle(cellStyle);
        if(startIndex == rowNum){
            cell.setCellValue(0d);
        }else{
            cell.setCellFormula("SUM(" + sc4 + (startIndex+1) + ":"+sc4+(rowNum)+")");
        }


        cellStyle = getCellStyle(wb);
        df = wb.createDataFormat();
        cellStyle.setDataFormat(df.getBuiltinFormat("0.00"));
        cell = row.createCell( 5);
        String sc5 = CellReference.convertNumToColString(5);
        cell.setCellStyle(cellStyle);
        if(startIndex == rowNum){
            cell.setCellValue(0d);
        }else{
            cell.setCellFormula("SUM(" + sc5 + (startIndex+1) + ":"+sc5+(rowNum)+")");
        }

        return rowNum;
    }

    private void createEntertainProductStasticsHead(HSSFWorkbook wb,HSSFSheet sheet,int rowNum,int size){
        HSSFCellStyle style = wb.createCellStyle(); // 样式对象
        HSSFFont font = wb.createFont();// 生成一个字体
        font.setColor(HSSFColor.BLACK.index);// HSSFColor.VIOLET.index // 字体颜色
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);// 字体增粗
        style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        style.setFillForegroundColor(HSSFColor.GREY_40_PERCENT.index);
        style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        style.setBorderRight(HSSFCellStyle.BORDER_THIN);
        style.setBorderTop(HSSFCellStyle.BORDER_THIN);
        style.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);//垂直居中
        style.setFont(font);// 把字体应用到当前的样式
        //style.setFillBackgroundColor(HSSFColor.BLACK.index);
        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = row.createCell(1);
        cell.setCellValue("项目类型");
        cell.setCellStyle(style);
        sheet.setColumnWidth(1, 22 * 172);
        cell = row.createCell(2);
        cell.setCellValue("公司名称");
        cell.setCellStyle(style);
        sheet.setColumnWidth(2, 22 * 172);
        cell = row.createCell(3);
        cell.setCellValue("产品名称");
        cell.setCellStyle(style);
        sheet.setColumnWidth(3, 22 * 172);
        cell = row.createCell(4);
        cell.setCellValue("核销数量");
        sheet.setColumnWidth(4, 22 * 172);
        cell.setCellStyle(style);
        cell = row.createCell(5);
        cell.setCellValue("收入金额");
        sheet.setColumnWidth(4, 22 * 172);
        cell.setCellStyle(style);
        if(size == 0){
            return;
        }
        sheet.addMergedRegion(new CellRangeAddress( rowNum+1,rowNum + size,1, 1));
    }

    /**
     * 招待统计(招待人)
     * @param wb
     * @param sheet
     * @param startTime
     * @param endTime
     * @param organizer
     * @param rowNum
     * @return
     */
    private int entertainPersonStastics(HSSFWorkbook wb,HSSFSheet sheet,Date startTime, Date endTime, Organizer organizer,int rowNum,List<Map> entertainList){

        Map<String,List<Map>> entertainMap = new HashMap<String,List<Map>>();
        if(entertainList!=null){
            for (Map map : entertainList) {
                String companyName = MapUtils.getString(map,"company_name","");
                List<Map> list = entertainMap.get(companyName);
                if(list==null){
                    list = new ArrayList<Map>();
                    entertainMap.put(companyName,list);
                }
                list.add(map);
            }
        }

        HSSFCellStyle headStyle = wb.createCellStyle(); // 样式对象
        HSSFFont font = wb.createFont();// 生成一个字体
        font.setColor(HSSFColor.BLACK.index);// HSSFColor.VIOLET.index // 字体颜色
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);// 字体增粗
        font.setFontHeightInPoints((short)16);
        headStyle.setFont(font);// 把字体应用到当前的样式
        headStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        headStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);//垂直居中
        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = row.createCell(1);
        cell.setCellStyle(headStyle);
        cell.setCellValue("招待汇总(招待人)");

        HSSFCellStyle cellStyle = getCellStyle(wb);

        rowNum++;
        if(entertainList!=null){
            createEntertainStasticsHead(wb,sheet,rowNum,entertainList.size());
        }else{
            createEntertainStasticsHead(wb,sheet,rowNum,0);
        }
        rowNum++;

        int startIndex = rowNum;

        if(entertainList != null){
            Iterator<Map.Entry<String, List<Map>>> iterator = entertainMap.entrySet().iterator();

            HSSFCellStyle cellStyleNormal = getCellStyle(wb);
            HSSFDataFormat df = wb.createDataFormat();
            cellStyleNormal.setDataFormat(df.getBuiltinFormat("0"));

            HSSFCellStyle cellStyleDecimal = getCellStyle(wb);
            df = wb.createDataFormat();
            cellStyleDecimal.setDataFormat(df.getBuiltinFormat("0.00"));

            while (iterator.hasNext()) {
                Map.Entry<String, List<Map>> next = iterator.next();
                List<Map> list = next.getValue();
                if(list.size()>0){
                    sheet.addMergedRegion(new CellRangeAddress( rowNum,rowNum + list.size()-1,2, 2));
                }
                for (Map map : list) {
                    String companyName = MapUtils.getString(map,"company_name","");
                    String personName = MapUtils.getString(map,"entertain_person_name","");
                    int num = MapUtils.getIntValue(map,"num",0);
                    double money = new BigDecimal(MapUtils.getDoubleValue(map,"money",0)).setScale(2,RoundingMode.HALF_UP).doubleValue();

                    row = sheet.createRow(rowNum);
                    cell = row.createCell( 1);
                    cell.setCellValue("招待汇总");
                    cell.setCellStyle(cellStyle);
                    cell = row.createCell( 2);
                    cell.setCellValue(companyName);
                    cell.setCellStyle(cellStyle);
                    cell = row.createCell( 3);
                    cell.setCellValue(personName);
                    cell.setCellStyle(cellStyle);

                    cell = row.createCell( 4);
                    cell.setCellStyle(cellStyleNormal);
                    cell.setCellValue(num);

                    cell = row.createCell( 5);
                    cell.setCellStyle(cellStyleDecimal);
                    cell.setCellValue(money);
                    rowNum++;
                }
            }
        }

        row = sheet.createRow(rowNum);
        cell = row.createCell( 1);
        cell.setCellValue("总计");
        cell.setCellStyle(cellStyle);
        cell = row.createCell( 2);
        cell.setCellValue("");
        cell.setCellStyle(cellStyle);
        cell = row.createCell( 3);
        cell.setCellValue("");
        cell.setCellStyle(cellStyle);

        cellStyle = getCellStyle(wb);
        HSSFDataFormat df = wb.createDataFormat();
        cellStyle.setDataFormat(df.getBuiltinFormat("0"));
        cell = row.createCell( 4);
        String sc4 = CellReference.convertNumToColString(4);
        cell.setCellStyle(cellStyle);
        if(startIndex == rowNum){
            cell.setCellValue(0d);
        }else{
            cell.setCellFormula("SUM(" + sc4 + (startIndex+1) + ":"+sc4+(rowNum)+")");
        }


        cellStyle = getCellStyle(wb);
        df = wb.createDataFormat();
        cellStyle.setDataFormat(df.getBuiltinFormat("0.00"));
        cell = row.createCell( 5);
        String sc5 = CellReference.convertNumToColString(5);
        cell.setCellStyle(cellStyle);
        if(startIndex == rowNum){
            cell.setCellValue(0d);
        }else{
            cell.setCellFormula("SUM(" + sc5 + (startIndex+1) + ":"+sc5+(rowNum)+")");
        }

        return rowNum;
    }

    private void createEntertainStasticsHead(HSSFWorkbook wb,HSSFSheet sheet,int rowNum,int size){
        HSSFCellStyle style = wb.createCellStyle(); // 样式对象
        HSSFFont font = wb.createFont();// 生成一个字体
        font.setColor(HSSFColor.BLACK.index);// HSSFColor.VIOLET.index // 字体颜色
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);// 字体增粗
        style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        style.setFillForegroundColor(HSSFColor.GREY_40_PERCENT.index);
        style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        style.setBorderRight(HSSFCellStyle.BORDER_THIN);
        style.setBorderTop(HSSFCellStyle.BORDER_THIN);
        style.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);//垂直居中
        style.setFont(font);// 把字体应用到当前的样式
        //style.setFillBackgroundColor(HSSFColor.BLACK.index);
        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = row.createCell(1);
        cell.setCellValue("项目类型");
        cell.setCellStyle(style);
        sheet.setColumnWidth(1, 22 * 172);
        cell = row.createCell(2);
        cell.setCellValue("公司名称");
        cell.setCellStyle(style);
        sheet.setColumnWidth(2, 22 * 172);
        cell = row.createCell(3);
        cell.setCellValue("招待人");
        cell.setCellStyle(style);
        sheet.setColumnWidth(3, 22 * 172);
        cell = row.createCell(4);
        cell.setCellValue("核销数量");
        sheet.setColumnWidth(4, 22 * 172);
        cell.setCellStyle(style);
        cell = row.createCell(5);
        cell.setCellValue("收入金额");
        sheet.setColumnWidth(4, 22 * 172);
        cell.setCellStyle(style);
        if(size == 0){
            return;
        }
        sheet.addMergedRegion(new CellRangeAddress( rowNum+1,rowNum + size,1, 1));
    }

    /**
     * 挂账统计
     * @param wb
     * @return
     */
    private HSSFSheet createHungStasticsSheet(HSSFWorkbook wb,Date startTime, Date endTime, Organizer organizer,List<Map> hungUpList){

        HSSFSheet sheet = wb.createSheet("挂账统计");
        int rowNum = 1;

        /*挂账人维度汇总*/
        rowNum = hungUpPersonStastics(wb,sheet,startTime,endTime,organizer,rowNum,hungUpList);

        rowNum = rowNum + 3;

        /*产品维度进行汇总*/
        hungUpProductStastics(wb,sheet,startTime,endTime,organizer,rowNum);

        return sheet;
    }

    /**
     * 产品维度进行汇总
     * @param wb
     * @param sheet
     * @param startTime
     * @param endTime
     * @param organizer
     * @param rowNum
     * @return
     */
    private int hungUpProductStastics(HSSFWorkbook wb,HSSFSheet sheet,Date startTime, Date endTime, Organizer organizer,int rowNum){
        List<Map> hungUpList = zooOnAccountOrdersService.hungUpProductStastic(startTime, endTime, organizer.getOrganizerId(), null);
        Map<String,List<Map>> hungUpMap = new HashMap<String,List<Map>>();
        if(hungUpList!=null){
            for (Map map : hungUpList) {
                String companyName = MapUtils.getString(map,"company_name","");
                List<Map> list = hungUpMap.get(companyName);
                if(list==null){
                    list = new ArrayList<Map>();
                    hungUpMap.put(companyName,list);
                }
                list.add(map);
            }

        }

        HSSFCellStyle headStyle = wb.createCellStyle(); // 样式对象
        HSSFFont font = wb.createFont();// 生成一个字体
        font.setColor(HSSFColor.BLACK.index);// HSSFColor.VIOLET.index // 字体颜色
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);// 字体增粗
        font.setFontHeightInPoints((short)16);
        headStyle.setFont(font);// 把字体应用到当前的样式
        headStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        headStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);//垂直居中
        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = row.createCell(1);
        cell.setCellStyle(headStyle);
        cell.setCellValue("挂帐汇总(产品)");

        HSSFCellStyle cellStyle = getCellStyle(wb);

        rowNum++;
        if(hungUpList!=null){
            createHungProductStasticsHead(wb,sheet,rowNum,hungUpList.size());
        }else{
            createHungProductStasticsHead(wb,sheet,rowNum,0);
        }
        rowNum++;

        int startIndex = rowNum;

        if(hungUpList != null){

            Iterator<Map.Entry<String, List<Map>>> iterator = hungUpMap.entrySet().iterator();

            HSSFCellStyle cellStyleNormal = getCellStyle(wb);
            HSSFDataFormat df = wb.createDataFormat();
            cellStyleNormal.setDataFormat(df.getBuiltinFormat("0"));

            HSSFCellStyle cellStyleDecimal = getCellStyle(wb);
            df = wb.createDataFormat();
            cellStyleDecimal.setDataFormat(df.getBuiltinFormat("0.00"));

            while (iterator.hasNext()) {
                Map.Entry<String, List<Map>> next = iterator.next();
                List<Map> list = next.getValue();
                if(list.size()>0){
                    sheet.addMergedRegion(new CellRangeAddress( rowNum,rowNum + list.size()-1,2, 2));
                }

                for (Map map : list) {
                    String companyName = MapUtils.getString(map,"company_name","");
                    String personName = MapUtils.getString(map,"activity_name","");
                    int num = MapUtils.getIntValue(map,"num",0);
                    double money = new BigDecimal(MapUtils.getDoubleValue(map,"money",0)).setScale(2,RoundingMode.HALF_UP).doubleValue();

                    row = sheet.createRow(rowNum);
                    cell = row.createCell( 1);
                    cell.setCellValue("挂帐汇总");

                    cell = row.createCell( 2);
                    cell.setCellValue(companyName);
                    cell.setCellStyle(cellStyle);
                    cell = row.createCell( 3);
                    cell.setCellValue(personName);
                    cell.setCellStyle(cellStyle);

                    cell = row.createCell( 4);
                    cell.setCellStyle(cellStyleNormal);
                    cell.setCellValue(num);

                    cell = row.createCell( 5);
                    cell.setCellStyle(cellStyleDecimal);
                    cell.setCellValue(money);
                    rowNum++;
                }
            }
        }

        row = sheet.createRow(rowNum);
        cell = row.createCell( 1);
        cell.setCellValue("总计");
        cell.setCellStyle(cellStyle);
        cell = row.createCell( 2);
        cell.setCellValue("");
        cell.setCellStyle(cellStyle);
        cell = row.createCell( 3);
        cell.setCellValue("");
        cell.setCellStyle(cellStyle);

        cellStyle = getCellStyle(wb);
        HSSFDataFormat df = wb.createDataFormat();
        cellStyle.setDataFormat(df.getBuiltinFormat("0"));
        cell = row.createCell( 4);
        String sc4 = CellReference.convertNumToColString(4);
        cell.setCellStyle(cellStyle);
        if(startIndex==rowNum){
            cell.setCellValue(0d);
        }else{
            cell.setCellFormula("SUM(" + sc4 + (startIndex+1) + ":"+sc4+(rowNum)+")");
        }

        cellStyle = getCellStyle(wb);
        df = wb.createDataFormat();
        cellStyle.setDataFormat(df.getBuiltinFormat("0.00"));
        cell = row.createCell( 5);
        String sc5 = CellReference.convertNumToColString(5);
        cell.setCellStyle(cellStyle);
        if(startIndex==rowNum){
            cell.setCellValue(0d);
        }else{
            cell.setCellFormula("SUM(" + sc5 + (startIndex+1) + ":"+sc5+(rowNum)+")");
        }


        return rowNum;
    }

    private int hungUpPersonStastics(HSSFWorkbook wb,HSSFSheet sheet,Date startTime, Date endTime, Organizer organizer,int rowNum,List<Map> hungUpList){
        Map<String,List<Map>> hungUpMap = new HashMap<String,List<Map>>();
        if(hungUpList!=null){
            for (Map map : hungUpList) {
                String companyName = MapUtils.getString(map,"company_name","");
                List<Map> list = hungUpMap.get(companyName);
                if(list==null){
                    list = new ArrayList<Map>();
                    hungUpMap.put(companyName,list);
                }
                list.add(map);
            }

        }

        HSSFCellStyle headStyle = wb.createCellStyle(); // 样式对象
        HSSFFont font = wb.createFont();// 生成一个字体
        font.setColor(HSSFColor.BLACK.index);// HSSFColor.VIOLET.index // 字体颜色
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);// 字体增粗
        font.setFontHeightInPoints((short)16);
        headStyle.setFont(font);// 把字体应用到当前的样式
        headStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        headStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);//垂直居中
        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = row.createCell(1);
        cell.setCellStyle(headStyle);
        cell.setCellValue("挂帐汇总(挂账人)");

        HSSFCellStyle cellStyle = getCellStyle(wb);

        //List<Map> abnormalList = zooOnAccountOrdersService.abnormalStastic(startTime, endTime, organizer.getOrganizerId(), null);
        if(hungUpList != null){
            rowNum++;
            createHungStasticsHead(wb,sheet,rowNum,hungUpList.size());
            rowNum++;

            HSSFCellStyle cellStyleNormal = getCellStyle(wb);
            HSSFDataFormat df = wb.createDataFormat();
            cellStyleNormal.setDataFormat(df.getBuiltinFormat("0"));

            HSSFCellStyle cellStyleDecimal = getCellStyle(wb);
            df = wb.createDataFormat();
            cellStyleDecimal.setDataFormat(df.getBuiltinFormat("0.00"));

            Iterator<Map.Entry<String, List<Map>>> iterator = hungUpMap.entrySet().iterator();

            while (iterator.hasNext()) {
                Map.Entry<String, List<Map>> next = iterator.next();
                List<Map> list = next.getValue();
                if(list.size()>0){
                    sheet.addMergedRegion(new CellRangeAddress( rowNum,rowNum + list.size()-1,2, 2));
                }
                for (Map map : list) {
                    String companyName = MapUtils.getString(map,"company_name","");
                    String personName = MapUtils.getString(map,"hangup_person_name","");
                    int num = MapUtils.getIntValue(map,"num",0);
                    double money = new BigDecimal(MapUtils.getDoubleValue(map,"money",0)).setScale(2,RoundingMode.HALF_UP).doubleValue();

                    row = sheet.createRow(rowNum);
                    cell = row.createCell( 1);
                    cell.setCellValue("挂帐汇总");
                    cell.setCellStyle(cellStyle);
                    cell = row.createCell( 2);
                    cell.setCellValue(companyName);
                    cell.setCellStyle(cellStyle);
                    cell = row.createCell( 3);
                    cell.setCellValue(personName);
                    cell.setCellStyle(cellStyle);

                    cell = row.createCell( 4);
                    cell.setCellStyle(cellStyleNormal);
                    cell.setCellValue(num);

                    cell = row.createCell( 5);
                    cell.setCellStyle(cellStyleDecimal);
                    cell.setCellValue(money);
                    rowNum++;
                }
            }
        }

        row = sheet.createRow(rowNum);
        cell = row.createCell( 1);
        cell.setCellValue("总计");
        cell.setCellStyle(cellStyle);
        cell = row.createCell( 2);
        cell.setCellValue("");
        cell.setCellStyle(cellStyle);
        cell = row.createCell( 3);
        cell.setCellValue("");
        cell.setCellStyle(cellStyle);

        cellStyle = getCellStyle(wb);
        HSSFDataFormat df = wb.createDataFormat();
        cellStyle.setDataFormat(df.getBuiltinFormat("0"));
        cell = row.createCell( 4);
        String sc4 = CellReference.convertNumToColString(4);
        cell.setCellStyle(cellStyle);
        cell.setCellFormula("SUM(" + sc4 + 4 + ":"+sc4+(rowNum)+")");

        cellStyle = getCellStyle(wb);
        df = wb.createDataFormat();
        cellStyle.setDataFormat(df.getBuiltinFormat("0.00"));
        cell = row.createCell( 5);
        String sc5 = CellReference.convertNumToColString(5);
        cell.setCellStyle(cellStyle);
        cell.setCellFormula("SUM(" + sc5 + 4 + ":"+sc5+(rowNum)+")");

        return rowNum;
    }

    private HSSFCellStyle getCellStyle(HSSFWorkbook wb){
        HSSFCellStyle headStyle = wb.createCellStyle(); // 样式对象
        headStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        headStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        headStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        headStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        headStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
        headStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);//垂直居中
        return headStyle;
    }

    private void createHungProductStasticsHead(HSSFWorkbook wb,HSSFSheet sheet,int rowNum,int size){
        HSSFCellStyle style = wb.createCellStyle(); // 样式对象
        HSSFFont font = wb.createFont();// 生成一个字体
        font.setColor(HSSFColor.BLACK.index);// HSSFColor.VIOLET.index // 字体颜色
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);// 字体增粗
        style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        style.setFillForegroundColor(HSSFColor.GREY_40_PERCENT.index);
        style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        style.setBorderRight(HSSFCellStyle.BORDER_THIN);
        style.setBorderTop(HSSFCellStyle.BORDER_THIN);
        style.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);//垂直居中
        style.setFont(font);// 把字体应用到当前的样式
        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = row.createCell(1);
        cell.setCellValue("项目类型");
        cell.setCellStyle(style);
        sheet.setColumnWidth(1, 22 * 172);
        cell = row.createCell(2);
        cell.setCellValue("公司名称");
        cell.setCellStyle(style);
        sheet.setColumnWidth(2, 22 * 172);
        cell = row.createCell(3);
        cell.setCellValue("产品名称");
        cell.setCellStyle(style);
        sheet.setColumnWidth(3, 22 * 172);
        cell = row.createCell(4);
        cell.setCellValue("核销数量");
        sheet.setColumnWidth(4, 22 * 172);
        cell.setCellStyle(style);
        cell = row.createCell(5);
        cell.setCellValue("收入金额");
        sheet.setColumnWidth(4, 22 * 172);
        cell.setCellStyle(style);
        if(size == 0){
            return;
        }
        sheet.addMergedRegion(new CellRangeAddress( rowNum+1,rowNum + size,1, 1));
    }

    private void createHungStasticsHead(HSSFWorkbook wb,HSSFSheet sheet,int rowNum,int size){
        HSSFCellStyle style = wb.createCellStyle(); // 样式对象
        HSSFFont font = wb.createFont();// 生成一个字体
        font.setColor(HSSFColor.BLACK.index);// HSSFColor.VIOLET.index // 字体颜色
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);// 字体增粗
        style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        style.setFillForegroundColor(HSSFColor.GREY_40_PERCENT.index);
        style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        style.setBorderRight(HSSFCellStyle.BORDER_THIN);
        style.setBorderTop(HSSFCellStyle.BORDER_THIN);
        style.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);//垂直居中
        style.setFont(font);// 把字体应用到当前的样式
        //style.setFillBackgroundColor(HSSFColor.BLACK.index);
        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = row.createCell(1);
        cell.setCellValue("项目类型");
        cell.setCellStyle(style);
        sheet.setColumnWidth(1, 22 * 172);
        cell = row.createCell(2);
        cell.setCellValue("公司名称");
        cell.setCellStyle(style);
        sheet.setColumnWidth(2, 22 * 172);
        cell = row.createCell(3);
        cell.setCellValue("挂帐人");
        cell.setCellStyle(style);
        sheet.setColumnWidth(3, 22 * 172);
        cell = row.createCell(4);
        cell.setCellValue("核销数量");
        sheet.setColumnWidth(4, 22 * 172);
        cell.setCellStyle(style);
        cell = row.createCell(5);
        cell.setCellValue("收入金额");
        sheet.setColumnWidth(4, 22 * 172);
        cell.setCellStyle(style);
        /*cell = row.createCell(6);
        cell.setCellValue("异常退款金额");
        sheet.setColumnWidth(4, 22 * 172);
        cell.setCellStyle(style);
        cell = row.createCell(7);
        cell.setCellValue("应收金额");
        sheet.setColumnWidth(4, 22 * 172);
        cell.setCellStyle(style);*/
        if(size == 0){
            return;
        }
        sheet.addMergedRegion(new CellRangeAddress( rowNum+1,rowNum + size,1, 1));
    }

    /**
     * 查询招待挂账
     * @param companyname
     * @param personName
     * @param abnormalList
     * @return
     */
    private Map findAbnormalMap(String companyname,String personName,List<Map> abnormalList){
        if(abnormalList==null){
            return null;
        }
        for (Map abnormal : abnormalList) {
            if(companyname.equals(MapUtils.getString(abnormal,"company_name",""))
                    && personName.equals(MapUtils.getString(abnormal,"hangup_person_name",""))){
                return abnormal;
            }
        }
        return null;
    }

    //生成收银员支付方式汇总sheet
    private HSSFSheet createStasticsSheet(Date startTime, Date endTime,BigDecimal totalStoragePayPrice,Map<Integer,Map<String,Map<String,Object>>> saleProductStastic,
                                          List<Map<String, Object>> rentOrders,List<Map<String, Object>> rentTimeOutOrders,
                                          List<Map<String, Object>> addServiceOrders,List<Map<String, Object>> distributorOrders,Map<Integer,Map<String,Object>> indemnityProduct,
                                          Map<Integer,Map<String,Object>> refundProduct,Map<Integer,Map<String,BigDecimal>> payTypeStastic,
                                          Map<Integer,Map<String,BigDecimal>> depositStastic,List<Map<String,Object>> allStastic,
                                          HSSFWorkbook wb,List<Map> entertainList,List<Map> hungUpList,
                                          Map<String,Map<String,Map<String,Map<String,Object>>>> abnormalPayStasticsList,
                                          Set<String> abnormalPayNameSet){
        HSSFSheet sheet = wb.createSheet("财务汇总表");

        int rowNum = 2;

        HSSFCellStyle headStyle = wb.createCellStyle(); // 样式对象
        HSSFFont font = wb.createFont();// 生成一个字体
        font.setColor(HSSFColor.BLACK.index);// HSSFColor.VIOLET.index // 字体颜色
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);// 字体增粗
        font.setFontHeightInPoints((short)16);
        headStyle.setFont(font);// 把字体应用到当前的样式
        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = row.createCell(1);
        cell.setCellValue("时间：" + DateUtil.convertDateToString(startTime,"yyyy/MM/dd HH:mm:ss") + "-" + DateUtil.convertDateToString(endTime,"yyyy/MM/dd HH:mm:ss"));
        cell.setCellStyle(headStyle);

        rowNum++;
        rowNum++;

        headStyle = wb.createCellStyle(); // 样式对象
        font = wb.createFont();// 生成一个字体
        font.setColor(HSSFColor.BLACK.index);// HSSFColor.VIOLET.index // 字体颜色
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);// 字体增粗
        font.setFontHeightInPoints((short)16);
        headStyle.setFont(font);// 把字体应用到当前的样式
        headStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        headStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);//垂直居中
        row = sheet.createRow(rowNum);
        cell = row.createCell(1);
        cell.setCellStyle(headStyle);
        cell.setCellValue("线下销售汇总表格");

        int[] startIndex = new int[0];

        rowNum++;
        if(saleProductStastic!=null){
            FinanceExcelUtil.createProductHead(wb,sheet,rowNum);
            rowNum++;
            //产品
            rowNum = FinanceExcelUtil.createProdctItem(wb,sheet,rowNum,saleProductStastic);
            rowNum++;

            startIndex =ArrayUtils.add(startIndex,rowNum);

        }

        //租赁物//超时租赁物
        if((rentOrders!=null||rentTimeOutOrders!=null) && (rentOrders.size()>0 || rentTimeOutOrders.size()>0)){
            //租赁物
            rowNum = FinanceExcelUtil.createRentItem(wb,sheet,rowNum,rentOrders,rentTimeOutOrders);
            rowNum++;

            startIndex =ArrayUtils.add(startIndex,rowNum);
        }

        //增值服务
        if(addServiceOrders!=null&&addServiceOrders.size()>0){
            //增值服务
            rowNum = FinanceExcelUtil.createAddServiceItem(wb,sheet,rowNum,addServiceOrders);
            rowNum++;

            startIndex=ArrayUtils.add(startIndex,rowNum);
        }
        //分销渠道
        if(distributorOrders!=null&&distributorOrders.size()>0){
            //分销渠道
            rowNum = createDistributorItem(wb,sheet,rowNum,distributorOrders);
            rowNum++;

            startIndex=ArrayUtils.add(startIndex,rowNum);
        }

        if(startIndex.length>0){
            HSSFCellStyle hjStyle = wb.createCellStyle(); // 样式对象
            hjStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
            hjStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
            hjStyle.setFillForegroundColor(HSSFColor.GREY_40_PERCENT.index);
            hjStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
            hjStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
            hjStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
            hjStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);

            row = sheet.createRow(rowNum);
            cell = row.createCell(1);
            cell.setCellValue("收入总金额");
            cell.setCellStyle(hjStyle);
            cell = row.createCell(2);
            cell.setCellValue("");
            cell.setCellStyle(hjStyle);

            HSSFDataFormat df = wb.createDataFormat();
            hjStyle.setDataFormat(df.getBuiltinFormat("0.00"));
            cell = row.createCell(3);
            String sc = CellReference.convertNumToColString(3);
            String sumStr = "";
            for (int index : startIndex) {
                if(sumStr.equals("")){
                    sumStr += sc + index ;
                }else{
                    sumStr += "+"+sc + index ;
                }
            }
            cell.setCellFormula(sumStr);
            cell.setCellStyle(hjStyle);
            cell = row.createCell(4);
            String pc = CellReference.convertNumToColString(4);
            sumStr = "";
            for (int index : startIndex) {
                if(sumStr.equals("")){
                    sumStr += pc + index ;
                }else{
                    sumStr += "+"+pc + index ;
                }
            }
            cell.setCellFormula(sumStr);
            cell.setCellStyle(hjStyle);
            rowNum++;
        }


        if(indemnityProduct!=null&&indemnityProduct.size()>0){
            //赔偿
            rowNum = FinanceExcelUtil.createDepositIndemnityItem(wb,sheet,rowNum,indemnityProduct);
            rowNum++;
        }

        //挂账
        if(hungUpList!=null && hungUpList.size()>0){
            Map<String,Map<String,Object>> hungUpMap = new HashMap<String,Map<String,Object>>();
            for (Map hup : hungUpList){
                String companyName = MapUtils.getString(hup,"company_name","");
                Map<String,Object> item = hungUpMap.get(companyName);
                if(item == null){
                    item = new HashMap<String,Object>();
                    hungUpMap.put(companyName,item);
                    item.put("name",companyName);
                }
                int num = MapUtils.getIntValue(item, "num", 0);
                double money = MapUtils.getDoubleValue(item, "money", 0);
                item.put("num",num+MapUtils.getIntValue(hup, "num", 0));
                item.put("money",money+MapUtils.getDoubleValue(hup, "money", 0));
            }
            rowNum = FinanceExcelUtil.createHungUpItem(wb,sheet,rowNum,hungUpMap);
            rowNum++;
        }

        //招待
        if(entertainList!=null && entertainList.size()>0){
            Map<String,Map<String,Object>> entertainMap = new HashMap<String,Map<String,Object>>();
            for (Map hup : entertainList){
                String companyName = MapUtils.getString(hup,"company_name","");
                Map<String,Object> item = entertainMap.get(companyName);
                if(item == null){
                    item = new HashMap<String,Object>();
                    entertainMap.put(companyName,item);
                    item.put("name",companyName);
                }
                int num = MapUtils.getIntValue(item, "num", 0);
                double money = MapUtils.getDoubleValue(item, "money", 0);
                item.put("num",num+MapUtils.getIntValue(hup, "num", 0));
                item.put("money",money+MapUtils.getDoubleValue(hup, "money", 0));
            }
            rowNum = FinanceExcelUtil.createentertainItem(wb,sheet,rowNum,entertainMap);
            rowNum++;
        }

        rowNum++;
        rowNum++;
        row = sheet.createRow(rowNum);
        cell = row.createCell(1);
        cell.setCellStyle(headStyle);
        cell.setCellValue("异常退款汇总");
        //异常退款订单汇总
        rowNum++;
        /*if(refundProduct!=null && refundProduct.size()>0){
            createrefundProductHead(wb,sheet,rowNum);
            rowNum++;
            //异常退款订单汇总
            rowNum = createRefundProductItem(wb,sheet,rowNum,refundProduct);
            rowNum++;
        }*/

        if(abnormalPayStasticsList!=null && abnormalPayStasticsList.size()>0){
            String[] payHead = abnormalPayNameSet.toArray(new String[abnormalPayNameSet.size()]);
            payHead = ArrayUtils.add(payHead,0,"项目名称");
            payHead = ArrayUtils.add(payHead,1,"产品名称");
            payHead = ArrayUtils.add(payHead,"总计");
            createreExcelHead(wb,sheet,rowNum,payHead);
            rowNum++;
            //异常退款订单汇总
            rowNum = createPayRefundProductItem(wb,sheet,rowNum,abnormalPayStasticsList,abnormalPayNameSet);
            rowNum++;
        }


        rowNum++;
        rowNum++;
        //支付方式汇总 Map<Integer,Map<String,BigDecimal>> payTypeStastic
        row = sheet.createRow(rowNum);
        cell = row.createCell(1);
        cell.setCellStyle(headStyle);
        cell.setCellValue("支付方式汇总");
        rowNum++;
        if(payTypeStastic!=null){
            FinanceExcelUtil.createPayTypeHead(wb,sheet,rowNum);
            rowNum++;
            //支付方式
            rowNum = FinanceExcelUtil.createPayTypeStasticItem(wb,sheet,rowNum,payTypeStastic);
            rowNum++;
        }
        rowNum++;
        rowNum++;
        //押金汇总
        row = sheet.createRow(rowNum);
        cell = row.createCell(1);
        cell.setCellStyle(headStyle);
        cell.setCellValue("押金支付方式汇总");
        rowNum++;
        if(payTypeStastic!=null){
            FinanceExcelUtil.createDepositStasticHead(wb,sheet,rowNum);
            rowNum++;
            //押金支付方式
            rowNum = FinanceExcelUtil.createDepositStasticItem(wb,sheet,rowNum,depositStastic);
            rowNum++;
        }
        rowNum++;
        rowNum++;
        //汇总
        row = sheet.createRow(rowNum);
        cell = row.createCell(1);
        cell.setCellStyle(headStyle);
        cell.setCellValue("汇总(储值支付不计算在实收金额中)");
        rowNum++;
        if(payTypeStastic!=null){
            //汇总x
            rowNum = FinanceExcelUtil.createEndItem(wb,sheet,rowNum,allStastic);
            rowNum++;
        }

        return sheet;
    }

    /**
     * 创建异常退款(支付统计)
     * @param wb
     * @param sheet
     * @param rowNum
     * @return
     */
    private int createPayRefundProductItem(HSSFWorkbook wb,HSSFSheet sheet,int rowNum,Map<String,Map<String,Map<String,Map<String,Object>>>> abnormalPayStasticsList,Set<String> payNameSet){

        HSSFCellStyle headStyle = wb.createCellStyle(); // 样式对象
        headStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        headStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        headStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        headStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        headStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
        headStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);//垂直居中

        int rowStartIndex = rowNum;
        Set<Map.Entry<String,Map<String, Map<String, Map<String, Object>>>>> entries = abnormalPayStasticsList.entrySet();
        Iterator<Map.Entry<String,Map<String, Map<String, Map<String, Object>>>>> iterator = entries.iterator();
        while (iterator.hasNext()){

            Map.Entry<String,Map<String, Map<String, Map<String, Object>>>> next = iterator.next();
            Map<String,Map<String, Map<String, Object>>> value = next.getValue();
            String key = next.getKey();

            Iterator<Map.Entry<String, Map<String, Map<String, Object>>>> prodIterator = value.entrySet().iterator();

            sheet.addMergedRegion(new CellRangeAddress(rowNum, rowNum + value.size()-1, 1, 1));
            while (prodIterator.hasNext()){

                HSSFRow row = sheet.createRow(rowNum);

                Map.Entry<String, Map<String, Map<String, Object>>> prodNext = prodIterator.next();

                Map<String, Map<String, Object>> payValueMap = prodNext.getValue();
                String productName = prodNext.getKey();

                int index = 1;
                HSSFCell cell = row.createCell(index);
                cell.setCellValue(key);
                cell.setCellStyle(headStyle);
                index++;

                cell = row.createCell(index);
                cell.setCellValue(productName);
                cell.setCellStyle(headStyle);
                index++;

                for (String payName : payNameSet) {
                    Map<String, Object> stringObjectMap = payValueMap.get(payName);
                    cell = row.createCell(index);
                    cell.setCellValue(new BigDecimal(MapUtils.getDouble(stringObjectMap,"money",0d)).setScale(2,RoundingMode.HALF_UP).doubleValue());
                    cell.setCellStyle(headStyle);
                    index++;
                }

                cell = row.createCell(index);
                String pc = CellReference.convertNumToColString(index-1);
                String pc3 = CellReference.convertNumToColString(3);
                cell.setCellFormula("SUM(" + pc3 + (rowNum+1) + ":"+pc+(rowNum+1)+")");
                cell.setCellStyle(headStyle);

                rowNum++;
            }


        }
        /*while(iterator.hasNext()){
            Map.Entry<Integer, Map<String,Object>> next = iterator.next();
            Integer key = next.getKey();
            Map<String,Object> map = (Map<String,Object>)next.getValue();
            sheet.addMergedRegion(new CellRangeAddress(rowStartIndex, rowNum, 1, 1));

            HSSFRow row = sheet.createRow(rowNum);
            HSSFCell cell = row.createCell(1);
            String cellName=FinanceExcelUtil.getActivityTypeIdName(key);
            cell.setCellValue(cellName);
            cell.setCellStyle(headStyle);
            cell = row.createCell(2);
            cell.setCellValue(MapUtils.getString(map,"name",""));
            cell.setCellStyle(headStyle);
            cell = row.createCell(3);
            cell.setCellValue(MapUtils.getIntValue(map,"num",0));
            cell.setCellStyle(headStyle);
            cell = row.createCell(4);
            cell.setCellValue(new BigDecimal(MapUtils.getDoubleValue(map,"money",0)).setScale(2,RoundingMode.HALF_UP).doubleValue());
            cell.setCellStyle(headStyle);
            rowNum++;
        }*/
        HSSFCellStyle hjStyle = wb.createCellStyle(); // 样式对象
        hjStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        hjStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        hjStyle.setFillForegroundColor(HSSFColor.GREY_40_PERCENT.index);
        hjStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        hjStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        hjStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        hjStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);

        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = row.createCell(1);
        cell.setCellValue("合计");
        cell.setCellStyle(hjStyle);
        cell = row.createCell(2);
        cell.setCellValue("");
        cell.setCellStyle(hjStyle);

        int index = 3;
        for (String payName : payNameSet) {
            cell = row.createCell(index);
            String sc = CellReference.convertNumToColString(index);
            cell.setCellFormula("SUM(" + sc + (rowStartIndex+1) + ":"+sc+rowNum+")");
            cell.setCellStyle(hjStyle);
            index++;
        }

        cell = row.createCell(index);
        String sc = CellReference.convertNumToColString(index);
        cell.setCellFormula("SUM(" + sc + (rowStartIndex+1) + ":"+sc+rowNum+")");
        cell.setCellStyle(hjStyle);

        /*if(refundProduct.size()==0){
            HSSFDataFormat df = wb.createDataFormat();
            hjStyle.setDataFormat(df.getBuiltinFormat("0.00"));
            cell = row.createCell(3);
            cell.setCellValue(0);
            cell.setCellStyle(hjStyle);
            cell = row.createCell(4);
            cell.setCellValue(0);
            cell.setCellStyle(hjStyle);
        }else{
            HSSFDataFormat df = wb.createDataFormat();
            hjStyle.setDataFormat(df.getBuiltinFormat("0.00"));
            cell = row.createCell(3);
            String sc = CellReference.convertNumToColString(3);
            cell.setCellFormula("SUM(" + sc + (rowStartIndex+1) + ":"+sc+rowNum+")");
            cell.setCellStyle(hjStyle);
            cell = row.createCell(4);
            String pc = CellReference.convertNumToColString(4);
            cell.setCellFormula("SUM(" + pc + (rowStartIndex+1) + ":"+pc+rowNum+")");
            cell.setCellStyle(hjStyle);
        }*/

        return rowNum;
    }

    /**
     * 创建分销商
     * @param wb
     * @param sheet
     * @param rowNum
     * @return
     */
    private int createDistributorItem(HSSFWorkbook wb,HSSFSheet sheet,int rowNum,List<Map<String, Object>> distributorOrders){

        HSSFCellStyle headStyle = wb.createCellStyle(); // 样式对象
        headStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        headStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        headStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        headStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        headStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
        headStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);//垂直居中

        int rowStartIndex = rowNum;
        for(int i=0;i<distributorOrders.size();i++){
            Map<String,Object> rentMap=distributorOrders.get(i);
            sheet.addMergedRegion(new CellRangeAddress(rowNum, rowNum+distributorOrders.size(), 1, 1));

            HSSFRow row = sheet.createRow(rowNum);
            HSSFCell cell = row.createCell(1);
            cell.setCellValue("分销渠道");
            cell.setCellStyle(headStyle);
            cell = row.createCell(2);
            cell.setCellValue(OtaConstant.getById(MapUtils.getIntValue(rentMap,"distributor_id",0)).getName());
            cell.setCellStyle(headStyle);
            cell = row.createCell(3);
            cell.setCellValue(MapUtils.getIntValue(rentMap,"totalNum",0));
            cell.setCellStyle(headStyle);
            cell = row.createCell(4);
            cell.setCellValue(new BigDecimal(MapUtils.getDoubleValue(rentMap,"totalMoney",0)).setScale(2,RoundingMode.HALF_UP).doubleValue());
            cell.setCellStyle(headStyle);
            rowNum++;
        }
        HSSFCellStyle hjStyle = wb.createCellStyle(); // 样式对象
        hjStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        hjStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        hjStyle.setFillForegroundColor(HSSFColor.GREY_40_PERCENT.index);
        hjStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        hjStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        hjStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        hjStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);

        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = row.createCell(1);
        cell.setCellValue("合计");
        cell.setCellStyle(hjStyle);
        cell = row.createCell(2);
        cell.setCellValue("");
        cell.setCellStyle(hjStyle);

        if(distributorOrders.size()==0){
            HSSFDataFormat df = wb.createDataFormat();
            hjStyle.setDataFormat(df.getBuiltinFormat("0.00"));
            cell = row.createCell(3);
            String sc = CellReference.convertNumToColString(3);
            cell.setCellValue(0);
            cell.setCellStyle(hjStyle);
            cell = row.createCell(4);
            String pc = CellReference.convertNumToColString(4);
            cell.setCellValue(0);
            cell.setCellStyle(hjStyle);
        }else{
            HSSFDataFormat df = wb.createDataFormat();
            hjStyle.setDataFormat(df.getBuiltinFormat("0.00"));
            cell = row.createCell(3);
            String sc = CellReference.convertNumToColString(3);
            cell.setCellFormula("SUM(" + sc + (rowStartIndex+1) + ":"+sc+rowNum+")");
            cell.setCellStyle(hjStyle);
            cell = row.createCell(4);
            String pc = CellReference.convertNumToColString(4);
            cell.setCellFormula("SUM(" + pc + (rowStartIndex+1) + ":"+pc+rowNum+")");
            cell.setCellStyle(hjStyle);
        }

        return rowNum;
    }
    /**
     * 创建赔偿
     * @param wb
     * @param sheet
     * @param rowNum
     * @return
     */
    private int createDepositIndemnityItem(HSSFWorkbook wb,HSSFSheet sheet,int rowNum,Map<Integer,Map<String,Object>> indemnityProduct){

        HSSFCellStyle headStyle = wb.createCellStyle(); // 样式对象
        headStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        headStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        headStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        headStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        headStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
        headStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);//垂直居中

        int rowStartIndex = rowNum;
        Iterator<Map.Entry<Integer,Map<String,Object>>> iterator = indemnityProduct.entrySet().iterator();
        while(iterator.hasNext()){
            Map.Entry<Integer, Map<String,Object>> next = iterator.next();
            Integer key = next.getKey();
            Map<String,Object> map = (Map<String,Object>)next.getValue();
            sheet.addMergedRegion(new CellRangeAddress(rowStartIndex, rowNum, 1, 1));

            HSSFRow row = sheet.createRow(rowNum);
            HSSFCell cell = row.createCell(1);
            cell.setCellValue("赔偿");
            cell.setCellStyle(headStyle);
            cell = row.createCell(2);
            cell.setCellValue(MapUtils.getString(map,"name",""));
            cell.setCellStyle(headStyle);
            cell = row.createCell(3);
            cell.setCellValue(MapUtils.getIntValue(map,"num",0));
            cell.setCellStyle(headStyle);
            cell = row.createCell(4);
            cell.setCellValue(new BigDecimal(MapUtils.getDoubleValue(map,"money",0)).setScale(2,RoundingMode.HALF_UP).doubleValue());
            cell.setCellStyle(headStyle);
            rowNum++;
        }
        HSSFCellStyle hjStyle = wb.createCellStyle(); // 样式对象
        hjStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        hjStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        hjStyle.setFillForegroundColor(HSSFColor.GREY_40_PERCENT.index);
        hjStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        hjStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        hjStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        hjStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);

        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = row.createCell(1);
        cell.setCellValue("合计");
        cell.setCellStyle(hjStyle);
        cell = row.createCell(2);
        cell.setCellValue("");
        cell.setCellStyle(hjStyle);

        if(indemnityProduct.size()==0){
            HSSFDataFormat df = wb.createDataFormat();
            hjStyle.setDataFormat(df.getBuiltinFormat("0.00"));
            cell = row.createCell(3);
            cell.setCellValue(0);
            cell.setCellStyle(hjStyle);
            cell = row.createCell(4);
            cell.setCellValue(0);
            cell.setCellStyle(hjStyle);
        }else{
            HSSFDataFormat df = wb.createDataFormat();
            hjStyle.setDataFormat(df.getBuiltinFormat("0.00"));
            cell = row.createCell(3);
            String sc = CellReference.convertNumToColString(3);
            cell.setCellFormula("SUM(" + sc + (rowStartIndex+1) + ":"+sc+rowNum+")");
            cell.setCellStyle(hjStyle);
            cell = row.createCell(4);
            String pc = CellReference.convertNumToColString(4);
            cell.setCellFormula("SUM(" + pc + (rowStartIndex+1) + ":"+pc+rowNum+")");
            cell.setCellStyle(hjStyle);
        }

        return rowNum;
    }

    /**
     * 创建异常退款
     * @param wb
     * @param sheet
     * @param rowNum
     * @return
     */
    private int createRefundProductItem(HSSFWorkbook wb,HSSFSheet sheet,int rowNum,Map<Integer,Map<String,Object>> refundProduct){

        HSSFCellStyle headStyle = wb.createCellStyle(); // 样式对象
        headStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        headStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        headStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        headStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        headStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
        headStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);//垂直居中

        int rowStartIndex = rowNum;
        Iterator<Map.Entry<Integer,Map<String,Object>>> iterator = refundProduct.entrySet().iterator();
        while(iterator.hasNext()){
            Map.Entry<Integer, Map<String,Object>> next = iterator.next();
            Integer key = next.getKey();
            Map<String,Object> map = (Map<String,Object>)next.getValue();
            sheet.addMergedRegion(new CellRangeAddress(rowStartIndex, rowNum, 1, 1));

            HSSFRow row = sheet.createRow(rowNum);
            HSSFCell cell = row.createCell(1);
            String cellName=FinanceExcelUtil.getActivityTypeIdName(key);
            cell.setCellValue(cellName);
            cell.setCellStyle(headStyle);
            cell = row.createCell(2);
            cell.setCellValue(MapUtils.getString(map,"name",""));
            cell.setCellStyle(headStyle);
            cell = row.createCell(3);
            cell.setCellValue(MapUtils.getIntValue(map,"num",0));
            cell.setCellStyle(headStyle);
            cell = row.createCell(4);
            cell.setCellValue(new BigDecimal(MapUtils.getDoubleValue(map,"money",0)).setScale(2,RoundingMode.HALF_UP).doubleValue());
            cell.setCellStyle(headStyle);
            rowNum++;
        }
        HSSFCellStyle hjStyle = wb.createCellStyle(); // 样式对象
        hjStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        hjStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        hjStyle.setFillForegroundColor(HSSFColor.GREY_40_PERCENT.index);
        hjStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        hjStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        hjStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        hjStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);

        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = row.createCell(1);
        cell.setCellValue("合计");
        cell.setCellStyle(hjStyle);
        cell = row.createCell(2);
        cell.setCellValue("");
        cell.setCellStyle(hjStyle);

        if(refundProduct.size()==0){
            HSSFDataFormat df = wb.createDataFormat();
            hjStyle.setDataFormat(df.getBuiltinFormat("0.00"));
            cell = row.createCell(3);
            cell.setCellValue(0);
            cell.setCellStyle(hjStyle);
            cell = row.createCell(4);
            cell.setCellValue(0);
            cell.setCellStyle(hjStyle);
        }else{
            HSSFDataFormat df = wb.createDataFormat();
            hjStyle.setDataFormat(df.getBuiltinFormat("0.00"));
            cell = row.createCell(3);
            String sc = CellReference.convertNumToColString(3);
            cell.setCellFormula("SUM(" + sc + (rowStartIndex+1) + ":"+sc+rowNum+")");
            cell.setCellStyle(hjStyle);
            cell = row.createCell(4);
            String pc = CellReference.convertNumToColString(4);
            cell.setCellFormula("SUM(" + pc + (rowStartIndex+1) + ":"+pc+rowNum+")");
            cell.setCellStyle(hjStyle);
        }

        return rowNum;
    }

    private void createreExcelHead(HSSFWorkbook wb,HSSFSheet sheet,int rowNum,String[] head){
        HSSFCellStyle style = wb.createCellStyle(); // 样式对象
        HSSFFont font = wb.createFont();// 生成一个字体
        font.setColor(HSSFColor.BLACK.index);// HSSFColor.VIOLET.index // 字体颜色
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);// 字体增粗
        style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        style.setFillForegroundColor(HSSFColor.GREY_40_PERCENT.index);
        style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        style.setBorderRight(HSSFCellStyle.BORDER_THIN);
        style.setBorderTop(HSSFCellStyle.BORDER_THIN);
        style.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);//垂直居中
        style.setFont(font);// 把字体应用到当前的样式
        //style.setFillBackgroundColor(HSSFColor.BLACK.index);
        HSSFRow row = sheet.createRow(rowNum);

        for(int i = 0 ; i < head.length; i++){
            HSSFCell cell = row.createCell(i+1);
            cell.setCellValue(head[i]);
            cell.setCellStyle(style);
        }
    }

    private void createrefundProductHead(HSSFWorkbook wb,HSSFSheet sheet,int rowNum){
        HSSFCellStyle style = wb.createCellStyle(); // 样式对象
        HSSFFont font = wb.createFont();// 生成一个字体
        font.setColor(HSSFColor.BLACK.index);// HSSFColor.VIOLET.index // 字体颜色
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);// 字体增粗
        style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        style.setFillForegroundColor(HSSFColor.GREY_40_PERCENT.index);
        style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        style.setBorderRight(HSSFCellStyle.BORDER_THIN);
        style.setBorderTop(HSSFCellStyle.BORDER_THIN);
        style.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);//垂直居中
        style.setFont(font);// 把字体应用到当前的样式
        //style.setFillBackgroundColor(HSSFColor.BLACK.index);
        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = row.createCell(1);
        cell.setCellValue("项目类型");
        cell.setCellStyle(style);
        cell = row.createCell(2);
        cell.setCellValue("名称");
        cell.setCellStyle(style);
        cell = row.createCell(3);
        cell.setCellValue("异常退款数量");
        cell.setCellStyle(style);
        cell = row.createCell(4);
        cell.setCellValue("异常退款金额");
        cell.setCellStyle(style);
    }


}