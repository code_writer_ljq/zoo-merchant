package com.zoo.zoomerchantadminweb;

import com.alibaba.fastjson.JSONObject;
import com.zoo.activity.service.DateTypeService;
import com.zoo.activity.util.DateTypeUtil;
import com.zoo.activity.constant.ResponseData;
import com.zoo.activity.core.Status;
import com.zoo.activity.dao.model.UserBean;
import com.zoo.activity.dao.model.ZooCustomer;
import com.zoo.activity.service.HuaxuezooService;
import com.zoo.web.bean.WebResult;
import com.zoo.wechat.common.Config;
import com.zoo.zoomerchantadminweb.base.BaseController;
import com.zoo.zoomerchantadminweb.context.WebUpmsContext;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

/**
 * Created by zhai on 15/10/28.
 * 工具API
 */
@Api(value = "/api/util", tags = {"工具类接口"}, description = "工具类接口")
@RestController
@RequestMapping("/api/util")
public class UtilController extends BaseController {

    private HuaxuezooService huaxuezooService;

    @Autowired
    private DateTypeService dateTypeService;

    private static final String BAIDU_APIKEY = "8fd5e109e8caaf5b4c69229eaafbbd79";

    @Autowired
    public void setHuaxuezooService(HuaxuezooService huaxuezooService) {
        this.huaxuezooService = huaxuezooService;
    }

    /**
     * 数据转移
     */
    @RequestMapping("/huaxuezoo/linkHuaxuezooUser.json")
    @ResponseBody
    public Object batchLinkHuaxuezooUser() {
        ResponseData responseData = new ResponseData();
        ZooCustomer zooCustomer = huaxuezooService.batchLinkHuaxueZooCustomer();
        responseData.setStatus(Status.success);
        responseData.setData(zooCustomer);
        responseData.setMsg("batchUpdateUser success!");
        return responseData;
    }

    /**
     * 导入用户unionid
     * http://localhost:8080/huaxuezoo/batchUpdateUser.json
     */
    @RequestMapping("/huaxuezoo/batchUpdateUser.json")
    @ResponseBody
    public Object batchUpdateUser() {

        ResponseData responseData = new ResponseData();
        huaxuezooService.batchUpdateUserFromMysql();
        responseData.setStatus(Status.success);
        responseData.setMsg("batchUpdateUser success!");
        return responseData;
    }

    /**
     * 导入用户openID
     */
    @RequestMapping("/huaxuezoo/upateAllOpenId.json")
    @ResponseBody
    public Object updateAllOpenId(@RequestParam(value = "next", required = false) String next) {
        ResponseData responseData = new ResponseData();

        UserBean zooCustomsAllOpenId = huaxuezooService.getZooCustomsAllOpenId(next);
        responseData.setStatus(Status.success);
        responseData.setData(zooCustomsAllOpenId);
        responseData.setMsg("check holidy success!");
        return responseData;
    }

    @RequestMapping("/huaxuezoo/updateUser.json")
    @ResponseBody
    public Object huaxuezooUpdateUser(@RequestParam(value = "next", required = false) String next) {
        ResponseData responseData = new ResponseData();

        if (next == null) {
            next = "";
        }

        UserBean userBean = huaxuezooService.updateZooCustomSync(next);
        responseData.setStatus(Status.success);
        responseData.setData(userBean);
        responseData.setMsg("check holidy success!");
        return responseData;
    }

    @RequestMapping("/util/holiday.json")
    @ResponseBody
    public Object getRegionListByParentId(@RequestParam("date") String date) {
        ResponseData responseData = new ResponseData();
        String httpUrl = "http://www.easybots.cn/api/holiday.php";
        BufferedReader reader;
        String result;
        StringBuffer sbf = new StringBuffer();
        Map<String, Object> resultMap;
        httpUrl = httpUrl + "?d=" + date;
        String response = "0";

        try {
            URL url = new URL(httpUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.connect();
            InputStream is = connection.getInputStream();
            reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            String strRead;
            while ((strRead = reader.readLine()) != null) {
                sbf.append(strRead);
            }
            reader.close();
            result = sbf.toString();
            resultMap = (Map) JSONObject.parse(result);
            Object resultObject = resultMap.get(date);
            if (resultObject instanceof String) {
                response = (String) resultMap.get(date);
            }
            if (resultObject instanceof Integer) {
                response = Integer.toString((Integer) resultMap.get(date));
            }
        } catch (Exception e) {
            responseData.setStatus(Status.error);
            responseData.setMsg("check holidy failed!");
            responseData.setData(response);
            // logger.error(AssUtils.parse(e));
        }
        responseData.setStatus(Status.success);
        responseData.setMsg("check holidy success!");
        responseData.setData(response);
        return responseData;
    }

    @RequestMapping("/huaxuezoo/getUserInfo.json")
    @ResponseBody
    public Object getHuaxuezooUserInfoByOpenId(@RequestParam(value = "openId", required = false) String openId) {
        ResponseData responseData = new ResponseData();
        ZooCustomer zooCustomer = huaxuezooService.getZooCustomerFromApiByOpenId(openId);
        if (zooCustomer != null) {
            responseData.setStatus(Status.success);
            responseData.setMsg("get user info success!");
            responseData.setData(zooCustomer);
        } else {
            responseData.setStatus(Status.error);
            responseData.setMsg("get user info failed!");
        }
        return responseData;
    }

    /**
     * 返回值
     * 0:工作日,平日
     * 1:休息日,周六周末
     * 2.节假日
     *
     * @param date
     * @return
     */
    @ApiOperation(value = "获取日期的种类", notes = "获取日期的种类", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "date", value = "日期时间（格式：21080828），返回值（返回值：0:工作日,平日  1:休息日,周六周末  2.节假日）", required = true, dataType = "string", paramType = "query")
    })
    @RequestMapping(value = "/new-holiday.json", method = RequestMethod.GET)
    public Object getStatusOfDate(@RequestParam("date") String date, HttpServletRequest request) {
//        String baiduApiDateUrl = "http://apis.baidu.com/xiaogg/holiday/holiday?d=" + date;
//        //让链接主动关闭,防止占用资源
//        HttpClient client = new HttpClient(new HttpClientParams(), new SimpleHttpConnectionManager(true));
//        HttpClientParams httpClientParams = client.getParams();
//        //设置重试的次数
//        httpClientParams.setParameter(HttpMethodParams.RETRY_HANDLER, new DefaultHttpMethodRetryHandler());
//        httpClientParams.setConnectionManagerTimeout(1000);
//        httpClientParams.setSoTimeout(1000);//设置超时时间
//        GetMethod get = null;
//        boolean retry = true;
//        try {
//            get = new GetMethod(baiduApiDateUrl);
//            get.addRequestHeader("apikey", BAIDU_APIKEY);
//            int statusCode = client.executeMethod(get);
//            if (statusCode == HttpStatus.SC_OK) {
//                System.out.println("OK");
//                String status = get.getResponseBodyAsString();
//                if(!"0".equals(status) &&!"1".equals(status) &&!"2".equals(status)){
//                    data.setStatus(Status.error);
//                    data.setMsg(status);
//                }else{
//                data.setStatus(Status.success);
//                data.setData(status);
//                }
//            } else {
//                //获取失败,重试一次.
//                if (retry) {
//                    retry = !retry;
//                    data = getStatusOfDate(date);
//                } else {
//                    data.setStatus(Status.error);
//                    data.setMsg("baiduApi execute error");
//                }
//            }
//        } catch (IOException e) {
//            System.out.println("ERROR2");
//            if (retry) {
//                data = getStatusOfDate(date);
//                retry = !retry;
//            } else {
//                data.setStatus(Status.error);
//                data.setMsg("baiduApi execute io-error");
//            }
//        } finally {
//            if (get != null) get.releaseConnection();
//        }
//
//        boolean baiduExecuteSuccess = true;
//        if (Status.error.equals(data.getStatus())) {
//            //如果调用baiduApid接口两次后还是失败,那么调用本地节假日方法
//            baiduExecuteSuccess = false;
//            data.setStatus(Status.success);
//            data.setData(getDateType(date));
//        }
//        //此处已经判断出了date的类型,但由于百度接口数据有错误,需要再次调用本地检验匹配
//        //如果百度调用成功,才校验,调用失败说明已经调用过本地校验,无需再次调用
//        String baiduResDate = data.getData() + "";
//        if(!"2".equals(baiduResDate)) {
        //如果是平日或周六周日,就需要再次校验.
        String localDate = dateTypeService.getDateType(date, WebUpmsContext.getOrgnization(request).getOrganizerId());
        return WebResult.getSuccessResult("data", localDate);
//        }
    }

    /**
     * 获取 本地地址
     *
     * @return
     */
    @RequestMapping("/util/getDomain.json")
    @ResponseBody
    public Object getDomain() {
        ResponseData responseData = new ResponseData();

        try {
            responseData.setData(Config.instance().getDomain());
            responseData.setStatus(Status.success);
        } catch (Exception e) {
            responseData.setStatus(Status.error);
            responseData.setMsg("获取信息失败");
        }

        return responseData;
    }

}
