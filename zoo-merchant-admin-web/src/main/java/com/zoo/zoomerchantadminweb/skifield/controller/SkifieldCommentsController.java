package com.zoo.zoomerchantadminweb.skifield.controller;

import com.zoo.common.page.PageList;
import com.zoo.activity.dao.nutz.pojo.CommonAccount;
import com.zoo.skifield.dao.model.Skifield;
import com.zoo.skifield.service.SkiFieldService;
import com.zoo.skifield.service.SkifieldCommentService;
import com.zoo.web.bean.WebResult;
import com.zoo.zoomerchantadminweb.context.WebUpmsContext;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@Api(value = "api/server/skifield/comments", tags = {"主办方场地评论接口"}, description = "主办方场地评论接口描述")
@RestController
@RequestMapping("api/server/skifield/comments")
public class SkifieldCommentsController {

    @Autowired
    private SkiFieldService skiFieldService;

    @Autowired
    private SkifieldCommentService skifieldCommentService;

    @ApiOperation(value = "获取雪场评论", notes = "获取雪场评论列表", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "size", value = "条", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "valid", value = "是否有效", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "name", value = "名称", required = false, dataType = "string", paramType = "query"),
    })
    @RequestMapping(value = "/list.json")
    public WebResult getCommentsList(@RequestParam(value = "size", required = false, defaultValue = "10") Integer size,
                                     @RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
                                     @RequestParam(value = "valid", required = false, defaultValue = "1") Integer valid,
                                     @RequestParam(value = "name", required = false) String name, HttpServletRequest request) {
        WebResult webResult = WebResult.getSuccessResult();

        CommonAccount account = WebUpmsContext.getAccount(request);

        if (null == account) {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }

        try {
            Integer skiFieldId = null;
            // 根据举办方Id查找雪场
            Skifield skifield = skiFieldService.getSkiFieldByOrganizerId(account.getOrganizerId());
            if (skifield == null || skifield.getId() == null) {
                webResult.putResult("data", new PageList());
                return webResult;
            }
            skiFieldId = skifield.getId();

            /* 获取雪场评论信息 */
            PageList pageList = skifieldCommentService.skifieldCommentList2(page, size, valid, name, skiFieldId + "");
            webResult.putResult("data", pageList);
        } catch (Exception e) {
            e.printStackTrace();
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }
        return webResult;
    }

    @ApiOperation(value = "删除", notes = "删除评论/照片", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "id", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "type", value = "种类", required = true, dataType = "string", paramType = "query")
    })
    @RequestMapping(value = "/delete.json")
    public WebResult getCommentsList(@RequestParam(value = "id") String id,
                                     @RequestParam(value = "type") String type, HttpServletRequest request) {
        WebResult webResult = WebResult.getSuccessResult();

        CommonAccount account = WebUpmsContext.getAccount(request);

        if (null == account) {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }

        if (type.equals("deletePhoto")) {
            skifieldCommentService.deletePhoto(id);
        } else {
            skifieldCommentService.deleteComment(id);
        }
        return webResult;
    }

}
