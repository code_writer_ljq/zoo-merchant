package com.zoo.zoomerchantadminweb.skifield.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.zoo.activity.constant.ResponseData;
import com.zoo.activity.core.Status;
import com.zoo.activity.dao.model.Organizer;
import com.zoo.activity.dao.model.OrganizerWithScenic;
import com.zoo.activity.service.OrganizerService;
import com.zoo.activity.util.BaiduApiUtil;
import com.zoo.activity.util.HtmlUtil;
import com.zoo.skifield.dao.model.*;
import com.zoo.skifield.service.CityService;
import com.zoo.skifield.service.CountryService;
import com.zoo.skifield.service.SkiFieldService;
import com.zoo.skifield.service.TagService;
import com.zoo.web.bean.WebResult;
import com.zoo.zoomerchantadminweb.context.WebUpmsContext;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.*;

@Api(value = "api/server/skifield", tags = {"主办方场地接口"}, description = "主办方场地接口描述")
@RestController
@RequestMapping("api/server/skifield")
public class SkifieldController {

    @Resource(name = "skiFieldService")
    SkiFieldService skiFieldService;

    @Resource(name = "cityService")
    CityService cityService;

    @Resource(name = "countryService")
    CountryService countryService;

    @Resource(name = "tagService")
    TagService tagService;


    /**
     * 获取全部国家简单信息 select使用
     */
    @ApiOperation(value = "获取全部国家简单信息", notes = "获取全部国家简单信息 select使用", httpMethod = "GET")
    @ApiImplicitParams({})
    @RequestMapping(value = "/getCountryList.json")
    public WebResult getSimpleCountryList() {
        List<Map> countryList = null;
        try {
            countryList = skiFieldService.getSimpleCountry();
        } catch (Exception e) {
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }
        return WebResult.getSuccessResult("data", countryList);
    }

    /**
     * 根据国家id 查询所有城市List
     *
     * @param countryId
     * @return
     */
    @ApiOperation(value = "根据国家id查询所有城市List", notes = "根据国家id 查询所有城市List select使用", httpMethod = "GET")
    @ApiImplicitParams({})
    @RequestMapping(value = "/getCityListByCountryId.json")
    public WebResult getSimpleCityListByCountryId(Integer countryId) {

        if (countryId == null || "".equals(countryId)) {
            countryId = 1;// 如果国家为空，默认是中国
        }
        List<Map> countryList = null;
        try {
            countryList = skiFieldService.getSimpleCityListByCountryId(countryId);
        } catch (Exception e) {
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }
        return WebResult.getSuccessResult("data", countryList);
    }


    /**
     * 根据主办方id 获取雪场信息
     */
    @ApiOperation(value = "获取雪场信息", notes = "根据主办方id 获取雪场信息", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "organizerId", value = "主办方ID", required = true, dataType = "integer", paramType = "query"),
    })
    @RequestMapping(value = "/getInfo.json")
    public WebResult getSkiFieldInfoByOrganizerId(Integer organizerId, HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null && organizerId == null) {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
        if (organizer != null && organizerId == null) {
            organizerId = organizer.getOrganizerId();
        }

        Skifield skifield = null;
        List<SkifieldPictureGroup> skifieldPictureGroupList = null;
        List<Map<String, Object>> fileInfos = null;
        Map<String, Object> respData = new HashMap<>();
        try {
            skifield = skiFieldService.getSkiFieldByOrganizerId(organizerId);
            respData.put("skifield", skifield);
            if (null == skifield) {
                respData.put("groups", null);
                respData.put("fileInfos", null);
                return WebResult.getSuccessResult("data", respData);
            }
            skifieldPictureGroupList = skiFieldService.getGroupsWithPicInfoBySkifieldID(skifield.getId());
            respData.put("groups", skifieldPictureGroupList);
            if (skifieldPictureGroupList.size() <= 0) {
                fileInfos = skiFieldService.getSkifieldPic(organizerId);
            }
            respData.put("fileInfos", fileInfos);
        } catch (Exception e) {
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }

        return WebResult.getSuccessResult("data", respData);
    }

    /**
     * 调用百度地图api接口查询坐标
     *
     * @param addr
     * @return
     */
    @ApiOperation(value = "查询坐标", notes = "调用百度地图api接口查询坐标", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "addr", value = "地址", required = true, dataType = "string", paramType = "query"),
    })
    @RequestMapping(value = "/getLngAndLat.json", method = RequestMethod.POST)
    public WebResult getLngAndLat(@RequestParam("addr") String addr) {
        Map resMap = null;
        try {
            resMap = BaiduApiUtil.getLngAndLat(addr);
        } catch (Exception e) {
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }
        return WebResult.getSuccessResult("data", resMap);
    }

    /**
     * 获取雪场后台的标签list select多选用
     *
     * @return
     */
    @ApiOperation(value = "获取标签list", notes = "获取雪场后台的标签list select多选用", httpMethod = "GET")
    @ApiImplicitParams({})
    @RequestMapping(value = "/getSkiFieldTagList.json", method = RequestMethod.GET)
    public WebResult getSkiFieldTagList() {

        List<Tag> res = null;
        try {
            res = tagService.tagList();
        } catch (Exception e) {
            e.printStackTrace();
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }
        return WebResult.getSuccessResult("data", res);
    }


    /**
     * 保存雪场信息(包括基本信息、雪道信息、住宿食宿、雪场详情、图片视频)
     *
     * @return
     */
    @ApiOperation(value = "保存雪场信息", notes = "保存雪场信息(包括基本信息、雪道信息、住宿食宿、雪场详情、图片视频)", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "name", value = "名称", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "countryId", value = "国家", required = true, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "city", value = "城市", required = true, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "mapx", value = "经度", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "mapy", value = "纬度", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "tel1", value = "电话", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "weblink", value = "网址", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "address", value = "地址", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "summary", value = "简介", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "skiType", value = "适合用板", required = true, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "primaryroad", value = "初级雪道", required = true, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "middleroad", value = "中级雪道", required = true, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "seniorroad", value = "高级雪道", required = true, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "suggestList", value = "推荐理由", required = false, dataType = "list", paramType = "query"),
            @ApiImplicitParam(name = "tags", value = "标签", required = false, dataType = "list", paramType = "query"),
            @ApiImplicitParam(name = "roadId", value = "雪道信息参数", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "roadPicUrl", value = "雪道信息参数", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "roadNote", value = "雪道信息参数", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "foodInfo", value = "住宿食宿参数", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "introduceHtml", value = "雪场详情参数", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "groupList", value = "分组", required = true, dataType = "string", paramType = "query"),

    })
    @RequestMapping(value = "/save.json", method = RequestMethod.POST)
    public WebResult skiInfoSave(// 基本信息参数
                                 @RequestParam(value = "name", required = false) String name,
                                 @RequestParam(value = "countryId", required = false) Integer countryId,
                                 @RequestParam(value = "city", required = false) Integer city,
                                 @RequestParam(value = "mapx", required = false) String mapx,
                                 @RequestParam(value = "mapy", required = false) String mapy,
                                 @RequestParam(value = "tel1", required = false) String tel1,
                                 @RequestParam(value = "weblink", required = false) String weblink,
                                 @RequestParam(value = "address", required = false) String address,
                                 @RequestParam(value = "summary", required = false) String summary,
                                 @RequestParam(value = "skiType", required = false) Integer skiType,
                                 @RequestParam(value = "primaryroad", required = false) Integer primaryroad,
                                 @RequestParam(value = "middleroad", required = false) Integer middleroad,
                                 @RequestParam(value = "seniorroad", required = false) Integer seniorroad,
                                 @RequestParam(value = "suggestList", required = false) List<String> suggestList,
                                 @RequestParam(value = "tags", required = false) List<Integer> tags,
                                 // 雪道信息参数
                                 @RequestParam(value = "roadId", required = false) String roadId,
                                 @RequestParam(value = "roadPicUrl", required = false) String roadPicUrl,
                                 @RequestParam(value = "roadNote", required = false) String roadNote,
                                 // 住宿食宿参数
                                 @RequestParam(value = "foodInfo", required = false) String foodInfo,
                                 // 雪场详情参数
                                 @RequestParam(value = "introduceHtml", required = false) String introduceHtml,
                                 @RequestParam(value = "groupList", required = false) String groupList, HttpServletRequest request) {
        ResponseData responseData = new ResponseData();
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null) {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }

        try {
            // 保存 雪场基本信息
            ResponseData resDataBase = (ResponseData) skiBaseInfoSave(name, countryId, city, organizer.getLatitude(), organizer.getLongitude(), tel1, weblink, address,
                    summary, skiType, primaryroad, middleroad, seniorroad, suggestList, tags, organizer);
            if (!resDataBase.getStatus().equals(Status.success)) {
                responseData.setMsg(resDataBase.getMsg());
                responseData.setStatus(resDataBase.getStatus());
                return WebResult.getErrorResult(WebResult.Code.ERROR);
            }

            Skifield skifield = skiFieldService.getSkiFieldByOrganizerId(organizer.getOrganizerId());
            if (skifield == null || skifield.getId() == null) {
                return WebResult.getErrorResult(WebResult.Code.ERROR);
            }

            // 保存 雪道信息
            ResponseData resDataRoad = (ResponseData) skiRoadInfoSave(roadId, roadPicUrl, roadNote, skifield);
            if (!resDataBase.getStatus().equals(Status.success)) {
                responseData.setMsg(resDataRoad.getMsg());
                responseData.setStatus(resDataRoad.getStatus());
                return WebResult.getErrorResult(WebResult.Code.ERROR);
            }

            // 保存 住宿食宿信息
            ResponseData resFoodDetail = (ResponseData) skiFoodSave(foodInfo, skifield);
            if (!resDataBase.getStatus().equals(Status.success)) {
                responseData.setMsg(resFoodDetail.getMsg());
                responseData.setStatus(resFoodDetail.getStatus());
                return WebResult.getErrorResult(WebResult.Code.ERROR);
            }

            // 保存 雪场详情
            ResponseData resDataDetail = (ResponseData) skiDetailSave(introduceHtml, skifield);
            if (!resDataBase.getStatus().equals(Status.success)) {
                responseData.setMsg(resDataDetail.getMsg());
                responseData.setStatus(resDataDetail.getStatus());
                return WebResult.getErrorResult(WebResult.Code.ERROR);
            }

            List<SkifieldPictureGroup> groupsWithFile = groupStringToList(groupList, skifield.getId());
            if (!skiFieldService.saveSkifieldGroups(groupsWithFile, skifield.getId())) {
                return WebResult.getErrorResult(WebResult.Code.ERROR);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }

        return WebResult.getSuccessResult();
    }

    /**
     * 保存雪场基本信息(此基本信息包括 基本表信息、推荐理由、标签)
     *
     * @return
     */
    private Object skiBaseInfoSave(String name, Integer countryId, Integer city, String mapx, String mapy,
                                   String tel1, String weblink, String address, String summary,
                                   Integer skiType, Integer primaryroad, Integer middleroad, Integer seniorroad, List<String> suggestList,
                                   List<Integer> tags,
                                   Organizer organizer) {
        ResponseData responseData = new ResponseData();
        Integer organizerId = organizer.getOrganizerId();

        /* 进行修改操作之前的的雪场原数据 */
        Skifield oldskifield = null;
        // 雪场基本信息数据
        Skifield skifieldForUpdate = new Skifield();
        skifieldForUpdate.setName(name);
        skifieldForUpdate.setCountryId(countryId);
        skifieldForUpdate.setCity(city);
        skifieldForUpdate.setMapx(mapx);
        skifieldForUpdate.setMapy(mapy);
//        skifieldForUpdate.setScore(score);
        skifieldForUpdate.setScore(5.0);
        skifieldForUpdate.setTel1(tel1);
        skifieldForUpdate.setWeblink(weblink);
        skifieldForUpdate.setAddress(address);
        skifieldForUpdate.setSummary(summary);
        skifieldForUpdate.setSkiType(skiType);
        skifieldForUpdate.setPrimaryroad(primaryroad);
        skifieldForUpdate.setMiddleroad(middleroad);
        skifieldForUpdate.setSeniorroad(seniorroad);
        skifieldForUpdate.setUpdatetime(new Date());
        skifieldForUpdate.setOrganizerId(organizerId);
        try {
            // 根据举办方Id查找雪场id
            Integer skiFieldId = null;
            Skifield skifield = skiFieldService.getSkiFieldByOrganizerId(organizerId);
            /*雪场基本信息*/
            if (skifield == null || skifield.getId() == null) {// 进行新增雪场操作
                skiFieldId = skiFieldService.getNewSkiId();
                skifieldForUpdate.setId(skiFieldId);
                if (!skiFieldService.saveSkiBaseInfo(skifieldForUpdate)) {
                    responseData.setStatus(Status.error);
                    responseData.setMsg("新增“基本信息”失败!");
                    return responseData;
                }
            } else { // 更新操作
                skiFieldId = skifield.getId();
                oldskifield = skifield;
                if (!skiFieldService.updateSkiFieldByOrgaizerId(skifieldForUpdate)) {
                    responseData.setStatus(Status.error);
                    responseData.setMsg("修改“基本信息”失败!");
                    return responseData;
                }
            }

            /*保存推荐理由*/
            if (suggestList != null && suggestList.size() > 0) {
                List<String> sugguestList = new ArrayList<String>();
                for (int i = 0; i < suggestList.size(); i++) {
                    String[] tmpStr = suggestList.get(i).replace("，", ",").replace("；", ",").replace(";", ",").split(",");
                    Collections.addAll(sugguestList, tmpStr);
                }

                if (!skiFieldService.saveSkiSuggest(sugguestList, skiFieldId)) {
                    responseData.setStatus(Status.error);
                    responseData.setMsg("修改“基本信息”失败!");
                    return responseData;
                }
            }

            /*保存标签*/
            if (tags != null) {
                if (!skiFieldService.saveSkiTagList(tags, skiFieldId)) {
                    responseData.setStatus(Status.error);
                    responseData.setMsg("修改“基本信息”失败!");
                    return responseData;
                }
            }

            /*bug fix*/
            if (skifield != null && skifield.getCountryId() != null) {
                countryService.setCountryValid(skifield.getCountryId());
                /*  bug fix 解决雪场修改国家导致数据错误的问题  start */
                if (oldskifield != null) {
                    countryService.setCountryValid(oldskifield.getCountryId());
                }
                /*  bug fix 解决雪场修改国家导致数据错误的问题  end */
            }
            if (skifield != null && skifield.getCity() != null) {
                cityService.setCityValid(skifield.getCity());
                /* bug fix 解决雪场修改城市导致数据错误的问题   start */
                if (oldskifield != null) {
                    cityService.setCityValid(oldskifield.getCity());
                }
                /* bug fix 解决雪场修改城市导致数据错误的问题  end */
            }

        } catch (Exception e) {
            e.printStackTrace();
            responseData.setMsg("保存雪场基本信息出错");
            responseData.setStatus(Status.error);
            return responseData;
        }

        responseData.setData(null);
        responseData.setMsg("修改基本信息成功");
        responseData.setStatus(Status.success);
        return responseData;
    }


    private Object skiRoadInfoSave(String roadId, String roadPicUrl, String roadNote,
                                   Skifield skifield) {
        ResponseData responseData = new ResponseData();

        String msg = "修改雪道信息失败";
        String status = Status.error;
        try {
            SkiFieldRoad skiFieldRoad = new SkiFieldRoad();
            skiFieldRoad.setRoadid(roadId);
            skiFieldRoad.setSkiid(skifield.getId());
            skiFieldRoad.setRoadnote(roadNote);
            skiFieldRoad.setRoadpicture(roadPicUrl);
            if (skiFieldService.saveSkiRoadInfo(skiFieldRoad)) {
                msg = "修改雪道信息成功";
                status = Status.success;
            }
        } catch (Exception e) {
            e.printStackTrace();
            responseData.setMsg("保存雪道信息出错");
            responseData.setStatus(Status.error);
            return responseData;
        }
        responseData.setMsg(msg);
        responseData.setStatus(status);
        return responseData;
    }

    /**
     * 保存食宿信息
     *
     * @return
     */
    private Object skiFoodSave(String foodInfo, Skifield skifield) {
        ResponseData responseData = new ResponseData();

        String msg = "修改雪场食宿信息失败";
        String status = Status.error;
        try {
            List<SkiFieldFood> skiFieldFoodList = foodInfoStringToList(foodInfo, skifield.getId());
            if (skiFieldFoodList.size() == 0) {
                skiFieldService.deleteSkiFoodBySkiId(skifield.getId());
                responseData.setMsg("提交数据为空,执行删除操作");
                responseData.setStatus(Status.success);
                return responseData;
            }
            if (skiFieldService.saveSkiFoodInfo(skiFieldFoodList)) {
                msg = "修改雪场食宿信息成功";
                status = Status.success;
            }
        } catch (Exception e) {
            e.printStackTrace();
            responseData.setMsg("保存信息出错");
            responseData.setStatus(Status.error);
            return responseData;
        }

        responseData.setMsg(msg);
        responseData.setStatus(status);
        return responseData;
    }

    /**
     * 字符串通过json转换成list对象
     *
     * @return
     */
    private List<SkiFieldFood> foodInfoStringToList(String foodInfo, Integer skiFieldId) {
        JSONArray foodInfoJsonArry = JSON.parseArray(foodInfo);
        List<SkiFieldFood> skiFieldFoodList = new ArrayList<>();
        for (int i = 0; i < foodInfoJsonArry.size(); i++) {
            Map<String, Object> mapJson = (Map<String, Object>) JSON.parse(foodInfoJsonArry.get(i).toString());
            SkiFieldFood skiFieldFood = new SkiFieldFood();
            if (mapJson.containsKey("skiid") && mapJson.get("skiid") != null) {
                skiFieldFood.setSkiid(Integer.valueOf(mapJson.get("skiid").toString()));
            }
            if (mapJson.containsKey("foodid") && mapJson.get("foodid") != null) {
                skiFieldFood.setFoodid(mapJson.get("foodid").toString());
            }
            if (mapJson.containsKey("picture") && mapJson.get("picture") != null) {
                skiFieldFood.setPicture(mapJson.get("picture").toString());
            }
            if (mapJson.containsKey("description") && mapJson.get("description") != null) {
                skiFieldFood.setDescription(mapJson.get("description").toString());
            }
            if (mapJson.containsKey("level") && mapJson.get("level") != null) {
                skiFieldFood.setLevel(Integer.valueOf(mapJson.get("level").toString()));
            }
            skiFieldFood.setSkiid(skiFieldId);
            skiFieldFoodList.add(skiFieldFood);
        }
        return skiFieldFoodList;
    }

    /**
     * 保存雪场详情信息（富文本）
     *
     * @return
     */
    private Object skiDetailSave(String introduceHtml, Skifield skifield) {
        ResponseData responseData = new ResponseData();

        String msg = "修改雪场详情失败";
        String status = Status.error;
        try {
            Skifield skifieldForSave = new Skifield();
            skifieldForSave.setId(skifield.getId());
            skifieldForSave.setIntroduceDetail(HtmlUtil.Html2Text(introduceHtml));
            skifieldForSave.setIntroduceHtml(introduceHtml);
            if (skiFieldService.updateDetailInfo(skifieldForSave)) {
                msg = "修改雪场详情成功";
                status = Status.success;
            }
        } catch (Exception e) {
            e.printStackTrace();
            responseData.setMsg("保存雪场详情信息出错");
            responseData.setStatus(Status.error);
            return responseData;
        }

        responseData.setMsg(msg);
        responseData.setStatus(status);
        return responseData;
    }

    /**
     * 图片分组字符串信息转换成 list
     *
     * @return
     */
    private List<SkifieldPictureGroup> groupStringToList(String groupList, Integer skiFieldId) {
        List<SkifieldPictureGroup> groupsWithFile = new ArrayList<>();
        JSONArray groupJsonArry = JSON.parseArray(groupList);
        for (int i = 0; i < groupJsonArry.size(); i++) {
            Map<String, Object> mapJson = (Map<String, Object>) JSON.parse(groupJsonArry.get(i).toString());
            SkifieldPictureGroup skifieldPictureGroup = new SkifieldPictureGroup();
            if (mapJson.containsKey("groupName") && mapJson.get("groupName") != null) {
                skifieldPictureGroup.setGroupName(mapJson.get("groupName").toString());
            }
            if (mapJson.containsKey("id") && mapJson.get("id") != null) {
                skifieldPictureGroup.setId(Integer.valueOf(mapJson.get("id").toString()));
            }
            if (mapJson.containsKey("createTime") && mapJson.get("createTime") != null) {
                skifieldPictureGroup.setCreateTime(new Date(Long.valueOf(mapJson.get("createTime").toString())));
            }
            if (mapJson.containsKey("updateTime") && mapJson.get("updateTime") != null) {
                skifieldPictureGroup.setUpdateTime(new Date(Long.valueOf(mapJson.get("updateTime").toString())));
            }
            if (mapJson.containsKey("pictureList") && mapJson.get("pictureList") != null) {
                JSONArray pictureJsonArry = JSON.parseArray(mapJson.get("pictureList").toString());
                List<SkifieldPicture> pictureList = new ArrayList<>();
                for (int j = 0; j < pictureJsonArry.size(); j++) {
                    Map<String, Object> mapJsonPic = (Map<String, Object>) JSON.parse(pictureJsonArry.get(j).toString());
                    SkifieldPicture skifieldPicture = new SkifieldPicture();
                    if (mapJsonPic.containsKey("path") && mapJsonPic.get("path") != null) {
                        skifieldPicture.setPath(mapJsonPic.get("path").toString());
                    }
                    if (mapJsonPic.containsKey("videoUrl") && mapJsonPic.get("videoUrl") != null) {
                        skifieldPicture.setVideoUrl(mapJsonPic.get("videoUrl").toString());
                    }
                    if (mapJsonPic.containsKey("pictureid") && mapJsonPic.get("pictureid") != null) {
                        skifieldPicture.setPictureid(mapJsonPic.get("pictureid").toString());
                    }
                    if (skifieldPictureGroup.getId() != null) {
                        skifieldPicture.setGroupId(skifieldPictureGroup.getId());
                    }
                    skifieldPicture.setLevel(j + 1);
                    skifieldPicture.setSkifieldid(skiFieldId);
                    pictureList.add(skifieldPicture);
                }
                skifieldPictureGroup.setPictureList(pictureList);
            }
            skifieldPictureGroup.setLevel(i + 1);
            skifieldPictureGroup.setSkifieldId(skiFieldId);
            groupsWithFile.add(skifieldPictureGroup);
        }

        return groupsWithFile;
    }


}
