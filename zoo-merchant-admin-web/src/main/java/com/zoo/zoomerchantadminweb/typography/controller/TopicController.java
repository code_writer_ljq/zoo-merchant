package com.zoo.zoomerchantadminweb.typography.controller;

import com.alibaba.fastjson.JSON;

import com.zoo.common.page.PageList;
import com.zoo.activity.dao.model.Organizer;
import com.zoo.activity.dao.model.Topic;
import com.zoo.activity.dao.model.TopicTarget;
import com.zoo.activity.service.TopicService;
import com.zoo.activity.vo.TopicModel;
import com.zoo.util.data.BeanUtilsExtends;
import com.zoo.web.bean.WebResult;
import com.zoo.zoomerchantadminweb.context.WebUpmsContext;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Api(value = "api/server/topic", tags = {"主办方专题接口"}, description = "主办方专题接口描述")
@RestController
@RequestMapping("api/server/topic")
public class TopicController {

    @Autowired
    private TopicService topicService;

    @ApiOperation(value = "获取主办方专题列表", notes = "获取主办方专题列表", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "size", value = "分页大小", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "title", value = "标题", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "valid", value = "是否有效", required = false, dataType = "int", paramType = "query")
    })
    @RequestMapping(value = "/list.json")
    public WebResult getTopicList(@RequestParam(value = "title", required = false) String title,
                                  @RequestParam(value = "valid", required = false) Integer valid,
                                  @RequestParam(required = false, defaultValue = "1") Integer page,
                                  @RequestParam(required = false, defaultValue = "10") Integer size,
                                  HttpServletRequest request) throws Exception {

        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (null != organizer) {
            PageList articleList = topicService.getTopicWithoutBlob(organizer.getOrganizerId(), title, page, size, valid);
            return WebResult.getSuccessResult("data", articleList);
        } else {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
    }

    @ApiOperation(value = "是否有效", notes = "是否有效", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "topicId", value = "专题ID", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "valid", value = "0否 1是", required = true, dataType = "int", paramType = "query"),
    })
    @RequestMapping(value = "/valid.json")
    public Object getTopicList(@RequestParam("topicId") Integer topicId,
                               @RequestParam("valid") Integer valid,
                               HttpServletRequest request) throws Exception {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (null != organizer) {
            Organizer topicOrganizer = topicService.findTopicOwnOrgName(topicId);
            if (topicOrganizer != null && topicOrganizer.getOrganizerId() != null && topicOrganizer.getOrganizerId().equals(organizer.getOrganizerId())) {
                return WebResult.getSuccessResult("data", topicService.updateTopicValid(topicId, valid));
            } else {
                return WebResult.getErrorResult(WebResult.Code.NO_PERMISSION);
            }
        } else {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
    }

    @ApiOperation(value = "是否显示标题", notes = "是否显示标题", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "topicId", value = "专题ID", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "titleCtrl", value = "0否 1是", required = true, dataType = "int", paramType = "query"),
    })
    @RequestMapping(value = "/titleCtrl.json")
    public Object updateTopicCtrl(@RequestParam("topicId") Integer topicId,
                                  @RequestParam("titleCtrl") Integer titleCtrl,
                                  HttpServletRequest request) throws Exception {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (null != organizer) {
            Organizer topicOrganizer = topicService.findTopicOwnOrgName(topicId);
            if (topicOrganizer != null && topicOrganizer.getOrganizerId() != null && topicOrganizer.getOrganizerId().equals(organizer.getOrganizerId())) {
                return WebResult.getSuccessResult("data", topicService.updateTopicTitleCtrl(topicId, titleCtrl));
            } else {
                return WebResult.getErrorResult(WebResult.Code.NO_PERMISSION);
            }
        } else {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }

    }

    /**
     * 保存topic
     *
     * @param topicModel
     * @param articles   创建topic时关联的资讯
     * @param activities 创建topic时关联的活动
     * @param request
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "保存专题", notes = "保存专题", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "articles", value = "创建topic时关联的资讯", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "activities", value = "创建topic时关联的活动", required = false, dataType = "string", paramType = "query"),
    })
    @RequestMapping(value = "/save.json", method = RequestMethod.POST)
    public WebResult doTopicSave(@Valid @ModelAttribute TopicModel topicModel,
                                 @RequestParam(value = "articles") String articles,
                                 @RequestParam(value = "activities") String activities,
                                 HttpServletRequest request) throws Exception {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer != null) {
            Topic topic = new Topic();
            BeanUtilsExtends.copyProperties(topic, topicModel);
            List<TopicTarget> articleTargetModels = JSON.parseArray(articles, TopicTarget.class);
            List<TopicTarget> activityTargetModels = JSON.parseArray(activities, TopicTarget.class);
            if (topicModel.getTopicId() != null) {
                topic.setOrganizerId(null);
                topic.setValid(null);
                topic.setTopicType(null);
                return WebResult.getSuccessResult("data", topicService.saveTopic(topic, articleTargetModels, activityTargetModels));
            } else {
                topic.setTopicId(null);
                topic.setCreateTime(new Date());
                topic.setTopicType(0);
                topic.setValid(1);
                topic.setOrganizerId(organizer.getOrganizerId());
                return WebResult.getSuccessResult("data", topicService.saveTopic(topic, articleTargetModels, activityTargetModels));
            }
        } else {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
    }

    @ApiOperation(value = "专题详情", notes = "专题详情", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "topicId", value = "专题ID", required = true, dataType = "int", paramType = "query"),
    })
    @RequestMapping(value = "/detail.json")
    public Object getTopicDetail(@RequestParam("topicId") Integer topicId,
                                 HttpServletRequest request) throws Exception {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (null != organizer) {
            Topic topic = topicService.getTopicById(topicId);
            List<TopicTarget> topicTargets = topicService.findTopicTargetWithSort(topicId);
            Map<String, Object> resultMap = new HashMap<>();
            resultMap.put("topic", topic);
            resultMap.put("topicTargets", topicTargets);
            resultMap.put("topicJson", JSON.toJSONString(topic));
            resultMap.put("topicTargetsJson", JSON.toJSONString(topicTargets));
            return WebResult.getSuccessResult("data", resultMap);
        } else {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
    }

    /**
     * 处理添加topicTarget请求
     *
     * @param topicId    关联topic的id
     * @param articles   关联的资讯
     * @param activities 关联的活动
     */
    @ApiOperation(value = "处理添加topicTarget请求", notes = "处理添加topicTarget请求", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "topicId", value = "专题ID", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "articles", value = "创建topic时关联的资讯", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "activities", value = "创建topic时关联的活动", required = true, dataType = "string", paramType = "query"),
    })
    @RequestMapping(value = "/addTarget.json")
    public void doTopicTargetAdd(@RequestParam(value = "topicId") Integer topicId,
                                 @RequestParam(value = "articles") String articles,
                                 @RequestParam(value = "activities") String activities) {
        this.topicTargetAdd(topicId, articles, 1);
        this.topicTargetAdd(topicId, activities, 0);
    }

    /**
     * 添加topicTarget
     *
     * @param topic_id 关联topic的id
     * @param target   关联内容
     * @param type     关联类型 1 资讯 0 活动
     */
    private void topicTargetAdd(Integer topic_id, String target, Integer type) {
        //若无数据,target字符串为:"[]",即此时长度是2
        if (target.length() > 2) {
            String[] targetStr = target.substring(1, target.length() - 1).split(",");
            for (int i = 0; i < targetStr.length; i++) {
                TopicTarget topicTarget = new TopicTarget();
                topicTarget.setTopicId(topic_id);
                topicTarget.setTargetType(type);
                topicTarget.setTargetId(Integer.valueOf(targetStr[i]));
                topicService.addTopicTarget(topicTarget);
            }
        }
    }
}
