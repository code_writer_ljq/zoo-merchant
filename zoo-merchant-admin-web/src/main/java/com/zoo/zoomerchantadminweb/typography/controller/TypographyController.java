package com.zoo.zoomerchantadminweb.typography.controller;

import com.zoo.activity.dao.model.Organizer;
import com.zoo.activity.dao.model.Typography;
import com.zoo.activity.exception.TransParamToModelException;
import com.zoo.activity.service.RedisService;
import com.zoo.activity.service.TypographyService;
import com.zoo.activity.vo.TypographyModel;
import com.zoo.util.data.BeanUtilsExtends;
import com.zoo.web.bean.WebResult;
import com.zoo.zoomerchantadminweb.context.WebUpmsContext;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import java.lang.reflect.InvocationTargetException;

import static com.zoo.activity.util.AssUtils.copyProperties;

@Api(value = "api/server/typography", tags = {"主办方微信页面管理接口"}, description = "主办方微信页面管理接口描述")
@RestController
@RequestMapping("api/server/typography")
public class TypographyController {

    @Resource(name = "typographyService")
    private TypographyService typographyService;

    @Resource(name = "redisService")
    private RedisService redisService;

    @ApiOperation(value = "获取页面位置", notes = "获取页面位置 select使用", httpMethod = "GET")
    @ApiImplicitParams({})
    @RequestMapping(value = "/typographyType/list.json", method = RequestMethod.GET)
    public WebResult getTypographyTypeList() {
        try {
            return WebResult.getSuccessResult("data", typographyService.getTypographyTypeList(0));
        } catch (Exception e) {
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }
    }

    @ApiOperation(value = "获取页面设置列表", notes = "根据页面位置 获取列表", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "size", value = "条", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "typeId", value = "位置ID", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "title", value = "标题", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "targetType", value = "关联类型", required = false, dataType = "int", paramType = "query")
    })
    @RequestMapping(value = "/list.json", method = RequestMethod.GET)
    public WebResult getTypographyList(@RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
                                       @RequestParam(value = "size", required = false, defaultValue = "10") Integer size,
                                       @RequestParam(value = "typeId", required = false) Integer typeId,
                                       @RequestParam(value = "title", required = false) String title,
                                       @RequestParam(value = "targetType", required = false) Integer targetType,
                                       HttpServletRequest request) {

        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (StringUtils.isBlank(title)) {
            title = null;
        }
        if (organizer != null) {
            WebResult result = WebResult.getSuccessResult();
            result.putResult("data", typographyService.getTypographyListPageBounds(typeId, 0, size, page, organizer.getOrganizerId(), title, targetType));
            result.putResult("organizerId", organizer.getOrganizerId());
            return result;
        } else {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
    }

    @ApiOperation(value = "同步首页", notes = "同步首页", httpMethod = "GET")
    @ApiImplicitParams({})
    @RequestMapping(value = "/synchronous.json", method = RequestMethod.GET)
    public WebResult sysTypography(HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer != null) {
            typographyService.sysTypography(organizer.getOrganizerId());
            redisService.refreshRedisData("ShopInfo", organizer.getOrganizerId());
            return WebResult.getSuccessResult();
        } else {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
    }


    @ApiOperation(value = "删除", notes = "删除", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "typographyId", value = "版面ID", required = true, dataType = "integer", paramType = "body"),
    })
    @RequestMapping(value = "/delete.json", method = RequestMethod.POST)
    public WebResult getTypographyDelete(@RequestParam("typographyId") Integer typographyId, HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer != null) {
            if (typographyService.checkIsOrganizer(typographyId, organizer.getOrganizerId())) {
                return WebResult.getSuccessResult("data", typographyService.deleteTypography(typographyId));
            } else {
                return WebResult.getErrorResult(WebResult.Code.NO_PERMISSION);
            }
        } else {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
    }

    @ApiOperation(value = "上移", notes = "上移", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "typographyId", value = "版面ID", required = true, dataType = "integer", paramType = "body"),
    })
    @RequestMapping(value = "/moveUp.json", method = RequestMethod.POST)
    public WebResult typographyMoveUp(@RequestParam("typographyId") Integer typographyId, HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer != null) {
            if (typographyService.checkIsOrganizer(typographyId, organizer.getOrganizerId())) {
                typographyService.moveUpSort(typographyId);
                return WebResult.getSuccessResult();
            } else {
                return WebResult.getErrorResult(WebResult.Code.NO_PERMISSION);
            }
        } else {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
    }


    @ApiOperation(value = "下移", notes = "下移", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "typographyId", value = "版面ID", required = true, dataType = "integer", paramType = "body"),
    })
    @RequestMapping(value = "/moveDown.json", method = RequestMethod.POST)
    public WebResult typographyMoveDown(@RequestParam("typographyId") Integer typographyId, HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer != null) {
            if (typographyService.checkIsOrganizer(typographyId, organizer.getOrganizerId())) {
                typographyService.moveDownSort(typographyId);
                return WebResult.getSuccessResult();
            } else {
                return WebResult.getErrorResult(WebResult.Code.NO_PERMISSION);
            }
        } else {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
    }


    @ApiOperation(value = "保存", notes = "保存", httpMethod = "POST")
    @ApiImplicitParams({
    })
    @RequestMapping(value = "/save.json", method = RequestMethod.POST)
    public WebResult typographySave(TypographyModel typographyModel, HttpServletRequest request) throws TransParamToModelException, InvocationTargetException, IllegalAccessException {
        Typography typography = new Typography();
        BeanUtilsExtends.copyProperties(typography, typographyModel);
        if (typographyModel.getParentId() == null) {
            typography.setParentId(null);
        }

        if (typographyModel.getTargetId() == null) {
            typography.setTargetId(0);
        }
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer != null) {
            if (typography.getTypographyId() != null && typography.getTypographyId() != 0) {
                if (!typographyService.checkIsOrganizer(typography.getTypographyId(), organizer.getOrganizerId())) {
                    return WebResult.getErrorResult(WebResult.Code.NO_PERMISSION);
                }
            }
            typography.setClientId(organizer.getOrganizerId());
            if (typographyService.saveTypography(typography)) {
                return WebResult.getSuccessResult();
            } else {
                return WebResult.getErrorResult(WebResult.Code.UPDATE_FAILED);
            }
        } else {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
    }

}
