package com.zoo.zoomerchantadminweb.typography.controller;

import com.alibaba.fastjson.JSON;
import com.zoo.common.page.PageList;
import com.zoo.activity.dao.model.*;
import com.zoo.activity.service.ArticleCategoryService;
import com.zoo.activity.service.ArticleService;
import com.zoo.activity.service.MailService;
import com.zoo.activity.vo.ArticleModel;
import com.zoo.util.data.BeanUtilsExtends;
import com.zoo.web.bean.WebResult;
import com.zoo.wechat.common.Config;
import com.zoo.zoomerchantadminweb.context.WebUpmsContext;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Api(value = "api/server/article", tags = {"主办方资讯接口"}, description = "主办方资讯接口描述")
@RestController
@RequestMapping("api/server/article")
public class ArticleController {

    @Autowired
    private ArticleService articleService;

    @Autowired
    private MailService mailService;

    @Autowired
    private ArticleCategoryService articleCategoryService;

    @ApiOperation(value = "获取主办方资讯列表", notes = "获取主办方资讯列表", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "size", value = "分页大小", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "status", value = "状态", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "title", value = "标题", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "author", value = "作者", required = false, dataType = "string", paramType = "query")
    })
    @RequestMapping(value = "/list.json")
    public Object getArticleList(@RequestParam(value = "status", required = false) Integer status,
                                 @RequestParam(value = "title", required = false) String title,
                                 @RequestParam(value = "author", required = false) String author,
                                 @RequestParam(required = false, defaultValue = "1") Integer page,
                                 @RequestParam(required = false, defaultValue = "10") Integer size,
                                 HttpServletRequest request) throws Exception {


        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (null != organizer) {
            PageList articleList = articleService.getArticleListByOrgId(organizer.getOrganizerId(), status, title, author, page, size);
            return WebResult.getSuccessResult("data", articleList);
        } else {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
    }

    /**
     * 文章置顶
     *
     * @param articleId
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "资讯置顶", notes = "资讯置顶", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "articleId", value = "资讯ID", required = true, dataType = "int", paramType = "query")
    })
    @RequestMapping(value = "/top.json", method = RequestMethod.POST)
    public WebResult topArticle(@RequestParam(value = "articleId") int articleId) throws Exception {

        try {
            if (articleService.topArticle(articleId)) {
                return WebResult.getSuccessResult();
            } else {
                return WebResult.getErrorResult(WebResult.Code.UPDATE_FAILED);
            }
        } catch (Exception e) {
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }
    }

    /**
     * 文章取消置顶
     *
     * @param articleId
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "资讯取消置顶", notes = "资讯取消置顶", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "articleId", value = "资讯ID", required = true, dataType = "int", paramType = "query")
    })
    @RequestMapping(value = "/cancelTop.json", method = RequestMethod.POST)
    public WebResult cancelTopArticle(@RequestParam(value = "articleId") int articleId) throws Exception {

        try {
            if (articleService.cancleTopArticle(articleId)) {
                return WebResult.getSuccessResult();
            } else {
                return WebResult.getErrorResult(WebResult.Code.UPDATE_FAILED);
            }
        } catch (Exception e) {
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }
    }

    @ApiOperation(value = "资讯同步", notes = "资讯同步", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "articleId", value = "资讯ID", required = true, dataType = "int", paramType = "query")
    })
    @RequestMapping(value = "/sync.json", method = RequestMethod.POST)
    public WebResult syncArticle(@RequestParam(value = "articleId") int articleId) throws Exception {

        try {
            if (articleService.syncArticle(articleId)) {
                return WebResult.getSuccessResult();
            } else {
                return WebResult.getErrorResult(WebResult.Code.UPDATE_FAILED);
            }
        } catch (Exception e) {
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }
    }

    @ApiOperation(value = "资讯发布", notes = "资讯发布", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "articleId", value = "资讯ID", required = true, dataType = "int", paramType = "query")
    })
    @RequestMapping(value = "/publish.json", method = RequestMethod.POST)
    public WebResult publishArticle(@RequestParam(value = "articleId") int articleId) throws Exception {
        try {
            if (articleService.publishArticle(articleId)) {

                mailService.sendMail("isnowadmin@huaxuezoo.com", "资讯发布-滑雪族", "有新资讯发布，请前往http://" + Config.instance().getDomain() + "/manage/article-list 处理是否显示！");

                return WebResult.getSuccessResult();
            } else {
                return WebResult.getErrorResult(WebResult.Code.UPDATE_FAILED);
            }
        } catch (Exception e) {
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }
    }

    @ApiOperation(value = "资讯取消发布", notes = "资讯取消发布", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "articleId", value = "资讯ID", required = true, dataType = "int", paramType = "query")
    })
    @RequestMapping(value = "/recall.json", method = RequestMethod.POST)
    public WebResult recallArticle(@RequestParam(value = "articleId") int articleId) throws Exception {

        try {
            if (articleService.recallArticle(articleId)) {
                return WebResult.getSuccessResult();
            } else {
                return WebResult.getErrorResult(WebResult.Code.UPDATE_FAILED);
            }
        } catch (Exception e) {
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }
    }

    @ApiOperation(value = "资讯详情", notes = "资讯详情", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "articleId", value = "资讯ID", required = true, dataType = "int", paramType = "query")
    })
    @RequestMapping(value = "/detail.json", method = RequestMethod.GET)
    public Object showArticle(@RequestParam(value = "articleId") int articleId) {
        Map<String, Object> resultMap = new HashMap<>();
        Article article = articleService.getArticleById(articleId);
        ArticleStatus articleStatus = articleService.getArticleStatusById(articleId);
        resultMap.put("article", article);
        resultMap.put("articleStatus", articleStatus);
        if (article.getTagNames() != null && article.getTagNames().length() > 0) {
            String[] tagSplit = article.getTagNames().split(",");
            List tagList = Arrays.asList(tagSplit);
            resultMap.put("tags", tagList);
        }
        resultMap.put("articleJson", JSON.toJSONString(article));
        return WebResult.getSuccessResult("data", resultMap);
    }

    @ApiOperation(value = "资讯保存", notes = "资讯保存", httpMethod = "POST")
    @RequestMapping(value = "/save.json", method = RequestMethod.POST)
    public WebResult addArticle(@Valid @ModelAttribute ArticleModel articleModel, HttpServletRequest request) throws Exception {

        ArticleWithBLOBs article = new ArticleWithBLOBs();
        Organizer organizer = WebUpmsContext.getOrgnization(request);

        BeanUtilsExtends.copyProperties(article, articleModel);

        if (article.getArticleId() == null || article.getArticleId() == 0) {
            if (articleService.addArticle(article, organizer.getOrganizerId()) != null) {
                return WebResult.getSuccessResult("data", article);
            } else {
                return WebResult.getErrorResult(WebResult.Code.ADD_FAILED);
            }
        } else {
            if (articleService.updateArticle(article) != null) {
                return WebResult.getSuccessResult("data", article);
            } else {
                return WebResult.getErrorResult(WebResult.Code.UPDATE_FAILED);
            }
        }


    }
}
