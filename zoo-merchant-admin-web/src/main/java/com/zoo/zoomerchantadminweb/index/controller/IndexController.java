package com.zoo.zoomerchantadminweb.index.controller;

import com.alibaba.fastjson.JSONArray;
import com.zoo.activity.dao.model.Authority;
import com.zoo.activity.dao.nutz.pojo.CommonAccount;
import com.zoo.activity.service.IndexService;
import com.zoo.activity.service.OrganizerService;
import com.zoo.activity.util.DateUtil;
import com.zoo.msg.service.MsgNoticeService;
import com.zoo.web.bean.WebResult;
import com.zoo.wechat.common.Config;
import com.zoo.zoomerchantadminweb.base.BaseController;
import com.zoo.zoomerchantadminweb.context.WebUpmsContext;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Api(value = "api/server/index", tags = {"主办方首页接口"}, description = "主办方首页接口描述")
@RestController
@RequestMapping("api/server/index")
public class IndexController extends BaseController {

    @Resource(name = "indexService")
    private IndexService indexService;

    @Resource(name = "organizerService")
    private OrganizerService organizerService;

    @Autowired
    private MsgNoticeService msgNoticeService;

    @ApiOperation(value = "主办方首页信息", notes = "主办方首页信息,不包括最下方图表数据", httpMethod = "POST")
    @RequestMapping(value = "/info.json", method = RequestMethod.POST)
    public WebResult index(HttpServletRequest request, @RequestParam(value = "name",required = false) String name) {
        CommonAccount account = WebUpmsContext.getAccount(request);
        Map resultMap = new HashMap();
        if (name.equals("msgList")) {
            resultMap.put("msgList", msgNoticeService.getMsgNoticeByTargetId(new Long(account.getOrganizerId()), 10, 1));
        } else {
            resultMap.putAll(indexService.getIndexDataByUser(account.getCustomerId(), account.getOrganizerId(), name));
        }
        return WebResult.getSuccessResult("data", resultMap);
    }

//    @ApiOperation(value = "主办方首页图表信息", notes = "主办方首页图表信息,不包括上方文本信息", httpMethod = "POST")
//    @ApiImplicitParams({
//            @ApiImplicitParam(name = "startTime", value = "开始时间", required = true, dataType = "string", paramType = "query"),
//            @ApiImplicitParam(name = "endTime", value = "结束时间", required = true, dataType = "string", paramType = "query"),
//    })
//    @RequestMapping(value = "/charts.json", method = RequestMethod.POST)
//    public WebResult indexChartsData(@RequestParam("startTime") String startTime, @RequestParam("endTime") String endTime, HttpServletRequest request) {
//        CommonAccount account = WebUpmsContext.getAccount(request);
//        return WebResult.getSuccessResult("data", indexService.getOrderCountList4index(account.getOrganizerId(), startTime, endTime));
//    }

    @ApiOperation(value = "主办方首页培训内容详情", notes = "主办方首页培训内容详情", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "trainingId", value = "ID", required = true, dataType = "int", paramType = "query")
    })
    @RequestMapping(value = "/material.json", method = RequestMethod.POST)
    public WebResult indexChartsData(@RequestParam("trainingId") Integer trainingId, HttpServletRequest request) {
//        CommonAccount account = WebUpmsContext.getAccount(request);
        return WebResult.getSuccessResult("data", indexService.getTrainingMaterialById(trainingId));
    }


    @ApiOperation(value = "保存首页快捷入口", notes = "保存某个主办方用户 进入首页时看到的快捷入口", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "quickMenu", value = "快速入口json字符串", required = true, dataType = "string", paramType = "query")
    })
    @RequestMapping(value = "/saveQuickEntry.json", method = RequestMethod.POST)
    public WebResult saveQuickEntry(HttpServletRequest request,
                                    @RequestParam(value = "quickMenu", required = true, defaultValue = "[]") String quickMenu) {
        CommonAccount account = WebUpmsContext.getAccount(request);
        if (account == null) {
            WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }

        try {
            JSONArray resultJson = new JSONArray(); // 实际保存的入口

            JSONArray quickMenuJson = JSONArray.parseArray(quickMenu); // 传入的 快捷入口

            // 获取用户查看主办方的权限
            List<Authority> permission = organizerService.getPermissionListByUserId(account.getCustomerId(), Authority.SYS_MERCHANT_ADMIN_WEB);

            // 根据允许的权限，删选传入的 快捷入口
            if (permission != null && permission.size() > 0) {
                if (quickMenuJson.size() > 0) {
                    for (Authority authority : permission) {
                        // 不是父级菜单 （自己菜单） 才进行是否有权限判断
                        if (authority.getParentId() != null && authority.getParentId() != 0) {
                            for (int i = 0; i < quickMenuJson.size(); i++) {
                                Integer authorityId = quickMenuJson.getInteger(i);
                                if (authority.getAuthorityId().equals(authorityId)) {
                                    resultJson.add(authority);
                                }
                            }
                        }
                    }
                }
            } else {
                WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
            }

            if (!indexService.saveQuickEntry(account.getCustomerId(), resultJson.toString())) {
                WebResult.getErrorResult(WebResult.Code.ADD_FAILED);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }

        return WebResult.getSuccessResult();
    }


    @ApiOperation(value = "获取快捷入口", notes = "获取当前登录的 主办方用户，所保存的快捷入口", httpMethod = "GET")
    @RequestMapping(value = "/quickEntry.json", method = RequestMethod.GET)
    public WebResult getQuickEntry(HttpServletRequest request) {
        CommonAccount account = WebUpmsContext.getAccount(request);
        if (account == null) {
            WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
        try {
            return WebResult.getSuccessResult("data", indexService.getQuickEntry(account.getCustomerId()));
        } catch (Exception e) {
            e.printStackTrace();
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }
    }

    @ApiOperation(value = "主办方首页培训内容详情", notes = "主办方首页培训内容详情", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "fileId", value = "ID", required = true, dataType = "int", paramType = "query")
    })
    @RequestMapping(value = "/file.json", method = RequestMethod.GET)
    public void downFile(@RequestParam("fileId") Integer fileId, HttpServletResponse response) throws Exception {
        com.zoo.training.model.File file = indexService.getFileById(fileId);
        int bytesum = 0;
        int byteread = 0;
        URL url = new URL(file.getUrl());

        InputStream inStream = null;
        FileOutputStream fs = null;
        String baseStr = Config.instance().getTmpPath() + File.separator + DateUtil.convertDateToString(new Date()) + ".doc";
        try {
            URLConnection conn = url.openConnection();
            inStream = conn.getInputStream();
            fs = new FileOutputStream(baseStr);

            byte[] buffer = new byte[1204];
            //int length;
            while ((byteread = inStream.read(buffer)) != -1) {
                bytesum += byteread;
                //System.out.println(bytesum);
                fs.write(buffer, 0, byteread);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (inStream != null) {
                inStream.close();
            }
            if (fs != null) {
                fs.close();
            }
        }
        //其次将下载到本地的文件通过resopnse返回给用户
        downLoadFile(baseStr, response, file.getTitle());


    }
}
