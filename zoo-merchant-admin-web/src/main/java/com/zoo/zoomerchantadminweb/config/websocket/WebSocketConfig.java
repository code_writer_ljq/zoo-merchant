package com.zoo.zoomerchantadminweb.config.websocket;

import com.zoo.common.util.PropertiesFileUtil;
import com.zoo.common.util.RedisUtil;
import com.zoo.wechat.common.Config;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

/**
 * @desc：Websocket请求处理回调方法，不要忘记在springmvc的配置文件中配置对此类的自动扫描<br/>参考：http://www.open-open.com/lib/view/open1408453906131.html
 * @author: Stone+
 * @time: 2015-6-12 下午4:36:19
 * @ver: 1.0.0
 */
@Configuration
@EnableWebMvc
@EnableWebSocket
public class WebSocketConfig extends WebMvcConfigurerAdapter implements WebSocketConfigurer {

	@Autowired
	private WebSocketEndPoint webSocketEndPoint;

	@Override
	public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
		if (PropertiesFileUtil.getInstance("config").get("websocket").equals("true")) {
			// 用来注册websocket server实现类，第二个参数是访问websocket的地址
			registry.addHandler(webSocketHander(), "/websocket/").addInterceptors(
					new HandshakeInterceptor()).setAllowedOrigins("*");
			// 用来注册socketjs实现类，第二个参数是访问socketjs的地址
			registry.addHandler(webSocketHander(), "/websocket/socketjs/")
					.addInterceptors(new HandshakeInterceptor()).setAllowedOrigins("*").withSockJS();
			System.out.println("———————————————————开启websocket———————————————————————");
		}
	}

	@Bean
	public WebSocketHandler webSocketHander() {
		return this.webSocketEndPoint;
	}
}
