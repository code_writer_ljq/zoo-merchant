package com.zoo.zoomerchantadminweb.config;

import com.zoo.activity.service.OrganizerService;
import com.zoo.finance.service.ZooOperatorLoginService;
import com.zoo.zoomerchantadminweb.web.interceptor.ContextInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.*;

import javax.annotation.Resource;

/**
 * Web配置
 * @Author:LiuZhiMin
 */
@Configuration
public class WebServletContextConfiguration extends WebMvcConfigurerAdapter {

    @Resource
    private OrganizerService organizerService;

    @Resource
    private ZooOperatorLoginService zooOperatorLoginService;


    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        //将所有/static/** 访问都映射到classpath:/static/ 目录下
        //registry.addResourceHandler("/**").addResourceLocations("classpath:/static/");
        //swagger2
        registry.addResourceHandler("/swagger-ui.html").addResourceLocations("classpath:/META-INF/resources/");
        registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
        super.addResourceHandlers(registry);
    }

    /**
     * 拦截器
     * @param registry
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {

        ContextInterceptor contextInterceptor = new ContextInterceptor();
        contextInterceptor.setOrganizerService(organizerService);
        contextInterceptor.setZooOperatorLoginService(zooOperatorLoginService);

        //上下文信息拦截器
        registry.addInterceptor(contextInterceptor).addPathPatterns("/api/**");
        //registry.addInterceptor(new CatInterceptor()).addPathPatterns("/**");
        super.addInterceptors(registry);
    }
}
