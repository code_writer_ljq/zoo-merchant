package com.zoo.zoomerchantadminweb.config;

import com.zoo.account.service.AccountService;
import com.zoo.activity.service.OrganizerService;
import com.zoo.upms.service.AuthorityService;
import com.zoo.upms.service.UpmsAccountRoleRelService;
import com.zoo.web.session.UpmsSessionDao;
import com.zoo.web.session.UpmsSessionFactory;
import com.zoo.zoomerchantadminweb.author.realm.AuthFilter;
import com.zoo.zoomerchantadminweb.author.realm.UpmsRealm;
import org.apache.shiro.session.mgt.SessionManager;
import org.apache.shiro.session.mgt.eis.SessionDAO;
import org.apache.shiro.spring.security.interceptor.AuthorizationAttributeSourceAdvisor;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.apache.shiro.web.servlet.Cookie;
import org.apache.shiro.web.servlet.SimpleCookie;
import org.apache.shiro.web.session.mgt.DefaultWebSessionManager;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.filter.DelegatingFilterProxy;

import javax.annotation.Resource;
import javax.servlet.Filter;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author LiuZhiMin
 * @date 2018/6/13 上午11:50
 * @Description: Shiro权限管理配置
 */
@Configuration
public class ShiroConfig {

    @Resource
    private OrganizerService organizerService;

    @Bean
    public FilterRegistrationBean delegatingFilterProxy(){
        FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean();
        DelegatingFilterProxy proxy = new DelegatingFilterProxy();
        proxy.setTargetFilterLifecycle(true);
        proxy.setTargetBeanName("shiroFilter");
        filterRegistrationBean.setFilter(proxy);
        return filterRegistrationBean;
    }

    @Bean
    public UpmsRealm realm(AccountService accountService, UpmsAccountRoleRelService upmsAccountRoleRelService,AuthorityService authorityService){
        UpmsRealm upmsRealm = new UpmsRealm();
        upmsRealm.setAccountService(accountService);
        upmsRealm.setOrganizerService(organizerService);
        upmsRealm.setUpmsAccountRoleRelService(upmsAccountRoleRelService);
        upmsRealm.setAuthorityService(authorityService);
        return upmsRealm;
    }

    @Bean
    public AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor(SecurityManager securityManager) {
        AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor = new AuthorizationAttributeSourceAdvisor();
        authorizationAttributeSourceAdvisor.setSecurityManager(securityManager);
        return authorizationAttributeSourceAdvisor;
    }

    @Bean
    public SessionManager sessionManager(Cookie cookie,SessionDAO sessionDAO){
        DefaultWebSessionManager sessionManager = new DefaultWebSessionManager();
        sessionManager.setSessionFactory(new UpmsSessionFactory());
        //session超时时间是180000毫秒(1.5小时)
        sessionManager.setGlobalSessionTimeout(1500 * 60 * 60);
        sessionManager.setSessionIdCookieEnabled(true);
        sessionManager.setSessionValidationSchedulerEnabled(false);
        sessionManager.setSessionIdCookie(cookie);
        sessionManager.setSessionDAO(sessionDAO);
        return sessionManager;
    }

    @Bean
    public SessionDAO sessionDAO(){
        return new UpmsSessionDao();
    }

    @Bean
    public SecurityManager securityManager(SessionManager sessionManager,UpmsRealm realm) {
        DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager();
        // 设置realm.
        securityManager.setRealm(realm);
        //设置session manager
        securityManager.setSessionManager(sessionManager);
        return securityManager;
    }

    @Bean
    public SimpleCookie cookie(){
        return new SimpleCookie("token");
    }

    @Bean("shiroFilter")
    public ShiroFilterFactoryBean shiroFilter(SecurityManager securityManager) {
        ShiroFilterFactoryBean shiroFilterFactoryBean = new ShiroFilterFactoryBean();

        // 必须设置 SecurityManager
        shiroFilterFactoryBean.setSecurityManager(securityManager);

        // 如果不设置默认会自动寻找Web工程根目录下的"/login.jsp"页面
        shiroFilterFactoryBean.setLoginUrl("/api/account/login");

        //自定义拦截器
        Map<String, Filter> filtersMap = new LinkedHashMap<String, Filter>();
        filtersMap.put("authc",new AuthFilter());
        shiroFilterFactoryBean.setFilters(filtersMap);

        // 权限控制map.
        Map<String, String> filterChainDefinitionMap = new LinkedHashMap<String, String>();
        // 配置不会被拦截的链接 顺序判断
        filterChainDefinitionMap.put("/", "anon");
        filterChainDefinitionMap.put("/static/**", "anon");
        filterChainDefinitionMap.put("/api/account/register", "anon");
        filterChainDefinitionMap.put("/api/account/verifyCode", "anon");
        filterChainDefinitionMap.put("/*.html","anon");
        filterChainDefinitionMap.put("/*.js","anon");
        filterChainDefinitionMap.put("/*.css","anon");
        filterChainDefinitionMap.put("/webjars/**","anon");
        filterChainDefinitionMap.put("/swagger-resources/**","anon");
        filterChainDefinitionMap.put("/v2/**","anon");
        filterChainDefinitionMap.put("/api/account/login","anon");
        filterChainDefinitionMap.put("/api/account/setRedis","anon");
        filterChainDefinitionMap.put("/api/account/getRedis","anon");
        filterChainDefinitionMap.put("/api/account/setRedisByte","anon");
        filterChainDefinitionMap.put("/api/account/getRedisByte","anon");
        filterChainDefinitionMap.put("/api/account/email/verify","anon");
        filterChainDefinitionMap.put("/websocket","anon");
        filterChainDefinitionMap.put("/websocket/**","anon");
        //配置退出 过滤器,其中的具体的退出代码Shiro已经替我们实现了
        filterChainDefinitionMap.put("/logout", "logout");
        //<!-- 过滤链定义，从上向下顺序执行，一般将/**放在最为下边 -->
        //<!-- authc:所有url都必须认证通过才可以访问; anon:所有url都都可以匿名访问-->
        filterChainDefinitionMap.put("/**", "authc");

        shiroFilterFactoryBean
                .setFilterChainDefinitionMap(filterChainDefinitionMap);
        return shiroFilterFactoryBean;
    }



}
