package com.zoo.zoomerchantadminweb.config.websocket;

import com.google.common.collect.Maps;
import com.zoo.activity.dao.model.Organizer;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import java.util.HashMap;
import java.util.Map;

/**
 * @desc：Websocket处理类
 * @author: Stone+
 * @time: 2015-6-12 下午4:20:50
 * @ver: 1.0.0
 */
@Component("WebSocketEndPoint")
public class WebSocketEndPoint extends TextWebSocketHandler {

//	@Autowired
//	RedisTemplate<String, Object> redisTemplate;

	private Map<String, Map<String, WebSocketSession>> socketSessionMap = new HashMap<>();

	public Map<String, WebSocketSession> getSocketSessionMap(Integer orgId) {
		return socketSessionMap.get(orgId);
	}

	public Map<String, WebSocketSession> getSocketSessionMap(long orgId,long userId) {
		return socketSessionMap.get(orgId+"_" + userId);
	}

	@Override
	protected void handleTextMessage(WebSocketSession session,
			TextMessage message) throws Exception {
		super.handleTextMessage(session, message);
		//获得主办方
		// Organizer organizer = (Organizer) session.getAttributes().get("ORGANIZER_KEY");
		// String orgId = String.valueOf(organizer.getOrganizerId());
		// 获得访问者主机
		/* String hostStr = session.getRemoteAddress().getHostString() + ":"
				+ session.getRemoteAddress().getPort(); */
		// 获得消息
		/* String msg = message.getPayload(); */

	}

	@Override
	public void afterConnectionClosed(WebSocketSession session,
			CloseStatus closeStatus) throws Exception {
		Map<String, String> uriParams = getUriParams(session.getUri().toString());
		String token = uriParams.get("token");

		//扫描支付连接带有userId和orgId
		String userId = uriParams.get("userId");
		if(StringUtils.isNotEmpty(userId)){
			token += "_" + userId;
		}

//		Organizer organizer = (Organizer) session.getAttributes().get("ORGANIZER_KEY");
		if (StringUtils.isNotBlank(token)) {
			Map<String, WebSocketSession> sessionList = socketSessionMap.get(token);
			if (sessionList != null) {
				sessionList.remove(session.getId());
			}
			System.out.println("——————————失去连接——————————" + token);
		} else {
			System.out.println("——————————非法关闭——————————");
		}
	}

	@Override
	public void afterConnectionEstablished(WebSocketSession session) throws Exception {
		Map<String, String> uriParams = getUriParams(session.getUri().toString());
		String token = uriParams.get("token");

		//扫描支付连接带有userId和orgId
		String userId = uriParams.get("userId");
		if(StringUtils.isNotEmpty(userId)){
			token += "_" + userId;
		}

//		Organizer organizer = (Organizer) session.getAttributes().get("ORGANIZER_KEY");
		if (StringUtils.isNotBlank(token)) {
			Map<String, WebSocketSession> sessionList = socketSessionMap.get(token);
			if (sessionList != null) {
				System.out.println(sessionList);
				sessionList.put(session.getId(), session);
			} else {
				sessionList = new HashMap<>();
				sessionList.put(session.getId(), session);
				socketSessionMap.put(token, sessionList);
			}
			System.out.println("——————————创建连接——————————" + token);
		} else {
			System.out.println("——————————非法创建——————————");
			session.close();
		}
	}

	private Map<String,String> getUriParams(String url){
		String p = url.substring(url.indexOf("?") + 1);
		String[] split = p.split("&");
		Map<String,String> pMap = Maps.newHashMap();
		for (String s : split) {
			String[] paramsArray = s.split("=");
			pMap.put(paramsArray[0],paramsArray[1]);
		}
		return pMap;
	}

	@Override
	public void handleTransportError(WebSocketSession session,
			Throwable exception) throws Exception {
		System.out.println("——————————连接异常——————————");
		super.handleTransportError(session, exception);
	}

}
