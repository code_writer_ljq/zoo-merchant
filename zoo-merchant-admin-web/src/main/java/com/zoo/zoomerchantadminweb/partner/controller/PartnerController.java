//package com.zoo.zoomerchantadminweb.partner.controller;
//
//import com.alibaba.fastjson.JSON;
//import com.zoo.account.constant.AccountEnum;
//import com.zoo.account.support.AccountDispatcher;
//import com.zoo.activity.util.DateTypeUtil;
//import com.zoo.activity.constant.ResponseData;
//import com.zoo.activity.core.SpringContextUtil;
//import com.zoo.activity.core.Status;
//import com.zoo.activity.dao.mapper.*;
//import com.zoo.activity.dao.model.*;
//import com.zoo.activity.dao.nutz.pojo.ActivityRelation;
//import com.zoo.activity.dao.nutz.pojo.CommonAccount;
//import com.zoo.activity.service.*;
//import com.zoo.activity.util.*;
//import com.zoo.activity.util.parnter.PartnerDispatcher;
//import com.zoo.activity.vo.ScheduleCoach;
//import com.zoo.icenow.EntryConstant;
//import com.zoo.icenow.dao.model.Entry;
//import com.zoo.icenow.service.EnterService;
//import com.zoo.rest.sms.sdk.utils.DateUtil;
//import com.zoo.wechat.common.Config;
//import com.zoo.zoomerchantadminweb.base.BaseController;
//import net.sf.json.JSONArray;
//import net.sf.json.JSONObject;
//import org.apache.commons.lang3.StringUtils;
//import org.apache.log4j.Logger;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.core.task.TaskExecutor;
//import org.springframework.data.mongodb.core.MongoTemplate;
//import org.springframework.data.redis.core.RedisTemplate;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.ResponseBody;
//import sun.misc.BASE64Decoder;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import javax.servlet.http.HttpSession;
//import java.io.*;
//import java.net.URLDecoder;
//import java.util.*;
//import java.util.concurrent.TimeUnit;
//
///**
// * Created by zhai on 15/11/30.
// */
//@RequestMapping("/partner")
//public class PartnerController extends BaseController {
//
//    static Logger logger = Logger.getLogger(PartnerController.class);
//    @Autowired
//    private OrdersService ordersService;
//
//    @Autowired
//    private EpisodeService episodeService;
//
//    @Autowired
//    private PartnerSystemService partnerSystemService;
//
//    @Autowired
//    private ChargeService chargeService;
//
//    @Autowired
//    private ActivityService activityService;
//
//    @Autowired
//    private OrderOperateService orderOperateService;
//
//    @Autowired
//    private VerifyService verifyService;
//
//    @Autowired
//    private ScheduleCoachService scheduleCoachService;
//
//    @Autowired
//    private CoachService coachService;
//
//    @Autowired
//    private OtaService otaService;
//
//    @Autowired
//    private OrganizerService organizerService;
//
//    @Autowired
//    private PlayerService playerService;
//
//    @Autowired
//    private DepositService depositService;
//
//    @Autowired
//    private EnterService enterService;
//
//    @Autowired
//    private RedisTemplate<String, Object> redisTemplate;
//
//    static TaskExecutor taskExecutor = SpringContextUtil.getBean(TaskExecutor.class);
//
//    //闸机位置map<id,location>
//    private static Map<String, Integer> locationMap;
//
//    private final static String DATE_DES_PASSWORD = "AH1B4Z@G%!&6sdRpqK23LpNZ";
//    private final static long DATE_REFRESH_SEASON_CARD_QCODE = 1000 * 10;
//
//    static {
//        //大厅 location 1
//        locationMap = new HashMap<>();
//        locationMap.put("010000002051", 1);
//        locationMap.put("010000002052", 1);
//        locationMap.put("010000002053", 1);
//        locationMap.put("010000002054", 1);
//        locationMap.put("010000002055", 1);
//        locationMap.put("010000002056", 1);
//        locationMap.put("010000002057", 1);
//        locationMap.put("010000002058", 1);
//        locationMap.put("010000002059", 1);
//        locationMap.put("010000002060", 1);
//        //L1 location 2
//        locationMap.put("010000002061", 2);
//        locationMap.put("010000002062", 2);
//        locationMap.put("010000002063", 2);
//        locationMap.put("010000002064", 2);
//        locationMap.put("010000002065", 2);
//        locationMap.put("010000002066", 2);
//        locationMap.put("010000002067", 2);
//        //L2 location 3
//        locationMap.put("010000002068", 3);
//        locationMap.put("010000002069", 3);
//        locationMap.put("010000002070", 3);
//        locationMap.put("010000002071", 3);
//        locationMap.put("010000002072", 3);
//        locationMap.put("010000002073", 3);
//        //魔毯 location 4
//        locationMap.put("010000002074", 4);
//        locationMap.put("010000002075", 4);
//        locationMap.put("010000002076", 4);
//    }
//
//    @RequestMapping(value = "/ticket/check.json")
//    @ResponseBody
//    public Object partnerCheck(@RequestParam(value = "orgId", required = true) String organizerId,
//                               @RequestParam(value = "key", required = true) String key,
//                               @RequestParam(value = "code", required = true) String code, HttpSession session) {
//        ResponseData responseData = new ResponseData();
//        PartnerSystem partnerSystem = partnerSystemService.getPartnerSystemByOrgId(Integer.parseInt(organizerId));
//        if (partnerSystem != null) {
//            try {
//                String decryptString = DesUtils.decrypt(key, partnerSystem.getPassword());
//                String targetString = partnerSystem.getAppId() + partnerSystem.getAppSecret();
//                if (decryptString.contains(targetString)) {
//                    String timeStamp = decryptString.split(targetString)[1];
//                    Date now = new Date();
//                    long seconds = now.getTime() - new Date(Long.parseLong(timeStamp)).getTime();
//                    if (seconds / 60000 > 2) {
//                        responseData.setData(null);
//                        responseData.setStatus(Status.error);
//                        responseData.setMsg("请求超时，请联系滑雪族！");
//                        return responseData;
//                    }
//                } else {
//                    responseData.setData(null);
//                    responseData.setStatus(Status.error);
//                    responseData.setMsg("加密失败，请联系滑雪族！");
//                    return responseData;
//                }
//            } catch (Exception e) {
//                logger.error(AssUtils.parse(e));
//                responseData.setData(null);
//                responseData.setStatus(Status.error);
//                responseData.setMsg("系统异常，请联系滑雪族！");
//                return responseData;
//            }
//        } else {
//            responseData.setData(null);
//            responseData.setStatus(Status.error);
//            responseData.setMsg("系统异常，请联系滑雪族！");
//            return responseData;
//        }
//        try {
//            Orders orders = ordersService.getOrderDetailByCode(code);
//            if (orders == null) {
//                responseData.setData(null);
//                responseData.setStatus(Status.error);
//                responseData.setMsg("该订单不存在！");
//            } else {
//                OrderView order = ordersService.getOrderDetail(orders.getOrderId());
//                if (order.getSellerId() == Integer.parseInt(organizerId)) {
//                    responseData.setData(order);
//                    responseData.setStatus(Status.success);
//                    responseData.setMsg("获取订单信息成功！");
//                } else {
//                    responseData.setData(null);
//                    responseData.setStatus(Status.error);
//                    responseData.setMsg("没有获取该订单权限！");
//                }
//            }
//        } catch (Exception e) {
//            logger.error(AssUtils.parse(e));
//            responseData.setData(null);
//            responseData.setStatus(Status.error);
//            responseData.setMsg("系统异常，请联系滑雪族！");
//        }
//        return responseData;
//    }
//
//    @RequestMapping(value = "/ticket/verify.json")
//    @ResponseBody
//    public Object partnerVerify(@RequestParam(value = "orgId", required = false) String organizerId,
//                                @RequestParam(value = "key", required = false) String key,
//                                @RequestParam(value = "code", required = false) String code,
//                                @RequestParam(value = "num", required = false) String num, HttpSession session,
//                                HttpServletRequest request, HttpServletResponse response) {
//        logger.info("ticket verify begin");
//        if (null != organizerId) {
//            ResponseData responseData = new ResponseData();
//            PartnerSystem partnerSystem = partnerSystemService.getPartnerSystemByOrgId(Integer.parseInt(organizerId));
//            if (partnerSystem != null) {
//                try {
//                    String decryptString = DesUtils.decrypt(key, partnerSystem.getPassword());
//                    String targetString = partnerSystem.getAppId() + partnerSystem.getAppSecret();
//                    if (decryptString.contains(targetString)) {
//                        String timeStamp = decryptString.split(targetString)[1];
//                        Date now = new Date();
//                        long seconds = now.getTime() - new Date(Long.parseLong(timeStamp)).getTime();
//                        if (seconds / 60000 > 2) {
//                            responseData.setData(null);
//                            responseData.setStatus(Status.error);
//                            responseData.setMsg("请求超时，请联系滑雪族！");
//                            return responseData;
//                        }
//                    } else {
//                        responseData.setData(null);
//                        responseData.setStatus(Status.error);
//                        responseData.setMsg("加密失败，请联系滑雪族！");
//                        return responseData;
//                    }
//                } catch (Exception e) {
//                    logger.error(AssUtils.parse(e));
//                    responseData.setData(null);
//                    responseData.setStatus(Status.error);
//                    responseData.setMsg("系统异常，请联系滑雪族！");
//                    return responseData;
//                }
//            } else {
//                responseData.setData(null);
//                responseData.setStatus(Status.error);
//                responseData.setMsg("系统异常，请联系滑雪族！");
//                return responseData;
//            }
//            Orders orders = ordersService.getOrderDetailByCode(code);
//            if (orders == null) {
//                responseData.setStatus(Status.error);
//                responseData.setMsg("该订单不存在！");
//                return responseData;
//            } else {
//                if (orders.getSellerId().equals(Integer.parseInt(organizerId))) {
//                    int verifyNum;
//                    try {
//                        verifyNum = Integer.parseInt(num);
//                    } catch (Exception e) {
//                        logger.error(AssUtils.parse(e));
//                        responseData.setData(null);
//                        responseData.setStatus(Status.error);
//                        responseData.setMsg("您输入的核销数量不合法,必须为正整数，请确认后重试！");
//                        return responseData;
//                    }
//                    if (verifyNum <= 0) {
//                        responseData.setData(null);
//                        responseData.setStatus(Status.error);
//                        responseData.setMsg("您输入的核销数量不合法，必须为正整数，请确认后重试！");
//                        return responseData;
//                    }
//                    Map resultMap = null;
//                    resultMap = ordersService.partnerVerify(orders, verifyNum);
//
//                    if (resultMap.get(0) != null) {
//                        responseData.setData(null);
//                        responseData.setStatus(Status.error);
//                        responseData.setMsg(resultMap.get(0).toString());
//                    } else {
//                        if (resultMap.get(1) != null) {
//                            responseData.setData(null);
//                            responseData.setStatus(Status.success);
//                            responseData.setMsg("核销成功！");
//                        }
//
//                        if (orders.getBuyerId() < 0) {
//                            otaService.pushOrderVerifyStatus(ordersService.getOrdersByCode(orders.getCode()), verifyNum);
//                        }
//                    }
//                }
//            }
//            return responseData;
//        } else {
//            try {
//                String xml = request.getParameter("xml");
//                logger.info("partner verify info:" + xml);
//                Integer interfaceType = PartnerDispatcher.getPartnerTypeByResponseXml(xml);
//                Map<String, String> treatMap = PartnerDispatcher.partnerTicketVerifyPush(xml, interfaceType);
//                if (treatMap.get("result").equals(Status.success)) {
//                    String orderCode = treatMap.get("orderCode");
//                    String verifyCount = treatMap.get("verifyCount");
//                    Orders orders = ordersService.getOrdersByCode(orderCode);
//                    if (null != orderCode && null != verifyCount) {
//                        Map resultMap = ordersService.partnerVerify(orders, Integer.parseInt(verifyCount));
//                        logger.info("verify result:" + resultMap);
//
//                        if (resultMap.get(1) != null) {
//                            if (orders.getBuyerId() < 0) {
//                                otaService.pushOrderVerifyStatus(ordersService.getOrdersByCode(orders.getCode()), Integer.parseInt(verifyCount));
//                            }
//                            return "success";
//                        } else {
//                            return "error";
//                        }
//                    } else {
//                        logger.info("核销推送,xml:" + xml);
//                    }
//                }
//            } catch (Exception e) {
//                logger.error(AssUtils.parse(e));
//            }
//        }
//        return "error";
//    }
//
//    /**
//     * 查询教练订单接口
//     *
//     * @param organizerId
//     * @param key
//     * @param code
//     * @param session
//     * @return
//     */
//    @RequestMapping(value = "/ticket/coach/check.json")
//    @ResponseBody
//    public Object verfiyCheck(@RequestParam(value = "orgId", required = true) String organizerId,
//                              @RequestParam(value = "key", required = true) String key,
//                              @RequestParam(value = "code", required = true) String code, HttpSession session) {
//        ResponseData responseData = new ResponseData();
//        PartnerSystem partnerSystem = partnerSystemService.getPartnerSystemByOrgId(Integer.parseInt(organizerId));
//        if (partnerSystem != null) {
//            try {
//                String decryptString = DesUtils.decrypt(key, partnerSystem.getPassword());
//                String targetString = partnerSystem.getAppId() + partnerSystem.getAppSecret();
//                if (decryptString.contains(targetString)) {
//                    String timeStamp = decryptString.split(targetString)[1];
//                    Date now = new Date();
//                    long seconds = now.getTime() - new Date(Long.parseLong(timeStamp)).getTime();
//                    if (seconds / 60000 > 2) {
//                        responseData.setData(null);
//                        responseData.setStatus(Status.error);
//                        responseData.setMsg("请求超时，请联系滑雪族！");
//                        return responseData;
//                    }
//                } else {
//                    responseData.setData(null);
//                    responseData.setStatus(Status.error);
//                    responseData.setMsg("加密失败，请联系滑雪族！");
//                    return responseData;
//                }
//            } catch (Exception e) {
//                logger.error(AssUtils.parse(e));
//                responseData.setData(null);
//                responseData.setStatus(Status.error);
//                responseData.setMsg("系统异常，请联系滑雪族！");
//                return responseData;
//            }
//        } else {
//            responseData.setData(null);
//            responseData.setStatus(Status.error);
//            responseData.setMsg("系统异常，请联系滑雪族！");
//            return responseData;
//        }
//        try {
//            Orders orders = ordersService.getOrderDetailByCode(code);
//            List<Player> playerList = playerService.getPlayersByOrder(orders.getOrderId());
//
//            if (orders == null) {
//                responseData.setData(null);
//                responseData.setStatus(Status.error);
//                responseData.setMsg("该订单不存在！");
//            } else {
//                OrderView orderView = ordersService.getOrderDetail(orders.getOrderId());
//                OrderCoachExtendView coachOrderView = ordersService.getOrderCoachExtendWithCoachByOrderId(orders.getOrderId());
//
//                coachOrderView.setCode(orderView.getCode());
//                coachOrderView.setTitle(orderView.getTitle());
//                coachOrderView.setPayTime(orderView.getPayTime());
//                coachOrderView.setTotalPrice(orderView.getTotalPrice());
//
//                if (playerList != null && playerList.size() > 0) {
//                    coachOrderView.setPlayerRealname(playerList.get(0).getRealName());
//                    coachOrderView.setPlayerPhone(playerList.get(0).getPhone());
//                }
//
//                //copyProperties(coachOrder, orderView);
//                if (orders.getSellerId() == Integer.parseInt(organizerId)) {
//                    responseData.setData(coachOrderView);
//                    responseData.setStatus(Status.success);
//                    responseData.setMsg("获取订单信息成功！");
//                } else {
//                    responseData.setData(null);
//                    responseData.setStatus(Status.error);
//                    responseData.setMsg("没有获取该订单权限！");
//                }
//            }
//        } catch (Exception e) {
//            logger.error(AssUtils.parse(e));
//            responseData.setData(null);
//            responseData.setStatus(Status.error);
//            responseData.setMsg("系统异常，请联系滑雪族！");
//        }
//        return responseData;
//    }
//
//    /**
//     * 教练订单核销接口
//     *
//     * @param organizerId
//     * @param key
//     * @param code
//     * @param session
//     * @param request
//     * @param response
//     * @return
//     */
//    @RequestMapping(value = "/ticket/coach/verify.json")
//    @ResponseBody
//    public Object partnerCoachTicketVerify(@RequestParam(value = "orgId", required = true) String organizerId,
//                                           @RequestParam(value = "key", required = true) String key,
//                                           @RequestParam(value = "code", required = true) String code,
//                                           HttpSession session, HttpServletRequest request, HttpServletResponse response) {
//        logger.info("ticket coach verify begin");
//        if (null != organizerId) {
//            ResponseData responseData = new ResponseData();
//            PartnerSystem partnerSystem = partnerSystemService.getPartnerSystemByOrgId(Integer.parseInt(organizerId));
//            if (partnerSystem != null) {
//                try {
//                    String decryptString = DesUtils.decrypt(key, partnerSystem.getPassword());
//                    String targetString = partnerSystem.getAppId() + partnerSystem.getAppSecret();
//                    if (decryptString.contains(targetString)) {
//                        String timeStamp = decryptString.split(targetString)[1];
//                        Date now = new Date();
//                        long seconds = now.getTime() - new Date(Long.parseLong(timeStamp)).getTime();
//                        if (seconds / 60000 > 2) {
//                            responseData.setData(null);
//                            responseData.setStatus(Status.error);
//                            responseData.setMsg("请求超时，请联系滑雪族！");
//                            return responseData;
//                        }
//                    } else {
//                        responseData.setData(null);
//                        responseData.setStatus(Status.error);
//                        responseData.setMsg("加密失败，请联系滑雪族！");
//                        return responseData;
//                    }
//                } catch (Exception e) {
//                    logger.error(AssUtils.parse(e));
//                    responseData.setData(null);
//                    responseData.setStatus(Status.error);
//                    responseData.setMsg("系统异常，请联系滑雪族！");
//                    return responseData;
//                }
//            } else {
//                responseData.setData(null);
//                responseData.setStatus(Status.error);
//                responseData.setMsg("系统异常，请联系滑雪族！");
//                return responseData;
//            }
//            Orders orders = ordersService.getOrderDetailByCode(code);
//            if (orders == null) {
//                responseData.setStatus(Status.error);
//                responseData.setMsg("该订单不存在！");
//                return responseData;
//            } else {
//                Integer parentOrderId = 0;
//                boolean multi = false;
//
//                if (orders.getSellerId().equals(Integer.parseInt(organizerId))) {
//
//                    //开始判断教练订单能否核销
//                    OrderView ordersView = ordersService.getCustomerOrderDetail(orders.getOrderId(), Integer.parseInt(organizerId));
//
//                    if (null == ordersView) {
//                        OrderView orderView = ordersService.getOrderDetail(orders.getOrderId());
//                        List<ActivityRelation> relations = activityService.getRelationIds(orderView.getActivityId());
//                        boolean hasPermission = false;
//                        for (ActivityRelation relation : relations) {
//                            if (relation.getOrganizerId().equals(Integer.parseInt(organizerId))) {
//                                hasPermission = true;
//                                break;
//                            }
//                        }
//                        if (!hasPermission) {
//                            responseData.setStatus(Status.error);
//                            responseData.setMsg("无核销权限");
//                        } else {
//                            ordersView = ordersService.getCustomerOrderDetail(orders.getOrderId(), null);
//                        }
//                    }
//
//                    // 判断票券是否到生效时间start
//                    Episode episode = episodeService.getEpisodeDetailById(ordersView.getEpisodeId());
//                    Activity activity = activityService.getActivityById(episode.getActivityId());
//                    if (episode.getAdvanceBookingTime() != null && activity.getTypeId() != 8 && activity.getTypeId() != 2) {
//                        Date now = new Date();
//                        if ("hour".equals(episode.getAdvanceBookingType())) {
//                            if ((ordersView.getPayTime().getTime() + episode.getAdvanceBookingTime() * 3600 * 1000) > now.getTime()) {
//                                responseData.setStatus(Status.error);
//                                responseData.setMsg("该票券尚未生效,生效时间:" +
//                                        com.zoo.activity.util.DateUtil.dateToDateString(new Date(ordersView.getPayTime().getTime() + episode.getAdvanceBookingTime() * 3600 * 1000), com.zoo.activity.util.DateUtil.TIMEF_FORMAT));
//                                return responseData;
//                            }
//                        } else if ("day".equals(episode.getAdvanceBookingType())) {
//                            if ((ordersView.getPayTime().getTime() + episode.getAdvanceBookingTime() * 24 * 3600 * 1000) > now.getTime()) {
//                                responseData.setStatus(Status.error);
//                                responseData.setMsg("该票券尚未生效,生效时间:" +
//                                        com.zoo.activity.util.DateUtil.dateToDateString(new Date(ordersView.getPayTime().getTime() + episode.getAdvanceBookingTime() * 24 * 3600 * 1000), com.zoo.activity.util.DateUtil.TIMEF_FORMAT));
//                                return responseData;
//                            }
//                        } else if ("minute".equals(episode.getAdvanceBookingType())) {
//                            if ((ordersView.getPayTime().getTime() + episode.getAdvanceBookingTime() * 60 * 1000) > now.getTime()) {
//                                responseData.setStatus(Status.error);
//                                responseData.setMsg("该票券尚未生效,生效时间:" +
//                                        com.zoo.activity.util.DateUtil.dateToDateString(new Date(ordersView.getPayTime().getTime() + episode.getAdvanceBookingTime() * 60 * 1000), com.zoo.activity.util.DateUtil.TIMEF_FORMAT));
//                                return responseData;
//                            }
//                        } else {
//                            responseData.setStatus(Status.error);
//                            responseData.setMsg("网络拥堵");
//                            return responseData;
//                        }
//                    }
//                    // 判断票券是否到生效时间end
//
//                    if (null != ordersView.getParentId() && ordersView.getParentId() != 0) {
//                        parentOrderId = ordersView.getParentId();
//                        multi = true;
//                    }
//                    if (ordersView != null && ordersView.getOrderId().equals(orders.getOrderId()) && ordersView.getOrderStatus() == 1 && activity.getTypeId() == 8 && ordersView.getAppointmentTime() != null) {
//
//                        if (com.zoo.activity.util.DateUtil.isToday(ordersView.getAppointmentTime())) {// 万龙说 只能核销当天的
//                            try {
//                                verifyService.verify(ordersView.getOrderId(), 1, activity.getTypeId(),
//                                        Integer.parseInt(organizerId), "partner-coach", 1, null);
//                            } catch (Exception e) {
//                                logger.error("核销发生错误：" + AssUtils.parse(e));
//                            }
//                            responseData.setStatus(Status.success);
//                            responseData.setMsg("核销成功。");
//                        } else {
//                            responseData.setStatus(Status.error);
//                            responseData.setMsg("该订单无法核销。");
//                        }
//                    } else {
//                        responseData.setStatus(Status.error);
//                        responseData.setMsg("该订单无法核销。");
//                    }
//
//                    if (multi) {
//                        boolean allVerify = true;
//                        List<Orders> childOrders = ordersService.getChildOrders(parentOrderId);
//                        for (Orders order : childOrders) {
//                            if (order.getStatus() == 1) {
//                                allVerify = false;
//                            }
//                        }
//                        if (allVerify) {
//                            Orders parentOrder = ordersService.getSimpleOrdersById(parentOrderId);
//                            parentOrder.setStatus(5);
//                            ordersService.updateOrders(parentOrder);
//                        }
//                    }
//                } else {
//                    responseData.setStatus(Status.error);
//                    responseData.setMsg("你没有此订单核销权限,请联系滑雪族!");
//                }
//            }
//            return responseData;
//        } else {
//            try {
//                String xml = request.getParameter("xml");
//                logger.info("partner verify info:" + xml);
//                Integer interfaceType = PartnerDispatcher.getPartnerTypeByResponseXml(xml);
//                Map<String, String> treatMap = PartnerDispatcher.partnerTicketVerifyPush(xml, interfaceType);
//                if (treatMap.get("result").equals(Status.success)) {
//                    String orderCode = treatMap.get("orderCode");
//                    String verifyCount = treatMap.get("verifyCount");
//                    if (null != orderCode && null != verifyCount) {
//                        Map resultMap = ordersService.partnerVerify(ordersService.getOrdersByCode(orderCode), Integer.parseInt(verifyCount));
//                        logger.info("verify result:" + resultMap);
//
//                        if (resultMap.get(1) != null) {
//                            return "success";
//                        } else {
//                            return "error";
//                        }
//                    } else {
//                        logger.info("退票推送,xml:" + xml);
//                    }
//                }
//            } catch (Exception e) {
//                logger.error(AssUtils.parse(e));
//            }
//        }
//        return "error";
//    }
//
//    @RequestMapping(value = "/ticket/refund.json")
//    public void ticketRefundNotify(HttpServletRequest request, HttpServletResponse response, HttpSession session) {
//
//        String xml = request.getParameter("xml");
//        Integer interfaceType = PartnerDispatcher.getPartnerTypeByResponseXml(xml);
//        Map<String, String> treatMap = PartnerDispatcher.partnerTicketRefundPush(xml, interfaceType);
//
//        PrintWriter writer = null;
//        try {
//            writer = response.getWriter();
//            String orderCode = treatMap.get("orderCode");
//            String backNum = treatMap.get("backNumber");
//
//            Refund refund = ordersService.getRefundByOrderCode(orderCode);
//            Orders orders = ordersService.getOrdersByCode(orderCode);
//
//            com.alibaba.fastjson.JSONObject pushInfo = new com.alibaba.fastjson.JSONObject();
//            if (orders.getBuyerId() == -2) {
//                pushInfo.put("method", "pushOrderStatus");
//                pushInfo.put("baiduOrderId", orders.getAgentOrderCode());
//                pushInfo.put("tpOrderId", orders.getCode());
//                pushInfo.put("partnerCode", "huaxuezu");
//                pushInfo.put("uuid", String.valueOf(new Date().getTime()));
//                pushInfo.put("version", "1.0.0");
//                pushInfo.put("timestamp", (int) (new Date().getTime() / 1000));
//                pushInfo.put("voucherIdCard", 0);
//            }
//
//            if (treatMap.get("result").equals(Status.success) && treatMap.get("status").equals("1")) {
//                refund.setStatus(0);
//            } else {
//                Charge charge = chargeService.getChargeById(orders.getChargeId());
//                Episode episode = episodeService.getEpisodeByChargeId(orders.getChargeId());
//                boolean multi = charge.getIncrement() == 1;
//                orderOperateService.occupy(episode.getEpisodeId(), charge.getChargeId(), Integer.parseInt(backNum), multi);
//
//                refund.setStatus(-3);
//                orders.setStatus(1);
//            }
//
//            ordersService.updateOrders(orders);
//            ordersService.updateRefund(refund);
//            writer.write("success");
//        } catch (Exception e) {
//            writer.write("failed");
//            AssUtils.parse(e);
//        }
//
//        writer.flush();
//        writer.close();
//    }
//
//    @RequestMapping(value = "/ticket/push.json")
//    public void productUpdatePush(HttpServletRequest request, HttpServletResponse response) throws Exception {
//        response.setContentType("text/html;charset=utf-8");
//        String xml = request.getParameter("xml");
//        PrintWriter writer = response.getWriter();
//        PartnerDispatcher.partnerTicketUpdatePush(xml, null);
//        writer.write("success");
//    }
//
//    @RequestMapping(value = "/jinfeiying/push.json")
//    public void jinfeiyingVerify(HttpServletRequest request, HttpServletResponse response) throws Exception {
//        if (request.getParameter("type") == null) {
//            //首次验证
//            String spare = request.getParameter("spare");
//            byte[] spareByte = spare.getBytes("UTF8");
//            byte[] bytes = SignatureUtils.decodeBase64(spareByte);
//            String tmp = new String(bytes, "UTF8");
//            logger.info("JFYSPARE------------" + tmp);
//            String decodeSpare = tmp.substring(51, tmp.length() - 3);
//            PrintWriter writer = response.getWriter();
//            writer.write(decodeSpare);
////            writer.write("{'status':'666'}");
//        } else {
//            Enumeration enu = request.getParameterNames();
//            Map<String, Object> map = new HashMap<>();
//            JSONObject jsonObject = new JSONObject();
//            while (enu.hasMoreElements()) {
//                String paraName = (String) enu.nextElement();
//                if (paraName.equals("signature") || paraName.equals("0")) {
//                    jsonObject.put(paraName, request.getParameter(paraName));
//                    continue;
//                }
//                map.put(paraName, request.getParameter(paraName));
//                jsonObject.put(paraName, request.getParameter(paraName));
//            }
//            String sign = SignatureUtils.getJFYSignContent(map);
//            if (sign.equals(request.getParameter("signature"))) {
//                String type = request.getParameter("type");
//                switch (type) {
//                    case "118100":
//                        String ordernumber = request.getParameter("ordernumber");
//                        String number = request.getParameter("number");
//                        PrintWriter writer = response.getWriter();
//                        Orders orders = ordersService.getOrdersByVerifyURL(ordernumber);
//                        Map resultMap = ordersService.partnerVerify(orders, Integer.parseInt(number));
//                        if (resultMap.get(1) != null) {
//                            writer.write("{'status':'666'}");
//                            logger.info("金飞鹰核销推送处理成功--orderCode:" + ordersService.getOrdersByVerifyURL(ordernumber).getCode());
//                            if (orders.getBuyerId() < 0) {
//                                otaService.pushOrderVerifyStatus(ordersService.getOrdersByCode(orders.getCode()), Integer.parseInt(number));
//                            }
//                        } else {
//                            writer.write("{'status':'222'}");
//                            logger.info("金飞鹰核销推送异常--verifyURL:" + ordernumber);
//                        }
//                        break;
//                    default:
//                        logger.info("金飞鹰推送信息:" + jsonObject.toString());
//                        break;
//                }
//            } else {
//                logger.info("金飞鹰错误推送信息:" + jsonObject.toString());
//            }
//        }
//    }
//
//    @RequestMapping(value = "/piaofutong/push.json")
//    public void piaofutongPust(HttpServletRequest request, HttpServletResponse response) throws IOException {
//        StringBuffer sb = new StringBuffer();
//        InputStream is = request.getInputStream();
//        InputStreamReader isr = new InputStreamReader(is);
//        BufferedReader br = new BufferedReader(isr);
//        String s = "";
//        while ((s = br.readLine()) != null) {
//            sb.append(s);
//        }
//        String str = sb.toString();
//        if (str.equals("")) {
//            Writer writer = response.getWriter();
//            writer.write("success");
//        } else {
//            Writer writer = response.getWriter();
//            JSONObject jsonObject = JSONObject.fromObject(str);
//            Integer orderState = jsonObject.getInt("OrderState");
//            String orderCode;
//            Map resultMap;
//            switch (orderState) {
//                case 1:
//                    orderCode = jsonObject.getString("OrderCall");
//                    try {
//                        Orders orders = ordersService.getOrdersByCode(orderCode);
//                        resultMap = ordersService.partnerVerify(orders, orders.getAvailableCount());
//                        if (resultMap.get(1) != null) {
//                            writer.write("200");
//                            logger.info("票务通核销推送处理成功--orderCode:" + orderCode);
//                            if (orders.getBuyerId() < 0) {
//                                otaService.pushOrderVerifyStatus(ordersService.getOrdersByCode(orders.getCode()), orders.getAvailableCount());
//                            }
//                        } else {
//                            writer.write((String) resultMap.get(0));
//                            logger.info("票务通核销推送异常--orderCode:" + orderCode + resultMap.get(0));
//                        }
//                    } catch (Exception e) {
//                        writer.write("error");
//                        logger.info("票务通核销推送异常--orderCode:" + orderCode);
//                    }
//                    break;
//                case 7:
//                    orderCode = jsonObject.getString("OrderCall");
//                    try {
//                        Orders orders = ordersService.getOrdersByCode(orderCode);
//                        resultMap = ordersService.partnerVerify(orders, jsonObject.getInt("Tnumber"));
//                        if (resultMap.get(1) != null) {
//                            writer.write("200");
//                            logger.info("票务通核销推送处理成功--orderCode:" + orderCode + "核销数量--" + jsonObject.getInt("Tnumber"));
//                            if (orders.getBuyerId() < 0) {
//                                otaService.pushOrderVerifyStatus(ordersService.getOrdersByCode(orders.getCode()), jsonObject.getInt("Tnumber"));
//                            }
//                        } else {
//                            writer.write((String) resultMap.get(0));
//                            logger.info("票务通核销推送异常--orderCode:" + orderCode + resultMap.get(0));
//                        }
//                    } catch (Exception e) {
//                        writer.write("error");
//                        logger.info("票务通核销推送异常--orderCode:" + orderCode);
//                    }
//                    break;
//                case 8:
//                    orderCode = jsonObject.getString("OrderCall");
//                    String refundType = jsonObject.getString("Refundtype");
//                    String tNumber = jsonObject.getString("Tnumber");
//                    String refundNum = String.valueOf(ordersService.getOrderDetailByCode(orderCode).getAvailableCount() - Integer.valueOf(tNumber));
//                    Refund refund = ordersService.getRefundByOrderCode(orderCode);
//                    Orders orders = ordersService.getOrdersByCode(orderCode);
//                    if (refundType.equals("1")) {
//                        refund.setStatus(0);
//                    } else {
//                        Charge charge = chargeService.getChargeById(orders.getChargeId());
//                        Episode episode = episodeService.getEpisodeByChargeId(orders.getChargeId());
//                        boolean multi = charge.getIncrement() == 1;
//                        orderOperateService.occupy(episode.getEpisodeId(), charge.getChargeId(), Integer.parseInt(refundNum), multi);
//                        refund.setStatus(-3);
//                        orders.setStatus(1);
//                    }
//                    ordersService.updateOrders(orders);
//                    ordersService.updateRefund(refund);
//                    writer.write("200");
//                    break;
//                case 9:
//                    break;
//            }
//        }
//    }
//
//    @RequestMapping(value = "/yglz/push.json", method = RequestMethod.POST)
//    public void YglzVerify(@RequestParam(value = "xml", required = true) String xml,
//                           HttpServletResponse response) throws Exception {
//        response.setHeader("Content-type", "text/html;charset=UTF-8");
//        Writer writer = response.getWriter();
//        Map<String, String> requestMap;
//        try {
//            requestMap = XMLUtil.xml2Map(URLDecoder.decode(xml, "utf-8"));
//        } catch (Exception e) {
//            writer.write("xml解析失败");
//            logger.info("处理阳光绿洲核销推送失败--xml解析失败: xml -> " + URLDecoder.decode(xml, "utf-8"));
//            return;
//        }
//        String verifyUrl = requestMap.get("order_num");
//        String verifyNumStr = requestMap.get("num");
//        String verifyReqSeq = requestMap.get("platform_req_seq");
//        Integer verifyNum;
//        if (verifyUrl == null || "".equals(verifyUrl) || verifyNumStr == null || "".equals(verifyNumStr)
//                || verifyReqSeq == null || "".equals(verifyReqSeq)) {
//            writer.write("参数解析失败");
//            logger.info("处理阳光绿洲核销推送失败:参数错误 --verifyUrl: " + verifyUrl + " --verifyNum: " + verifyNumStr
//                    + " --verifyReqSeq: " + verifyReqSeq);
//            return;
//        }
//
//        try {
//            verifyNum = Integer.parseInt(verifyNumStr);
//        } catch (Exception e) {
//            writer.write("num参数有误");
//            logger.info("处理阳光绿洲核销推送失败--num参数有误: " + verifyNumStr);
//            return;
//        }
//
//        Orders order = ordersService.getOrdersByVerifyURL(verifyUrl);
//        if (null == order) {
//            writer.write("订单不存在");
//            logger.info("阳光绿洲核销推送错误:不存在的订单。 --verifyUrl = " + verifyUrl);
//            return;
//        }
//        OrderHistoryMapper orderHistoryMapper = SpringContextUtil.getBean(OrderHistoryMapper.class);
//        OrderHistory orderHistory = orderHistoryMapper.getOrderHistoryByOtherInfo(verifyReqSeq, order.getOrderId());
//        if (orderHistory != null) {
//            writer.write("success");
//            logger.info("重复的阳光绿洲核销推送--请求流水号: " + verifyReqSeq + " 外部订单号: " + verifyUrl);
//            return;
//        }
//        try {
//
//            Map verifyResult = ordersService.partnerVerify(order, verifyNum, verifyReqSeq);
//            if (verifyResult.get(1) != null) {
//                if (order.getBuyerId() < 0) {
//                    otaService.pushOrderVerifyStatus(ordersService.getOrdersByCode(order.getCode()), verifyNum);
//                }
//                writer.write("success");
//                logger.info("成功处理阳光绿洲核销推送--orderCode:" + order.getCode());
//            } else {
//                writer.write("error msg: " + verifyResult.get(0));
//                logger.info("处理阳光绿洲核销推送失败--orderCode:" + verifyUrl + " --errorMsg:" + verifyResult.get(0));
//            }
//            writer.close();
//        } catch (Exception e) {
//            writer.write("核销同步失败");
//            logger.info("处理阳光绿洲核销推送失败--verifyUrl:" + verifyUrl);
//        }
//    }
//
//    /**
//     * 智游宝退款推送   ()
//     *
//     * @param retreatBatchNo
//     * @param orderCode      订单号
//     * @param subOrderCode
//     * @param auditStatus
//     * @param returnNum      退款数量
//     * @param sign           签名
//     * @param response
//     */
//    @RequestMapping(value = "/zyb/push/refund.json", method = RequestMethod.GET)
//    public void zybRefund(@RequestParam("retreatBatchNo") String retreatBatchNo,
//                          @RequestParam("orderCode") String orderCode,
//                          @RequestParam("subOrderCode") String subOrderCode,
//                          @RequestParam("auditStatus") String auditStatus,
//                          @RequestParam("returnNum") Integer returnNum,
//                          @RequestParam("sign") String sign,
//                          HttpServletResponse response) {
//        PrintWriter writer = null;
//        try {
//            writer = response.getWriter();
//        } catch (IOException e) {
//            return;
//        }
//
//
//        if (orderCode == null || "".equals(orderCode)
//                || returnNum == null || returnNum <= 0) {
//            logger.error("智游宝退款推送参数错误: orderCode=" + orderCode + ", returnNum = " + returnNum);
//            writer.write("failed");
//            return;
//        }
//        Orders order = ordersService.getOrdersByCode(orderCode);
//        if (order == null) {
//            logger.error("智游宝退款推送错误,不存在的订单: orderCode = " + orderCode);
//            writer.write("failed");
//            return;
//        }
//        PartnerSystemMapper partnerSystemMapper = SpringContextUtil.getBean(PartnerSystemMapper.class);
//        PartnerSystem partnerSystem = partnerSystemMapper.selectByOrgId(order.getSellerId());
//        if (null == partnerSystem) {
//            logger.error("智游宝退款推送错误,未查到分销商: orderCode = " + orderCode);
//            writer.write("failed");
//            return;
//        }
//
//        String hxzSign = MD5Util.getLow32Md5(orderCode + partnerSystem.getPassword());
//        if (!sign.equals(hxzSign)) {
//            logger.error("智游宝核销推送错误,签名错误: orderCode = " + orderCode + ", sign = " + sign);
//            writer.write("failed");
//            return;
//        }
//
//        Refund refund = ordersService.getRefundByOrderCode(orderCode);
//        if (refund == null) {
//            logger.error("智游宝退款推送错误,未找到退款记录: orderCode = " + orderCode);
//            writer.write("failed");
//            return;
//        }
//
//        Charge charge = chargeService.getChargeById(order.getChargeId());
//        Episode episode = episodeService.getEpisodeByChargeId(charge.getChargeId());
//
//        try {
//            if (null != refund && refund.getStatus() == 0) {
//                logger.error("智游宝退款推送错误,重复的退款推送: orderCode = " + orderCode);
//                writer.write("failed");
//            } else if (auditStatus != null && !"success".equals(auditStatus)) { // 退款失败
//                boolean multi = charge.getIncrement() == 1;
//                orderOperateService.occupy(episode.getEpisodeId(), charge.getChargeId(), returnNum, multi);
//
//                refund.setStatus(-3);
//                order.setStatus(1);
//
//                ordersService.updateOrders(order);
//                ordersService.updateRefund(refund);
//
//                logger.error("智游宝订单退款失败, 拒绝退款: orderCode = " + orderCode);
//                writer.write("success");
//            } else {
//                refund.setStatus(0);
//                ordersService.updateRefund(refund);
//                logger.error("智游宝订单退款退款 退款成功: orderCode = " + orderCode);
//                writer.write("success");
//            }
//        } catch (Exception e) {
//            logger.error("处理智游宝退款推送发生异常: orderCode = " + orderCode + "Exception: " + e);
//            writer.write("failed");
//            AssUtils.parse(e);
//        }
//        writer.flush();
//        writer.close();
//    }
//
//    /**
//     * 智游宝核销推送 （非实名制、实名制）
//     *
//     * @param order_no       订单号
//     * @param status         check
//     * @param sub_order_no
//     * @param checkNum       核销数量
//     * @param returnNum
//     * @param total
//     * @param certNo         （身份证1#身份证2  #为分隔符）
//     * @param checkTime      URLEncoder.encode(核销时间, "UTF-8") 核销推送标识
//     * @param sign           签名
//     * @param request
//     * @param response
//     * @param servletSession
//     */
//    @RequestMapping(value = "/zyb/push/check.json")
//    public void zybCheck(@RequestParam("order_no") String order_no,
//                         @RequestParam("status") String status,
//                         @RequestParam("sub_order_no") String sub_order_no,
//                         @RequestParam("checkNum") Integer checkNum,
//                         @RequestParam("returnNum") Integer returnNum,
//                         @RequestParam("total") Integer total,
//                         @RequestParam(value = "certNo", required = false) String certNo,
//                         @RequestParam("checkTime") String checkTime,
//                         @RequestParam("sign") String sign,
//                         HttpServletRequest request, HttpServletResponse response, HttpSession servletSession) {
//        if (order_no == null || "".equals(order_no)
//                || checkNum == null || checkNum <= 0) {
//            logger.error("智游宝核销推送参数错误: orderCode=" + order_no + ", returnNum = " + returnNum);
//            return;
//        }
//        Orders order = ordersService.getOrdersByCode(order_no);
//        if (order == null) {
//            logger.error("智游宝核销推送错误,不存在的订单: orderCode = " + order_no);
//            return;
//        }
//        PartnerSystemMapper partnerSystemMapper = SpringContextUtil.getBean(PartnerSystemMapper.class);
//        PartnerSystem partnerSystem = partnerSystemMapper.selectByOrgId(order.getSellerId());
//        if (null == partnerSystem) {
//            logger.error("智游宝核销推送错误,未查到分销商: orderCode = " + order_no);
//            return;
//        }
//        String hxzSign = MD5Util.getLow32Md5("order_no=" + order_no + partnerSystem.getPassword());
//        if (!sign.equals(hxzSign)) {
//            logger.error("智游宝核销推送错误,签名错误: orderCode = " + order_no + ", sign = " + sign);
//            return;
//        }
//
//        OrderHistoryMapper orderHistoryMapper = SpringContextUtil.getBean(OrderHistoryMapper.class);
//        OrderHistory orderHistory = orderHistoryMapper.getOrderHistoryByOtherInfo(checkTime, order.getOrderId());
//        if (orderHistory != null) {
//            logger.error("重复的智游宝核销推送 OrderCode = " + order_no);
//            return;
//        }
//        Writer writer;
//        try {
//            writer = response.getWriter();
//            Map verifyResult = ordersService.partnerVerify(order, checkNum, checkTime);
//            if (verifyResult.get(1) != null) {
//                if (order.getBuyerId() < 0) {
//                    otaService.pushOrderVerifyStatus(ordersService.getOrdersByCode(order.getCode()), checkNum);
//                }
//                writer.write("success");
//                logger.info("成功处理智游宝核销推送--orderCode:" + order.getCode());
//            } else {
//                writer.write("error msg: " + verifyResult.get(0));
//                logger.error("处理智游宝核销推送失败--orderCode:" + order_no + " --errorMsg:" + verifyResult.get(0));
//            }
//            if (null != writer) {
//                writer.close();
//            }
//        } catch (Exception e) {
//            logger.error("处理智游宝核销推送失败: orderCode = " + order_no + " exception: " + AssUtils.parse(e));
//        }
//    }
//
//    @RequestMapping(value = "/hhkj/check.json")
//    public void hhkjPush(HttpServletRequest request, HttpServletResponse response) throws IOException {
//        response.setHeader("Content-type", "text/html;charset=UTF-8");
//        OrganizerWithScenic organizer = null;
//        StringBuffer sb = new StringBuffer();
//        InputStream is = request.getInputStream();
//        InputStreamReader isr = new InputStreamReader(is);
//        BufferedReader br = new BufferedReader(isr);
//        String s = "";
//        while ((s = br.readLine()) != null) {
//            sb.append(s);
//        }
//        String str = sb.toString();
//        Map<String, String> requestMap;
//        Writer writer = response.getWriter();
//
//        try {
//            requestMap = XMLUtil.xml2Map(URLDecoder.decode(str, "utf-8"));
//        } catch (Exception e) {
//            writer.write("xml解析失败");
//            logger.info("处理浩瀚科技推送失败--xml解析失败: xml -> " + URLDecoder.decode(str, "utf-8"));
//            return;
//        }
//        logger.info("处理浩瀚科技推送--xml解析: xml -> " + URLDecoder.decode(str, "utf-8"));
//
////        String md5String = requestMap.get("security_md5");
////        String key = "sgGZMATnhSWALyEsKNqYU7BS";
//
////        if (md5String.toLowerCase().equals(MD5Util.getLow32Md5(requestMap.get("randomid")) + requestMap.get("pos_id") + key)) {
//
//        String cardId = requestMap.get("CardID");
//        String posId = requestMap.get("pos_id");
//        if (cardId.equals("CE356881500104E0") || cardId.equals("F0BC8267500104E0") || cardId.equals("D6266881500104E0") || cardId.equals("CBC18267500104E0") || cardId.equals("9AFE8267500104E0")) {
//            Calendar testBegin = Calendar.getInstance();
//            Calendar testEnd = Calendar.getInstance();
//            if (new Date().getHours() > 18) {
//                testBegin.set(Calendar.HOUR_OF_DAY, 18);
//                testEnd.set(Calendar.MINUTE, 30);
//                testEnd.set(Calendar.HOUR_OF_DAY, 21);
//                testEnd.set(Calendar.MINUTE, 30);
//                String resultString = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
//                        "<business_trans version=\"1.0\">\n" +
//                        "<request_type>Paycard</request_type>\n" +
//                        "<cardName>" + "夜场（自带板）" + "</cardName>\n" +
//                        "<startTime>" + (DateUtil.dateToStr(new Date(testBegin.getTime().getTime()), 4)) + "</startTime>\n" +
//                        "<endDate>" + (DateUtil.dateToStr(new Date(testEnd.getTime().getTime()), 4)) + "</endDate>\n" +
//                        "<useNum>0</useNum>\n" +
//                        "<cardState>1</cardState>\n" +
//                        "<theState>1</theState>\n" +
//                        "<randomid>" + new Date().getTime() + "</randomid>\n" +
////                        "<security_md5>" + md5String + "</security_md5>\n" +
//                        "</business_trans>";
//                writer.write(resultString);
//                return;
//            } else {
//                testBegin.set(Calendar.HOUR_OF_DAY, 8);
//                testEnd.set(Calendar.MINUTE, 30);
//                testEnd.set(Calendar.HOUR_OF_DAY, 16);
//                testEnd.set(Calendar.MINUTE, 30);
//                String resultString = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
//                        "<business_trans version=\"1.0\">\n" +
//                        "<request_type>Paycard</request_type>\n" +
//                        "<cardName>" + "雪票" + "</cardName>\n" +
//                        "<startTime>" + (DateUtil.dateToStr(new Date(testBegin.getTime().getTime()), 4)) + "</startTime>\n" +
//                        "<endDate>" + (DateUtil.dateToStr(new Date(testEnd.getTime().getTime()), 4)) + "</endDate>\n" +
//                        "<useNum>0</useNum>\n" +
//                        "<cardState>1</cardState>\n" +
//                        "<theState>1</theState>\n" +
//                        "<randomid>" + new Date().getTime() + "</randomid>\n" +
////                        "<security_md5>" + md5String + "</security_md5>\n" +
//                        "</business_trans>";
//                writer.write(resultString);
//                return;
//            }
//        }
//
//        Date now = new Date();
//
//        RentDetailMapper rentDetailMapper = SpringContextUtil.getBean(RentDetailMapper.class);
//        MaterialMapper materialMapper = SpringContextUtil.getBean(MaterialMapper.class);
//        EquipmentRelationshipsMapper equipmentRelationshipsMapper = SpringContextUtil.getBean(EquipmentRelationshipsMapper.class);
//        Material material = materialMapper.selectByCupWaferNum(cardId, null);
//
//        if (null == material || material.getStatus() != 1) {
//            String resultString =
//                    "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
//                            "<business_trans version=\"1.0\">\n" +
//                            "<status>Error</status>\n" +
//                            "<Returnsinfo>卡号有误</Returnsinfo>\n" +
//                            "<randomid>" + now.getTime() + "</randomid>\n" +
//                            "</business_trans>";
//            writer.write(resultString);
//            return;
//        }
//
//        //教练卡
//        if (material.getMaterialType() != null && material.getMaterialType().equals(1)) {
//            logger.info("教练卡开始______" + material.getMaterialNum());
//            CoachMapper coachMapper = SpringContextUtil.getBean(CoachMapper.class);
//            Coach coach = coachMapper.getCoachByMaterial(material.getMaterialId());
//            ScheduleCoach scheduleCoach = scheduleCoachService.getGuideCoach(coach.getOrganizerId(), coach.getCoachId());
////            logger.info("出导教练信息______" + scheduleCoach != null ? scheduleCoach.getCoachId() : "空");
//            if (scheduleCoach != null && scheduleCoach.getGuideOrder() != null && material.getStatus().equals(1)) {
//                CoachGuide coachGuide = coachService.getCurrentGuide(coach.getCoachId());
//                OrderCoachExtend orderCoachExtend = scheduleCoach.getGuideOrder();
//                Date appointmentTime = orderCoachExtend.getAppointmentTime();
//                Double duration = orderCoachExtend.getDuration();
//                organizer = organizerService.getAllInfoById(coach.getOrganizerId());
//                Date start = null;
//                Date end = null;
//                if (coachGuide != null) {
//                    logger.info("coachGuideStart" + start);
//                    start = coachGuide.getGuideStart();
//                    end = coachService.getCourseEndTime(start, duration, organizer, 1);
//                    if (coachGuide.getGuideEnd() != null) {
//                        end = coachGuide.getGuideEnd();
//                    }
//                } else {
//                    start = coachService.getCourseStartTime(appointmentTime, duration, organizer);
//                    end = coachService.getCourseEndTime(start, duration, organizer, 1);
//                }
//                logger.info("教练课程开始时间______" + start);
//                logger.info("教练课程结束时间______" + end);
//                logger.info("订单信息______" + JSON.toJSONString(orderCoachExtend));
//                if (start.before(now) && end.after(now)) {
//                    logger.info("教练通过___" + coach.getRealname() + "卡号______" + material.getMaterialNum());
//                    String resultString = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
//                            "<business_trans version=\"1.0\">\n" +
//                            "<request_type>Paycard</request_type>\n" +
//                            "<cardName>" + "教练-" + coach.getRealname() + "</cardName>\n" +
//                            "<startTime>" + DateUtil.dateToStr(start, 4) + "</startTime>\n" +
//                            "<endDate>" + (DateUtil.dateToStr(end, 4)) + "</endDate>\n" +
//                            "<useNum>0</useNum>\n" +
//                            "<cardState>1</cardState>\n" +
//                            "<theState>1</theState>\n" +
//                            "<randomid>" + now.getTime() + "</randomid>\n" +
////                        "<security_md5>" + md5String + "</security_md5>\n" +
//                            "</business_trans>";
//                    writer.write(resultString);
//                    return;
//                } else {
//                    String resultString =
//                            "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
//                                    "<business_trans version=\"1.0\">\n" +
//                                    "<status>Error</status>\n" +
//                                    "<Returnsinfo>没有出导订单</Returnsinfo>\n" +
//                                    "<randomid>" + now.getTime() + "</randomid>\n" +
//                                    "</business_trans>";
//                    writer.write(resultString);
//                    return;
//                }
//            } else {
//                //没有出导订单
//                String resultString =
//                        "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
//                                "<business_trans version=\"1.0\">\n" +
//                                "<status>Error</status>\n" +
//                                "<Returnsinfo>没有出导订单</Returnsinfo>\n" +
//                                "<randomid>" + now.getTime() + "</randomid>\n" +
//                                "</business_trans>";
//                writer.write(resultString);
//                return;
//            }
//        }
//
//        RentDetail rentDetail = rentDetailMapper.getDetailByMaterialIdAndStatus(material.getMaterialId(), material.getStatus());
//
//        if (null == rentDetail) {
//            String resultString =
//                    "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
//                            "<business_trans version=\"1.0\">\n" +
//                            "<status>Error</status>\n" +
//                            "<Returnsinfo>卡号有误</Returnsinfo>\n" +
//                            "<randomid>" + now.getTime() + "</randomid>\n" +
//                            "</business_trans>";
//            writer.write(resultString);
//            return;
//        }
//
//        if (null != redisTemplate.opsForValue().get(cardId)) {
//            String resultString =
//                    "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
//                            "<business_trans version=\"1.0\">\n" +
//                            "<status>Error</status>\n" +
//                            "<Returnsinfo>一分钟内不能再次刷卡</Returnsinfo>\n" +
//                            "<randomid>" + now.getTime() + "</randomid>\n" +
//                            "</business_trans>";
//            writer.write(resultString);
//            return;
//        }
//
//        EquipmentRelationships equipmentRelationships = equipmentRelationshipsMapper.selectByPrimaryKey(rentDetail.getRelationId());
//        OrderView parentOrder = ordersService.getOrderDetail(ordersService.getOrderDetail(equipmentRelationships.getOrderId()).getParentId());
//        OrderView orders = ordersService.getDefaultChildOrderView(ordersService.getOrderDetail(equipmentRelationships.getOrderId()).getParentId());
//
//        organizer = organizerService.getAllInfoById(orders.getSellerId());
//        Integer nowHour = now.getHours();
//        Integer nowMin = now.getMinutes();
//
//        Charge charge = chargeService.getChargeById(orders.getChargeId());
//
//        //指定日期校验
//        Episode episode = episodeService.getEpisodeDetailById(charge.getEpisodeId());
//
//        if (episode.getStartTime().after(now) || episode.getEndTime().before(now)) {
//            String resultString =
//                    "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
//                            "<business_trans version=\"1.0\">\n" +
//                            "<status>Error</status>\n" +
//                            "<Returnsinfo>已过使用时间</Returnsinfo>\n" +
//                            "<randomid>" + now.getTime() + "</randomid>\n" +
//                            "</business_trans>";
//            writer.write(resultString);
//            return;
//        }
//
//        if (episode.getNeedAppointmentDate() != null && episode.getNeedAppointmentDate().equals(1)) {
//            List<Player> playerList = ordersService.getOrderPlayers(orders.getOrderId());
//            Date appointmentTime = null;
//            if (playerList.size() > 0) {
//                appointmentTime = playerList.get(0).getAppointmentTime();
//            }
//            if (null == rentDetail.getBorrowTime()) {
//                if (!com.zoo.activity.util.DateUtil.isToday(appointmentTime)) {
//                    String resultString =
//                            "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
//                                    "<business_trans version=\"1.0\">\n" +
//                                    "<status>Error</status>\n" +
//                                    "<Returnsinfo>非预定日期</Returnsinfo>\n" +
//                                    "<randomid>" + now.getTime() + "</randomid>\n" +
//                                    "</business_trans>";
//                    writer.write(resultString);
//                    return;
//                }
//            } else {
//                if (!com.zoo.activity.util.DateUtil.isToday(appointmentTime) && !com.zoo.activity.util.DateUtil.isToday(com.zoo.activity.util.DateUtil.addDay(appointmentTime, 1))) {
//                    String resultString =
//                            "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
//                                    "<business_trans version=\"1.0\">\n" +
//                                    "<status>Error</status>\n" +
//                                    "<Returnsinfo>非预定日期</Returnsinfo>\n" +
//                                    "<randomid>" + now.getTime() + "</randomid>\n" +
//                                    "</business_trans>";
//                    writer.write(resultString);
//                    return;
//                }
//            }
//        }
//
//        Integer dayShow = charge.getDayShow();
//        Integer weekDay = charge.getWeekday();
//
//        String nowFormat = SimpleDateFormatFactory.getInstance("yyyyMMdd").format(now);
//        if (weekDay == 1) {
//            //平日
//            if (!DateTypeUtil.getDateType(nowFormat).equals("0")) {
//                String resultString =
//                        "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
//                                "<business_trans version=\"1.0\">\n" +
//                                "<status>Error</status>\n" +
//                                "<Returnsinfo>您的票券不在使用范围内</Returnsinfo>\n" +
//                                "<randomid>" + now.getTime() + "</randomid>\n" +
//                                "</business_trans>";
//                writer.write(resultString);
//                return;
//            }
//        } else if (weekDay == 2) {
//            //周末
//            if (DateTypeUtil.getDateType(nowFormat).equals("0")) {
//                String resultString =
//                        "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
//                                "<business_trans version=\"1.0\">\n" +
//                                "<status>Error</status>\n" +
//                                "<Returnsinfo>您的票券不在使用范围内</Returnsinfo>\n" +
//                                "<randomid>" + now.getTime() + "</randomid>\n" +
//                                "</business_trans>";
//                writer.write(resultString);
//                return;
//            }
//        } else if (weekDay == 3) {
////            if (!DateTypeUtil.getDateType(nowFormat).equals("2")) {
////                String resultString =
////                        "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
////                                "<business_trans version=\"1.0\">\n" +
////                                "<status>Error</status>\n" +
////                                "<Returnsinfo>您的票券不在使用范围内</Returnsinfo>\n" +
////                                "<randomid>" + now.getTime() + "</randomid>\n" +
////                                "</business_trans>";
////                writer.write(resultString);
////                return;
////            }
//        } else if (weekDay == 4) {
//            if (DateTypeUtil.getDateType(nowFormat).equals("2")) {
//                String resultString =
//                        "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
//                                "<business_trans version=\"1.0\">\n" +
//                                "<status>Error</status>\n" +
//                                "<Returnsinfo>您的票券不在使用范围内</Returnsinfo>\n" +
//                                "<randomid>" + now.getTime() + "</randomid>\n" +
//                                "</business_trans>";
//                writer.write(resultString);
//                return;
//            }
//        }
//
//        if (dayShow == 0) {
//            //夜场
//            if ((nowHour + nowMin.doubleValue() / 60) < (organizer.getNightStart().doubleValue() + organizer.getNightStartMin().doubleValue() / 60) || (nowHour + nowMin.doubleValue() / 60) >= (organizer.getNightEnd().doubleValue() + organizer.getNightEndMin().doubleValue() / 60)) {
//                String resultString =
//                        "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
//                                "<business_trans version=\"1.0\">\n" +
//                                "<status>Error</status>\n" +
//                                "<Returnsinfo>您的票券不在使用范围内</Returnsinfo>\n" +
//                                "<randomid>" + now.getTime() + "</randomid>\n" +
//                                "</business_trans>";
//                writer.write(resultString);
//                return;
//            }
//
//        } else if (dayShow == 1) {
//            //日场
//            if ((nowHour + nowMin.doubleValue() / 60) < (organizer.getDaytimeStart().doubleValue() + organizer.getDaytimeStartMin().doubleValue() / 60) || (nowHour + nowMin.doubleValue() / 60) >= (organizer.getDaytimeEnd().doubleValue() + organizer.getDaytimeEndMin().doubleValue() / 60)) {
//                String resultString =
//                        "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
//                                "<business_trans version=\"1.0\">\n" +
//                                "<status>Error</status>\n" +
//                                "<Returnsinfo>您的票券不在使用范围内</Returnsinfo>\n" +
//                                "<randomid>" + now.getTime() + "</randomid>\n" +
//                                "</business_trans>";
//                writer.write(resultString);
//                return;
//            }
//        } else if (dayShow == 3) {
//            //观光
//            if ((!locationMap.containsKey(posId)) || (locationMap.containsKey(posId) && (locationMap.get(posId).equals(3) || locationMap.get(posId).equals(4)))) {
//                String resultString =
//                        "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
//                                "<business_trans version=\"1.0\">\n" +
//                                "<status>Error</status>\n" +
//                                "<Returnsinfo>您的票券在此范围内禁用</Returnsinfo>\n" +
//                                "<randomid>" + now.getTime() + "</randomid>\n" +
//                                "</business_trans>";
//                writer.write(resultString);
//                return;
//            }
//            if ((nowHour + nowMin.doubleValue() / 60) < (organizer.getDaytimeStart().doubleValue() + organizer.getDaytimeStartMin().doubleValue() / 60) || (nowHour + nowMin.doubleValue() / 60) >= (organizer.getDaytimeEnd().doubleValue() + organizer.getDaytimeEndMin().doubleValue() / 60)) {
//                String resultString =
//                        "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
//                                "<business_trans version=\"1.0\">\n" +
//                                "<status>Error</status>\n" +
//                                "<Returnsinfo>您的票券不在使用范围内</Returnsinfo>\n" +
//                                "<randomid>" + now.getTime() + "</randomid>\n" +
//                                "</business_trans>";
//                writer.write(resultString);
//                return;
//            }
//        } else if (dayShow == 4) {
//            //嘉年华
//            if ((!locationMap.containsKey(posId)) || (locationMap.containsKey(posId) && locationMap.get(posId).equals(2))) {
//                String resultString =
//                        "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
//                                "<business_trans version=\"1.0\">\n" +
//                                "<status>Error</status>\n" +
//                                "<Returnsinfo>您的票券在此范围内禁用</Returnsinfo>\n" +
//                                "<randomid>" + now.getTime() + "</randomid>\n" +
//                                "</business_trans>";
//                writer.write(resultString);
//                return;
//            }
//            if ((nowHour + nowMin.doubleValue() / 60) < (organizer.getDaytimeStart().doubleValue() + organizer.getDaytimeStartMin().doubleValue() / 60) || (nowHour + nowMin.doubleValue() / 60) >= (organizer.getDaytimeEnd().doubleValue() + organizer.getDaytimeEndMin().doubleValue() / 60)) {
//                String resultString =
//                        "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
//                                "<business_trans version=\"1.0\">\n" +
//                                "<status>Error</status>\n" +
//                                "<Returnsinfo>您的票券不在使用范围内</Returnsinfo>\n" +
//                                "<randomid>" + now.getTime() + "</randomid>\n" +
//                                "</business_trans>";
//                writer.write(resultString);
//                return;
//            }
//        }
//        long maxTime;
//        long borrowTime;
//        if (null == rentDetail.getBorrowTime()) {
//            borrowTime = now.getTime();
//        } else {
//            borrowTime = rentDetail.getBorrowTime().getTime();
//        }
//        Double daytime = (organizer.getDaytimeEnd().doubleValue() + organizer.getDaytimeEndMin().doubleValue() / 60) - (organizer.getDaytimeStart().doubleValue() + organizer.getDaytimeStartMin().doubleValue() / 60);
//        if (charge.getValidRange().doubleValue() <= daytime) {
//            maxTime = borrowTime + (long) (charge.getValidRange() * 60 * 60 * 1000);
//        }
////        else if (charge.getValidRange().equals(daytime)) {
////            maxTime = borrowTime + 24 * 60 * 60 * 1000;
////        }
//        else if (charge.getValidRange() < (2 * daytime)) {
//            maxTime = Math.round(borrowTime + (24 + ((daytime % 2) + daytime) / 2) * 60 * 60 * 1000);
//        } else {
//            maxTime = Math.round(borrowTime + (24 + ((daytime % 2) + daytime)) * 60 * 60 * 1000);
//        }
//
//        //矫正maxTime
//        Date max = new Date(maxTime);
//        Calendar calendar = Calendar.getInstance();
//        calendar.setTime(max);
//
//        if (dayShow == 1) {
//            if (calendar.get(Calendar.HOUR_OF_DAY) > organizer.getDaytimeEnd()) {
//                calendar.set(Calendar.HOUR_OF_DAY, organizer.getDaytimeEnd());
//                calendar.set(Calendar.MINUTE, organizer.getDaytimeEndMin());
//                calendar.set(Calendar.SECOND, 0);
//                calendar.set(Calendar.MILLISECOND, 0);
//            } else if (calendar.get(Calendar.HOUR_OF_DAY) < organizer.getDaytimeStart()) {
//                calendar.setTime(com.zoo.activity.util.DateUtil.addDay(max, -1));
//                calendar.set(Calendar.HOUR_OF_DAY, organizer.getDaytimeEnd());
//                calendar.set(Calendar.MINUTE, organizer.getDaytimeEndMin());
//                calendar.set(Calendar.SECOND, 0);
//                calendar.set(Calendar.MILLISECOND, 0);
//            }
//        } else if (dayShow == 0) {
//            if (calendar.get(Calendar.HOUR_OF_DAY) > organizer.getNightEnd()) {
//                calendar.set(Calendar.HOUR_OF_DAY, organizer.getNightEnd());
//                calendar.set(Calendar.MINUTE, organizer.getNightEndMin());
//                calendar.set(Calendar.SECOND, 0);
//                calendar.set(Calendar.MILLISECOND, 0);
//            } else if (calendar.get(Calendar.HOUR_OF_DAY) < organizer.getNightStart()) {
//                calendar.setTime(com.zoo.activity.util.DateUtil.addDay(max, -1));
//                calendar.set(Calendar.HOUR_OF_DAY, organizer.getNightEnd());
//                calendar.set(Calendar.MINUTE, organizer.getNightEndMin());
//                calendar.set(Calendar.SECOND, 0);
//                calendar.set(Calendar.MILLISECOND, 0);
//            }
//        }
//
//        maxTime = calendar.getTime().getTime();
//
//
//        if (null == rentDetail.getBorrowTime()) {
//            rentDetail.setBorrowTime(now);
//            rentDetailMapper.updateByPrimaryKeySelective(rentDetail);
//
//            long currentTime = now.getTime();
//
//            String resultString = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
//                    "<business_trans version=\"1.0\">\n" +
//                    "<request_type>Paycard</request_type>\n" +
//                    "<cardName>" + orders.getChargeName() + "</cardName>\n" +
//                    "<startTime>" + DateUtil.dateToStr(new Date(borrowTime), 4) + "</startTime>\n" +
//                    "<endDate>" + (DateUtil.dateToStr(new Date(maxTime), 4)) + "</endDate>\n" +
//                    "<useNum>0</useNum>\n" +
//                    "<cardState>1</cardState>\n" +
//                    "<theState>1</theState>\n" +
//                    "<randomid>" + currentTime + "</randomid>\n" +
////                        "<security_md5>" + md5String + "</security_md5>\n" +
//                    "</business_trans>";
//            writer.write(resultString);
////            asyncSaveData(parentOrder, orders, rentDetail, cardId, posId, now);
//            if (dayShow == 3) {
//                redisTemplate.opsForValue().set(cardId + locationMap.get(posId).toString(), cardId, charge.getValidRange().longValue(), TimeUnit.HOURS);
//                return;
//            }
//            redisTemplate.opsForValue().set(cardId, cardId, 60, TimeUnit.SECONDS);
//            return;
//
//        } else {
//            if (now.getTime() < maxTime) {
//
//                if (dayShow == 3) {
//                    if (null != redisTemplate.opsForValue().get(cardId + locationMap.get(posId).toString())) {
//                        String resultString =
//                                "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
//                                        "<business_trans version=\"1.0\">\n" +
//                                        "<status>Error</status>\n" +
//                                        "<Returnsinfo>您的票券在此区域只能使用一次</Returnsinfo>\n" +
//                                        "<randomid>" + now.getTime() + "</randomid>\n" +
//                                        "</business_trans>";
//                        writer.write(resultString);
//                        return;
//                    } else {
//                        String resultString = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
//                                "<business_trans version=\"1.0\">\n" +
//                                "<request_type>Paycard</request_type>\n" +
//                                "<cardName>" + orders.getChargeName() + "</cardName>\n" +
//                                "<startTime>" + DateUtil.dateToStr(rentDetail.getBorrowTime(), 4) + "</startTime>\n" +
//                                "<endDate>" + (DateUtil.dateToStr(new Date(maxTime), 4)) + "</endDate>\n" +
//                                "<useNum>0</useNum>\n" +
//                                "<cardState>1</cardState>\n" +
//                                "<theState>1</theState>\n" +
//                                "<randomid>" + now.getTime() + "</randomid>\n" +
////                            "<security_md5>" + md5String + "</security_md5>\n" +
//                                "</business_trans>";
//                        writer.write(resultString);
////                        asyncSaveData(parentOrder, orders, rentDetail, cardId, posId, now);
//                        redisTemplate.opsForValue().set(cardId + locationMap.get(posId).toString(), cardId, charge.getValidRange().longValue(), TimeUnit.HOURS);
//                        return;
//                    }
//                } else {
//
//                    String resultString = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
//                            "<business_trans version=\"1.0\">\n" +
//                            "<request_type>Paycard</request_type>\n" +
//                            "<cardName>" + orders.getChargeName() + "</cardName>\n" +
//                            "<startTime>" + DateUtil.dateToStr(rentDetail.getBorrowTime(), 4) + "</startTime>\n" +
//                            "<endDate>" + (DateUtil.dateToStr(new Date(maxTime), 4)) + "</endDate>\n" +
//                            "<useNum>0</useNum>\n" +
//                            "<cardState>1</cardState>\n" +
//                            "<theState>1</theState>\n" +
//                            "<randomid>" + now.getTime() + "</randomid>\n" +
////                            "<security_md5>" + md5String + "</security_md5>\n" +
//                            "</business_trans>";
//                    writer.write(resultString);
////                    asyncSaveData(parentOrder, orders, rentDetail, cardId, posId, now);
//                    redisTemplate.opsForValue().set(cardId, cardId, 60, TimeUnit.SECONDS);
//                    return;
//                }
//
//            } else {
//                String resultString =
//                        "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
//                                "<business_trans version=\"1.0\">\n" +
//                                "<status>Error</status>\n" +
//                                "<Returnsinfo>卡过期或卡的使用次数用完</Returnsinfo>\n" +
//                                "<randomid>" + now.getTime() + "</randomid>\n" +
//                                "</business_trans>";
//                writer.write(resultString);
//                return;
//            }
//
//        }
//
////        } else {
////            String resultString =
////                    "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
////                            "<business_trans version=\"1.0\">\n" +
////                            "<status>Error</status>\n" +
////                            "<Returnsinfo>验证失败</Returnsinfo>\n" +
////                            "<randomid>" + new Date() + "</randomid>\n" +
////                            "</business_trans>";
////            writer.write(resultString);
////            return;
////        }
//
//    }
//
//    @RequestMapping(value = "/yuangu/check.json", method = RequestMethod.POST)
//    @ResponseBody
//    public Object yuanguCheck(@RequestParam("orderCode") String orderCode, HttpServletRequest request) {
//        JSONObject resultJson = new JSONObject();
//        String secret = request.getHeader("secret");
//        String timestamp = request.getHeader("timestamp");
//        PartnerSystem partnerSystem = partnerSystemService.getByAgentId("yuangu-01");
//        if (!MD5Util.getLow32Md5(partnerSystem.getAppId() + partnerSystem.getAppSecret() + timestamp).equals(secret)) {
//            resultJson.put("IsSuccess", false);
//            resultJson.put("ErrMsg", "验证失败");
//            return resultJson;
//        }
//        if (StringUtils.isBlank(orderCode)) {
//            resultJson.put("IsSuccess", false);
//            resultJson.put("ErrMsg", "订单号不能为空");
//            return resultJson;
//        }
//        Orders parentOrder = ordersService.getOrderDetailByCode(orderCode);
//        if (null == parentOrder) {
//            resultJson.put("IsSuccess", false);
//            resultJson.put("ErrMsg", "无效订单号");
//            return resultJson;
//        }
//        OrderView mainOrder = ordersService.getDefaultChildOrderView(parentOrder.getOrderId());
//        if (mainOrder.getOrderStatus() == 5) {
//            resultJson.put("IsSuccess", false);
//            resultJson.put("ErrMsg", "订单已经核销");
//            return resultJson;
//        }
//        Charge mainCharge = chargeService.getChargeById(mainOrder.getChargeId());
//        String playerMeta = episodeService.getEpisodeByChargeId(mainCharge.getChargeId()).getPlayerMeta();
//        String shoeId = "";
//        JSONArray jsonArray = JSONArray.fromObject(playerMeta);
//        Iterator<Object> iterator = jsonArray.iterator();
//        while (iterator.hasNext()) {
//            JSONObject jsonObject = (JSONObject) iterator.next();
//            if (jsonObject.getString("title").equals("鞋号")) {
//                shoeId = jsonObject.getString("id");
//            }
//        }
//        resultJson.put("IsSuccess", true);
//        resultJson.put("ErrMsg", "ok");
//        JSONObject ticketData = new JSONObject();
//        ticketData.put("TicketNo", parentOrder.getOrderId());
//        ticketData.put("OrderNo", parentOrder.getCode());
//        JSONObject guestInfo = new JSONObject();
//        Player player = playerService.getPlayersByOrder(mainOrder.getOrderId()).get(0);
//        guestInfo.put("GuestName", player.getRealName());
//        guestInfo.put("TelPhone", player.getPhone());
//        guestInfo.put("Sex", player.getGender().equals("male") ? "男" : "女");
//        JSONObject otherInfo = JSONObject.fromObject(new String(player.getOtherInfo()));
//        guestInfo.put("ShoeSize", otherInfo.getString(shoeId));
//        ticketData.put("GuestInfo", guestInfo);
//        String[] ticketNo = new String[mainOrder.getChargeNum()];
//
//        JSONArray billItem = new JSONArray();
//        for (int i = mainOrder.getChargeNum() - mainOrder.getAvailableCount(); i < mainOrder.getChargeNum(); i++) {
//            JSONObject billItemTemp = new JSONObject();
//            billItemTemp.put("TicketNo", mainOrder.getCode() + "-" + String.valueOf(i + 1));
//            billItemTemp.put("ItemCode", mainOrder.getChargeAlias());
//            ticketNo[i] = mainOrder.getCode() + "-" + String.valueOf(i + 1);
//            billItem.add(billItemTemp);
//        }
//        ticketData.put("BillItem", billItem);
//        List<OrderView> childOrders = ordersService.getIncrementChildOrderView(parentOrder.getOrderId());
//        JSONArray leassItem = new JSONArray();
//        for (OrderView o : childOrders) {
//            for (int j = mainOrder.getChargeNum() - mainOrder.getAvailableCount(); j < o.getChargeNum(); j++) {
//                JSONObject leassItemTemp = new JSONObject();
//                leassItemTemp.put("TicketNo", ticketNo[j]);
//                leassItemTemp.put("ItemCode", mainOrder.getChargeAlias());
//                leassItemTemp.put("EquipmentCode", o.getChargeAlias());
//                leassItem.add(leassItemTemp);
//            }
//        }
//        ticketData.put("LeassItem", leassItem);
//        resultJson.put("TicketData", ticketData);
//        return resultJson;
//    }
//
//    @RequestMapping(value = "/yuangu/verify.json", method = RequestMethod.POST)
//    @ResponseBody
//    public Object yuanguVerify(@RequestParam("verifyInfo") String verifyInfo, @RequestParam("orderCode") String ticketNo, HttpServletRequest request) throws Exception {
//
//
//        logger.info("远古调用核销接口--------verifyInfo:" + verifyInfo + "----ticketNo" + ticketNo);
//        JSONObject resultJson = new JSONObject();
//
//        String secret = request.getHeader("secret");
//        String timestamp = request.getHeader("timestamp");
//        PartnerSystem partnerSystem = partnerSystemService.getByAgentId("yuangu-01");
//        if (!MD5Util.getLow32Md5(partnerSystem.getAppId() + partnerSystem.getAppSecret() + timestamp).equals(secret)) {
//            resultJson.put("IsSuccess", false);
//            resultJson.put("ErrMsg", "验证失败");
//            return resultJson;
//        }
//
//        if (StringUtils.isBlank(verifyInfo)) {
//            resultJson.put("IsSuccess", false);
//            resultJson.put("ErrMsg", "核销信息不能为空");
//            return resultJson;
//        }
//        if (StringUtils.isBlank(ticketNo)) {
//            resultJson.put("IsSuccess", false);
//            resultJson.put("ErrMsg", "订单号不能为空");
//            return resultJson;
//        }
//
//        JSONArray verifyInfoArray;
//        try {
//            verifyInfoArray = JSONArray.fromObject(verifyInfo);
//        } catch (Exception e) {
//            logger.error(AssUtils.parse(e));
//            resultJson.put("IsSuccess", false);
//            resultJson.put("ErrMsg", "核销信息有误");
//            return resultJson;
//        }
//
//        if (null == verifyInfoArray || verifyInfoArray.size() == 0) {
//            resultJson.put("IsSuccess", false);
//            resultJson.put("ErrMsg", "核销信息有误");
//            return resultJson;
//        }
//
//        OrderView parentOrder = ordersService.getOrderViewByCode(ticketNo);
//        OrderView mainOrder = ordersService.getDefaultChildOrderView(parentOrder.getOrderId());
//        if (null == mainOrder) {
//            mainOrder = parentOrder;
//        }
//
//        Charge charge = chargeService.getChargeById(mainOrder.getChargeId());
//        Activity activity = activityService.getActivityById(charge.getActivityId());
//
//        Iterator iterator = verifyInfoArray.iterator();
//        JSONObject verifyMap = new JSONObject();
//
//        while (iterator.hasNext()) {
//            JSONObject jsonObject = (JSONObject) iterator.next();
//            String code = jsonObject.getString("ticketNo");
//            Integer num = jsonObject.getInt("num");
//            try {
//                logger.info("远古调用核销接口，开始核销--------" + code);
//                if (null != code && code.contains("-")) {
//                    Orders orders = ordersService.getOrdersByCode(code.split("-")[0]);
//                    verifyService.verify(orders.getOrderId(), num, activity.getTypeId(), orders.getVerifyOrganizer(), "yuangu", 3, null);
//                    PartnerDispatcher.verifyPust4Yuangu(code.split("-")[0]);
//                } else if (null != code) {
//                    Orders orders = ordersService.getOrdersByCode(code);
//                    verifyService.verify(orders.getOrderId(), num, activity.getTypeId(), orders.getVerifyOrganizer(), "yuangu", 3, null);
//                    PartnerDispatcher.verifyPust4Yuangu(code);
//                }
//
//                if (verifyMap.containsKey(code)) {
//                    int count = verifyMap.getInt(code);
//                    verifyMap.put(code, count++);
//                } else {
//                    verifyMap.put(code, 1);
//                }
//
//            } catch (Exception e) {
//                logger.error(AssUtils.parse(e));
//                resultJson.put("IsSuccess", false);
//                resultJson.put("ErrMsg", "核销失败，请联系滑雪族");
//                logger.info("远古核销失败,订单号------" + code);
//                return resultJson;
//            }
//        }
//
//        resultJson.put("IsSuccess", true);
//        resultJson.put("ErrMsg", "核销成功");
//        resultJson.put("Result", verifyMap);
//        asyncVerify(mainOrder.getCode());
//
//        return resultJson;
//    }
//
//    @RequestMapping(value = "/yuangu/refundDeposit.json", method = RequestMethod.POST)
//    @ResponseBody
//    public Object yuanguRefundDeposit(@RequestParam("orderCode") String orderCode, @RequestParam("info") String info, @RequestParam("type") Integer type, HttpServletRequest request) {
//        JSONObject resultJson = new JSONObject();
//        String secret = request.getHeader("secret");
//        String timestamp = request.getHeader("timestamp");
//        PartnerSystem partnerSystem = partnerSystemService.getByAgentId("yuangu-01");
//        if (!MD5Util.getLow32Md5(partnerSystem.getAppId() + partnerSystem.getAppSecret() + timestamp).equals(secret)) {
//            resultJson.put("IsSuccess", false);
//            resultJson.put("ErrMsg", "验证失败");
//            return resultJson;
//        }
//        if (StringUtils.isBlank(orderCode)) {
//            resultJson.put("IsSuccess", false);
//            resultJson.put("ErrMsg", "订单号不能为空");
//            return resultJson;
//        }
//        Orders parentOrder = ordersService.getOrderDetailByCode(orderCode);
//        if (null == parentOrder) {
//            resultJson.put("IsSuccess", false);
//            resultJson.put("ErrMsg", "无效订单号");
//            return resultJson;
//        }
//        Deposit deposit = depositService.getDepositDetailByCode(orderCode);
//        if (null == deposit || deposit.getStatus() != 1) {
//            resultJson.put("IsSuccess", false);
//            resultJson.put("ErrMsg", "无押金信息");
//            return resultJson;
//        }
//
//        if (type == 1) {
//            //全退
//            CommonAccount account = new CommonAccount();
//            account.setCustomerId(0 - parentOrder.getSellerId());
//            if (1 == depositService.dealRefundDeposit(deposit, account, deposit.getAmount(), "", Config.instance().getOpUserId())) {
//                resultJson.put("IsSuccess", true);
//                resultJson.put("ErrMsg", "退还押金成功");
//                return resultJson;
//            } else {
//                resultJson.put("IsSuccess", false);
//                resultJson.put("ErrMsg", "押金退还失败，请联系滑雪族");
//                return resultJson;
//            }
//
//        } else if (type == 0) {
//            //不退
//            deposit.setStatus(4);
//            depositService.updateDeposit(deposit);
//            resultJson.put("IsSuccess", true);
//            resultJson.put("ErrMsg", "处理成功");
//            return resultJson;
//        }
//        return resultJson;
//    }
//
//    public static int getWeekOfDate(Date dt) {
//        Calendar cal = Calendar.getInstance();
//        cal.setTime(dt);
//        int w = cal.get(Calendar.DAY_OF_WEEK) - 1;
//        if (w < 0)
//            w = 0;
//        return w;
//    }
//
//    private static class VerifyThread implements Runnable {
//        private String code;
//
//        public VerifyThread(String code) {
//            this.code = code;
//        }
//
//        @Override
//        public void run() {
//            try {
//                Date now = new Date();
//                OrdersService ordersService = SpringContextUtil.getBean(OrdersService.class);
//                VerifyService verifyService = SpringContextUtil.getBean(VerifyService.class);
//                DepositService depositService = SpringContextUtil.getBean(DepositService.class);
//                RentDetailMapper rentDetailMapper = SpringContextUtil.getBean(RentDetailMapper.class);
//                OrderView mainOrder = ordersService.getOrderViewByCode(code);
//                if (mainOrder.getOrderStatus() == 5) {
//                    OrderView parentOrder = ordersService.getOrderDetail(mainOrder.getParentId());
//                    if (null != parentOrder) {
//                        List<OrderView> childOrders = ordersService.getIncrementChildOrderView(parentOrder.getOrderId());
//                        for (OrderView o : childOrders) {
//                            if (o.getOrderStatus() == 1) {
//                                verifyService.verify(o.getOrderId(), o.getAvailableCount(), o.getTypeId(), o.getVerifyOrganizer(), null, 3, null);
//                                PartnerDispatcher.verifyPust4Yuangu(o.getCode());
//                            }
//                        }
//
//                        List<EquipmentRelationships> relationshipsByParentOrderId = depositService.getRelationshipsByParentOrderId(parentOrder.getOrderId());
//
//                        for (EquipmentRelationships e : relationshipsByParentOrderId) {
//                            if (e.getStatus() != 1) {
//                                List<RentDetail> rentDetails = rentDetailMapper.getByRelationIdAndStatus(e.getRelationId(), 0);
//                                for (RentDetail r : rentDetails) {
//                                    r.setBorrowTime(now);
//                                    r.setReturnTime(now);
//                                    r.setStatus(2);
//                                    rentDetailMapper.updateByPrimaryKeySelective(r);
//                                }
//                                depositService.check(e.getRelationId());
//                            }
//                        }
//
//                    }
//
//                }
//
//
//            } catch (Exception e) {
//                logger.error(AssUtils.parse(e));
//            }
//
//        }
//    }
//
//    private static void asyncVerify(String code) {
//        taskExecutor.execute(new VerifyThread(code));
//    }
//
//    private static class SavaDataThread implements Runnable {
//
//        private OrderView parentOrder;
//        private OrderView mainOrder;
//        private RentDetail rentDetail;
//        private String cardId;
//        private String posId;
//        private Date now;
//
//        public SavaDataThread(OrderView parentOrder, OrderView mainOrder, RentDetail rentDetail, String cardId, String posId, Date now) {
//            this.parentOrder = parentOrder;
//            this.mainOrder = mainOrder;
//            this.rentDetail = rentDetail;
//            this.cardId = cardId;
//            this.posId = posId;
//            this.now = now;
//        }
//
//        @Override
//        public void run() {
//
//            AccessData accessData = new AccessData();
//            accessData.setCardId(cardId);
//            accessData.setPosId(posId);
//            accessData.setTime(now);
//            accessData.setId(MongoUtil.getNextId("AccessData"));
//
//            ChargeService chargeService = SpringContextUtil.getBean(ChargeService.class);
//            Charge charge = chargeService.getChargeById(mainOrder.getChargeId());
//            accessData.setChargeId(charge.getChargeId());
//            accessData.setChargeName(charge.getChargeName());
//
//            EpisodeService episodeService = SpringContextUtil.getBean(EpisodeService.class);
//            Episode episode = episodeService.getEpisodeDetailById(charge.getEpisodeId());
//            accessData.setEpisodeId(episode.getEpisodeId());
//            accessData.setEpisodeName(episode.getName());
//
//            accessData.setActivityId(parentOrder.getActivityId());
//            accessData.setActivityName(parentOrder.getTitle());
//
//            accessData.setPayType(parentOrder.getPayType());
//            accessData.setFirstTime(rentDetail.getBorrowTime());
//
//            accessData.setDetailId(rentDetail.getDetailId());
//            accessData.setMaterialId(rentDetail.getMaterialId());
//            accessData.setOrderId(parentOrder.getOrderId());
//            accessData.setStatus(1);
//            accessData.setOrgId(mainOrder.getVerifyOrganizer());
//            accessData.setOrgName(mainOrder.getOrgName());
//            accessData.setCustomerId(parentOrder.getBuyerId());
//            if (parentOrder.getPayType().equals("微信支付")) {
//                CommonAccount account = (CommonAccount) AccountDispatcher.getAccount(AccountEnum.FetchAccountTypeEnum.FETCH_BY_CUSTOMER.getValue(), parentOrder.getBuyerId().toString()).getData();
//                accessData.setCustomerName(account.getNickname());
//                accessData.setPhone(account.getPhone());
//            } else {
//                PlayerService playerService = SpringContextUtil.getBean(PlayerService.class);
//                Player player = playerService.getPlayersByOrder(mainOrder.getOrderId()).get(0);
//                accessData.setCustomerName(player.getRealName());
//                accessData.setPhone(player.getPhone());
//            }
//
//            MongoTemplate mongoTemplate = SpringContextUtil.getBean(MongoTemplate.class);
//            mongoTemplate.insert(accessData, "AccessData");
//
//        }
//    }
//
//    private static void asyncSaveData(OrderView parentOrder, OrderView mainOrder, RentDetail rentDetail, String cardId, String posId, Date now) {
//        taskExecutor.execute(new SavaDataThread(parentOrder, mainOrder, rentDetail, cardId, posId, now));
//    }
//
//    @RequestMapping(value = "/oudao/push.json")
//    @ResponseBody
//    public Object ouDaoPush(HttpServletRequest request) throws IOException {
//        String str = "";
//        Enumeration enu = request.getParameterNames();
//        while (enu.hasMoreElements()) {
//            str = (String) enu.nextElement();
//            logger.info("oudao参数" + str + ": " + request.getParameter(str));
//        }
//        logger.info("oudao推送" + str);
//        ResponseData responseData = new ResponseData();
//        if (StringUtils.isBlank(str)) {
//            responseData.setStatus(Status.error);
//            responseData.setMsg("请求数据为空");
//            return responseData;
//        }
//        try {
//            JSONObject jsonObject = JSONObject.fromObject(str);
//            JSONArray jsonArray = jsonObject.getJSONArray("Orders");
//            Iterator<JSONObject> iterator = jsonArray.iterator();
//            while (iterator.hasNext()) {
//                JSONObject order = iterator.next();
//                String code = order.getString("ExternalId");
//                OrderView orderView = ordersService.getOrderViewByCode(code);
//                if (null == orderView) {
////                    responseData.setStatus(Status.error);
////                    responseData.setMsg("订单" + code + "不存在");
////                    return responseData;
//                    logger.info("订单" + code + "不存在");
//                    continue;
//                }
//                if (orderView.getOrderStatus() != 1) {
//                    logger.info("订单" + code + "不可核销, 订单状态为：" + orderView.getOrderStatus());
//                    continue;
//                }
//                Activity activity = activityService.getActivityById(orderView.getActivityId());
//                try {
//                    verifyService.verify(orderView.getOrderId(), orderView.getAvailableCount(), activity.getTypeId(), orderView.getVerifyOrganizer(), "oudao", 3, null);
//                } catch (Exception e) {
//                    logger.info("订单" + code + "核销失败,抛出异常");
//                    logger.error(AssUtils.parse(e));
////                    responseData.setStatus(Status.error);
////                    responseData.setMsg("核销失败，请联系滑雪族");
////                    return responseData;
//                    continue;
//                }
//            }
//
//        } catch (Exception e) {
//            logger.error(AssUtils.parse(e));
//            responseData.setStatus(Status.error);
//            responseData.setMsg("请求数据格式错误");
//            return responseData;
//        }
//        responseData.setStatus(Status.success);
//        responseData.setMsg("核销成功");
//        return responseData;
//    }
//
//    /**
//     * 浩瀚推送核销
//     *
//     * @return
//     */
//    @RequestMapping(value = "/haohan/push.json")
//    @ResponseBody
//    public Object hanHanPush(HttpServletRequest request) throws IOException {
//        JSONObject resultJson = new JSONObject();
//
//        StringBuffer sb = new StringBuffer();
//        InputStream is = request.getInputStream();
//        InputStreamReader isr = new InputStreamReader(is);
//        BufferedReader br = new BufferedReader(isr);
//        String s = "";
//        while ((s = br.readLine()) != null) {
//            sb.append(s);
//        }
//        String jsonStr = sb.toString();
//        logger.info("haohan推送:" + jsonStr);
//        if (jsonStr.equals("")) {
//            resultJson.put("code", 400);
//            resultJson.put("msg", "推送数据为空");
//            logger.info("haohan______推送数据为空");
//            return resultJson;
//        }
//        JSONObject jsonObject = JSONObject.fromObject(jsonStr);
//        try {
//            if (!jsonObject.containsKey("OutOrderCode") || jsonObject.get("OutOrderCode").equals("")) {
//                resultJson.put("code", 401);
//                resultJson.put("msg", "OutOrderCode外部订单未推送");
//                logger.info("haohan______OutOrderCode外部订单未推送");
//                return resultJson;
//            }
//            String orderCode = jsonObject.getString("OutOrderCode");
//            OrderView orderView = ordersService.getOrderViewByCode(orderCode);
//            if (orderView == null) {
//                resultJson.put("code", 402);
//                resultJson.put("msg", "订单" + orderCode + ",在滑雪族系统中不存在");
//                logger.info("haohan______订单" + orderCode + "不存在");
//                return resultJson;
//            }
//            logger.info("orderView:" + orderView.toString());
//            ActivityWithBLOBs activity = activityService.getActivityById(orderView.getActivityId());
//            try {
//                verifyService.verify(orderView.getOrderId(), orderView.getAvailableCount(), activity.getTypeId(), orderView.getVerifyOrganizer(), "haohan", 3, null);
//            } catch (Exception e) {
//                logger.info("haohan______订单" + orderCode + ", 核销失败，抛出异常");
//                logger.error(AssUtils.parse(e));
//                resultJson.put("code", 500);
//                resultJson.put("msg", "核销失败，请联系滑雪族");
//                return resultJson;
//            }
//
//        } catch (Exception e) {
//            logger.error(AssUtils.parse(e));
//            resultJson.put("code", 500);
//            resultJson.put("msg", "内部异常错误");
//            return resultJson;
//        }
//
//        return "OK";
//    }
//
//    @RequestMapping(value = "/ice/push.json", method = RequestMethod.POST)
//    @ResponseBody
//    public Object icePush(HttpServletRequest request) throws Exception {
//
//        StringBuffer sb = new StringBuffer();
//        InputStream is = request.getInputStream();
//        InputStreamReader isr = new InputStreamReader(is);
//        BufferedReader br = new BufferedReader(isr);
//        String s = "";
//        while ((s = br.readLine()) != null) {
//            sb.append(s);
//        }
//        String json = URLDecoder.decode(sb.toString(), "utf-8");
//
//        Map<String, Object> requestMap;
//        JSONObject resultJson;
//        JSONObject requestJson;
//        logger.info("中软对接数据:" + json);
//        try {
//            requestJson = JSONObject.fromObject(json);
//        } catch (Exception e) {
//            logger.error(AssUtils.parse(e));
//            resultJson = EntryConstant.buildJsonResponse(EntryConstant.EntryStatusEnum.INVALID_PARAMS.getStatusCode(), EntryConstant.EntryStatusEnum.INVALID_PARAMS.getErrorMsg(), null, null, null);
//            return resultJson;
//        }
//
//        requestMap = EntryConstant.requestCheck(requestJson);
//
//        if (requestMap.containsKey("statusCode") && !requestMap.get("statusCode").equals(EntryConstant.EntryStatusEnum.SUCCESS)) {
//            resultJson = EntryConstant.buildJsonResponse((Integer) requestMap.get("statusCode"), requestMap.get("errorMsg").toString(), null, null, null);
//            return resultJson;
//        }
//
//        String body = requestMap.get("body").toString();
//        String key = requestMap.get("key").toString();
//        Integer userId = Integer.valueOf(requestMap.get("userId").toString());
//        String method = requestMap.get("method").toString();
//        String sequence = DesUtils.createSequence();
//
//        String contentResult = null;
//        JSONObject bodyJson;
//        try {
//            contentResult = DesUtils.decryptBody(body.getBytes(), key.getBytes());
//            if (null == contentResult) {
//                contentResult = new String(new BASE64Decoder().decodeBuffer(body));
//            }
//            bodyJson = JSONObject.fromObject(contentResult);
//        } catch (Exception e) {
//            logger.error(AssUtils.parse(e));
//            resultJson = EntryConstant.buildJsonResponse(EntryConstant.EntryStatusEnum.INVALID_PARAMS.getStatusCode(), EntryConstant.EntryStatusEnum.INVALID_PARAMS.getErrorMsg(), null, null, null);
//            return resultJson;
//        }
//        Map<String, Object> resultMap;
//        String type = bodyJson.getString("type");
//        String cardId = bodyJson.getString("cardId");
//        if (type.equals("card") && cardId.equals("91E6ACCA")) {
//            return EntryConstant.buildJsonResponse(EntryConstant.EntryStatusEnum.SUCCESS.getStatusCode(), EntryConstant.EntryStatusEnum.SUCCESS.getErrorMsg(), "", EntryConstant.getSign(sequence + EntryConstant.EntryStatusEnum.SUCCESS.getStatusCode().toString() + key), sequence);
//        }
//
//        Entry entry = EntryConstant.getEntry(cardId, userId, type);
//
//        if (null == entry) {
//            resultJson = EntryConstant.buildJsonResponse(EntryConstant.EntryStatusEnum.INVALID_CODE.getStatusCode(), EntryConstant.EntryStatusEnum.INVALID_CODE.getErrorMsg(), null, null, null);
//            return resultJson;
//        }
//        switch (method) {
//            case "getIn":
//                resultMap = enterService.getIn4Ice(entry, userId);
//                break;
//            case "getOut":
//                resultMap = enterService.getOut4Ice(entry, userId);
//                break;
//            default:
//                resultJson = EntryConstant.buildJsonResponse(EntryConstant.EntryStatusEnum.INVALID_METHOD.getStatusCode(), EntryConstant.EntryStatusEnum.INVALID_METHOD.getErrorMsg(), null, null, null);
//                return resultJson;
//        }
//
//        if (resultMap.containsKey("statusCode") && !resultMap.get("statusCode").equals(EntryConstant.EntryStatusEnum.SUCCESS.getStatusCode())) {
//            resultJson = EntryConstant.buildJsonResponse((Integer) resultMap.get("statusCode"), resultMap.get("errorMsg").toString(), null, null, null);
//            return resultJson;
//        }
//        resultJson = EntryConstant.buildJsonResponse((Integer) resultMap.get("statusCode"), resultMap.get("errorMsg").toString(), resultMap.get("body").toString(), EntryConstant.getSign(sequence + resultMap.get("statusCode").toString() + key), sequence);
//
//        return resultJson;
//    }
//
//}
