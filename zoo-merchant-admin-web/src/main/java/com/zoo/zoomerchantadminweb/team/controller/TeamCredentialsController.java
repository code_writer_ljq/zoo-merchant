package com.zoo.zoomerchantadminweb.team.controller;


import com.alibaba.fastjson.JSONArray;
import com.zoo.activity.dao.model.*;
import com.zoo.activity.dao.nutz.pojo.CommonAccount;
import com.zoo.activity.service.EpisodeService;
import com.zoo.activity.service.OrdersService;
import com.zoo.activity.service.PlayerService;
import com.zoo.activity.service.TeamCredentialsService;
import com.zoo.common.page.PageList;
import com.zoo.web.bean.WebResult;
import com.zoo.zoomerchantadminweb.context.WebUpmsContext;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/server/team/credentials")
@Api(value = "api/server/team/credentials", tags = {"团队订单相关接口"}, description = "团队订单相关接口")
public class TeamCredentialsController {

    @Autowired
    private TeamCredentialsService teamCredentialsService;

    @Autowired
    private PlayerService playerService;

    @Autowired
    private EpisodeService episodeService;

    @Autowired
    private OrdersService ordersService;

    @ApiOperation(value = "我的团队单列表", notes = "我的团队单列表", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页", required = false, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "size", value = "条", required = false, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "code", value = "订单号", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "orderCode", value = "个人订单号", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "companyName", value = "旅游团", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "contactName", value = "联系人", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "contactPhone", value = "联系人电话", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "productId", value = "产品Id", required = false, dataType = "Integer", paramType = "query"),
            @ApiImplicitParam(name = "productName", value = "产品名称", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "customer", value = "下单销售", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "payTypeId", value = "付款方式", required = false, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "startTime", value = "下单开始时间", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "endTime", value = "下单结束时间", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "startAppointmentTime", value = "到达开始时间", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "endAppointmentTime", value = "到达结束时间", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "status", value = "审批状态", required = false, dataType = "string", paramType = "query")
    })
    @GetMapping(value = "/list.json")
    public WebResult getTeamCredentialsList(@RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
                                            @RequestParam(value = "size", required = false, defaultValue = "10") Integer size,
                                            @RequestParam(value = "code", required = false) String code,
                                            @RequestParam(value = "orderCode", required = false) String orderCode,
                                            @RequestParam(value = "companyName", required = false) String companyName,
                                            @RequestParam(value = "contentName", required = false) String contactName,
                                            @RequestParam(value = "contentPhone", required = false) String contactPhone,
                                            @RequestParam(value = "productId", required = false) Integer productId,
                                            @RequestParam(value = "productName", required = false) String productName,
                                            @RequestParam(value = "customer", required = false) Integer customer,
                                            @RequestParam(value = "payTypeId", required = false) Integer payTypeId,
                                            @RequestParam(value = "startTime", required = false) String startTime,
                                            @RequestParam(value = "endTime", required = false) String endTime,
                                            @RequestParam(value = "startAppointmentTime", required = false) String startAppointmentTime,
                                            @RequestParam(value = "endAppointmentTime", required = false) String endAppointmentTime,
                                            @RequestParam(value = "status", required = false) String status, HttpServletRequest request){
//        CommonAccount account = WebUpmsContext.getAccount(request);
//        if(account.getIsnowType() == 1 && "*".equals(account.getIsnowPermission())){
//            customer = null;
//        }
//        if(null == customer){String startAppointmentTime, String endAppointmentTime
//            customer = account.getCustomerId();
//        }
        Organizer orgnization = WebUpmsContext.getOrgnization(request);
        if (StringUtils.isBlank(status) || status.equals("[]")) {
            status = null;
        } else {
            status = "(" + status + ")";
        }
        if(StringUtils.isNotEmpty(contactPhone)){
            contactPhone = contactPhone.trim();
        }
        PageList pageList = teamCredentialsService.getTeamCredentialsList(page, size, orgnization.getOrganizerId(), code, orderCode, companyName, customer, payTypeId,  startTime, endTime, status, startAppointmentTime, endAppointmentTime, contactName, contactPhone, productName, productId);
        WebResult webResult = WebResult.getSuccessResult();
        webResult.put("data", pageList);
        return webResult;
    }

    @ApiOperation(value = "团队详情信息", notes = "团队详情信息", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "teamId", value = "团队ID)", required = false, dataType = "string", paramType = "query"),
    })
    @RequestMapping(value = "/detail.json", method = RequestMethod.POST)
    public Object confirmPayType(@RequestParam(value = "teamId", required = false) Integer teamId,
                                 HttpServletRequest request) {
        CommonAccount account = WebUpmsContext.getAccount(request);
        HashMap<String, Object> result = new HashMap<>();
        TeamCredentials teamCredentials = teamCredentialsService.selectByPrimaryKey(teamId);

        // 组装下单前报名数据
        HashMap<Integer, HashMap<String, Object>> map = new HashMap<>();

        List<Map> playerList = new ArrayList();
        List<Player> players = playerService.getPlayerListByTeamId(teamId);
        Map<Integer, List<Player>> groupMap = players.stream().collect(Collectors.groupingBy(Player::getOrderId));
        groupMap.forEach((k, v) -> {
            HashMap<String, Object> resultMap = new HashMap<>();
            resultMap.put("index", map.size() + 1);
            resultMap.put("realName", v.get(0).getRealName());
            resultMap.put("phone", v.get(0).getPhone());
            resultMap.put("number", v.size());
            map.put(k, resultMap);
        });

        for (Integer key : map.keySet()) {
            playerList.add(map.get(key));
        }

        List<Episode> episodes = episodeService.getEpisodeDetailWithChargesByActivityId(teamCredentials.getProductId(), account.getOrganizerId(), 0);
        result.put("productDetail", episodes);
        result.put("teamDetail", teamCredentials);
        result.put("playerList", playerList);

        return WebResult.getSuccessResult("data", result);
    }

    @ApiOperation(value = "团队状态更新", notes = "团队状态更新", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "teamId", value = "团队单ID", required = true, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "payType", value = "付款方式", required = false, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "status", value = "审批状态", required = false, dataType = "integer", paramType = "query"),
    })
    @PostMapping("/updateTeamStatus.json")
    public WebResult updateTeamStatus(@RequestParam("teamId") Integer teamId,
                                      @RequestParam(value = "payType", required = false) Integer payType,
                                      @RequestParam(value = "status", required = false) Integer status, HttpServletRequest request) {
        Organizer orgnization = WebUpmsContext.getOrgnization(request);
        CommonAccount account = WebUpmsContext.getAccount(request);
        if (teamCredentialsService.updateTeamStatus(teamId, payType, status, orgnization.getOrganizerId(), account) > 0) {
            return WebResult.getSuccessResult();
        }
        return WebResult.getErrorResult(WebResult.Code.UPDATE_FAILED);
    }

    @ApiOperation(value = "我的团队订单更新", notes = "我的团队订单更新", httpMethod = "POST")
    @PostMapping("/update.json")
    public WebResult update(TeamCredentials teamCredentials, HttpServletRequest request){
        teamCredentials.setId(Integer.valueOf(request.getParameter("teamId")));
        if(teamCredentialsService.update(teamCredentials) > 0){
            return WebResult.getSuccessResult();
        }
        return WebResult.getErrorResult(WebResult.Code.UPDATE_FAILED);
    }

    @ApiOperation(value = "我的团队订单详情", notes = "我的团队订单详情", httpMethod = "GET")
    @GetMapping("/detail.json")
    public WebResult getDetail(String code) {
        TeamCredentials teamCredentials = teamCredentialsService.selectByCode(code);
        WebResult webResult = WebResult.getSuccessResult();
        webResult.put("data", teamCredentials);
        return webResult;
    }

    @ApiOperation(value = "发送短信", notes = "发送短信", httpMethod = "POST")
    @PostMapping("/sendSms.json")
    public WebResult sendSms(@RequestParam(value = "json", required = false) String json, @RequestParam("teamId") Integer teamId, HttpServletRequest request) {
        Organizer orgnization = WebUpmsContext.getOrgnization(request);
        List<Map> list = Collections.emptyList();
        if(StringUtils.isNotEmpty(json)) {
            list = JSONArray.parseArray(json, Map.class);
            
            list.forEach(e -> {
                if("***".equals(e.get("code").toString())){
                    Orders orders = ordersService.getSimpleOrdersById(Integer.parseInt(e.get("orderId").toString()));
                    if(orders != null) e.put("code", orders.getCode());
                }
            });
        }else {
            List<Map> players = playerService.getPlayerByTeamCredentialsId(teamId);

            if(!CollectionUtils.isEmpty(players)){
                players =  players.stream().filter(e -> !e.get("order_status").toString().equals("3")).collect(Collectors.toList());
                list = players.stream().map(e -> {
                        Map map = new HashMap();
                        map.put("code", e.get("code"));
                        map.put("phone", e.get("phone"));
                        map.put("orderId", e.get("order_id"));
                        map.put("realName", e.get("real_name"));
                        return map;
                }).collect(Collectors.toList());

            }else {
                return WebResult.getErrorResult("发送短信失败！");
            }
        }

        return teamCredentialsService.SendSms(list, orgnization, teamId);
    }
}
