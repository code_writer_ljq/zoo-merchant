package com.zoo.zoomerchantadminweb.team.controller;


import com.zoo.activity.dao.model.ApprovalRecord;
import com.zoo.activity.dao.model.ApprovalRecordExample;
import com.zoo.activity.dao.model.TeamCredentialsExample;
import com.zoo.activity.service.ApprovalRecordService;
import com.zoo.web.bean.WebResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/server/approval/record")
@Api(value = "api/server/approval/record", tags = {"审批记录相关接口"}, description = "审批记录相关接口")
public class ApprovalRecordController {

    @Autowired
    private ApprovalRecordService approvalRecordService;

    @ApiOperation(value = "添加审批记录", notes = "添加审批记录", httpMethod = "POST")
    @PostMapping("/save.json")
    public WebResult create(ApprovalRecord approvalRecord){
        if(approvalRecordService.create(approvalRecord) <= 0){
            return WebResult.getErrorResult(WebResult.Code.ADD_FAILED);
        }
        return WebResult.getSuccessResult();
    }

    @ApiOperation(value = "查询审批记录", notes = "查询审批记录", httpMethod = "GET")
    @ApiImplicitParam(name = "teamCredentialsId", value = "团队订单ID", required = true, dataType = "integer", paramType = "query")
    @GetMapping("/listByTeamCredentialsId.json")
    public WebResult listByTeamCredentialsId(@RequestParam(name = "teamCredentialsId") Integer teamCredentialsId){
        ApprovalRecordExample example = new ApprovalRecordExample();
        example.createCriteria().andTeamCredentialsIdEqualTo(teamCredentialsId).andValidEqualTo(0);
        List<ApprovalRecord> approvalRecords = approvalRecordService.selectByExample(example);
        WebResult result = WebResult.getSuccessResult();
        result.put("data", approvalRecords);
        return result;
    }
}
