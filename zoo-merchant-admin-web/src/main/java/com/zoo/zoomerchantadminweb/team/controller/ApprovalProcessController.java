package com.zoo.zoomerchantadminweb.team.controller;


import com.zoo.activity.dao.model.ApprovalProcess;
import com.zoo.activity.dao.model.ApprovalProcessExample;
import com.zoo.activity.dao.model.Organizer;
import com.zoo.activity.service.ApprovalProcessService;
import com.zoo.web.bean.WebResult;
import com.zoo.zoomerchantadminweb.context.WebUpmsContext;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

@RestController
@RequestMapping("/api/server/approval/process")
@Api(value = "api/server/approval/process", tags = {"审批流程相关接口"}, description = "审批流程相关接口")
public class ApprovalProcessController {

    @Autowired
    private ApprovalProcessService approvalProcessService;

    @ApiOperation(value = "添加审批流程", notes = "添加审批流程", httpMethod = "POST")
    @PostMapping("/save.json")
    public WebResult create(HttpServletRequest request, ApprovalProcess approvalProcess){
        Organizer organizer = WebUpmsContext.getOrgnization(request);

        ApprovalProcessExample approvalProcessExample = new ApprovalProcessExample();
        approvalProcessExample.createCriteria()
                .andOrganizerIdEqualTo(organizer.getOrganizerId())
                .andValidEqualTo(1);
        ApprovalProcess beforeApprovalProcess = approvalProcessService.selectFirstByExample(approvalProcessExample);
        if(beforeApprovalProcess != null){
            if(!beforeApprovalProcess.getCustomerId().equals(approvalProcess.getCustomerId())){
                beforeApprovalProcess.setValid(0);
                approvalProcessService.updateByPrimaryKey(beforeApprovalProcess);
            }else {
                return WebResult.getSuccessResult();
            }
        }
        approvalProcess.setOrganizerId(organizer.getOrganizerId());
        approvalProcess.setValid(1);
        Date date = new Date();
        approvalProcess.setCreateTime(date);
        approvalProcess.setUpdateTime(date);
        if(approvalProcessService.create(approvalProcess) <= 0){
            return WebResult.getErrorResult(WebResult.Code.ADD_FAILED);
        }
        return WebResult.getSuccessResult();
    }

    @ApiOperation(value = "查询当前审批流程", notes = "查询当前审批流程", httpMethod = "GET")
    @GetMapping("/getCurrentApprovalProcess.json")
    public WebResult getCurrentApprovalProcess(HttpServletRequest request){
        Organizer organizer = WebUpmsContext.getOrgnization(request);

        ApprovalProcessExample approvalProcessExample = new ApprovalProcessExample();
        approvalProcessExample.createCriteria()
                .andOrganizerIdEqualTo(organizer.getOrganizerId())
                .andValidEqualTo(1)
                .andTypeEqualTo(0);
        ApprovalProcess approvalProcess = approvalProcessService.selectFirstByExample(approvalProcessExample);
        WebResult<Object> successResult = WebResult.getSuccessResult();
        successResult.put("data", approvalProcess);
        return successResult;
    }
}
