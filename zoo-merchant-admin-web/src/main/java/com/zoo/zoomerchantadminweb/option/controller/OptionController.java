package com.zoo.zoomerchantadminweb.option.controller;

import com.zoo.activity.dao.model.*;
import com.zoo.activity.dao.nutz.pojo.ActivityRelation;
import com.zoo.activity.dao.nutz.pojo.CommonAccount;
import com.zoo.activity.dao.nutz.pojo.RentEquipment;
import com.zoo.activity.service.*;
import com.zoo.hotel.service.HotelService;
import com.zoo.web.bean.WebResult;
import com.zoo.wechat.common.Config;
import com.zoo.zoomerchantadminweb.context.WebUpmsContext;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.*;
import java.util.stream.Collectors;

@Api(value = "api/server/option", tags = {"主办方用于下拉列表的接口"}, description = "主办方下拉列表接口描述")
@RestController
@RequestMapping("api/server/option")
public class OptionController {

    @Autowired
    private DistributionAccountService distributionAccountService;

    @Resource(name = "activityService")
    private ActivityService activityService;

    @Resource(name = "organizerService")
    private OrganizerService organizerService;

    @Resource(name = "episodeService")
    private EpisodeService episodeService;

    @Resource(name = "articleService")
    private ArticleService articleService;

    @Resource(name = "topicService")
    private TopicService topicService;

    @Resource(name = "hotelService")
    private HotelService hotelService;

    @Resource(name = "depositService")
    private DepositService depositService;

    @Resource(name = "distributionService")
    private DistributionService distributionService;

    @Autowired
    private ArticleCategoryService articleCategoryService;

    @Autowired
    private TagService activityTagService;

    @Autowired
    private TagRelationShipsService tagRelationShipsService;

    @Autowired
    private OrdersService ordersService;

    @Autowired
    private SkiFieldService skiFieldService;

    @Autowired
    private ChargeService chargeService;

    @Autowired
    private SystemConfigService systemConfigService;

    /**
     * 获取当前的organizer包含的activity基本信息,前台下拉列表使用,
     * 不包含指定教练预约
     */

    @ApiOperation(value = "主办方产品列表", notes = "获取当前的organizer包含的activity基本信息,前台下拉列表使用", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "typeId", value = "产品种类", required = false, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "isEnd", value = "是否结束", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "hasCoach", value = "是否包含教练", required = false, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "cardType", value = "次卡种类", required = false, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "distribution", value = "是否为分销产品", required = false, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "courseType", value = "课程种类", required = false, dataType = "integer", paramType = "query"),
    })
    @RequestMapping(value = "/activity.json", method = RequestMethod.GET)
    public WebResult activityOption(@RequestParam(value = "typeId", required = false) Integer typeId,
                                    @RequestParam(value = "isEnd", required = false) String isEnd,
                                    @RequestParam(value = "hasCoach", required = false, defaultValue = "0") Integer hasCoach,
                                    @RequestParam(value = "cardType", required = false) String cardType,
                                    @RequestParam(value = "distribution", required = false) Integer distribution,
                                    @RequestParam(value = "rent", required = false, defaultValue = "0") Integer rent,
                                    @RequestParam(value = "courseType", required = false) Integer courseType,
                                    HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
//        if ("0".equals(cardType)) {
//            cardType = "1";
//        }
        if (null != organizer) {
            List<Map> activityList;
            // 解析要查询产品的类型ID
            List<Integer> typeIds = new ArrayList<>();
            if (hasCoach == 1) {
                // 教练查询相关查询，改造一下。   以后再增加在产品类型就不用修改程序计算二进制TYPE了，也不用再更新代码升级服务器了。
                typeIds.add(2);
                typeIds.add(8);
            } else {
                if (typeId != null) {
                    List<ActivityType> allowTypes = organizerService.getOrganizerActivityAuth(organizer.getType());
                    for (ActivityType activityType : allowTypes) {
                        if (activityType.getTypeId().equals(typeId) && typeId != 4) {
                            typeIds.add(typeId);
                        }
                    }
                }
            }


            Integer cardTypeNum = null;
            if(StringUtils.isNotEmpty(cardType)){
                cardTypeNum = Integer.parseInt(cardType);
            }
            if (typeIds.size() == 0)
                typeIds = null;
            if (hasCoach == 1) {
                activityList = episodeService.getCourseTitleWithCoach(organizer.getOrganizerId(), typeIds, isEnd, distribution, courseType);
            } else {
                activityList = activityService.getSimpleInfo(organizer.getOrganizerId(), typeIds, isEnd, cardTypeNum, distribution, rent);
            }

            return WebResult.getSuccessResult("data", activityList);
        } else {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }

    }

    @ApiOperation(value = "分销订单列表", notes = "获取当前的organizer包含的activity、charge信息", httpMethod = "GET")
    @ApiImplicitParams({
    })
    @RequestMapping(value = "/distribution/activityTitle.json", method = RequestMethod.GET)
    public WebResult distributionActivityTitleOption(HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (null != organizer) {
            return WebResult.getSuccessResult("data", activityService.getDistributionActivityTitleList(organizer.getOrganizerId()));
        } else {
            return WebResult.getErrorResult(WebResult.Code.COACH_GET_ORGANIZER_INFO_ERROR);
        }
    }

    @ApiOperation(value = "分配分销产品列表", notes = "获取当前的organizer包含的activity、charge信息", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "distributionAccountId", value = "分销员ID", required = false, dataType = "int", paramType = "query")
    })
    @RequestMapping(value = "/distribution/activity.json", method = RequestMethod.GET)
    public WebResult distributionActivityOption(@RequestParam(name = "distributionAccountId", required = false) Integer distributionAccountId,
                                                HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (null != organizer) {
            return WebResult.getSuccessResult("data", activityService.getDistributionActivityList(organizer.getOrganizerId(), distributionAccountId));
        } else {
            return WebResult.getErrorResult(WebResult.Code.COACH_GET_ORGANIZER_INFO_ERROR);
        }
    }

    @ApiOperation(value = "获取分销员下拉框", notes = "获取分销员下拉框（可按照分销员角色和是否有效来筛选，默认(不传参数)筛选无效主办方关联的分销商）", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "role", value = "角色（0，员工 1 分销商）", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "valid", value = "分销商是否有效（0无效， 1有效）", required = false, dataType = "int", paramType = "query")
    })
    @RequestMapping(value = "/distribution/account.json", method = RequestMethod.GET)
    public WebResult getDistributionAccountByOrganizer(@RequestParam(name = "role", required = false) Integer distributionRole,
                                                       @RequestParam(name = "valid", required = false, defaultValue = "1") Integer valid,
                                                       HttpServletRequest request, HttpSession session) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null) {
            return WebResult.getErrorResult(WebResult.Code.COACH_GET_ORGANIZER_INFO_ERROR);
        }

        // 获取下拉框信息
        return WebResult.getSuccessResult("data", distributionAccountService.getDistributionAccountTitleList(organizer.getOrganizerId(), distributionRole, valid));
    }

    @ApiOperation(value = "主办方产品种类列表", notes = "获取当前的organizer包含的产品种类基本信息,前台下拉列表使用", httpMethod = "GET")
    @RequestMapping(value = "/activityType.json", method = RequestMethod.GET)
    @ResponseBody
    public Object getActivityTypeOption(HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);

        List<ActivityType> activityTypes = organizerService.getOrganizerActivityAuth(organizer.getType());
        List<Integer> types = new ArrayList<>();
        for (ActivityType activityType : activityTypes) {
            if (activityType.getTypeId().equals(8) || activityType.getTypeId().equals(9) || activityType.getTypeId().equals(10) || activityType.getTypeId().equals(4))//此处返回不包含指定教练预约
                continue;
            types.add(activityType.getTypeId());
        }
        if (types.size() > 0) {
            return WebResult.getSuccessResult("data", activityService.getActivityType(types));
        } else {
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }

    }


    /**
     * b端显示的所有activity信息,前台下拉列表使用
     */
    @ApiOperation(value = "b端显示的所有activity信息", notes = "b端显示的所有activity信息,前台下拉列表使用", httpMethod = "GET")
    @ApiImplicitParams({})
    @RequestMapping(value = "/client/activity.json", method = RequestMethod.GET)
    public Object wechatActivityOption(HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer != null) {
            return WebResult.getSuccessResult("data", activityService.getWechatActivityOptionByOrgId(organizer.getOrganizerId()));
        } else {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
    }

    /**
     * b端显示某活动的所有episode信息,前台下拉列表使用
     */
    @ApiOperation(value = "b端显示的某产品下所有episode信息", notes = "b端显示某活动的所有episode信息,前台下拉列表使用", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "activityId", value = "产品ID", required = true, dataType = "integer", paramType = "query"),
    })
    @RequestMapping(value = "/client/episode.json", method = RequestMethod.GET)
    public WebResult wechatEpisodeOption(@RequestParam("activityId") Integer activityId, HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer != null) {
            return WebResult.getSuccessResult("data", activityService.getWechatEpisodeOptionByActivityId(activityId));
        } else {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
    }


    /**
     * b端显示的所有 线下售卖 的activity信息,前台下拉列表使用
     */
    @ApiOperation(value = "收银台线下产品列表", notes = "收银台拉取线下产品列表", httpMethod = "GET")
    @RequestMapping(value = "/offline/activity.json", method = RequestMethod.GET)
    @ResponseBody
    public Object offlineActivityOption(@RequestParam(value = "cardType", required = false) Integer cardType,
                                        HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer != null) {
            return WebResult.getSuccessResult("data", activityService.getOfflineActivityOptionByOrgId(organizer.getOrganizerId(), cardType, null));
        } else {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
    }

    @ApiOperation(value = "场次列表", notes = "收银台根据产品ID获取某个产品下所有场次列表", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "activityId", value = "产品ID", required = true, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "cardType", value = "产品类型（0 普通票务   1团队票务    2联名次卡     3套票）", required = true, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "increment", value = "增值服务（不传值  票+增值服务，  0 票， 1增值服务）", required = true, dataType = "integer", paramType = "query"),
    })
    @RequestMapping(value = "/offline/episode.json", method = RequestMethod.GET)
    public WebResult getEpisodeSimplelist(@RequestParam("activityId") Integer activityid,
                                          @RequestParam(value = "cardType", required = false, defaultValue = "0") Integer cardType,
                                          @RequestParam(value = "increment", required = false) Integer increment,
                                          HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (null != organizer) {
            List<Episode> episodes = episodeService.getEpisodeDetailWithChargesByActivityId(activityid, organizer.getOrganizerId(), increment);
            Map<String, Object> result = new HashMap<>();
            result.put("episodeList", episodes);

            if (cardType != 1) {
                Map<Integer, List<Charge>> rentChargesMap = new HashMap<>();
                Map<Integer, List<Charge>> incrementChargesMap = new HashMap<>();
                for (Episode episode : episodes) {
                    List<Charge> rentTmp = chargeService.getValidRentChargeList(episode.getEpisodeId());
                    rentChargesMap.put(episode.getEpisodeId(), rentTmp);
                    List<Charge> incrementTmp = chargeService.getIncrementChargesByEpisode(episode.getEpisodeId());
                    incrementChargesMap.put(episode.getEpisodeId(), incrementTmp);

                }
                result.put("rentChargesMap", rentChargesMap);
                result.put("incrementChargesMap", incrementChargesMap);
                result.put("depositGradient", organizer.getDepositGradient());
            }
            return WebResult.getSuccessResult("data", result);
        } else {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
    }

    /**
     * 获取当前的organizer包含的episode基本信息<br>
     *
     * @param activityId 传入activityId作为刷选条件
     *                   前台下拉列表使用,  episodeService.getEpisodeSimpeInfoByOrganizer();
     */
    @ApiOperation(value = "主办方场次列表,用于与产品列表联动", notes = "获取当前的organizer的activity下episode基本信息,前台下拉列表使用,", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "activityId", value = "产品ID", required = false, dataType = "integer", paramType = "query"),
    })
    @RequestMapping(value = "/episode.json", method = RequestMethod.GET)
    public Object episodeOption(@RequestParam(value = "activityId", required = false) Integer activityId, HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (null != organizer) {
            List<Map> episodes = episodeService.getEpisodeSimpeInfoByOrganizer(organizer.getOrganizerId(), activityId);
            if (null == episodes || episodes.size() == 0) {
                boolean hasPermission = false;
                List<ActivityRelation> relations = activityService.getRelationIds(activityId);
                for (ActivityRelation relation : relations) {
                    if (relation.getOrganizerId().equals(organizer.getOrganizerId())) {
                        hasPermission = true;
                    }
                }
                if (hasPermission) {
                    episodes = episodeService.getEpisodeSimpeInfoByOrganizer(null, activityId);
                }
            }
            return WebResult.getSuccessResult("data", episodes);
        } else {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
    }

    /**
     * 获取 本地地址
     *
     * @return
     */
    @ApiOperation(value = "获取本地地址", notes = "获取本地地址 testwx weixin demo等", httpMethod = "GET")
    @ApiImplicitParams({})
    @RequestMapping("/getDomain.json")
    public WebResult getDomain() {
        try {
            return WebResult.getSuccessResult("data", Config.instance().getDomain());
        } catch (Exception e) {
            return WebResult.getErrorResult(WebResult.Code.REQUEST_ERROR);
        }
    }

    /**
     * b端显示的所有文章信息，前台下拉列表使用
     *
     * @param request
     * @return
     */
    @ApiOperation(value = "b端显示的所有文章信息", notes = "b端显示的所有文章信息，前台下拉列表使用", httpMethod = "GET")
    @RequestMapping(value = "/article.json", method = RequestMethod.GET)
    public WebResult wechatArticleOption(HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer != null) {
            return WebResult.getSuccessResult("data", articleService.getWechatArticleOptionByOrgId(organizer.getOrganizerId()));
        } else {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
    }

    @ApiOperation(value = "b端显示文章详情", notes = "b端显示文章详情，前台下拉列表使用", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "articleId", value = "文章ID", required = false, dataType = "integer", paramType = "query"),
    })
    @RequestMapping(value = "/article/detail.json", method = RequestMethod.GET)
    public WebResult activityDetail(@RequestParam("articleId") Integer articleId, HttpSession session) {

        return WebResult.getSuccessResult("data", articleService.getArticleDetailById(articleId));
    }

    /**
     * 专题信息，下拉框使用
     *
     * @param request
     * @return
     */
    @ApiOperation(value = "b端显示的所有专题信息", notes = "b端显示的所有专题信息，前台下拉列表使用", httpMethod = "GET")
    @RequestMapping(value = "/topic.json", method = RequestMethod.GET)
    public WebResult topicOption(HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer != null) {
            return WebResult.getSuccessResult("data", topicService.getTopicOptionByOrg(organizer.getOrganizerId()));
        } else {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
    }

    /**
     * 专题选项详情
     *
     * @param topicId
     * @param request
     * @return
     */
    @ApiOperation(value = "专题选项详情", notes = "专题选项详情,前台下拉列表使用,", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "topicId", value = "专题ID", required = true, dataType = "integer", paramType = "query"),
    })
    @RequestMapping(value = "/topic/detail.json", method = RequestMethod.GET)
    @ResponseBody
    public WebResult topicDetail(@RequestParam("topicId") Integer topicId, HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer != null) {
            return WebResult.getSuccessResult("data", topicService.getTopicOptionDetail(topicId));
        } else {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
    }

    /**
     * b端显示的所有hotel信息,前台下拉列表使用
     */
    @ApiOperation(value = "b端显示的所有酒店信息", notes = "b端显示的所有hotel信息,前台下拉列表使用", httpMethod = "GET")
    @RequestMapping(value = "/hotel.json", method = RequestMethod.GET)
    public WebResult wechatHotelOption(HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer != null) {
            return WebResult.getSuccessResult("data", hotelService.getHotelOptionByOrgId(organizer.getOrganizerId()));
        } else {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
    }

    @ApiOperation(value = "酒店选项详情", notes = "酒店选项详情,前台下拉列表使用,", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "hotelId", value = "酒店ID", required = true, dataType = "integer", paramType = "query"),
    })
    @RequestMapping("/hotel/detail.json")
    public WebResult getHotelDetail(@RequestParam("hotelId") Integer hotelId) {
        Map hotel = hotelService.getHotelDetail(hotelId);
        if (hotel != null) {
            return WebResult.getSuccessResult("data", hotelService.getHotelDetail(hotelId));
        } else {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
    }

    @ApiOperation(value = "装备选项详情", notes = "装备选项详情,前台下拉列表使用,", httpMethod = "GET")
    @RequestMapping(value = "/equipment.json", method = RequestMethod.GET)
    public WebResult getRentEquipment(HttpServletRequest request) {

        Organizer organizer = WebUpmsContext.getOrgnization(request);

        if (organizer == null) {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
        List<RentEquipment> rentEquipments = depositService.getRentEquipmentByOrganizer(organizer.getOrganizerId());

        List<Map<String, String>> result = new LinkedList<>();
        for (RentEquipment rentEquipment : rentEquipments) {
            Map<String, String> map = new HashMap<>();
            map.put("id", rentEquipment.getEquipmentId().toString());
            map.put("text", rentEquipment.getEquipmentName());
            result.add(map);
        }
        return WebResult.getSuccessResult("data", result);
    }

    @ApiOperation(value = "获取所有租赁物", notes = "获取所有租赁物,创建租赁物时使用,", httpMethod = "GET")
    @RequestMapping(value = "/allEquipment.json", method = RequestMethod.GET)
    public WebResult getAllRentEquipment(HttpServletRequest request) {
        return WebResult.getSuccessResult("data", episodeService.getRentEquipment(WebUpmsContext.getOrgnization(request).getCategory()));
    }

    @ApiOperation(value = "租赁产品选项", notes = "租赁产品选项,前台下拉列表使用,", httpMethod = "GET")
    @RequestMapping("/rentEpisode.json")
    public WebResult getRentEpisode(HttpServletRequest request) {
        return WebResult.getSuccessResult("data", episodeService.getRentEpisode(WebUpmsContext.getOrgnization(request).getOrganizerId()));
    }

    @ApiOperation(value = "租赁产品选项(产品+场次)", notes = "租赁产品选项,前台下拉列表使用,", httpMethod = "GET")
    @RequestMapping("/rentActivity.json")
    public WebResult getRentActivityWithEpisodes(HttpServletRequest request) {

        List<ActivityWithEpisodes> activityWithEpisodes = activityService.getRentalActivityListWithEpisodes(WebUpmsContext.getOrgnization(request).getOrganizerId());

        WebResult webResult = WebResult.getSuccessResult();

        webResult.put("data", activityWithEpisodes);

        return webResult;
    }

    @ApiOperation(value = "OTA列表选项", notes = "OTA列表选项,前台下拉列表使用,", httpMethod = "GET")
    @RequestMapping(value = "/ota.json")
    public WebResult distributionOption(HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        return WebResult.getSuccessResult("data", distributionService.getAvailableDistributor(organizer.getOrganizerId()));
    }

    @ApiOperation(value = "获取资讯分类", notes = "获取资讯分类,前台下拉列表使用,", httpMethod = "GET")
    @RequestMapping(value = "/article-categories", method = RequestMethod.GET)
    public WebResult getArticleCategoryList() throws Exception {
        try {
            List<ArticleCategory> categories = articleCategoryService.getAllCategories();
            return WebResult.getSuccessResult("data", categories);
        } catch (Exception e) {
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }

    }

    @ApiOperation(value = "获取产品标签", notes = "获取产品标签,前台下拉列表使用,", httpMethod = "GET")
    @RequestMapping(value = "/activity/tags", method = RequestMethod.GET)
    public WebResult getActivityTags(@RequestParam(value = "activityId", required = false) Integer activityId) {
        if (null != activityId) {
            //根据标签的类型获得该类型下的所有标签列表
            List<Tag> tagList = activityTagService.getActivityTags();
            //Map<Integer, List<Tag>> map1 = new HashMap<>();
            //map1.put(tagType,tagList);

            //根据商品id获得该商品对应的标签id
            Tag tag = tagRelationShipsService.getTagsRelationShipsList(activityId);
            //Map<Integer, Integer> map = new HashMap<>();

            //把该商品对应的标签id放入map中
            //map.put(activityId, tag.getTagId());
            Map<String, Object> map = new HashMap<>();
            map.put("list", tagList);
            map.put("tag", tag);

            //返回前台map,key是前台回显标签的ID，value是该类型的标签列表
            return WebResult.getSuccessResult("data", map);

        } else {
            Map<String, Object> map = new HashMap<>();
            map.put("list", activityTagService.getActivityTags());
            return WebResult.getSuccessResult("data", map);
        }

    }

    @ApiOperation(value = "获取完整订单号", notes = "获取完整订单号,前台下拉列表使用,", httpMethod = "GET")
    @RequestMapping(value = "/order/codes.json", method = RequestMethod.GET)
    public WebResult getOrderCodes(@RequestParam(value = "code") String code, Integer isTeamOrder, HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (StringUtils.isNotBlank(code)) {
            return WebResult.getSuccessResult("data", ordersService.getOrderCodes(code, isTeamOrder, organizer.getOrganizerId()));
        } else {
            return WebResult.getSuccessResult();
        }

    }

    @ApiOperation(value = "获取主办方下所有会员卡产品列表", notes = "获取主办方下所有会员卡产品列表,前台下拉列表使用", httpMethod = "GET")
    @RequestMapping(value = "/member-activity.json", method = RequestMethod.GET)
    public WebResult memberActivityOption(HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);

        return WebResult.getSuccessResult("data", activityService.getAllMemberActivityList4Option(organizer.getOrganizerId()));
    }

    @ApiOperation(value = "获取B端所有雪场列表", notes = "获取B端所有雪场列表,前台下拉列表使用,", httpMethod = "GET")
    @RequestMapping(value = "/skifield.json", method = RequestMethod.GET)
    public WebResult getSkiFields(HttpServletRequest request) {
        CommonAccount account = WebUpmsContext.getAccount(request);
        if (null == account) {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        } else {
            return WebResult.getSuccessResult("data", skiFieldService.getAllSkiFieldSimpleInfo());
        }

    }

    @ApiOperation(value = "判断是否是会员平台主办方", notes = "会员列表，详情使用", httpMethod = "GET")
    @RequestMapping(value = "/isMemberOrganizer.json", method = RequestMethod.GET)
    public WebResult isMemberOrganizer(HttpServletRequest request){
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        SystemConfig systemConfig = systemConfigService.selectByOrganizerId(organizer.getOrganizerId(), 2);
        if(null != systemConfig) return WebResult.getSuccessResult("data", "1");
        return WebResult.getSuccessResult("data", "0");
    }

    @ApiOperation(value = "获取主办方票务列表", notes = "下拉框使用", httpMethod = "GET")
    @RequestMapping(value = "/charge.json", method = RequestMethod.GET)
    public WebResult getChargeList(@RequestParam(value = "episodeId", required = false) Integer episodeId, HttpServletRequest request){
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        List<Charge> chargeList = chargeService.getchargeByOrganizerId(organizer.getOrganizerId(), episodeId);
        List<Map> result = new ArrayList<>();
        for (Charge charge : chargeList) {
            Map<String, Object> map = new HashMap<>();
            map.put("id", charge.getChargeId());
            map.put("text", charge.getChargeId() + "." + charge.getChargeName());
            result.add(map);

        }
        return WebResult.getSuccessResult("data", result);
    }
}
