package com.zoo.zoomerchantadminweb.ota.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.zoo.activity.constant.ResponseData;
import com.zoo.activity.core.Status;
import com.zoo.activity.core.util.SystemSessionUtil;
import com.zoo.activity.dao.nutz.pojo.CommonAccount;
import com.zoo.activity.dao.nutz.pojo.OtaBindingRecord;
import com.zoo.activity.util.AssUtils;
import com.zoo.activity.util.DateUtil;
import com.zoo.common.page.PageList;
import com.zoo.activity.dao.model.*;
import com.zoo.activity.dao.nutz.page.Pagination;
import com.zoo.activity.dao.nutz.pojo.OtaRelation;
import com.zoo.activity.service.*;
import com.zoo.ota.OtaConstant;
import com.zoo.ota.cbm.CbmUtil;
import com.zoo.util.data.BeanUtilsExtends;
import com.zoo.web.bean.WebResult;
import com.zoo.zoomerchantadminweb.context.WebUpmsContext;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

@Api(value = "api/server/ota", tags = {"主办方OTA相关接口"}, description = "主办方OTA相关接口描述")
@RestController
@RequestMapping("api/server/ota")
public class OtaController {
    static Logger logger = Logger.getLogger(OtaController.class);

    @Resource(name = "organizerService")
    OrganizerService organizerService;

    @Resource(name = "regionService")
    RegionService regionService;

    @Resource(name = "otaService")
    private OtaService otaService;

    @Resource(name = "orderService")
    private OrdersService ordersService;

    @Resource(name = "activityService")
    private ActivityService activityService;

    @Resource(name = "chargeService")
    private ChargeService chargeService;

    @Resource(name = "episodeService")
    private EpisodeService episodeService;

    @Resource(name = "distributionService")
    private DistributionService distributionService;

    @Autowired
    private PlayerService playerService;

    @Autowired
    private QuartzService quartzService;

    @ApiOperation(value = "景区信息更新", notes = "主办方景区信息更新", httpMethod = "POST")
    @RequestMapping(value = "/update.json", method = RequestMethod.POST)
    public WebResult updateProfile(OtaOrganizerModel organizerModel, HttpServletRequest request) {

        Organizer organizer = WebUpmsContext.getOrgnization(request);

        OrganizerWithScenic orgByUser = new OrganizerWithScenic();
        try {
            BeanUtilsExtends.copyProperties(orgByUser, organizerModel);
        } catch (Exception e) {
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }
        orgByUser.setOrganizerId(organizer.getOrganizerId());

        if (organizerService.updateOrganizerAllInfo(orgByUser)) {
            return WebResult.getSuccessResult();
        } else {
            return WebResult.getErrorResult(WebResult.Code.UPDATE_FAILED);
        }

    }

    @ApiOperation(value = "获取景区信息", notes = "主办方景区信息获取", httpMethod = "GET")
    @RequestMapping(value = "/info.json", method = RequestMethod.POST)
    public WebResult getProfile(HttpServletRequest request) {

        Organizer organizer = WebUpmsContext.getOrgnization(request);

        OrganizerWithScenic orgByUser = organizerService.getAllInfoById(organizer.getOrganizerId());
        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("province", regionService.getRegionListByParentId(1));
        if (null != orgByUser.getProvince()) {
            resultMap.put("city", regionService.getRegionListByParentId(orgByUser.getProvince()));
        }
        if (null != orgByUser.getCity()) {
            resultMap.put("district", regionService.getRegionListByParentId(orgByUser.getCity()));
        }

        try {
            OtaOrganizerModel otaOrganizerModel = new OtaOrganizerModel();
            BeanUtils.copyProperties(otaOrganizerModel, orgByUser);
            resultMap.put("organizer", otaOrganizerModel);

        } catch (Exception e) {
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }

        return WebResult.getSuccessResult("data", resultMap);

    }

    /**
     * 分销列表
     *
     * @return 已分销的产品
     */
    @ApiOperation(value = "已分销的产品列表", notes = "已分销的产品", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "size", value = "条", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "activityId", value = "产品ID", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "startTime", value = "开始时间", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "endTime", value = "结束时间", required = false, dataType = "string", paramType = "query")
    })
    @RequestMapping(value = "/list.json")
    public WebResult distributionList(@RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
                                      @RequestParam(value = "size", required = false, defaultValue = "10") Integer size,
                                      @RequestParam(value = "activityId", required = false) Integer activityId,
                                      @RequestParam(value = "startTime", required = false) String startTime,
                                      @RequestParam(value = "endTime", required = false) String endTime,
                                      HttpServletRequest request) {
        PageBounds pageBounds = new PageBounds();
        pageBounds.setPage(page);
        pageBounds.setLimit(size);
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        Pagination distributionList = distributionService.getDistributeActivityListByOrgId(page, size, organizer.getOrganizerId(), activityId, startTime, endTime);
        return WebResult.getSuccessResult("data", distributionList);
    }


    @ApiOperation(value = "OTA订单列表", notes = "后台使用的OTA订单列表功能(订单列表可以分页,默认按照创建时间排序)", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "size", value = "条", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "activityId", value = "产品ID", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "status", value = "状态", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "distributorId", value = "分销商ID", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "startTime", value = "开始时间", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "endTime", value = "结束时间", required = false, dataType = "string", paramType = "query"),

    })
    @RequestMapping(value = "/orders.json")
    public WebResult getDistributionOrders(@RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
                                           @RequestParam(value = "size", required = false, defaultValue = "10") Integer size,
                                           @RequestParam(value = "code", required = false) String code,
                                           @RequestParam(value = "agentOrderCode", required = false) String agentOrderCode,
                                           @RequestParam(value = "distributorSequenceId", required = false) String distributorSequenceId,
                                           @RequestParam(value = "activityId", required = false) Integer activityId,
                                           @RequestParam(value = "distributorId", required = false) Integer distributorId,
                                           @RequestParam(value = "status", required = false) String status,
                                           @RequestParam(value = "startTime", required = false) String startTime,
                                           @RequestParam(value = "endTime", required = false) String endTime,
                                           HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (org.apache.commons.lang3.StringUtils.isBlank(status) || status.equals("[]")) {
            status = null;
        } else {
            status = "(" + status + ")";
        }
        PageList result = ordersService.getOrderViewListByOption(activityId, null, null, status, code, null, size, page, organizer.getOrganizerId(), startTime, endTime, 1, distributorId, false, null, null, null, null, null, distributorSequenceId, agentOrderCode, null, null);
        return WebResult.getSuccessResult("data", result);
    }


    @ApiOperation(value = "OTA核销订单列表", notes = "后台使用的OTA核销订单列表功能(订单列表可以分页,默认按照创建时间排序)", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "size", value = "条", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "distributorSequenceId", value = "商家编号", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "activityId", value = "产品ID", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "distributorId", value = "分销商ID", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "verifyStartTime", value = "开始时间", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "verifyEndTime", value = "结束时间", required = false, dataType = "string", paramType = "query"),

    })
    @RequestMapping(value = "/verifyOrders.json")
    public WebResult getDistributionVerifyOrders(@RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
                                           @RequestParam(value = "size", required = false, defaultValue = "10") Integer size,
                                           @RequestParam(value = "code", required = false) String code,
                                           @RequestParam(value = "agentOrderCode", required = false) String agentOrderCode,
                                           @RequestParam(value = "distributorSequenceId", required = false) String distributorSequenceId,
                                           @RequestParam(value = "activityId", required = false) Integer activityId,
                                           @RequestParam(value = "distributorId", required = false) Integer distributorId,
                                           @RequestParam(value = "verifyStartTime", required = false) String verifyStartTime,
                                           @RequestParam(value = "verifyEndTime", required = false) String verifyEndTime,
                                           HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);

        PageList result = ordersService.getDistributionVerifyOrders(activityId, code, organizer.getOrganizerId(), verifyStartTime, verifyEndTime, distributorId, distributorSequenceId, size, page, agentOrderCode);
        return WebResult.getSuccessResult("data", result);
    }


    /**
     * 选择可分销的产品
     *
     * @param activities 产品IDs
     * @return 状态
     */
    @ApiOperation(value = "选择可分销的产品", notes = "选择可分销的产品", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "activities", value = "产品ID集合JsonArray字符串", required = false, dataType = "string", paramType = "query")
    })
    @RequestMapping(value = "/select.json", method = RequestMethod.POST)
    public Object activitySelect(@RequestParam("activities") String activities, HttpServletRequest request) {
        JSONArray activityJson = JSON.parseArray(activities);
        if (null == activityJson) {
            activityJson = new JSONArray();
        }
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        OrganizerWithScenic scenic = organizerService.getAllInfoById(organizer.getOrganizerId());
        if (StringUtils.isBlank(scenic.getScenicName()) || StringUtils.isBlank(scenic.getDescription()) || StringUtils.isBlank(scenic.getScenicDescription2()) || StringUtils.isBlank(scenic.getScenicDescription3()) || StringUtils.isBlank(scenic.getScenicOpen()) || StringUtils.isBlank(scenic.getScenicPic1())
                || null == scenic.getDaytimeStart() || null == scenic.getDaytimeStartMin() || null == scenic.getDaytimeEnd() || null == scenic.getDaytimeEndMin() || StringUtils.isBlank(scenic.getLongitude()) || StringUtils.isBlank(scenic.getLatitude())
                || null == scenic.getProvince() || null == scenic.getCity() || null == scenic.getDistrict() || 0 == scenic.getProvince() || 0 == scenic.getCity() || 0 == scenic.getDistrict()) {
            return WebResult.getErrorResult(WebResult.Code.OTA_SETTING_REQUIRE);
        }
        for (Object obj : activityJson) {
            Integer activityId = Integer.parseInt(((Map) obj).get("id").toString());
            ActivityWithBLOBs activity = activityService.getActivityById(activityId);
            activity.setOtaDistribute(1);
            activityService.updateActivity(activity);
        }
        return WebResult.getSuccessResult();
    }

    @ApiOperation(value = "产品详情", notes = "产品详情", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "activityId", value = "产品ID", required = false, dataType = "int", paramType = "query")
    })
    @RequestMapping(value = "/detail.json")
    public WebResult distributionDetail(@RequestParam("activityId") Integer activityId, HttpServletRequest request) {
        List<Episode> episodes = episodeService.getEpisodeListByActivityIdAndStatus(activityId, WebUpmsContext.getOrgnization(request).getOrganizerId(), 1);
        CopyOnWriteArrayList<Charge> chargeList = new CopyOnWriteArrayList<>();
        for (Episode episode : episodes) {
            List<Charge> charges = chargeService.getChargeList(episode.getEpisodeId());
            chargeList.addAll(charges);
            JSONArray jsonArray = new JSONArray();
            for (Charge charge : chargeList) {
                jsonArray.clear();
                if (charge.getIncrement() == 1 || charge.getValid() == 0) {
                    chargeList.remove(charge);
                } else {
                    List<OtaRelation> relations = (List<OtaRelation>) otaService.getOtaRelation(charge.getChargeId(), OtaConstant.OtaRelationEnum.productRelation.getRelationType(), OtaConstant.VALID, null, null, 1, 1000).getList();
                    for (OtaRelation relation : relations) {
                        jsonArray.add(relation.getOtaId());
                    }
                    charge.setOtaIds(jsonArray.toJSONString());
                }
            }
        }
        return WebResult.getSuccessResult("data", chargeList);
    }

    /**
     * 取消分销的产品
     *
     * @param activityId 产品ID
     * @return 状态
     */
    @ApiOperation(value = "产品取消分销", notes = "产品取消分销", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "activityId", value = "产品ID", required = false, dataType = "int", paramType = "query")
    })
    @RequestMapping(value = "/cancel.json", method = RequestMethod.POST)
    public Object activityCancel(@RequestParam("activityId") Integer activityId) {
        ActivityWithBLOBs activity = activityService.getActivityById(activityId);
        activity.setOtaDistribute(0);
        activityService.updateActivity(activity);
        List<Integer> activityIds = new ArrayList<>();
        activityIds.add(activityId);
        Organizer organizer = organizerService.getOrganizerDetail(activity.getOrganizerId());
        List<Charge> charges = chargeService.getChargeByActivityIds(activityIds);
        for (Charge charge : charges) {
            List<OtaRelation> relations = (List<OtaRelation>) otaService.getOtaRelation(charge.getChargeId(), OtaConstant.OtaRelationEnum.productRelation.getRelationType(), null, null, null, 1, 1000).getList();
            for (OtaRelation otaRelation : relations) {
                otaService.pushProductStatus(organizer, charge, OtaConstant.INVALID, otaRelation.getOtaId());
            }
        }
        return WebResult.getSuccessResult();
    }

    @ApiOperation(value = "产品保存分销", notes = "产品保存分销", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "activityId", value = "产品ID", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "distributeInfo", value = "分销详情JsonArray字符串", required = false, dataType = "string", paramType = "query")
    })
    @RequestMapping(value = "save.json", method = RequestMethod.POST)
    public Object saveDistributeInfo(@RequestParam("distributeInfo") String distributeInfo, @RequestParam("activityId") Integer activityId, HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        List<HashMap> infoList = JSON.parseArray(distributeInfo, HashMap.class);
        Map<Integer, List<Integer>> infoMap = new HashMap<>();
        for (HashMap info : infoList) {
            Integer chargeId = Integer.parseInt(info.get("chargeId").toString());
            List<Integer> otaIds;
            if (infoMap.containsKey(chargeId)) {
                otaIds = infoMap.get(chargeId);
            } else {
                otaIds = new ArrayList<>();
            }
            otaIds.add(Integer.parseInt(info.get("otaId").toString()));
            infoMap.put(chargeId, otaIds);

//            if(MapUtils.getInteger(info, "otaId").equals(OtaConstant.OtaIdEnum.CBM.getOtaId())){
//                String result = CBMPushProduct(info.get("chargeId").toString());
//                System.out.println("中行推送信息：" + result);
//                logger.info("中行推送信息：" + result);
//            }
        }
        List<Integer> activityIds = new ArrayList<>();
        activityIds.add(activityId);
        List<Charge> charges = chargeService.getChargeByActivityIds(activityIds);
        String errMsg = "";
        for (Charge charge : charges) {
            List<OtaRelation> relations = (List<OtaRelation>) otaService.getOtaRelation(charge.getChargeId(), OtaConstant.OtaRelationEnum.productRelation.getRelationType(), null, null, null, 1, 1000).getList();
            if (infoMap.containsKey(charge.getChargeId())) {
                if (null != charge.getValidRange() && charge.getValidRange() > 0) {
                    List<Integer> chargeOtaIds = infoMap.get(charge.getChargeId());
                    if (null != chargeOtaIds && chargeOtaIds.size() > 0) {
                        for (OtaRelation relation : relations) {
                            Integer otaId = relation.getOtaId();
                            if (!chargeOtaIds.contains(otaId) && relation.getStatus().equals(OtaConstant.VALID)) {
                                otaService.pushProductStatus(organizer, charge, OtaConstant.INVALID, otaId);
                            }
                        }
                    } else {
                        for (OtaRelation relation : relations) {
                            Integer otaId = relation.getOtaId();
                            if (relation.getStatus().equals(OtaConstant.VALID)) {
                                otaService.pushProductStatus(organizer, charge, OtaConstant.INVALID, otaId);
                            }
                        }
                    }
                    Map<Integer, Integer> existMap = new HashMap<>();
                    if (null != chargeOtaIds && chargeOtaIds.size() > 0) {
                        for (Integer otaId : chargeOtaIds) {
                            boolean exist = false;
                            for (OtaRelation relation : relations) {
                                if (relation.getOtaId().equals(otaId)) {
                                    exist = true;
                                    if (relation.getStatus().equals(OtaConstant.INVALID)) {
                                        otaService.pushProductStatus(organizer, charge, OtaConstant.VALID, otaId);
                                    }
                                }
                            }
                            if (!exist) {
                                existMap.put(otaId, 1);
                            }
                        }
                    }
                    for (Integer otaId : existMap.keySet()) {
                        OtaRelation otaRelation = new OtaRelation();
                        otaRelation.setOrgId(organizer.getOrganizerId());
                        otaRelation.setStatus(OtaConstant.VALID);
                        otaRelation.setRelationType(OtaConstant.OtaRelationEnum.productRelation.getRelationType());
                        otaRelation.setRelationId(charge.getChargeId());
                        otaRelation.setOtaId(otaId);
                        double rate = OtaConstant.getById(otaId).getRate();
                        if (otaId.equals(10899)) {
                            if (organizer.getOrganizerId().equals(47)) {
                                rate = 0.05;
                            } else if (organizer.getOrganizerId().equals(209)) {
                                rate = 0.03;
                            } else if (organizer.getOrganizerId().equals(246)) {
                                rate = 0.04;
                            } else if (organizer.getOrganizerId().equals(69)) {
                                rate = 0.05;
                            } else if (organizer.getOrganizerId().equals(304)) {
                                rate = 0.05;
                            } else {
                                rate = 0.05;
                            }
                        }
                        BigDecimal settlePrice = charge.getPrice().multiply(new BigDecimal(1).subtract(new BigDecimal(rate))).setScale(2, BigDecimal.ROUND_HALF_UP);
                        otaRelation.setOtaPrice(null != settlePrice && settlePrice.compareTo(new BigDecimal(0.01)) > 0 ? settlePrice : new BigDecimal(0.01));
                        otaService.saveOrUpdateRelation(otaRelation, 0);
                        List<OtaRelation> orgRelations = (List<OtaRelation>) otaService.getOtaRelation(organizer.getOrganizerId(), OtaConstant.OtaRelationEnum.orgRelation.getRelationType(), null, otaId, null, 1, 1000).getList();
                        if (null != orgRelations && orgRelations.size() > 0) {
                            otaService.pushNewProduct(organizer, charge, otaId);
                        } else {
                            OtaRelation orgRelation = new OtaRelation();
                            orgRelation.setOrgId(organizer.getOrganizerId());
                            orgRelation.setStatus(OtaConstant.VALID);
                            orgRelation.setOtaId(otaId);
                            orgRelation.setRelationId(organizer.getOrganizerId());
                            orgRelation.setRelationType(OtaConstant.OtaRelationEnum.orgRelation.getRelationType());
                            otaService.saveOrUpdateRelation(orgRelation, 0);
                            otaService.pushNewStadium(organizer, otaId);
                            otaService.pushNewProduct(organizer, charge, otaId);
                        }
                    }
                } else {
                    return WebResult.getErrorResult(WebResult.Code.ERROR);
                }
            } else {
                for (OtaRelation relation : relations) {
                    otaService.pushProductStatus(organizer, charge, OtaConstant.INVALID, relation.getOtaId());
                }
            }
        }
        if (errMsg.length() == 0) {
        } else {
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }
        return WebResult.getSuccessResult();
    }


    @ApiOperation(value = "通过parentId获取地区", notes = "通过parentId获取地区", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "parentId", value = "parentId", required = false, dataType = "int", paramType = "query")
    })
    @RequestMapping("/region/regionListByParentId.json")
    public WebResult getRegionListByParentId(@RequestParam("parentId") String parentId) {
        return WebResult.getSuccessResult("data", regionService.getRegionListByParentId(Integer.parseInt(parentId)));
    }

    @ApiOperation(value = "获取所有地区", notes = "获取所有地区", httpMethod = "GET")
    @RequestMapping(value = "/region/regionList.json", method = RequestMethod.GET)
    public WebResult getRegionList() {
        return WebResult.getSuccessResult("data", regionService.getRegionList());
    }

    @RequestMapping(value = "/CreateCBMFile.json", method = RequestMethod.GET)
    public WebResult CreateCBMFile(String ids){
        String[] idArray = ids.split(",");
        StringBuilder sb = new StringBuilder();
        for (String s : idArray) {
            int id = Integer.parseInt(s);
            Charge charge = chargeService.getChargeById(id);
            if(charge == null) {
                throw new RuntimeException("不存在该票种: " + id);
            }
            ActivityListView activityView = activityService.getActivityDetailById(charge.getActivityId());
            OrganizerWithScenic organizerDetail = organizerService.getAllInfoById(activityView.getOrgId());
            Episode episode = episodeService.getEpisodeDetailById(charge.getEpisodeId());

            sb.append(charge.getChargeId()).append("|*|");
            sb.append("3021032").append("|*|");
            sb.append(activityView.getPosterUrl()).append("|*|");
            sb.append(activityView.getPosterUrl()).append("|*|");
            sb.append(charge.getChargeName()).append("|*|");
            sb.append("|*|");
            sb.append("|*|");
            sb.append("|*|");
            sb.append("|*|");
            sb.append("|*|");
            sb.append(organizerDetail.getAddress()).append("|*|");
            sb.append(organizerDetail.getLongitude()).append("|*|");
            sb.append(organizerDetail.getLatitude()).append("|*|");
            sb.append("|*|");
            sb.append("|*|");
            sb.append("|*|");
            sb.append("|*|");
            sb.append("|*|");
            sb.append(activityView.getDescription()).append("|*|");
            sb.append("|*|");
            sb.append(charge.getPrice().multiply(new BigDecimal(100)).intValue()).append("|*|");
            sb.append(charge.getPrice().multiply(new BigDecimal(100)).intValue()).append("|*|");
            sb.append(charge.getPrice().multiply(new BigDecimal(100)).intValue()).append("|*|");
            sb.append(activityView.getDescription()).append("|*|");
            sb.append("|*|");
            sb.append(episode.getStatus() == 0 ? "00" : "01").append("|*|");
            sb.append(DateUtil.convertDateToString(episode.getStartTime(), DateUtil.TIMEF_FORMAT)).append("|*|");
            sb.append(DateUtil.convertDateToString(episode.getEndTime(), DateUtil.TIMEF_FORMAT)).append("|*|");
            sb.append(DateUtil.convertDateToString(episode.getStartTime(), DateUtil.TIMEF_FORMAT)).append("|*|");
            sb.append(DateUtil.convertDateToString(episode.getEndTime(), DateUtil.TIMEF_FORMAT)).append("|*|");
            sb.append("|*|");
            sb.append("[{\"field\":\"userName\",\"description\":\"姓名\"},{\"field\":\"phone\",\"description\":\"手机号\"},{\"field\":\"certId\",\"description\":\"证件号码\"},{\"field\":\"certType\",\"description\":\"证件类型\"}]").append("|*|");
            sb.append(organizerDetail.getOrganizerId()).append("|*|");
            sb.append(organizerDetail.getName()).append("|*|");
            sb.append(organizerDetail.getLogoUrl()).append("|*|");
            sb.append(organizerDetail.getLogoUrl()).append("|*|");
            sb.append(organizerDetail.getDescription()).append("|*|");
            sb.append(organizerDetail.getDescription()).append("|*|");
            sb.append(organizerDetail.getAddress()).append("|*|");
            sb.append(organizerDetail.getLongitude()).append("|*|");
            sb.append(organizerDetail.getLatitude()).append("|*|");
            sb.append(organizerDetail.getPhone()).append("|*|");
            sb.append("104130070110008").append("|*|");
            sb.append("11220005").append("|*|");
            sb.append("|*|");
            sb.append("|*|");
            sb.append("|*|");
            sb.append("00").append("|*|");
            sb.append(organizerDetail.getDaytimeStart()).append(":").append(organizerDetail.getDaytimeStartMin() == 0 ? "00" : organizerDetail.getDaytimeStartMin()).append("|*|");
            if(organizerDetail.getNightEnd() == 0) {
                sb.append(organizerDetail.getDaytimeEnd()).append(":").append(organizerDetail.getDaytimeEndMin() == 0 ? "00" : organizerDetail.getDaytimeEndMin()).append("|*|");
            } else {
                sb.append(organizerDetail.getNightEnd()).append(":").append(organizerDetail.getNightEndMin() == 0 ? "00" : organizerDetail.getNightEndMin()).append("|*|");
            }
            sb.append("|*|");
            sb.append("|*|");
            sb.append("|*|");
            sb.append("|*|");
            sb.append("huaxuezoo").append("|*|");
            sb.append("HUAXUEZOO");
            sb.append("1044182202000037").append("|*|");
            sb.append(activityView.getPosterUrl()).append("|*|");
            sb.append("|*|");
            sb.append("|*|");
            sb.append(0).append("|*|");
            sb.append(charge.getChargeId()).append("|*|");
            sb.append("|*|");
            sb.append("|*|");
            sb.append("|*|");
            sb.append("|*|");
            sb.append("|*|");
            sb.append("|*|");
            sb.append("|*|");
            sb.append("|*|");
            sb.append("|*|");
            sb.append("|*|");
            sb.append("\n");
        }
        WebResult webResult = WebResult.getSuccessResult();
        webResult.putResult("data", sb.toString());
        return webResult;
    }

    @RequestMapping(value = "/pushChargeDetail.json", method = RequestMethod.POST)
    public WebResult pushChargeDetail(String ids) {

        String data = CBMPushProduct(ids);


        return WebResult.getSuccessResult("data", data);
    }

    public String CBMPushProduct(String ids){
        String data = "";
        String[] idArray = ids.split(",");
        for (String s : idArray) {
            JSONArray jsonArray = new JSONArray();
            int id = Integer.parseInt(s);
            Charge charge = chargeService.getChargeById(id);
            if(charge == null) {
                throw new RuntimeException("不存在该票种: " + id);
            }
            ActivityListView activityView = activityService.getActivityDetailById(charge.getActivityId());
            OrganizerWithScenic organizerDetail = organizerService.getAllInfoById(activityView.getOrgId());
            Episode episode = episodeService.getEpisodeDetailById(charge.getEpisodeId());
            JSONObject o = new JSONObject();
            o.put("commodityId", charge.getChargeId());
            o.put("commodityTypeId", "3021032");
            o.put("picUrl", activityView.getPosterUrl());
            o.put("rotationnChartUrl", activityView.getPosterUrl());
            o.put("commodityName", charge.getChargeName());
            o.put("monthlySales", 0);
            o.put("commoditySales", 0);
            o.put("commodityStock", 99999);
            o.put("phone", CbmUtil.getRequest(organizerDetail.getPhone()));
            o.put("site", "");
            o.put("detailSite", organizerDetail.getAddress());
            o.put("commodityLongitude", organizerDetail.getLongitude());
            o.put("commodityLatitude", organizerDetail.getLatitude());
//            Map<String, String> addressMap = AddressUtil.GetLocationMsg(Double.parseDouble(organizerDetail.getLatitude()), Double.parseDouble(organizerDetail.getLongitude()));
            Region province = regionService.getRegionById(organizerDetail.getProvince());
            Region city = regionService.getRegionById(organizerDetail.getCity());
            o.put("provinceCode", province.getCode());
            o.put("cityCode", city.getCode());
            o.put("regionCode", city.getCode());
            o.put("reserveDays", "7");
            o.put("advanceReserveDays", "0");


            o.put("commodityShow", activityView.getDescription());
            o.put("commodityTag", "");
            o.put("commodityDailyPrice", charge.getPrice().multiply(new BigDecimal(100)).intValue());
            o.put("holidayPrice", charge.getPrice().multiply(new BigDecimal(100)).intValue());
            o.put("vacationPrice", charge.getPrice().multiply(new BigDecimal(100)).intValue());
            o.put("commodityDescribe", activityView.getDescription());
            o.put("purchaseLimits", "");
            o.put("commodityState", episode.getStatus() == 0 ? "00" : "01");
            o.put("onFrameTime", DateUtil.convertDateToString(episode.getStartTime(), DateUtil.TIMEF_FORMAT));
            o.put("offFrameTime", DateUtil.convertDateToString(episode.getEndTime(), DateUtil.TIMEF_FORMAT));
            o.put("useStartTime", DateUtil.convertDateToString(episode.getStartTime(), DateUtil.TIMEF_FORMAT));
            o.put("useEndTime", DateUtil.convertDateToString(episode.getEndTime(), DateUtil.TIMEF_FORMAT));
            o.put("holydayRule", "");
            JSONArray array = new JSONArray();
            Date date = new Date();

            Date endDay = DateUtil.addDay(date, 14);
            Date episodeEndTime = episode.getEndTime();

            if(endDay.compareTo(episodeEndTime) > 0){
                endDay = episodeEndTime;
            }

            while (date.compareTo(endDay) < 0){
                String dateToString = DateUtil.convertDateToString(date, DateUtil.DATE_FORMAT);
                JSONObject ob = new JSONObject();
                ob.put("date", dateToString);
                ob.put("price", String.valueOf(charge.getPrice().multiply(new BigDecimal(100)).intValue()));
                ob.put("inventory", "9999");
                array.add(ob);
                date = DateUtil.addDay(date, 1);
            }
            o.put("datePrviceStock", CbmUtil.getRequest(array));
            o.put("userInfo", "[{\"field\":\"userName\",\"description\":\"姓名\"},{\"field\":\"phone\",\"description\":\"手机号\"},{\"field\":\"certId\",\"description\":\"证件号码\"},{\"field\":\"certType\",\"description\":\"证件类型\"}]");
            o.put("shopId", organizerDetail.getOrganizerId());
            o.put("shopName", organizerDetail.getName());
            o.put("shopRotationChart", organizerDetail.getLogoUrl());
            o.put("shopPic", organizerDetail.getLogoUrl());
            o.put("shopDetailPic", organizerDetail.getDescription());
            o.put("shopDescription", organizerDetail.getDescription());
            o.put("shopAddress", organizerDetail.getAddress());
            o.put("shopLongitude", organizerDetail.getLongitude());
            o.put("shopLatitude", organizerDetail.getLatitude());
            o.put("shopPhone", CbmUtil.getRequest(organizerDetail.getPhone()));
            o.put("merId", "104130070110008");
            o.put("merTermId", "11220005");

            o.put("shopProvinceCode", province.getCode());
            o.put("shopCityCode", city.getCode());
            o.put("shopRegionCode", city.getCode());
            o.put("shopState", "00");

            o.put("businessStartTime", organizerDetail.getDaytimeStart() + ":" + (organizerDetail.getDaytimeStartMin() == 0 ? "00" : organizerDetail.getDaytimeStartMin()));
            if(organizerDetail.getNightEnd() == 0) {
                o.put("businessEndTime", organizerDetail.getDaytimeEnd() + ":" + (organizerDetail.getDaytimeEndMin() == 0 ? "00" : organizerDetail.getDaytimeEndMin()));
            } else {
                o.put("businessEndTime", organizerDetail.getNightEnd() + ":" + (organizerDetail.getNightEndMin() == 0 ? "00" : organizerDetail.getNightEndMin()));
            }
            o.put("posId", "huaxuezoo");
            o.put("dataSource", "HUAXUEZOO");
            o.put("brandCode", "1044182202000037");
            o.put("typeOfProduct", "0");
            o.put("commodityParentId", charge.getChargeId());

            jsonArray.add(o);

            System.out.println("中行数据：" + JSON.toJSONString(jsonArray));
            String result = otaService.pushChargeDetail(jsonArray);

            JSONObject jsonObject = JSONObject.parseObject(result);

            if(jsonObject == null) {
                Quartz q = quartzService.selectByTargetIdAndType(episode.getEpisodeId(), 16, 1);
                if(q != null) {
                    q.setValid(0);
                    q.setUpdateTime(new Date());
                    q.setResult("中行接口404");
                    quartzService.updateQuartzTask(q);

                }
                return "中行接口404";
            }

            String msg = jsonObject.getString("msg");

            data = data + msg + "\n";
            System.out.println(msg);
            String[] split = msg.split("]，同步失败数据");
            String[] split1 = split[0].split("同步成功数据:\\[");
            if(split1.length == 2){
                Long longTriggerTime = new Date().getTime() + 30 * 1000;
                String jobName = "cbmPushProductJob" + Integer.toString(episode.getEpisodeId()) + String.valueOf(Math.random() * 9000 + 1000).substring(0, 5) + longTriggerTime;

                if(DateUtil.getNewWeekDay(new Date(), 3).compareTo(episode.getEndTime()) < 0){

                    Quartz q = quartzService.selectByTargetIdAndType(episode.getEpisodeId(), 16, 1);
                    if(q != null) {
                        q.setValid(0);
                        q.setUpdateTime(new Date());
                        q.setResult("更新数据，未执行");
                        quartzService.updateQuartzTask(q);
                    }

                    Quartz newQuartz = new Quartz();
                    newQuartz.setCreateTime(new Date());

                    Date newWeekDay = DateUtil.getNewWeekDay(new Date(), 3);
                    Date endTime = DateUtil.getEndTime(newWeekDay);
//                System.out.println(endTime);
//                String quartzTimeString = DateUtil.getQuartzTimeString(endTime);
//                System.out.println(quartzTimeString);
//                Date date = ;
//                System.out.println(date);
//                System.out.println(DateUtil.getQuartzTimeString(DateUtil.addHour(endTime, 1)));

                    newQuartz.setTriggerTime(DateUtil.addHour(endTime, 1));
                    newQuartz.setQuartzType(16);
                    newQuartz.setTargetId(episode.getEpisodeId());
                    newQuartz.setValid(1);
                    newQuartz.setResult(msg);
//                newQuartz.setResult(quartz.getResult());
                    newQuartz.setQuartzName(jobName);
                    quartzService.addQuartzTask(newQuartz);
                }


            } else {
                Quartz q = quartzService.selectByTargetIdAndType(episode.getEpisodeId(), 16, 1);
                if(q != null) {
                    q.setValid(0);
                    q.setUpdateTime(new Date());
                    q.setResult(msg);
                    quartzService.updateQuartzTask(q);
                }
            }
        }
//        System.out.println("1111111111");
        return data;
    }
    @RequestMapping(value = "/checkOrder.json", method = RequestMethod.POST)
    @ResponseBody
    public Object checkOtaOrder(@RequestParam("type") Integer type, @RequestParam("info") String info, HttpServletRequest request) {
//        ResponseData responseData = new ResponseData();
        CommonAccount account = WebUpmsContext.getAccount(request);
        Organizer orgnization = WebUpmsContext.getOrgnization(request);
//        Integer clientId = Integer.parseInt(SystemSessionUtil.getClientId(session));
        // info 去除空格、制表符、换页符等等
        if (StringUtils.isBlank(info)) {
//            responseData.setMsg("订单码不能为空!");
//            responseData.setStatus(Status.error);
            return WebResult.getErrorResult("订单码不能为空!");
        }
        info = info.replaceAll("\\s*", "");
        if (null != account) {
            if (type == 0) {
                Integer orderId = 0;
                Integer activityId = 0;
                Integer episodeId = 0;
                List<Player> players = playerService.getPlayersByPhone(info, 1);
                if (null == players || players.size() == 0) {
//                    responseData.setMsg("该手机号暂无订单,无法绑定");
//                    responseData.setStatus(Status.error);
                    return WebResult.getErrorResult("该手机号暂无订单,无法绑定!");
                } else {
                    List<String> codes = new ArrayList<>();
                    for (Player player : players) {
                        OrderView orders = ordersService.getOrderDetail(player.getOrderId());
                        if (orders.getBuyerId() < 0 && orders.getOrderStatus() == 1 && !codes.contains(orders.getCode()) && orders.getSellerId().equals(orgnization.getOrganizerId())) {
                            codes.add(orders.getCode());
                            orderId = orders.getOrderId();
                            activityId = orders.getActivityId();
                            episodeId = orders.getEpisodeId();

                        }
                    }
                    if (codes.size() == 0) {
//                        responseData.setMsg("订单不存在,无法绑定");
//                        responseData.setStatus(Status.error);
                        return WebResult.getErrorResult("订单不存在,无法绑定!");
                    } else if (codes.size() == 1) {
                        Map<String, Object> data = new HashMap<>();
                        data.put("count", 1);
                        data.put("orderId", orderId);
                        data.put("activityId", activityId);
                        data.put("episodeId", episodeId);
//                        responseData.setStatus(Status.success);
//                        responseData.setData(data);
//                        responseData.setMsg("订单获取成功");
                        WebResult<Object> result = WebResult.getSuccessResult();
                        result.setMsg("订单获取成功");
                        return result;
                    } else {
                        Map<String, Object> data = new HashMap<>();
                        data.put("count", codes.size());
                        data.put("dataInfo", JSON.toJSONString(codes));
//                        responseData.setStatus(Status.success);
//                        responseData.setData(data);
//                        responseData.setMsg("请选择具体订单");
                        WebResult<Object> result = WebResult.getSuccessResult();
                        result.setMsg("请选择具体订单");
                        return result;
                    }
                }
            } else if (type == 1 || type == 2) {
                String code = info.split("-")[0];
                Orders orders = ordersService.getOrdersByCode(code);
                if (null != orders) {
                    if (!orders.getSellerId().equals(orgnization.getOrganizerId())) {
//                        responseData.setMsg("订单不属于本雪场,无法换票");
//                        responseData.setStatus(Status.error);
//                        return responseData;
                        return WebResult.getErrorResult("订单不属于本雪场,无法换票!");
                    }
                    if (orders.getBuyerId() >= 0) {
//                        responseData.setMsg("非分销商订单,无法换票");
//                        responseData.setStatus(Status.error);
//                        return responseData;
                        return WebResult.getErrorResult("非分销商订单,无法换票!");
                    }
                    if (orders.getStatus() != 1) {
//                        responseData.setMsg("订单非支付状态,无法换票");
//                        responseData.setStatus(Status.error);
//                        return responseData;
                        return WebResult.getErrorResult("订单非支付状态,无法换票!");
                    }
                    Map<String, Object> data = new HashMap<>();

                    if (null != orders.getParentId() && orders.getParentId() > 0) {
                        Orders parentOrder = ordersService.getSimpleOrdersById(orders.getParentId());
                        if (parentOrder != null && parentOrder.getStatus() != null) {
                            if (parentOrder.getStatus() == 4) {
                                // 订单已关闭重新绑定时，进行OTA订单解绑操作
                                resetOtaOrderInfo(parentOrder, orders, 0);

                                // 更新父订单产品相关信息
                                resetParentOrderInfo(parentOrder, orders);
                            } else if (parentOrder.getStatus() == 0) {
                                // responseData.setMsg("订单已经绑定,无法重复绑定");
                                // responseData.setStatus(Status.error);
                                data.put("count", 0);
                                data.put("orderId", orders.getOrderId());
                                data.put("activityId", orders.getActivityId());
                                data.put("episodeId", orders.getEpisodeId());
//                                responseData.setMsg("该订单已完成兑换,确定要重新换票吗? (温馨提示: 兑换后订单可在订单中心查看)");
//                                responseData.setStatus(Status.success);
//                                responseData.setData(data);
                                WebResult<Object> result = WebResult.getSuccessResult();
                                result.setMsg("该订单已完成兑换,确定要重新换票吗? (温馨提示: 兑换后订单可在订单中心查看)");
                                result.putResult("data", data);
                                return result;
                            } else {
//                                responseData.setMsg("该订单已完成兑换，请到订单中心查看");
//                                responseData.setStatus(Status.error);
//                                return responseData;
                                return WebResult.getErrorResult("该订单已完成兑换，请到订单中心查看!");
                            }
                        } else {
//                            responseData.setMsg("该订单换票失败，请重新换票");
//                            responseData.setStatus(Status.error);
//                            return responseData;
                            return WebResult.getErrorResult("该订单换票失败，请重新换票!");
                        }
                    }
                    data.put("count", 1);
                    data.put("availableCount", orders.getAvailableCount());
                    data.put("orderId", orders.getOrderId());
                    data.put("activityId", orders.getActivityId());
                    data.put("episodeId", orders.getEpisodeId());
//                    responseData.setStatus(Status.success);
//                    responseData.setMsg("订单获取成功");
//                    responseData.setData(data);
                    WebResult<Object> result = WebResult.getSuccessResult();
                    result.setMsg("订单获取成功");
                    result.putResult("data", data);
                    return result;
                } else {
//                    responseData.setMsg("订单不存在,无法换票");
//                    responseData.setStatus(Status.error);
//                    return responseData;
                    return WebResult.getErrorResult("订单不存在,无法换票!");
                }
            } else {
//                responseData.setMsg("请选择换票模式");
//                responseData.setStatus(Status.error);
//                return responseData;
                return WebResult.getErrorResult("请选择换票模式!");
            }
        } else {
//            responseData.setMsg("用户授权登录失败,请后台退出微信重新换票");
//            responseData.setStatus(Status.error);
//            return responseData;
            return WebResult.getErrorResult("用户授权登录失败,请后台退出微信重新换票!");
        }
    }

    private void resetParentOrderInfo(Orders parentOrder, Orders otaOrder) {
        Orders firstChildOrder = ordersService.getFirstChildOrder(parentOrder.getOrderId());
        if (firstChildOrder != null) {
            parentOrder.setActivityId(firstChildOrder.getActivityId());
            parentOrder.setEpisodeId(firstChildOrder.getEpisodeId());
            parentOrder.setChargeId(firstChildOrder.getChargeId());

            parentOrder.setActivityName(firstChildOrder.getActivityName());
            parentOrder.setEpisodeName(firstChildOrder.getEpisodeName());
            parentOrder.setChargeName(firstChildOrder.getChargeName());

            parentOrder.setTotalPrice(parentOrder.getTotalPrice().subtract(otaOrder.getTotalPrice()));
            parentOrder.setChargeNum(parentOrder.getChargeNum() - otaOrder.getChargeNum());

            ordersService.updateOrders(parentOrder);
        }
    }

    private void resetOtaOrderInfo(Orders parentOrder, Orders otaOrder, Integer saveBindRecordType) {
        if (otaOrder != null) {
            if (saveBindRecordType == 0) {
                Integer bindType = null;
                if (parentOrder != null && parentOrder.getStatus() == 0) {
                    bindType = 3;
                } else if (parentOrder != null && parentOrder.getStatus() == 4) {
                    bindType = 4;
                }

                OtaBindingRecord otaBindingRecord = new OtaBindingRecord();
                otaBindingRecord.setOtaOrderId(otaOrder.getOrderId());
                otaBindingRecord.setParentOrderId(otaOrder.getParentId());
                otaBindingRecord.setType(bindType);
                otaBindingRecord.setCreateTime(new Date());
                otaService.saveOrUpdateRelation(otaBindingRecord, 0);
            }

            otaOrder.setParentId(0);
            otaOrder.setParentCode(otaOrder.getCode());
            otaOrder.setExtOrderType(1);
            ordersService.updateOrders(otaOrder);
        }
    }

    @RequestMapping(value = "/confirmOrder.json", method = RequestMethod.POST)
    @ResponseBody
    public Object confirmOtaRentOrder(@RequestParam("mainOrderId") Integer mainOrderId, @RequestParam("rentInfo") String rentInfo, HttpServletRequest request) {

        ResponseData responseData = new ResponseData();
        CommonAccount account = WebUpmsContext.getAccount(request);
        Organizer orgnization = WebUpmsContext.getOrgnization(request);
        if (null != account) {
            Integer customerId = account.getCustomerId();
            Orders mainOrder = ordersService.getSimpleOrdersById(mainOrderId);
            if (null == mainOrder || mainOrder.getStatus() != 1) {
                responseData.setMsg("原始订单有误，无法绑定");
                responseData.setStatus(Status.error);
            } else {
                Integer parentId = mainOrder.getParentId();
                if (null != parentId && parentId > 0) {
                    Orders parentOrder = ordersService.getSimpleOrdersById(mainOrder.getParentId());
                    if (null != parentOrder) {
                        if (parentOrder.getStatus() == 1) {
                            responseData.setMsg("该订单已经完成绑定并支付成功，无法重复绑定");
                            responseData.setStatus(Status.error);
                            return responseData;
                        } else if (parentOrder.getStatus() == 0) {
                            List<Orders> children = ordersService.getChildOrders(parentId);
                            for (Orders orders : children) {
                                if (orders.getBuyerId() < 0) {
                                    resetOtaOrderInfo(parentOrder, orders, 0);
                                    continue;
                                }
                                orders.setStatus(4);
                                ordersService.updateOrders(orders);
                            }
                            parentOrder.setStatus(4);
                            resetParentOrderInfo(parentOrder, mainOrder);
                        } else {
                            responseData.setMsg("订单状态有误，无法绑定");
                            responseData.setStatus(Status.error);
                            return responseData;
                        }
                    } else {
                        resetOtaOrderInfo(parentOrder, mainOrder, 1);
                    }
                }
                try {
                    Integer client = 0;
                    if (orgnization != null) {
                        client = orgnization.getOrganizerId();
                    }
                    Map<String, Object> orderResult = otaService.confirmRentOrder(customerId, mainOrderId, rentInfo, client);
                    Boolean hasError = (Boolean) orderResult.get("IsError");
                    if (!hasError) {
                        responseData.setStatus(Status.success);
                        Map map = new HashMap();
                        map.put("code", orderResult.get("code"));
                        map.put("orderId", orderResult.get("orderId"));
                        responseData.setData(map);
                    } else {
                        responseData.setStatus(Status.error);
                        responseData.setMsg((String) orderResult.get("ErrMsg"));
                    }
                } catch (Exception e) {
//                    log.error(AssUtils.parse(e));
                    responseData.setMsg("创建订单失败");
                    responseData.setStatus(Status.error);
                    return responseData;
                }
            }
        } else {
            responseData.setMsg("授权失败");
            responseData.setStatus(Status.error);
        }
        return responseData;
    }

    @RequestMapping(value = "/unbidOrder.json", method = RequestMethod.POST)
    @ResponseBody
    public Object unbindOtaOrder(@RequestParam("orderId") Integer orderId, HttpSession session) {
        ResponseData res = new ResponseData();
        Orders order = ordersService.getSimpleOrdersById(orderId);
        Orders parentOrder = ordersService.getSimpleOrdersById(order.getParentId());
        if (order == null || parentOrder == null) {
            res.setData(null);
            res.setMsg("订单不存在,无法进行解绑操作!");
            res.setStatus(Status.error);
            return  res;
        }

        if (order.getStatus() != 1 || order.getParentId() == null) {
            res.setData(null);
            res.setMsg("订单状态有误或订单已经解绑");
            res.setStatus(Status.error);
            return  res;
        }

        Map<String, Object> data = new HashMap<>();
//        ordersService.unbindOtaOrder(orderId);
        resetOtaOrderInfo(parentOrder, order, 0);
        parentOrder.setStatus(4);
        resetParentOrderInfo(parentOrder, order);
        data.put("orderId", orderId);
        res.setData(data);
        res.setMsg("订单解绑成功!");
        res.setStatus(Status.success);
        return  res;
    }

}
