package com.zoo.zoomerchantadminweb.order.controller;

import com.zoo.common.util.RedisUtil;
import com.zoo.web.bean.WebResult;
import org.apache.commons.lang3.StringUtils;

/**
 * 扫码支付结果
 */
public class SweepPayUtils {

    public static void putResult(long organizerId,long orderId,long customerId,String message,String status){
        String key = organizerId+"_" + orderId + "_" + customerId;
        RedisUtil.set(key,message+"%T%"+status , 60 * 60 * 24);
    }

    public static void clearResult(long organizerId,long orderId,long customerId){
        String key = organizerId+"_" + orderId + "_" + customerId;
        RedisUtil.remove(key);
    }

    public static WebResult getResult(long organizerId,long orderId,long customerId){
        WebResult result = WebResult.getSuccessResult();

        String key = organizerId+"_" + orderId + "_" + customerId;

        String value = RedisUtil.get(key);
        if(StringUtils.isEmpty(value)){
            result.put("payMessage","支付中");
            result.put("payStatus","PAYING");
        }else{
            String[] split = value.split("%T%");
            result.put("payMessage",split[0]);
            result.put("payStatus",split[1]);
        }
        return result;
    }

}
