package com.zoo.zoomerchantadminweb.order.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zoo.account.constant.AccountEnum;
import com.zoo.account.service.AccountService;
import com.zoo.account.support.AccountResult;
import com.zoo.activity.constant.NoticeConstant;
import com.zoo.activity.dao.nutz.pojo.OtaBindingRecord;
import com.zoo.activity.exception.OccupyException;
import com.zoo.activity.util.RegexUtil;
import com.zoo.activity.vo.RentBalance;
import com.zoo.bean.util.ActivityBeanUtils;
import com.zoo.common.page.PageList;
import com.zoo.activity.dao.model.*;
import com.zoo.activity.dao.nutz.pojo.ActivityRelation;
import com.zoo.activity.dao.nutz.pojo.CommonAccount;
import com.zoo.activity.service.*;
import com.zoo.activity.util.DateUtil;
import com.zoo.activity.util.bean.OrderResultBean;
import com.zoo.download.activity.service.RentDetailViewService;
import com.zoo.finance.dao.model.ZooOnAccountOrdersRemark;
import com.zoo.finance.service.ZooBalanceDepositIndemnityService;
import com.zoo.finance.service.ZooOnAccountOrdersRemarkService;
import com.zoo.icenow.dao.model.CommonAccountMoney;
import com.zoo.icenow.service.StorageService;
import com.zoo.partner.service.PartnerService;
import com.zoo.partner.service.PartnerSystemService;
import com.zoo.rest.sms.sdk.utils.encoder.BASE64Encoder;
import com.zoo.web.bean.WebResult;
import com.zoo.wechat.common.Config;
import com.zoo.wechat.util.profitSharing.ProfitSharingUtil;
import com.zoo.zoomerchantadminweb.base.BaseController;
import com.zoo.zoomerchantadminweb.context.WebUpmsContext;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URLEncoder;
import java.util.*;

@Api(value = "api/server/order", tags = {"主办方订单相关接口"}, description = "主办方订单相关接口描述")
@RestController
@RequestMapping("api/server/order")
public class OrderController extends BaseController {

    @Resource(name = "orderService")
    private OrdersService ordersService;

    @Resource(name = "playerService")
    private PlayerService playerService;

    @Resource(name = "activityService")
    private ActivityService activityService;

    @Resource(name = "episodeService")
    private EpisodeService episodeService;

    @Resource(name = "enrollInfoService")
    private EnrollInfoService enrollInfoService;

    @Resource(name = "chargeService")
    private ChargeService chargeService;

    @Resource(name = "verifyService")
    private VerifyService verifyService;

    @Resource(name = "AccountService")
    private AccountService accountService;

    @Resource(name = "organizerService")
    private OrganizerService organizerService;

    @Resource(name = "noticeService")
    private NoticeService noticeService;

    @Resource(name = "storageService")
    private StorageService storageService;

    @Autowired
    private CoachService coachService;

    @Autowired
    private OtaService otaService;

    @Autowired
    private PartnerService partnerService;

    @Autowired
    private PartnerSystemService partnerSystemService;

    @Autowired
    private ZooOnAccountOrdersRemarkService zooOnAccountOrdersRemarkService;

    @Autowired
    private DepositService depositService;

    @Autowired
    private ProfitSharingPlatformService platformService;

    @Autowired
    private ZooBalanceDepositIndemnityService indemnityService;

    @Autowired
    private OrganizerPayConfigService organizerPayConfigService;

    @Autowired
    private OrderSweepCodePayService orderSweepCodePayService;

    @Autowired
    private TeamCredentialsService teamCredentialsService;

    @Autowired
    private LimitCardService limitCardService;


    @Autowired
    private TicketLimitService ticketLimitService;

    /**
     * 后台使用的订单列表功能(订单列表可以分页,默认按照创建时间排序)<br/>
     * 刷选条件:活动ID,场次ID,订单状态，订单号(订单号可使用模糊查询)<br/>
     *
     * @param page       页数
     * @param size       页大小
     * @param activityId 活动ID
     * @param episodeId  场次ID
     * @param status     活动状态
     * @param code       订单号(模糊查询)
     * @return 返回一个PageList对象, list的装入对象:OrderView
     * @see OrderView
     */

    @ApiOperation(value = "订单列表", notes = "后台使用的订单列表功能(订单列表可以分页,默认按照创建时间排序)", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页", required = false, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "size", value = "条", required = false, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "activityId", value = "产品ID", required = false, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "episodeId", value = "场次ID", required = false, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "chargeId", value = "票务ID", required = false, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "status", value = "状态", required = false, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "phone", value = "手机号", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "code", value = "订单号", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "startTime", value = "开始时间", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "endTime", value = "结束时间", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "distribution", value = "分销", required = false, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "isOta", value = "OTA", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "payType", value = "订单来源{0: '线上支付', 2: '线上储值支付', 3: '线下储值支付', 4: '线下支付', 5: '线下现金', 6: '线下刷卡', 7: '分销商支付', 8: '线下挂账支付', 9: '线下招待支付', 10: '扫码支付'}", required = false, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "invoices", value = "开票状态 0未开 1已开", required = false, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "isHaveRemark", value = "是否存在异常备注 Y有异常备注 N无异常备注", required = false, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "keyword", value = "关键字", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "chargeMode", value = "票种分类", required = false, dataType = "integer", paramType = "query")
    })
    @RequestMapping(value = "/list.json", method = RequestMethod.GET)
    public Object getOrderList(@RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
                               @RequestParam(value = "size", required = false, defaultValue = "10") Integer size,
                               @RequestParam(value = "activityId", required = false) Integer activityId,
                               @RequestParam(value = "episodeId", required = false) Integer episodeId,
                               @RequestParam(value = "chargeId", required = false) Integer chargeId,
                               @RequestParam(value = "status", required = false) String status,
                               @RequestParam(value = "phone", required = false) String phone,
                               @RequestParam(value = "code", required = false) String code,
                               @RequestParam(value = "startTime", required = false) String startTime,
                               @RequestParam(value = "endTime", required = false) String endTime,
                               @RequestParam(value = "distribution", required = false) Integer distribution,
                               @RequestParam(value = "payType", required = false) Integer payType,
                               @RequestParam(value = "invoices", required = false) Integer invoices,
                               @RequestParam(value = "ishaveRemark", required = false) String ishaveRemark,
                               @RequestParam(value = "isOta", required = false, defaultValue = "-1") String isOta,
                               @RequestParam(value = "keyword", required = false) String keyword,
                               @RequestParam(value = "chargeMode", required = false) Integer chargeMode,
                               HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer != null) {
            if (code != null) {
                code = code.trim();
            }
            Integer organizerId = null;
            Integer ota = Integer.parseInt(isOta);
//            if (ota < 0) {
//                ota = null;
//            }
            organizerId = organizer.getOrganizerId();
//            else {
//                ota = organizer.getOrganizerId();
//            }

            if (StringUtils.isBlank(status) || status.equals("[]")) {
                status = null;
            } else {
                status = "(" + status + ")";
            }

            if(StringUtils.isNotEmpty(keyword)) {
                if(RegexUtil.isPhoneNumber(keyword)) {
                    phone = keyword;
                } else {
                    code = keyword;
                }
                keyword = null;
            }
            Orders orders = null;
            if (StringUtils.isNotBlank(code)) {
                orders = ordersService.getOrdersByCode(code);
            }
            if (orders != null && orders.getBuyerId() < 0) {
                code = orders.getParentCode();
            }
            PageList pageList = ordersService.getOrderViewListByOption(activityId, episodeId, chargeId, status, code, phone, size, page, organizerId, startTime, endTime,  ota < 0? null:ota, null, false, distribution, null, payType, invoices, ishaveRemark, null, null, keyword, chargeMode);


            for (Object o : pageList.getDataList()) {
                //OrderView orderView = (OrderView) o;
                Map orderView = (Map) o;
                if (organizer.getOrganizerId() < 0) {
                    Player player = playerService.getPlayersByOrder(MapUtils.getInteger(orderView, "orderId")).get(0);
                    orderView.put("buyerName", player.getRealName());
                    orderView.put("playerPhone", player.getPhone());
                }

                BigDecimal indemnityMoney = indemnityService.selectIndemnityMoney(orderView.get("code").toString());
                if(indemnityMoney == null) {
                    orderView.put("indemnityMoney", 0);
                    orderView.put("refundableDeposit", orderView.get("deposit") == null ? 0 : new BigDecimal(orderView.get("deposit").toString()).setScale(2, BigDecimal.ROUND_HALF_UP));
                } else {
                    orderView.put("indemnityMoney", indemnityMoney.setScale(2, BigDecimal.ROUND_HALF_UP));
                    orderView.put("refundableDeposit", orderView.get("deposit") == null ? 0 : new BigDecimal(orderView.get("deposit").toString()).subtract(indemnityMoney).setScale(2, BigDecimal.ROUND_HALF_UP));
                }

                Deposit deposit = depositService.getDepositDetailByCode(orderView.get("code").toString());
                if(deposit != null && deposit.getStatus() == 3) {
                    orderView.put("refundableDeposit", 0);
                }
                //orderView.setBuyerName(player.getRealName());
                //orderView.setPlayerPhone(player.getPhone());
            }


            /*for (Object o : pageList.getDataList()) {
                Map orderView = (Map)o;
                Integer payTypeId = MapUtils.getInteger(orderView,"pay_type_id");
                if(payTypeId!=null){
                    String payName = FinanceExcelUtil.getPayName(payTypeId);
                    orderView.put("pay_type_id",payName);
                }
            }*/
//            BigDecimal allOrdersPrice = ordersService.getAllOrdersPriceByOption(activityId, episodeId, status, code, phone, organizerId, startTime, endTime, false, distribution);
//            BigDecimal allOrdersDeposit = ordersService.getAllOrdersDepositByOption(activityId, episodeId, status, code, phone, organizerId, startTime, endTime, false);
            Map<String, Object> map = new HashMap<>();
            map.put("list", pageList);
//            map.put("allOrdersPrice", allOrdersPrice);
//            map.put("allOrdersDeposit", allOrdersDeposit);
            return WebResult.getSuccessResult("data", map);
        } else {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
    }


    /**
     * 订单详情界面
     *
     * @param orderId 订单id(只有活动的组织者才能看到该订单详情)
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "订单详情", notes = "订单详情(只有活动的组织者才能看到该订单详情)", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "orderId", value = "订单ID", required = true, dataType = "integer", paramType = "query"),
    })
    @RequestMapping("/detail.json")
    public Object orderDetailToJson(@RequestParam("orderId") Integer orderId, HttpServletRequest request) {
        Map<Object, Object> map = new HashMap<>();
        OrderView orderView = ordersService.getOrderDetail(orderId);

        Organizer organizer = WebUpmsContext.getOrgnization(request);
        /*
         * 如果订单核销权限不属于当前登录用户 返回没有权限
         */

        if (!orderView.getSellerId().equals(organizer.getOrganizerId())) {
            List<ActivityRelation> relations = activityService.getRelationIds(orderView.getActivityId());
            boolean hasPermission = false;
            for (ActivityRelation relation : relations) {
                if (relation.getOrganizerId().equals(organizer.getOrganizerId())) {
                    hasPermission = true;
                    break;
                }
            }
            if (orderView.getVerifyOrganizer().equals(organizer.getOrganizerId())) {
                hasPermission = true;
            }
            if (!hasPermission) {
                return WebResult.getErrorResult(WebResult.Code.NO_PERMISSION);
            }
        }
        List<OrderView> orderViews = new ArrayList<>();
        try {
            if (null == orderView.getParentId()) {
                orderViews.addAll(ordersService.getChildOrderViews(orderView.getOrderId()));
            } else if (orderView.getParentId() == 0) {
                orderViews.add(orderView);
            }

            for (OrderView temp : orderViews) {
                temp.setEpisodeName(episodeService.getEpisodeDetailById(temp.getEpisodeId()).getName());
            }

        } catch (Exception e) {
//            logger.error(AssUtils.parse(e));
        }
        List<Map> playerMeta = JSON.parseArray(episodeService.getEpisodeDetailById(orderView.getEpisodeId()).getPlayerMeta().replace("\n", ""), Map.class);
        List<PlayerWithEnrollInfo> playerWithEnrollInfoList = enrollInfoService.getPlayerWithEnrollInfoByOrderId(orderId);
        map.put("parentOrder", orderView);
        map.put("orders", orderViews);
        map.put("players", playerWithEnrollInfoList);
        map.put("playerMeta", playerMeta);
        map.put("cardType", orderView.getCardType());
        if (orderView.getCardType() == 2) {
            Integer verifyCount = 0;
            List<VerifyRecord> records = verifyService.getVerifyRecordListByOrder(orderView.getOrderId());
            for (VerifyRecord verifyRecord : records) {
                verifyCount += verifyRecord.getVerifyOrderNum();
            }
            Charge charge = chargeService.getChargeById(orderView.getChargeId());
            map.put("chargeNumber", charge.getChargeNumber() * orderView.getChargeNum());
            map.put("leftNumber", charge.getChargeNumber() * orderView.getChargeNum() - verifyCount);
        }
        if (orderView.getCardType() == 3) {
            Charge charge = chargeService.getChargeById(orderView.getChargeId());
            List<ItemRule> allItems = JSONArray.parseArray(charge.getItemRule(), ItemRule.class);
            for (ItemRule rule : allItems) {
                rule.setAvailableNumber(orderView.getChargeNum());
            }
            List<VerifyRecord> verifyRecords = verifyService.getVerifyRecordListByOrder(orderId);
            for (VerifyRecord record : verifyRecords) {
                for (ItemRule rule : allItems) {
                    if (record.getVerifyItemIndex().equals(rule.getItemIndex())) {
                        rule.setAvailableNumber(rule.getAvailableNumber() - record.getVerifyOrderNum());
                    }
                }
            }
//            model.addAttribute("unitPrice", orderView.getTotalPrice().divide(new BigDecimal(orderView.getChargeNum())));
            map.put("unitPrice", orderView.getTotalPrice().divide(new BigDecimal(orderView.getChargeNum())));
            if (charge.getChargeMode() == 2) {
                List<Integer> itemsSelected = JSON.parseArray(orderView.getItems(), Integer.class);
                List<ItemRule> tmp = new ArrayList<>();
                for (ItemRule itemRule : allItems) {
                    if (itemsSelected.contains(itemRule.getItemIndex())) {
                        tmp.add(itemRule);
                    }
                }
                allItems = tmp;
            }
            map.put("items", JSON.toJSONString(allItems).replaceAll("\'", ""));
        } else {
            map.put("items", "");
        }
        return WebResult.getSuccessResult("data", map);
    }

    /**
     * 订单详情界面（coach端用）
     *
     * @param orderId 订单id(只有活动的组织者才能看到该订单)
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "订单详情(coach端用)", notes = "订单详情(只有活动的组织者才能看到该订单详情)", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "orderId", value = "订单ID", required = false, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "code", value = "订单号", required = false, dataType = "string", paramType = "query"),
    })
    @RequestMapping(value = "/orderDetail.json", method = RequestMethod.GET)
    public Object orderDetailJson(@RequestParam(value = "orderId", required = false) Integer orderId,
                                  @RequestParam(value = "code", required = false) String code,
                                  HttpServletRequest request) throws Exception {
        Map<String, Object> resMap = new HashMap<>();
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        List<OrderView> orderViewList = new ArrayList<>();
        Integer isOwner = 0;
        Episode episode = null;
        boolean isOnlyRent = false;

        try {
            if (null != organizer) {
                /**
                 * 检查当前订单是否的商家是否是 当前的登录用户
                 */
                OrderView orderView = null;
                if (orderId != null) {
                    orderView = ordersService.getOrderDetail(orderId);
                }
                if (StringUtils.isNotBlank(code)) {
                    Orders tmp = ordersService.getOrderDetailByCode(code);
                    orderView = ordersService.getOrderDetail(tmp.getOrderId());
                }
                if (orderView == null || (!orderView.getSellerId().equals(organizer.getOrganizerId()))) {
                    boolean hasPermission = false;
                    if (null != orderView) {
                        List<ActivityRelation> activityRelations = activityService.getRelationIds(orderView.getActivityId());
                        for (ActivityRelation relation : activityRelations) {
                            if (relation.getOrganizerId().equals(organizer.getOrganizerId())) {
                                hasPermission = true;
                                break;
                            }
                        }
                        if (!hasPermission) {
                            return WebResult.getErrorResult(WebResult.Code.NO_PERMISSION);
                        } else {
                            isOwner = 0;
                        }
                    } else {
                        return WebResult.getErrorResult(WebResult.Code.NO_PERMISSION);
                    }
                } else {
                    isOwner = 1;
                }
                if (null == orderView.getParentId()) {
                    resMap.put("mainOrder", orderView);
                    OrderView orderView1 = ordersService.getDefaultChildOrderView(orderView.getOrderId());
                    if (orderView1 == null) {
                        orderView1 = ordersService.getChildOrderViews(orderView.getOrderId()).get(0);
                        isOnlyRent = true;
                    }
                    Pay pay = null;
                    if (orderView1 != null) {
                        pay = ordersService.getPayInfoByOrderId(orderView1.getOrderId());
                        Charge charge = chargeService.getChargeById(orderView.getChargeId());
                        if (charge.getChargeMode() == 3) {
                            List<Map> limitCardEntryList = ordersService.getLimitCardEntry(orderView1.getOrderId());
                            if(!CollectionUtils.isEmpty(limitCardEntryList)){
                                resMap.put("chargeMode", charge.getChargeMode());
                                resMap.put("entryList", limitCardEntryList);
                                long count = limitCardEntryList.stream().filter(e -> Integer.parseInt(e.get("status").toString()) == 1 || Integer.parseInt(e.get("status").toString()) == 2).count();
                                resMap.put("availableCount", limitCardEntryList.size() - count);
                                Deposit deposit = depositService.getDepositDetailByCode(orderView.getCode());
                                if (deposit != null) {
                                    resMap.put("depositStatus", deposit.getStatus());
                                }
                            }
                        }
                    }

                    resMap.put("pay", pay);
                    Integer sumOrderId = orderView.getOrderId();
                    orderViewList.addAll(ordersService.getChildOrderViews(sumOrderId));
                    Activity activity = activityService.getActivityById(orderView.getActivityId());
                    episode = episodeService.getEpisodeDetailById(orderView.getEpisodeId());
//                    logger.info(JSON.toJSONString(activity));
                    if (activity != null) {
                        resMap.put("activityType", activity.getTypeId());
                    }
                    if (activity.getTypeId() == 2) {
                        List<Player> players = ordersService.getOrderPlayers(orderId);
                        resMap.put("appointmentTime", DateUtil.getZhCNDateTime(players.get(0).getAppointmentTime()));
                    } else {
                        List<Player> players = new ArrayList<>();
                        if (isOnlyRent) {
                            players = ordersService.getOrderPlayers(orderView.getOrderId());
                        } else {
                            if (orderViewList.size() > 0) {
                                players = ordersService.getOrderPlayers(orderViewList.get(0).getOrderId());
                            }
                        }
                        if (players.size() > 0 && null != players.get(0).getAppointmentTime()) {
                            resMap.put("otherAppointmentTime", DateUtil.getZhCNDate(players.get(0).getAppointmentTime()));
                        }
                    }
                    resMap.put("activity", activity);
                    OrderView orderView2 = ordersService.getDefaultChildOrderView(orderView.getOrderId());
                    List<PlayerWithEnrollInfo> playerWithEnrollInfoList = null;
                    if (orderView2 != null) {
                        playerWithEnrollInfoList = enrollInfoService.getPlayerWithEnrollInfoByOrderId(orderView2.getOrderId());
                    }
                    if (isOnlyRent) {
                        playerWithEnrollInfoList = enrollInfoService.getPlayerWithEnrollInfoByOrderId(sumOrderId);
                    }
                    resMap.put("players", playerWithEnrollInfoList);

                } else if (0 == orderView.getParentId()) {
                    episode = episodeService.getEpisodeDetailById(orderView.getEpisodeId());
                    if (orderId != null) {
                        orderView = ordersService.getCustomerOrderDetail(orderId, null);
                    }
                    if (StringUtils.isNotBlank(code)) {
                        orderView = ordersService.getCustomerOrderDetailByCode(code, null, null);
                    }
                    Pay pay = ordersService.getPayInfoByOrderId(orderView.getOrderId());
                    resMap.put("pay", pay);

                    orderViewList.add(orderView);
                    Activity activity = activityService.getActivityById(orderView.getActivityId());
//                    logger.info(JSON.toJSONString(activity));
                    if (activity != null) {
                        resMap.put("activityType", activity.getTypeId());
                    }

                    if (activity.getTypeId() == 2) {
                        List<Player> players = ordersService.getOrderPlayers(orderId);
                        resMap.put("appointmentTime", DateUtil.getZhCNDateTime(players.get(0).getAppointmentTime()));
                    } else {
                        List<Player> players = ordersService.getOrderPlayers(orderId);
                        if (players.size() > 0 && null != players.get(0).getAppointmentTime()) {
                            resMap.put("otherAppointmentTime", DateUtil.getZhCNDate(players.get(0).getAppointmentTime()));
                        }
                    }

                    Charge charge = chargeService.getChargeById(orderView.getChargeId());
                    if (charge.getChargeMode() == 4) {
                        List<Map> entryAndActivationRecordList = ordersService.getEntryAndActivationRecordList(orderView.getOrderId());

                        if(!CollectionUtils.isEmpty(entryAndActivationRecordList)){
                            resMap.put("chargeMode", charge.getChargeMode());
                            resMap.put("entryList", entryAndActivationRecordList);
                        }

                    }
                    if (charge.getChargeMode() == 3) {
                        List<Map> limitCardEntryList = ordersService.getLimitCardEntry(orderView.getOrderId());
                        if(!CollectionUtils.isEmpty(limitCardEntryList)){
                            resMap.put("chargeMode", charge.getChargeMode());
                            resMap.put("entryList", limitCardEntryList);
                            long count = limitCardEntryList.stream().filter(e -> Integer.parseInt(e.get("status").toString()) == 1 || Integer.parseInt(e.get("status").toString()) == 2).count();
                            resMap.put("availableCount", limitCardEntryList.size() - count);
                        }
                    }
                    if (activity.getCardType() == 2) {
                        List<VerifyRecord> verifyRecords = verifyService.getVerifyRecordListByOrder(orderId);
                        resMap.put("chargeNumber", charge.getChargeNumber() * orderView.getChargeNum());
                        Integer verifyCount = 0;
                        for (VerifyRecord record : verifyRecords) {
                            verifyCount += record.getVerifyOrderNum();
                        }
                        resMap.put("leftNumber", charge.getChargeNumber() * orderView.getChargeNum() - verifyCount);
                        List<Map> cardVerify = new ArrayList<>();
                        for (VerifyRecord verifyRecord : verifyRecords) {
                            CommonAccount verifyUser = (CommonAccount) accountService.getAccount(AccountEnum.FetchAccountTypeEnum.FETCH_BY_CUSTOMER.getValue(), verifyRecord.getVerifyUser()).getData();
                            String verifyOrg = organizerService.getOrgNameById(verifyUser.getOrganizerId());
                            Map<String, Object> map = new HashMap<>();
                            map.put("verifyTime", DateUtil.getDateTimeStr(verifyRecord.getVerifyTime()));
                            map.put("verifyOrg", verifyOrg);
                            map.put("verifyNum", verifyRecord.getVerifyOrderNum());
                            cardVerify.add(map);
                        }
                        resMap.put("cardVerifyList", cardVerify);
                    }
                    resMap.put("activity", activity);
                    List<PlayerWithEnrollInfo> playerWithEnrollInfoList = enrollInfoService.getPlayerWithEnrollInfoByOrderId(orderId);
                    resMap.put("players", playerWithEnrollInfoList);
                } else {
                    resMap.put("subOrderId", orderView.getOrderId());
                    OrderView mainOrderView = ordersService.getOrderDetail(orderView.getParentId());
                    episode = episodeService.getEpisodeDetailById(mainOrderView.getEpisodeId());
                    resMap.put("mainOrder", mainOrderView);
                    orderViewList.addAll(ordersService.getChildOrderViews(orderView.getParentId()));
                    Activity activity = activityService.getActivityById(orderView.getActivityId());
//                    logger.info(JSON.toJSONString(activity));
                    if (activity != null) {
                        resMap.put("activityType", activity.getTypeId());
                    }
                    if (activity.getTypeId() == 2) {
                        List<Player> players = ordersService.getOrderPlayers(orderId);
                        resMap.put("appointmentTime", DateUtil.getZhCNDateTime(players.get(0).getAppointmentTime()));
                    } else {
                        OrderView defaultOrder = ordersService.getDefaultChildOrderView(mainOrderView.getOrderId());
                        if (null == defaultOrder) {
                            isOnlyRent = true;
                        }
                        List<PlayerWithEnrollInfo> playerWithEnrollInfoList = null;
                        List<Player> players = ordersService.getOrderPlayers(orderId);
                        if (isOnlyRent) {
                            playerWithEnrollInfoList = enrollInfoService.getPlayerWithEnrollInfoByOrderId(mainOrderView.getOrderId());
                            players = ordersService.getOrderPlayers(mainOrderView.getOrderId());
                        } else {
                            playerWithEnrollInfoList = enrollInfoService.getPlayerWithEnrollInfoByOrderId(defaultOrder.getOrderId());
                        }
                        resMap.put("players", playerWithEnrollInfoList);
                        if (players.size() > 0 && null != players.get(0).getAppointmentTime()) {
                            resMap.put("otherAppointmentTime", DateUtil.getZhCNDate(players.get(0).getAppointmentTime()));
                        }
                    }
                    Pay pay = ordersService.getPayInfoByOrderId(orderView.getOrderId());
                    resMap.put("pay", pay);
                    resMap.put("activity", activity);
                }
            }
            List<Map> playerMeta = JSON.parseArray(episode.getPlayerMeta().replace("\n", ""), Map.class);
            resMap.put("playerMeta", playerMeta);
            resMap.put("isOwner", isOwner);
            resMap.put("orders", orderViewList);
            if(resMap.containsKey("mainOrder")){
                OrderView view = (OrderView)resMap.get("mainOrder");
                BigDecimal indemnityMoney = indemnityService.selectIndemnityMoney(view.getCode());
                if(indemnityMoney == null) {
                    resMap.put("indemnityMoney", 0);
                    resMap.put("refundableDeposit", view.getDeposit() == null ? 0 : view.getDeposit().setScale(2, BigDecimal.ROUND_HALF_UP));
                } else {
                    resMap.put("indemnityMoney", indemnityMoney.setScale(2, BigDecimal.ROUND_HALF_UP));
                    resMap.put("refundableDeposit", view.getDeposit() == null ? 0 : view.getDeposit().subtract(indemnityMoney).setScale(2, BigDecimal.ROUND_HALF_UP));
                }

                Deposit deposit = depositService.getDepositDetailByCode(view.getCode());
                if(deposit != null && deposit.getStatus() == 3) {
                    resMap.put("refundableDeposit", 0);
                }

            }
            if (isOnlyRent) {
                resMap.put("size", orderViewList.size() + 1);
            } else {
                resMap.put("size", orderViewList.size());
            }

        } catch (Exception e) {
            e.printStackTrace();
            WebResult.getErrorResult(WebResult.Code.ERROR);
        }

        return WebResult.getSuccessResult("data", resMap);
    }

    /**
     * 申请退款
     */
    @ApiOperation(value = "申请退款", notes = "申请退款", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "refundInfo", value = "退款信息", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "multi", value = "是否为复合订单", required = true, dataType = "string", paramType = "query"),
    })
    @RequiresPermissions(value = {"sys_manager:cashier:order:read:applyRefund", "sys_manager:order:read:applyRefund"}, logical = Logical.OR)
    @RequestMapping(value = "/applyRefund.json", method = RequestMethod.POST)
    public Object applyRefund(@RequestParam("refundInfo") String refundInfo, @RequestParam("multi") String multi, HttpServletRequest request) throws Exception {

        CommonAccount cashierAccount = WebUpmsContext.getAccount(request);

        Organizer organizer = WebUpmsContext.getOrgnization(request);
        List<Map> refundJson = JSON.parseArray(refundInfo, Map.class);
        Charge charge = chargeService.getChargeById(ordersService.getSimpleOrdersById(Integer.parseInt(refundJson.get(0).get("orderId").toString())).getChargeId());
        Episode episode = episodeService.getEpisodeDetailById(charge.getEpisodeId());

        String msg = "该订单不可" + (charge.getPrice().doubleValue() == 0 ? "取消。" : "退款。");
        Boolean hasError = false;

        Orders parentOrders;

        if (multi.equals("0")) {
            parentOrders = ordersService.getSimpleOrdersById(Integer.parseInt(refundJson.get(0).get("orderId").toString()));
        } else {
            parentOrders = ordersService.getSimpleOrdersById(ordersService.getSimpleOrdersById(Integer.parseInt(refundJson.get(0).get("orderId").toString())).getParentId());
        }

        if((new Date().getTime() - parentOrders.getCreateTime().getTime()) / 1000 / 3600 / 24 >= 300 && parentOrders.getPayTypeId() == 1){
            return WebResult.getErrorResult("该笔订单已超过在线支付原路退款有效期（300天），如需退款请采用其它方式");
        }

        BigDecimal totalRefundPrice = BigDecimal.valueOf(0);

        for (Map refundMap : refundJson) {
            totalRefundPrice = totalRefundPrice.add(new BigDecimal(refundMap.get("refundPrice").toString()));
            Integer orderId = Integer.parseInt(refundMap.get("orderId").toString());
            Pay payInfoByOrderId = ordersService.getPayInfoByOrderId(orderId);
            //挂账招待不可执行退款
            if (payInfoByOrderId.getPayType() != null && (payInfoByOrderId.getPayType().intValue() == 9 || payInfoByOrderId.getPayType().intValue() == 10)) {
                return WebResult.getErrorResult(WebResult.Code.HUNG_UP_CAN_NOT_REFUND);
            }
        }

        // 分账回退检查
        ProfitSharingPlatform profitSharingPlatform = platformService.selectByPayTypeId(parentOrders.getPayTypeId());
        if (profitSharingPlatform != null && profitSharingPlatform.getValid() == 1 && parentOrders.getProfitSharingStatus() == 2) {
            Map<String, String> params = new HashMap<>();
            params.put("code", parentOrders.getParentCode());
            params.put("abnormalRefundMoney", totalRefundPrice.toString());
            if (!ProfitSharingUtil.profitSharingReturn(params)) {
                return WebResult.getErrorResult(WebResult.Code.PROFIT_SHARING_RETURN_ERROR);
            }
        }

        Activity activity = activityService.getActivityById(episode.getActivityId());
        AccountResult accountResult = accountService.getAccount(AccountEnum.FetchAccountTypeEnum.FETCH_BY_CUSTOMER.getValue(), ordersService.getSimpleOrdersById(Integer.parseInt(refundJson.get(0).get("orderId").toString())).getBuyerId().toString());
        CommonAccount customer = (CommonAccount) accountResult.getData();
        OrderResultBean orderResultBean;
        Integer refundNumber = 0;
        Integer sumNumber = 0;

        // 将对接erp代码迁移为service服务 使用service形式去调用对接退款问题
        if ((parentOrders.getSellerId().equals(220) || Config.instance().getKunlunPartnerOrgIds().contains(String.valueOf(parentOrders.getSellerId()))) && !activity.getTypeId().equals(8) && StringUtils.isNotBlank(parentOrders.getVerifyCode())) {
            PartnerSystem partnerSystem = partnerSystemService.getPartnerSystemByOrgId(parentOrders.getSellerId());
            Map<String, String> resultMap = partnerService.partnerOrderQuery(parentOrders.getVerifyCode(), partnerSystem.getInterfaceType());
            if (null != resultMap && resultMap.containsKey("isVerify") && resultMap.get("isVerify").equals("T")) {
                String user = null;
                if (parentOrders.getSellerId().equals(220)) {
                    user = "taiwu_" + resultMap.get("verifyTime");
                } else {
                    user = "partner-cuiyunshan";
                }
                verifyService.verify(parentOrders.getOrderId(), parentOrders.getAvailableCount(), 6, parentOrders.getSellerId(), user, 3, -1);
                //该订单已经核销
                return WebResult.getErrorResult(WebResult.Code.CASHIER_CANT_VERIFY);
            }
        }

        String transctionId = "";
        BigDecimal totalPrice = new BigDecimal(0);
        Orders otaOrder = null;
        for (Map refundMap : refundJson) {
            if (new BigDecimal(refundMap.get("refundPrice").toString()).doubleValue() > -1) {
                Integer orderId = Integer.parseInt(refundMap.get("orderId").toString());
                Orders order = ordersService.getSimpleOrdersById(orderId);

                if (order.getStatus() != 1) {
                    return WebResult.getErrorMsgResult(WebResult.Code.ERROR, "订单状态有误，请刷新页面后重试");
                }

                if (order.getBuyerId() <= 0 && order.getStatus() == 1) {
                    otaOrder = order;
                    continue;
                }

                totalPrice = totalPrice.add(new BigDecimal(refundMap.get("refundPrice").toString()));
                charge = chargeService.getChargeById(order.getChargeId());
                Episode tmp = episodeService.getEpisodeByChargeId(charge.getChargeId());
                Activity temActivity = activityService.getActivityById(tmp.getActivityId());
                // 检查状态是否是Order status == 1 或者status == 5
                if (order.getStatus() != 1 && order.getStatus() != 5) {
                    hasError = true;
                }
                // 有错
                if (hasError) {
                    return WebResult.getErrorMsgResult(WebResult.Code.ERROR, msg);
                }
//                logger.info("manage端 --申请退款：order " + JSON.toJSONString(order));


                Boolean noRefund = false;
                List<Integer> canRefundExtendIdList = new ArrayList<>();
                //教练次卡退款处理（修改预约教练的状态）
                if (temActivity.getTypeId() == 8 && charge.getChargeMode() == 3 && temActivity.getActivityMode() == 1) {
                    //查询订单下所有预约
                    List<OrderCoachExtend> orderCoachExtendList = coachService.findOrderCoachExtendListByOrderId(orderId, 135);

                    for (OrderCoachExtend extend : orderCoachExtendList) {
                        if (extend.getExtendStatus() == 1 || extend.getExtendStatus() == 3 || extend.getExtendStatus() == 5) {
                            noRefund = true;
                            break;
                        } else {
                            canRefundExtendIdList.add(extend.getExtendId());
                        }
                    }
                } else if (temActivity.getTypeId() == 8 && temActivity.getActivityMode() == 2) {
                    if (charge.getCloudsType() > 1) {
                        return WebResult.getErrorResult(WebResult.Code.COACH_COURSE_LEAGUE_REFUND_CASHIER_FAIL);
                    }
                }

                //判断是教练次卡能否退款
                if (noRefund) {
                    //"退款失败,该订单已有教练确认或出导"
                    return WebResult.getErrorResult(WebResult.Code.COACH_REFUND_CASHIER_FAIL);
                }

                try {
                    // 退款处理
                    if (tmp.getEpisodeId().equals(episode.getEpisodeId())) {
                        orderResultBean = ordersService.createRefund(cashierAccount, customer, order, tmp, charge, new BigDecimal(refundMap.get("refundPrice").toString()), canRefundExtendIdList, null);
                    } else {
                        orderResultBean = ordersService.createRefund(cashierAccount, customer, order, tmp, charge, new BigDecimal(refundMap.get("refundPrice").toString()), canRefundExtendIdList, false);
                    }

                    refundNumber += orderResultBean.getResultCount();
                    sumNumber++;
                    transctionId = orderResultBean.getTransactionId();

                    if(order.getParentId() != null && charge.getIncrement() != 1 && order.getEquipmentId() == null) {
//                        List<Player> players = playerService.getPlayersByOrder(order.getOrderId());
//                        Date appointmentTime = null;
//                        if(!CollectionUtils.isEmpty(players)) {
//                            appointmentTime = players.get(0).getAppointmentTime();
//                        }
//                        ticketLimitService.changeSellDateCount(charge.getChargeId(), customer.getCustomerId(), order.getPayTime(), order.getAvailableCount());


                        if (charge.getSellLimitType() == 1) {
                            ticketLimitService.changeSellDateCount(charge.getChargeId(), null, order.getPayTime(), order.getAvailableCount());
                        } else if (charge.getSellLimitType() == 2) {

                            ticketLimitService.changeSellDateCount(charge.getChargeId(), null, ticketLimitService.getAppointmentTime(order.getOrderId()), order.getAvailableCount());
                        }

                    }
                } catch (OccupyException e) {
                    return WebResult.getErrorMsgResult(WebResult.Code.ERROR, e.getMessage());
                } catch (RuntimeException e) {
                    return WebResult.getErrorMsgResult(WebResult.Code.ERROR, e.getMessage());
                } catch (Exception e) {
                    return WebResult.getErrorMsgResult(WebResult.Code.ERROR, e.getMessage());
                }
            }
        }

        if (!refundNumber.equals(sumNumber)) {
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        } else {

            if (!multi.equals("0")) {
                ordersService.createRefundForParent(totalPrice, transctionId, parentOrders, parentOrders.getAvailableCount() != 0 ? parentOrders.getAvailableCount() : parentOrders.getChargeNum());
            }

            if (otaOrder != null) {
                otaOrder.setParentId(0);
                otaOrder.setParentCode(otaOrder.getCode());
                otaOrder.setExtOrderType(1);
                ordersService.updateOrders(otaOrder);

                OtaBindingRecord bindingRecord = new OtaBindingRecord();
                bindingRecord.setOtaOrderId(otaOrder.getOrderId());
                bindingRecord.setParentOrderId(parentOrders.getOrderId());
                bindingRecord.setType(2);
                bindingRecord.setCreateTime(new Date());
                otaService.saveOrUpdateRelation(bindingRecord, 0);

                resetParentOrderInfo(parentOrders, otaOrder);
            }

            // 更新session
//            SystemSessionUtil.setAccount((CommonAccount) accountResult.getData(), session);
            // 给主办方发送邮件通知
            List<String> tempEmailList = new ArrayList<>();
            String noticeEmail = "";
            if (StringUtils.isNotBlank(organizer.getEmail())) {
                noticeEmail = organizer.getEmail();
            }
            if (StringUtils.isNotBlank(episode.getNoticeEmail())) {
                noticeEmail = episode.getNoticeEmail();
            }
            if (StringUtils.isNotBlank(noticeEmail)) {
                tempEmailList.add(noticeEmail);
            }
//            ZooNotify.notifyByEmail(tempEmailList, "滑雪族活动 - 退款申请", ("您的#" + activity.getTitle() + "#有新的退款申请，请前往 http://" + Config.instance().getDomain() + "/server/order-detail?orderId=" + parentOrders.getOrderId() + "  进行查看。"));

            // 使用微信模板发送 退款通知
            if (!parentOrders.getPayType().equals("线下支付")) {
                noticeService.asyncWechatPush((CommonAccount) accountResult.getData(), null, null, charge, parentOrders, null, null, null, null, null, null, NoticeConstant.APPLY_RERUND, 0);
            }

            // 给管理员发送邮件通知
            List<String> notifyTarget = new ArrayList<>();
            notifyTarget.add("isnowadmin@huaxuezoo.com");


            CommonAccount account = WebUpmsContext.getAccount(request);
            String opUserId = Config.instance().getOpUserId();

            OrganizerWithScenic organizerWithScenic = organizerService.getAllInfoById(organizer.getOrganizerId());
            if((parentOrders.getPayTypeId() == 1 || parentOrders.getPayTypeId() == 2 || parentOrders.getPayTypeId() == 4 || parentOrders.getPayTypeId() == 13) && organizerWithScenic.getRefundConfirm() == 1) {
                ordersService.taskRefund(opUserId, account, parentOrders.getCode(), organizer.getOrganizerId());
            }

//            ZooNotify.notifyByEmail(notifyTarget, "滑雪族活动 - 退款申请", "有新的退款申请提交，请前往http://" + Config.instance().getDomain() + "/manage/refund-list  进行处理。")
            return WebResult.getSuccessResult();
        }
    }

    @ApiOperation(value = "开发票", notes = "修改订单的发票状态", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "orderId", value = "订单ID", required = true, dataType = "integer", paramType = "query"),
    })
    @RequestMapping(value = "/dealInvoices.json", method = RequestMethod.POST)
    public Object dealInvoices(@RequestParam("orderId") Integer orderId) {
        Orders orders = ordersService.getSimpleOrdersById(orderId);
        orders.setInvoices(1);
        ordersService.updateOrders(orders);
        return WebResult.getSuccessResult();
    }


    @ApiOperation(value = "绑定订单check", notes = "检查该订单是否已经绑定过", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "orderId", value = "订单ID", required = true, dataType = "integer", paramType = "query"),
    })
    @RequestMapping("/change/check.json")
    public Object orderChangeCheck(@RequestParam("orderId") Integer orderId,
                                   HttpServletRequest request) {
        OrderChange orderChange = ordersService.getOrderChangeByOrderIdAndStatus(orderId, 1);
        if (null != orderChange) {
            return WebResult.getErrorMsgResult(WebResult.Code.ERROR, "已经绑定过用户");
        }
        return WebResult.getSuccessResult();
    }


    @ApiOperation(value = "绑定订单", notes = "将线下订单绑定为线上订单", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "orderId", value = "订单ID", required = true, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "password", value = "密码", required = true, dataType = "integer", paramType = "query"),
    })
    @RequestMapping("/change.json")
    public Object orderChange(@RequestParam("orderId") Integer orderId,
                              @RequestParam("password") String password,
                              HttpServletRequest request) {
        CommonAccount account = WebUpmsContext.getAccount(request);
        Orders orders = ordersService.getSimpleOrdersById(orderId);

        if (!account.getOrganizerId().equals(orders.getSellerId()) || !orders.getBuyerId().equals(account.getCustomerId())) {
            return WebResult.getErrorResult(WebResult.Code.CASHIER_ORDER_BAIND_NOT_PERMISSION);
        }
        if (password != null) {
            password = password.trim();
        }
        OrderChange orderChange = ordersService.getOrderChangeByOrderIdAndStatus(orderId, 0);
        String code;
        if (null == orderChange) {
            code = ordersService.createOrderChange(orderId, account.getCustomerId(), password);
        } else {
            ordersService.updateOrderChange(orderChange.getChangeId(), password);
            code = new BASE64Encoder().encode(orderChange.getChangeId().toString().getBytes());
        }
        String url = "http://" + Config.instance().getDomain() + "/wechat/home?clientId=" + orders.getSellerId() + "&routePath=/order/change/" + URLEncoder.encode(code) + "/#/order/change/" + URLEncoder.encode(code) + "/";
        return WebResult.getSuccessResult("data", url);
    }


    /**
     * 导出订单信息（按照操作员和产品分类进行导出）优化版
     */
    @ApiOperation(value = "导出订单信息（按照操作员和产品分类进行导出）", httpMethod = "GET")
    @RequestMapping(value = "/export-orders-with-operator-and-episode-info-optimize", method = RequestMethod.GET)
    public void exportOrdersWithOperatorAndEpisodeInfoOptimize(@RequestParam(value = "startTime", required = false) String startTime,
                                                               @RequestParam(value = "endTime", required = false) String endTime,
                                                               @RequestParam(value = "activityId", required = false) Integer activityId,
                                                               @RequestParam(value = "episodeId", required = false) Integer episodeId,
                                                               @RequestParam(value = "status", required = false) Integer status,
                                                               @RequestParam(value = "code", required = false) String code,
                                                               @RequestParam(value = "phone", required = false) String phone, // 操作员的联系电话
                                                               HttpServletRequest request, HttpServletResponse response) throws Exception {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer != null) {

            Map<String, Object> ordersWithOperatorList = ordersService.getOperatorByOdersNotPageOptimize(organizer.getOrganizerId(), startTime, endTime, activityId, episodeId, status, code, phone);
            String baseFilePath = request.getSession().getServletContext().getRealPath("/");
            String filePath = ordersService.exportOperatorExcelFileOptimize(ordersWithOperatorList, baseFilePath);
            downLoadFile(filePath, response, "线下操作员订单信息.xls");
        }

    }

    @ApiOperation(value = "获取支付信息", notes = "获取支付信息，用于打印", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "orderId", value = "订单ID", required = true, dataType = "integer", paramType = "query"),
    })
    @RequestMapping(value = "/payInfo.json", method = RequestMethod.GET)
    public Object payInfo(@RequestParam("orderId") Integer orderId) {
        OrderView orderView = ordersService.getDefaultChildOrderView(orderId);
        List<OrderView> childs = ordersService.getChildOrderViews(orderId);
        if (null == orderView) {
            if (childs.size() > 0) {
                orderView = childs.get(0);
            } else {
                orderView = ordersService.getOrderDetail(orderId);
            }
        }
        Iterator it = childs.iterator();
        while (it.hasNext()) {
            OrderView order = (OrderView) it.next();
            if (order.getOrderId().equals(orderView.getOrderId())) {
                it.remove();
            }
        }
        Pay pay = ordersService.getPayInfoByOrderId(orderView != null ? orderView.getOrderId() : orderId);
        CommonAccount commonAccount;
        AccountResult account = accountService.getAccount(AccountEnum.FetchAccountTypeEnum.FETCH_BY_CUSTOMER.getValue(), String.valueOf(orderView.getBuyerId()));
        commonAccount = (CommonAccount) account.getData();
        Map<String, Object> result = new HashMap<>();
        result.put("mainOrder", orderView);
        result.put("pay", pay);
        result.put("childs", childs);
        result.put("customer", commonAccount);
        result.put("chargeNum", orderView.getChargeNum());
        return WebResult.getSuccessResult("data", result);
    }

    @ApiOperation(value = "获取支付信息，用于打印", notes = "获取支付信息，用于打印", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "orderId", value = "订单ID", required = true, dataType = "integer", paramType = "query"),
    })
    @RequestMapping(value = "/printInfo.json", method = RequestMethod.GET)
    public WebResult printInfo(@RequestParam("orderId") Integer orderId) {
        Orders orders = ordersService.getSimpleOrdersById(orderId);
        OrderView parentOrder = ordersService.getOrderViewByCode(orders.getParentCode());
        List<OrderView> incrementChildOrderView = ordersService.getIncrementChildOrderView(parentOrder.getOrderId());

        OrderView mainOrder = ordersService.getDefaultChildOrderView(parentOrder.getOrderId());
        Pay pay;
        int rent = 0;

        if (parentOrder.getParentId() == null) {
            if (mainOrder == null) {
                mainOrder = incrementChildOrderView.get(0);
                rent = 1;
            }
        } else if (parentOrder.getParentId() == 0) {
            mainOrder = parentOrder;
        }
        pay = ordersService.getPayInfoByOrderId(mainOrder.getOrderId());

        Map<String, Object> resultMap = new HashMap<>();
        Map<String, String> buyer = new HashMap<>();
        resultMap.put("extSweepCodeWay", parentOrder.getExtSweepCodeWay());
        resultMap.put("code", parentOrder.getCode());
        resultMap.put("payType", pay.getPayType());
        if(mainOrder.getBuyerId() < 0) {
            resultMap.put("payType", parentOrder.getPayTypeId());
        }
        resultMap.put("payTime", DateUtil.dateToDateString(pay.getTime()));

        if (ActivityBeanUtils.OnlinePayNameEnum.containPayTypeId(pay.getPayType())) {
            String buyerId = String.valueOf(mainOrder.getBuyerId());
            if (mainOrder.getCardType() == 1) {
                TeamCredentials teamCredentials = teamCredentialsService.selectByPrimaryKey(orders.getTeamCredentialsId());
                buyerId = String.valueOf(teamCredentials.getCashierId());
            }
            AccountResult account = accountService.getAccount(AccountEnum.FetchAccountTypeEnum.FETCH_BY_CUSTOMER.getValue(), buyerId);
            CommonAccount commonAccount = (CommonAccount) account.getData();
            buyer.put("buyerName", commonAccount.getNickname());
            buyer.put("buyerPhone", commonAccount.getPhone());
            resultMap.put("buyer", buyer);
            List<Player> players;
            if (rent == 1) {
                players = ordersService.getOrderPlayers(parentOrder.getOrderId());
            } else {
                players = ordersService.getOrderPlayers(mainOrder.getOrderId());
            }
            if (players.size() > 0 && null != players.get(0).getAppointmentTime()) {
                resultMap.put("otherAppointmentTime", DateUtil.getZhCNDate(players.get(0).getAppointmentTime()));
            }
        } else {
            String buyerId = String.valueOf(mainOrder.getBuyerId());
            if (mainOrder.getCardType() == 1) {
                TeamCredentials teamCredentials = teamCredentialsService.selectByPrimaryKey(orders.getTeamCredentialsId());
                buyerId = String.valueOf(teamCredentials.getCashierId());
            }

            AccountResult account = accountService.getAccount(AccountEnum.FetchAccountTypeEnum.FETCH_BY_CUSTOMER.getValue(), buyerId);
            CommonAccount commonAccount = (CommonAccount) account.getData();
            Map<String, String> cashier = new HashMap<>();
            cashier.put("cashierName", commonAccount.getRealname());
            cashier.put("cashierPhone", commonAccount.getPhone());
            resultMap.put("cashier", cashier);
            List players = playerService.getPlayersByOrder(parentOrder.getOrderId());
            if (null == players || players.size() == 0) {
                players = playerService.getPlayersByOrder(mainOrder.getOrderId());
                if (null != players && players.size() > 0) {
                    Player player = (Player) players.get(0);
                    if (mainOrder.getTypeId() == 5) {
                        CampPlayerInfo campPlayerInfo = playerService.getCampPlayerInfoByPlayerId(player.getPlayerId());
                        buyer.put("buyerName", campPlayerInfo.getName());
                        buyer.put("buyerPhone", campPlayerInfo.getPhone());
                        resultMap.put("buyer", buyer);
                    } else {
                        buyer.put("buyerName", player.getRealName());
                        buyer.put("buyerPhone", player.getPhone());
                        resultMap.put("buyer", buyer);
                    }

                }
            } else {
                Player player = (Player) players.get(0);
                if (mainOrder.getTypeId() == 5) {
                    CampPlayerInfo campPlayerInfo = playerService.getCampPlayerInfoByPlayerId(player.getPlayerId());
                    buyer.put("buyerName", campPlayerInfo.getName());
                    buyer.put("buyerPhone", campPlayerInfo.getPhone());
                    resultMap.put("buyer", buyer);
                } else {
                    buyer.put("buyerName", player.getRealName());
                    buyer.put("buyerPhone", player.getPhone());
                    resultMap.put("buyer", buyer);
                }
            }
        }

        resultMap.put("createTime", DateUtil.dateToDateString(parentOrder.getCreateTime()));
        resultMap.put("activityName", mainOrder.getTitle());

        Map<String, Object> mainCharge = new HashMap<>();
        if (rent == 0) {
            mainCharge.put("name", mainOrder.getChargeName());
            mainCharge.put("count", mainOrder.getChargeNum());
            mainCharge.put("amount", mainOrder.getTotalPrice());
            resultMap.put("mainOrder", mainCharge);
        }
        List<Map> equipmentCharge = new LinkedList<>();
        List<Map> incrementCharge = new LinkedList<>();
        for (OrderView orderView : incrementChildOrderView) {
            Map<String, Object> tmp = new HashMap<>();
            Charge charge = chargeService.getChargeById(orderView.getChargeId());
            tmp.put("name", orderView.getChargeName());
            tmp.put("count", orderView.getChargeNum());
            tmp.put("amount", orderView.getTotalPrice());
            if (charge.getEquipmentId() != null) {
                equipmentCharge.add(tmp);
            } else {
                incrementCharge.add(tmp);
            }
        }
        resultMap.put("equipmentOrder", equipmentCharge);
        resultMap.put("incrementOrder", incrementCharge);

        if (null != parentOrder.getDeposit()) {
            resultMap.put("deposit", parentOrder.getDeposit());
            resultMap.put("amount", parentOrder.getTotalPrice());
            resultMap.put("exceptDeposit", parentOrder.getTotalPrice().subtract(parentOrder.getDeposit()));
        } else {
            resultMap.put("amount", parentOrder.getTotalPrice());
        }

        return WebResult.getSuccessResult("data", resultMap);
    }

    /**
     * 根据orderid 获取订单相关信息(线下支付用，目前很多信息用来判断储值消费相关)
     *
     * @return
     */
    @RequestMapping(value = "/offlineOrderInfo.json", method = RequestMethod.GET)
    public WebResult offlineOrderInfo(HttpSession session,
                                      @RequestParam(value = "orderId") Integer orderId, HttpServletRequest request) {
        Map<String, Object> resultMap = new HashMap<>();
        JSONArray incrementArray = new JSONArray();

        // 验证基本信息
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null) {
            return WebResult.getErrorResult(WebResult.Code.NO_PERMISSION);
        }
        BigDecimal rentAmount = new BigDecimal(0);
        BigDecimal optionalAmount = new BigDecimal(0);
        try {
            Orders order = ordersService.getSimpleOrdersById(orderId);
            List<Charge> chargeList = new ArrayList<>();
            List<Orders> childOrders = ordersService.getChildOrders(order.getOrderId());
            BigDecimal otaAmount = new BigDecimal(0);
            if (null == childOrders || childOrders.size() == 0) {
                if(order.getBuyerId() < 0) {
                    otaAmount = order.getTotalPrice();
                }
                Charge charge = chargeService.getChargeById(order.getChargeId());
                chargeList.add(charge);
                resultMap.put("chargeAmount", order.getTotalPrice());
                resultMap.put("chargeCount", order.getChargeNum());
                resultMap.put("chargeName", charge.getChargeName());
                resultMap.put("chargeMode", charge.getChargeMode());
            } else {
                for (Orders childOrder : childOrders) {
                    if(childOrder.getBuyerId() < 0) {
                        otaAmount = childOrder.getTotalPrice();
                    }
                    Charge multiCharge = chargeService.getChargeById(childOrder.getChargeId());
                    chargeList.add(multiCharge);
                    if (multiCharge.getIncrement() == 0) {
                        resultMap.put("chargeAmount", childOrder.getTotalPrice());
                        resultMap.put("chargeCount", childOrder.getChargeNum());
                        resultMap.put("chargeName", multiCharge.getChargeName());
                        continue;
                    }
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("count", childOrder.getChargeNum());
                    jsonObject.put("name", multiCharge.getChargeName());
                    incrementArray.add(jsonObject);
                    if (multiCharge.getEquipmentId() == null) {
                        optionalAmount = optionalAmount.add(childOrder.getTotalPrice());
                    } else {
                        rentAmount = rentAmount.add(childOrder.getTotalPrice());
                    }
                }
            }
            resultMap.put("increment", incrementArray);
            resultMap.put("deposit", order.getDeposit());
            resultMap.put("amount", order.getTotalPrice().subtract(otaAmount));
            resultMap.put("rentAmount", rentAmount);
            resultMap.put("optionalAmount", optionalAmount);
            Episode episode = episodeService.getEpisodeDetailById(chargeList.get(0).getEpisodeId());
            Activity activity = activityService.getActivityById(episode.getActivityId());

            resultMap.put("chargeName", null == resultMap.get("chargeName") ? activity.getTitle() : activity.getTitle() + "-" + resultMap.get("chargeName"));

            /* 判断当前产品是否能用储值金额 支付 */
            int isAllowUseStorage = 0;
            if (organizer.getType() != null) {
                String typeStr = activityService.getOrganizerActivityTypeAllow(organizer.getType()); // 获取二进制
                // 主办方开放储值功能 并且 产品可以用储值金额购买 (并且目前，押金不为0的订单，也不让用用储值支付)
                if (typeStr.substring(9, 10).equals("1") && activity.getAllowUseStorage() == 1 && (order.getDeposit() == null || order.getDeposit().doubleValue() == 0)) {
                    isAllowUseStorage = 1;
                }
            }
            resultMap.put("isAllowUseStorage", isAllowUseStorage);


            resultMap.put("balanceAmount", 0);
            if (isAllowUseStorage == 1) {
                /* 获取player表的phone字段的 common_account 的用户信息 */
                List<Player> players = playerService.getPlayersByOrder((null == childOrders || childOrders.size() == 0) ? order.getOrderId() : childOrders.get(0).getOrderId());
                if (players.size() <= 0) {
                    return WebResult.getErrorResult(WebResult.Code.CASHIER_INVALID_REGISTRATION_FORM);
                }
                String custoemrPhone = players.get(0).getPhone(); // 储值会员的手机号

                if (activity.getTypeId() == 5) {
                    custoemrPhone = playerService.getCampPlayerInfoByPlayerId(players.get(0).getPlayerId()).getPhone();
                }

                AccountResult accountResult = accountService.getAccount(AccountEnum.FetchAccountTypeEnum.FETCH_BY_PHONE.getValue(), custoemrPhone);
                CommonAccount commonAccount = null;
                if (accountResult != null && accountResult.getData() != null) {
                    commonAccount = (CommonAccount) accountResult.getData();
                } else {
                    return WebResult.getSuccessResult("data", resultMap);
                }
                /* 获取 储值余额 */
//                BigDecimal balanceAmount = storageService.getStorageAmountByCustomerIdAndOrganizerId(commonAccount.getCustomerId(), organizer.getOrganizerId());
                // 更换获取储值余额方式
                CommonAccountMoney balance = storageService.getBalance(organizer.getOrganizerId(), commonAccount.getCustomerId());
                resultMap.put("balanceAmount", balance == null ? 0 : balance.getBalance());
            }

            return WebResult.getSuccessResult("data", resultMap);

        } catch (Exception e) {
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }
    }

    /**
     * 查询支付结果
     *
     * @param orderId
     * @param request
     * @return
     */
    @RequestMapping(value = "/sweepPayResult", method = RequestMethod.GET)
    public WebResult sweepPayResult(Integer orderId, HttpServletRequest request) {
        CommonAccount commonAccount = WebUpmsContext.getAccount(request);
        return SweepPayUtils.getResult(commonAccount.getOrganizerId(), orderId, commonAccount.getCustomerId());
    }

    @ApiOperation(value = "订单备注", notes = "订单备注", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "orderId", value = "订单ID", required = true, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "remark", value = "订单备注", required = true, dataType = "String", paramType = "query")
    })
    @RequestMapping(value = "/remark", method = RequestMethod.GET)
    public Object remarks(@RequestParam("orderId") Integer orderId, @RequestParam("remark") String remark, HttpServletRequest request) {
        if(StringUtils.isEmpty(remark)) return WebResult.getErrorResult("备注不能为空！");
        Orders ordersSimple = ordersService.getSimpleOrdersById(orderId);
        if (ordersSimple == null) {
            return WebResult.getErrorResult(WebResult.Code.ORDER_ID_IS_ERRO);
        }

        CommonAccount commonAccount = WebUpmsContext.getAccount(request);

        Orders orders = new Orders();
        orders.setOrderId(orderId);
        orders.setMarkNote(remark);

        orders.setMarkNoteUserId(commonAccount.getCustomerId().longValue());
        ordersService.updateOrders(orders);

        return WebResult.getSuccessResult();
    }

    @ApiOperation(value = "查询订单可退款金额", notes = "查询订单可退款金额", httpMethod = "GET")
    @RequestMapping(value = "/queryOrderCanRefundMoney", method = {RequestMethod.GET})
    public Object queryOrderCanRefundMoney(@RequestParam("orderId") Integer orderId) {
        Orders ordersSimple = ordersService.getSimpleOrdersById(orderId);

        BigDecimal deposit = ordersSimple.getDeposit();
        if (deposit == null) {
            deposit = BigDecimal.ZERO;
        }

        BigDecimal money = ordersSimple.getTotalPrice().subtract(deposit);

        List<Orders> childOrders = ordersService.getChildOrders(orderId);
        if (childOrders != null) {
            for (Orders childOrder : childOrders) {
                if (childOrder.getBuyerId() != null && childOrder.getBuyerId().intValue() < 0) {
                    return WebResult.getSuccessResult("money", money.subtract(childOrder.getTotalPrice()).setScale(2, RoundingMode.HALF_DOWN));
                }
            }
        }

        return WebResult.getSuccessResult("money", money.setScale(2, RoundingMode.HALF_DOWN));
    }

    @ApiOperation(value = "异常备注", notes = "添加订单异常备注", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "orderId", value = "订单ID", required = true, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "remark", value = "订单异常备注", required = true, dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "extAbnormalRefundMoney", value = "异常退款金额", required = true, dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "extAbnormalType", value = "异常类型", required = true, dataType = "String", paramType = "query"),
    })
    @RequestMapping(value = "/abnormalRemarks", method = {RequestMethod.POST, RequestMethod.GET})
    public Object abnormalRemarks(String refundInfo, String multi,
                                  @RequestParam("orderId") Integer orderId, @RequestParam("remark") String remark,
                                  @RequestParam(value = "extAbnormalRefundMoney", required = false, defaultValue = "0") BigDecimal extAbnormalRefundMoney,
                                  @RequestParam("extAbnormalType") String extAbnormalType,
                                  HttpServletRequest request) {
        CommonAccount commonAccount = WebUpmsContext.getAccount(request);
        Organizer orgnization = WebUpmsContext.getOrgnization(request);

        String opUserId = Config.instance().getOpUserId();

        OrganizerWithScenic organizerWithScenic = organizerService.getAllInfoById(orgnization.getOrganizerId());
        Orders ordersSimple = ordersService.getSimpleOrdersById(orderId);
        if (ordersSimple == null) {
            return WebResult.getErrorResult(WebResult.Code.ORDER_ID_IS_ERRO);
        }

        if (extAbnormalRefundMoney.compareTo(BigDecimal.ZERO) <= 0) {
            return WebResult.getErrorResult(WebResult.Code.ORDER_ABNORMAL_REFUND_LARGER_ZERO);
        }
        if (ordersSimple.getTotalPrice().compareTo(extAbnormalRefundMoney) < 0) {
            return WebResult.getErrorResult(WebResult.Code.ORDER_REFUND_MONEY_ERRO);
        }

        if(null != ordersSimple.getExtAbnormalRefundStatus()){
            return WebResult.getErrorMsgResult(WebResult.Code.ERROR, "该订单已申请退款");
        }

        if (StringUtils.isNotEmpty(refundInfo)) {
            extAbnormalRefundMoney = BigDecimal.ZERO;
            List<Map> refundJson = JSON.parseArray(refundInfo, Map.class);
            for (Map refundMap : refundJson) {
                extAbnormalRefundMoney = extAbnormalRefundMoney.add(new BigDecimal(refundMap.get("refundPrice").toString()));
            }
        }

        BigDecimal otamoney = BigDecimal.ZERO;
        List<Orders> childOrders = ordersService.getChildOrders(orderId);
        if (childOrders != null) {
            for (Orders childOrder : childOrders) {
                if (childOrder.getBuyerId() != null && childOrder.getBuyerId().intValue() < 0) {
                    otamoney = childOrder.getTotalPrice();
                    break;
                }
            }
        }

        BigDecimal totalPrice = ordersSimple.getTotalPrice().subtract(otamoney);

        Orders orders = new Orders();
        orders.setExtAbnormalRefundStatus(1);

        Object result = null;

        if (extAbnormalRefundMoney.doubleValue() > totalPrice.subtract(ordersSimple.getDeposit() == null ? BigDecimal.ZERO : ordersSimple.getDeposit()).doubleValue()) {
            return WebResult.getErrorMsgResult(WebResult.Code.ERROR, "异常退款金额不能大于可退款金额（不包含押金的总金额）");
        }

        // 判断是否存已租赁待归还的租赁物，如果存在不允许进行异常退款操作
        List<RentBalance> rentBalances = depositService.getRentBalanceByOrderCode(orgnization.getOrganizerId(), ordersSimple.getCode());
        if (rentBalances != null) {
            for (RentBalance rentBalance : rentBalances) {
                //0待租赁 1待归还 2已归还 3暂存 4超时 5损坏
                if (rentBalance.getDetailStatus() == 4) {
                    return WebResult.getErrorResult(WebResult.Code.ORDER_REFUND_HAVE_4);
                } else if (rentBalance.getDetailStatus() == 5) {
                    return WebResult.getErrorResult(WebResult.Code.ORDER_REFUND_HAVE_5);
                } else if (rentBalance.getDetailStatus() == 3) {
                    return WebResult.getErrorResult(WebResult.Code.ORDER_REFUND_HAVE_3);
                } else if (rentBalance.getDetailStatus() == 1) {
                    return WebResult.getErrorResult(WebResult.Code.ORDER_REFUND_HAVE_RETURN);
                }
            }
        }

        if (ordersSimple.getBuyerId() <= 0) {
            // 执行异常退款 （不做任何处理），只购买了一张票，没有租赁物的订单

        } else if (ordersSimple.getPayTypeId().intValue() == 1 && ordersSimple.getStatus().intValue() == 1 && (extAbnormalRefundMoney != null && extAbnormalRefundMoney.doubleValue() > 0)
                && ordersSimple.getBuyerId() > 0) {

            OrganizerPayConfigExample payConfigExample = new OrganizerPayConfigExample();
            payConfigExample.createCriteria().andOrganizerIdEqualTo(commonAccount.getOrganizerId());
            OrganizerPayConfig organizerPayConfig = organizerPayConfigService.selectFirstByExample(payConfigExample);

            if (organizerPayConfig != null && StringUtils.isNotEmpty(organizerPayConfig.getSubmchid())) {
                if (organizerPayConfig.getSubmichidDataTime() != null && ordersSimple.getPayTime().getTime() < organizerPayConfig.getSubmichidDataTime().getTime()) {
                    orders.setExtAbnormalRefundStatus(3);
                } else {
                    if (extAbnormalRefundMoney.doubleValue() > 0) {
                        //申请退款流程
                        try {
                            result = applyRefund(refundInfo, multi, request);

                            if (result != null) {
                                WebResult webResult = (WebResult) result;
                                if (WebResult.Code.SUCCESS.code != webResult.getCode()) {
                                    return webResult;
                                }
                            } else {
                                return WebResult.getErrorResult(WebResult.Code.ERROR);
                            }

//                            if (extAbnormalRefundMoney.doubleValue() > 0) {
//                                //创建退款申请
//                                orders.setExtAbnormalRefundStatus(1);
//                            } else {
//                                orders.setExtAbnormalRefundStatus(3);
//                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        orders.setExtAbnormalRefundStatus(3);
                    }
                }
            } else {
                orders.setExtAbnormalRefundStatus(3);
            }
        } else if (extAbnormalRefundMoney != null && extAbnormalRefundMoney.doubleValue() > 0) {
            //扫码支付(异常退款)
            if (ordersSimple.getPayTypeId().intValue() == 4 || ordersSimple.getPayTypeId().intValue() == 13
            || ordersSimple.getPayTypeId().intValue() ==2) {
            /*BigDecimal refundAmount = depositService.getRefundAmount(ordersSimple.getCode());
            if (refundAmount == null) {
                refundAmount = BigDecimal.ZERO;
            }*/

            /*BigDecimal indemnityMoney = indemnityService.selectIndemnityMoney(ordersSimple.getCode());
            if(indemnityMoney == null){
                indemnityMoney = BigDecimal.ZERO;
            }*/

                //判断所有租赁物是否归还
            /*List<RentBalance> rentBalances = depositService.getRentBalanceByOrderCode(orgnization.getOrganizerId(), ordersSimple.getCode());
            if(rentBalances != null){
                for (RentBalance rentBalance : rentBalances) {
                    //0待租赁 1待归还 2已归还 3暂存 4超时 5损坏
                    if(rentBalance.getDetailStatus() != 2){
                        return WebResult.getErrorResult(WebResult.Code.ORDER_REFUND_HAVE_RETURN);
                    }
                }
            }*/

                //BigDecimal subtract = totalPrice.subtract(indemnityMoney).subtract(refundAmount);
            /*BigDecimal subtract = totalPrice.subtract(refundAmount);
            if (subtract.doubleValue() < 0) {
                return WebResult.getErrorResult(WebResult.Code.ORDER_REFUND_MONEY_ANY_MORE);
            }
            if (subtract.subtract(extAbnormalRefundMoney).doubleValue() < 0) {
                return WebResult.getErrorResult(WebResult.Code.ORDER_REFUND_MONEY_ANY_MORE);
            }

            Refund abnormalRefund = ordersService.getRefundByOrderCode(ordersSimple.getCode());
            if (abnormalRefund == null) {
                //创建退款(退款失败可重复退款，不创建重复记录)
                abnormalRefund = ordersService.createAbnormalRefund(commonAccount, extAbnormalRefundMoney, ordersSimple, 1);
            }

            if (extAbnormalRefundMoney.doubleValue() == 0) {
                abnormalRefund.setStatus(1);
                ordersService.updateRefund(abnormalRefund);
            } else {
                OrderSweepCodePay orderSweepCodePaySuccess = orderSweepCodePayService.findOrderSweepCodePaySuccess(ordersSimple.getCode());

                String unipayOpShopId = organizerPayConfigService.getUnipayOpShopId(orgnization.getOrganizerId());
                Map<String, String> respRefund = UnionPayApi.refund(orderSweepCodePaySuccess.getPayCode(), abnormalRefund.getOutRefundNo(), (int) (totalPrice.doubleValue() * 100), (int) (extAbnormalRefundMoney.doubleValue() * 100), unipayOpShopId);

                if (UnionPayApi.resultCode(respRefund)) {
                    abnormalRefund.setStatus(1);
                    ordersService.updateRefund(abnormalRefund);
                } else {
                    abnormalRefund.setStatus(2);
                    ordersService.updateRefund(abnormalRefund);
                    return WebResult.getErrorResult(WebResult.Code.ORDER_REFUND_FAIL);
                }
            }*/

                if (extAbnormalRefundMoney.doubleValue() > 0) {
                    Refund abnormalRefund = ordersService.createAbnormalRefund(commonAccount, extAbnormalRefundMoney, ordersSimple, 1, remark);
                    //创建退款申请


                    if(organizerWithScenic.getRefundConfirm() == 1) {
                        ordersService.taskRefund(opUserId, commonAccount, ordersSimple.getCode(), orgnization.getOrganizerId());
                    }
                } else {
                    orders.setExtAbnormalRefundStatus(3);
                }

            } else if (ordersSimple.getPayTypeId().intValue() == 1 && (ordersSimple.getStatus() == 5 || ordersSimple.getStatus() == 1)) {

                // 线上交易通过配置微信分账子商户的机构，异常退款需要走退款申请流程，并生成异常退款记录

                OrganizerPayConfigExample payConfigExample = new OrganizerPayConfigExample();
                payConfigExample.createCriteria().andOrganizerIdEqualTo(commonAccount.getOrganizerId());
                OrganizerPayConfig organizerPayConfig = organizerPayConfigService.selectFirstByExample(payConfigExample);

                if (organizerPayConfig != null && StringUtils.isNotEmpty(organizerPayConfig.getSubmchid())) {
                    if (organizerPayConfig.getSubmichidDataTime() != null && ordersSimple.getPayTime().getTime() < organizerPayConfig.getSubmichidDataTime().getTime()) {
                        orders.setExtAbnormalRefundStatus(3);
                    } else {
                        if (extAbnormalRefundMoney.doubleValue() > 0) {
                            ProfitSharingPlatform profitSharingPlatform = platformService.selectByPayTypeId(ordersSimple.getPayTypeId());
                            if (profitSharingPlatform != null && profitSharingPlatform.getValid() == 1
                                    && ordersSimple.getProfitSharingStatus() == 2) {
                                Map<String, String> params = new HashMap<>();
                                params.put("code", ordersSimple.getParentCode());
                                params.put("abnormalRefundMoney", extAbnormalRefundMoney.toString());
                                if (!ProfitSharingUtil.profitSharingReturn(params)) {
                                    return WebResult.getErrorResult(WebResult.Code.PROFIT_SHARING_RETURN_ERROR);
                                }
                            }

                            Refund abnormalRefund = ordersService.createAbnormalRefund(commonAccount, extAbnormalRefundMoney, ordersSimple, 1, remark);

                            if(organizerWithScenic.getRefundConfirm() == 1) {
                                ordersService.taskRefund(opUserId, commonAccount, ordersSimple.getCode(), orgnization.getOrganizerId());
                            }
                        } else {
                            orders.setExtAbnormalRefundStatus(3);
                        }
                    }
                }

            }
        }

        Date now = new Date();
        orders.setOrderId(orderId);
        orders.setRemark(remark);
        orders.setExtAbnormalRefundMoney(extAbnormalRefundMoney);
        orders.setExtAbnormalType(extAbnormalType);
        orders.setExtAbnormalCashierId(commonAccount.getCustomerId());
        orders.setExtAbnormalZooOperatorLoginId(commonAccount.getZooOperatorLoginId());
        orders.setExtAbnormalDealTime(now);
        orders.setExtAbnormalRefundDateTime(now);

        if (ordersSimple.getStatus() != 5) {
            if (ordersSimple.getParentId() == null) {
                /*List<Orders> childOrders = ordersService.getChildOrders(orderId);*/
                /*List<Integer> childIds = new ArrayList<>();
                for (Orders childOrder : childOrders) {
                    childIds.add(childOrder.getOrderId());
                }*/
                //verifyService.batchVerify(Arrays.asList(ordersSimple.getOrderId()),new ArrayList<>(),commonAccount);
            } else {
                //verifyService.batchVerify(new ArrayList<>(),Arrays.asList(ordersSimple.getOrderId()),commonAccount);
            }
        }

        ordersService.updateOrders(orders);

        //财务系统备注
        ZooOnAccountOrdersRemark ordersRemark = new ZooOnAccountOrdersRemark();
        ordersRemark.setRemark(remark);
        ordersRemark.setExtAbnormalRefundMoney(extAbnormalRefundMoney);
        ordersRemark.setExtAbnormalType(extAbnormalType);
        ordersRemark.setExtAbnormalCashierId(commonAccount.getCustomerId());
        ordersRemark.setExtAbnormalZooOperatorLoginId(commonAccount.getZooOperatorLoginId());
        ordersRemark.setExtAbnormalDealTime(now);
        zooOnAccountOrdersRemarkService.insertSelective(ordersRemark);

        if (result != null) {
            return result;
        }

        return WebResult.getSuccessResult();
    }

    @ApiOperation(value = "订单列表", notes = "后台使用的订单列表功能(订单列表可以分页,默认按照创建时间排序)", httpMethod = "GET")
    @ApiImplicitParam(name = "teamCredentialsId", value = "团队单ID", required = true, dataType = "integer", paramType = "query")
    @GetMapping("/getTeamChildrenOrders.json")
    public WebResult getTeamChildrenOrders(@RequestParam("teamCredentialsId") Integer teamCredentialsId, @RequestParam(value = "code", required = false) String code, HttpServletRequest request) {
        Organizer orgnization = WebUpmsContext.getOrgnization(request);
        List map = ordersService.getTeamChildrenOrders(teamCredentialsId, orgnization.getOrganizerId(), code);
        WebResult result = WebResult.getSuccessResult();
        result.put("data", map);
        return result;
    }

    @ApiOperation(value = "团队订单列表", notes = "团队订单列表", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页", required = false, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "size", value = "条", required = false, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "productId", value = "产品ID", required = false, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "productName", value = "产品名称", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "code", value = "订单code", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "teamCode", value = "团队订单code", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "contactName", value = "联系人名称", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "contactPhone", value = "联系人手机号", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "companyName", value = "公司名称", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "payTypeId", value = "支付类型", required = false, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "startTime", value = "开始时间", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "endTime", value = "结束时间", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "customer", value = "下单销售", required = false, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "status", value = "订单状态", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "customerName", value = "用户名称", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "customerPhone", value = "用户手机号", required = false, dataType = "string", paramType = "query"),
    })
    @GetMapping("/getTeamOrders.json")
    public WebResult getTeamOrders(@RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
                                   @RequestParam(value = "size", required = false, defaultValue = "10") Integer size,
                                   @RequestParam(value = "productId", required = false) Integer productId,
                                   @RequestParam(value = "productName", required = false) String productName,
                                   @RequestParam(value = "code", required = false) String code,
                                   @RequestParam(value = "teamCode", required = false) String teamCode,
                                   @RequestParam(value = "contactName", required = false) String contactName,
                                   @RequestParam(value = "contactPhone", required = false) String contactPhone,
                                   @RequestParam(value = "companyName", required = false) String companyName,
                                   @RequestParam(value = "payTypeId", required = false) Integer payTypeId,
                                   @RequestParam(value = "startTime", required = false) String startTime,
                                   @RequestParam(value = "endTime", required = false) String endTime,
                                   @RequestParam(value = "customer", required = false) Integer customer,
                                   @RequestParam(value = "status", required = false) String status,
                                   @RequestParam(value = "customerName", required = false) String customerName,
                                   @RequestParam(value = "customerPhone", required = false) String customerPhone, HttpServletRequest request) {
        Organizer orgnization = WebUpmsContext.getOrgnization(request);
        if (StringUtils.isBlank(status) || status.equals("[]")) {
            status = null;
        } else {
            status = "(" + status + ")";
        }
        PageList pageList = ordersService.getTeamOrders(orgnization.getOrganizerId(), productId, productName, code, teamCode, contactName, contactPhone, companyName, payTypeId, startTime, endTime, customer, status, customerName, customerPhone, page, size);
        WebResult webResult = WebResult.getSuccessResult();
        webResult.put("data", pageList);
        return webResult;
    }

    private void resetParentOrderInfo(Orders parentOrder, Orders otaOrder) {
        Orders firstChildOrder = ordersService.getFirstChildOrder(parentOrder.getOrderId());
        if (firstChildOrder != null) {
            parentOrder.setActivityId(firstChildOrder.getActivityId());
            parentOrder.setEpisodeId(firstChildOrder.getEpisodeId());
            parentOrder.setChargeId(firstChildOrder.getChargeId());

            parentOrder.setActivityName(firstChildOrder.getActivityName());
            parentOrder.setEpisodeName(firstChildOrder.getEpisodeName());
            parentOrder.setChargeName(firstChildOrder.getChargeName());

            parentOrder.setTotalPrice(parentOrder.getTotalPrice().subtract(otaOrder.getTotalPrice()));
            parentOrder.setChargeNum(parentOrder.getChargeNum() - otaOrder.getChargeNum());

            ordersService.updateOrders(parentOrder);
        }
    }

    @ApiOperation(value = "出票情况", notes = "出票情况", httpMethod = "GET")
    @GetMapping("/getEquipmentInfo.json")
    public WebResult getEquipmentInfo(@RequestParam("teamId") Integer teamId) {
        List<Map> dataList = ordersService.getEquipmentInfo(teamId);
        WebResult webResult = WebResult.getSuccessResult();
        webResult.put("data", dataList);
        return webResult;
    }

    @ApiOperation(value = "关闭订单", notes = "关闭订单", httpMethod = "POST")
    @PostMapping("/closeOrder.json")
    public WebResult closeOrder(@RequestParam("orderId") Integer orderId){
        Orders order = new Orders();
        order.setOrderId(orderId);
        order.setStatus(4);
        ordersService.updateOrders(order);
        return WebResult.getSuccessResult();
    }

    @ApiOperation(value = "查询最新季卡订单", notes = "查询最新季卡订单", httpMethod = "GET")
    @GetMapping("/getLastLimitCardOrder.json")
    public WebResult getLastLimitCardOrder(HttpServletRequest request){
        Organizer orgnization = WebUpmsContext.getOrgnization(request);
        OrderView orderView = ordersService.getLastLimitCardOrder(orgnization.getOrganizerId());
        LimitCardMember limitCardMember = limitCardService.getLimitCardMemberByOrderId(orderView.getOrderId());

        if(limitCardMember != null){
            orderView = null;
        }

        return WebResult.getSuccessResult("data", orderView);
    }

}
