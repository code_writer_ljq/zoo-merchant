package com.zoo.zoomerchantadminweb.order.controller;

import com.zoo.common.page.PageList;
import com.zoo.activity.dao.model.Organizer;
import com.zoo.activity.service.OrganizerService;
import com.zoo.activity.service.UnRefundService;
import com.zoo.activity.util.DateUtil;
import com.zoo.web.bean.WebResult;
import com.zoo.zoomerchantadminweb.context.WebUpmsContext;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.util.*;

@Api(value = "api/server/unrefund", tags = {"主办方不可退款接口"}, description = "主办方不可退款接口描述")
@RestController
@RequestMapping("api/server/unrefund")
public class UnRefundController {
    @Autowired
    UnRefundService unRefundService;
    @Autowired
    OrganizerService organizerService;

    @ApiOperation(value = "不可退款记录", notes = "不可退款记录", httpMethod = "GET")
    @RequestMapping("/list.json")
    public WebResult unRefundListJson(@RequestParam(value = "activityId", required = false) Integer activityId,
                                    @RequestParam(value = "episodeId", required = false) Integer episodeId,
                                    @RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
                                    @RequestParam(value = "size", required = false, defaultValue = "10") Integer size,
                                    @RequestParam(value = "startTime", required = false) String startTime,
                                    @RequestParam(value = "typeId", required = true) Integer typeId,
                                    @RequestParam(value = "endTime", required = false) String endTime,
                                    @RequestParam(value = "code", required = false) String code,
                                    HttpServletRequest request) {

        Organizer organizer = WebUpmsContext.getOrgnization(request);

        Date startDate = null;
        Date endDate = null;
        try {
            if (startTime != null) {
                startDate = DateUtil.convertStringToDate(startTime + " 00:00:00", "yyyy-MM-dd HH:mm:ss");
            }
            if (endTime != null) {
                endDate = DateUtil.convertStringToDate(endTime + " 23:59:59", "yyyy-MM-dd HH:mm:ss");
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (StringUtils.isNotBlank(code)) {
            code = code.trim();
        }
        PageList unRefundList = unRefundService.getUnRefundList(activityId, episodeId, code, startDate, endDate, null, organizer.getOrganizerId(), page, size);
        Map map = new HashMap<>();
        map.put("unRefundList", unRefundList);

        return WebResult.getSuccessResult("data", map);
    }
}
