package com.zoo.zoomerchantadminweb.order.controller;

import com.zoo.activity.dao.model.Organizer;
import com.zoo.activity.service.OrganizerService;
import com.zoo.finance.dao.model.ZooBalanceDepositIndemnity;
import com.zoo.finance.dao.model.ZooBalanceDepositIndemnityExample;
import com.zoo.finance.service.ZooBalanceDepositIndemnityService;
import com.zoo.web.bean.WebResult;
import com.zoo.zoomerchantadminweb.context.WebUpmsContext;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.util.*;

@Api(value = "api/server/depositindemnity", tags = {"主办方赔偿接口"}, description = "主办方赔偿接口描述")
@RestController
@RequestMapping("api/server/depositindemnity")
public class DepositIndemnityController {
    @Autowired
    ZooBalanceDepositIndemnityService zooBalanceDepositIndemnityService;
    @Autowired
    OrganizerService organizerService;

    @ApiOperation(value = "赔偿记录", notes = "赔偿记录", httpMethod = "GET")
    @RequestMapping("/list.json")
    public WebResult unRefundListJson(@RequestParam(value = "page", required = false, defaultValue = "0") Integer page,
                                      @RequestParam(value = "size", required = false, defaultValue = "10") Integer size,
                                      @RequestParam(value = "startTime", required = false) String startTime,
                                      @RequestParam(value = "endTime", required = false) String endTime,
                                      @RequestParam(value = "code", required = false) String code,
                                      HttpServletRequest request) {

        Organizer organizer = WebUpmsContext.getOrgnization(request);

        ZooBalanceDepositIndemnityExample zooBalanceDepositIndemnityExample = new ZooBalanceDepositIndemnityExample();
        ZooBalanceDepositIndemnityExample.Criteria criteria = zooBalanceDepositIndemnityExample.or();
        ZooBalanceDepositIndemnityExample.Criteria criteria1 = zooBalanceDepositIndemnityExample.or();
        criteria.andOrganizerIdEqualTo(organizer.getOrganizerId());
        criteria1.andOrganizerIdEqualTo(organizer.getOrganizerId());
        zooBalanceDepositIndemnityExample.setOrderByClause("indemnity_time desc");
        try {
            if (StringUtils.isNotEmpty(startTime) && StringUtils.isNotEmpty(endTime)) {
                criteria.andIndemnityTimeBetween(com.zoo.activity.util.DateUtil.convertStringToDate(startTime + " 00:00:00", "yyyy-MM-dd HH:mm:ss"), com.zoo.activity.util.DateUtil.convertStringToDate(endTime + " 23:59:59", "yyyy-MM-dd HH:mm:ss"));
                criteria1.andIndemnityTimeBetween(com.zoo.activity.util.DateUtil.convertStringToDate(startTime + " 00:00:00", "yyyy-MM-dd HH:mm:ss"), com.zoo.activity.util.DateUtil.convertStringToDate(endTime + " 23:59:59", "yyyy-MM-dd HH:mm:ss"));
            } else if (StringUtils.isNotEmpty(startTime) && StringUtils.isEmpty(endTime)) {
                criteria.andIndemnityTimeGreaterThanOrEqualTo(com.zoo.activity.util.DateUtil.convertStringToDate(startTime + " 00:00:00", "yyyy-MM-dd HH:mm:ss"));
                criteria1.andIndemnityTimeGreaterThanOrEqualTo(com.zoo.activity.util.DateUtil.convertStringToDate(startTime + " 00:00:00", "yyyy-MM-dd HH:mm:ss"));
            } else if (StringUtils.isNotEmpty(endTime) && StringUtils.isEmpty(startTime)) {
                criteria.andIndemnityTimeLessThanOrEqualTo(com.zoo.activity.util.DateUtil.convertStringToDate(endTime + " 00:00:00", "yyyy-MM-dd HH:mm:ss"));
                criteria1.andIndemnityTimeLessThanOrEqualTo(com.zoo.activity.util.DateUtil.convertStringToDate(endTime + " 00:00:00", "yyyy-MM-dd HH:mm:ss"));
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if(StringUtils.isNotEmpty(code)){
            criteria.andOrderCodeEqualTo(code);
            criteria1.andOrderParentCodeEqualTo(code);
        }
        page=(page-1)*10;
        List<ZooBalanceDepositIndemnity> rows = zooBalanceDepositIndemnityService.selectByExampleForOffsetPage(zooBalanceDepositIndemnityExample, page, size);
        long total = zooBalanceDepositIndemnityService.countByExample(zooBalanceDepositIndemnityExample);
        Map<String, Object> result = new HashMap<>(2);
        result.put("rows", rows);
        result.put("total", total);
        return WebResult.getSuccessResult("data", result);
    }
}
