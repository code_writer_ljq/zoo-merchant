package com.zoo.zoomerchantadminweb.order.controller;

import com.zoo.common.page.PageList;
import com.zoo.activity.dao.model.*;
import com.zoo.activity.dao.nutz.pojo.CommonAccount;
import com.zoo.activity.service.*;
import com.zoo.web.bean.WebResult;
import com.zoo.wechat.common.Config;
import com.zoo.zoomerchantadminweb.context.WebUpmsContext;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.*;

@Api(value = "api/server/refund", tags = {"主办方退款相关接口"}, description = "主办方退款相关接口描述")
@RestController
@RequestMapping("api/server/refund")
public class RefundController {

    @Resource(name = "orderService")
    private OrdersService ordersService;


    @ApiOperation(value = "退款订单列表", notes = "后台使用的退款订单列表功能", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页", required = false, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "size", value = "条", required = false, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "wxStatus", value = "状态", required = false, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "phone", value = "手机号", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "code", value = "订单号", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "startTime", value = "开始时间", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "endTime", value = "结束时间", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "activityTypeId", value = "产品类型", required = false, dataType = "string", paramType = "query"),
    })
    @RequestMapping(value = "/list.json", method = RequestMethod.GET)
    public WebResult getOrderRefundList(
            @RequestParam(value = "activityId", required = false) Integer activityId,
            @RequestParam(value = "episodeId", required = false) Integer episodeId,
            @RequestParam(value = "wxStatus", required = false) Integer status,
            @RequestParam(value = "code", required = false) String code,
            @RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
            @RequestParam(value = "size", required = false, defaultValue = "10") Integer size,
            @RequestParam(value = "startTime", required = false) String startTime,
            @RequestParam(value = "endTime", required = false) String endTime,
            @RequestParam(value = "activityTypeId", required = false) Integer activityTypeId, HttpServletRequest request) {

        Integer sellerId = WebUpmsContext.getOrgnization(request).getOrganizerId();
        PageList refundOrders = ordersService.getRefundOrders(sellerId, activityId, episodeId, status, code, null, null, startTime, endTime, activityTypeId, page, size);

        List<RefundResult> dataList = (List<RefundResult>) refundOrders.getDataList();
        List<Map<String, Object>> orderMapList = new ArrayList<>();

        for (RefundResult orderData : dataList) {
            Map<String, Object> orderMap = new HashMap<>();
//            Integer orderId = orderData.getOrderId();
            //获取指定教练预约的订单,用于前台的具体显示
//            if (orderData.getChargeMode() != null) {
//                if (orderId != null && orderData.getChargeMode() != 3 && orderData.getActivityMode() !=
//                        null && orderData.getActivityMode() != 1) {
//                    OrderCoachExtendList orderCoachExtendList = coachService.findOrderCoachExtendByOrderId(orderId);
//                    orderMap.put("coachOrder", orderCoachExtendList);
//                } else {
//                    orderMap.put("coachOrder", null);
//                }
//            } else {
//                if (orderId != null && orderData.getActivityMode() !=
//                        null && orderData.getActivityMode() != 1) {
//                    OrderCoachExtendList orderCoachExtendList = coachService.findOrderCoachExtendByOrderId(orderId);
//                    orderMap.put("coachOrder", orderCoachExtendList);
//                } else {
//                    orderMap.put("coachOrder", null);
//                }
//            }

            if (null != orderData.getParentId() && orderData.getParentId() != 0) {
                continue;
            }
            orderMap.put("mainOrder", orderData);
            orderMapList.add(orderMap);
        }

        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("count", refundOrders.getCount());
        resultMap.put("order", orderMapList);

        return WebResult.getSuccessResult("data", resultMap);
    }

    @ApiOperation(value = "退款处理", notes = "退款处理", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "code", value = "订单号", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "refundId", value = "退款ID", required = false, dataType = "integer", paramType = "query")

    })
    @RequestMapping(value = "/deal.json", method = RequestMethod.POST)
    @RequiresPermissions(value = {"sys_manager:order:read:refund:deal"}, logical = Logical.OR)
    public WebResult dealRefundSendToWx(HttpServletRequest request, @RequestParam(value = "code", required = false) String code,
                                        @RequestParam(value = "refundId", required = false) Integer refundId) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        CommonAccount account = WebUpmsContext.getAccount(request);
        String opUserId = Config.instance().getOpUserId();
        Orders orderData = ordersService.getOrderDetailByCode(code);
        if((new Date().getTime() - orderData.getCreateTime().getTime()) / 1000 / 3600 / 24 >= 300 && orderData.getPayTypeId() == 1){
            return WebResult.getErrorResult("该笔订单已超过在线支付原路退款有效期（300天），如需退款请采用其它方式");
        }
        Refund refundDate = ordersService.getRefundByRefundId(refundId);

        if (!orderData.getSellerId().equals(organizer.getOrganizerId())) {
            return WebResult.getErrorResult(WebResult.Code.NO_PERMISSION);
        }

        // Refund refund = ordersService.dealRefundByCode(orderData, refundDate, opUserId, account);

        WebResult result = ordersService.dealRefundByCodeNew(orderData, refundDate, opUserId, account);
        return result;

        /*if (refund != null) {
            return WebResult.getSuccessResult("data", refund);
        } else {
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }*/

    }
}
