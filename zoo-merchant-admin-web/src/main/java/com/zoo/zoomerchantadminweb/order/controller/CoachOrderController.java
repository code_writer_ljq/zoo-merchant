package com.zoo.zoomerchantadminweb.order.controller;

import com.alibaba.fastjson.JSON;
import com.zoo.common.page.PageList;
import com.zoo.activity.dao.model.*;
import com.zoo.activity.service.*;
import com.zoo.web.bean.WebResult;
import com.zoo.zoomerchantadminweb.base.BaseController;
import com.zoo.zoomerchantadminweb.context.WebUpmsContext;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.util.*;

/**
 * Created by ljq on 2018/8/27.
 */
@Api(value = "/api/server/coach/order", tags = "【教练系统】教练订单相关接口", description = "教练订单、核销订单")
@RestController
@RequestMapping("/api/server/coach/order")
public class CoachOrderController extends BaseController {

    @Autowired
    private OrdersService ordersService;

    @Autowired
    private CoachService coachService;

    @Autowired
    private CourseService courseService;

    @Autowired
    private EpisodeService episodeService;

    @Autowired
    private EnrollInfoService enrollInfoService;

    @Autowired
    private ChargeService chargeService;

    @Autowired
    private OrganizerService organizerService;

    @Autowired
    private VerifyService verifyService;

    @Autowired
    private OrderCoachExtendService orderCoachExtendService;

    @Autowired
    private CoachGuideRelationshipsService coachGuideRelationshipsService;

    @Autowired
    private UnRefundService unRefundService;

    /**
     * 出导订单查询
     * @param page
     * @param size
     * @param activityId
     * @param episodeId
     * @param status
     * @param coachId
     * @param phone
     * @param code
     * @param startTime
     * @param sortType
     * @param sortProp
     * @param endTime
     * @param session
     * @return
     */
    @ApiOperation(value = "出导订单列表查询接口", notes = "根据筛选条件查询教练订单", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码(第几页)", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "size", value = "每页数量", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "activityId", value = "活动ID(activityId)", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "episodeId", value = "场次ID(episodeId)", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "status", value = "订单状态", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "coachId", value = "教练ID", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "phone", value = "用户手机号", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "code", value = "订单号", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "startTime", value = "开始时间(订单创建时间)", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "endTime", value = "结束时间(订单创建时间)", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "sortType", value = "列表排序规则（默认按降序排序：desc）", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "sortProp", value = "根据筛选条件进行排序（默认按照订单创建时间排序）", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "isHaveRemark", value = "是否异常退款订单", required = false, dataType = "string", paramType = "query"),
    })
    @RequestMapping(value = "/list.json", method = RequestMethod.GET)
    public Object getOrderList(@RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
                               @RequestParam(value = "size", required = false, defaultValue = "10") Integer size,
                               @RequestParam(value = "activityId", required = false) Integer activityId,
                               @RequestParam(value = "episodeId", required = false) Integer episodeId,
                               @RequestParam(value = "status", required = false) String status,
                               @RequestParam(value = "coachId", required = false) Integer coachId,
                               @RequestParam(value = "phone", required = false) String phone,
                               @RequestParam(value = "code", required = false) String code,
                               @RequestParam(value = "startTime", required = false) String startTime,
                               @RequestParam(value = "endTime", required = false) String endTime,
                               @RequestParam(value = "sortType", required = false, defaultValue = "desc") String sortType,
                               @RequestParam(value = "sortProp", required = false, defaultValue = "create_time") String sortProp,
                               @RequestParam(value = "isHaveRemark", required = false) String isHaveRemark,
                               @RequestParam(value = "abnormalRefundStartTime", required = false) String abnormalRefundStartTime,
                               @RequestParam(value = "abnormalRefundEndTime", required = false) String abnormalRefundEndTime,
                               HttpServletRequest request, HttpSession session) {
        //使用sql进行分页，计算limit参数
        /*Integer start = (page - 1) * size;
        Integer end = size;*/
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null) {
            //无法从session中获取organizer信息
            return WebResult.getErrorResult(WebResult.Code.COACH_GET_ORGANIZER_INFO_ERROR);
        } else {
            if (code != null) {
                code = code.trim();
            }
            if (StringUtils.isBlank(status) || status.equals("[]")) {
                status = null;
            } else {
                status = "(" + status + ")";
            }
            PageList pageList = ordersService.getOrderCoachListByOption(activityId, episodeId, status, code, phone, organizer.getOrganizerId(), startTime, endTime, coachId, sortType, sortProp, page, size,isHaveRemark, abnormalRefundStartTime, abnormalRefundEndTime);
            BigDecimal allOrdersPrice = ordersService.getOrderCoachListPriceByOption(activityId, episodeId, status, code, phone, organizer.getOrganizerId(), startTime, endTime, coachId,isHaveRemark, abnormalRefundStartTime, abnormalRefundEndTime);
            Map<String, Object> map = new HashMap<>();
            map.put("list", pageList);
            map.put("allOrdersPrice", allOrdersPrice);
            return WebResult.getSuccessResult("data", map);
        }
    }

    /**
     * 获取教练订单核销记录
     * @param activityId
     * @param episodeId
     * @param page
     * @param size
     * @param startTime
     * @param endTime
     * @param code
     * @param cardType
     * @param request
     * @param session
     * @return
     */
    @ApiOperation(value = "核销记录列表查询接口", notes = "获取教练订单核销记录", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码(第几页)", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "size", value = "每页数量", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "activityId", value = "活动ID(activityId)", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "episodeId", value = "场次ID(episodeId)", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "code", value = "订单号", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "isRefund", value = "是否可退款（Y:是，N:否）", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "startTime", value = "hexi时间(订单核销时间)", required = false, dataType = "long", paramType = "query"),
            @ApiImplicitParam(name = "endTime", value = "结束时间(订单核销时间)", required = false, dataType = "long", paramType = "query"),
    })
    @RequestMapping(value = "/verify/list.json", method = RequestMethod.GET)
    public Object coachVerifyList(@RequestParam(value = "activityId", required = false) Integer activityId,
                                 @RequestParam(value = "episodeId", required = false) Integer episodeId,
                                 @RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
                                 @RequestParam(value = "size", required = false, defaultValue = "10") Integer size,
                                 @RequestParam(value = "startTime", required = false) Long startTime,
                                 @RequestParam(value = "endTime", required = false) Long endTime,
                                 @RequestParam(value = "code", required = false) String code,
                                 @RequestParam(value = "cardType", required = false, defaultValue = "1") String cardType,
                                  @RequestParam(value = "isRefund", required = false) String isRefund,
                                 HttpServletRequest request, HttpSession session) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);

        if (episodeId != null) {
            activityId = courseService.getActivityIdByEpisodeId(episodeId);
        }


        // B端改版 - 教练订单核销查询单独提供接口，不去计算产品类型ID了(typeId)
        /*List<ActivityType> allowTypes = organizerService.getOrganizerActivityAuth(typeId);
        List<Integer> typeIds = new ArrayList<>();
        for (ActivityType activityType : allowTypes) {
            typeIds.add(activityType.getTypeId());
        }*/
        List<Integer> typeIds = new ArrayList<>();
        typeIds.add(2);
        typeIds.add(8);

        String startDate = null;
        String endDate = null;
        if (startTime != null) {
            startDate = DateFormatUtils.format(new Date(startTime), "yyyy-MM-dd 00:00:00");
        }
        if (endTime != null) {
            endDate = DateFormatUtils.format(new Date(endTime), "yyyy-MM-dd 23:59:59");
        }
        if (StringUtils.isNotBlank(code)) {
            code = code.trim();
        }
        PageList verifyRecordList = verifyService.getVerifyRecordList(activityId, episodeId, code, startDate, endDate, typeIds, organizer.getOrganizerId(), Integer.parseInt(cardType),isRefund, page, size);
        BigDecimal allVerifyPriceForRecords = verifyService.getAllVerifyPriceForRecords(activityId, episodeId, code, startDate, endDate, typeIds, organizer.getOrganizerId());
        Map map = new HashMap<>();
        map.put("verifyRecordList", verifyRecordList);
        map.put("allVerifyPriceForRecords", allVerifyPriceForRecords);

        return WebResult.getSuccessResult("data", map);
    }

    //教练系统中，教练课时卡(次卡)出导记录接口
    @ApiOperation(value = "出导记录列表查询接口", notes = "根据筛选条件查询教练出导记录", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码(第几页)", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "size", value = "每页数量", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "activityId", value = "活动ID(activityId)", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "episodeId", value = "场次ID(episodeId)", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "status", value = "出导状态", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "coachId", value = "教练ID", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "phone", value = "教练手机号", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "code", value = "订单号", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "startTime", value = "开始时间(预约时间)", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "endTime", value = "结束时间(预约时间)", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "sortType", value = "列表排序规则（默认按降序排序：desc）", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "sortProp", value = "根据筛选条件进行排序（默认按照订单创建时间排序）", required = false, dataType = "string", paramType = "query"),
    })
    @RequestMapping(value = "/guide-record/list.json", method = RequestMethod.GET)
    public Object getOrderGuideRecordList(@RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
                                          @RequestParam(value = "size", required = false, defaultValue = "10") Integer size,
                                          @RequestParam(value = "activityId", required = false) Integer activityId,
                                          @RequestParam(value = "episodeId", required = false) Integer episodeId,
                                          @RequestParam(value = "status", required = false) Integer status,
                                          @RequestParam(value = "coachId", required = false) Integer coachId,
                                          @RequestParam(value = "phone", required = false) String coachPhone,
                                          @RequestParam(value = "code", required = false) String code,
                                          @RequestParam(value = "startTime", required = false) String startTime,
                                          @RequestParam(value = "endTime", required = false) String endTime,
                                          @RequestParam(value = "sortType", required = false, defaultValue = "desc") String sortType,
                                          @RequestParam(value = "sortProp", required = false, defaultValue = "create_time") String sortProp,
                                          HttpServletRequest request, HttpSession session) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null) {
            //无法从session中获取organizer信息
            return WebResult.getErrorResult(WebResult.Code.COACH_GET_ORGANIZER_INFO_ERROR);
        }

        if (code != null) {
            code = code.trim();
        }
        PageList pageList = ordersService.getOrderCoachExtendListByOption(activityId, episodeId, status, code, coachPhone, organizer.getOrganizerId(), startTime, endTime, coachId, sortType, sortProp, page, size);
        // 查询教练出导记录.
        return WebResult.getSuccessResult("data", pageList);
    }

    @ApiOperation(value = "团课出导记录列表查询接口", notes = "根据筛选条件查询团课出导记录", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码(第几页)", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "size", value = "每页数量", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "episodeId", value = "场次ID(episodeId)", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "status", value = "出导状态", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "cloudsType", value = "团课状态", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "coachId", value = "教练ID", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "phone", value = "教练手机号", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "startTime", value = "团课开始时间", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "endTime", value = "团课结束时间", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "sortType", value = "列表排序规则（默认按降序排序：desc）", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "sortProp", value = "根据筛选条件进行排序（默认按照订单创建时间排序）", required = false, dataType = "string", paramType = "query"),
    })
    @RequestMapping(value = "/league/guideRecord/list.json", method = RequestMethod.GET)
    public Object getLeagueGuideRecordList(@RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
                                          @RequestParam(value = "size", required = false, defaultValue = "10") Integer size,
                                          @RequestParam(value = "episodeId", required = false) Integer episodeId,
                                          @RequestParam(value = "status", required = false) Integer status,
                                          @RequestParam(value = "cloudsType", required = false) Integer cloudsType,
                                          @RequestParam(value = "coachId", required = false) Integer coachId,
                                          @RequestParam(value = "phone", required = false) String coachPhone,
                                          @RequestParam(value = "startTime", required = false) String startTime,
                                          @RequestParam(value = "endTime", required = false) String endTime,
                                          @RequestParam(value = "sortType", required = false, defaultValue = "desc") String sortType,
                                          @RequestParam(value = "sortProp", required = false, defaultValue = "create_time") String sortProp,
                                          HttpServletRequest request, HttpSession session) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null) {
            //无法从session中获取organizer信息
            return WebResult.getErrorResult(WebResult.Code.COACH_GET_ORGANIZER_INFO_ERROR);
        }

        PageList pageList = orderCoachExtendService.getCourseLeagueGuideRecordListByOption(organizer.getOrganizerId(), episodeId, startTime, endTime, coachId, coachPhone, status, cloudsType, sortType, sortProp, page, size);
        return WebResult.getSuccessResult("data", pageList);
    }

    /**
     */
    @ApiOperation(value = "查询团课订单详情接口", notes = "查询团课订单详情接口", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "chargeId", value = "chargeId", required = true, dataType = "int", paramType = "query")
    })
    @RequestMapping(value = "/league/detail.json", method = RequestMethod.GET)
    public Object courseLeagueDetail(@RequestParam("chargeId") Integer chargeId,
                                     HttpServletRequest request, HttpSession session) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (null == organizer) {
            return WebResult.getErrorResult(WebResult.Code.COACH_GET_ORGANIZER_INFO_ERROR);
        }

        CourseLeagueCharge courseLeagueDetail = chargeService.getCourseLeagueDetailByChargeId(chargeId);
        CoachGuideRecordView oce = orderCoachExtendService.getCourseLeagueAppointmentByChargeId(chargeId);
        List<Map> coachList = coachGuideRelationshipsService.getOrderOrCoachInfoByOption(oce.getExtendId(), 0);
        List<Orders> ordersList = ordersService.getMainOrderListByChargeId(chargeId);

        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("courseLeagueDetail", courseLeagueDetail);
        resultMap.put("courseLeagueGuideDetail", oce);
        resultMap.put("courseLeagueCoachList", coachList);
        resultMap.put("courseLeagueOrderList", ordersList);

        return WebResult.getSuccessResult("data", resultMap);
    }

    @ApiOperation(value = "根据订单号查询课时卡课时剩余情况", notes = "根据订单号查询课时卡课时剩余情况", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "code", value = "订单号", required = true, dataType = "string", paramType = "query")
    })
    @RequestMapping(value = "/frequency-info.json", method = RequestMethod.GET)
    public Object getOrderFrequencyInfo(@RequestParam(value = "code", required = true) String code,
                                          HttpServletRequest request, HttpSession session) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null) {
            //无法从session中获取organizer信息
            return WebResult.getErrorResult(WebResult.Code.COACH_GET_ORGANIZER_INFO_ERROR);
        }
        if (code != null) {
            code = code.trim();
        }
        Map map = new HashMap();
        Orders order = ordersService.getOrderDetailByCode(code);
        Charge charge = chargeService.getChargeById(order.getChargeId());
        Integer usedFrequency = ordersService.getfrequencyCountByOrderId(order.getOrderId(), 135);
        map.put("totalPrice", order.getTotalPrice());
        map.put("totalFrequency", charge.getFrequency());
        map.put("availableFrequency", charge.getFrequency() - usedFrequency);

        return WebResult.getSuccessResult("data", map);
    }

//    /**
//     *
//     * @param orderId  订单ID
//     * @param extendId
//     * @param detailType 查询订单详情  普通课程订单：0， 教练次卡订单：1
//     * @param session
//     * @param model
//     * @param request
//     * @return
//     */
//    //教练预约详情
//    @RequestMapping("/{orderId}")
//    public String toCoachOrderDetail(@PathVariable(value = "orderId") Integer orderId,
//                                     @RequestParam(value = "extendId", required = false) Integer extendId,
//                                     @RequestParam(value = "detailType", required = false) Integer detailType,
//                                     HttpSession session, Model model, HttpServletRequest request) {
//        injectOrgInfo(session, model);
//
//        Organizer organizer = WebUpmsContext.getOrgnization(request);
//        /*
//         * 如果订单不属于当前登录用户 返回没有权限
//         */
//        if (organizer == null || organizer.getOrganizerId() == null) {
//            return forwardErrorPage("获取主办方信息失败", model, request);
//        }
//        OrderView orderView = ordersService.getOrderDetail(orderId);
//        if (orderView == null) {
//            return forwardErrorPage("订单信息不存在", model, request);
//        }
//        if (!orderView.getSellerId().equals(organizer.getOrganizerId())) {
//            return forwardErrorPage("没有权限", model, request);
//        }
//        //注意orderView获取的顺序,ordersService.getOrderDetail(orderId);返回的orderView.getEpisodeId(),
//        List<Map> playerMeta = JSON.parseArray(episodeService.getEpisodeDetailById(orderView.getEpisodeId()).getPlayerMeta().replace("\n", ""), Map.class);
//        List<Map> gradeMeta = new ArrayList<>();
//        if (StringUtils.isNotBlank(organizer.getCourseGradeMeta())) {
//            gradeMeta = JSON.parseArray(organizer.getCourseGradeMeta().replace("\n", ""), Map.class);
//        }
//        List<Map> coaches = coachService.getCoachListOption(orderView.getEpisodeId());
//        //该订单关联课程下面的教练列表.用户修改教练下拉框显示
//        model.addAttribute("courseCoachs", JSON.toJSONString(coaches));
//        //getCustomerOrderDetail(orderId,organizer.getOrganizerId());没有返回orderView.getEpisodeId()
//        orderView = ordersService.getCustomerOrderDetail(orderId, organizer.getOrganizerId());
//        List<PlayerWithEnrollInfo> playerWithEnrollInfoList = enrollInfoService.getPlayerWithEnrollInfoByOrderId(orderId);
//        //订单的基本信息
//        model.addAttribute("order", orderView);
//        OrderCoachExtendList orderCoachExtendList = coachService.findOrderCoachExtendByOrderId(orderId);
//        //该订单预约教练的信息
//        model.addAttribute("orderCoach", orderCoachExtendList);
//        //报名用户的信息
//        model.addAttribute("players", JSON.toJSONString(playerWithEnrollInfoList));
//        model.addAttribute("playerMeta", JSON.toJSONString(playerMeta));
//        model.addAttribute("gradeMeta", JSON.toJSONString(gradeMeta));
//        model.addAttribute("filterRuler", new FreemarkFilter());
//        return forwardToCustomerPage("pages/admin-course-order-detail");
//    }

    /**
     * 教练预约详情
     * @param orderId
     * @param extendId
     * @param detailType 查询订单详情  普通课程订单：0， 教练次卡订单：1
     * @param session
     * @param model
     * @param request
     * @return
     */
    @ApiOperation(value = "【出导订单和出导记录】查看详情接口", notes = "查看预约详情(订单详情)接口", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "orderId", value = "订单ID", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "extendId", value = "预约ID", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "detailType", value = "订单类型（普通课程订单：0， 教练次卡订单：1）", required = true, dataType = "int", paramType = "query"),
    })
    @RequestMapping(value = "/detail.json", method = RequestMethod.GET)
    public Object orderDetail(@RequestParam(value = "orderId") Integer orderId,
                              @RequestParam(value = "extendId", required = false) Integer extendId,
                              @RequestParam(value = "detailType", required = true) Integer detailType,
                              HttpSession session, Model model, HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null || organizer.getOrganizerId() == null) {
            return WebResult.getErrorResult(WebResult.Code.COACH_GET_ORGANIZER_INFO_ERROR);
        }
        Map<String, Object> result = new HashMap<>();
        //OrderView orderView = ordersService.getOrderDetail(orderId);
        //getCustomerOrderDetail(orderId,organizer.getOrganizerId());没有返回orderView.getEpisodeId()
        //1. 根据订单号查询订单详情     2. 根据订单号和order_coach_extend查询订单详情(在教练系统中出导订单和出导记录点击详情按钮)
        OrderView orderView = ordersService.getDetailAndCoachFrequencyForOrder(orderId, extendId, organizer.getOrganizerId());
        if (orderView == null) {
            // 订单信息不存在
            return WebResult.getErrorResult(WebResult.Code.CASHIER_GET_ORDER_FAILED);
        }
        if (!orderView.getSellerId().equals(organizer.getOrganizerId())) {
            // 没有权限
            return WebResult.getErrorResult(WebResult.Code.NO_PERMISSION);
        }

        String episodePlayerMeta = null;
        if (detailType == 0) {//普通课程订单详情 -- order和orderCoachExtend时一对一的关系
            episodePlayerMeta = episodeService.getEpisodeDetailById(orderView.getEpisodeId()).getPlayerMeta().replace("\n", "");
        }

        if (detailType == 1) {//教练次卡订单详情 --
            Charge charge = chargeService.getChargeById(coachService.getCoachExtendByExtendId(extendId).getChargeId());
            episodePlayerMeta = episodeService.getEpisodeDetailById(charge.getEpisodeId()).getPlayerMeta().replace("\n", "");
        }


        //注意orderView获取的顺序,ordersService.getOrderDetail(orderId);返回的orderView.getEpisodeId(),
        List<Map> playerMeta = JSON.parseArray(episodePlayerMeta, Map.class);
        List<Map> gradeMeta = new ArrayList<>();
        if (StringUtils.isNotBlank(organizer.getCourseGradeMeta())) {
            gradeMeta = JSON.parseArray(organizer.getCourseGradeMeta().replace("\n", ""), Map.class);
        }

        //普通教练课程和教练课时卡使用同一方法查询报名人信息，只是传参数extendId的差别
        List<PlayerWithEnrollInfo> playerWithEnrollInfoList = enrollInfoService.getPlayerWithEnrollInfoByOrderIdAndExtendId(orderId, extendId, episodePlayerMeta);
        //查询orderCoachExtend和coach信息
        OrderCoachExtendList orderCoachExtendList = coachService.findOrderCoachExtendByOrderIdAndExtendId(orderId, extendId);
        //查询订单的支付记录
        Pay pay = ordersService.getPayInfoByOrderId(orderId);

        result.put("playerMeta", playerMeta);
        result.put("gradeMeta", gradeMeta);
        //订单的基本信息
        result.put("order", orderView);
        //该订单预约教练的信息
        result.put("orderCoach", orderCoachExtendList);
        //报名用户的信息
        result.put("players", playerWithEnrollInfoList);
        // 订单支付信息
        result.put("pay", pay);

        return WebResult.getSuccessResult("data", result);
    }

    @ApiOperation(value = "不可退款记录", notes = "不可退款记录", httpMethod = "GET")
    @RequestMapping("/unrefund/list.json")
    public WebResult coachUnRefundListJson(@RequestParam(value = "activityId", required = false) Integer activityId,
                                      @RequestParam(value = "episodeId", required = false) Integer episodeId,
                                      @RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
                                      @RequestParam(value = "size", required = false, defaultValue = "10") Integer size,
                                      @RequestParam(value = "startTime", required = false) Long startTime,
                                      @RequestParam(value = "typeId", required = true) Integer typeId,
                                      @RequestParam(value = "endTime", required = false) Long endTime,
                                      @RequestParam(value = "code", required = false) String code,
                                      @RequestParam(value = "isRefund", required = false) String isRefund,
                                      HttpServletRequest request) {

        Organizer organizer = WebUpmsContext.getOrgnization(request);
        List<Integer> typeIds = new ArrayList<>();
        typeIds.add(2);
        typeIds.add(8);

        Date startDate = null;
        Date endDate = null;
        if (startTime != null) {
            startDate = new Date(startTime);
        }
        if (endTime != null) {
            endDate = new Date(endTime);
        }
        if (StringUtils.isNotBlank(code)) {
            code = code.trim();
        }
        PageList unRefundList = unRefundService.getUnRefundList(activityId, episodeId, code, startDate, endDate, typeIds, organizer.getOrganizerId(), page, size);
        Map map = new HashMap<>();
        map.put("unRefundList", unRefundList);

        return WebResult.getSuccessResult("data", map);
    }

}
