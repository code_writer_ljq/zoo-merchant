package com.zoo.zoomerchantadminweb.order.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.zoo.account.constant.AccountEnum;
import com.zoo.account.service.AccountService;
import com.zoo.activity.util.RegexUtil;
import com.zoo.common.page.PageList;
import com.zoo.activity.dao.model.*;
import com.zoo.activity.dao.nutz.pojo.ActivityRelation;
import com.zoo.activity.dao.nutz.pojo.CommonAccount;
import com.zoo.activity.service.*;
import com.zoo.web.bean.WebResult;
import com.zoo.zoomerchantadminweb.context.WebUpmsContext;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

@Api(value = "api/server/verify", tags = {"主办方核销接口"}, description = "主办方核销接口描述")
@RestController
@RequestMapping("api/server/verify")
public class VerifyController {

    @Autowired
    private OrganizerService organizerService;

    @Autowired
    private VerifyService verifyService;

    @Autowired
    private OrdersService ordersService;

    @Autowired
    private ActivityService activityService;

    @Autowired
    private EpisodeService episodeService;

    @Autowired
    private ChargeService chargeService;

    @Autowired
    private AccountService accountService;

    @ApiOperation(value = "核销记录", notes = "核销记录", httpMethod = "GET")
    @RequestMapping("/list.json")
    public WebResult verifyListJson(@RequestParam(value = "activityId", required = false) Integer activityId,
                                    @RequestParam(value = "episodeId", required = false) Integer episodeId,
                                    @RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
                                    @RequestParam(value = "size", required = false, defaultValue = "10") Integer size,
                                    @RequestParam(value = "startTime", required = false) Long startTime,
                                    @RequestParam(value = "typeId", required = true) Integer typeId,
                                    @RequestParam(value = "endTime", required = false) Long endTime,
                                    @RequestParam(value = "code", required = false) String code,
                                    @RequestParam(value = "cardType", required = false, defaultValue = "1") String cardType,
                                    @RequestParam(value = "isRefund", required = false) String isRefund,
                                    HttpServletRequest request) {


        Organizer organizer = WebUpmsContext.getOrgnization(request);


        List<ActivityType> allowTypes = organizerService.getOrganizerActivityAuth(typeId);
        List<Integer> typeIds = new ArrayList<>();
        for (ActivityType activityType : allowTypes) {
            typeIds.add(activityType.getTypeId());
        }

        String startDate = null;
        String endDate = null;
        if (startTime != null) {
            startDate = DateFormatUtils.format(new Date(startTime), "yyyy-MM-dd 00:00:00");
        }
        if (endTime != null) {
            endDate = DateFormatUtils.format(new Date(endTime), "yyyy-MM-dd 23:59:59");
        }
        if (StringUtils.isNotBlank(code)) {
            code = code.trim();
        }
        PageList verifyRecordList = verifyService.getVerifyRecordList(activityId, episodeId, code, startDate, endDate, typeIds, organizer.getOrganizerId(), Integer.parseInt(cardType), isRefund, page, size);
//        BigDecimal allVerifyPriceForRecords = verifyService.getAllVerifyPriceForRecords(activityId, episodeId, code, startDate, endDate, typeIds, organizer.getOrganizerId());
        Map map = new HashMap<>();
        map.put("verifyRecordList", verifyRecordList);
//        map.put("allVerifyPriceForRecords", allVerifyPriceForRecords);

        return WebResult.getSuccessResult("data", map);
    }

    @ApiOperation(value = "可核销订单接口", notes = "可核销订单接口", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "size", value = "条", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "code", value = "订单号", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "phone", value = "手机号", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "type", value = "种类", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "activityId", value = "产品", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "keyword", value = "关键字查询", required = false, dataType = "string", paramType = "query")
    })
    @RequestMapping("/available.json")
    public WebResult verifyAvailable(
            @RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
            @RequestParam(value = "size", required = false, defaultValue = "10") Integer size,
            @RequestParam(value = "code", required = false) String code,
            @RequestParam(value = "phone", required = false) String phone,
            @RequestParam(value = "type", required = false) Integer type,
            @RequestParam(value = "activityId", required = false) Integer activityId,
            @RequestParam(value = "keyword", required = false) String keyword,
            HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if(StringUtils.isNotEmpty(keyword)) {
            if(RegexUtil.isPhoneNumber(keyword)) {
                phone = keyword;
            } else {
                code = keyword;
            }
            keyword = null;
        }
        return WebResult.getSuccessResult("data", ordersService.getVerifyAvailableOrders(code, organizer.getOrganizerId(), type, phone, activityId, keyword, page, size));
    }

    @ApiOperation(value = "核销", notes = "核销", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "verifyInfo", value = "核销信息JSON", required = false, dataType = "string", paramType = "query"),
    })
    @RequestMapping(value = "/verify.json", method = RequestMethod.POST)
    public WebResult verify(@RequestParam(value = "verifyInfo", required = true) String verifyInfo,
                            HttpServletRequest request) {
        List<Map> verifyJson = JSON.parseArray(verifyInfo, Map.class);
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        boolean multi = false;
        Integer parentOrderId = 0;
        for (Map tmp : verifyJson) {
            Integer tmpOrderId = Integer.parseInt(tmp.get("orderId").toString());
            Integer tmpVerifyCount = Integer.parseInt(tmp.get("verifyCount").toString());
            if (tmpVerifyCount == 0) {
                continue;
            }
            OrderView ordersView = ordersService.getCustomerOrderDetail(tmpOrderId, organizer.getOrganizerId());

            if (null == ordersView) {
                OrderView tmpOrders = ordersService.getOrderDetail(tmpOrderId);

                List<ActivityRelation> relations = activityService.getRelationIds(tmpOrders.getActivityId());
                boolean hasPermission = false;
                for (ActivityRelation relation : relations) {
                    if (relation.getOrganizerId().equals(organizer.getOrganizerId())) {
                        hasPermission = true;
                        break;
                    }
                }
                if (!hasPermission) {
                    return WebResult.getErrorResult(WebResult.Code.NO_PERMISSION);
                } else {
                    ordersView = ordersService.getCustomerOrderDetail(tmpOrderId, null);
                }
            }
            if (ordersView.getOrderStatus() == 2 || ordersView.getOrderStatus() == 3) {
                return WebResult.getErrorMsgResult(WebResult.Code.ADD_FAILED, "该订单已进入退款流程");
            }


            if (null != ordersView.getTypeId() && null != tmp.get("coach") && ordersView.getTypeId().equals(2)) {
                Orders orders = ordersService.getSimpleOrdersById(tmpOrderId);
                orders.setVerifyUrl(tmp.get("coach").toString());
                ordersService.updateOrders(orders);
            }
            // 判断票券是否到生效时间start
            Episode episode = episodeService.getEpisodeDetailById(ordersView.getEpisodeId());
            Activity activity = activityService.getActivityById(episode.getActivityId());
            if (episode.getAdvanceBookingTime() != null && activity.getTypeId() != 8 && activity.getTypeId() != 2) {
                Date now = new Date();
                if ("hour".equals(episode.getAdvanceBookingType())) {
                    if ((ordersView.getPayTime().getTime() + episode.getAdvanceBookingTime() * 3600 * 1000) > now.getTime()) {
                        return WebResult.getErrorResult(WebResult.Code.CHARGE_NOT_START);
                    }
                } else if ("day".equals(episode.getAdvanceBookingType())) {
                    if ((ordersView.getPayTime().getTime() + episode.getAdvanceBookingTime() * 24 * 3600 * 1000) > now.getTime()) {
                        return WebResult.getErrorResult(WebResult.Code.CHARGE_NOT_START);
                    }
                } else if ("minute".equals(episode.getAdvanceBookingType())) {
                    if ((ordersView.getPayTime().getTime() + episode.getAdvanceBookingTime() * 60 * 1000) > now.getTime()) {
                        return WebResult.getErrorResult(WebResult.Code.CHARGE_NOT_START);
                    }
                } else {
                    return WebResult.getErrorResult(WebResult.Code.ERROR);
                }
            }
            // 判断票券是否到生效时间end

            if (null != ordersView.getParentId() && ordersView.getParentId() != 0) {
                parentOrderId = ordersView.getParentId();
                multi = true;
            }
            if (ordersView != null && ordersView.getOrderId().equals(tmpOrderId) && ordersView.getOrderStatus() == 1) {
                if (ordersView.getAvailableCount() < tmpVerifyCount && activity.getCardType() != 2) {
                    return WebResult.getErrorResult(WebResult.Code.AVAILABLE_NOT_ENOUGH);
                } else {
                    if (activity.getCardType() == 2) {
                        List<VerifyRecord> verifyRecords = verifyService.getVerifyRecordListByOrder(tmpOrderId);
                        Integer verifiedCount = 0;
                        for (VerifyRecord record : verifyRecords) {
                            verifiedCount += record.getVerifyOrderNum();
                        }
                        if ((verifiedCount + tmpVerifyCount) > (ordersView.getChargeNum() * chargeService.getChargeById(ordersView.getChargeId()).getChargeNumber())) {
                            return WebResult.getErrorResult(WebResult.Code.AVAILABLE_NOT_ENOUGH);
                        }
                    }
                    /*对于自营雪票的产品 判断订单能否手动核销*/
//                    if (activity.getTypeId() == 6) {
//                        PartnerSystem partnerSystem = partnerSystemService.getPartnerSystemByOrgId(activity.getOrganizerId());
//                        if (partnerSystem != null && partnerSystem.getInterfaceType() > 0 && StringUtils.isNotBlank(ordersView.getChargeAlias())) {
//                            responseData.setStatus(Status.error);
//                            responseData.setMsg("该订单不能核销。");
//                            return responseData;
//                        }
//                    }
                    if (activity.getTypeId() == 3) {
                        return WebResult.getErrorResult(WebResult.Code.CANT_NOT_VERIFY);
                    } else {
                        Integer itemIndex = -1;
                        if (tmp.containsKey("itemIndex")) {
                            itemIndex = Integer.parseInt(tmp.get("itemIndex").toString());
                        }
                        try {
                            String userId = WebUpmsContext.getAccount(request).getUserId();
                            userId = ((CommonAccount) (accountService.getAccount(AccountEnum.FetchAccountTypeEnum.FETCH_BY_USER.getValue(), userId).getData())).getCustomerId().toString();
                            verifyService.verify(ordersView.getOrderId(), tmpVerifyCount, activity.getTypeId(),
                                    organizer.getOrganizerId(), userId, 1, itemIndex);
                        } catch (Exception e) {
                            return WebResult.getErrorResult(WebResult.Code.ERROR);
                        }
                    }
                }
            } else {
                return WebResult.getErrorResult(WebResult.Code.CANT_NOT_VERIFY);
            }
        }
        if (multi) {
            boolean allVerify = true;
            List<Orders> childOrders = ordersService.getChildOrders(parentOrderId);
            for (Orders orders : childOrders) {
                if (orders.getStatus() == 1) {
                    allVerify = false;
                }
            }
            if (allVerify) {
                Orders parentOrder = ordersService.getSimpleOrdersById(parentOrderId);
                parentOrder.setStatus(5);
                ordersService.updateOrders(parentOrder);
            }
        }
        return WebResult.getSuccessResult();

    }

    @ApiOperation(value = "批量核销接口", notes = "批量核销接口", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", value = "批量核销ID", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "parentIds", value = "批量核销ID", required = false, dataType = "string", paramType = "query")
    })
    @RequestMapping(value = "/batch-verify.json", method = RequestMethod.POST)
    public WebResult batchVerify(
            @RequestParam(value = "ids") String ids, @RequestParam(value = "parentIds") String parentIds, HttpServletRequest request) {
        if (StringUtils.isBlank(ids) && StringUtils.isBlank(parentIds)) {
            return WebResult.getErrorMsgResult(WebResult.Code.UPDATE_FAILED, "请先选择批量核销订单");
        }
        JSONArray parentIdsArray = JSONArray.parseArray(parentIds);
        JSONArray orderIdsArray = JSONArray.parseArray(ids);
        if (parentIdsArray.size() == 0 && orderIdsArray.size() == 0) {
            return WebResult.getErrorMsgResult(WebResult.Code.UPDATE_FAILED, "请先选择批量核销订单");
        }
        List parentList = parentIdsArray.subList(0, parentIdsArray.size());
        List orderList = orderIdsArray.subList(0, orderIdsArray.size());

        CommonAccount account = WebUpmsContext.getAccount(request);

        verifyService.batchVerify(parentList, orderList, account);

        return WebResult.getSuccessResult();
    }


}
