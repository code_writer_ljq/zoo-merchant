package com.zoo.zoomerchantadminweb.player.controller;

import com.alibaba.fastjson.JSON;
import com.zoo.activity.dao.model.*;
import com.zoo.activity.service.*;
import com.zoo.activity.util.DateUtil;
import com.zoo.web.bean.WebResult;
import com.zoo.zoomerchantadminweb.base.BaseController;
import com.zoo.zoomerchantadminweb.context.WebUpmsContext;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.*;

/**
 * Created by lihao on 15/8/24.
 */
@Api(value = "/api/server/player", tags = {"报名信息相关接口"}, description = "报名信息相关")
@RestController
@RequestMapping("/api/server/player")
public class PlayerController extends BaseController {

    @Autowired
    private OrdersService ordersService;

    @Autowired
    private PlayerService playerService;

    @Autowired
    private EpisodeService episodeService;

    @Autowired
    private ActivityService activityService;

    @Autowired
    private ChargeService chargeService;

    @Autowired
    private CampPlayerInfoService campPlayerInfoService;

    @Autowired
    private CourseService courseService;

    @ApiOperation(value = "团课签到查询接口", notes = "团课签到查询接口", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "chargeId", value = "chargeId", required = true, dataType = "int", paramType = "query")
    })
    @RequestMapping(value = "/course-league/signIn.json", method = RequestMethod.GET)
    public WebResult getPlayerSignInInfo(@RequestParam(value = "chargeId", required = true) Integer chargeId
                                        , HttpSession session) throws Exception {
        return WebResult.getSuccessResult("data", playerService.getCoachCourseLeaguePlayerListByChargeId(chargeId));
    }


//    @RequestMapping("/list")
//    public Object playList(HttpSession session, @RequestParam(value = "activityId", required = false) Integer activityId,
//                           @RequestParam(value = "episodeId", required = false) String episodeId,
//                           @RequestParam("cardType") String cardType,
//                           Model model) throws Exception {
//        injectOrgInfo(session, model);
//        model.addAttribute("cardType", cardType);
//        model.addAttribute("activityId", activityId);
//        if (StringUtils.isNotBlank(episodeId)) {
//            model.addAttribute("episodeId", episodeId);
//        }
//        return forwardToCustomerPage("pages/admin-player-list");
//    }
//
//    @Auth(authPath = "/hotel/management")
//    @RequestMapping("/hotel-order-list")
//    public Object hotelList(HttpSession session, @RequestParam(value = "activityId", required = false) Integer activityId,
//                            @RequestParam(value = "episodeId", required = false) String episodeId,
//                            Model model) throws Exception {
//        injectOrgInfo(session, model);
//        model.addAttribute("activityId", activityId);
//        if (StringUtils.isNotBlank(episodeId)) {
//            model.addAttribute("episodeId", episodeId);
//        }
//        return forwardToCustomerPage("pages/admin-hotel-order-list");
//    }
//
//    @Auth(authPath = "/hotel/record")
//    @RequestMapping("/hotel-verify-list")
//    public Object ticketVerifyList(HttpSession session, Model model) throws Exception {
//        injectOrgInfo(session, model);
//        Organizer organizer = SystemSessionUtil.getOrganizer(session);
//        model.addAttribute("types", organizer.getType());
//        return forwardToCustomerPage("pages/admin-verify-record-list");
//    }
//
//    @Auth(authPath = "/camp/search")
//    @RequestMapping("/camp-player-list")
//    public Object campPlayList(HttpSession session, @RequestParam(value = "activityId", required = false) Integer activityId,
//                               @RequestParam(value = "episodeId", required = false) String episodeId,
//                               Model model) throws Exception {
//        injectOrgInfo(session, model);
//        model.addAttribute("activityId", activityId);
//        if (StringUtils.isNotBlank(episodeId)) {
//            model.addAttribute("episodeId", episodeId);
//        }
//        return forwardToCustomerPage("pages/admin-camp-player-list");
//    }
//
//    /**
//     * 导出报名人员的excel
//     */
//    @RequestMapping("/export-coachInfo-by-episode")
//    public void ExportCoachInfo(@RequestParam(value = "episodeId", required = false) Integer episodeId,
//                                @RequestParam(value = "status", required = false) Integer status,
//                                @RequestParam(value = "startTime", required = false) Long startTime,
//                                @RequestParam(value = "endTime", required = false) Long endTime,
//                                HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
//        String baseFilePath = request.getSession().getServletContext().getRealPath("/");
//        if (status == null) {
//            status = -1;
//        }
//        Date startDate = null;
//        Date endDate = null;
//        Organizer organizer = SystemSessionUtil.getOrganizer(session);
//        Boolean hasPermission = false;
//        String permissionStr = SystemSessionUtil.getUserPermission(session);
//        if (permissionStr.contains("players") || permissionStr.equals("*")) hasPermission = true;
//        Boolean isOwner = activityService.checkIsOrganizer(organizer.getOrganizerId(), episodeService.getEpisodeDetailById(episodeId).getActivityId());
//        if (!isOwner || !hasPermission) {
//            injectResponse(response, "没有权限");
//        }
//        if (startTime != null) {
//            startDate = new Date(startTime);
//        }
//        if (endTime != null) {
//            endDate = new Date(endTime);
//        }
//        List<PlayerWithEnrollInfo> players = playerService.getCoachPlayersByEpisode(episodeId, status, startDate, endDate);
//
//        String startDateStr = null;
//        String endDateStr = null;
//        if (startDate != null) {
//            startDateStr = DateUtil.convertDateToString(startDate, DateUtil.TIMEF_FORMAT);
//        }
//        if (endDate != null) {
//            endDateStr = DateUtil.convertDateToString(endDate, DateUtil.TIMEF_FORMAT);
//        }
//        String filePath = playerService.exportExcelFile(players, episodeId, startDateStr, endDateStr, baseFilePath);
//        downLoadFile(filePath, response, "教练预约.xls");
//    }
//
//
//    /**
//     * 导出报名人员的excel
//     */
//    @RequestMapping(value = "/export-player-by-episode", method = RequestMethod.GET)
//    public void Export(@RequestParam(value = "episodeId", required = false) Integer episodeId,
//                       @RequestParam(value = "status", required = false) Integer status,
//                       @RequestParam(value = "startTime", required = false) String startTime,
//                       @RequestParam(value = "endTime", required = false) String endTime,
//                       HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
//        String baseFilePath = request.getSession().getServletContext().getRealPath("/");
//        if (status == null) {
//            status = -1;
//        }
//        Organizer organizer = SystemSessionUtil.getOrganizer(session);
//        Boolean hasPermission = false;
//        String permissionStr = SystemSessionUtil.getUserPermission(session);
//        if (permissionStr.contains("player") || permissionStr.equals("*")) hasPermission = true;
//        Boolean isOwner = activityService.checkIsOrganizer(organizer.getOrganizerId(), episodeService.getEpisodeDetailById(episodeId).getActivityId());
//        if (!isOwner || !hasPermission) {
//            injectResponse(response, "没有权限");
//        }
//
//        List<PlayerWithEnrollInfo> players = playerService.getPlayersByEpisodeId(episodeId, status, startTime, endTime);
//        String filePath = playerService.exportExcelFile(players, episodeId, startTime, endTime, baseFilePath);
//
//        downLoadFile(filePath, response, "报名信息.xls");
//    }
//
//    /**
//     * 导出报名人员的excel
//     */
//    @RequestMapping(value = "/export-player-by-activity", method = RequestMethod.GET)
//    public void Export(@RequestParam(value = "activityId", required = false) Integer activityId,
//                       @RequestParam(value = "episodeId", required = false) Integer episodeId,
//                       @RequestParam(value = "status", required = false) Integer status,
//                       @RequestParam(value = "fromAssign", required = false) Integer fromAssign,
//                       @RequestParam(value = "startTime", required = false) String startTime,
//                       @RequestParam(value = "endTime", required = false) String endTime,
//                       HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
//        if (fromAssign != null) {
//            episodeId = courseService.getCourseIdByActivityId(activityId).getEpisodeId();
//        }
//        String baseFilePath = request.getSession().getServletContext().getRealPath("/");
//        if (status == null) {
//            status = -1;
//        }
//        Organizer organizer = SystemSessionUtil.getOrganizer(session);
//        Boolean hasPermission = false;
//        String permissionStr = SystemSessionUtil.getUserPermission(session);
//        if (permissionStr.contains("player") || permissionStr.equals("*")) hasPermission = true;
//        Boolean isOwner = activityService.checkIsOrganizer(organizer.getOrganizerId(), activityId);
//        if (!isOwner || !hasPermission) {
//            injectResponse(response, "没有权限");
//        }
//
//        String filePath;
//        if (null != episodeId && 0 != episodeId) {
//            List<PlayerWithEnrollInfo> players = playerService.getPlayersByEpisodeId(episodeId, status, startTime, endTime);
//            filePath = playerService.exportExcelFile(players, episodeId, startTime, endTime, baseFilePath);
//        } else {
//            filePath = playerService.exportExcelFile2(activityId, status, startTime, endTime, baseFilePath);
//        }
//        downLoadFile(filePath, response, "报名信息.xls");
//    }
//
//
//    /**
//     * 返回所有参加活动的player列表
//     *
//     * @param page       页数
//     * @param size       页大小
//     * @param activityId 活动ID
//     * @param episodeId  场次ID
//     * @param status     活动状态
//     * @return 返回一个PageList对象
//     */
//
//    @RequestMapping(value = "/player/list.json", method = RequestMethod.GET)
//    @ResponseBody
//    public Object getPlayerList(@RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
//                                @RequestParam(value = "size", required = false, defaultValue = "10") Integer size,
//                                @RequestParam(value = "activityId", required = false) Integer activityId,
//                                @RequestParam(value = "episodeId", required = false) Integer episodeId,
//                                @RequestParam(value = "name", required = false) String name,
//                                @RequestParam(value = "phone", required = false) String phone,
//                                @RequestParam(value = "status", required = false) Integer status, HttpSession session,
//                                @RequestParam(value = "cardType", required = false, defaultValue = "1") String cardType,
//                                @RequestParam(value = "startTime", required = false) String startTime,
//                                @RequestParam(value = "endTime", required = false) String endTime
//                                ) throws Exception {
//        if (cardType == "1") {
//            cardType = "0";
//        }
//        ResponseData responseData = new ResponseData();
//        PageBounds pageBounds = new PageBounds();
//        pageBounds.setPage(page);
//        pageBounds.setLimit(size);
//        if (!StringUtils.isBlank(name)) {
//            byte[] bytes = name.getBytes("ISO-8859-1");
//            name = new String(bytes, "utf-8");
//        }
//        Organizer organizer = SystemSessionUtil.getOrganizer(session);
//        PageList orderViews = ordersService.getOrderPlayerList(activityId, episodeId, status, organizer.getOrganizerId(), name, phone, Integer.parseInt(cardType), startTime, endTime, pageBounds);
//        for (Object obj : orderViews.getDataList()) {
//            OrderView orderView = (OrderView) obj;
//            if(orderView.getCardType() == 3) {
//                orderView.setPrice(orderView.getTotalPrice().divide(new BigDecimal(orderView.getChargeNum())));
//            }
//        }
//        BigDecimal allOrdersPriceForPlayers = ordersService.getAllOrderViewPriceForPlayers(activityId, episodeId, status, organizer.getOrganizerId(), name, phone, startTime, endTime);
//        Map map = new HashMap<>();
//        map.put("list", orderViews);
//        map.put("allOrdersPriceForPlayers", allOrdersPriceForPlayers);
//        responseData.setData(map);
//        responseData.setStatus(Status.success);
//        return responseData;
//    }
//
//
//    @RequestMapping(value = "/hotel-player/list.json", method = RequestMethod.GET)
//    @ResponseBody
//    public Object getHotelPlayerList(@RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
//                                     @RequestParam(value = "size", required = false, defaultValue = "10") Integer size,
//                                     @RequestParam(value = "activityId", required = false) Integer activityId,
//                                     @RequestParam(value = "episodeId", required = false) Integer episodeId,
//                                     @RequestParam(value = "name", required = false) String name,
//                                     @RequestParam(value = "phone", required = false) String phone,
//                                     @RequestParam(value = "status", required = false) Integer status, HttpSession session) throws Exception {
//        ResponseData responseData = new ResponseData();
//        PageBounds pageBounds = new PageBounds();
//        pageBounds.setPage(page);
//        pageBounds.setLimit(size);
//        if (!StringUtils.isBlank(name)) {
//            byte[] bytes = name.getBytes("ISO-8859-1");
//            name = new String(bytes, "utf-8");
//        }
//        Organizer organizer = SystemSessionUtil.getOrganizer(session);
//        PageList orderViews = ordersService.getHotelOrderPlayerList(activityId, episodeId, status, organizer.getOrganizerId(), name, phone, pageBounds);
//        Map map = new HashMap<>();
//        map.put("list", orderViews);
//        responseData.setData(map);
//        responseData.setStatus(Status.success);
//        return responseData;
//    }
//
//    @RequestMapping(value = "/camp/player/list.json", method = RequestMethod.GET)
//    @ResponseBody
//    public Object getCampPlayerList(@RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
//                                    @RequestParam(value = "size", required = false, defaultValue = "10") Integer size,
//                                    @RequestParam(value = "activityId", required = false) Integer activityId,
//                                    @RequestParam(value = "episodeId", required = false) Integer episodeId,
//                                    @RequestParam(value = "name", required = false) String name,
//                                    @RequestParam(value = "phone", required = false) String phone,
//                                    @RequestParam(value = "status", required = false) Integer status,
//                                    @RequestParam(value = "startTime", required = false) String startTime,
//                                    @RequestParam(value = "endTime", required = false) String endTime,
//                                    HttpSession session) throws Exception {
//        ResponseData responseData = new ResponseData();
//        PageBounds pageBounds = new PageBounds();
//        pageBounds.setPage(page);
//        pageBounds.setLimit(size);
//        if (!StringUtils.isBlank(name)) {
//            byte[] bytes = name.getBytes("ISO-8859-1");
//            name = new String(bytes, "utf-8");
//        }
//        Organizer organizer = SystemSessionUtil.getOrganizer(session);
//        PageList orderViews = ordersService.getOrderCampPlayerList(activityId, episodeId, status, organizer.getOrganizerId(), name, phone, startTime, endTime, pageBounds);
//
//        //统计票的数量及金额
////        Integer allOrdersChargeCount = 0;
////        for (Object obj : orderViews.getDataList()) {
////            OrderView orderView = (OrderView) obj;
////            if (orderView.getChargeNum() !=null && orderView.getChargeNum() > 0) {
////                allOrdersChargeCount += orderView.getChargeNum();
////            }
////        }
//        //BigDecimal allOrdersPriceForPlayers = ordersService.getAllOrderViewPriceForPlayers(activityId, episodeId, status, organizer.getOrganizerId(), name, phone, startTime, endTime);
//
//        Map map = new HashMap<>();
////        map.put("allOrdersPriceForPlayers", allOrdersPriceForPlayers);
////        map.put("allOrdersChargeCount", allOrdersChargeCount);
//        map.put("episodePlayerMeta", "");
//
//        if (episodeId != null) {
//            Episode episodeDetail = episodeService.getEpisodeDetailById(episodeId);
//            if (episodeDetail.getPlayerMeta() != null && episodeDetail.getPlayerMeta().length() > 0) {
//                map.put("episodePlayerMeta", episodeDetail.getPlayerMeta());
//            }
//        }
//
//        map.put("list", orderViews);
//        responseData.setData(map);
//        responseData.setStatus(Status.success);
//        return responseData;
//    }
//
//    @RequestMapping(value = "/camp/player/enrollInfoList.json", method = RequestMethod.GET)
//    @ResponseBody
//    public Object getPlayerList(@RequestParam(value = "episodeId", required = true) Integer episodeId, HttpSession session) throws Exception {
//        ResponseData responseData = new ResponseData();
//        Episode episode = episodeService.getEpisodeDetailById(episodeId);
//        Activity activity = activityService.getActivityById(episode.getActivityId());
//        if (activity.getTypeId() != 5) {
//            responseData.setData(null);
//            responseData.setStatus(Status.error);
//            responseData.setMsg("not camp activity");
//            return responseData;
//        }
//        PageList resultPageList = new PageList();
//        List<Map> playerMeta = JSON.parseArray(episodeService.getEpisodeDetailById(episodeId).getPlayerMeta().replace("\n", ""), Map.class);
//        List<String> resultEnrollInfoList = new ArrayList<>();
//        for (Map meta : playerMeta) {
//            resultEnrollInfoList.add((String) meta.get("title"));
//        }
//        resultPageList.setDataList(resultEnrollInfoList);
//        resultPageList.setCount(resultEnrollInfoList.size());

    /**
     * 修改报名人信息
     * @param playerId
     * @param realName
     * @param phone
     * @param appointmentTime
     * @param gender
     * @param idCardNo
     * @param idCardType
     * @param email
     * @param note
     * @param avatarUrl
     * @param otherInfoChanged
     * @param gradeInfoChanged
     * @param session
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "修改报名人信息接口", notes = "修改报名人信息", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "playerId", value = "报名ID", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "real_name", value = "报名人姓名", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "phone", value = "手机号", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "appointment_time", value = "预约时间(时间戳)", required = false, dataType = "long", paramType = "query"),
            @ApiImplicitParam(name = "gender", value = "性别(如果不选择性别就值为''，否则女0，男1)", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "id_card_no", value = "身份证号", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "id_card_type", value = "证件类型", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "email", value = "邮箱", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "note", value = "介绍", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "avatar_url", value = "图片地址(链接地址)", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "ski_level", value = "滑雪水平", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "otherInfo", value = "其他信息", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "gradeInfo", value = "评价表信息", required = false, dataType = "string", paramType = "query"),
    })
    @RequestMapping(value = "/update.json", method = RequestMethod.POST)
    public Object updatePlayer(@RequestParam(value = "playerId") Integer playerId,
                               @RequestParam(value = "real_name", required = false) String realName,
                               @RequestParam(value = "phone", required = false) String phone,
                               @RequestParam(value = "appointment_time", required = false) Long appointmentTime,
                               @RequestParam(value = "gender", required = false) String gender,
                               @RequestParam(value = "id_card_no", required = false) String idCardNo,
                               @RequestParam(value = "id_card_type", required = false) String idCardType,
                               @RequestParam(value = "email", required = false) String email,
                               @RequestParam(value = "note", required = false) String note,
                               @RequestParam(value = "avatar_url", required = false) String avatarUrl,
                               @RequestParam(value = "ski_level", required = false) String skiLevel,
                               @RequestParam(value = "age_group", required = false) String ageGroup,
                               @RequestParam(value = "otherInfo", required = false) String otherInfoChanged,
                               @RequestParam(value = "gradeInfo", required = false) String gradeInfoChanged,
                               HttpServletRequest request, HttpSession session) throws Exception {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        Player playerDB = playerService.getPlayerByPlayerId(playerId);
        Orders orders = ordersService.getSimpleOrdersById(playerDB.getOrderId());

        if (!orders.getSellerId().equals(organizer.getOrganizerId())) {
            // 没有权限
            return WebResult.getErrorResult(WebResult.Code.NO_PERMISSION);
        }
        Player player = new Player();
        player.setPlayerId(playerId);
        player.setRealName(realName);
        player.setPhone(phone);
        if (appointmentTime != null) {
            player.setAppointmentTime(new Date(appointmentTime));
        }
        player.setGender(gender);
        player.setIdCardNo(idCardNo);
        player.setIdCardType(idCardType);
        player.setEmail(email);
        player.setNote(note);
        player.setAvatarUrl(avatarUrl);
        if (StringUtils.isNotBlank(otherInfoChanged)) {
            if (StringUtils.isNotBlank(skiLevel))
                otherInfoChanged = otherInfoChanged.substring(0, otherInfoChanged.length() - 1) + ",\"ski_level\":\"" + skiLevel + "\"}";

            if (StringUtils.isNotBlank(ageGroup))
                otherInfoChanged = otherInfoChanged.substring(0, otherInfoChanged.length() - 1) + ",\"age_group\":\"" + ageGroup + "\"}";

            player.setOtherInfo(otherInfoChanged.getBytes("UTF-8"));
        }
        //if (StringUtils.isNotBlank(gradeInfoChanged) && !"{}".equals(gradeInfoChanged)) {
        if (StringUtils.isNotBlank(gradeInfoChanged)) {
            player.setGradeInfo(gradeInfoChanged.getBytes("UTF-8"));
        }
        player.setOrderId(playerDB.getOrderId());
        Boolean result = playerService.updatePlayer(player);
        //训练营情况下 更新campplayerInfo
        Activity activity = activityService.getActivityById(chargeService.getChargeById(orders.getChargeId()).getActivityId());
        Episode episode = episodeService.getEpisodeDetailById(chargeService.getChargeById(orders.getChargeId()).getEpisodeId());
        //训练营  插入到 camp_player_info表里面数据
        if (activity.getTypeId() == 5) {
            if (player.getOtherInfo() != null) {
                CampPlayerInfo campPlayerInfo = new CampPlayerInfo();
                campPlayerInfo.setOrderId(playerDB.getOrderId());
                campPlayerInfo.setPlayerId(player.getPlayerId());
                /*将player的otherInfo转换成String类型*/
                Map<String, String> otherInfo = (Map<String, String>) JSON.parse(otherInfoChanged);
                for (Map.Entry<String, String> entry : otherInfo.entrySet()) {
                    String key = entry.getKey();
                    String value = entry.getValue();
                    String otherInfoTitle = this.getOtherInfoTitleById(episode, key);
                    if (otherInfoTitle.contains("##")) {
                        String columName = otherInfoTitle.split("##")[1];
                        String tableName = columName.split(":")[0];
                        columName = columName.split(":")[1];
                        //训练营的时候
                        if (tableName.equals("camp_player_info")) {
                            switch (columName) {
                                case "name":
                                    campPlayerInfo.setName(value);
                                    break;
                                case "gender":
                                    campPlayerInfo.setGender(value);
                                    break;
                                case "birthday":
                                    campPlayerInfo.setBirthday(DateUtil.convertStringToDate(value, "yyyy年MM月dd日"));
                                    break;
                                case "height":
                                    campPlayerInfo.setHeight(Double.parseDouble(value));
                                    break;
                                case "weight":
                                    campPlayerInfo.setWeight(Double.parseDouble(value));
                                    break;
                                case "shoesSize":
                                    campPlayerInfo.setShoesSize(Double.parseDouble(value));
                                    break;
                                case "location":
                                    campPlayerInfo.setLocation(value);
                                    break;
                                case "zipCode":
                                    campPlayerInfo.setZipCode(value);
                                    break;
                                case "telephone":
                                    campPlayerInfo.setTelephone(value);
                                    break;
                                case "email":
                                    campPlayerInfo.setEmail(value);
                                    break;
                                case "phone":
                                    campPlayerInfo.setPhone(value);
                                    break;
                                case "expectTime":
                                    campPlayerInfo.setExpectTime(DateUtil.convertStringToDate(value, "yyyy年MM月dd日"));
                                    break;
                                case "skiLevel":
                                    campPlayerInfo.setSkiLevel(value);
                                    break;
                                case "fatherName":
                                    campPlayerInfo.setFatherName(value);
                                    break;
                                case "fatherJob":
                                    campPlayerInfo.setFatherJob(value);
                                    break;
                                case "fatherPhone":
                                    campPlayerInfo.setFatherPhone(value);
                                    break;
                                case "motherName":
                                    campPlayerInfo.setMotherName(value);
                                    break;
                                case "motherJob":
                                    campPlayerInfo.setMotherJob(value);
                                    break;
                                case "motherPhone":
                                    campPlayerInfo.setMotherPhone(value);
                                    break;
                                case "emergencyContactPersonName":
                                    campPlayerInfo.setEmergencyContactPersonName(value);
                                    break;
                                case "emergencyContactPersonPhone":
                                    campPlayerInfo.setEmergencyContactPersonPhone(value);
                                    break;
                                case "experience":
                                    campPlayerInfo.setExperience(value);
                                    break;
                                case "hobby":
                                    campPlayerInfo.setHobby(value);
                                    break;
                                case "specialty":
                                    campPlayerInfo.setSpecialty(value);
                                    break;
                                case "description":
                                    campPlayerInfo.setDescription(value);
                                    break;
                                case "specialFood":
                                    campPlayerInfo.setSpecialFood(value);
                                    break;
                                case "allergyFood":
                                    campPlayerInfo.setAllergyFood(value);
                                    break;
                                case "preference":
                                    campPlayerInfo.setPreference(value);
                                    break;
                                case "disease":
                                    campPlayerInfo.setDisease(value);
                                    break;
                                case "constitution":
                                    campPlayerInfo.setConstitution(value);
                                    break;
                                case "wechatId":
                                    campPlayerInfo.setWechatId(value);
                                    break;
                                case "skiType":
                                    campPlayerInfo.setSkiType(value);
                                    break;
                                case "other":
                                    campPlayerInfo.setOther(value);
                                    break;
                                case "school":
                                    campPlayerInfo.setSchool(value);
                                    break;
                                case "idCardNo":
                                    campPlayerInfo.setIdCardNo(value);
                                    break;
                                case "skiAge":
                                    campPlayerInfo.setSkiAge(Integer.parseInt(value));
                                    break;
                                case "nation":
                                    campPlayerInfo.setNation(value);
                                    break;
                                case "age":
                                    campPlayerInfo.setAge(Integer.parseInt(value));
                                    break;
                                case "playerType":
                                    campPlayerInfo.setPlayerType(value);
                                    break;
                                case "idCardType":
                                    campPlayerInfo.setIdCardType(value);
                                    break;
                                case "emergencyIdCardType":
                                    campPlayerInfo.setEmergencyIdCardType(value);
                                    break;
                                case "fatherIdCardType":
                                    campPlayerInfo.setFatherIdCardType(value);
                                    break;
                                case "motherIdCardType":
                                    campPlayerInfo.setMotherIdCardType(value);
                                    break;
                                case "emergencyIdCard":
                                    campPlayerInfo.setEmergencyIdCard(value);
                                    break;
                                case "salesperson":
                                    campPlayerInfo.setSalesperson(value);
                                    break;
                                case "recommender":
                                    campPlayerInfo.setRecommender(value);
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                }
                campPlayerInfoService.deleteByPlayerId(player.getPlayerId());
                campPlayerInfoService.insertSelective(campPlayerInfo);
            }
        }
        if (result) {
            return WebResult.getSuccessResult();
        } else {
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }
    }
//        responseData.setData(resultPageList);
//        responseData.setStatus(Status.success);
//        responseData.setMsg("Get city list success!");
//        return responseData;

//    }


    @ApiImplicitParam(name = "teamCredentialsId", value = "团队单ID", required = true, dataType = "int", paramType = "query")
    @GetMapping("/getPlayerByTeamCredentialsId.json")
    public WebResult getPlayerByTeamCredentialsId(@RequestParam("teamCredentialsId") Integer teamCredentialsId){
        List<Map> map = playerService.getPlayerByTeamCredentialsId(teamCredentialsId);
        WebResult webResult = WebResult.getSuccessResult();
        webResult.put("data", map);
        return webResult;
    }

    private String getOtherInfoTitleById(Episode episode, String otherInfoId) {
        String playerMeta = episode.getPlayerMeta();
        List<Map> list = JSON.parseArray(playerMeta, Map.class);
        for (Map<String, String> map : list) {
            if (map.get("id").equals(otherInfoId)) {
                return map.get("title");
            }
        }
        return null;
    }
}
