package com.zoo.zoomerchantadminweb.dowload.controller;

import com.github.pagehelper.PageInfo;
import com.zoo.activity.dao.model.Organizer;
import com.zoo.activity.dao.nutz.pojo.CommonAccount;
import com.zoo.activity.service.OrdersService;
import com.zoo.common.util.PropertiesFileUtil;
import com.zoo.download.service.DownloadDistributerService;
import com.zoo.finance.dao.model.*;
import com.zoo.finance.service.*;
import com.zoo.util.data.DateUtil;
import com.zoo.web.bean.WebResult;
import com.zoo.zoomerchantadminweb.context.WebUpmsContext;
import com.zoo.zoomerchantadminweb.finance.controller.BalanceAccountsController;
import com.zoo.zoomerchantadminweb.finance.controller.poi.util.FinanceExcelUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import jodd.util.StringUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.text.ParseException;
import java.util.*;

@Api(value = "api/download", tags = {"【财务系统】下载Excell接口"}, description = "这是为前台UE提供服务Excell下载接口")
@RestController
@RequestMapping("api/download")
public class DowloadController {

    private static final Logger LOGGER = LoggerFactory.getLogger(DowloadController.class);

    @Autowired
    private ZooSettlementDetailService zooSettlementDetailService;

    @Autowired
    private ZooSettlementTransferAccountsService zooSettlementTransferAccountsService;

    @Autowired
    private ZooSettlementInvoiceService zooSettlementInvoiceService;

    @Autowired
    private ZooBalanceStatisticsService zooBalanceStatisticsService;

    @Autowired
    private ZooRentExceptionService zooRentExceptionService;

    @Autowired
    private ZooOnAccountDayDetailService zooOnAccountDayDetailService;

    @Autowired
    private ZooEntertainOrdersService zooEntertainOrdersService;

    @Autowired
    private ZooOtaStatisticsService zooOtaStatisticsService;

    @Autowired
    private ZooPayStattisticsService zooPayStattisticsService;

    @Autowired
    private ZooRefundStatisticsService zooRefundStatisticsService;

    @Autowired
    private ZooBalanceDepositIndemnityService zooBalanceDepositIndemnityService;

    @Autowired
    private ZooEntertainDetailService zooEntertainDetailService;

    @Autowired
    private ZooOnAccountOrdersService zooOnAccountOrdersService;

    @Autowired
    private ZooOnAccountCompanyService zooOnAccountCompanyService;

    @Autowired
    private OrdersService ordersService;

    @Autowired
    private ZooBalanceOrdersService zooBalanceOrdersService;

    @Autowired
    private DownloadDistributerService downloadDistributerService;

    private String filePath = PropertiesFileUtil.getInstance("config").get("web.Excell-path");

    //导出文件系统路径
    private String exportFilePath = PropertiesFileUtil.getInstance("config").get("export.file.path");

    @ApiOperation(value = "导出excel通用接口", httpMethod = "GET")
    @RequestMapping(value = "/export-file-common", method = RequestMethod.GET)
    @ResponseBody
    public Map export(HttpServletResponse response, HttpServletRequest request,
                      @RequestParam(required = true, value = "service") String service) throws IOException {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        CommonAccount account = WebUpmsContext.getAccount(request);
        Enumeration enu = request.getParameterNames();
        Map parameterMap = new HashMap();
        while (enu.hasMoreElements()) {
            String paraName = (String) enu.nextElement();
            parameterMap.put(paraName, request.getParameter(paraName));
        }
        Map export = downloadDistributerService.export(parameterMap, service, account.getCustomerId().longValue(), account.getRealname(),account.getIsnowPassword(), organizer.getOrganizerId().longValue(), organizer.getName());
        return export;
    }

    @ApiOperation(value = "导出excel通用接口-下载文件", httpMethod = "GET")
    @RequestMapping(value = "/export-file-download", method = RequestMethod.GET)
    public void download(HttpServletResponse response, HttpServletRequest request,
                         @RequestParam(required = true, value = "fileName") String fileName,
                         @RequestParam(required = false, value = "downloadName") String downloadName) throws IOException {

        String file = exportFilePath + File.separator + fileName;
        File f = new File(file);
        if (!f.exists()) {
            ServletOutputStream outputStream = response.getOutputStream();
            outputStream.write("文件不存在".getBytes());
            response.flushBuffer();
            return;
        }

        if (StringUtils.isEmpty(downloadName)) {
            downloadName = System.currentTimeMillis() + "";
        }

        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        response.setHeader("Cache-Control", "no-cache");
        response.setHeader("Content-Disposition", "attachment;filename=" + new String(downloadName.getBytes("gbk"), "iso8859-1"));
        ServletOutputStream outputStream = response.getOutputStream();
        FileInputStream in = new FileInputStream(file);
        byte[] b = new byte[1024];
        int length = 0;
        while ((length = in.read(b)) > 0) {
            outputStream.write(b, 0, length);
        }
        response.flushBuffer();
    }


    @ApiOperation(value = "导出对账列表excel", httpMethod = "GET")
    @RequestMapping(value = "/export-balanceList", method = RequestMethod.GET)
    public void export(HttpServletResponse response, HttpServletRequest request,
                       @RequestParam(required = false, value = "status", defaultValue = "1") Integer status,
                       @RequestParam(required = false, value = "startTime") String startTime,
                       @RequestParam(required = false, value = "endTime") String endTime) throws IOException {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        Map<String, List<Map>> dataOneList = getStringListMap(1, startTime, endTime, organizer);
        Map<String, List<Map>> dataTwoList = getStringListMap(3, startTime, endTime, organizer);
        Map<String, List<Map>> dataThreeList = getStringListMap(2, startTime, endTime, organizer);
        String filename = organizer.getOrganizerId() + "_" + DateUtil.getToday("yyyyMMddHHmmss") + ".xls";
        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        response.setHeader("Cache-Control", "no-cache");
        response.setHeader("Content-Disposition", "attachment;filename=" + new String(filename.getBytes("gbk"), "iso8859-1"));
        ExcelExportUtil.exportSXSSFExcel(dataOneList, dataTwoList, dataThreeList, response.getOutputStream(), filePath + "/balanceList");
        response.flushBuffer();
    }

    @ApiOperation(value = "导出订单列表excel", httpMethod = "GET")
    @RequestMapping(value = "/export-balanceOrders", method = RequestMethod.GET)
    public void exportBalanceOrders(HttpServletResponse response, HttpServletRequest request,
                                    @RequestParam(required = true, value = "id") Integer id,
                                    @RequestParam(required = false, value = "startTime") String startTime,
                                    @RequestParam(required = false, value = "endTime") String endTime) throws IOException {
        Organizer organizer = WebUpmsContext.getOrgnization(request);

        ZooSettlementDetailExample zooSettlementDetailExample = new ZooSettlementDetailExample();
        zooSettlementDetailExample.createCriteria().andIdEqualTo(id);
        List<ZooSettlementDetail> zooSettlementDetails = zooSettlementDetailService.selectByExample(zooSettlementDetailExample);
        ZooSettlementDetail zooSettlementDetail = zooSettlementDetails.get(0);
        Date startDate = zooSettlementDetail.getStartTime();
        Date endDate = zooSettlementDetail.getEndTime();
        List<Map<String, List<Map>>> lists = new ArrayList<Map<String, List<Map>>>();
        try {
            if (startTime != null) {
                startDate = DateUtil.convertStringToDate(startTime + " 00:00:00", "yyyy-MM-dd HH:mm:ss");
            }
            if (endTime != null) {
                endDate = DateUtil.convertStringToDate(endTime + " 23:59:59", "yyyy-MM-dd HH:mm:ss");
            }

            Date goTime = startDate;
            Date endGoTime = endDate;
            for (int i = 1; goTime.getTime() < endDate.getTime(); i++) {
                Map<String, List<Map>> map = new HashMap<String, List<Map>>();
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(goTime);
                calendar.add(Calendar.DAY_OF_MONTH, 1);
                endGoTime = calendar.getTime();
                List<Map> ordersListMap = getOrdersListMap(goTime, endGoTime, organizer, zooSettlementDetail);
                if (ordersListMap != null && ordersListMap.size() > 0) {
                    map.put(DateUtil.convertDateToString(goTime, "yyyy-MM-dd HH:mm:ss"), ordersListMap);
                    lists.add(map);
                } else {
                    map.put(DateUtil.convertDateToString(goTime, "yyyy-MM-dd HH:mm:ss"), null);
                    lists.add(map);
                }
                goTime = endGoTime;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        ZooBalanceDepositIndemnityExample zooBalanceDepositIndemnityExample = new ZooBalanceDepositIndemnityExample();
        ZooBalanceDepositIndemnityExample.Criteria criteria = zooBalanceDepositIndemnityExample.createCriteria();
        criteria.andOrganizerIdEqualTo(organizer.getOrganizerId());
        criteria.andIndemnityTimeBetween(startDate, endDate);
        criteria.andPayTypeNotEqualTo(13);
        criteria.andPayTypeNotEqualTo(7);
        List<ZooBalanceDepositIndemnity> rows = zooBalanceDepositIndemnityService.selectByExampleForOffsetPage(zooBalanceDepositIndemnityExample, 0, 10000000);
        List<Map> maps = getIndemnityMaps(rows, zooSettlementDetail);

        String filename = organizer.getOrganizerId() + "_balanceOrders_" + DateUtil.getToday("yyyyMMddHHmmss") + ".xls";
        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        response.setHeader("Cache-Control", "no-cache");
        response.setHeader("Content-Disposition", "attachment;filename=" + new String(filename.getBytes("gbk"), "iso8859-1"));
        ExcelExportUtil.exportOrdersSXSSFExcel(lists, maps, response.getOutputStream(), filePath + "/balanceOrdersList");
        response.flushBuffer();
    }


    @ApiOperation(value = "导出产品统计详情列表Excell")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "startTime", value = "开始时间-(yyyy-MM-dd)", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "endTime", value = "结束时间-(yyyy-MM-dd)", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "status", value = "状态：1、线上，2、线下，3、不可退款", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "activityTypeId", value = "产品类型ID:0.最新活动; 1.滑雪度假;2.教练预约；3.超值票务；4.秒杀相关；5.训练营；6.自营票务；7.酒店；8.教练课程；9.储值卡；10.续时产品", required = false, dataType = "int", paramType = "query"),
    })
    @RequestMapping(value = "/export-detailStatistics", method = RequestMethod.GET)
    public void export(HttpServletResponse response, HttpServletRequest request,
                       @RequestParam(required = false, defaultValue = "0", value = "page") int page,
                       @RequestParam(required = false, defaultValue = "0", value = "size") int size,
                       @RequestParam(required = false, value = "status", defaultValue = "1") Integer status,
                       @RequestParam(required = false, value = "activityTypeId") Integer activityTypeId,
                       @RequestParam(required = false, value = "startTime") String startTime,
                       @RequestParam(required = false, value = "endTime") String endTime) throws IOException {
        Organizer organizer = WebUpmsContext.getOrgnization(request);

        List<Map> lists = getDetails(status, activityTypeId, startTime, endTime, organizer, page, size);
        String activityTypeName = ExcelExportUtil.convertActivityTypeIdToName(activityTypeId);
        String statusCh = "线上";
        if ("1".equals(String.valueOf(status))) {
            statusCh = "online";
        } else if ("2".equals(String.valueOf(status))) {
            statusCh = "offline";
        } else if ("3".equals(String.valueOf(status))) {
            statusCh = "refund";
        }
        for (int i = 0; i < lists.size(); i++) {
            lists.get(i).put("activityTypeName", activityTypeName);
        }
        String filename = statusCh + "_" + activityTypeId + "_" + organizer.getOrganizerId() + "_" + DateUtil.getToday("yyyyMMddHHmmss") + ".xls";
        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        response.setHeader("Content-disposition", "attachment;filename=" + new String(filename.getBytes("gbk"), "iso8859-1"));
        ExcelExportUtil.exportDetailSXSSFExcel(lists, response.getOutputStream(), filePath + "/detailStatistics", String.valueOf(activityTypeId));
        response.flushBuffer();
    }

    @ApiOperation(value = "导出招待列表Excell")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "size", value = "分页大小", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "trustees", value = "经办人姓名", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "trusteesPhone", value = "经办人电话", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "entertainPersonName", value = "招待人姓名", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "entertainPersonPhone", value = "招待人电话", required = false, dataType = "string", paramType = "query"),
    })
    //@RequiresPermissions("zoo:onAccountHangupDetail:read")
    @RequestMapping(value = "/export-entertainList", method = RequestMethod.GET)
    public void export(HttpServletResponse response, HttpServletRequest request,
                       @RequestParam(required = false, defaultValue = "0", value = "page") int page,
                       @RequestParam(required = false, defaultValue = "10", value = "size") int size,
                       @RequestParam(required = false, value = "startTime") String startTime,
                       @RequestParam(required = false, value = "endTime") String endTime,
                       @RequestParam(required = false, value = "entertainPersonName") String entertainPersonName,
                       @RequestParam(required = false, value = "trusteesPhone") String trusteesPhone,
                       @RequestParam(required = false, value = "entertainPersonPhone") String entertainPersonPhone,
                       @RequestParam(required = false, value = "trustees") String trustees) throws IOException {
        Organizer organizer = WebUpmsContext.getOrgnization(request);

        List<ZooEntertainDetail> zooEntertainDetails = getEntertaiinList(page, size, startTime, endTime, entertainPersonName, trusteesPhone, entertainPersonPhone, trustees, organizer);
        String filename = organizer.getOrganizerId() + "_" + DateUtil.getToday("yyyyMMddHHmmss") + ".xls";
        response.setContentType("application/vnd.ms-excel;charset=GB2312");
        response.setHeader("Content-disposition", "attachment;filename=" + new String(filename.getBytes("gbk"), "iso8859-1"));
        response.flushBuffer();
        ExcelExportUtil.exportEntertainSXSSFExcel(zooEntertainDetails, response.getOutputStream(), filePath + "/entertainList");
    }

    @ApiOperation(value = "导出招待主子订单列表Excell")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "size", value = "分页大小", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "trustees", value = "经办人姓名", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "trusteesPhone", value = "经办人电话", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "entertainPersonName", value = "招待人姓名", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "entertainPersonPhone", value = "招待人电话", required = false, dataType = "string", paramType = "query"),
    })
    //@RequiresPermissions("zoo:onAccountHangupDetail:read")
    @RequestMapping(value = "/export-entertainOrdersList", method = RequestMethod.GET)
    public void exportEntertainOrders(HttpServletResponse response, HttpServletRequest request,
                                      @RequestParam(required = false, defaultValue = "0", value = "page") int page,
                                      @RequestParam(required = false, defaultValue = "10", value = "size") int size,
                                      @RequestParam(required = false, value = "startTime") String startTime,
                                      @RequestParam(required = false, value = "endTime") String endTime,
                                      @RequestParam(required = false, value = "entertainPersonName") String entertainPersonName,
                                      @RequestParam(required = false, value = "trusteesPhone") String trusteesPhone,
                                      @RequestParam(required = false, value = "entertainPersonPhone") String entertainPersonPhone,
                                      @RequestParam(required = false, value = "trustees") String trustees) throws IOException {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        //主订单
        List<ZooEntertainDetail> zooEntertainDetails = getEntertaiinList(page, size, startTime, endTime, entertainPersonName, trusteesPhone, entertainPersonPhone, trustees, organizer);
        //主订单和子订单放入新的list中
        List<Map> maps = new ArrayList<Map>();
        if (zooEntertainDetails != null && zooEntertainDetails.size() > 0) {
            for (int i = 0; i < zooEntertainDetails.size(); i++) {
                ZooEntertainDetail zooEntertainDetail = zooEntertainDetails.get(i);
                Integer detailOrderId = zooEntertainDetail.getOrderId();
                Map map = new HashMap();
                map.put("titleName", "主订单");
                map.put("orderCode", zooEntertainDetail.getOrderCode());
                map.put("orderTime", DateUtil.convertDateToString(zooEntertainDetail.getOrderTime(), "yyyy-MM-dd HH:mm:ss"));
                map.put("activityName", zooEntertainDetail.getActivityName());
                map.put("num", zooEntertainDetail.getNum());
                map.put("money", zooEntertainDetail.getMoney());
                map.put("trustees", zooEntertainDetail.getTrustees());
                map.put("entertainPersonName", zooEntertainDetail.getEntertainPersonName());
                map.put("entertainPersonPhone", zooEntertainDetail.getEntertainPersonPhone());
                maps.add(map);
                //判断如果有子订单，则加入，如若订单id和主订单id相同，则不加
                ZooEntertainOrdersExample entertainOrdersExample = new ZooEntertainOrdersExample();
                entertainOrdersExample.createCriteria().andZooEntertainDetailIdEqualTo(zooEntertainDetail.getId());
                List<ZooEntertainOrders> zooEntertainOrders = zooEntertainOrdersService.selectByExample(entertainOrdersExample);
                if (zooEntertainOrders != null && zooEntertainOrders.size() > 0) {
                    for (int j = 0; j < zooEntertainOrders.size(); j++) {
                        ZooEntertainOrders zooEntertainOrder = zooEntertainOrders.get(j);
                        Integer orderId = zooEntertainOrder.getOrderId();
                        if (!detailOrderId.equals(orderId)) {
                            Map mapz = new HashMap();
                            mapz.put("titleName", "子订单");
                            mapz.put("orderCode", zooEntertainOrder.getOrderCode());
                            mapz.put("orderTime", DateUtil.convertDateToString(zooEntertainOrder.getOrderTime(), "yyyy-MM-dd HH:mm:ss"));
                            mapz.put("activityName", zooEntertainOrder.getActivityName());
                            mapz.put("num", zooEntertainOrder.getNum());
                            mapz.put("money", zooEntertainOrder.getMoney());
                            mapz.put("trustees", zooEntertainOrder.getTrustees());
                            mapz.put("entertainPersonName", zooEntertainOrder.getEntertainPersonName());
                            mapz.put("entertainPersonPhone", zooEntertainOrder.getEntertainPersonPhone());
                            maps.add(mapz);
                        }
                    }
                }
                Map mapk = new HashMap();
                mapk.put("titleName", "");
                mapk.put("orderCode", "");
                mapk.put("orderTime", "");
                mapk.put("activityName", "");
                mapk.put("num", "");
                mapk.put("money", "");
                mapk.put("trustees", "");
                mapk.put("entertainPersonName", "");
                mapk.put("entertainPersonPhone", "");
                maps.add(mapk);
            }
        }

        String filename = organizer.getOrganizerId() + "_" + DateUtil.getToday("yyyyMMddHHmmss") + ".xls";
        response.setContentType("application/vnd.ms-excel;charset=GB2312");
        response.setHeader("Content-disposition", "attachment;filename=" + new String(filename.getBytes("gbk"), "iso8859-1"));
        response.flushBuffer();
        ExcelExportUtil.exportEntertainOrdersExcel(maps, response.getOutputStream(), filePath + "/entertainList");
    }

    @ApiOperation(value = "导出挂账统计列表Excell")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "size", value = "分页大小", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "companyId", value = "挂账单位ID", required = false, dataType = "int", paramType = "query"),
    })
    @RequestMapping(value = "/export-hangupList", method = RequestMethod.GET)
    public void export(HttpServletResponse response, HttpServletRequest request,
                       @RequestParam(required = false, defaultValue = "0", value = "page") int page,
                       @RequestParam(required = false, defaultValue = "10", value = "size") int size,
                       @RequestParam(required = false, value = "companyId") Long companyId,
                       @RequestParam(required = false, value = "startTime") String startTime,
                       @RequestParam(required = false, value = "endTime") String endTime) throws IOException {
        Organizer organizer = WebUpmsContext.getOrgnization(request);

        if (StringUtils.isNotEmpty(startTime) && StringUtils.isNotEmpty(endTime)) {
            startTime = startTime + " 00:00:00";
            endTime = endTime + " 23:59:59";
        } else if (StringUtils.isNotEmpty(startTime) && StringUtils.isEmpty(endTime)) {
            startTime = startTime + " 00:00:00";
        } else if (StringUtils.isNotEmpty(endTime) && StringUtils.isEmpty(startTime)) {
            endTime = endTime + " 23:59:59";
        }

        List<Map> maps = zooOnAccountDayDetailService.selectGroupByCompanyId(organizer.getOrganizerId(), companyId, startTime, endTime, page, 0).getList();

        //订单详情
        ZooOnAccountOrdersExample example = new ZooOnAccountOrdersExample();
        ZooOnAccountOrdersExample.Criteria criteria = example.createCriteria();
        try {
            if (StringUtils.isNotEmpty(startTime) && StringUtils.isNotEmpty(endTime)) {
                criteria.andOrderTimeBetween(com.zoo.activity.util.DateUtil.convertStringToDate(startTime + " 00:00:00", "yyyy-MM-dd HH:mm:ss"), com.zoo.activity.util.DateUtil.convertStringToDate(endTime + " 23:59:59", "yyyy-MM-dd HH:mm:ss"));
            } else if (StringUtils.isNotEmpty(startTime) && StringUtils.isEmpty(endTime)) {
                criteria.andOrderTimeGreaterThanOrEqualTo(com.zoo.activity.util.DateUtil.convertStringToDate(startTime + " 00:00:00", "yyyy-MM-dd HH:mm:ss"));
            } else if (StringUtils.isNotEmpty(endTime) && StringUtils.isEmpty(startTime)) {
                criteria.andOrderTimeLessThanOrEqualTo(com.zoo.activity.util.DateUtil.convertStringToDate(endTime + " 00:00:00", "yyyy-MM-dd HH:mm:ss"));
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        criteria.andOrganizerIdEqualTo(organizer.getOrganizerId());
        example.setOrderByClause(" company_name desc, order_time desc ");
        List<ZooOnAccountOrders> ordersList = zooOnAccountOrdersService.selectByExample(example);
        List<Map> lists = new ArrayList<Map>();
        if (ordersList != null && ordersList.size() > 0) {
            for (int i = 0; i < ordersList.size(); i++) {
                Map map = new HashMap();
                ZooOnAccountOrders zooOnAccountOrders = ordersList.get(i);
                ZooOnAccountCompanyExample zooOnAccountCompanyExample = new ZooOnAccountCompanyExample();
                zooOnAccountCompanyExample.createCriteria().andIdEqualTo(Long.parseLong(String.valueOf(zooOnAccountOrders.getCompanyId())));
                List<ZooOnAccountCompany> zooOnAccountCompanys = zooOnAccountCompanyService.selectByExample(zooOnAccountCompanyExample);
                if (zooOnAccountCompanys != null && zooOnAccountCompanys.size() > 0) {
                    map.put("companyName", zooOnAccountCompanys.get(0).getCompanyName());
                }
                map.put("orderParentCode", zooOnAccountOrders.getOrderParentCode());
                map.put("orderTime", DateUtil.convertDateToString(zooOnAccountOrders.getOrderTime(), "yyyy-MM-dd HH:mm:ss"));
                map.put("hangupPersonName", zooOnAccountOrders.getHangupPersonName());
                map.put("hangupPersonPhone", zooOnAccountOrders.getHangupPersonPhone());
                map.put("trustees", zooOnAccountOrders.getTrustees());
                //map.put("trusteesPhone",zooOnAccountOrders.getTrusteesPhone());
                map.put("activityName", zooOnAccountOrders.getActivityName());
                map.put("episodeName", zooOnAccountOrders.getEpisodeName());
                map.put("chargeName", zooOnAccountOrders.getChargeName());
                map.put("num", zooOnAccountOrders.getNum());
                map.put("money", zooOnAccountOrders.getMoney());
                String operatorName = zooOnAccountOrders.getOperatorName();
                if (operatorName == null || operatorName.contains("小ZOO")) {
                    operatorName = "总管理员";
                }
                map.put("operatorName", operatorName);
                lists.add(map);
            }
        }

        String filename = organizer.getOrganizerId() + "_" + DateUtil.getToday("yyyyMMddHHmmss") + ".xls";
        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        response.setHeader("Content-disposition", "attachment;filename=" + new String(filename.getBytes("gbk"), "iso8859-1"));
        response.flushBuffer();
        ExcelExportUtil.exportHangupSXSSFExcel(lists, maps, response.getOutputStream(), filePath + "/hangupList");
    }


    @ApiOperation(value = "导出挂账详情Excell")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "size", value = "分页大小", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "companyId", value = "挂账单位ID", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "startTime", value = "统计开始时间（yyyy-MM-dd）", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "endTime", value = "统计结束时间（yyyy-MM-dd）", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "orderCode", value = "主订单编号", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "activityId", value = "产品ID", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "episodeId", value = "场次ID", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "hangupPersonName", value = "挂账人姓名", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "hangupPersonPhone", value = "挂账人手机号", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "trustees", value = "经办人", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "hangupStartTime", value = "挂账开始时间（yyyy-MM-dd HH:mm:ss）", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "hangupEndTime", value = "挂账结束时间（yyyy-MM-dd HH:mm:ss）", required = false, dataType = "string", paramType = "query"),
    })
    @RequestMapping(value = "/export-hangupDetail", method = RequestMethod.GET)
    public void hangupDetail(HttpServletResponse response, HttpServletRequest request,
                             @RequestParam(required = false, defaultValue = "0", value = "page") int page,
                             @RequestParam(required = false, defaultValue = "10", value = "size") int size,
                             @RequestParam(required = false, value = "companyId") Integer companyId,
                             @RequestParam(required = false, value = "orderCode") String orderCode,
                             @RequestParam(required = false, value = "activityId") Integer activityId,
                             @RequestParam(required = false, value = "episodeId") Integer episodeId,
                             @RequestParam(required = false, value = "hangupPersonName") String hangupPersonName,
                             @RequestParam(required = false, value = "hangupPersonPhone") String hangupPersonPhone,
                             @RequestParam(required = false, value = "trustees") String trustees,
                             @RequestParam(required = false, value = "startTime") String startTime,
                             @RequestParam(required = false, value = "endTime") String endTime,
                             @RequestParam(required = false, value = "hangupStartTime") String hangupStartTime,
                             @RequestParam(required = false, value = "hangupEndTime") String hangupEndTime) throws IOException {
        Organizer organizer = WebUpmsContext.getOrgnization(request);

        ZooOnAccountOrdersExample example = new ZooOnAccountOrdersExample();
        ZooOnAccountOrdersExample.Criteria criteria = example.createCriteria();
        try {
            if (StringUtils.isNotEmpty(startTime) && StringUtils.isNotEmpty(endTime)) {
                criteria.andOrderTimeBetween(com.zoo.activity.util.DateUtil.convertStringToDate(startTime + " 00:00:00", "yyyy-MM-dd HH:mm:ss"), com.zoo.activity.util.DateUtil.convertStringToDate(endTime + " 23:59:59", "yyyy-MM-dd HH:mm:ss"));
            } else if (StringUtils.isNotEmpty(startTime) && StringUtils.isEmpty(endTime)) {
                criteria.andOrderTimeGreaterThanOrEqualTo(com.zoo.activity.util.DateUtil.convertStringToDate(startTime + " 00:00:00", "yyyy-MM-dd HH:mm:ss"));
            } else if (StringUtils.isNotEmpty(endTime) && StringUtils.isEmpty(startTime)) {
                criteria.andOrderTimeLessThanOrEqualTo(com.zoo.activity.util.DateUtil.convertStringToDate(endTime + " 00:00:00", "yyyy-MM-dd HH:mm:ss"));
            }

            if (StringUtils.isNotEmpty(hangupStartTime) && StringUtils.isNotEmpty(hangupEndTime)) {
                criteria.andCreateTimeBetween(com.zoo.activity.util.DateUtil.convertStringToDate(hangupStartTime, "yyyy-MM-dd HH:mm:ss"), com.zoo.activity.util.DateUtil.convertStringToDate(hangupEndTime, "yyyy-MM-dd HH:mm:ss"));
            } else if (StringUtils.isNotEmpty(hangupStartTime) && StringUtils.isEmpty(hangupEndTime)) {
                criteria.andCreateTimeGreaterThanOrEqualTo(com.zoo.activity.util.DateUtil.convertStringToDate(hangupStartTime, "yyyy-MM-dd HH:mm:ss"));
            } else if (StringUtils.isNotEmpty(hangupEndTime) && StringUtils.isEmpty(hangupStartTime)) {
                criteria.andCreateTimeLessThanOrEqualTo(com.zoo.activity.util.DateUtil.convertStringToDate(hangupEndTime, "yyyy-MM-dd HH:mm:ss"));
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (companyId != null) {
            criteria.andCompanyIdEqualTo(companyId);
        }
        if (orderCode != null) {
            criteria.andOrderParentCodeEqualTo(orderCode);
        }
        if (activityId != null) {
            criteria.andActivityIdEqualTo(activityId);
        }
        if (episodeId != null) {
            criteria.andEpisodeIdEqualTo(episodeId);
        }
        if (hangupPersonName != null) {
            criteria.andHangupPersonNameEqualTo(hangupPersonName);
        }
        if (hangupPersonPhone != null) {
            criteria.andHangupPersonPhoneEqualTo(hangupPersonPhone);
        }
        if (trustees != null) {
            criteria.andTrusteesEqualTo(trustees);
        }
        criteria.andOrganizerIdEqualTo(organizer.getOrganizerId());
        example.setOrderByClause(" company_name desc, order_time desc ");
        List<ZooOnAccountOrders> ordersList = zooOnAccountOrdersService.selectByExample(example);
        List<Map> lists = new ArrayList<Map>();
        if (ordersList != null && ordersList.size() > 0) {
            for (int i = 0; i < ordersList.size(); i++) {
                Map map = new HashMap();
                ZooOnAccountOrders zooOnAccountOrders = ordersList.get(i);
                ZooOnAccountCompanyExample zooOnAccountCompanyExample = new ZooOnAccountCompanyExample();
                zooOnAccountCompanyExample.createCriteria().andIdEqualTo(Long.parseLong(String.valueOf(zooOnAccountOrders.getCompanyId())));
                List<ZooOnAccountCompany> zooOnAccountCompanys = zooOnAccountCompanyService.selectByExample(zooOnAccountCompanyExample);
                if (zooOnAccountCompanys != null && zooOnAccountCompanys.size() > 0) {
                    map.put("companyName", zooOnAccountCompanys.get(0).getCompanyName());
                }
                map.put("orderParentCode", zooOnAccountOrders.getOrderParentCode());
                map.put("orderTime", DateUtil.convertDateToString(zooOnAccountOrders.getOrderTime(), "yyyy-MM-dd HH:mm:ss"));
                map.put("hangupPersonName", zooOnAccountOrders.getHangupPersonName());
                map.put("hangupPersonPhone", zooOnAccountOrders.getHangupPersonPhone());
                map.put("trustees", zooOnAccountOrders.getTrustees());
                //map.put("trusteesPhone",zooOnAccountOrders.getTrusteesPhone());
                map.put("activityName", zooOnAccountOrders.getActivityName());
                map.put("episodeName", zooOnAccountOrders.getEpisodeName());
                map.put("chargeName", zooOnAccountOrders.getChargeName());
                map.put("num", zooOnAccountOrders.getNum());
                map.put("money", zooOnAccountOrders.getMoney());
                String operatorName = zooOnAccountOrders.getOperatorName();
                if (operatorName == null || operatorName.contains("小ZOO")) {
                    operatorName = "总管理员";
                }
                map.put("operatorName", operatorName);
                lists.add(map);
            }
        }

        String filename = organizer.getOrganizerId() + "_" + companyId + "_" + DateUtil.getToday("yyyyMMddHHmmss") + ".xls";
        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        response.setHeader("Content-disposition", "attachment;filename=" + new String(filename.getBytes("gbk"), "iso8859-1"));
        response.flushBuffer();
        ExcelExportUtil.exportHangupDetailExcel(lists, response.getOutputStream(), filePath + "/hangupDetail");
    }

    @ApiOperation(value = "导出消费记录Excell")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "startTime", value = "开始时间-(yyyy-MM-dd HH:mm:ss)", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "endTime", value = "结束时间-(yyyy-MM-dd HH:mm:ss)", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "cashierId", value = "收银员ID", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "cashierName", value = "收银员名称", required = false, dataType = "string", paramType = "query"),
    })
    @RequestMapping(value = "/consumelistExcell", method = RequestMethod.GET)
    public void consumelist(HttpServletResponse response, HttpServletRequest request,
                            @RequestParam(required = false, value = "startTime") String startTime,
                            @RequestParam(required = false, value = "endTime") String endTime,
                            @RequestParam(required = false, value = "cashierId") Integer cashierId,
                            @RequestParam(required = false, value = "cashierName") String cashierName) throws IOException {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        Integer orgId = organizer.getOrganizerId();
        Date startDate = null;
        Date endDate = null;
        try {
            if (startTime != null) {
                startDate = com.zoo.activity.util.DateUtil.convertStringToDate(startTime, "yyyy-MM-dd HH:mm:ss");
            }
            if (endTime != null) {
                endDate = com.zoo.activity.util.DateUtil.convertStringToDate(endTime, "yyyy-MM-dd HH:mm:ss");
            }
        } catch (ParseException e) {
            e.printStackTrace();
            startDate = null;
            endDate = null;
        }
        //产品统计
        List<Map<String, Object>> ordersB = ordersService.selectOrdersGroupByActivity(startDate, endDate, orgId, cashierId, cashierName);
        List<Map<String, Object>> orders = new ArrayList<Map<String, Object>>();
        //初始化
        orders.add(getProductInit("最新活动", "0"));
        orders.add(getProductInit("滑雪度假", "1"));
        orders.add(getProductInit("教练预约", "2"));
        orders.add(getProductInit("超值票务", "3"));
        orders.add(getProductInit("会员卡", "4"));
        orders.add(getProductInit("训练营", "5"));
        orders.add(getProductInit("自营票务", "6"));
        orders.add(getProductInit("酒店预定", "7"));
        orders.add(getProductInit("教练课程", "8"));
        orders.add(getProductInit("储值卡", "9"));
        orders.add(getProductInit("续时产品", "10"));
        if (ordersB != null && ordersB.size() > 0) {
            for (int i = 0; i < ordersB.size(); i++) {
                Map<String, Object> map = ordersB.get(i);
                String typeId = map.get("type_id").toString();
                ((List<Map<String, Object>>) orders.get(Integer.parseInt(typeId)).get("lists")).add(map);
            }
        }
        //租赁物统计
        List<Map<String, Object>> rentOrders = ordersService.selectRentOrdersGroupByActivity(startDate, endDate, orgId, cashierId, cashierName);

        //押金赔偿
        ZooBalanceDepositIndemnityExample indemnityExample = new ZooBalanceDepositIndemnityExample();
        ZooBalanceDepositIndemnityExample.Criteria indemnityExampleCriteria = indemnityExample.createCriteria();
        if (startDate != null) {
            indemnityExampleCriteria.andIndemnityTimeGreaterThanOrEqualTo(startDate);
        }
        if (endDate != null) {
            indemnityExampleCriteria.andIndemnityTimeLessThan(endDate);
        }
        if (cashierName != null) {
            indemnityExampleCriteria.andOperatorNameLike("%" + cashierName + "%");
        }
        if (cashierId != null) {
            indemnityExampleCriteria.andOperatorIdEqualTo(cashierId);
        }
        if (cashierName != null) {
            indemnityExampleCriteria.andOperatorNameEqualTo(cashierName);
        }
        //List<ZooBalanceDepositIndemnity> depositIndemnityList = zooBalanceDepositIndemnityService.selectByExample(indemnityExample);
        List<Map> depositPayOrders = zooBalanceDepositIndemnityService.selectStatisticByExample(indemnityExample);

        //押金未退
        List<Map<String, Object>> depositUnRefundOrders = ordersService.selectTotalMoneyFromDeposit(startDate, endDate, orgId, cashierId, cashierName);

        List<Map<String, Object>> exceptionOrders = new ArrayList<Map<String, Object>>();
        if (depositPayOrders != null && depositPayOrders.size() > 0) {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("name", "押金赔偿");
            map.put("totalMoney", depositPayOrders.get(0).get("indemnity"));
            exceptionOrders.add(map);
        } else {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("name", "押金赔偿");
            map.put("totalMoney", 0);
            exceptionOrders.add(map);
        }
        if (depositUnRefundOrders != null && depositUnRefundOrders.size() > 0) {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("name", "未退押金");
            map.put("totalMoney", depositUnRefundOrders.get(0).get("totalMoney"));
            exceptionOrders.add(map);
        } else {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("name", "未退押金");
            map.put("totalMoney", 0);
            exceptionOrders.add(map);
        }
        List<Map> ordersDistributionStatictis = ordersService.getOrdersPayStatictis(startDate, endDate, 100, cashierName, cashierId, orgId);
        List<Map> ordersPayStatictis = ordersService.getOrdersPayStatictis(startDate, endDate, null, cashierName, cashierId, orgId);
        //组装线上支付方式List
        List<Map> payOrders = new ArrayList<Map>();
        Map<String, Object> resultMap = new HashMap<String, Object>();

        List<Map<String, Object>> onlineList = new ArrayList<Map<String, Object>>();
        onlineList.add(getinitPayMap("线上微信", BigDecimal.ZERO));
        onlineList.add(getinitPayMap("线上储值支付", BigDecimal.ZERO));
        onlineList.add(getinitPayMap("分销商支付", BigDecimal.ZERO));
        List<Map<String, Object>> offlineList = new ArrayList<Map<String, Object>>();
        offlineList.add(getinitPayMap("线下现金", BigDecimal.ZERO));
        offlineList.add(getinitPayMap("线下支付宝", BigDecimal.ZERO));
        offlineList.add(getinitPayMap("线下刷卡", BigDecimal.ZERO));
        offlineList.add(getinitPayMap("线下微信", BigDecimal.ZERO));
        offlineList.add(getinitPayMap("线下预售", BigDecimal.ZERO));
        //offlineList.add(getinitPayMap("线下挂账", BigDecimal.ZERO));
        offlineList.add(getinitPayMap("线下储值支付", BigDecimal.ZERO));
        offlineList.add(getinitPayMap("线下挂账支付", BigDecimal.ZERO));
        offlineList.add(getinitPayMap("线下招待支付", BigDecimal.ZERO));


        BigDecimal onlineMoney = BigDecimal.ZERO;
        BigDecimal offlineMoney = BigDecimal.ZERO;
        BigDecimal totalMoney = BigDecimal.ZERO;
        if (ordersPayStatictis != null && ordersPayStatictis.size() > 0) {
            for (int i = 0; i < ordersPayStatictis.size(); i++) {
                if (ordersPayStatictis.get(i).get("pay_type_id") == null) {
                    continue;
                }
                String payType = ordersPayStatictis.get(i).get("pay_type_id").toString();
                BigDecimal money = (BigDecimal) ordersPayStatictis.get(i).get("totalMoney");
                money = money.setScale(2, BigDecimal.ROUND_UP);
                if ("1".equals(payType)) {//   有问题
                    onlineList.get(0).put("money", money);
                    onlineMoney = onlineMoney.add(money).setScale(2, BigDecimal.ROUND_UP);
                }
                if ("7".equals(payType)) {
                    onlineList.get(1).put("money", money);
                    onlineMoney = onlineMoney.add(money).setScale(2, BigDecimal.ROUND_UP);
                }


                if ("0".equals(payType)) {
                    offlineList.get(0).put("money", money);
                    offlineMoney = offlineMoney.add(money).setScale(2, BigDecimal.ROUND_UP);
                }
                if ("2".equals(payType)) {
                    offlineList.get(1).put("money", money);
                    offlineMoney = offlineMoney.add(money).setScale(2, BigDecimal.ROUND_UP);
                }
                if ("3".equals(payType)) {
                    offlineList.get(2).put("money", money);
                    offlineMoney = offlineMoney.add(money).setScale(2, BigDecimal.ROUND_UP);
                }
                if ("4".equals(payType)) {
                    offlineList.get(3).put("money", money);
                    offlineMoney = offlineMoney.add(money).setScale(2, BigDecimal.ROUND_UP);
                }
                if ("5".equals(payType)) {
                    offlineList.get(4).put("money", money);
                    offlineMoney = offlineMoney.add(money).setScale(2, BigDecimal.ROUND_UP);
                }
//                if("6".equals(payType)){
//                    offlineList.get(5).put("money",money);
//                    offlineMoney=offlineMoney.add(money);
//                }
                if ("8".equals(payType)) {
                    offlineList.get(5).put("money", money);
                    offlineMoney = offlineMoney.add(money);
                }
                if ("9".equals(payType)) {
                    offlineList.get(6).put("money", money);
                    offlineMoney = offlineMoney.add(money);
                }
                if ("10".equals(payType)) {
                    offlineList.get(7).put("money", money);
                    offlineMoney = offlineMoney.add(money);
                }
            }

        }
        if (ordersDistributionStatictis != null && ordersDistributionStatictis.size() > 0) {
            BigDecimal money = (BigDecimal) ordersDistributionStatictis.get(0).get("totalMoney");
            money = money.setScale(2, BigDecimal.ROUND_UP);
            onlineList.get(2).put("money", money);
            onlineMoney = onlineMoney.add(money);
        }

        totalMoney = totalMoney.add(onlineMoney);
        totalMoney = totalMoney.add(offlineMoney).setScale(2, BigDecimal.ROUND_UP);
        ;
        Map<String, Object> onlineMap = new HashMap<String, Object>();
        Map<String, Object> offlineMap = new HashMap<String, Object>();
        onlineMap.put("total", onlineMoney);
        onlineMap.put("list", onlineList);
        offlineMap.put("total", offlineMoney);
        offlineMap.put("list", offlineList);
        resultMap.put("online", onlineMap);
        resultMap.put("offline", offlineMap);
        resultMap.put("totalMoney", totalMoney);
        payOrders.add(resultMap);

        String filename = organizer.getOrganizerId() + "_consume_" + DateUtil.getToday("yyyyMMddHHmmss") + ".xls";
        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        response.setHeader("Content-disposition", "attachment;filename=" + new String(filename.getBytes("gbk"), "iso8859-1"));
        response.flushBuffer();
        ExcelExportUtil.exportConsumelistExcel(orders, rentOrders, exceptionOrders, payOrders, response.getOutputStream(), filePath + "/hangupDetail");
    }

    private Map getProductInit(String typeName, String typeId) {
        //0	最新活动-----指企业举办的相关活动（比如：举办滑雪锦标赛等），最新活动没有入园凭证，不需要生成二维码，产品自动核销
        //* 1	滑雪度假
        //* 2	教练预约：只能单次预约，不指定教练
        //* 3	超值票务
        //* 4	装备秒杀
        //* 5	训练营
        //* 6	自营票务
        //* 7	酒店预定
        //* 8	教练课程——(课时卡多次、课程单次)，课程与教练绑定
        //* 9	储值卡
        //* 10	续时产品
        Map initMap = new HashMap();
        initMap.put("type_id", typeId);
        initMap.put("typeName", typeName);
        initMap.put("lists", new ArrayList());
        return initMap;
    }

    private Map<String, Object> getinitPayMap(String payTypeStr, BigDecimal payMoney) {
        Map<String, Object> inintMap = new HashMap<String, Object>();
        inintMap.put("name", payTypeStr);
        inintMap.put("money", payMoney);
        return inintMap;
    }

    private String getTypeName(String typeId) {
        //* 0	最新活动
        //* 1	滑雪度假
        //* 2	教练预约
        //* 3	超值票务
        //* 4	装备秒杀
        //* 5	训练营
        //* 6	自营票务
        //* 7	酒店预定
        //* 8	教练课程
        //* 9	储值卡
        //* 10	续时产品
        String typeName = "";
        switch (typeId) {
            case "0":
                typeName = "最新活动";
                break;
            case "1":
                typeName = "滑雪度假";
                break;
            case "2":
                typeName = "教练预约";
                break;
            case "3":
                typeName = "超值票务";
                break;
            case "4":
                typeName = "会员卡";
                break;
            case "5":
                typeName = "训练营";
                break;
            case "6":
                typeName = "自营票务";
                break;
            case "7":
                typeName = "酒店预定";
                break;
            case "8":
                typeName = "教练课程";
                break;
            case "9":
                typeName = "储值卡";
                break;
            case "10":
                typeName = "续时产品";
                break;
            default:
                typeName = "";
                break;
        }
        return typeName;
    }

    private List<ZooEntertainDetail> getEntertaiinList(@RequestParam(required = false, defaultValue = "0", value = "page") int page, @RequestParam(required = false, defaultValue = "10", value = "size") int size, @RequestParam(required = false, value = "startTime") String startTime, @RequestParam(required = false, value = "endTime") String endTime, @RequestParam(required = false, value = "entertainPersonName") String entertainPersonName, @RequestParam(required = false, value = "trusteesPhone") String trusteesPhone, @RequestParam(required = false, value = "entertainPersonPhone") String entertainPersonPhone, @RequestParam(required = false, value = "trustees") String trustees, Organizer organizer) {
        ZooEntertainDetailExample example = new ZooEntertainDetailExample();
        ZooEntertainDetailExample.Criteria criteria = example.createCriteria();
        try {
            if (StringUtils.isNotEmpty(startTime) && StringUtils.isNotEmpty(endTime)) {
                criteria.andOrderTimeBetween(com.zoo.activity.util.DateUtil.convertStringToDate(startTime + " 00:00:00", "yyyy-MM-dd HH:mm:ss"), com.zoo.activity.util.DateUtil.convertStringToDate(endTime + " 23:59:59", "yyyy-MM-dd HH:mm:ss"));
            } else if (StringUtils.isNotEmpty(startTime) && StringUtils.isEmpty(endTime)) {
                criteria.andOrderTimeGreaterThanOrEqualTo(com.zoo.activity.util.DateUtil.convertStringToDate(startTime + " 00:00:00", "yyyy-MM-dd HH:mm:ss"));
            } else if (StringUtils.isNotEmpty(endTime) && StringUtils.isEmpty(startTime)) {
                criteria.andOrderTimeLessThanOrEqualTo(com.zoo.activity.util.DateUtil.convertStringToDate(endTime + " 00:00:00", "yyyy-MM-dd HH:mm:ss"));
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        criteria.andOrderCodeIsNotNull();

        if (StringUtils.isNotEmpty(entertainPersonName)) {
            criteria.andEntertainPersonNameLike("%" + entertainPersonName + "%");
        }
        if (StringUtils.isNotEmpty(trusteesPhone)) {
            criteria.andTrusteesEqualTo(trusteesPhone);
        }
        if (StringUtils.isNotEmpty(entertainPersonPhone)) {
            criteria.andEntertainPersonPhoneEqualTo(entertainPersonPhone);
        }
        if (StringUtils.isNotEmpty(trustees)) {
            criteria.andTrusteesLike("%" + trustees + "%");
        }

        criteria.andOrganizerIdEqualTo(organizer.getOrganizerId());

        List<ZooEntertainDetail> list = zooEntertainDetailService.selectByExampleForStartPage(example, page, size);
        return list;
    }

    private List<Map> getDetails(@RequestParam(required = false, value = "status", defaultValue = "1") Integer status, @RequestParam(required = false, value = "activityTypeId") Integer activityTypeId, @RequestParam(required = false, value = "startTime") String startTime, @RequestParam(required = false, value = "endTime") String endTime, Organizer organizer, int page, int size) {
        ZooBalanceStatisticsExample example = new ZooBalanceStatisticsExample();
        ZooBalanceStatisticsExample.Criteria criteria = example.createCriteria();
        try {
            startTime = formartDate(startTime);
            endTime = formartDate(endTime);
            if (StringUtils.isNotEmpty(startTime) && StringUtils.isNotEmpty(endTime)) {
                criteria.andDayBetween(com.zoo.activity.util.DateUtil.convertStringToDate(startTime + " 00:00:00", "yyyy-MM-dd HH:mm:ss"), com.zoo.activity.util.DateUtil.convertStringToDate(endTime + " 23:59:59", "yyyy-MM-dd HH:mm:ss"));
            } else if (StringUtils.isNotEmpty(startTime) && StringUtils.isEmpty(endTime)) {
                criteria.andDayGreaterThanOrEqualTo(com.zoo.activity.util.DateUtil.convertStringToDate(startTime + " 00:00:00", "yyyy-MM-dd HH:mm:ss"));
            } else if (StringUtils.isNotEmpty(endTime) && StringUtils.isEmpty(startTime)) {
                criteria.andDayLessThanOrEqualTo(com.zoo.activity.util.DateUtil.convertStringToDate(endTime + " 00:00:00", "yyyy-MM-dd HH:mm:ss"));
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (activityTypeId != null) {
            criteria.andActivityTypeIdEqualTo(activityTypeId);
        }
        if (status == 1) {
            //线上
            criteria.andLineOrOfflineNotEqualTo(2);
            criteria.andStatusRefundEqualTo(1);
        } else if (status == 2) {
            //线下
            criteria.andLineOrOfflineEqualTo(2);
        } else if (status == 3) {
            //不可退款（只有线上订单才计算不可退款）
            criteria.andLineOrOfflineNotEqualTo(2);
            criteria.andStatusRefundEqualTo(0);
        }

        criteria.andOrganizerIdEqualTo(organizer.getOrganizerId());
        criteria.andEquipmentIdIsNull();
        List<Map> list = zooBalanceStatisticsService.selectDetailStatisticsByExample(example, page, size).getList();
        return list;
    }

    private List<Map> getIndemnityMaps(List<ZooBalanceDepositIndemnity> rows, ZooSettlementDetail zooSettlementDetail) {
        String payWayRate = zooSettlementDetail.getPayWayRate();
        JSONArray jsonArray = new JSONArray(payWayRate);
        List<Map> maps = new ArrayList<Map>();
        if (rows != null && rows.size() > 0) {
            for (int i = 0; i < rows.size(); i++) {
                ZooBalanceDepositIndemnity obj = rows.get(i);
                // BigDecimal depositAmount = obj.getDepositAmount();
                BigDecimal indemnity = obj.getIndemnity();
                Boolean isGo = true;

                Map map = new HashMap();
                map.put("orderCode", obj.getOrderCode());
                map.put("orderParentCode", obj.getOrderParentCode());
                map.put("equipmentName", obj.getEquipmentName());
                map.put("indemnityTime", DateUtil.convertDateToString(obj.getIndemnityTime(), "yyyy-MM-dd HH:mm:ss"));
                map.put("rentStatusName", getRentStatusName(obj.getRentStatus()));
                map.put("depositAmount", obj.getDepositAmount());
                map.put("indemnity", obj.getIndemnity());
                if ("1".equals(String.valueOf(obj.getLineOrOffline()))) {
                    map.put("lineOrOffline", "线上");
                } else if ("2".equals(String.valueOf(obj.getLineOrOffline()))) {
                    map.put("lineOrOffline", "线下");
                } else if ("3".equals(String.valueOf(obj.getLineOrOffline()))) {
                    map.put("lineOrOffline", "OTA");
                } else {
                    map.put("lineOrOffline", "");
                }
                map.put("sysPayType", obj.getSysPayTypeName());

                for (int l = 0; l < jsonArray.length(); l++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(l);
                    BigDecimal channelRate = jsonObject.getBigDecimal("channelRate");
                    BigDecimal platformRate = jsonObject.getBigDecimal("platformRate");

                    if ("1".equals(String.valueOf(obj.getLineOrOffline()))) {
                        if (jsonObject.getString("payWayValue").equals(obj.getSysPayWayValue())) {
                            isGo = false;
                            //平台服务费
                            BigDecimal orderOnePTFuwuMoney = indemnity.multiply(platformRate).setScale(2, BigDecimal.ROUND_UP);//上浮
                            //单个订单支付通道服务费
                            BigDecimal orderOneTDFuwuMoney = indemnity.multiply(channelRate).setScale(2, BigDecimal.ROUND_UP);//上浮
                            //单个订单服务费
                            BigDecimal orderOneFuwuMoney = orderOnePTFuwuMoney.add(orderOneTDFuwuMoney);
                            //平台服务费
                            map.put("orderOnePTFuwuMoney", orderOnePTFuwuMoney);
                            //通道服务费
                            map.put("orderOneTDFuwuMoney", orderOneTDFuwuMoney);
                            //总服务费
                            map.put("orderOneFuwuMoney", orderOneFuwuMoney);
                            break;
                        }
                    }
                }
                if (isGo) {
                    /*Map map = new HashMap();
                    map.put("orderCode", obj.getOrderCode());
                    map.put("orderParentCode", obj.getOrderParentCode());
                    map.put("equipmentName", obj.getEquipmentName());
                    map.put("indemnityTime", DateUtil.convertDateToString(obj.getIndemnityTime(), "yyyy-MM-dd HH:mm:ss"));
                    map.put("rentStatusName", getRentStatusName(obj.getRentStatus()));
                    map.put("depositAmount", obj.getDepositAmount());
                    map.put("indemnity", obj.getIndemnity());
                    if ("1".equals(String.valueOf(obj.getLineOrOffline()))) {
                        map.put("lineOrOffline", "线上");
                    } else if ("2".equals(String.valueOf(obj.getLineOrOffline()))) {
                        map.put("lineOrOffline", "线下");
                    } else if ("3".equals(String.valueOf(obj.getLineOrOffline()))) {
                        map.put("lineOrOffline", "OTA");
                    } else {
                        map.put("lineOrOffline", "");
                    }
                    map.put("sysPayType", obj.getSysPayTypeName());
                    isGo = false;*/
                    //平台服务费
                    map.put("orderOnePTFuwuMoney", BigDecimal.ZERO);
                    //通道服务费
                    map.put("orderOneTDFuwuMoney", BigDecimal.ZERO);
                    //总服务费
                    map.put("orderOneFuwuMoney", BigDecimal.ZERO);
                }
                maps.add(map);
            }
        }
        return maps;
    }

    private String getRentStatusName(int status){
        //租赁状态（0待租赁 1待归还 2已归还 3暂存 4超时 5损坏）
        switch (status){
            case 0: return "待租赁";
            case 1: return "赔偿";
            case 2: return "已归还";
            case 3: return "暂存";
            case 4: return "超时";
            case 5: return "损坏";
        }
        return "";
    }

    private List<Map> getOrdersListMap(@RequestParam(required = false, value = "startTime") Date startTime, @RequestParam(required = false, value = "endTime") Date endTime, Organizer organizer, ZooSettlementDetail zooSettlementDetail) {
        String payWayRate = zooSettlementDetail.getPayWayRate();
        JSONArray jsonArray = new JSONArray(payWayRate);

        ZooBalanceOrdersExample zooBalanceOrdersExample = new ZooBalanceOrdersExample();
        ZooBalanceOrdersExample.Criteria criteria = zooBalanceOrdersExample.createCriteria();
        criteria.andVerifyTimeBetween(startTime, endTime);
        criteria.andOrganizerIdEqualTo(organizer.getOrganizerId());
        criteria.andPayTypeNotEqualTo(13);
        criteria.andPayTypeNotEqualTo(7);
        List<ZooBalanceOrders> orders = zooBalanceOrdersService.selectByExample(zooBalanceOrdersExample);
        List<Map> lists = new ArrayList<Map>();
        if (orders != null && orders.size() > 0) {
            for (int i = 0; i < orders.size(); i++) {
                ZooBalanceOrders obj = orders.get(i);
                Boolean isGo = true;
                for (int l = 0; l < jsonArray.length(); l++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(l);
                    BigDecimal channelRate = jsonObject.getBigDecimal("channelRate");
                    BigDecimal platformRate = jsonObject.getBigDecimal("platformRate");
                    BigDecimal rate = channelRate.add(platformRate);

                    Map map = new HashMap();
                    map.put("orderCode", obj.getOrderCode());
                    map.put("verifyTime", DateUtil.convertDateToString(obj.getVerifyTime(), "yyyy-MM-dd HH:mm:ss"));
                    map.put("activityType", ExcelExportUtil.convertActivityTypeIdToName(obj.getActivityTypeId()));
                    map.put("activityTitle", obj.getActivityTitle());
                    map.put("episodeTitle", obj.getEpisodeTitle());
                    map.put("chargeTitle", obj.getChargeTitle());
                    map.put("chargePrice", obj.getChargePrice());
                    map.put("verifyOrderNum", obj.getVerifyOrderNum());
                    map.put("verifyTotalPrice", obj.getVerifyTotalPrice());
                    map.put("statusRefund", "0".equals(String.valueOf(obj.getStatusRefund())) ? "不可退款" : "可退款");
                    String lineOrOffline = String.valueOf(obj.getLineOrOffline());
                    if ("1".equals(lineOrOffline)) {
                        map.put("lineOrOffline", "线上支付");
                    } else if ("2".equals(lineOrOffline)) {
                        map.put("lineOrOffline", "线下支付");
                    } else if ("3".equals(lineOrOffline)) {
                        map.put("lineOrOffline", "OTA");
                    } else {
                        map.put("lineOrOffline", "");
                    }
                    map.put("payType", obj.getSysPayTypeName());
                    if (obj.getEquipmentId() == null) {
                        map.put("isRent", "非租赁物");
                        map.put("episodeTitle", "---");
                    } else {
                        map.put("isRent", "租赁物");
                        map.put("episodeTitle", obj.getEpisodeTitle());
                    }
                    BigDecimal chargePrice = obj.getChargePrice();//单价
                    int verifyOrderNum = obj.getVerifyOrderNum();//条数
                    BigDecimal orderOneMoney = chargePrice.multiply(new BigDecimal(verifyOrderNum));
                    if (jsonObject.getString("payWayValue").equals(obj.getSysPayWayValue())) {
                        //如果没进过此判断，则需要将本条订单记录中服务费存储为###
                        isGo = false;
                        //平台服务费
                        BigDecimal orderOnePTFuwuMoney = orderOneMoney.multiply(platformRate).setScale(2, BigDecimal.ROUND_UP);//上浮
                        //单个订单支付通道服务费
                        BigDecimal orderOneTDFuwuMoney = orderOneMoney.multiply(channelRate).setScale(2, BigDecimal.ROUND_UP);//上浮
                        //单个订单服务费
                        BigDecimal orderOneFuwuMoney = orderOnePTFuwuMoney.add(orderOneTDFuwuMoney);
                        //平台服务费
                        map.put("orderOnePTFuwuMoney", orderOnePTFuwuMoney);
                        //通道服务费
                        map.put("orderOneTDFuwuMoney", orderOneTDFuwuMoney);
                        //总服务费
                        map.put("orderOneFuwuMoney", orderOneFuwuMoney);
                        lists.add(map);
                    }

                }
                if (isGo) {
                    Map map = new HashMap();
                    map.put("orderCode", obj.getOrderCode());
                    map.put("verifyTime", obj.getVerifyTime());
                    map.put("activityTitle", obj.getActivityTitle());
                    map.put("episodeTitle", obj.getEpisodeTitle());
                    map.put("chargeTitle", obj.getChargeTitle());
                    map.put("chargePrice", obj.getChargePrice());
                    map.put("verifyOrderNum", obj.getVerifyOrderNum());
                    map.put("verifyTotalPrice", obj.getVerifyTotalPrice());
                    map.put("statusRefund", "0".equals(String.valueOf(obj.getStatusRefund())) ? "不可退款" : "可退款");
                    String lineOrOffline = String.valueOf(obj.getLineOrOffline());
                    if ("1".equals(lineOrOffline)) {
                        map.put("lineOrOffline", "线上支付");
                    } else if ("2".equals(lineOrOffline)) {
                        map.put("lineOrOffline", "线下支付");
                    } else if ("3".equals(lineOrOffline)) {
                        map.put("lineOrOffline", "OTA");
                    } else {
                        map.put("lineOrOffline", "");
                    }
                    map.put("payType", obj.getSysPayTypeName());
                    if (obj.getEquipmentId() == null) {
                        map.put("isRent", "非租赁物");
                        map.put("episodeTitle", "---");
                    } else {
                        map.put("isRent", "租赁物");
                        map.put("episodeTitle", obj.getEpisodeTitle());
                    }
                    //平台服务费
                    map.put("orderOnePTFuwuMoney", BigDecimal.ZERO);
                    //通道服务费
                    map.put("orderOneTDFuwuMoney", BigDecimal.ZERO);
                    //总服务费
                    map.put("orderOneFuwuMoney", BigDecimal.ZERO);
                    lists.add(map);
                }
            }
        }
        return lists;
    }

    private Map<String, List<Map>> getStringListMap(@RequestParam(required = false, value = "status", defaultValue = "1") Integer status, @RequestParam(required = false, value = "startTime") String startTime, @RequestParam(required = false, value = "endTime") String endTime, Organizer organizer) {
        startTime = formartDate(startTime);
        endTime = formartDate(endTime);

        ZooBalanceStatisticsExample activityExample = new ZooBalanceStatisticsExample();
        ZooBalanceStatisticsExample.Criteria activityExampleCriteria = createActivityExampleCriteria(activityExample, startTime, endTime, status, organizer);
        List<Integer> activityTypeIds = new ArrayList<Integer>();
        activityTypeIds.add(2);
        activityTypeIds.add(8);

        /**查询产品统计**/
        activityExampleCriteria.andActivityTypeIdNotIn(activityTypeIds);
        activityExampleCriteria.andEquipmentIdIsNull();
        List<Map> prodMaps = zooBalanceStatisticsService.selectStatisticsGroupActivityTypeIdByExample(activityExample);

        /**滑雪学校产品统计**/
        ZooBalanceStatisticsExample coachExample = new ZooBalanceStatisticsExample();
        ZooBalanceStatisticsExample.Criteria coachExampleCriteria = createActivityExampleCriteria(coachExample, startTime, endTime, status, organizer);
        coachExampleCriteria.andActivityTypeIdIn(activityTypeIds);
        coachExampleCriteria.andEquipmentIdIsNull();
        List<Map> schoolMaps = zooBalanceStatisticsService.selectStatisticsGroupActivityTypeIdByExample(coachExample);

        /**租赁物统计**/
        ZooRentExceptionExample zooRentExceptionExample = new ZooRentExceptionExample();
        ZooRentExceptionExample.Criteria zooRentExceptionExampleCriteria = zooRentExceptionExample.createCriteria();
        zooRentExceptionExampleCriteria.andOrganizerIdEqualTo(organizer.getOrganizerId());
        try {
            if (StringUtils.isNotEmpty(startTime) && StringUtils.isNotEmpty(endTime)) {
                zooRentExceptionExampleCriteria.andDayBetween(com.zoo.activity.util.DateUtil.convertStringToDate(startTime + " 00:00:00", "yyyy-MM-dd HH:mm:ss"), com.zoo.activity.util.DateUtil.convertStringToDate(endTime + " 23:59:59", "yyyy-MM-dd HH:mm:ss"));
            } else if (StringUtils.isNotEmpty(startTime) && StringUtils.isEmpty(endTime)) {
                zooRentExceptionExampleCriteria.andDayGreaterThanOrEqualTo(com.zoo.activity.util.DateUtil.convertStringToDate(startTime + " 00:00:00", "yyyy-MM-dd HH:mm:ss"));
            } else if (StringUtils.isNotEmpty(endTime) && StringUtils.isEmpty(startTime)) {
                zooRentExceptionExampleCriteria.andDayLessThanOrEqualTo(com.zoo.activity.util.DateUtil.convertStringToDate(endTime + " 00:00:00", "yyyy-MM-dd HH:mm:ss"));
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (status == 1) {
            //线上
            zooRentExceptionExampleCriteria.andLineOrOfflineNotEqualTo(2);
            zooRentExceptionExampleCriteria.andStatusRefundEqualTo(1);
        } else if (status == 2) {
            //线下
            zooRentExceptionExampleCriteria.andLineOrOfflineEqualTo(2);
        } else if (status == 3) {
            //不可退款（只有线上订单才计算不可退款）
            zooRentExceptionExampleCriteria.andLineOrOfflineNotEqualTo(2);
            zooRentExceptionExampleCriteria.andStatusRefundEqualTo(0);
        }
        List<Map> rentMaps = zooRentExceptionService.selectStatisticByExample(zooRentExceptionExample);

        /**挂帐**/
        ZooOnAccountDayDetailExample dayDetailExample = new ZooOnAccountDayDetailExample();
        ZooOnAccountDayDetailExample.Criteria zooOnAccountDayDetailExampleCriteria = dayDetailExample.createCriteria();
        zooOnAccountDayDetailExampleCriteria.andOrganizerIdEqualTo(organizer.getOrganizerId());
        try {
            if (StringUtils.isNotEmpty(startTime) && StringUtils.isNotEmpty(endTime)) {
                zooOnAccountDayDetailExampleCriteria.andDayBetween(com.zoo.activity.util.DateUtil.convertStringToDate(startTime + " 00:00:00", "yyyy-MM-dd HH:mm:ss"), com.zoo.activity.util.DateUtil.convertStringToDate(endTime + " 23:59:59", "yyyy-MM-dd HH:mm:ss"));
            } else if (StringUtils.isNotEmpty(startTime) && StringUtils.isEmpty(endTime)) {
                zooOnAccountDayDetailExampleCriteria.andDayGreaterThanOrEqualTo(com.zoo.activity.util.DateUtil.convertStringToDate(startTime + " 00:00:00", "yyyy-MM-dd HH:mm:ss"));
            } else if (StringUtils.isNotEmpty(endTime) && StringUtils.isEmpty(startTime)) {
                zooOnAccountDayDetailExampleCriteria.andDayLessThanOrEqualTo(com.zoo.activity.util.DateUtil.convertStringToDate(endTime + " 00:00:00", "yyyy-MM-dd HH:mm:ss"));
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        List<Map> hungUpMaps = zooOnAccountDayDetailService.selectStatisticByExample(dayDetailExample);

        /**招待**/
        ZooEntertainOrdersExample ordersExample = new ZooEntertainOrdersExample();
        ZooEntertainOrdersExample.Criteria ordersExampleCriteria = ordersExample.createCriteria();
        ordersExampleCriteria.andOrganizerIdEqualTo(organizer.getOrganizerId());
        try {
            if (StringUtils.isNotEmpty(startTime) && StringUtils.isNotEmpty(endTime)) {
                ordersExampleCriteria.andOrderTimeBetween(com.zoo.activity.util.DateUtil.convertStringToDate(startTime + " 00:00:00", "yyyy-MM-dd HH:mm:ss"), com.zoo.activity.util.DateUtil.convertStringToDate(endTime + " 23:59:59", "yyyy-MM-dd HH:mm:ss"));
            } else if (StringUtils.isNotEmpty(startTime) && StringUtils.isEmpty(endTime)) {
                ordersExampleCriteria.andOrderTimeGreaterThanOrEqualTo(com.zoo.activity.util.DateUtil.convertStringToDate(startTime + " 00:00:00", "yyyy-MM-dd HH:mm:ss"));
            } else if (StringUtils.isNotEmpty(endTime) && StringUtils.isEmpty(startTime)) {
                ordersExampleCriteria.andOrderTimeLessThanOrEqualTo(com.zoo.activity.util.DateUtil.convertStringToDate(endTime + " 00:00:00", "yyyy-MM-dd HH:mm:ss"));
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        List<Map> entertainMaps = zooEntertainOrdersService.selectStatisticByExample(ordersExample);

        /**渠道（ota）**/
        ZooOtaStatisticsExample otaStatisticsExample = new ZooOtaStatisticsExample();
        ZooOtaStatisticsExample.Criteria otaStatisticsExampleCriteria = otaStatisticsExample.createCriteria();
        otaStatisticsExampleCriteria.andOrganizerIdEqualTo(organizer.getOrganizerId());
        try {
            if (StringUtils.isNotEmpty(startTime) && StringUtils.isNotEmpty(endTime)) {
                otaStatisticsExampleCriteria.andDayBetween(com.zoo.activity.util.DateUtil.convertStringToDate(startTime + " 00:00:00", "yyyy-MM-dd HH:mm:ss"), com.zoo.activity.util.DateUtil.convertStringToDate(endTime + " 23:59:59", "yyyy-MM-dd HH:mm:ss"));
            } else if (StringUtils.isNotEmpty(startTime) && StringUtils.isEmpty(endTime)) {
                otaStatisticsExampleCriteria.andDayGreaterThanOrEqualTo(com.zoo.activity.util.DateUtil.convertStringToDate(startTime + " 00:00:00", "yyyy-MM-dd HH:mm:ss"));
            } else if (StringUtils.isNotEmpty(endTime) && StringUtils.isEmpty(startTime)) {
                otaStatisticsExampleCriteria.andDayLessThanOrEqualTo(com.zoo.activity.util.DateUtil.convertStringToDate(endTime + " 00:00:00", "yyyy-MM-dd HH:mm:ss"));
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        List<Map> otaMaps = zooOtaStatisticsService.selectStatisticByExample(otaStatisticsExample);

        List<Map> otherMaps = new ArrayList<Map>();
        if (hungUpMaps != null && hungUpMaps.size() > 0 && hungUpMaps.get(0) != null) {
            hungUpMaps.get(0).put("name", "挂账");
            otherMaps.add(hungUpMaps.get(0));
        }
        if (entertainMaps != null && entertainMaps.size() > 0 && entertainMaps.get(0) != null) {
            entertainMaps.get(0).put("name", "招待");
            otherMaps.add(entertainMaps.get(0));
        }
        if (otaMaps != null && otaMaps.size() > 0 && otaMaps.get(0) != null) {
            otaMaps.get(0).put("name", "ota");
            otherMaps.add(otaMaps.get(0));
        }

        /**支付统计**/
        ZooPayStattisticsExample payStattisticsExample = new ZooPayStattisticsExample();
        ZooPayStattisticsExample.Criteria payStattisticsExampleCriteria = payStattisticsExample.createCriteria();
        payStattisticsExampleCriteria.andOrganizerIdEqualTo(organizer.getOrganizerId());
        try {
            if (StringUtils.isNotEmpty(startTime) && StringUtils.isNotEmpty(endTime)) {
                payStattisticsExampleCriteria.andDayBetween(com.zoo.activity.util.DateUtil.convertStringToDate(startTime + " 00:00:00", "yyyy-MM-dd HH:mm:ss"), com.zoo.activity.util.DateUtil.convertStringToDate(endTime + " 23:59:59", "yyyy-MM-dd HH:mm:ss"));
            } else if (StringUtils.isNotEmpty(startTime) && StringUtils.isEmpty(endTime)) {
                payStattisticsExampleCriteria.andDayGreaterThanOrEqualTo(com.zoo.activity.util.DateUtil.convertStringToDate(startTime + " 00:00:00", "yyyy-MM-dd HH:mm:ss"));
            } else if (StringUtils.isNotEmpty(endTime) && StringUtils.isEmpty(startTime)) {
                payStattisticsExampleCriteria.andDayLessThanOrEqualTo(com.zoo.activity.util.DateUtil.convertStringToDate(endTime + " 00:00:00", "yyyy-MM-dd HH:mm:ss"));
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        List<Map> payMaps = zooPayStattisticsService.selectStatisticByExample(payStattisticsExample);

        /**产品退款统计**/
        ZooRefundStatisticsExample refundStatisticsExample = new ZooRefundStatisticsExample();
        ZooRefundStatisticsExample.Criteria refundStatisticsExampleCriteria = refundStatisticsExample.createCriteria();
        refundStatisticsExampleCriteria.andOrganizerIdEqualTo(organizer.getOrganizerId());
        refundStatisticsExampleCriteria.andEquipmentIdIsNull();
        try {
            if (StringUtils.isNotEmpty(startTime) && StringUtils.isNotEmpty(endTime)) {
                refundStatisticsExampleCriteria.andDayBetween(com.zoo.activity.util.DateUtil.convertStringToDate(startTime + " 00:00:00", "yyyy-MM-dd HH:mm:ss"), com.zoo.activity.util.DateUtil.convertStringToDate(endTime + " 23:59:59", "yyyy-MM-dd HH:mm:ss"));
            } else if (StringUtils.isNotEmpty(startTime) && StringUtils.isEmpty(endTime)) {
                refundStatisticsExampleCriteria.andDayGreaterThanOrEqualTo(com.zoo.activity.util.DateUtil.convertStringToDate(startTime + " 00:00:00", "yyyy-MM-dd HH:mm:ss"));
            } else if (StringUtils.isNotEmpty(endTime) && StringUtils.isEmpty(startTime)) {
                refundStatisticsExampleCriteria.andDayLessThanOrEqualTo(com.zoo.activity.util.DateUtil.convertStringToDate(endTime + " 00:00:00", "yyyy-MM-dd HH:mm:ss"));
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        List<Map> proRefundMaps = zooRefundStatisticsService.selectStatisticByExample(refundStatisticsExample);

        /**租赁退款统计**/
        ZooRefundStatisticsExample rentRefundStatisticsExample = new ZooRefundStatisticsExample();
        ZooRefundStatisticsExample.Criteria rentRefundStatisticsExampleCriteria = rentRefundStatisticsExample.createCriteria();
        rentRefundStatisticsExampleCriteria.andOrganizerIdEqualTo(organizer.getOrganizerId());
        rentRefundStatisticsExampleCriteria.andEquipmentIdIsNotNull();
        try {
            if (StringUtils.isNotEmpty(startTime) && StringUtils.isNotEmpty(endTime)) {
                rentRefundStatisticsExampleCriteria.andDayBetween(com.zoo.activity.util.DateUtil.convertStringToDate(startTime + " 00:00:00", "yyyy-MM-dd HH:mm:ss"), com.zoo.activity.util.DateUtil.convertStringToDate(endTime + " 23:59:59", "yyyy-MM-dd HH:mm:ss"));
            } else if (StringUtils.isNotEmpty(startTime) && StringUtils.isEmpty(endTime)) {
                rentRefundStatisticsExampleCriteria.andDayGreaterThanOrEqualTo(com.zoo.activity.util.DateUtil.convertStringToDate(startTime + " 00:00:00", "yyyy-MM-dd HH:mm:ss"));
            } else if (StringUtils.isNotEmpty(endTime) && StringUtils.isEmpty(startTime)) {
                rentRefundStatisticsExampleCriteria.andDayLessThanOrEqualTo(com.zoo.activity.util.DateUtil.convertStringToDate(endTime + " 00:00:00", "yyyy-MM-dd HH:mm:ss"));
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        List<Map> rentRefundMaps = zooRefundStatisticsService.selectStatisticByExample(refundStatisticsExample);

        /**押金未退统计**/
        ZooBalanceDepositIndemnityExample balanceDepositIndemnityExample = new ZooBalanceDepositIndemnityExample();
        ZooBalanceDepositIndemnityExample.Criteria balanceDepositIndemnityExampleCriteria = balanceDepositIndemnityExample.createCriteria();
        balanceDepositIndemnityExampleCriteria.andOrganizerIdEqualTo(organizer.getOrganizerId());
        balanceDepositIndemnityExampleCriteria.andEquipmentIdIsNotNull();
        try {
            if (StringUtils.isNotEmpty(startTime) && StringUtils.isNotEmpty(endTime)) {
                balanceDepositIndemnityExampleCriteria.andIndemnityTimeBetween(com.zoo.activity.util.DateUtil.convertStringToDate(startTime + " 00:00:00", "yyyy-MM-dd HH:mm:ss"), com.zoo.activity.util.DateUtil.convertStringToDate(endTime + " 23:59:59", "yyyy-MM-dd HH:mm:ss"));
            } else if (StringUtils.isNotEmpty(startTime) && StringUtils.isEmpty(endTime)) {
                balanceDepositIndemnityExampleCriteria.andIndemnityTimeGreaterThanOrEqualTo(com.zoo.activity.util.DateUtil.convertStringToDate(startTime + " 00:00:00", "yyyy-MM-dd HH:mm:ss"));
            } else if (StringUtils.isNotEmpty(endTime) && StringUtils.isEmpty(startTime)) {
                balanceDepositIndemnityExampleCriteria.andIndemnityTimeLessThanOrEqualTo(com.zoo.activity.util.DateUtil.convertStringToDate(endTime + " 00:00:00", "yyyy-MM-dd HH:mm:ss"));
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        List<Map> depositNoRefundMaps = zooBalanceDepositIndemnityService.selectStatisticByExample(balanceDepositIndemnityExample);

        List<Map> refundMaps = new ArrayList<Map>();
        if (proRefundMaps != null && proRefundMaps.size() > 0 && proRefundMaps.get(0) != null) {
            proRefundMaps.get(0).put("name", "产品退款");
            refundMaps.add(proRefundMaps.get(0));
        }
        if (rentRefundMaps != null && rentRefundMaps.size() > 0 && rentRefundMaps.get(0) != null) {
            rentRefundMaps.get(0).put("name", "租赁物退款");
            refundMaps.add(rentRefundMaps.get(0));
        }
        if (depositNoRefundMaps != null && depositNoRefundMaps.size() > 0 && depositNoRefundMaps.get(0) != null) {
            depositNoRefundMaps.get(0).put("name", "押金赔偿");
            depositNoRefundMaps.get(0).put("money", depositNoRefundMaps.get(0).get("indemnity"));
            refundMaps.add(depositNoRefundMaps.get(0));
        }
        Map<String, List<Map>> dataList = new HashMap<String, List<Map>>();
        dataList.put("prodMaps", prodMaps);
        dataList.put("schoolMaps", schoolMaps);
        dataList.put("rentMaps", rentMaps);
        dataList.put("otherMaps", otherMaps);
        dataList.put("payMaps", payMaps);
        dataList.put("depositNoRefundMaps", depositNoRefundMaps);
        return dataList;
    }

    private ZooBalanceStatisticsExample.Criteria createActivityExampleCriteria(ZooBalanceStatisticsExample activityExample, String startTime, String endTime, Integer status, Organizer organizer) {
        ZooBalanceStatisticsExample.Criteria activityExampleCriteria = activityExample.createCriteria();
        try {
            if (StringUtils.isNotEmpty(startTime) && StringUtils.isNotEmpty(endTime)) {
                activityExampleCriteria.andDayBetween(DateUtil.convertStringToDate(startTime + " 00:00:00", "yyyy-MM-dd HH:mm:ss"), DateUtil.convertStringToDate(endTime + " 23:59:59", "yyyy-MM-dd HH:mm:ss"));
            } else if (StringUtils.isNotEmpty(startTime) && StringUtils.isEmpty(endTime)) {
                activityExampleCriteria.andDayGreaterThanOrEqualTo(DateUtil.convertStringToDate(startTime + " 00:00:00", "yyyy-MM-dd HH:mm:ss"));
            } else if (StringUtils.isNotEmpty(endTime) && StringUtils.isEmpty(startTime)) {
                activityExampleCriteria.andDayLessThanOrEqualTo(DateUtil.convertStringToDate(endTime + " 00:00:00", "yyyy-MM-dd HH:mm:ss"));
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (status == 1) {
            //线上
            activityExampleCriteria.andLineOrOfflineNotEqualTo(2);
            activityExampleCriteria.andStatusRefundEqualTo(1);
        } else if (status == 2) {
            //线下
            activityExampleCriteria.andLineOrOfflineEqualTo(2);
        } else if (status == 3) {
            //不可退款（只有线上订单才计算不可退款）
            activityExampleCriteria.andLineOrOfflineNotEqualTo(2);
            activityExampleCriteria.andStatusRefundEqualTo(0);
        }
        activityExampleCriteria.andOrganizerIdEqualTo(organizer.getOrganizerId());
        return activityExampleCriteria;
    }

    private String formartDate(String startTime) {
        try {
            if (StringUtil.isEmpty(startTime)) {
                return startTime;
            }
            com.zoo.activity.util.DateUtil.convertStringToDate(startTime, "yyyy-MM-dd");
            return startTime;
        } catch (ParseException e) {
            e.printStackTrace();
            Date d = new Date(Long.valueOf(startTime));
            return com.zoo.activity.util.DateUtil.convertDateToString(d, "yyyy-MM-dd");
        }
    }
}
