package com.zoo.zoomerchantadminweb.dowload.controller;


import com.zoo.activity.util.DateUtil;
import com.zoo.activity.util.ExportExcelUtil;
import com.zoo.activity.util.MapUtil;
import com.zoo.finance.dao.model.ZooBalanceOrders;
import com.zoo.finance.dao.model.ZooEntertainDetail;
import jodd.util.StringUtil;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.CellReference;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class ExcelExportUtil {
    private static final Logger LOGGER = LoggerFactory.getLogger(ExcelExportUtil.class);

    /**
     * 导出对账列表Excell
     *
     * @throws Exception
     */
    public static void exportSXSSFExcel(Map<String, List<Map>> dataOneList, Map<String, List<Map>> dataTwoList, Map<String, List<Map>> dataThreeList, OutputStream out, String baseFilePath) throws IOException {
        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheetone = workbook.createSheet("线上订单");
        HSSFSheet sheettwo = workbook.createSheet("不可退款订单");
        HSSFSheet sheetthree = workbook.createSheet("线下订单");

        int rowIndex = 0;//创建到第几行了
        int countdd = 3;//每个表格隔3行
        List<Map> prodMaps = dataOneList.get("prodMaps");
        String[] cellsprodMaps = {"产品类型", "报名数（人）", "购买数量", "金额（元）"};
        String[] cellsProtsprodMaps = {"activityTypeId", "orderNum|0", "orderNum+0", "totalPrice|1"};
        List<Map> schoolMaps = dataOneList.get("schoolMaps");
        String[] cellsschoolMaps = {"产品类型", "报名数（人）", "购买数量", "金额（元）"};
        String[] cellsProtsschoolMaps = {"activityTypeId", "orderNum|0", "orderNum+0", "totalPrice|1"};
        List<Map> rentMaps = dataOneList.get("rentMaps");
//        String[] cellsrentMaps = {"产品名称", "金额（元）", "数量（个）", "异常数量（个）", "异常金额（元)"};
//        String[] cellsProtsrentMaps = {"equipmentName", "totalMoney|1", "totalNum|0", "exceptionNum|0", "exceptionMoney|1"};
        String[] cellsrentMaps = {"产品名称", "金额（元）", "数量（个）"};
        String[] cellsProtsrentMaps = {"equipmentName", "totalMoney|1", "totalNum|0"};
        List<Map> otherMaps = dataOneList.get("otherMaps");
        String[] cellsotherMaps = {"产品类别", "数量", "金额"};
        String[] cellsProtsotherMaps = {"name", "num|0", "money|1"};
        List<Map> payMaps = dataOneList.get("payMaps");
        String[] cellspayMaps = {"支付类型", "数量", "金额"};
        String[] cellsProtspayMaps = {"payName", "num|0", "payMoney|1"};
        List<Map> depositNoRefundMaps = dataOneList.get("depositNoRefundMaps");
        String[] cellsdepositNoRefundMaps = {"品类", "数量（单）", "金额（元）"};
        String[] cellsProtsdepositNoRefundMaps = {"name", "num|0", "money|1"};
        rowIndex = CreatTable(workbook, sheetone, prodMaps, rowIndex, countdd, cellsprodMaps, cellsProtsprodMaps, "产品统计", 1);
        rowIndex = CreatTable(workbook, sheetone, schoolMaps, rowIndex, countdd, cellsschoolMaps, cellsProtsschoolMaps, "滑雪学校产品统计", 2);
        rowIndex = CreatTable(workbook, sheetone, rentMaps, rowIndex, countdd, cellsrentMaps, cellsProtsrentMaps, "租赁物统计", 2);
        rowIndex = CreatTable(workbook, sheetone, otherMaps, rowIndex, countdd, cellsotherMaps, cellsProtsotherMaps, "其他数据", 2);
        rowIndex = CreatTable(workbook, sheetone, payMaps, rowIndex, countdd, cellspayMaps, cellsProtspayMaps, "支付统计", 2);
        rowIndex = CreatTable(workbook, sheetone, depositNoRefundMaps, rowIndex, countdd, cellsdepositNoRefundMaps, cellsProtsdepositNoRefundMaps, "退款统计", 2);
        rowIndex = 0;
        prodMaps = dataTwoList.get("prodMaps");
        schoolMaps = dataTwoList.get("schoolMaps");
        rentMaps = dataTwoList.get("rentMaps");
        otherMaps = dataTwoList.get("otherMaps");
        payMaps = dataTwoList.get("payMaps");
        depositNoRefundMaps = dataTwoList.get("depositNoRefundMaps");
        rowIndex = CreatTable(workbook, sheettwo, prodMaps, rowIndex, countdd, cellsprodMaps, cellsProtsprodMaps, "产品统计", 1);
        rowIndex = CreatTable(workbook, sheettwo, schoolMaps, rowIndex, countdd, cellsschoolMaps, cellsProtsschoolMaps, "滑雪学校产品统计", 2);
        rowIndex = CreatTable(workbook, sheettwo, rentMaps, rowIndex, countdd, cellsrentMaps, cellsProtsrentMaps, "租赁物统计", 2);
        rowIndex = CreatTable(workbook, sheettwo, otherMaps, rowIndex, countdd, cellsotherMaps, cellsProtsotherMaps, "其他数据", 2);
        rowIndex = CreatTable(workbook, sheettwo, payMaps, rowIndex, countdd, cellspayMaps, cellsProtspayMaps, "支付统计", 2);
        rowIndex = CreatTable(workbook, sheettwo, depositNoRefundMaps, rowIndex, countdd, cellsdepositNoRefundMaps, cellsProtsdepositNoRefundMaps, "退款统计", 2);
        rowIndex = 0;
        prodMaps = dataThreeList.get("prodMaps");
        schoolMaps = dataThreeList.get("schoolMaps");
        rentMaps = dataThreeList.get("rentMaps");
        otherMaps = dataThreeList.get("otherMaps");
        payMaps = dataThreeList.get("payMaps");
        depositNoRefundMaps = dataThreeList.get("depositNoRefundMaps");
        rowIndex = CreatTable(workbook, sheetthree, prodMaps, rowIndex, countdd, cellsprodMaps, cellsProtsprodMaps, "产品统计", 1);
        rowIndex = CreatTable(workbook, sheetthree, schoolMaps, rowIndex, countdd, cellsschoolMaps, cellsProtsschoolMaps, "滑雪学校产品统计", 2);
        rowIndex = CreatTable(workbook, sheetthree, rentMaps, rowIndex, countdd, cellsrentMaps, cellsProtsrentMaps, "租赁物统计", 2);
        rowIndex = CreatTable(workbook, sheetthree, otherMaps, rowIndex, countdd, cellsotherMaps, cellsProtsotherMaps, "其他数据", 2);
        rowIndex = CreatTable(workbook, sheetthree, payMaps, rowIndex, countdd, cellspayMaps, cellsProtspayMaps, "支付统计", 2);
        rowIndex = CreatTable(workbook, sheetthree, depositNoRefundMaps, rowIndex, countdd, cellsdepositNoRefundMaps, cellsProtsdepositNoRefundMaps, "退款统计", 2);
        //workbook.write(out);

        File file = createFile(workbook, baseFilePath);
        FileInputStream in = new FileInputStream(file);
        byte[] b = new byte[1024];
        int len = 0;
        while ((len = in.read(b)) != -1) {
            out.write(b, 0, len);
        }
    }

    private static File createFile(HSSFWorkbook workbook, String baseFilePath) {
        File file = null;
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        try {
            workbook.write(os);

            byte[] content = os.toByteArray();
            file = new File(baseFilePath);//Excel文件生成后存储的位置。
            if (!file.exists()) {
                file.mkdirs();
            }
            file = new File(baseFilePath + File.separator + 12 + "-" + DateUtil.convertDateToString(new Date()) + ".xls");
            OutputStream fos;
            fos = new FileOutputStream(file);
            fos.write(content);
            os.close();
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return file;
    }

    /**
     * 导出对账详情列表Excell
     *
     * @throws Exception
     */
    public static void exportDetailSXSSFExcel(List<Map> detailList, OutputStream out, String baseFilePath, String activityTypeId) throws IOException {
        HSSFWorkbook workbook = new HSSFWorkbook();

        int rowIndex = 0;//创建到第几行了
        int countdd = 3;//每个表格隔3行
        if ("2".equals(activityTypeId) || "8".equals(activityTypeId)) {

            HSSFSheet sheetone = workbook.createSheet("滑雪学校详情");
            String[] cellsprodMaps = {"产品名称", "类型/时间", "购买数", "金额（元）"};
            String[] cellsProtsprodMaps = {"activityTypeName", "chargeTitle", "orderNum|0", "totalPrice|1"};
            rowIndex = CreatTable(workbook, sheetone, detailList, rowIndex, countdd, cellsprodMaps, cellsProtsprodMaps, "产品", 1);
        } else {
            HSSFSheet sheetone = workbook.createSheet("产品详情");
            String[] cellsprodMaps = {"产品名称", "场次", "票种", "报名人", "购买数", "金额（元）"};
            String[] cellsProtsprodMaps = {"activityTypeName", "episodeTitle", "chargeTitle", "orderNum|0", "orderNum+0", "totalPrice|1"};
            rowIndex = CreatTable(workbook, sheetone, detailList, rowIndex, countdd, cellsprodMaps, cellsProtsprodMaps, "产品", 1);
        }

        //workbook.write(out);
        File file = createFile(workbook, baseFilePath);
        FileInputStream in = new FileInputStream(file);
        byte[] b = new byte[1024];
        int len = 0;
        while ((len = in.read(b)) != -1) {
            out.write(b, 0, len);
        }
    }
    /**
     * 导出订单列表Excell
     *
     * @throws Exception
     */
    public static void exportOrdersSXSSFExcel(List<Map<String,List<Map>>> lists,List<Map> maps, OutputStream out, String baseFilePath) throws IOException {
        HSSFWorkbook workbook = new HSSFWorkbook();
        if(lists!=null&&lists.size()>0){
            for (int i = 0; i < lists.size(); i++) {
                Map<String,List<Map>> map=lists.get(i);
                Iterator<String> iterator = map.keySet().iterator();
                while (iterator.hasNext()){
                    String key = iterator.next();
                    String name=key.substring(0,10);
                    List<Map> listMaps = map.get(key);
                    HSSFSheet sheetone = workbook.createSheet(name);
                    if(listMaps!=null){
                        int rowIndex = 0;//创建到第几行了
                        int countdd = 3;//每个表格隔3行
                        String[] cellsprodMaps = {"订单号", "核销时间", "产品类型","活动名称", "场次名称", "票券名称", "单价", "数量", "总金额", "是否可退款","是否租赁物","租赁物名称", "支付类型", "支付方式","雪族服务费","微信服务费","总服务费"};
                        String[] cellsProtsprodMaps = {"orderCode", "verifyTime","activityType","activityTitle", "episodeTitle", "chargeTitle", "chargePrice|1", "verifyOrderNum|0", "verifyTotalPrice|1", "statusRefund","isRent","episodeTitle", "lineOrOffline","payType","orderOnePTFuwuMoney|1","orderOneTDFuwuMoney|1","orderOneFuwuMoney|1"};
                        rowIndex = CreatTable(workbook, sheetone, listMaps, rowIndex, countdd, cellsprodMaps, cellsProtsprodMaps, "订单详情", 0);
                    }
                }
            }
        }
        if(maps!=null&&maps.size()>0){
            HSSFSheet sheetone = workbook.createSheet("赔偿订单详情");
            int rowIndex = 0;//创建到第几行了
            int countdd = 3;//每个表格隔3行
            String[] cellsprodMaps = {"主订单号","子订单号", "赔偿类型","赔偿时间","租赁物名称", "赔偿金额","支付类型","支付方式","雪族服务费","微信服务费","总服务费"};
            String[] cellsProtsprodMaps = {"orderParentCode","orderCode", "rentStatusName","indemnityTime","equipmentName","indemnity|1","lineOrOffline","sysPayType","orderOnePTFuwuMoney|1","orderOneTDFuwuMoney|1","orderOneFuwuMoney|1"};
            rowIndex = CreatTable(workbook, sheetone, maps, rowIndex, countdd, cellsprodMaps, cellsProtsprodMaps, "赔偿订单详情", 0);
        }else{
            HSSFSheet sheetone = workbook.createSheet("赔偿订单详情");
        }
        //workbook.write(out);
        File file = createFile(workbook, baseFilePath);
        FileInputStream in = new FileInputStream(file);
        byte[] b = new byte[1024];
        int len = 0;
        while ((len = in.read(b)) != -1) {
            out.write(b, 0, len);
        }
    }

    /**
     * 导出不可退款订单列表Excell
     *
     * @throws Exception
     */
    public static void exportUnRefundOrdersSXSSFExcel(List<Map> maps, OutputStream out, String baseFilePath) throws IOException {
        HSSFWorkbook workbook = new HSSFWorkbook();
        if(maps!=null&&maps.size()>0){
            HSSFSheet sheetone = workbook.createSheet("不可退款订单");
            int rowIndex = 0;//创建到第几行了
            int countdd = 3;//每个表格隔3行
            String[] cellsprodMaps = {"主订单号","子订单号", "下单时间","产品类型", "产品名称","场次名称","票券名称","订单状态","支付方式", "数量", "单价", "总金额"};
            String[] cellsProtsprodMaps = {"parentCode","orderCode", "createTime","activityTypeId","activityName","episodeName","chargeName","orderStatus","payTypeId","chargeNum|0", "chargePrice|1","totalPrice|1"};
            rowIndex = CreatTable(workbook, sheetone, maps, rowIndex, countdd, cellsprodMaps, cellsProtsprodMaps, "订单详情", 0);
        }
        //workbook.write(out);
        File file = createFile(workbook, baseFilePath);
        FileInputStream in = new FileInputStream(file);
        byte[] b = new byte[1024];
        int len = 0;
        while ((len = in.read(b)) != -1) {
            out.write(b, 0, len);
        }
    }
    /**
     * 导出赔偿订单列表Excell
     *
     * @throws Exception
     */
    public static void exportIndemnityOrdersSXSSFExcel(List<Map> maps, OutputStream out, String baseFilePath) throws IOException {
        HSSFWorkbook workbook = new HSSFWorkbook();
        if(maps!=null&&maps.size()>0){
            HSSFSheet sheetone = workbook.createSheet("赔偿订单");
            int rowIndex = 0;//创建到第几行了
            int countdd = 3;//每个表格隔3行
            String[] cellsprodMaps = {"主订单号","子订单号", "赔偿时间","租赁物名称", "赔偿金额","支付类型","支付方式","赔偿操作人"};
            String[] cellsProtsprodMaps = {"orderParentCode","orderCode", "indemnityTime","equipmentName","indemnity|1","lineOrOffline","sysPayType","operatorName"};
            rowIndex = CreatTable(workbook, sheetone, maps, rowIndex, countdd, cellsprodMaps, cellsProtsprodMaps, "赔偿订单详情", 0);
        }
        //workbook.write(out);
        File file = createFile(workbook, baseFilePath);
        FileInputStream in = new FileInputStream(file);
        byte[] b = new byte[1024];
        int len = 0;
        while ((len = in.read(b)) != -1) {
            out.write(b, 0, len);
        }
    }
    /**
     * 导出招待列表Excell
     *
     * @throws Exception
     */
    public static void exportEntertainSXSSFExcel(List<ZooEntertainDetail> zooEntertainDetails, OutputStream out, String baseFilePath) throws IOException {
        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheetone = workbook.createSheet("招待列表");

        int rowIndex = 0;//创建到第几行了
        int countdd = 3;//每个表格隔3行
        String[] cellsprodMaps = {"经办人", "招待产品", "数量（单）", "金额（元）", "招待人姓名", "招待人电话"};
        String[] cellsProtsprodMaps = {"trustees", "activityName", "num|0", "money|1", "entertainPersonName", "entertainPersonPhone"};
        List<Map> lists = new ArrayList<Map>();
        for (ZooEntertainDetail obj : zooEntertainDetails) {
            Map map = new HashMap();
            map.put("trustees", obj.getTrustees());
            map.put("activityName", obj.getActivityName());
            map.put("num", obj.getNum());
            map.put("money", obj.getMoney());
            map.put("entertainPersonName", obj.getEntertainPersonName());
            map.put("entertainPersonPhone", obj.getEntertainPersonPhone());
            lists.add(map);
        }
        rowIndex = CreatTable(workbook, sheetone, lists, rowIndex, countdd, cellsprodMaps, cellsProtsprodMaps, "招待", 1);

        //workbook.write(out);
        File file = createFile(workbook, baseFilePath);
        FileInputStream in = new FileInputStream(file);
        byte[] b = new byte[1024];
        int len = 0;
        while ((len = in.read(b)) != -1) {
            out.write(b, 0, len);
        }
    }

    /**
     * 导出招待列表Excell
     *
     * @throws Exception
     */
    public static void exportEntertainOrdersExcel(List<Map> maps, OutputStream out, String baseFilePath) throws IOException {
        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheetone = workbook.createSheet("招待订单明细列表");

        int rowIndex = 0;//创建到第几行了
        int countdd = 3;//每个表格隔3行
        String[] cellsprodMaps = {"订单类型", "订单号", "订单创建时间", "招待产品", "数量（单）", "金额（元）", "经办人", "招待人姓名", "招待人电话"};
        String[] cellsProtsprodMaps = {"titleName", "orderCode", "orderTime", "activityName", "num", "money","trustees","entertainPersonName","entertainPersonPhone"};
        rowIndex = CreatTable(workbook, sheetone, maps, rowIndex, countdd, cellsprodMaps, cellsProtsprodMaps, "招待", 1);

        //workbook.write(out);
        File file = createFile(workbook, baseFilePath);
        FileInputStream in = new FileInputStream(file);
        byte[] b = new byte[1024];
        int len = 0;
        while ((len = in.read(b)) != -1) {
            out.write(b, 0, len);
        }
    }

    /**
     * 导出挂账列表Excell
     *
     * @throws Exception
     */
    public static void exportHangupSXSSFExcel(List<Map> lists,List<Map> detailList, OutputStream out, String baseFilePath) throws IOException {
        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheetone = workbook.createSheet("挂账列表");

        int rowIndex = 0;//创建到第几行了
        int countdd = 3;//每个表格隔3行
        String[] cellsprodMaps = {"挂账单位", "票务数量（单）", "票务金额（元）", "教练数量（单）", "教练金额（元）"};
        String[] cellsProtsprodMaps = {"companyName", "ticketNum|0", "ticketMoney|1", "coachNum|0", "coachMoney|1"};

        rowIndex = CreatTable(workbook, sheetone, detailList, rowIndex, countdd, cellsprodMaps, cellsProtsprodMaps, "挂账", 1);

        //订单明细sheet
        createOrderDetailSheet(workbook,lists);

        //workbook.write(out);
        /*File file = createFile(workbook, baseFilePath);
        FileInputStream in = new FileInputStream(file);
        byte[] b = new byte[1024];
        int len = 0;
        while ((len = in.read(b)) != -1) {
            out.write(b, 0, len);
        }*/
        workbook.write(out);
    }

    public static void createOrderDetailSheet(HSSFWorkbook workbook,List<Map> detailList){
        int rowIndex = 0;//创建到第几行了
        int countdd = 3;//每个表格隔3行
        String[] cellsprodMaps = {"主订单号", "订单创建时间", "挂账单位", "挂账人", "挂账手机号","经办人","产品名称","场次","票种","购买数","金额","收银员"};
        String[] cellsProtsprodMaps = {"orderParentCode", "orderTime", "companyName", "hangupPersonName", "hangupPersonPhone", "trustees", "activityName", "episodeName","chargeName","num|0","money|1","operatorName"};
        //String[] cellsProtsprodMaps = {"orderParentCode", "ticketNum|0", "ticketMoney|1", "coachNum|0", "coachMoney|1"};

        Map<String,List<Map>> companys = new HashMap<String,List<Map>>();
        if(detailList!=null){
            for (Map map : detailList) {
                String companyName = MapUtils.getString(map,"companyName");
                List<Map> list = companys.get(companyName);
                if(list == null){
                    list = new ArrayList<Map>();
                    companys.put(companyName,list);
                }
                list.add(map);

            }

        }

        Iterator<Map.Entry<String, List<Map>>> iterator = companys.entrySet().iterator();
        while (iterator.hasNext()){
            Map.Entry<String, List<Map>> next = iterator.next();
            String cpName = next.getKey();
            HSSFSheet sheetone = workbook.createSheet("挂账-"+cpName);
            List<Map> value = next.getValue();
            rowIndex = 0;
            CreatTable(workbook, sheetone, value, rowIndex, countdd, cellsprodMaps, cellsProtsprodMaps, "挂账-"+cpName, 1);
        }
    }

    /**
     * 导出挂账列表Excell
     *
     * @throws Exception
     */
    public static void exportHangupDetailExcel(List<Map> detailList, OutputStream out, String baseFilePath) throws IOException {
        HSSFWorkbook workbook = new HSSFWorkbook();

        createOrderDetailSheet(workbook,detailList);

        //workbook.write(out);
        File file = createFile(workbook, baseFilePath);
        FileInputStream in = new FileInputStream(file);
        byte[] b = new byte[1024];
        int len = 0;
        while ((len = in.read(b)) != -1) {
            out.write(b, 0, len);
        }
    }

    /**
     * 导出挂账列表Excell
     *
     * @throws Exception
     */
    public static void exportConsumelistExcel(List<Map<String,Object>> orders,List<Map<String,Object>> rentOrders,List<Map<String,Object>> exceptionOrders,List<Map> payOrders, OutputStream out, String baseFilePath) throws IOException {
        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheetone = workbook.createSheet("消费记录");

        int rowIndex = 0;//创建到第几行了
        int countdd = 3;//每个表格隔3行
        String[] cellsprodMaps = {"产品种类", "产品名称", "报名人数", "订单数量", "总计金额"};
        String[] cellsProtsprodMaps = {"typeName", "orderTime", "companyName", "hangupPersonName", "hangupPersonPhone"};
        //String[] cellsProtsprodMaps = {"orderParentCode", "ticketNum|0", "ticketMoney|1", "coachNum|0", "coachMoney|1"};

        rowIndex = CreatTableTwo(workbook, sheetone, orders,rentOrders,exceptionOrders,payOrders, rowIndex, countdd, cellsprodMaps, cellsProtsprodMaps, "挂账", 1);

        //workbook.write(out);
        File file = createFile(workbook, baseFilePath);
        FileInputStream in = new FileInputStream(file);
        byte[] b = new byte[1024];
        int len = 0;
        while ((len = in.read(b)) != -1) {
            out.write(b, 0, len);
        }
    }

    /**
     *
     * @param workbook
     * @param sheetone
     * @param lists
     * @param rowIndex
     * @param countdd
     * @param cells
     * @param cellsProts
     * @param title
     * @param isNoOne
     * @return
     */
    private static int CreatTable(HSSFWorkbook workbook, HSSFSheet sheetone, List<Map> lists, int rowIndex, int countdd, String[] cells, String[] cellsProts, String title, int isNoOne) {
        List<Map> prodMaps = new ArrayList<Map>();
        getTableMaps(lists, cellsProts, prodMaps);

        HSSFRow row;
        HSSFCell cell;// 标题
        HSSFFont titleFont = workbook.createFont();
        HSSFCellStyle titleCellStyle = workbook.createCellStyle();
        titleCellStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
        titleCellStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        titleCellStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        titleCellStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        titleCellStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
        //创建该sheet头
        if (isNoOne == 1) {
            //继续给第一行第一列添加样式及内容
            titleFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            titleFont.setFontHeightInPoints((short) 20);
            titleCellStyle = workbook.createCellStyle();
            titleCellStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
            titleCellStyle.setFont(titleFont);
            titleCellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
            titleCellStyle.setFillForegroundColor(HSSFColor.GREY_40_PERCENT.index);
            //titleCellStyle.setFillBackgroundColor(HSSFColor.AQUA.index); // 背景色
            titleCellStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
            titleCellStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
            titleCellStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
            titleCellStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
            //titleCellStyle.setFillBackgroundColor(HSSFColor.RED.index);//设置图案背景色
            //titleCellStyle.setFillPattern(HSSFCellStyle.SQUARES);//设置图案样式

            //创建第一行 第一列
            row = sheetone.createRow(rowIndex++);
//            cell = row.createCell(0);
//            cell.setCellValue(sheetone.getSheetName());
//            cell.setCellStyle(titleCellStyle);
            for (int i = 0; i < 12; i++) {
                cell = row.createCell(i);
                cell.setCellValue(sheetone.getSheetName());
                cell.setCellStyle(titleCellStyle);
            }

            sheetone.addMergedRegion(new CellRangeAddress(0, 0, 0, 11));
        }

        //设置table的title
        titleFont = workbook.createFont();
        titleFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        titleFont.setFontHeightInPoints((short) 15);
        titleCellStyle = workbook.createCellStyle();
        titleCellStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
        titleCellStyle.setFont(titleFont);
        titleCellStyle.setFillBackgroundColor(HSSFColor.BLUE_GREY.index); // 背景色
        titleCellStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        titleCellStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        titleCellStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        titleCellStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);

        row = sheetone.createRow(rowIndex++);
        cell = row.createCell(0);
        cell.setCellValue(title);
        cell.setCellStyle(titleCellStyle);
        for (int i = 0; i < cells.length; i++) {
            cell = row.createCell(i);
            cell.setCellValue(title);
            cell.setCellStyle(titleCellStyle);
        }
        sheetone.addMergedRegion(new CellRangeAddress(rowIndex - 1, rowIndex - 1, (short) 0, (short) cells.length - 1));

        titleFont = workbook.createFont();
        titleFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        titleFont.setFontHeightInPoints((short) 12);
        titleCellStyle = workbook.createCellStyle();
        titleCellStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
        titleCellStyle.setFont(titleFont);
        titleCellStyle.setFillBackgroundColor(HSSFColor.GREY_40_PERCENT.index);
        titleCellStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        titleCellStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        titleCellStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        titleCellStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
        row = sheetone.createRow(rowIndex++);
        //创建i列
        for (int i = 0; i < cells.length; i++) {
            cell = row.createCell(i);
            cell.setCellStyle(titleCellStyle);
            cell.setCellValue(cells[i]);
            sheetone.setColumnWidth(i, cells[i].getBytes().length * 2 * 172);
        }

        // 数据
        HSSFCellStyle dataCellStyle = workbook.createCellStyle();
        dataCellStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
        dataCellStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        dataCellStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        dataCellStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        dataCellStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);

        int rowSize = (prodMaps == null) ? 0 : prodMaps.size();
        for (int i = rowIndex; i < rowSize + rowIndex; i++) {
            Map map = prodMaps.get(i - rowIndex);
            row = sheetone.createRow(i);
            for (int j = 0; j < cells.length; j++) {
                cell = row.createCell(j);
                cell.setCellStyle(dataCellStyle);

                if(cellsProts[j].indexOf("+")>0){
                    String s=cellsProts[j];
                    String substr=cellsProts[j].substring(0,cellsProts[j].indexOf("+"))+"|";
                    if ("0".equals(s.substring(s.indexOf("+") + 1, s.length()))) {
                        substr=substr+"0";
                    } else if ("1".equals(s.substring(s.indexOf("|") + 1, s.length()))) {
                        substr=substr+"1";
                    }
                    if (map.get(substr) != null) {
                        if(map.get(substr) instanceof  BigDecimal){
                            cell.setCellValue(new BigDecimal(map.get(substr).toString()).setScale(2,RoundingMode.HALF_UP).doubleValue());
                        }else if(map.get(substr) instanceof Integer){
                            cell.setCellValue(new Integer(map.get(substr).toString()));
                        }else if(map.get(substr) instanceof Date){
                            cell.setCellValue(DateUtil.convertDateToString((Date)(map.get(substr)),"yyyy-MM-dd HH:mm:ss"));
                        }else{
                            cell.setCellValue(map.get(substr).toString());
                        }

                        int columnWidth = sheetone.getColumnWidth(j);
                        int propertyValueLength = map.get(substr).toString().getBytes().length * 2 * 172;
                        if (columnWidth < propertyValueLength) {
                            sheetone.setColumnWidth(j, propertyValueLength);
                        }
                    }
                }else{
                    if (map.get(cellsProts[j]) != null) {
                        if(map.get(cellsProts[j]) instanceof  BigDecimal){
                            cell.setCellValue(new BigDecimal(map.get(cellsProts[j]).toString()).setScale(2,RoundingMode.HALF_UP).doubleValue());
                        }else if(map.get(cellsProts[j]) instanceof Integer){
                            cell.setCellValue(new Integer(map.get(cellsProts[j]).toString()));
                        }else if(map.get(cellsProts[j]) instanceof Date){
                            cell.setCellValue(DateUtil.convertDateToString((Date)(map.get(cellsProts[j])),"yyyy-MM-dd HH:mm:ss"));
                        }else{
                            cell.setCellValue(map.get(cellsProts[j]).toString());
                        }
                        int columnWidth = sheetone.getColumnWidth(j);
                        int propertyValueLength = map.get(cellsProts[j]).toString().getBytes().length * 2 * 172;
                        if (columnWidth < propertyValueLength) {
                            sheetone.setColumnWidth(j, propertyValueLength);
                        }
                    }
                }

            }
        }
        HSSFCellStyle emptyCellStyle = workbook.createCellStyle();
        rowIndex = rowSize + rowIndex;
        for (int i = rowIndex; i < rowIndex + countdd; i++) {
            row = sheetone.createRow(i);
            for (int j = 0; j < cells.length; j++) {
//                if(i==rowIndex){
//                    cell = row.createCell(j);
//                    cell.setCellStyle(dataCellStyle);
//                    cell.setCellValue("合计");
//                }else{
                cell = row.createCell(j);
                cell.setCellStyle(emptyCellStyle);
//                }

            }
        }
        rowIndex = rowIndex + countdd;
        return rowIndex;
    }

    /**
     *
     * @param workbook
     * @param sheetone
     * @param rowIndex
     * @param countdd
     * @param cells
     * @param cellsProts
     * @param title
     * @param isNoOne
     * @return
     */
    private static int CreatTableTwo(HSSFWorkbook workbook, HSSFSheet sheetone, List<Map<String,Object>> orders,List<Map<String,Object>> rentOrders,List<Map<String,Object>> exceptionOrders,List<Map> payOrders, int rowIndex, int countdd, String[] cells, String[] cellsProts, String title, int isNoOne) {
        List<Map> prodMaps = new ArrayList<Map>();

        HSSFRow row;
        HSSFCell cell;// 标题
        HSSFFont titleFont = workbook.createFont();
        HSSFCellStyle titleCellStyle = workbook.createCellStyle();
        titleCellStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
        titleCellStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        titleCellStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        titleCellStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        titleCellStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
        //创建该sheet头
        if (isNoOne == 1) {
            //继续给第一行第一列添加样式及内容
            titleFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            titleFont.setFontHeightInPoints((short) 20);
            titleCellStyle = workbook.createCellStyle();
            titleCellStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
            titleCellStyle.setFont(titleFont);
            titleCellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
            titleCellStyle.setFillForegroundColor(HSSFColor.GREY_40_PERCENT.index);
            //titleCellStyle.setFillBackgroundColor(HSSFColor.AQUA.index); // 背景色
            titleCellStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
            titleCellStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
            titleCellStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
            titleCellStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
            //titleCellStyle.setFillBackgroundColor(HSSFColor.RED.index);//设置图案背景色
            //titleCellStyle.setFillPattern(HSSFCellStyle.SQUARES);//设置图案样式

            //创建第一行 第一列
            row = sheetone.createRow(rowIndex++);
//            cell = row.createCell(0);
//            cell.setCellValue(sheetone.getSheetName());
//            cell.setCellStyle(titleCellStyle);
            for (int i = 0; i < 5; i++) {
                cell = row.createCell(i);
                cell.setCellValue(sheetone.getSheetName());
                cell.setCellStyle(titleCellStyle);
            }

            sheetone.addMergedRegion(new CellRangeAddress(0, 0, 0, 4));
        }

        titleFont = workbook.createFont();
        titleFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        titleFont.setFontHeightInPoints((short) 12);
        titleCellStyle = workbook.createCellStyle();
        titleCellStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
        titleCellStyle.setFont(titleFont);
        titleCellStyle.setFillBackgroundColor(HSSFColor.GREY_40_PERCENT.index);
        titleCellStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        titleCellStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        titleCellStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        titleCellStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
        row = sheetone.createRow(rowIndex++);
        //创建i列
        for (int i = 0; i < cells.length; i++) {
            cell = row.createCell(i);
            cell.setCellStyle(titleCellStyle);
            cell.setCellValue(cells[i]);
            sheetone.setColumnWidth(i, cells[i].getBytes().length * 2 * 172);
        }

        for (int i = 0; i < orders.size(); i++) {
            Map map=orders.get(i);
            if(map!=null&&map.get("typeName")!=null){
                String typeName=map.get("typeName").toString();
                if(map.get("lists")!=null){
                    List list=(List)map.get("lists");
                    if(list.size()>0){
                        int beginCount=rowIndex;
                        for (int j = 0; j < list.size(); j++) {
                            row = sheetone.createRow(rowIndex++);
                            cell = row.createCell(0);
                            cell.setCellValue(typeName);
                            cell.setCellStyle(titleCellStyle);

                            Map map1=(Map)list.get(j);

                            cell = row.createCell(1);
                            cell.setCellValue(map1.get("activity_title")==null?"":map1.get("activity_title").toString());
                            cell.setCellStyle(titleCellStyle);

                            cell = row.createCell(2);
                            cell.setCellValue(map1.get("playerNum")==null?"":map1.get("playerNum").toString());
                            cell.setCellStyle(titleCellStyle);

                            cell = row.createCell(3);
                            cell.setCellValue(map1.get("totalNum")==null?"":map1.get("totalNum").toString());
                            cell.setCellStyle(titleCellStyle);

                            cell = row.createCell(4);
                            cell.setCellValue(map1.get("totalMoney")==null?0.00:Double.valueOf(map1.get("totalMoney").toString()));
                            cell.setCellStyle(titleCellStyle);
                        }
                        int endCount=rowIndex-1;
                        sheetone.addMergedRegion(new CellRangeAddress(beginCount, endCount, 0, 0));
                    }
                }
            }
        }
        if(rentOrders!=null&&rentOrders.size()>0){
            int beginentCount=rowIndex;
            for (int i = 0; i < rentOrders.size(); i++) {
                row = sheetone.createRow(rowIndex++);
                cell = row.createCell(0);
                cell.setCellValue("租赁物");
                cell.setCellStyle(titleCellStyle);
                Map map1=(Map)rentOrders.get(i);

                cell = row.createCell(1);
                cell.setCellValue(map1.get("activity_title")==null?"":map1.get("activity_title").toString());
                cell.setCellStyle(titleCellStyle);

                cell = row.createCell(2);
                cell.setCellValue(map1.get("playerNum")==null?"":map1.get("playerNum").toString());
                cell.setCellStyle(titleCellStyle);

                cell = row.createCell(3);
                cell.setCellValue(map1.get("totalNum")==null?"":map1.get("totalNum").toString());
                cell.setCellStyle(titleCellStyle);

                cell = row.createCell(4);
                cell.setCellValue(map1.get("totalMoney")==null?0.00:Double.valueOf(map1.get("totalMoney").toString()));
                cell.setCellStyle(titleCellStyle);
            }
            int endentCount=rowIndex-1;
            sheetone.addMergedRegion(new CellRangeAddress(beginentCount, endentCount, 0, 0));
        }
        row = sheetone.createRow(rowIndex++);
        cell = row.createCell(0);
        cell.setCellValue("异常信息");
        cell.setCellStyle(titleCellStyle);
        cell = row.createCell(2);
        cell.setCellValue("");
        cell.setCellStyle(titleCellStyle);
        cell = row.createCell(3);
        cell.setCellValue("");
        cell.setCellStyle(titleCellStyle);
        cell = row.createCell(1);
        cell.setCellValue(exceptionOrders.get(0).get("name").toString());//totalMoney
        cell.setCellStyle(titleCellStyle);
        cell = row.createCell(4);
        cell.setCellValue(Double.valueOf(exceptionOrders.get(0).get("totalMoney").toString()));
        cell.setCellStyle(titleCellStyle);

        row = sheetone.createRow(rowIndex++);
        cell = row.createCell(0);
        cell.setCellValue("异常信息");
        cell.setCellStyle(titleCellStyle);
        cell = row.createCell(2);
        cell.setCellValue("");
        cell.setCellStyle(titleCellStyle);
        cell = row.createCell(3);
        cell.setCellValue("");
        cell.setCellStyle(titleCellStyle);
        cell = row.createCell(1);
        cell.setCellValue(exceptionOrders.get(1).get("name").toString());//totalMoney
        cell.setCellStyle(titleCellStyle);
        cell = row.createCell(4);
        cell.setCellValue(Double.valueOf(exceptionOrders.get(1).get("totalMoney").toString()));
        cell.setCellStyle(titleCellStyle);

        row = sheetone.createRow(rowIndex++);
        cell = row.createCell(0);
        cell.setCellValue("总收入合计");
        cell.setCellStyle(titleCellStyle);
        cell = row.createCell(1);
        cell.setCellValue("");
        cell.setCellStyle(titleCellStyle);
        cell = row.createCell(2);
        cell.setCellValue("");
        cell.setCellStyle(titleCellStyle);
        cell = row.createCell(3);
        cell.setCellValue("");//totalMoney
        cell.setCellStyle(titleCellStyle);
        cell = row.createCell(4);
        String sc = CellReference.convertNumToColString(4);
        String ec = CellReference.convertNumToColString(4);
        int countRow=rowIndex-1;
        cell.setCellFormula("SUM(" + sc + (2) + ":"+ec+(countRow)+")");
        cell.setCellStyle(titleCellStyle);

        row = sheetone.createRow(rowIndex++);
        cell = row.createCell(0);
        cell.setCellValue("支付方式");
        cell.setCellStyle(titleCellStyle);
        cell = row.createCell(1);
        cell.setCellValue("");
        cell.setCellStyle(titleCellStyle);
        cell = row.createCell(2);
        cell.setCellValue("");
        cell.setCellStyle(titleCellStyle);
        cell = row.createCell(3);
        cell.setCellValue("");
        cell.setCellStyle(titleCellStyle);
        cell = row.createCell(4);
        cell.setCellValue("");
        cell.setCellStyle(titleCellStyle);

        row = sheetone.createRow(rowIndex++);
        cell = row.createCell(0);
        cell.setCellValue("线上合计");
        cell.setCellStyle(titleCellStyle);
        cell = row.createCell(1);
        cell.setCellValue("");
        cell.setCellStyle(titleCellStyle);
        cell = row.createCell(2);
        cell.setCellValue("");
        cell.setCellStyle(titleCellStyle);
        cell = row.createCell(3);
        cell.setCellValue("");
        cell.setCellStyle(titleCellStyle);
        cell = row.createCell(4);
        cell.setCellValue("");
        cell.setCellStyle(titleCellStyle);




        Map onlineMap=(Map) payOrders.get(0).get("online");
        Map offlineMap=(Map)payOrders.get(0).get("offline");
        List onlineList=(List) onlineMap.get("list");
        List offlineList=(List) offlineMap.get("list");
        for (int i = 0; i < onlineList.size(); i++) {
            row = sheetone.createRow(rowIndex++);
            cell = row.createCell(0);
            Map mmp=(Map)onlineList.get(i);
            cell.setCellValue(mmp.get("name").toString());
            cell.setCellStyle(titleCellStyle);
            cell = row.createCell(1);
            cell.setCellValue(mmp.get("money").toString());
            cell.setCellStyle(titleCellStyle);
            cell = row.createCell(2);
            cell.setCellValue("");
            cell.setCellStyle(titleCellStyle);
            cell = row.createCell(3);
            cell.setCellValue("");
            cell.setCellStyle(titleCellStyle);
            cell = row.createCell(4);
            cell.setCellValue("");
            cell.setCellStyle(titleCellStyle);
        }
        row = sheetone.createRow(rowIndex++);
        cell = row.createCell(0);
        cell.setCellValue("线下合计");
        cell.setCellStyle(titleCellStyle);
        cell = row.createCell(1);
        cell.setCellValue("");
        cell.setCellStyle(titleCellStyle);
        cell = row.createCell(2);
        cell.setCellValue("");
        cell.setCellStyle(titleCellStyle);
        cell = row.createCell(3);
        cell.setCellValue("");
        cell.setCellStyle(titleCellStyle);
        cell = row.createCell(4);
        cell.setCellValue("");
        cell.setCellStyle(titleCellStyle);
        for (int i = 0; i < offlineList.size(); i++) {
            row = sheetone.createRow(rowIndex++);
            cell = row.createCell(0);
            Map mmp=(Map)offlineList.get(i);
            cell.setCellValue(mmp.get("name").toString());
            cell.setCellStyle(titleCellStyle);
            cell = row.createCell(1);
            cell.setCellValue(mmp.get("money").toString());
            cell.setCellStyle(titleCellStyle);
            cell = row.createCell(2);
            cell.setCellValue("");
            cell.setCellStyle(titleCellStyle);
            cell = row.createCell(3);
            cell.setCellValue("");
            cell.setCellStyle(titleCellStyle);
            cell = row.createCell(4);
            cell.setCellValue("");
            cell.setCellStyle(titleCellStyle);
        }

        row = sheetone.createRow(rowIndex++);
        cell = row.createCell(0);
        cell.setCellValue("支付合计");
        cell.setCellStyle(titleCellStyle);
        cell = row.createCell(1);
        cell.setCellValue(payOrders.get(0).get("totalMoney").toString());
        cell.setCellStyle(titleCellStyle);
        cell = row.createCell(2);
        cell.setCellValue("");
        cell.setCellStyle(titleCellStyle);
        cell = row.createCell(3);
        cell.setCellValue("");
        cell.setCellStyle(titleCellStyle);
        cell = row.createCell(4);
        cell.setCellValue("");
        cell.setCellStyle(titleCellStyle);
//        // 数据
//        HSSFCellStyle dataCellStyle = workbook.createCellStyle();
//        dataCellStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
//        dataCellStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
//        dataCellStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
//        dataCellStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
//        dataCellStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
//
//        int rowSize = (prodMaps == null) ? 0 : prodMaps.size();
//        for (int i = rowIndex; i < rowSize + rowIndex; i++) {
//            Map map = prodMaps.get(i - rowIndex);
//            row = sheetone.createRow(i);
//            for (int j = 0; j < cells.length; j++) {
//                cell = row.createCell(j);
//                cell.setCellStyle(dataCellStyle);
//
//                if(cellsProts[j].indexOf("+")>0){
//                    String s=cellsProts[j];
//                    String substr=cellsProts[j].substring(0,cellsProts[j].indexOf("+"))+"|";
//                    if ("0".equals(s.substring(s.indexOf("+") + 1, s.length()))) {
//                        substr=substr+"0";
//                    } else if ("1".equals(s.substring(s.indexOf("|") + 1, s.length()))) {
//                        substr=substr+"1";
//                    }
//                    if (map.get(substr) != null) {
//                        cell.setCellValue(map.get(substr).toString());
//                        int columnWidth = sheetone.getColumnWidth(j);
//                        int propertyValueLength = map.get(substr).toString().getBytes().length * 2 * 172;
//                        if (columnWidth < propertyValueLength) {
//                            sheetone.setColumnWidth(j, propertyValueLength);
//                        }
//                    }
//                }else{
//                    if (map.get(cellsProts[j]) != null) {
//                        cell.setCellValue(map.get(cellsProts[j]).toString());
//                        int columnWidth = sheetone.getColumnWidth(j);
//                        int propertyValueLength = map.get(cellsProts[j]).toString().getBytes().length * 2 * 172;
//                        if (columnWidth < propertyValueLength) {
//                            sheetone.setColumnWidth(j, propertyValueLength);
//                        }
//                    }
//                }
//
//            }
//        }
//        HSSFCellStyle emptyCellStyle = workbook.createCellStyle();
//        rowIndex = rowSize + rowIndex;
//        for (int i = rowIndex; i < rowIndex + countdd; i++) {
//            row = sheetone.createRow(i);
//            for (int j = 0; j < cells.length; j++) {
////                if(i==rowIndex){
////                    cell = row.createCell(j);
////                    cell.setCellStyle(dataCellStyle);
////                    cell.setCellValue("合计");
////                }else{
//                cell = row.createCell(j);
//                cell.setCellStyle(emptyCellStyle);
////                }
//
//            }
//        }
//        rowIndex = rowIndex + countdd;
        return rowIndex;
    }
    private static void getTableMaps(List<Map> lists, String[] cellsProts, List<Map> prodMaps) {
        boolean flag=false;
        Map<String, Object> totalMap = new HashMap<String, Object>();
        for (Map m : lists) {
            Map map = new HashMap();
            for (int i = 0; i < cellsProts.length; i++) {
                String s = cellsProts[i];
                if (s.indexOf("|") > 0) {
                    flag=true;
                    int count = 0;
                    BigDecimal money = BigDecimal.ZERO;
                    if ("0".equals(s.substring(s.indexOf("|") + 1, s.length()))) {
                        //0代表int，1代表bigdicimal
                        count = MapUtils.getIntValue(m, s.substring(0, s.indexOf("|")));
                        map.put(s, count);

                        int totalInt = MapUtils.getIntValue(totalMap, s);
                        totalMap.put(s, totalInt + count);
                    } else if ("1".equals(s.substring(s.indexOf("|") + 1, s.length()))) {
                        try {
                            money = m.get( s.substring(0, s.indexOf("|")))==null?BigDecimal.ZERO:new BigDecimal(m.get( s.substring(0, s.indexOf("|"))).toString());
                        }catch (NumberFormatException e){
                            System.out.println(m.get( s.substring(0, s.indexOf("|"))));
                        }

                        map.put(s, money);

                        BigDecimal totalDouble = totalMap.get(s)==null?BigDecimal.ZERO:(BigDecimal)totalMap.get(s);
                        totalMap.put(s,money.add(totalDouble));
                    }
                    //cellsProts[i] = s.substring(0, s.indexOf("|"));
                }else if(s.indexOf("+") > 0){
                    flag=true;
                    //不计算，在excell取值时候取重复的那条数据
                } else {
                    if ("payTypeId".equals(s)) {
                        if(m.get(s) == null){
                            System.out.println("数据错误");
                        }else{
                            map.put(s, convertPayTypeIdToName(Integer.parseInt(m.get(s).toString())));
                        }
                    }else{
                        map.put(s, m.get(s));
                    }

                    if (i == 0) {
                        if(flag){
                            totalMap.put(s, "合计");
                        }else{
                            totalMap.put(s, "");
                        }
                    } else {
                        totalMap.put(s, "");
                    }
                }

            }
            prodMaps.add(map);
        }
        prodMaps.add(totalMap);
    }

    public static String convertActivityTypeIdToName(int id) {
        switch (id) {
            case 0:
                return "最新活动";
            case 1:
                return "滑雪度假";
            case 2:
                return "教练预约";
            case 3:
                return "超值票务";
            case 4:
                return "会员卡";
            case 5:
                return "训练营";
            case 6:
                return "自营票务";
            case 7:
                return "酒店预定";
            case 8:
                return "教练课程";
            case 9:
                return "储值卡";
            case 10:
                return "续时产品";
            default:
                return "";
        }
    }
    //* 0：线下现金
    //* 1：线上
    //* 2：线下支付宝
    //* 3：线下刷卡
    //* 4：线下微信
    //* 5：线下预售
    //* 6：线下挂账
    //* 7：线上储值支付
    //* 8：线下储值支付
    //* 999：混合支付
    //* 9：线下挂账支付
    //* 10：线下招待支付
    public static String convertPayTypeIdToName(int id) {
        switch (id) {
            case 0:
                return "线下现金";
            case 1:
                return "线上微信";
            case 2:
                return "线下支付宝";
            case 3:
                return "线下银行卡";
            case 4:
                return "线下微信";
            case 5:
                return "线下预售";
            case 6:
                return "线下挂账";
            case 7:
                return "线上储值支付";
            case 8:
                return "线下储值支付";
            case 9:
                return "线下挂账支付";
            case 10:
                return "线下招待支付";
            case 999:
                return "混合支付";
            default:
                return "";
        }
    }


}
