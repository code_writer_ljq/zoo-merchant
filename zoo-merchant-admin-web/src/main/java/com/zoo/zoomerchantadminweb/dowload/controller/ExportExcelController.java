package com.zoo.zoomerchantadminweb.dowload.controller;

import com.github.pagehelper.PageInfo;
import com.zoo.activity.dao.model.Organizer;
import com.zoo.activity.dao.nutz.pojo.CommonAccount;
import com.zoo.common.util.PropertiesFileUtil;
import com.zoo.download.export.service.ExportDataTaskService;
import com.zoo.download.export.service.ExportLogService;
import com.zoo.download.filePackage.FileUtils;
import com.zoo.download.service.DownloadDistributerService;
import com.zoo.web.bean.WebResult;
import com.zoo.zoomerchantadminweb.context.WebUpmsContext;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.*;

@Api(value = "api/export", tags = {"【文件导出系统】导出Excell接口"}, description = "这是为前台UE提供服务Excell下载接口")
@RestController
@RequestMapping("api/export")
public class ExportExcelController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExportExcelController.class);

    @Autowired
    private DownloadDistributerService downloadDistributerService;

    @Autowired
    ExportDataTaskService exportDataTaskService;

    @Autowired
    ExportLogService exportLogService;

    private String filePath=PropertiesFileUtil.getInstance("config").get("web.Excell-path");

    //导出文件系统路径
    private String exportFilePath=PropertiesFileUtil.getInstance("config").get("export.file.path");

    @ApiOperation(value = "删除任务", httpMethod = "GET")
    @RequestMapping(value = "/task/delete", method = RequestMethod.GET)
    public Object delete(HttpServletResponse response, HttpServletRequest request,
                           @RequestParam(value = "taskId", required = true) Long taskId) throws IOException {

        CommonAccount account = WebUpmsContext.getAccount(request);

        exportLogService.deleteLog(taskId,account.getCustomerId().longValue());

        return WebResult.getSuccessResult();
    }

    @ApiOperation(value = "获取任务列表", httpMethod = "GET")
    @RequestMapping(value = "/task/list", method = RequestMethod.GET)
    public Object taskList(HttpServletResponse response, HttpServletRequest request,
                           @RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
                           @RequestParam(value = "size", required = false, defaultValue = "10") Integer size) throws IOException {

        Organizer organizer = WebUpmsContext.getOrgnization(request);
        CommonAccount account = WebUpmsContext.getAccount(request);
        PageInfo pageInfo = exportDataTaskService.taskList(organizer.getOrganizerId().longValue(), account.getCustomerId().longValue(),account.getIsnowPassword(), page, size);

        Map<String,Object> data = new HashMap<>();
        data.put("count",pageInfo.getTotal());
        data.put("dataList",pageInfo.getList());

        return WebResult.getSuccessResult("data",data);
    }

    @ApiOperation(value = "导出excel通用接口", httpMethod = "GET")
    @RequestMapping(value = "/export-file-common", method = RequestMethod.GET)
    public Map export(HttpServletResponse response, HttpServletRequest request,
                       @RequestParam(required = true, value = "service") String service) throws IOException {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        CommonAccount account = WebUpmsContext.getAccount(request);

        Map parameterMap = new HashMap();
        Enumeration parameterNames = request.getParameterNames();
        while (parameterNames.hasMoreElements()){
            String o = (String)parameterNames.nextElement();
            String parameter = request.getParameter(o);
            if(StringUtils.isNotEmpty(parameter)){
                parameterMap.put(o,parameter);
            }
        }

        Map export = downloadDistributerService.export(parameterMap, service ,account.getCustomerId().longValue(), account.getRealname(),account.getIsnowPassword(), organizer.getOrganizerId().longValue(),organizer.getName());
        return export;
    }

    /**
     * 获取文件扩展名
     *
     * @return string
     */
    private String getFileExt(String fileName) {
        return fileName.substring(fileName.lastIndexOf("."));
    }

    @ApiOperation(value = "导出excel通用接口-下载文件", httpMethod = "GET")
    @RequestMapping(value = "/export-file-download", method = RequestMethod.GET)
    public void download(HttpServletResponse response, HttpServletRequest request,
                      @RequestParam(required = true, value = "fileName") String fileName,
                      @RequestParam(required = false, value = "downloadName") String downloadName) throws IOException {

        String file = exportFilePath + File.separator + fileName;
        File f = new File(file);
        if(!f.exists()){
            ServletOutputStream outputStream = response.getOutputStream();
            outputStream.write("文件不存在".getBytes());
            response.flushBuffer();
            return;
        }

        if(StringUtils.isEmpty(downloadName)){
            downloadName = System.currentTimeMillis() + getFileExt(fileName);
        }else{
            downloadName = downloadName + getFileExt(fileName);
        }

        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        response.addHeader("Cache-Control", "no-cache");

        final String userAgent = request.getHeader("USER-AGENT");
        if(StringUtils.contains(userAgent, "MSIE")){//IE浏览器
            downloadName = URLEncoder.encode(downloadName,"UTF-8");
        }else if(StringUtils.contains(userAgent, "Mozilla")){//google,火狐浏览器
            downloadName = new String(downloadName.getBytes(), "ISO8859-1");
        }else{
            downloadName = URLEncoder.encode(downloadName,"UTF-8");//其他浏览器
        }

        response.addHeader("Content-Disposition", "attachment;filename=" + downloadName);
        ServletOutputStream outputStream = response.getOutputStream();
        FileInputStream in = new FileInputStream(file);
        byte[] b = new byte[1024];
        int length = 0;
        while ((length = in.read(b))>0){
            outputStream.write(b,0,length);
        }
        response.flushBuffer();
    }
}
