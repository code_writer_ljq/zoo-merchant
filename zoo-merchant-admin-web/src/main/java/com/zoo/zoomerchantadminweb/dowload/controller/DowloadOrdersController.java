package com.zoo.zoomerchantadminweb.dowload.controller;

import com.zoo.activity.dao.model.ActivityType;
import com.zoo.activity.dao.model.Organizer;
import com.zoo.activity.service.OrganizerService;
import com.zoo.activity.service.UnRefundService;
import com.zoo.common.util.PropertiesFileUtil;
import com.zoo.finance.dao.model.*;
import com.zoo.finance.service.*;
import com.zoo.util.data.DateUtil;
import com.zoo.zoomerchantadminweb.context.WebUpmsContext;
import com.zoo.zoomerchantadminweb.finance.controller.poi.util.FinanceExcelUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jodd.util.StringUtil;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.util.*;

@Api(value = "api/downloadorders", tags = {"【订单】下载Excell接口"}, description = "这是为前台UE提供服务Excell下载接口")
@RestController
@RequestMapping("api/downloadorders")
public class DowloadOrdersController {

    private static final Logger LOGGER = LoggerFactory.getLogger(DowloadOrdersController.class);

    @Autowired
    private UnRefundService unRefundService;
    @Autowired
    OrganizerService organizerService;
    @Autowired
    ZooBalanceDepositIndemnityService zooBalanceDepositIndemnityService;

    private String filePath=PropertiesFileUtil.getInstance("config").get("web.Excell-path");

    @ApiOperation(value = "导出不可退款列表excel", httpMethod = "GET")
    @RequestMapping(value = "/export-unrefundList", method = RequestMethod.GET)
    public void export(HttpServletResponse response, HttpServletRequest request,
                       @RequestParam(value = "activityId", required = false) Integer activityId,
                       @RequestParam(value = "episodeId", required = false) Integer episodeId,
                       @RequestParam(value = "startTime", required = false) String startTime,
                       @RequestParam(value = "typeId", required = true) Integer typeId,
                       @RequestParam(value = "endTime", required = false) String endTime,
                       @RequestParam(value = "code", required = false) String code,
                       @RequestParam(value = "isVerify", required = false) String isVerify) throws IOException {
        Organizer organizer = WebUpmsContext.getOrgnization(request);

        List<ActivityType> allowTypes = organizerService.getOrganizerActivityAuth(typeId);
        List<Integer> typeIds = new ArrayList<>();
        for (ActivityType activityType : allowTypes) {
            typeIds.add(activityType.getTypeId());
        }

        Date startDate = null;
        Date endDate = null;
        if (startTime != null) {
            try {
                startDate = DateUtil.convertStringToDate(startTime+" 00:00:00","yyyy-MM-dd HH:mm:ss");
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        if (endTime != null) {
            try {
                endDate = DateUtil.convertStringToDate(endTime+" 23:59:59","yyyy-MM-dd HH:mm:ss");
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        if (StringUtils.isNotBlank(code)) {
            code = code.trim();
        }
        List<Map> dataList = (List<Map>)unRefundService.getUnRefundList(activityId, episodeId, code, startDate, endDate, typeIds, organizer.getOrganizerId(), 0, 1000000).getDataList();
        if(dataList!=null&&dataList.size()>0){
            for (int i = 0; i < dataList.size(); i++) {
                String activityTypeName = FinanceExcelUtil.getActivityTypeIdName(MapUtils.getInteger(dataList.get(i), "activityTypeId"));
                dataList.get(i).put("activityTypeId",activityTypeName);
                dataList.get(i).put("orderStatus",FinanceExcelUtil.getOrderStatus(MapUtils.getInteger(dataList.get(i),"orderStatus")));
            }
        }
        String filename = organizer.getOrganizerId() + "_" + DateUtil.getToday("yyyyMMddHHmmss") + ".xls";
        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        response.setHeader("Cache-Control", "no-cache");
        response.setHeader("Content-Disposition", "attachment;filename=" + new String(filename.getBytes("gbk"), "iso8859-1"));
        ExcelExportUtil.exportUnRefundOrdersSXSSFExcel(dataList, response.getOutputStream(),filePath+"/UnRefundList");
        response.flushBuffer();
    }

    @ApiOperation(value = "导出赔偿列表excel", httpMethod = "GET")
    @RequestMapping(value = "/export-indemnityList", method = RequestMethod.GET)
    public void export(HttpServletResponse response, HttpServletRequest request,
                       @RequestParam(value = "startTime", required = false) String startTime,
                       @RequestParam(value = "endTime", required = false) String endTime,
                       @RequestParam(value = "code", required = false) String code) throws IOException {
        Organizer organizer = WebUpmsContext.getOrgnization(request);

        ZooBalanceDepositIndemnityExample zooBalanceDepositIndemnityExample = new ZooBalanceDepositIndemnityExample();
        ZooBalanceDepositIndemnityExample.Criteria criteria = zooBalanceDepositIndemnityExample.or();
        ZooBalanceDepositIndemnityExample.Criteria criteria1 = zooBalanceDepositIndemnityExample.or();
        criteria.andOrganizerIdEqualTo(organizer.getOrganizerId());
        criteria1.andOrganizerIdEqualTo(organizer.getOrganizerId());
        zooBalanceDepositIndemnityExample.setOrderByClause("indemnity_time desc");
        try {
            if (StringUtils.isNotEmpty(startTime) && StringUtils.isNotEmpty(endTime)) {
                criteria.andIndemnityTimeBetween(com.zoo.activity.util.DateUtil.convertStringToDate(startTime + " 00:00:00", "yyyy-MM-dd HH:mm:ss"), com.zoo.activity.util.DateUtil.convertStringToDate(endTime + " 23:59:59", "yyyy-MM-dd HH:mm:ss"));
                criteria1.andIndemnityTimeBetween(com.zoo.activity.util.DateUtil.convertStringToDate(startTime + " 00:00:00", "yyyy-MM-dd HH:mm:ss"), com.zoo.activity.util.DateUtil.convertStringToDate(endTime + " 23:59:59", "yyyy-MM-dd HH:mm:ss"));
            } else if (StringUtils.isNotEmpty(startTime) && StringUtils.isEmpty(endTime)) {
                criteria.andIndemnityTimeGreaterThanOrEqualTo(com.zoo.activity.util.DateUtil.convertStringToDate(startTime + " 00:00:00", "yyyy-MM-dd HH:mm:ss"));
                criteria1.andIndemnityTimeGreaterThanOrEqualTo(com.zoo.activity.util.DateUtil.convertStringToDate(startTime + " 00:00:00", "yyyy-MM-dd HH:mm:ss"));
            } else if (StringUtils.isNotEmpty(endTime) && StringUtils.isEmpty(startTime)) {
                criteria.andIndemnityTimeLessThanOrEqualTo(com.zoo.activity.util.DateUtil.convertStringToDate(endTime + " 00:00:00", "yyyy-MM-dd HH:mm:ss"));
                criteria1.andIndemnityTimeLessThanOrEqualTo(com.zoo.activity.util.DateUtil.convertStringToDate(endTime + " 00:00:00", "yyyy-MM-dd HH:mm:ss"));
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if(StringUtils.isNotEmpty(code)){
            criteria.andOrderCodeEqualTo(code);
            criteria1.andOrderParentCodeEqualTo(code);
        }
        List<ZooBalanceDepositIndemnity> rows = zooBalanceDepositIndemnityService.selectByExampleForOffsetPage(zooBalanceDepositIndemnityExample, 0, 10000000);
        List<Map> maps=new ArrayList<Map>();
        if(rows!=null&&rows.size()>0){
            for (int i = 0; i < rows.size(); i++) {
                ZooBalanceDepositIndemnity obj=rows.get(i);
                Map map=new HashMap();
                map.put("orderCode",obj.getOrderCode());
                map.put("orderParentCode",obj.getOrderParentCode());
                map.put("equipmentName",obj.getEquipmentName());
                map.put("indemnityTime",DateUtil.convertDateToString(obj.getIndemnityTime(),"yyyy-MM-dd HH:mm:ss"));
                map.put("depositAmount",obj.getDepositAmount());
                map.put("indemnity",obj.getIndemnity());
                if("1".equals(String.valueOf(obj.getLineOrOffline()))){
                    map.put("lineOrOffline","线上");
                }else if("2".equals(String.valueOf(obj.getLineOrOffline()))){
                    map.put("lineOrOffline","线下");
                }else if("3".equals(String.valueOf(obj.getLineOrOffline()))){
                    map.put("lineOrOffline","OTA");
                }else{
                    map.put("lineOrOffline","");
                }
                map.put("sysPayType",obj.getSysPayTypeName());
                map.put("operatorName",obj.getOperatorName());
                maps.add(map);
            }
        }

        String filename = organizer.getOrganizerId() + "_" + DateUtil.getToday("yyyyMMddHHmmss") + ".xls";
        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        response.setHeader("Cache-Control", "no-cache");
        response.setHeader("Content-Disposition", "attachment;filename=" + new String(filename.getBytes("gbk"), "iso8859-1"));
        ExcelExportUtil.exportIndemnityOrdersSXSSFExcel(maps, response.getOutputStream(),filePath+"/IndemnityList");
        response.flushBuffer();
    }

    private String formartDate(String startTime){
        try {
            if(StringUtil.isEmpty(startTime)){
                return startTime;
            }
            com.zoo.activity.util.DateUtil.convertStringToDate(startTime,"yyyy-MM-dd");
            return startTime;
        } catch (ParseException e) {
            e.printStackTrace();
            Date d = new Date(Long.valueOf(startTime));
            return com.zoo.activity.util.DateUtil.convertDateToString(d,"yyyy-MM-dd");
        }
    }
}
