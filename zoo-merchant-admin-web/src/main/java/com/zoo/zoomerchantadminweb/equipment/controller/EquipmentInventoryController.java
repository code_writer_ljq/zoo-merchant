package com.zoo.zoomerchantadminweb.equipment.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zoo.activity.dao.model.*;
import com.zoo.activity.dao.nutz.pojo.CommonAccount;
import com.zoo.activity.service.EquipmentDetailService;
import com.zoo.activity.service.EquipmentTotalService;
import com.zoo.activity.util.DateUtil;
import com.zoo.web.bean.WebResult;
import com.zoo.zoomerchantadminweb.base.BaseController;
import com.zoo.zoomerchantadminweb.context.WebUpmsContext;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * Created by ljq on 2018/9/19.
 */
@Api(value = "/api/server/equipment/inventory", tags = {"主办方租赁库存(总数)相关接口"}, description = "租赁库存相关接口描述")
@RestController
@RequestMapping("/api/server/equipment/inventory")
public class EquipmentInventoryController extends BaseController {

    @Autowired
    private EquipmentTotalService equipmentTotalService;

    @Autowired
    private EquipmentDetailService equipmentDetailService;

    @ApiOperation(value = "编辑租赁库存保存接口", notes = "编辑租赁库存保存接口(包含库存总数和损坏数)", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "equipmentInventorys", value = "租赁物库存（格式: [{equipmentId:123, count: 10}, {equipmentId:123, count: 10}]）", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "editMode", value = "编辑保存模式(库存总数为0，损坏数为1)", required = true, dataType = "int", paramType = "query"),
    })
    @RequestMapping(value = "/save.json", method = RequestMethod.POST)
    public Object saveEquipmentTotalByOrganizer(@RequestParam(value = "equipmentInventorys", required = true) String equipmentInventorys,
                                                @RequestParam(value = "editMode", required = true, defaultValue = "0") Integer editMode,
                                                HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        CommonAccount account = WebUpmsContext.getAccount(request);
        if (organizer == null || account == null) {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }

        // 解析批量处理的租赁物信息
        JSONArray equipments = new JSONArray();
        if (StringUtils.isNotBlank(equipmentInventorys)) {
            equipments = JSON.parseArray(equipmentInventorys);
        } else {
            return WebResult.getErrorResult(WebResult.Code.EQUIPMENT_TOTAL_OR_STORAGE_SAVE_FAIL);
        }

        for (int i = 0; i < equipments.size(); i++) {
            JSONObject jsonObject = equipments.getJSONObject(i);
            Integer equipmentId = jsonObject.getInteger("equipmentId");
            Integer count = jsonObject.getInteger("count");

            // 编辑总数和损坏数量不能为负数
            if (null == count || count < 0)
                continue;

            // 查询当前保存租赁物，在主办方下的库存情况
            EquipmentTotalExample equipmentTotalExample = new EquipmentTotalExample();
            equipmentTotalExample.createCriteria().andOrganizerIdEqualTo(organizer.getOrganizerId())
                    .andEquipmentIdEqualTo(equipmentId)
                    .andValidEqualTo(1);  // 查询有效的库存
            List<EquipmentTotal> equipmentTotalList = equipmentTotalService.selectByExample(equipmentTotalExample);

            // 根据设置保存的库存数量，计算并更新现有库存数量
            if (equipmentTotalList.size() > 1) {
                // 如果主办方租赁物库存总数大于1， 库存表示有问题的
                continue;
            }

            EquipmentTotal equipmentTotal;
            EquipmentTotal newEquipmentTotal = new EquipmentTotal();
            if (editMode == 0) {// 编辑保存租赁物库存总数
                // 如果没有库存那么就添加库存数量
                if (equipmentTotalList.size() <= 0) {
                    if (count == 0) {
                        // 0代表无库存。直接跳过
                        continue;
                    }

                    Date now = new Date();
                    equipmentTotal = new EquipmentTotal();
                    equipmentTotal.setEquipmentId(equipmentId);
                    equipmentTotal.setOrganizerId(organizer.getOrganizerId());
                    equipmentTotal.setEquipmentName(equipmentTotalService.getEquipmentName(equipmentId));
                    equipmentTotal.setTotalCount(count);
                    equipmentTotal.setRentCount(0);
                    equipmentTotal.setAvailableCount(count);
                    equipmentTotal.setStorageCount(0);
                    equipmentTotal.setDamageCount(0);
                    equipmentTotal.setValid(1);
                    equipmentTotal.setOperator(account.getCustomerId());
                    equipmentTotal.setCreateTime(now);
                    equipmentTotal.setUpdateTime(now);
                    equipmentTotalService.insert(equipmentTotal);
                } else {
                    equipmentTotal = equipmentTotalList.get(0);
                    if (equipmentTotal.getTotalCount() == count) {
                        // 无需修改库存总数，则跳过
                        continue;
                    }

                    newEquipmentTotal.setId(equipmentTotal.getId());
                    // 计算可使用数量的增加数量或减掉的数量
                    Integer delta = count - equipmentTotal.getTotalCount();
                    Integer newAvailableCount = equipmentTotal.getAvailableCount() + delta;
                    newEquipmentTotal.setTotalCount(count);
                    newEquipmentTotal.setAvailableCount(newAvailableCount);
                    equipmentTotalService.updateByPrimaryKeySelective(newEquipmentTotal);
                }
            }

            if (editMode == 1) {// 编辑保存租赁物损坏数量
                // 如果当前没有编辑租赁物的库存，是不能编辑保存它的损坏数量的
                if (equipmentTotalList.size() <= 0)
                    continue;

                equipmentTotal = equipmentTotalList.get(0);
                if (equipmentTotal.getDamageCount() == count) {
                    // 无需修改损坏总数，则跳过
                    continue;
                }

                // 计算已租或未租数量的增加数量或减掉的数量
                Integer delta = equipmentTotal.getDamageCount() - count;

                newEquipmentTotal.setId(equipmentTotal.getId());
                if (delta < 0) { // 增加损坏数量     ==> 1.损坏数量置为count  2.已租数量做减法 3.如果已租数量为0，那么就直接减掉未租数量 4.如果已租数量不够，再去减掉未租
                    Integer newRentCount = equipmentTotal.getRentCount() + delta;

                    if (newRentCount < 0) {
                        newEquipmentTotal.setRentCount(0);
                        Integer newAvailableCount = equipmentTotal.getAvailableCount() + newRentCount;
                        if (newAvailableCount < 0) {
                            newEquipmentTotal.setAvailableCount(0);
                            Integer newStorageCount = equipmentTotal.getStorageCount() + newAvailableCount;
                            if (newStorageCount < 0) {
                                // 损坏总数不能超过库存总数
                                continue;
                            } else {
                                newEquipmentTotal.setStorageCount(newStorageCount);
                            }
                        } else {
                            newEquipmentTotal.setAvailableCount(newAvailableCount);
                        }
                    } else {
                        newEquipmentTotal.setRentCount(newRentCount);
                    }

                    // 设置损坏数量
                    newEquipmentTotal.setDamageCount(count);
                }

                if (delta > 0) { // 减少损坏数量     ==> 1.损坏数量置为count  2.未租数量做加法
                    Integer newAvailableCount = equipmentTotal.getAvailableCount() + delta;
                    // 设置损坏数量
                    // 设置损坏数量
                    newEquipmentTotal.setDamageCount(count);
                    newEquipmentTotal.setAvailableCount(newAvailableCount);
                }

                // 更新租赁库存表
                equipmentTotalService.updateByPrimaryKeySelective(newEquipmentTotal);
            }
        }

        return WebResult.getSuccessResult();
    }

    /**
     * 返回数据用处： 1.展示当前主办方租赁物库存列表  2.编辑库存总数和损坏数量的时候用
     * 此接口两个用处，只会调用一次。（加载页面时调用）
     *
     * @param request
     * @return
     */
    @ApiOperation(value = "查询主办方的租赁物库存", notes = "查询主办方的租赁物库存（库存列表显示和编辑库存数量的时候会用到此接口返回数据）", httpMethod = "GET")
    @RequestMapping(value = "/list.json", method = RequestMethod.GET)
    public Object getEquipmentTotalListByOrganizer(HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null) {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }

        // 查询当前主办方下设置的所有租赁物的库存情况
        EquipmentTotalExample equipmentTotalExample = new EquipmentTotalExample();
        equipmentTotalExample.createCriteria().andOrganizerIdEqualTo(organizer.getOrganizerId()) // 主办方ID
                .andValidEqualTo(1);// 有效的

        List<EquipmentTotal> equipmentTotalList = equipmentTotalService.selectByExample(equipmentTotalExample);

        return WebResult.getSuccessResult("data", equipmentTotalList);
    }

    @ApiOperation(value = "查询主办方租赁物异常概览", notes = "查询主办方租赁物异常概览", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "startTime", value = "图表查询条件(开始时间，格式：时间戳)", required = false, dataType = "long", paramType = "query"),
            @ApiImplicitParam(name = "endTime", value = "图表查询条件(结束时间，格式：时间戳)", required = false, dataType = "long", paramType = "query"),
            @ApiImplicitParam(name = "isBarCharts", value = "是否是查询柱形图(图表)数据（0:租赁物异常表格数据  1:库存柱形图数据）", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "isAbnormalData", value = "租赁物异常数据（0:全部租赁物数据  1:异常租赁物数据）", required = false, dataType = "int", paramType = "query"),
    })
    @RequestMapping(value = "/lease/list.json", method = RequestMethod.GET)
    public Object getEquipmentDetailListByOrganizer(@RequestParam(value = "startTime", required = false) Long startTime,
                                                    @RequestParam(value = "endTime", required = false) Long endTime,
                                                    @RequestParam(value = "isBarCharts", required = true, defaultValue = "0") Integer isBarCharts,
                                                    @RequestParam(value = "isAbnormalData", required = false, defaultValue = "1") Integer isAbnormalData,
                                                    HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        // 是否统计所有状态
        Boolean isStatisticsAllStatus = false;
        if (isBarCharts == 1 && isAbnormalData == 0)
            isStatisticsAllStatus = true;
        //判断是否取全部租赁物信息
        Boolean isCompletionEquipment = false;
        if (isBarCharts == 1)
            isCompletionEquipment = true;

        if (organizer == null) {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }

        // 查询当前主办方下所有库存租赁物的异常情况
        EquipmentDetailExample equipmentDetailExample = new EquipmentDetailExample();
        // 封装查询条件
        EquipmentDetailExample.Criteria criteria = equipmentDetailExample.createCriteria().andOrganizerIdEqualTo(organizer.getOrganizerId()) // 主办方ID
                .andValidEqualTo(1);// 有效的

        List<Integer> statusIdList = new ArrayList<>();
        statusIdList.add(3); // 超时未处理
        statusIdList.add(5); // 损坏未处理
        if (isAbnormalData == 1) {
            statusIdList.add(4); // 超时已处理
            statusIdList.add(6); // 损坏已处理
        }
        if (isAbnormalData == 0) {
            statusIdList.add(0); //已租赁
            statusIdList.add(1); //已归还
            statusIdList.add(2); //暂存
        }
        criteria.andStatusIn(statusIdList);

        if (isBarCharts == 1) { // 租赁物异常柱形图数据需要增加查询条件（以租赁的创建时间维度查询）
            if (startTime != null && startTime > 0)
                criteria.andCreateTimeGreaterThanOrEqualTo(new Date(startTime));

            if (endTime != null && endTime > 0)
                criteria.andCreateTimeLessThanOrEqualTo(DateUtil.getEndTime(new Date(endTime)));
        }

        // 查询当前主办方下所有租赁物异常情况概览数据
        List<EquipmentDetailStatisticsView> equipmentDetailStatisticsViews = equipmentDetailService.getEquipmentDetailListByOrganizer(equipmentDetailExample, isStatisticsAllStatus, isCompletionEquipment);
        return WebResult.getSuccessResult("data", equipmentDetailStatisticsViews);
    }

    @ApiOperation(value = "编辑库存或者损坏总数获取数据接口", notes = "编辑库存或者损坏总数获取数据接口", httpMethod = "GET")
    @RequestMapping(value = "/edit/list.json", method = RequestMethod.GET)
    public Object getEquipmentInventoryListByOrganizer(HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null) {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }

        List<EquipmentTotal> equipmentDetailList = equipmentTotalService.getAllEquipmentInventoryDetailByOrganizerId(organizer.getOrganizerId());

        return WebResult.getSuccessResult("data", equipmentDetailList);
    }
}
