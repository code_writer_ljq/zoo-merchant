package com.zoo.zoomerchantadminweb.equipment.controller;

import com.alibaba.fastjson.JSONArray;
import com.zoo.account.constant.AccountEnum;
import com.zoo.account.service.AccountService;
import com.zoo.account.support.AccountResult;
import com.zoo.activity.util.MapUtil;
import com.zoo.bean.util.ActivityBeanUtils;
import com.zoo.common.page.PageList;
import com.zoo.activity.dao.model.*;
import com.zoo.activity.dao.nutz.pojo.CommonAccount;
import com.zoo.activity.dao.nutz.pojo.RentEquipment;
import com.zoo.activity.service.*;
import com.zoo.activity.util.DateUtil;
import com.zoo.activity.vo.RentBalance;
import com.zoo.icenow.dao.model.Entry;
import com.zoo.icenow.service.EnterService;
import com.zoo.web.bean.WebResult;
import com.zoo.zoomerchantadminweb.context.WebUpmsContext;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.*;

@Api(value = "api/server/equipment", tags = {"主办方租赁相关接口"}, description = "主办方租赁相关接口描述")
@RestController
@RequestMapping("api/server/equipment")
public class EquipmentController {

    @Resource(name = "depositService")
    private DepositService depositService;

    @Resource(name = "enterService")
    private EnterService enterService;

    @Resource(name = "orderService")
    private OrdersService ordersService;

    @Resource(name = "organizerService")
    private OrganizerService organizerService;

    @Resource(name = "episodeService")
    private EpisodeService episodeService;

    @Autowired
    private AccountService accountService;

    @Autowired
    private PlayerService playerService;

    @Autowired
    private ActivityService activityService;

    @ApiOperation(value = "订单是否需要出卡", notes = "判断该订单是否需要出卡", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "orderId", value = "订单ID", required = true, dataType = "integer", paramType = "query"),})
    @RequestMapping(value = "/cards.json", method = RequestMethod.GET)
    public WebResult getRentEquipmentCards(@RequestParam("orderId") Integer orderId, HttpServletRequest request) {

        Organizer organizer = WebUpmsContext.getOrgnization(request);

        if (organizer == null) {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
        RentEquipment cardEquipment = depositService.getCardEquipment();
        EquipmentRelationships equipmentRelationships = depositService.getRelationshipsByParentOrderIdAndStatusAndType(orderId, 0, cardEquipment.getEquipmentId());
        if (equipmentRelationships != null) {
            List<RentDetail> rentDetails = depositService.getByRelationIdAndStatus(equipmentRelationships.getRelationId(), null);

            Map<String, Object> result = new HashMap<>();
            result.put("equipmentRelationships", equipmentRelationships);
            result.put("rentDetails", rentDetails);

            return WebResult.getSuccessResult("data", result);
        } else {
            return WebResult.getErrorResult(WebResult.Code.EQUIPMENT_CARD_NOT_ENOUGH);
        }
    }

    @ApiOperation(value = "出卡", notes = "将卡号与订单绑定", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "code", value = "订单号", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "cardNumber", value = "卡号", required = true, dataType = "long", paramType = "query"),
    })
    @RequestMapping(value = "/outCard.json", method = RequestMethod.POST)
    public WebResult outCard(@RequestParam("code") String code, @RequestParam("cardNumber") Long cardNumber, HttpServletRequest request) {
        CommonAccount commonAccount = WebUpmsContext.getAccount(request);
        String hex = Long.toHexString(cardNumber);
        hex = transform(hex, "");
//        RentDetail rentDetail = depositService.outCard(code, hex, commonAccount);
        Map<String, Object> resultMap = enterService.outCard(hex, code, commonAccount, 1);

        if (!resultMap.containsKey("rentDetail") || !resultMap.containsKey("result")) {
            return WebResult.getErrorResult(WebResult.Code.EQUIPMENT_CARD_FAILED);
        }
        if (resultMap.get("rentDetail") == null) {
            return WebResult.getErrorMsgResult(WebResult.Code.ERROR, resultMap.containsKey("result") ? resultMap.get("result").toString() : "写卡失败");
        } else {
            return WebResult.getSuccessResult("data", resultMap.get("rentDetail"));
        }

    }

    @ApiOperation(value = "换卡", notes = "卡丢失或损坏时换卡操作", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "materialNum", value = "当前卡面号", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "cardNumber", value = "目标卡号", required = true, dataType = "long", paramType = "query"),
    })
    @RequestMapping(value = "/transferCard.json", method = RequestMethod.POST)
    public WebResult transferCard(@RequestParam("materialNum") String materialNum, @RequestParam("cardNumber") Long cardNumber, HttpServletRequest request) {
        CommonAccount account = WebUpmsContext.getAccount(request);
        if (account != null) {

            String hex = Long.toHexString(cardNumber);
            hex = transform(hex, "");
            Map<String, Object> resultMap = enterService.transferCard(materialNum, hex, account);
            if (resultMap.get("des") != null && resultMap.get("des").equals("换卡成功")) {
                return WebResult.getSuccessResult("data", resultMap.get("rentDetail"));
            } else {
                return WebResult.getErrorMsgResult(WebResult.Code.ERROR, resultMap.get("des").toString());
            }
        } else {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
    }


    @ApiOperation(value = "租赁信息列表", notes = "租赁信息列表", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页", required = false, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "size", value = "条", required = false, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "detailStatus", value = "状态", required = false, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "equipmentId", value = "装备ID", required = false, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "borrowStart", value = "开始借出时间", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "borrowEnd", value = "结束借出时间", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "cardNumber", value = "卡号", required = false, dataType = "long", paramType = "query"),
            @ApiImplicitParam(name = "code", value = "订单号", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "payType", value = "订单来源", required = false, dataType = "integer", paramType = "query")
    })
    @RequestMapping(value = "/list.json", method = RequestMethod.POST)
    public Object getRentEquipmentList(@RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
                                       @RequestParam(value = "size", required = false, defaultValue = "10") Integer size,
                                       @RequestParam(value = "detailStatus", required = false) Integer detailStatus,
                                       @RequestParam(value = "equipmentId", required = false) Integer equipmentId,
                                       @RequestParam(value = "borrowStart", required = false) String borrowStart,
                                       @RequestParam(value = "borrowEnd", required = false) String borrowEnd,
                                       @RequestParam(value = "cardNumber", required = false) Long cardNumber,
                                       @RequestParam(value = "code", required = false) String code,
                                       @RequestParam(value = "payType", required = false) Integer payType, HttpServletRequest request) {
        String hex = null;
        if (cardNumber != null) {
            hex = Long.toHexString(cardNumber);
            hex = transform(hex, "");
        }
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer != null) {
            PageList pageList = depositService.getRentEquipmentListByOrgId(organizer.getOrganizerId(), equipmentId, code, detailStatus, borrowStart, borrowEnd, hex, size, page, payType);

            return WebResult.getSuccessResult("data", pageList);
        } else {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }

    }

    @ApiOperation(value = "通过扫支付码获取订单", notes = "通过扫支付码获取订单", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "code", value = "支付码", required = false, dataType = "string", paramType = "query")
    })
    @RequestMapping("/getOrderCodeByTransctionId.json")
    public WebResult getOrderCodeByTransctionId(@RequestParam(value = "code", required = false) String code, HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer != null) {
            Orders orders = ordersService.getSimpleOrdersByTranscationId(code);

            if (null == orders) {
                return WebResult.getErrorMsgResult(WebResult.Code.ERROR, "无订单信息");
            }

            return WebResult.getSuccessResult("data", orders.getCode());

        } else {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
    }

    @ApiOperation(value = "通过刷卡获取订单号", notes = "通过刷卡获取订单号", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "cardNumber", value = "卡号", required = false, dataType = "long", paramType = "query")
    })
    @RequestMapping("/listByCard.json")
    public WebResult getRentEquipmentListByCard(@RequestParam(value = "cardNumber", required = false) Long cardNumber, HttpServletRequest request) {
        String hex = null;
        if (cardNumber != null) {
            hex = Long.toHexString(cardNumber);
            hex = transform(hex, "");
        }
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer != null) {
            Material material = depositService.selectByGateWaferNum(hex, organizer.getOrganizerId(), 1);
            if (null == material || material.getStatus() != 1) {
                return WebResult.getErrorMsgResult(WebResult.Code.ERROR, "该卡无订单信息");
            }
            Entry entry = enterService.getByMaterialId(material.getMaterialId());

            if (null == entry) {
                return WebResult.getErrorMsgResult(WebResult.Code.ERROR, "该卡无订单信息");
            }

            Orders orders = ordersService.getSimpleOrdersById(entry.getOrderId());
            if (orders.getParentId() == null || orders.getParentId() == 0) {
                return WebResult.getSuccessResult("data", orders.getCode());
            }
            Orders parentOrder = ordersService.getSimpleOrdersById(orders.getParentId());
            return WebResult.getSuccessResult("data", parentOrder.getCode());

        } else {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
    }

    @ApiOperation(value = "通过刷卡获取卡库存信息", notes = "通过刷卡获取卡库存信息", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "cardNumber", value = "卡号", required = false, dataType = "long", paramType = "query")
    })
    @RequestMapping("/getCard.json")
    public WebResult getMaterialByCard(@RequestParam(value = "cardNumber", required = false) Long cardNumber, HttpServletRequest request) {
        String hex = null;
        if (cardNumber != null) {
            hex = Long.toHexString(cardNumber);
            hex = transform(hex, "");
        }
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer != null) {
            Material material = depositService.selectByGateWaferNum(hex, organizer.getOrganizerId(), 0);
            if (null == material) {
                return WebResult.getErrorMsgResult(WebResult.Code.ERROR, "目标卡无效");
            }
            return WebResult.getSuccessResult("data", material);

        } else {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
    }


    @ApiOperation(value = "租赁", notes = "租赁", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "detailId", value = "ID", required = true, dataType = "int", paramType = "query")
    })
    @RequestMapping("/rent.json")
    public WebResult rent(@RequestParam("detailId") Integer detailId, HttpServletRequest request) {

        Organizer organizer = WebUpmsContext.getOrgnization(request);

        CommonAccount account = WebUpmsContext.getAccount(request);

        if (organizer == null) {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }

        return depositService.rentByPc(detailId, account);
    }

    private String transform(String origin, String des) {
        if (StringUtils.isBlank(des) && origin.length() < 8) {
            int len = 8 - origin.length();
            for (int i = 0; i < len; i++)
                origin = "0" + origin;
        }
        if (origin.length() <= des.length()) {
            return des.toUpperCase();
        } else {
            String last = origin.substring(origin.length() - 2 - des.length(), origin.length() - des.length());
            return transform(origin, des + last);
        }
    }

    @ApiOperation(value = "归还", notes = "归还", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "detailId", value = "ID", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "code", value = "订单号", required = true, dataType = "string", paramType = "query")
    })
    @RequestMapping("/return.json")
    public WebResult returnByPc(@RequestParam("detailId") Integer detailId, @RequestParam("code") String code, HttpServletRequest request) throws Exception {

        Organizer organizer = WebUpmsContext.getOrgnization(request);
        CommonAccount account = WebUpmsContext.getAccount(request);

        if (organizer == null || account == null) {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }

        OrganizerWithScenic organizerWithScenic = organizerService.getAllInfoById(organizer.getOrganizerId());
        Orders orders = ordersService.getOrdersByCode(code);
        Episode episode = episodeService.getEpisodeByChargeId(orders.getChargeId());
        Integer validRange = episode.getDuration().intValue();
        Double daytime = (organizerWithScenic.getDaytimeEnd().doubleValue() + organizerWithScenic.getDaytimeEndMin().doubleValue() / 60) - (organizerWithScenic.getDaytimeStart().doubleValue() + organizerWithScenic.getDaytimeStartMin().doubleValue() / 60);

        try {
            Integer relationId = depositService.returnByPc(detailId, validRange, daytime, organizer.getDelayTime(), account);

            depositService.check(relationId);

            return WebResult.getSuccessResult();

        } catch (Exception e) {
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }
    }

    @ApiOperation(value = "全部归还", notes = "code，detailId两个参数", httpMethod = "POST")
    @RequestMapping("/all/return.json")
    public WebResult allReturnByPc(String params, HttpServletRequest request) throws Exception {

        Organizer organizer = WebUpmsContext.getOrgnization(request);
        CommonAccount account = WebUpmsContext.getAccount(request);

        if (organizer == null || account == null) {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
        List<Map> maps = JSONArray.parseArray(params, Map.class);
        OrganizerWithScenic organizerWithScenic = organizerService.getAllInfoById(organizer.getOrganizerId());
        maps.forEach(e -> {
            String code = MapUtils.getString(e, "code");
            Integer detailId = MapUtils.getInteger(e, "detailId");
            Orders orders = ordersService.getOrdersByCode(code);
            Episode episode = episodeService.getEpisodeByChargeId(orders.getChargeId());
            Integer validRange = episode.getDuration().intValue();
            Double daytime = (organizerWithScenic.getDaytimeEnd().doubleValue() + organizerWithScenic.getDaytimeEndMin().doubleValue() / 60) - (organizerWithScenic.getDaytimeStart().doubleValue() + organizerWithScenic.getDaytimeStartMin().doubleValue() / 60);

            Integer relationId = depositService.returnByPc(detailId, validRange, daytime, organizer.getDelayTime(), account);

            try {
                depositService.check(relationId);
            } catch (Exception ex) {
                ex.printStackTrace();
            }

        });

        return WebResult.getSuccessResult();

    }

    @ApiOperation(value = "点击归还弹框信息", notes = "点击归还弹框信息", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "code", value = "订单号（parentCode）", required = true, dataType = "string", paramType = "query")
    })
    @RequestMapping("/balances.json")
    public WebResult rentBalances(@RequestParam("code") String code, HttpServletRequest request) throws Exception {

        Organizer organizer = WebUpmsContext.getOrgnization(request);
        CommonAccount account = WebUpmsContext.getAccount(request);

        if (organizer == null || account == null) {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
        Orders parentOrder = ordersService.getOrderDetailByCode(code);

        try {

            List<RentBalance> rentBalances = depositService.getRentBalanceByOrderCode(organizer.getOrganizerId(), parentOrder.getCode());
            Map<String, Object> result = new HashMap<>();
            result.put("list", rentBalances);
            result.put("parentOrder", parentOrder);
            if (null == depositService.getDepositDetailByCode(parentOrder.getCode())) {
                result.put("deposit", null);
            } else {
                result.put("deposit", depositService.getDepositDetailAllByCode(parentOrder.getCode()));
            }
            return WebResult.getSuccessResult("data", result);

        } catch (Exception e) {
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }
    }

    @ApiOperation(value = "装备详情", notes = "装备详情,供打印使用", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "detailId", value = "ID", required = true, dataType = "int", paramType = "query")
    })
    @RequestMapping(value = "/detail.json", method = RequestMethod.GET)
    public WebResult getRentDetailByDetailID(@RequestParam("detailId") Integer detailId) {
        try {
            return WebResult.getSuccessResult("data", depositService.getIndemnityInfoBydetailId(detailId));
        } catch (Exception e) {
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }

    }

    @ApiOperation(value = "处理异常", notes = "处理异常", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "detailId", value = "ID", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "code", value = "订单号", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "indemnity", value = "赔偿金额", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "type", value = "种类（可使用 归还）", required = true, dataType = "int", paramType = "query")
    })
    @RequestMapping("/deal-exception.json")
    public WebResult dealException(@RequestParam("detailId") Integer detailId,
                                   @RequestParam("code") String code,
                                   @RequestParam("indemnity") BigDecimal indemnity,
                                   @RequestParam("type") Integer type,
                                   HttpServletRequest request) throws Exception {

        Organizer organizer = WebUpmsContext.getOrgnization(request);
        CommonAccount account = WebUpmsContext.getAccount(request);
        if (organizer == null || account == null) {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }

        OrderView orders = ordersService.getOrderViewByCode(code);

        depositService.dealException(detailId, indemnity, type, account, orders, null);

        EquipmentRelationships equipmentRelationships = depositService.getEquipmentRelationshipsByOrderId(orders.getOrderId());

        depositService.check(equipmentRelationships.getRelationId());

        return WebResult.getSuccessResult();
    }

    @ApiOperation(value = "处理赔偿", notes = "处理异常", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "detailId", value = "ID", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "info", value = "信息", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "indemnity", value = "赔偿金额", required = true, dataType = "int", paramType = "query"),
    })
    @RequestMapping("/indemnity.json")
    public WebResult dealIndemnity(@RequestParam("detailId") Integer detailId,
                                   @RequestParam("indemnity") BigDecimal indemnity,
                                   @RequestParam("info") String info,
                                   @RequestParam(value = "type", defaultValue = "2") Integer type,
                                   HttpServletRequest request) throws Exception {

        Organizer organizer = WebUpmsContext.getOrgnization(request);
        CommonAccount account = WebUpmsContext.getAccount(request);
        if (organizer == null || account == null) {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }

        RentDetailView rentDetailView = depositService.getRentDetailViewByDetailId(detailId);

        OrderView orders = ordersService.getOrderDetail(rentDetailView.getOrderId());

        depositService.dealException(detailId, indemnity, type, account, orders, info);

        EquipmentRelationships equipmentRelationships = depositService.getEquipmentRelationshipsByOrderId(orders.getOrderId());

        depositService.check(equipmentRelationships.getRelationId());

        return WebResult.getSuccessResult();
    }

    @ApiOperation(value = "获取赔偿信息，用于打印", notes = "获取赔偿信息，用于打印", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "detailId", value = "详情ID", required = true, dataType = "integer", paramType = "query"),
    })
    @RequestMapping(value = "/printInfo.json", method = RequestMethod.GET)
    public WebResult printInfo(@RequestParam("detailId") Integer detailId) {
        RentDetailView rentDetailView = depositService.getRentDetailViewByDetailId(detailId);
        OrderView parentOrder = ordersService.getOrderViewByCode(rentDetailView.getParentCode());
        OrderView orderView = ordersService.getOrderDetail(rentDetailView.getOrderId());
        OrderView mainOrder = ordersService.getDefaultChildOrderView(parentOrder.getOrderId());
        if (mainOrder == null) {
            mainOrder = orderView;
        }
        Pay pay;

        pay = ordersService.getPayInfoByOrderId(mainOrder.getOrderId());

        Map<String, Object> resultMap = new HashMap<>();
        Map<String, String> buyer = new HashMap<>();
        resultMap.put("code", parentOrder.getCode());
        resultMap.put("payType", pay.getPayType());
        resultMap.put("payTime", DateUtil.dateToDateString(pay.getTime()));

        if (ActivityBeanUtils.OnlinePayNameEnum.containPayTypeId(pay.getPayType())) {
            AccountResult account = accountService.getAccount(AccountEnum.FetchAccountTypeEnum.FETCH_BY_CUSTOMER.getValue(), String.valueOf(mainOrder.getBuyerId()));
            CommonAccount commonAccount = (CommonAccount) account.getData();
            buyer.put("buyerName", commonAccount.getNickname());
            buyer.put("buyerPhone", commonAccount.getPhone());
            resultMap.put("buyer", buyer);
        } else {
            List players = playerService.getPlayersByOrder(parentOrder.getOrderId());
            if (null == players || players.size() == 0) {
                players = playerService.getPlayersByOrder(mainOrder.getOrderId());
                if (null != players && players.size() > 0) {
                    Player player = (Player) players.get(0);
                    buyer.put("buyerName", player.getRealName());
                    buyer.put("buyerPhone", player.getPhone());
                    resultMap.put("buyer", buyer);
                }
            } else {
                Player player = (Player) players.get(0);
                buyer.put("buyerName", player.getRealName());
                buyer.put("buyerPhone", player.getPhone());
                resultMap.put("buyer", buyer);
            }
        }
        resultMap.put("cashierName", rentDetailView.getOperatorName());
        RentDetail rentDetail = depositService.getRentDetailByDetailID(detailId);
        resultMap.put("indemnity", rentDetail.getIndemnity());

        resultMap.put("createTime", DateUtil.dateToDateString(parentOrder.getCreateTime()));
        Activity activity = activityService.getActivityById(mainOrder.getActivityId());
        resultMap.put("activityName", activity.getTitle());
        resultMap.put("chargeName", orderView.getChargeName());
        resultMap.put("deposit", parentOrder.getDeposit());
        resultMap.put("note", rentDetail.getOtherInfo());


        return WebResult.getSuccessResult("data", resultMap);
    }

    @ApiOperation(value = "获取租赁物的操作记录", notes = "获取租赁物的操作记录", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "detailId", value = "详情ID", required = true, dataType = "integer", paramType = "query"),
    })
    @RequestMapping(value = "/history.json", method = RequestMethod.GET)
    public WebResult historyList(@RequestParam("detailId") Integer detailId, HttpServletRequest request) {
        CommonAccount account = WebUpmsContext.getAccount(request);
        if (account == null) {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
        return WebResult.getSuccessResult("data", depositService.getHistoryListByDetailId(detailId));
    }

    @ApiOperation(value = "获取剩余押金金额", notes = "获取剩余押金金额", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "code", value = "订单号", required = true, dataType = "string", paramType = "query"),
    })
    @RequestMapping(value = "/deposit.json", method = RequestMethod.GET)
    public WebResult deposit(@RequestParam("code") String code, HttpServletRequest request) {
        CommonAccount account = WebUpmsContext.getAccount(request);
        if (account == null) {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
        BigDecimal result = depositService.getRefundAmount(code);
        return WebResult.getSuccessResult("data", null == result ? 0 : result);
    }

    @ApiOperation(value = "更新租赁物状态", notes = "更新租赁物状态", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "rentDetailIds", value = "租赁物id列表（,分隔）", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "status", value = "状态", required = true, dataType = "integer", paramType = "query"),
    })
    @RequestMapping(value = "/updateRentDetailStatus.json", method = RequestMethod.POST)
    public WebResult updateRentDetailStatus(@RequestParam("rentDetailIds") String rentDetailIds, @RequestParam("status") Integer status){
        String[] ids = rentDetailIds.split(",");
        depositService.updateRentDetailStatus(Arrays.asList(ids), status);
        return WebResult.getSuccessResult();
    }


}
