package com.zoo.zoomerchantadminweb.websocket.controller;

import com.zoo.web.bean.WebResult;
import com.zoo.zoomerchantadminweb.config.websocket.WebSocketEndPoint;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import javax.servlet.http.HttpSession;
import java.util.Map;

/**
 * Created by ljq on 2018/8/31.
 */
@Api(value = "/api/server/websocket/", tags = "主办方websocket接口", description = "使用websocket进行消息推送相关接口")
@RestController
@RequestMapping("/api/server/websocket")
public class WebSocketController {

    @Autowired
    private WebSocketEndPoint webSocketEndPoint;

    @ApiOperation(value = "教练后台websocket推送接口", notes = "向教练后台PC客户端推送消息", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "message", value = "要推送的消息", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "orgId", value = "主办方ID", required = true, dataType = "int", paramType = "query"),
    })
    @RequestMapping(value = "/push.json", method = RequestMethod.POST)
    public Object test(@RequestParam("message") String message,
                       @RequestParam("orgId") Integer orgId,
                       HttpSession session) throws Exception {
        Map<String, WebSocketSession> sessionMap = webSocketEndPoint.getSocketSessionMap(orgId);
        if (sessionMap != null && sessionMap.size() > 0) {
            TextMessage textMessage = new TextMessage(message);

            for (Map.Entry<String, WebSocketSession> entry : sessionMap.entrySet()) {
                try {
                    entry.getValue().sendMessage(textMessage);
                } catch (Exception e) {
                    // 异常信息
                    e.printStackTrace();
                    return WebResult.getErrorResult(WebResult.Code.ERROR);
                }
            }
            return WebResult.getSuccessResult("data", "已推送" + sessionMap.size() + "个连接");
        } else {
            return WebResult.getSuccessResult("data", "没有打开的教练后台客户端,没有可以推送的连接");
        }
    }
}
