package com.zoo.zoomerchantadminweb.product.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.zoo.activity.core.Status;
import com.zoo.activity.dao.model.*;
import com.zoo.activity.dao.nutz.pojo.ActivityRelation;
import com.zoo.activity.dao.nutz.pojo.CommonAccount;
import com.zoo.activity.dao.nutz.pojo.OtaRelation;
import com.zoo.activity.dao.nutz.pojo.RentRelation;
import com.zoo.activity.service.*;
import com.zoo.activity.util.DateUtil;
import com.zoo.activity.util.ZooNotify;
import com.zoo.activity.util.parnter.PartnerDispatcher;
import com.zoo.activity.vo.EpisodeModel;
import com.zoo.icenow.service.EnterService;
import com.zoo.ota.OtaConstant;
import com.zoo.partner.service.PartnerService;
import com.zoo.partner.service.PartnerSystemService;
import com.zoo.skifield.dao.model.Skifield;
import com.zoo.web.bean.WebResult;
import com.zoo.wechat.common.Config;
import com.zoo.zoomerchantadminweb.context.WebUpmsContext;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.math.MathContext;
import java.util.*;

@Api(value = "api/server/episode", tags = {"主办方场次接口"}, description = "主办方场次接口描述")
@RestController
@RequestMapping("api/server/episode")
public class EpisodeController {

    private static final Logger LOGGER = LoggerFactory.getLogger(EpisodeController.class);

    @Resource(name = "episodeService")
    private EpisodeService episodeService;

    @Resource(name = "chargeService")
    private ChargeService chargeService;

    @Resource(name = "otaService")
    private OtaService otaService;

    @Resource(name = "activityService")
    private ActivityService activityService;

    @Resource(name = "organizerService")
    private OrganizerService organizerService;

    @Resource(name = "nextIdService")
    private NextIdService nextIdService;

    @Autowired
    private OrdersService ordersService;

    @Autowired
    private MailService mailService;

    @Autowired
    private PartnerSystemService partnerSystemService;

    @Autowired
    private PartnerService partnerService;

    @Autowired
    private EnterService enterService;

    @Autowired
    private SkiFieldService skiFieldService;


    @ApiOperation(value = "产品详情下场次列表", notes = "根据activityId查询场次列表(分页操作;举办者只能看到自己举办场次的详情)", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "size", value = "条", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "activityId", value = "产品ID", required = true, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "rent", value = "是否为租赁产品", required = false, dataType = "integer", paramType = "query"),

    })
    @RequestMapping(value = "/list.json", method = RequestMethod.GET)
    public WebResult episodeList(@RequestParam(value = "page", required = false, defaultValue = "1") String page,
                                 @RequestParam(value = "size", required = false, defaultValue = "10") String size,
                                 @RequestParam("activityId") Integer activityId, @RequestParam(value = "rent", defaultValue = "0") Integer rent,
                                 HttpServletRequest request) {

        PageBounds pageBounds = new PageBounds();
        pageBounds.setPage(Integer.parseInt(page));
        pageBounds.setLimit(Integer.parseInt(size));
        Organizer organizer = WebUpmsContext.getOrgnization(request);

        if (null != organizer) {
            WebResult result = WebResult.getSuccessResult();
            result.putResult("data", episodeService.getEpisodeListByActivityId(activityId, organizer.getOrganizerId(), rent, Integer.parseInt(size), Integer.parseInt(page)));
            return result;
        } else {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }

    }

    @ApiOperation(value = "删除场次", notes = "根据episodeId删除场次", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "episodeId", value = "场次ID", required = true, dataType = "integer", paramType = "query"),
    })
    @RequestMapping(value = "/delete.json", method = RequestMethod.POST)
    public WebResult episodeDelete(@RequestParam("episodeId") Integer episodeId, HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        Episode episode = episodeService.getEpisodeDetailById(episodeId);
        if (episode.getPlayerCount() > 0) {
            return WebResult.getErrorResult(WebResult.Code.EPISODE_ALREADY_SINGED_UP);
        }
        List<RentRelation> rentRelations = episodeService.getRelationById(null, episodeId);
        if (null != rentRelations && rentRelations.size() > 0) {
            return WebResult.getErrorResult(WebResult.Code.EPISODE_BIND_INCREAMENT);
        }

        Integer orderCount = ordersService.getCourseOrderCountByEpisodeId(episodeId);

        if (null == orderCount || orderCount > 0) {
            return WebResult.getErrorMsgResult(WebResult.Code.EPISODE_ALREADY_SINGED_UP, "已生成订单，无法删除");
        }

        if (episodeService.deleteEpisodeWithOrganzier(episodeId, organizer.getOrganizerId())) {

            List<Charge> charges = chargeService.getChargeList(episodeId);
            for (Charge charge : charges) {
                List<OtaRelation> relations = (List<OtaRelation>) otaService.getOtaRelation(charge.getChargeId(), OtaConstant.OtaRelationEnum.productRelation.getRelationType(), OtaConstant.VALID, null, null, 1, 1000).getList();
                for (OtaRelation relation : relations) {
                    otaService.pushProductStatus(organizer, charge, OtaConstant.INVALID, relation.getOtaId());
                }
            }
            return WebResult.getSuccessResult();
        } else {
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }
    }


    @ApiOperation(value = "复制场次", notes = "目前只有活动、旅游、训练营、酒店预定支持复制功能", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "needEpisodeId", value = "被复制的场次ID", required = false, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "startTime", value = "开始时间", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "endTime", value = "结束时间", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "registerStart", value = "报名开始时间", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "registerDeadline", value = "报名结束时间", required = false, dataType = "string", paramType = "query"),
    })
    @RequestMapping(value = "/copy.json", method = RequestMethod.POST)
    public Object episodeCopy(@RequestParam(value = "needEpisodeId", required = false) Integer needEpisodeId,
                              @RequestParam(value = "startTime", required = false) String startTime,
                              @RequestParam(value = "endTime", required = false) String endTime,
                              @RequestParam(value = "registerStart", required = false) String registerStart,
                              @RequestParam(value = "registerDeadline", required = false) String registerDeadline,
                              HttpServletRequest request) {


        if (startTime == null || startTime.length() == 0) {
            return WebResult.getErrorResult(WebResult.Code.EPISODE_NEED_START_TIME);
        }

        if (endTime == null || endTime.length() == 0) {
            return WebResult.getErrorResult(WebResult.Code.EPISODE_NEED_END_TIME);
        }


        if (registerDeadline == null || registerDeadline.length() == 0) {
            return WebResult.getErrorResult(WebResult.Code.EPISODE_NEED_REGISTER_END_TIME);
        }

        try {
            //读取 need episode id 数据
            Episode needEpisodeDetail = episodeService.getEpisodeDetailById(needEpisodeId);
            if (needEpisodeDetail == null) {
                return WebResult.getErrorResult(WebResult.Code.ERROR);
            }

            //check 数据   activity  0 1 7 可以复制
            Activity activityDetail = activityService.getActivityById(needEpisodeDetail.getActivityId());

            if (!(activityDetail.getTypeId() == 0 ||
                    activityDetail.getTypeId() == 1 ||
                    activityDetail.getTypeId() == 5 ||
                    activityDetail.getTypeId() == 7 ||
                    activityDetail.getTypeId() == 6)) {
                return WebResult.getErrorResult(WebResult.Code.EPISODE_ACTIVITY_TYPE_ERROR);
            }

            Integer nextEpisodeId = nextIdService.getNextId("episode");
            //读取 charge 数据
            List<Charge> chargeList = chargeService.getChargeList(needEpisodeId);
            needEpisodeDetail.setEpisodeId(nextEpisodeId);
            needEpisodeDetail.setPlayerCount(0);
            needEpisodeDetail.setHint(0);
            needEpisodeDetail.setShare(0);

            //新建 episode 数据

            needEpisodeDetail.setStartTime(new Date(Long.parseLong(startTime)));
            needEpisodeDetail.setEndTime(new Date(Long.parseLong(endTime)));
            needEpisodeDetail.setRegisterDeadline(new Date(Long.parseLong(registerDeadline)));

            needEpisodeDetail.setRefundDeadline(null);

            if (StringUtils.isNotBlank(registerStart)) {
                needEpisodeDetail.setRegisterStart(new Date(Long.parseLong(registerStart)));
            } else {
                needEpisodeDetail.setRegisterStart(null);
            }

            if (needEpisodeDetail.getStartTime().getTime() < new Date().getTime()) {
                return WebResult.getErrorResult(WebResult.Code.EPISODE_START_TIME_BEFORE_NOW);
            }
            if (needEpisodeDetail.getEndTime().getTime() < new Date().getTime()) {
                return WebResult.getErrorResult(WebResult.Code.EPISODE_END_TIME_BEFORE_NOW);
            }
//            if (needEpisodeDetail.getRegisterStart() != null && needEpisodeDetail.getRegisterStart().getTime() < new Date().getTime()) {
//                return WebResult.getErrorResult(WebResult.Code.EPISODE_REGISTER_STAR_TIME_BEFORE_NOW);
//            }
            if (needEpisodeDetail.getRegisterDeadline().getTime() < new Date().getTime()) {
                return WebResult.getErrorResult(WebResult.Code.EPISODE_REGISTER_END_TIME_BEFORE_NOW);
            }
            if (needEpisodeDetail.getRegisterStart() != null && needEpisodeDetail.getRegisterStart().getTime() > needEpisodeDetail.getRegisterDeadline().getTime()) {
                return WebResult.getErrorResult(WebResult.Code.EPISODE_REGISTER_TIME_ERROR);
            }
            if (needEpisodeDetail.getRegisterDeadline().getTime() > needEpisodeDetail.getEndTime().getTime()) {
                return WebResult.getErrorResult(WebResult.Code.EPISODE_REGISTER_END_TIME_BEFORE_END);
            }
            if (needEpisodeDetail.getStartTime().getTime() > needEpisodeDetail.getEndTime().getTime()) {
                return WebResult.getErrorResult(WebResult.Code.EPISODE_END_TIME_BEFORE_START_TIME);
            }
            if (needEpisodeDetail.getRefundDeadline() != null && needEpisodeDetail.getRegisterDeadline().getTime() > needEpisodeDetail.getRefundDeadline().getTime()) {
                return WebResult.getErrorResult(WebResult.Code.EPISODE_END_TIME_BEFORE_REFUND_TIME);
            }


            //设置操作员
            needEpisodeDetail.setOperator(WebUpmsContext.getUserName(request));
            //设置epiosde状态
            needEpisodeDetail.setStatus(0);
            //插入episode 数据
            needEpisodeDetail.setCharges(chargeList);

            ListIterator<Charge> listIterator = chargeList.listIterator();

            //新建 charge 数据
            while (listIterator.hasNext()) {
                Charge charge = listIterator.next();
                charge.setChargeId(null);
                charge.setChargeCount(0);
                charge.setEpisodeId(nextEpisodeId);
            }

            if (episodeService.addEpisode(needEpisodeDetail, null, new ArrayList<Integer>(), null)) {
                return WebResult.getSuccessResult("data", nextEpisodeId);
            } else {
                return WebResult.getErrorResult(WebResult.Code.ERROR);
            }

        } catch (Exception e) {
            e.printStackTrace();
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }
    }


    /**
     * 场次报名暂停
     *
     * @return
     */
    @ApiOperation(value = "暂停报名", notes = "根据episodeId暂停报名", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "episodeId", value = "场次ID", required = true, dataType = "integer", paramType = "query"),
    })
    @RequestMapping(value = "/stopEnroll.json", method = RequestMethod.POST)
    public WebResult episodeStopEnroll(@RequestParam(value = "episodeId") Integer episodeId,
                                       HttpServletRequest request) {

        Organizer organizer = WebUpmsContext.getOrgnization(request);
        Organizer realOrg = organizerService.getOrganizerDetail(organizer.getOrganizerId());
        if (organizer != null) {
            Episode episode = episodeService.getEpisodeDetailById(episodeId);
            if (episode == null) {
                return WebResult.getErrorResult(WebResult.Code.ERROR);
            }
            if (!activityService.checkIsOrganizer(organizer.getOrganizerId(), episode.getActivityId())) {
                return WebResult.getErrorResult(WebResult.Code.NO_PERMISSION);
            } else {
                if (episode.getStatus() == 1 || episode.getStatus() == 2) {
                    episode.setStatus(5);
                    if (episodeService.saveEpisode(episode)) {
                        List<Charge> charges = chargeService.getChargeList(episodeId);
                        for (Charge charge : charges) {
                            List<OtaRelation> relations = (List<OtaRelation>) otaService.getOtaRelation(charge.getChargeId(), OtaConstant.OtaRelationEnum.productRelation.getRelationType(), OtaConstant.VALID, null, null, 1, 1000).getList();
                            for (OtaRelation relation : relations) {
                                otaService.pushProductStatus(organizer, charge, OtaConstant.INVALID, relation.getOtaId());
                            }
                        }
                        return WebResult.getSuccessResult();
                    } else {
                        return WebResult.getErrorResult(WebResult.Code.ERROR);
                    }
                } else {
                    return WebResult.getErrorResult(WebResult.Code.EPISODE_STOP_AGAIN);
                }
            }
        } else {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
    }

//    /**
//     * 场次报名继续
//     *
//     * @return
//     */
//    @ApiOperation(value = "继续报名", notes = "根据episodeId继续报名", httpMethod = "POST")
//    @ApiImplicitParams({
//            @ApiImplicitParam(name = "episodeId", value = "场次ID", required = true, dataType = "integer", paramType = "query"),
//    })
//    @RequestMapping(value = "/continueEnroll.json", method = RequestMethod.POST)
//    public Object episodeContinueEnroll(@RequestParam(value = "episodeId") Integer episodeId,
//                                        HttpServletRequest request) {
//        Organizer organizer = WebUpmsContext.getOrgnization(request);
//        Organizer realOrg = organizerService.getOrganizerDetail(organizer.getOrganizerId());
//        if (organizer != null) {
//            Episode episode = episodeService.getEpisodeDetailById(episodeId);
//            if (episode == null) {
//                return WebResult.getErrorResult(WebResult.Code.ERROR);
//            }
//            if (!activityService.checkIsOrganizer(organizer.getOrganizerId(), episode.getActivityId())) {
//                return WebResult.getErrorResult(WebResult.Code.NO_PERMISSION);
//            } else {
//                if (episode.getStatus() == 5) {
//                    if (episode.getPlayerLimit() > 0 && episode.getPlayerCount() >= episode.getPlayerLimit()) {
//                        // 已满员
//                        episode.setStatus(2);
//                    } else {
//                        // 有名额
//                        episode.setStatus(1);
//                    }
//
//                    if (episodeService.saveEpisode(episode)) {
//                        List<Charge> charges = chargeService.getChargeList(episodeId);
//                        for (Charge charge : charges) {
//                            List<OtaRelation> relations = (List<OtaRelation>) otaService.getOtaRelation(charge.getChargeId(), OtaConstant.OtaRelationEnum.productRelation.getRelationType(), null, null, null, 1, 1000).getList();
//                            for (OtaRelation relation : relations) {
//                                otaService.pushProductStatus(organizer, charge, OtaConstant.VALID, relation.getOtaId());
//                            }
//                        }
//                        return WebResult.getSuccessResult();
//                    } else {
//                        return WebResult.getErrorResult(WebResult.Code.ERROR);
//                    }
//                } else {
//                    return WebResult.getErrorResult(WebResult.Code.EPISODE_START_AGAIN);
//                }
//            }
//        } else {
//            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
//        }
//    }

    /**
     * 目前发布场次的时候 只是修改了场次的状态 并没有修改活动的状态
     *
     * @return
     */
    @ApiOperation(value = "场次发布", notes = "根据episodeId发布场次", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "episodeId", value = "场次ID", required = true, dataType = "integer", paramType = "query"),
    })
    @RequestMapping(value = "/publish.json", method = RequestMethod.POST)
    public Object episodePublish(@RequestParam(value = "episodeId") Integer episodeId,
                                 HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer != null) {
            Episode episode = episodeService.getEpisodeDetailById(episodeId);
            if (episode == null) {
                return WebResult.getErrorResult(WebResult.Code.ERROR);
            }
            if (!activityService.checkIsOrganizer(organizer.getOrganizerId(), episode.getActivityId())) {
                return WebResult.getErrorResult(WebResult.Code.NO_PERMISSION);
            } else {

                List<Charge> validChargeList = chargeService.getValidChargeList(episodeId);

                if (null == validChargeList || validChargeList.size() == 0) {
                    return WebResult.getErrorMsgResult(WebResult.Code.ERROR, "至少保证一种票种在售卖");
                }

                if (episode.getStatus() == 0) {
                    episode.setStatus(1);
                    if (episode.getRegisterStart() == null) {
                        episode.setRegisterStart(new Date());
                    }
                    if (episodeService.saveEpisode(episode)) {
                        Activity activity = activityService.getActivityById(episode.getActivityId());
                        activity.setPublishTime(new Date());
                        activityService.updateActivityBySelective(activity);
                        episodeService.registTask(episode);
//                        List<String> notifyTarget = new ArrayList<>();
//                        notifyTarget.add("isnowadmin@huaxuezoo.com");
//                        ZooNotify.notifyByEmail(notifyTarget, "发布-滑雪族", "有新的产品发布，请前往http://" + Config.instance().getDomain() + "/manage/activity-detail?activityId=" + episode.getActivityId() + "  进行处理。");
                        mailService.sendMail("isnowadmin@huaxuezoo.com", "发布-滑雪族", "有新的产品发布，请前往http://" + Config.instance().getDomain() + "/manage/activity-detail?activityId=" + episode.getActivityId() + "  进行处理。");
                        List<Charge> charges = chargeService.getChargeList(episodeId);
                        for (Charge charge : charges) {
                            List<OtaRelation> relations = (List<OtaRelation>) otaService.getOtaRelation(charge.getChargeId(), OtaConstant.OtaRelationEnum.productRelation.getRelationType(), OtaConstant.VALID, null, null, 1, 1000).getList();
                            for (OtaRelation relation : relations) {
                                otaService.pushProductStatus(organizer, charge, OtaConstant.VALID, relation.getOtaId());
                            }
                        }
                        return WebResult.getSuccessResult();
                    } else {
                        return WebResult.getErrorResult(WebResult.Code.ERROR);
                    }
                } else if (episode.getStatus() == 5) {
                    if (episode.getPlayerLimit() > 0 && episode.getPlayerCount() >= episode.getPlayerLimit()) {
                        // 已满员
                        episode.setStatus(2);
                    } else {
                        // 有名额
                        episode.setStatus(1);
                    }
                    if (episodeService.saveEpisode(episode)) {
                        List<Charge> charges = chargeService.getChargeList(episodeId);
                        for (Charge charge : charges) {
                            List<OtaRelation> relations = (List<OtaRelation>) otaService.getOtaRelation(charge.getChargeId(), OtaConstant.OtaRelationEnum.productRelation.getRelationType(), null, null, null, 1, 1000).getList();
                            for (OtaRelation relation : relations) {
                                otaService.pushProductStatus(organizer, charge, OtaConstant.VALID, relation.getOtaId());
                            }
                        }
                        return WebResult.getSuccessResult();
                    } else {
                        return WebResult.getErrorResult(WebResult.Code.ERROR);
                    }
                } else if (episode.getStatus() == -1) {
                    return WebResult.getErrorResult(WebResult.Code.EPISODE_NOT_EDIT);
                } else {
                    return WebResult.getErrorResult(WebResult.Code.EPISODE_PUBLISH_AGAIN);
                }
            }
        } else {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
    }

    @ApiOperation(value = "场次详情", notes = "根据episodeId获取场次详情", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "episodeId", value = "场次ID", required = true, dataType = "integer", paramType = "query"),
    })
    @RequestMapping(value = "/detail.json", method = RequestMethod.POST)
    public WebResult episodeDetail(@RequestParam("episodeId") Integer episodeId, HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        Episode episode = episodeService.getEpisodeDetailWithChargesAndMessagesById(episodeId, organizer.getOrganizerId());
        if (episode.getSkiFieldId() != null && episode.getSkiFieldId() == 0) {
            episode.setSkiFieldId(null);
        }

        Activity activity = activityService.getActivityById(episode.getActivityId());
        if (!organizer.getOrganizerId().equals(activity.getOrganizerId())) {
            return WebResult.getErrorResult(WebResult.Code.NO_PERMISSION);
        }
        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("episode", episode);
        resultMap.put("cardType", activity.getCardType());
        resultMap.put("activityName", activity.getTitle());
        if (activity.getCardType() == 2) {
            List<Map<String, Object>> relationList = new ArrayList<>();
            List<ActivityRelation> relations = activityService.getRelationIds(activity.getActivityId());
            for (ActivityRelation relation : relations) {
                Map<String, Object> relationMap = new HashMap<>();
                relationMap.put("organizerId", relation.getOrganizerId());
                relationMap.put("organizerName", organizerService.getOrganizerDetail(relation.getOrganizerId()).getName());
                relationList.add(relationMap);
            }
            resultMap.put("relation", relationList);
        }
        return WebResult.getSuccessResult("data", resultMap);
    }

    /**
     * 传入一个episodeModel对象 如果传入对象中包含episodeId的参数 执行修改操作</br>
     * 否则执行添加操作
     *
     * @param episodeModel
     */
    @ApiOperation(value = "场次保存", notes = "传入一个episodeModel对象 如果传入对象中包含episodeId的参数 执行修改操作", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "relationVerifyCode", value = "核销码（套票使用）", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "editMode", value = "编辑模式", required = true, dataType = "int", paramType = "query"),
    })
    @RequestMapping(value = "/save.json", method = RequestMethod.POST)
    public WebResult episodeSave(@RequestParam(value = "relationVerifyCode", required = false, defaultValue = "0") String relationVerifyCode, EpisodeModel episodeModel,
                                 @RequestParam(value = "editMode", required = false, defaultValue = "0") String editMode,
                                 HttpServletRequest request) {

        Organizer organizer = WebUpmsContext.getOrgnization(request);
        OrganizerWithScenic organizerWithScenic = organizerService.getAllInfoById(organizer.getOrganizerId());

        Episode episode = new Episode();
        Date now = new Date();
        double total = 0;

        episode.setActivityId(episodeModel.getActivityId());
        episode.setEpisodeId(episodeModel.getEpisodeId());
        episode.setName(episodeModel.getName());
        episode.setVoucherSend(episodeModel.getVoucherSend());

        if (episodeModel.getSkiFieldId() == null) {
            episode.setProvinceId((null != organizerWithScenic.getProvince() && organizerWithScenic.getProvince() != 0 && organizerWithScenic.getProvince() != 1) ? organizerWithScenic.getProvince() : 2);
            episode.setCityId((null != organizerWithScenic.getCity() && organizerWithScenic.getCity() != 0 && organizerWithScenic.getCity() != 1) ? organizerWithScenic.getCity() : 2);
            episode.setDistrictId((null != organizerWithScenic.getDistrict() && organizerWithScenic.getDistrict() != 0 && organizerWithScenic.getDistrict() != 1) ? organizerWithScenic.getDistrict() : 4);
            episode.setLocation(organizer.getAddress());
            episode.setLatitude(organizer.getLatitude());
            episode.setLongitude(organizer.getLongitude());
            episode.setSkiFieldId(0);
        } else {
            ZNSSkiField znsSkiField = skiFieldService.getSkiFieldById(episodeModel.getSkiFieldId());
            episode.setProvinceId(znsSkiField.getProvinceId());
            episode.setCityId(znsSkiField.getCityId());
            episode.setDistrictId(znsSkiField.getDistrictId());
            episode.setLocation(znsSkiField.getAddress());
            episode.setLatitude(znsSkiField.getLatitude());
            episode.setLongitude(znsSkiField.getLongitude());
            episode.setSkiFieldId(znsSkiField.getSkiFieldId());
        }


        episode.setNote(episodeModel.getNote());
        episode.setPlayerMeta(episodeModel.getPlayerMeta());
        episode.setPlayerLimit(episodeModel.getPlayerLimit());
        episode.setWechatGroupUrl(episodeModel.getWechatGroupUrl());
        episode.setAdvanceBookingTime(episodeModel.getAdvanceBookingTime());
        episode.setAdvanceBookingType(episodeModel.getAdvanceBookingType());
        episode.setInviteCode(null != episodeModel.getInviteCode() ? episodeModel.getInviteCode() : "");

        if (episodeModel.getDateOfUse() != null && episodeModel.getDateOfUse() == 1) {
            if (editMode.equals("0")) {
                episode.setStartTime(now);
            } else {
                episode.setStartTime(null == episodeModel.getStartTime() ? now : new Date(Long.parseLong(episodeModel.getStartTime())));
            }
            episode.setEndTime(new Date(Long.parseLong("32503651200000")));
        } else {
            episode.setStartTime(new Date(Long.parseLong(episodeModel.getStartTime())));
            episode.setEndTime(new Date(Long.parseLong(episodeModel.getEndTime())));
        }
        episode.setNoticePhone(episodeModel.getNoticePhone());
        episode.setNoticeEmail(episodeModel.getNoticeEmail());
        episode.setNeedPlayerCount(episodeModel.getNeedPlayerCount());
        episode.setNeedAppointmentDate(episodeModel.getNeedAppointmentDate());
        episode.setDurationType(episodeModel.getDurationType());
        episode.setDuration(episodeModel.getDuration());
        episode.setRegisterDeadline(new Date(Long.parseLong(episodeModel.getRegisterDeadline())));

        if (StringUtils.isNotBlank(episodeModel.getRegisterStart()) && !episodeModel.getRegisterStart().equals("0")) {
            episode.setRegisterStart(new Date(Long.parseLong(episodeModel.getRegisterStart())));
        } else {
            episode.setRegisterStart(null);
        }
        episode.setRegisterDeadline(new Date(Long.parseLong(episodeModel.getRegisterDeadline())));
        if (StringUtils.isNotBlank(episodeModel.getRefundDeadline())) {
            episode.setRefundDeadline(new Date(Long.parseLong(episodeModel.getRefundDeadline())));
        } else {
            episode.setRefundDeadline(null);
        }
        episode.setUsableTime(episodeModel.getUsableTime());
        /**
         *  编辑模式判断
         *  0 创建，1 编辑，2 暂停时编辑，3 部分编辑
         */
        if (editMode.equals("0") || editMode.equals("1")) {

            if (episode.getEndTime().getTime() < now.getTime()) {
                return WebResult.getErrorResult(WebResult.Code.EPISODE_END_TIME_BEFORE_NOW);
            }
            if (episode.getRegisterStart() != null && episode.getRegisterStart().getTime() < now.getTime()) {
                return WebResult.getErrorResult(WebResult.Code.EPISODE_REGISTER_STAR_TIME_BEFORE_NOW);
            }
            if (episode.getRegisterDeadline().getTime() < now.getTime()) {
                return WebResult.getErrorResult(WebResult.Code.EPISODE_REGISTER_END_TIME_BEFORE_NOW);
            }
            if (episode.getRegisterStart() != null && episode.getRegisterStart().getTime() > episode.getRegisterDeadline().getTime()) {
                return WebResult.getErrorResult(WebResult.Code.EPISODE_REGISTER_TIME_ERROR);
            }
            if (episode.getRegisterDeadline().getTime() > episode.getEndTime().getTime()) {
                return WebResult.getErrorResult(WebResult.Code.EPISODE_REGISTER_END_TIME_BEFORE_END);
            }
            if (episode.getStartTime().getTime() > episode.getEndTime().getTime()) {
                return WebResult.getErrorResult(WebResult.Code.EPISODE_END_TIME_BEFORE_START_TIME);
            }
            if (episode.getRefundDeadline() != null && episode.getRegisterDeadline().getTime() > episode.getRefundDeadline().getTime()) {
                return WebResult.getErrorResult(WebResult.Code.EPISODE_END_TIME_BEFORE_REFUND_TIME);
            }
        }
        episode.setOperator(WebUpmsContext.getUserName(request));

        Activity activity = activityService.getActivityById(episodeModel.getActivityId());

        if (activity.getTypeId() == 6 || activity.getTypeId() == 3) {
            episode.setAdvanceRefundTime(episodeModel.getAdvanceRefundTime());
            episode.setAdvanceRefundType("day");
        }

//        if (activity.getTypeId() == 5 || activity.getTypeId() == 0 || activity.getTypeId() == 1 || activity.getTypeId() == 7) {
//            if (StringUtils.isNotBlank(episodeModel.getVerifyCode())) {
//                episode.setVerifyFlag(1);
//                episode.setVerifyCode(episodeModel.getVerifyCode());
//            } else {
//                if (activity.getTypeId() == 9 || activity.getTypeId() == 10) { // 租赁 和 储值产品 都为手动核销
//                    episode.setVerifyFlag(1);
//                } else {
//                    episode.setVerifyFlag(0);
//                    episode.setVerifyCode("");
//                }
//            }
//        } else {
//            episode.setVerifyFlag(1);
//            episode.setVerifyCode("");
//        }

        episode.setVerifyFlag(1);
        if (activity.getTypeId() == 0) {
            if (StringUtils.isNotBlank(episodeModel.getVerifyCode())) {
                episode.setVerifyCode(episodeModel.getVerifyCode());
            }
        } else {
            episode.setVerifyCode("");
        }

        if (null != relationVerifyCode && !relationVerifyCode.equals("0")) {
            episode.setVerifyFlag(1);
            JSONArray relation = JSONArray.parseArray(relationVerifyCode);
            for (Object obj : relation) {
                String s = (String) obj;
                String[] relationStrings = s.split("#");
                Integer orgId = Integer.parseInt(relationStrings[0]);
                String verifyCode = relationStrings[1];
                if (orgId.equals(organizer.getOrganizerId())) {
                    episode.setVerifyCode(verifyCode);
                }
            }
        }
        if (StringUtils.isNotBlank(episodeModel.getCharges())) {
            List<Charge> charges = new ArrayList<>();
            List<Map> chargesTmp = JSON.parseArray(episodeModel.getCharges(), Map.class);
            for (Map chargeTmp : chargesTmp) {
                Charge charge = JSON.parseObject(JSON.toJSONString(chargeTmp), Charge.class);
                if (null == charge.getChargeMode()) {
                    charge.setChargeMode(-1);
                }
                if (null == charge.getChargeNumber()) {
                    charge.setChargeNumber(0);
                }
                if (charge.getOriginPrice() == null) {
                    charge.setOriginPrice(new BigDecimal(0));
                }
                if (episode.getRefundDeadline() != null) {
                    charge.setIsPayback(1);
                } else {
                    charge.setIsPayback(0);
                }
                if (charge.getEquipmentId() != null) {
                    charge.setChargeName(episodeService.getEquipmentName(charge.getEquipmentId()) + "租赁");
                }
                if (charge.getValid() == 1) {
                    total = total + charge.getPrice().doubleValue();
                }

                JSONObject ob = new JSONObject();
                Integer sellLimitType = MapUtils.getInteger(chargeTmp, "sellLimitType", null);
                charge.setSellLimitType(sellLimitType);
                if(sellLimitType != null && (sellLimitType == 1 || sellLimitType == 2)) {
                    JSONObject jsonObject = new JSONObject();
                    JSONArray sellLimit = (JSONArray) chargeTmp.get("sellLimit");
                    Integer sellDayCount = MapUtils.getInteger(chargeTmp, "sellDayCount", -1);
                    Integer sellAllCount = MapUtils.getInteger(chargeTmp, "sellAllCount", -1);
                    jsonObject.put("sellLimitTime", sellLimit);
                    jsonObject.put("sellDayCount", sellDayCount);
                    jsonObject.put("sellAllCount", sellAllCount);
                    charge.setChargeLimit(MapUtils.getInteger(chargeTmp, "sellAllCount", 0));
                    ob.put("sellLimit", jsonObject);
                } else {
                    charge.setChargeLimit(0);
                }

                Integer customerLimitType = MapUtils.getInteger(chargeTmp, "customerLimitType", null);
                charge.setBuyLimitType(customerLimitType);
                if(customerLimitType != null && customerLimitType == 1) {
                    JSONObject jsonObject = new JSONObject();
                    JSONArray customerLimit = (JSONArray) chargeTmp.get("customerLimit");
                    Integer customerBuyOrderCount = MapUtils.getInteger(chargeTmp, "customerBuyOrderCount", -1);
                    Integer customerBuyTicketCount = MapUtils.getInteger(chargeTmp, "customerBuyTicketCount", -1);
                    Integer customerBuyCount = MapUtils.getInteger(chargeTmp, "customerBuyCount", -1);
                    jsonObject.put("customerLimitTime", customerLimit);
                    jsonObject.put("customerBuyOrderCount", customerBuyOrderCount);
                    jsonObject.put("customerBuyTicketCount", customerBuyTicketCount);
                    jsonObject.put("customerBuyCount", customerBuyCount);
                    charge.setBuyLimitCount(MapUtils.getInteger(chargeTmp, "customerBuyCount", null));
                    ob.put("customerLimit", jsonObject);
                }
                if(!ob.isEmpty()){
                    charge.setLimitJson(JSONObject.toJSONString(ob));
                }

                charges.add(charge);
            }

            if ((activity.getTypeId() == 3 || activity.getTypeId() == 6) && activity.getRent() == 0) {
                PartnerSystem partnerSystem = partnerSystemService.getPartnerSystemByOrgId(organizer.getOrganizerId());
                if (null != partnerSystem && partnerSystem.getInterfaceType() == 12) {
                    episode.setPlayerMeta("[{\"id\":\"Contacter\",\"title\":\"姓名\",\"type\":\"text\",\"placeHolder\":\"请填写真实姓名\",\"values\":[]},{\"id\":\"ContactPhone\",\"title\":\"手机号\",\"type\":\"text\",\"placeHolder\":\"手机号\",\"values\":[]},{\"id\":\"AfterAngle\",\"title\":\"后脚角度\",\"type\":\"text\",\"placeHolder\":\"后脚角度\",\"values\":[]},{\"id\":\"AfterValue\",\"title\":\"后脱离值\",\"type\":\"text\",\"placeHolder\":\"后脱离值\",\"values\":[]},{\"id\":\"DeliveryAddress\",\"title\":\"收货地址\",\"type\":\"text\",\"placeHolder\":\"收货地址\",\"values\":[]},{\"id\":\"Direction\",\"title\":\"站立方式\",\"type\":\"text\",\"placeHolder\":\"左脚在前/右脚在前\",\"values\":[]},{\"id\":\"FrontAngle\",\"title\":\"前脚角度\",\"type\":\"text\",\"placeHolder\":\"前脚角度\",\"values\":[]},{\"id\":\"FrontValue\",\"title\":\"前脱离值\",\"type\":\"text\",\"placeHolder\":\"前脱离值\",\"values\":[]},{\"id\":\"Height\",\"title\":\"身高\",\"type\":\"text\",\"placeHolder\":\"身高（CM）\",\"values\":[]},{\"id\":\"ShoeSize\",\"title\":\"鞋号\",\"type\":\"text\",\"placeHolder\":\"鞋号\",\"values\":[]},{\"id\":\"Spacing\",\"title\":\"两脚间距\",\"type\":\"text\",\"placeHolder\":\"两脚间距\",\"values\":[]},{\"id\":\"Weight\",\"title\":\"体重\",\"type\":\"text\",\"placeHolder\":\"体重（KG）\",\"values\":[]},{\"id\":\"HeadSize\",\"title\":\"头围\",\"type\":\"select\",\"placeHolder\":\"\",\"values\":[{\"key\":\"大\",\"value\":\"大\"},{\"key\":\"中\",\"value\":\"中\"},{\"key\":\"小\",\"value\":\"小\"}]},{\"id\":\"SkillLevel\",\"title\":\"滑雪水平\",\"type\":\"select\",\"placeHolder\":\"\",\"values\":[{\"key\":\"0\",\"value\":\"初级\"},{\"key\":\"1\",\"value\":\"中级\"},{\"key\":\"2\",\"value\":\"高级\"}]}]");
                }
                if (null != partnerSystem && null != partnerSystem.getInterfaceType() && partnerSystem.getInterfaceType() > 0 && partnerSystem.getInterfaceType() != 12 && partnerSystem.getInterfaceType() != 15 && partnerSystem.getInterfaceType() != 16) {
                    //要从大东或鑫海处去获取票信息
                    Map<String, Object> result = partnerService.getPartnerProduct(charges, partnerSystem, episode);
                    System.out.println("第三方系统保存产品信息--------------------------------------：" + JSON.toJSONString(result));
                    if (!result.get("result").equals(Status.success)) {
                        return WebResult.getErrorMsgResult(WebResult.Code.ERROR, (String) result.get("msg"));
                    } else {
                        if (partnerSystem.getInterfaceType().equals(5) && result.size() > 2) {
                            Date beginDate = DateUtil.wxTimeToDateTime(result.get("beginDate").toString());
                            Date endDate = DateUtil.wxTimeToDateTime(result.get("endDate").toString());
                            if (null != beginDate) {
                                if (episode.getStartTime().before(beginDate)) {
                                    return WebResult.getErrorMsgResult(WebResult.Code.ERROR, "商品开始时间错误");
                                }
                            }
                            if (episode.getEndTime().after(endDate)) {
                                return WebResult.getErrorMsgResult(WebResult.Code.ERROR, "商品结束时间错误");
                            }
                        }
                        if (partnerSystem.getInterfaceType().equals(6) && result.size() > 2) {
                            Date beginDate = DateUtil.wxTimeToDateTime(result.get("beginDate").toString());
                            Date endDate = DateUtil.wxTimeToDateTime(result.get("endDate").toString());
                            if (episode.getStartTime().before(beginDate)) {
                                return WebResult.getErrorMsgResult(WebResult.Code.ERROR, "商品开始时间错误");
                            }
                            if (episode.getEndTime().after(endDate)) {
                                return WebResult.getErrorMsgResult(WebResult.Code.ERROR, "商品结束时间错误");
                            }
                        }
//                        if (partnerSystem.getInterfaceType().equals(8) && result.size() > 2) {
//                            List<Map> otherInfoList = (ArrayList) result.get("otherInfo");
//                            for (Map<String, Object> otherInfo : otherInfoList) {
//                                JSONObject otherInfoJson = new JSONObject();
//                                otherInfoJson.put("pp_ruleid", otherInfo.get("pp_ruleid"));
//                                otherInfoJson.put("profitcenter", otherInfo.get("profitcenter"));
//                                otherInfoJson.put("productclasskind", otherInfo.get("productclasskind"));
//                                String productId = otherInfo.get("productid").toString();
//                                for (Charge charge : charges) {
//                                    if (StringUtils.isNotBlank(charge.getAlias())) {
//                                        if (charge.getAlias().equals(productId)) {
//                                            charge.setOtherInfo(otherInfoJson.toString());
//                                        }
//                                    }
//                                }
//                            }
//                        }

                        if (partnerSystem.getInterfaceType().equals(18) && result.size() > 2) {
                            Date beginSellDate = new DateTime(result.get("beginSellDate")).toDate();
                            Date endSellDate = new DateTime(result.get("endSellDate")).toDate();
                            if ((episode.getRegisterStart() != null ? episode.getRegisterStart() : new Date()).before(beginSellDate)) {
                                return WebResult.getErrorMsgResult(WebResult.Code.ERROR, "商品开始时间错误,请参照昆仑系统对应产品售卖开始时间");
                            } else if (episode.getRegisterDeadline().after(endSellDate)) {
                                return WebResult.getErrorMsgResult(WebResult.Code.ERROR, "商品售卖结束时间错误,请参照昆仑系统对应产品售卖结束时间");
                            }
                        }
                        // 阳光绿州 智游宝 需要身份证信息
                        if (!partnerSystem.getInterfaceType().equals(10)
                                && !partnerSystem.getInterfaceType().equals(11) && !partnerSystem.getInterfaceType().equals(5)) {
                            episode.setPlayerMeta((String) result.get("playerMeta"));
                        }
                    }
                }
            }

            episode.setCharges(charges);
        }


        List<Integer> rentIncrementList = new ArrayList<>();
        if (StringUtils.isNotBlank(episodeModel.getRentRelationEpisodeList())) {
            List<Map> episodesTmp = JSON.parseArray(episodeModel.getRentRelationEpisodeList(), Map.class);
            for (Map episodeTmp : episodesTmp) {
                Integer rentEpisodeId = Integer.parseInt(episodeTmp.get("episodeId").toString());
                rentIncrementList.add(rentEpisodeId);
                total = total + episodeService.getPriceDepositTotal(rentEpisodeId).doubleValue();
            }
        }

        if (total == 0 && activity.getRent() != 1) {
            return WebResult.getErrorMsgResult(WebResult.Code.ERROR, "票种价格、租赁价格、租赁押金、增值价格总价不能为0");
        }

        try {
            if (episode.getEpisodeId() == null) {
                /*同步代码，保证线程安全*/
                synchronized (this) {
                    episode.setEpisodeId(episodeService.getNextEpisodeId());
                    /*创建episode的时候，将状态置为草稿*/
                    episode.setStatus(0);
                    if (episodeService.addEpisode(episode, null, rentIncrementList, relationVerifyCode)) {
                        /*当类型是教练的时候保存用户的选中规则*/
                        if (activityService.getActivityById(episodeModel.getActivityId()).getTypeId() == 2) {
                            List<String> chargeRules = JSON.parseArray(episodeModel.getChargeRules(), String.class);
                            if (chargeRules.size() > 0) {
                                episodeService.insertChargeRules(chargeRules, episode.getEpisodeId());
                            }
                        }
                        return WebResult.getSuccessResult();
                    }
                }
            } else {
                Integer status = episodeService.getEpisodeStatusById(episode.getEpisodeId());
                episode.setStatus(status == -1 ? 0 : status);
                episodeService.updateEpisode(episode, null, rentIncrementList);
                episodeService.updateTask(episode);
                /*当类型是教练的时候保存用户的选中规则*/
                if (activityService.getActivityById(episodeModel.getActivityId()).getTypeId() == 2) {
                    List<String> chargeRules = JSON.parseArray(episodeModel.getChargeRules(), String.class);
                    if (chargeRules.size() > 0) {
                        episodeService.insertChargeRules(chargeRules, episode.getEpisodeId());
                    }
                }
                return WebResult.getSuccessResult();
            }
        } catch (Exception e) {
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }
        return WebResult.getErrorResult(WebResult.Code.UPDATE_FAILED);
    }


    @ApiOperation(value = "判断有没有对接第三方", notes = "判断有没有对接第三方", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "activityId", value = "产品ID", required = true, dataType = "integer", paramType = "query"),
    })
    @RequestMapping(value = "/partner/check.json", method = RequestMethod.GET)
    public WebResult partnerCheck(@RequestParam("activityId") Integer activityId, HttpServletRequest request) {

        Activity activity = activityService.getActivityById(activityId);

        Organizer organizer = WebUpmsContext.getOrgnization(request);

        PartnerSystem partnerSystem = partnerSystemService.getPartnerSystemByOrgId(organizer.getOrganizerId());

        WebResult webResult = WebResult.getSuccessResult();

        if (activity.getTypeId() == 6 && partnerSystem != null && activity.getRent() == 0) {
            if (partnerSystem.getInterfaceType() == 0) {
                webResult.putResult("partner", "dajian");
            } else if (partnerSystem.getInterfaceType() == 10 || partnerSystem.getInterfaceType() == 11) {
                webResult.putResult("partner", "true");
            } else {
                webResult.putResult("partner", "true");
            }
        } else {
            webResult.putResult("partner", "false");
        }

        if (activity.getTypeId() == 6) {

            List<Map> locations = enterService.getGateLocationByOrganizer(organizer.getOrganizerId());

            webResult.putResult("location", locations);
        }

        return webResult;
    }

    @ApiOperation(value = "删除当前场次下的租赁物", notes = "删除当前场次下的租赁物", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "episodeId", value = "场次ID", required = true, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "rentEpisodeId", value = "租赁场次ID", required = true, dataType = "integer", paramType = "query"),
    })
    @RequestMapping(value = "/rent/delete.json", method = RequestMethod.POST)
    public WebResult deleteRentEpisode(@RequestParam("episodeId") Integer episodeId, @RequestParam("rentEpisodeId") Integer rentEpisodeId, HttpServletRequest request) {
        CommonAccount account = WebUpmsContext.getAccount(request);
        if (null == account) {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }

        return episodeService.deleteRentEpisode(episodeId, rentEpisodeId);
    }

    @ApiOperation(value = "查询场次下是否有租赁物", notes = "查询场次下是否有租赁物", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "episodeId", value = "场次ID", required = true, dataType = "integer", paramType = "query"),
    })
    @RequestMapping(value = "/rent/hasRent.json", method = RequestMethod.GET)
    public WebResult hasRent(@RequestParam("episodeId") Integer episodeId){
        List<Episode> rentIncrementEpisode = episodeService.getRentIncrementEpisode(episodeId);
        if(CollectionUtils.isEmpty(rentIncrementEpisode)){
            return WebResult.getSuccessResult("data", false);
        }
        return WebResult.getSuccessResult("data", true);
    }

    @RequestMapping(value = "/rent/hasRentOrIncrement.json", method = RequestMethod.GET)
    public WebResult hasRentOrIncrement(@RequestParam("episodeId") Integer episodeId){
        List<Episode> rentIncrementEpisode = episodeService.getRentIncrementEpisode(episodeId);
        if(!CollectionUtils.isEmpty(rentIncrementEpisode)){
            return WebResult.getSuccessResult("data", true);
        }
        List<Charge> chargeList = chargeService.getChargeList(episodeId);
        for (Charge charge : chargeList) {
            if(charge.getValid() == 1 && charge.getIncrement() == 1) {
                return WebResult.getSuccessResult("data", true);
            }
        }
        return WebResult.getSuccessResult("data", false);
    }

}
