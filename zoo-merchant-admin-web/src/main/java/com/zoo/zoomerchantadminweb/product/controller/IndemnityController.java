package com.zoo.zoomerchantadminweb.product.controller;

import com.zoo.activity.constant.ResponseData;
import com.zoo.activity.core.Status;
import com.zoo.activity.core.util.SystemSessionUtil;
import com.zoo.activity.dao.model.Activity;
import com.zoo.activity.dao.model.ActivityWithBLOBs;
import com.zoo.activity.dao.model.Organizer;
import com.zoo.activity.dao.nutz.pojo.CommonAccount;
import com.zoo.activity.service.ActivityService;
import com.zoo.icenow.dao.model.Entry;
import com.zoo.icenow.dao.model.Indemnity;
import com.zoo.icenow.service.EnterService;
import com.zoo.web.bean.WebResult;
import com.zoo.zoomerchantadminweb.context.WebUpmsContext;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Api(value = "api/server/indemnity", tags = {"主办方续时产品接口"}, description = "主办方续时产品接口描述")
@RestController
@RequestMapping("api/server/indemnity")
public class IndemnityController {

    @Resource(name = "activityService")
    ActivityService activityService;

    @Autowired
    private EnterService enterService;

    @ApiOperation(value = "主办方续时产品", notes = "主办方续时产品", httpMethod = "GET")
    @RequestMapping(value = "/activity.json", method = RequestMethod.GET)
    public WebResult rentManage(HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        Activity activity = activityService.getIndemnityActivity(organizer.getOrganizerId());
        return WebResult.getSuccessResult("data", activity);
    }

    @ApiOperation(value = "主办方续时产品添加", notes = "主办方续时产品添加、修改", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "indemnityActivityId", value = "ID", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "indemnityActivityTitle", value = "标题", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "indemnityActivityPostUrl", value = "海报", required = true, dataType = "string", paramType = "query")
    })
    @RequestMapping(value = "/activity/save.json", method = RequestMethod.POST)
    public WebResult addRentActivity(@RequestParam(value = "indemnityActivityId", required = false) Integer indemnityActivityId,
                                     @RequestParam("indemnityActivityTitle") String indemnityActivityTitle,
                                     @RequestParam("indemnityActivityPostUrl") String indemnityActivityPostUrl,
                                     HttpServletRequest request) {
        ActivityWithBLOBs activity = new ActivityWithBLOBs();
        activity.setTitle(indemnityActivityTitle);
        activity.setOrganizerId(WebUpmsContext.getOrgnization(request).getOrganizerId());
        activity.setTypeId(10);
        activity.setVisible(1);
        activity.setStatus(1);
        activity.setRent(0);
        activity.setPosterUrl(indemnityActivityPostUrl);
        boolean success;
        if (indemnityActivityId == null || indemnityActivityId == 0) {
            Activity exist = activityService.getIndemnityActivity(WebUpmsContext.getOrgnization(request).getOrganizerId());
            if (null != exist) {
                return WebResult.getErrorMsgResult(WebResult.Code.ERROR, "已经添加租赁产品，请刷新后重试~");
            }
            activity.setActivityId(activityService.getNextActivityId());
            activity.setCreateTime(new Date());
            success = activityService.addActivity(activity, null);
        } else {
            activity.setActivityId(indemnityActivityId);
            activity.setUpdateTime(new Date());
            success = activityService.updateActivity(activity);
        }
        if (success) {
            return WebResult.getSuccessResult();
        } else {
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }
    }

    @ApiOperation(value = "根据入园记录获取续时产品", notes = "根据入园记录获取续时产品", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "entryId", value = "入园ID", required = true, dataType = "int", paramType = "query")
    })
    @RequestMapping(value = "/product.json", method = RequestMethod.GET)
    public WebResult indemnityProductInfo(@RequestParam("entryId") Integer entryId, HttpServletRequest request) {
        Map<String, Object> resultMap = new HashMap<>();

        CommonAccount customer = WebUpmsContext.getAccount(request);
        Entry entry = enterService.getById(entryId);

        if (null == entry || entry.getStatus() != 3) {
            return WebResult.getErrorMsgResult(WebResult.Code.ERROR, "不需要续时");
        }

        Indemnity indemnity = enterService.getIndemnityByEntryIdAndStatus(entryId, 0);

        if (null == indemnity || indemnity.getStatus() != 0) {
            return WebResult.getErrorResult(WebResult.Code.UPDATE_FAILED);
        }

        resultMap.put("indemnityId", indemnity.getIndemnityId());
        Date now = new Date();

        Integer range = (int) (now.getTime() - entry.getMaxTime().getTime()) / 1000 / 60;

        /* 获取充值产品list */
        List<Map<String, Object>> productList = enterService.getIndemnityProductList(customer.getOrganizerId(), range);
        resultMap.put("productList", productList);


        return WebResult.getSuccessResult("data", resultMap);
    }
}
