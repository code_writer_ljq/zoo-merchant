package com.zoo.zoomerchantadminweb.product.controller;

import com.zoo.activity.dao.model.Charge;
import com.zoo.activity.dao.model.Episode;
import com.zoo.activity.dao.model.Organizer;
import com.zoo.activity.dao.nutz.pojo.CommonAccount;
import com.zoo.activity.dao.nutz.pojo.OtaRelation;
import com.zoo.activity.dao.nutz.pojo.RentRelation;
import com.zoo.activity.service.*;
import com.zoo.ota.OtaConstant;
import com.zoo.web.bean.WebResult;
import com.zoo.zoomerchantadminweb.context.WebUpmsContext;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

@Api(value = "api/server/charge", tags = {"主办方票券接口"}, description = "主办方票券接口描述")
@RestController
@RequestMapping("api/server/charge")
public class ChargeController {

    @Resource(name = "chargeService")
    private ChargeService chargeService;

    @Resource(name = "activityService")
    private ActivityService activityService;

    @Resource(name = "otaService")
    private OtaService otaService;

    @Resource(name = "orderService")
    private OrdersService ordersService;

    @Resource(name = "episodeService")
    private EpisodeService episodeService;


    @ApiOperation(value = "票种开放购买", notes = "票种开放购买", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "chargeId", value = "票种ID", required = true, dataType = "int", paramType = "query"),
    })
    @RequestMapping(value = "/start.json", method = RequestMethod.POST)
    public WebResult chargeStart(@RequestParam("chargeId") Integer chargeId, HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (null != organizer) {
            Charge charge = chargeService.getChargeById(chargeId);
            if (!activityService.checkIsOrganizer(organizer.getOrganizerId(), charge.getActivityId())) {
                return WebResult.getErrorResult(WebResult.Code.NO_PERMISSION);
            }
            charge.setValid(1);
            if (chargeService.updateCharge(charge)) {
                return WebResult.getSuccessResult();
            } else {
                return WebResult.getErrorResult(WebResult.Code.UPDATE_FAILED);
            }
        } else {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
    }

    @ApiOperation(value = "票种暂停购买", notes = "票种暂停购买", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "chargeId", value = "票种ID", required = true, dataType = "int", paramType = "query"),
    })
    @RequestMapping(value = "/stop.json", method = RequestMethod.POST)
    public WebResult chargeStop(@RequestParam("chargeId") Integer chargeId, HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (null != organizer) {
            Charge charge = chargeService.getChargeById(chargeId);
            if (!activityService.checkIsOrganizer(organizer.getOrganizerId(), charge.getActivityId())) {
                return WebResult.getErrorResult(WebResult.Code.NO_PERMISSION);
            }
            charge.setValid(0);
            if (chargeService.updateCharge(charge)) {
                List<OtaRelation> relations = (List<OtaRelation>) otaService.getOtaRelation(charge.getChargeId(), OtaConstant.OtaRelationEnum.productRelation.getRelationType(), OtaConstant.VALID, null, null, 1, 1000).getList();
                for (OtaRelation relation : relations) {
                    otaService.pushProductStatus(organizer, charge, OtaConstant.INVALID, relation.getOtaId());
                }
                return WebResult.getSuccessResult();
            } else {
                return WebResult.getErrorResult(WebResult.Code.UPDATE_FAILED);
            }
        } else {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
    }

    /**
     * 根据chargeId删除用户的票卷信息,当票卷已经有人购买的时候不能删除<br>
     * 并且只有活动主办方才有权限删除票卷信息
     *
     * @param chargeId 票卷ID
     */
    @ApiOperation(value = "删除票卷", notes = "根据chargeId删除用户的票卷信息,当票卷已经有人购买的时候不能删除", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "chargeId", value = "票种ID", required = true, dataType = "int", paramType = "query"),
    })
    @RequestMapping(value = "/delete.json", method = RequestMethod.POST)
    public WebResult chargeDelete(@RequestParam("chargeId") Integer chargeId, HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (null != organizer) {
            Charge charge = chargeService.getChargeById(chargeId);
            if (charge == null) {
                return WebResult.getErrorResult(WebResult.Code.DELTED_FAILED);
            }

            if (charge.getChargeCount() > 0) {
                return WebResult.getErrorResult(WebResult.Code.EPISODE_ALREADY_SINGED_UP);
            }

            //check orders表 如果charge 已经下单 不允许删除
            int countOrder = ordersService.countOrderByChargeId(charge.getChargeId());

            if (countOrder > 0) {
                return WebResult.getErrorResult(WebResult.Code.EPISODE_ALREADY_SINGED_UP);
            }

            if (!activityService.checkIsOrganizer(organizer.getOrganizerId(), charge.getActivityId())) {
                return WebResult.getErrorResult(WebResult.Code.NO_PERMISSION);
            }
            if (chargeService.deleteCharge(chargeId)) {

                List<OtaRelation> relations = (List<OtaRelation>) otaService.getOtaRelation(charge.getChargeId(), OtaConstant.OtaRelationEnum.productRelation.getRelationType(), OtaConstant.VALID, null, null, 1, 1000).getList();
                for (OtaRelation relation : relations) {
                    otaService.pushProductStatus(organizer, charge, OtaConstant.INVALID, relation.getOtaId());
                }
                return WebResult.getSuccessResult();
            } else {
                return WebResult.getErrorResult(WebResult.Code.DELTED_FAILED);
            }
        } else {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
    }

    @ApiOperation(value = "租赁物开放、暂停购买", notes = "租赁物开放、暂停购买", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "episodeId", value = "场次ID", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "rentEpisodeId", value = "租赁物场次ID", required = true, dataType = "int", paramType = "query"),
    })
    @RequestMapping(value = "/stop2.json", method = RequestMethod.POST)
    public WebResult chargeStop2(@RequestParam("episodeId") Integer episodeId, @RequestParam("rentEpisodeId") Integer rentEpisodeId, HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (null != organizer) {
            Episode episode = episodeService.getEpisodeDetailById(episodeId);
            if (!activityService.checkIsOrganizer(organizer.getOrganizerId(), episode.getActivityId())) {
                return WebResult.getErrorResult(WebResult.Code.NO_PERMISSION);
            }
            List<RentRelation> rentRelations = episodeService.getRelationById(episodeId, rentEpisodeId);
            for (RentRelation rentRelation : rentRelations) {
                rentRelation.setStatus(1 - rentRelation.getStatus());
            }
            if (episodeService.updateRelation(rentRelations)) {
                return WebResult.getSuccessResult();
            } else {
                return WebResult.getErrorResult(WebResult.Code.UPDATE_FAILED);
            }
        } else {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
    }

    @ApiOperation(value = "票券列表", notes = "根据episodeId获取票券列表", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "episodeId", value = "场次ID", required = true, dataType = "int", paramType = "query"),
    })
    @RequestMapping(value = "/chargeList.json", method = RequestMethod.POST)
    public WebResult getChargesByEpisodeId(@RequestParam("episodeId") Integer episodeId) {
        return WebResult.getSuccessResult("data", chargeService.getChargeList(episodeId));
    }

    @ApiOperation(value = "会员价格列表", notes = "获取该票种的会员价", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "chargeId", value = "票券ID", required = false, dataType = "int", paramType = "query"),
    })
    @RequestMapping(value = "/chargePriceList.json", method = RequestMethod.POST)
    public WebResult getChargesPriceListByEpisodeId(@RequestParam(value = "chargeId", required = false) Integer chargeId, HttpServletRequest request) {

        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (null == organizer) {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        } else {
            return WebResult.getSuccessResult("data", chargeService.getChargePriceList(chargeId, organizer.getOrganizerId()));
        }

    }

    @ApiOperation(value = "票务售卖情况", notes = "票务售卖情况", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "time", value = "售卖时间", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "sellType", value = "售卖模式（1：通用 2：日历）", required = true, dataType = "integer", paramType = "query")
    })
    @GetMapping(value = "/chargeSellDetail.json")
    public Object chargeSellDetail(@RequestParam String time, @RequestParam Integer sellType, HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if(organizer == null) return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        List<Map<String, Object>> data = chargeService.chargeSellDetail(time, sellType, organizer.getOrganizerId());
        return WebResult.getSuccessResult("data", data);
    }

    @PostMapping("/updateChargeLimit.json")
    public Object updateChargeLimit(@RequestParam Integer chargeId, @RequestParam String date, @RequestParam Integer count, HttpServletRequest request){
        CommonAccount account = WebUpmsContext.getAccount(request);
        chargeService.updateChargeLimit(chargeId, date, count, account.getCustomerId());
        return WebResult.getSuccessResult();
    }
}
