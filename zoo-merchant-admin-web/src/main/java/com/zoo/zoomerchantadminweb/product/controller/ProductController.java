package com.zoo.zoomerchantadminweb.product.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.zoo.activity.dao.model.*;
import com.zoo.activity.dao.nutz.pojo.ActivityRelation;
import com.zoo.activity.dao.nutz.pojo.OtaRelation;
import com.zoo.activity.service.*;
import com.zoo.activity.util.DateUtil;
import com.zoo.activity.util.OpenSearchUtil;
import com.zoo.activity.vo.ActivityModel;
import com.zoo.common.page.PageList;
import com.zoo.ota.OtaConstant;
import com.zoo.util.data.BeanUtilsExtends;
import com.zoo.web.bean.WebResult;
import com.zoo.zoomerchantadminweb.context.WebUpmsContext;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.text.ParseException;
import java.util.*;


@Api(value = "api/server/activity", tags = {"主办方产品接口"}, description = "主办方产品接口描述")
@RestController
@RequestMapping("api/server/activity")
public class ProductController {

    @Resource(name = "activityService")
    ActivityService activityService;

    @Resource(name = "organizerService")
    OrganizerService organizerService;

    @Resource(name = "episodeService")
    EpisodeService episodeService;

    @Resource(name = "chargeService")
    ChargeService chargeService;

    @Resource(name = "tagRelationShipsService")
    TagRelationShipsService tagRelationShipsService;

    @Autowired
    private OrdersService ordersService;

    @Autowired
    private OtaService otaService;

    @Autowired
    private RedisService redisService;

    //    @RequiresPermissions("product:read")
    @ApiOperation(value = "主办方产品列表", notes = "后台强c端使用的产品列表,会显示这个机构下面的所有的活动", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "size", value = "条", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "status", value = "状态", required = false, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "activityId", value = "产品ID", required = false, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "distribution", value = "是否为分销产品", required = false, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "cardType", value = "产品类型", required = false, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "startTime", value = "开始时间", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "endTime", value = "结束类型", required = false, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "offline", value = "是否开启线下", required = false, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "storage", value = "是否开启储值", required = false, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "visible", value = "是否对外显示", required = false, dataType = "integer", paramType = "query")
    })
    @RequestMapping(value = "/list.json", method = RequestMethod.GET)
    public WebResult activityList(@RequestParam(value = "page", required = false, defaultValue = "1") String page,
                                  @RequestParam(value = "size", required = false, defaultValue = "10") String size,
                                  @RequestParam(value = "status", required = false) Integer status,
                                  @RequestParam(value = "activityId", required = false) Integer activityId,
                                  @RequestParam(value = "distribution", required = false) Integer distribution,
                                  @RequestParam(value = "activityType", required = false) Integer activityType,
                                  @RequestParam(value = "cardType", required = false) Integer cardType,
                                  @RequestParam(value = "startTime", required = false) String startTime,
                                  @RequestParam(value = "endTime", required = false) String endTime,
                                  @RequestParam(value = "offline", required = false) Integer offline,
                                  @RequestParam(value = "storage", required = false) Integer storage,
                                  @RequestParam(value = "visible", required = false) Integer visible,

                                  HttpServletRequest request) {


        Organizer organizer = WebUpmsContext.getOrgnization(request);

        if (null == organizer) {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        } else {
            WebResult result = WebResult.getSuccessResult();
            result.putResult("data", activityService.getActivityList(activityId, null, organizer.getOrganizerId(), distribution, status, Integer.parseInt(size), Integer.parseInt(page), activityType, cardType, startTime, endTime, offline, storage, visible));
            return result;
        }

    }

    @ApiOperation(value = "主办方产品详情", notes = "主办方产品详情，不包含场次信息", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "activityId", value = "产品ID", required = true, dataType = "integer", paramType = "query"),
    })
    @RequestMapping(value = "/activityDetail.json", method = RequestMethod.GET)
    public WebResult activityDetail(@RequestParam(value = "activityId") Integer activityId, HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        ActivityWithBLOBs activity = activityService.getCustomerActivityDetailById(activityId, organizer.getOrganizerId());
        if (!activity.getOrganizerId().equals(organizer.getOrganizerId())) {
            return WebResult.getErrorResult(WebResult.Code.NO_PERMISSION);
        }

        WebResult result = WebResult.getSuccessResult();
        Map<String, Object> resulMap = new HashMap<>();
        if (activity.getTypeId() == 1) {
            String description = activity.getDescription();
            Map map = JSON.parseObject(description, Map.class);
            /*如果检查到活动类型是 旅游活动的时候  对description做处理*/
            activity.setDescription("目的地：" + map.get("1") + "<br> 出发地：" + map.get("2") + "<br>供应商：" + map.get("3"));
            resulMap.put("endPlace", map.get("1"));
            resulMap.put("startPlace", map.get("2"));
            resulMap.put("travelOrg", map.get("3"));
        }
        if (activity.getCardType() == 2) {
            StringBuffer sb = new StringBuffer();
            List<ActivityRelation> relations = activityService.getRelationIds(activity.getActivityId());
            for (ActivityRelation relation : relations) {
                if (!relation.getOrganizerId().equals(organizer.getOrganizerId())) {
                    sb.append(organizerService.getOrganizerDetail(relation.getOrganizerId()).getName()).append(",");
                } else {
                    sb.append(organizerService.getOrganizerDetail(relation.getOrganizerId()).getName()).append("(活动发布者)").append(",");
                }
            }
            resulMap.put("relation", sb.substring(0, sb.length() - 1));
        }


        OrganizerRenderActivity organizerRenderActivity = activityService.getActivityRender(activityId, organizer.getOrganizerId());
        OrganizerWithScenic organizerWithScenic = organizerService.getAllInfoById(organizer.getOrganizerId());
        if (organizerWithScenic.getDaytimeStart() != null && organizerWithScenic.getDaytimeStart() != 0 && organizerWithScenic.getDaytimeEnd() != null && organizerWithScenic.getDaytimeEnd() != 0) {
            resulMap.put("offline", "true");
        }
        resulMap.put("render", organizerRenderActivity == null ? 0 : organizerRenderActivity.getStatus());
        resulMap.put("activity", activity);
        resulMap.put("clientId", organizer.getOrganizerId());
        result.putResult("data", resulMap);
        return result;

    }

    @ApiOperation(value = "主办方产品详情2", notes = "主办方产品详情，不包含场次信息2", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "activityId", value = "产品ID", required = true, dataType = "integer", paramType = "query"),
    })
    @RequestMapping(value = "/detail.json", method = RequestMethod.GET)
    public Object activityDetailJson(@RequestParam("activityId") Integer activityId, HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer != null) {
            return WebResult.getSuccessResult("data", activityService.getActivityDetailById(activityId));
        } else {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
    }

    @ApiOperation(value = "删除产品", notes = "隐藏并不真正从库里删除", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "activityId", value = "产品ID", required = true, dataType = "integer", paramType = "body"),
    })
    @RequestMapping(value = "/delete.json", method = RequestMethod.POST)
    public WebResult doActivityDelete(@RequestParam("activityId") Integer activityId, HttpServletRequest request) throws
            Exception {
        Organizer organizer = WebUpmsContext.getOrgnization(request);

        ActivityWithBLOBs activityFormDB = activityService.getActivityById(activityId);
        if (!activityFormDB.getOrganizerId().equals(organizer.getOrganizerId())) {
            return WebResult.getErrorResult(WebResult.Code.NO_PERMISSION);
        }

        List<Episode> episodes = activityService.getEpisodeListByActId(activityId);

        for (Episode episode : episodes) {
            Integer orderCount = ordersService.getCourseOrderCountByEpisodeId(episode.getEpisodeId());
            if (null == orderCount || orderCount > 0) {
                return WebResult.getErrorMsgResult(WebResult.Code.EPISODE_ALREADY_SINGED_UP, "已生成订单，无法删除");
            }
        }

        if (activityService.deleteActivity(activityId, null)) {
            for (Episode episode : episodes) {
                if (episodeService.deleteEpisodeWithOrganzier(episode.getEpisodeId(), organizer.getOrganizerId())) {

                    List<Charge> charges = chargeService.getChargeList(episode.getEpisodeId());
                    for (Charge charge : charges) {
                        List<OtaRelation> relations = (List<OtaRelation>) otaService.getOtaRelation(charge.getChargeId(), OtaConstant.OtaRelationEnum.productRelation.getRelationType(), OtaConstant.VALID, null, null, 1, 1000).getList();
                        for (OtaRelation relation : relations) {
                            otaService.pushProductStatus(organizer, charge, OtaConstant.INVALID, relation.getOtaId());
                        }
                    }
                    return WebResult.getSuccessResult();
                } else {
                    return WebResult.getErrorResult(WebResult.Code.ERROR);
                }
            }
        } else {
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }

        /*删除活动的时候,删除openSearch的索引*/
//        OpenSearch search = new OpenSearch();
//        search.setActivity_id(activityId);
//        OpenSearchUtil.removeDoc(OpenSearchUtil.tableName, OpenSearchUtil.index, OpenSearchUtil.beanToMap(search));

        return WebResult.getSuccessResult();
    }

    @ApiOperation(value = "添加产品", notes = "参数为ActivityModel", httpMethod = "POST")
    @RequestMapping(value = "/add.json", method = RequestMethod.POST)
    public WebResult doActivityAdd(@Valid ActivityModel activityModel, HttpServletRequest request) throws
            Exception {
        ActivityWithBLOBs activity = new ActivityWithBLOBs();
        Organizer organizer = WebUpmsContext.getOrgnization(request);

        List<Integer> relationIds = null;
        BeanUtilsExtends.copyProperties(activity, activityModel);
        /*设置活动的创建时间*/
        activity.setCreateTime(new Date());
        if (organizer != null) {
            activity.setOrganizerId(organizer.getOrganizerId());
        }

        if (null != activityModel.getCardType() && activityModel.getCardType() == 2) {
            JSONArray relationOrg = JSONArray.parseArray(activityModel.getRelationOrgIds());
            relationIds = new ArrayList<>();
            for (Object relationId : relationOrg) {
                relationIds.add(Integer.parseInt((String) relationId));
            }
        }

        /*同步代码,保证线程安全*/
        synchronized (this) {
            Integer activityId = activityService.getNextActivityId();
            activity.setActivityId(activityId);
            activity.setUpdateTime(new Date());
            activity.setShowMess(activityModel.getShowMess());
            activity.setOperator(WebUpmsContext.getUserName(request));
            activity.setStatus(1);
            if (activityService.addActivity(activity, relationIds)) {
                /*自营票务*/
//                if (activity.getTypeId() == 6) {
//                    TagRelationships tagRelationships = new TagRelationships();
//                    tagRelationships.setTargetId(activityId);
//                    tagRelationships.setTagId(activityModel.getTag());
//                    tagRelationships.setWeight(0);
//                    tagRelationships.setTargetType(8);
//                    tagRelationShipsService.add(tagRelationships);
//                }

                /*创建活动的时候,添加openSearch的索引*/
//                List<OpenSearch> openSearches = activityService.getOpenSearchByOption(activity.getActivityId(), null, null);
//                for (OpenSearch openSearch : openSearches) {
//                    OpenSearchUtil.addDoc(OpenSearchUtil.tableName, OpenSearchUtil.index, OpenSearchUtil.beanToMap(openSearch));
//                }
                WebResult result = WebResult.getSuccessResult();
                result.putResult("data", activity.getActivityId());
                return result;
            } else {
                return WebResult.getErrorResult(WebResult.Code.ADD_FAILED);
            }
        }

    }

    @ApiOperation(value = "复制产品", notes = "复制产品", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "activityId", value = "产品ID", required = true, dataType = "integer", paramType = "body"),
    })
    @RequestMapping(value = "/copy.json", method = RequestMethod.POST)
    public Object copyActivity(@RequestParam(value = "activityId", required = true) Integer activityId, HttpServletRequest request) {
        Integer nextActivityId = activityService.getNextActivityId();
        List<Episode> originEpisodeList = activityService.getEpisodeListByActId(activityId);
        synchronized (this) {
            try {
                ActivityWithBLOBs activityDetail = activityService.getActivityById(activityId);
                activityDetail.setCreateTime(new Date());
                activityDetail.setActivityId(null);
                activityDetail.setActivityId(nextActivityId);
                if (!(activityDetail.getTypeId() == 0 ||
                        activityDetail.getTypeId() == 1 ||
                        activityDetail.getTypeId() == 5 ||
                        activityDetail.getTypeId() == 7 ||
                        activityDetail.getTypeId() == 6)) {
                    return WebResult.getErrorResult(WebResult.Code.EPISODE_ACTIVITY_TYPE_ERROR);
                }

                activityService.addActivity(activityDetail, null);

                for (Episode episode : originEpisodeList) {
                    int needEpisodeId = episode.getEpisodeId();
                    //读取 need episode id 数据
                    Episode needEpisodeDetail = episodeService.getEpisodeDetailById(needEpisodeId);
                    if (needEpisodeDetail == null) {
                        return WebResult.getErrorResult(WebResult.Code.ERROR);
                    }

                    Integer nextEpisodeId = episodeService.getNextEpisodeId();
                    //读取 charge 数据
                    List<Charge> chargeList = chargeService.getChargeList(needEpisodeId);
                    needEpisodeDetail.setEpisodeId(nextEpisodeId);
                    needEpisodeDetail.setPlayerCount(0);
                    needEpisodeDetail.setHint(0);
                    needEpisodeDetail.setShare(0);
                    needEpisodeDetail.setActivityId(nextActivityId);

                    //设置操作员
                    needEpisodeDetail.setOperator(WebUpmsContext.getUserName(request));
                    needEpisodeDetail.setStatus(0);
                    needEpisodeDetail.setCharges(chargeList);
                    ListIterator<Charge> listIterator = chargeList.listIterator();
                    while (listIterator.hasNext()) {
                        Charge charge = listIterator.next();
                        charge.setChargeId(null);
                        charge.setChargeCount(0);
                        charge.setEpisodeId(nextEpisodeId);
                        charge.setActivityId(nextActivityId);
                    }
                    if (!episodeService.addEpisode(needEpisodeDetail, null, new ArrayList<Integer>(), null)) {
                        return WebResult.getErrorResult(WebResult.Code.ADD_FAILED);
                    }
                }
                return WebResult.getSuccessResult("data", nextActivityId);
            } catch (Exception e) {
                return WebResult.getErrorResult(WebResult.Code.ERROR);
            }
        }
    }

    @ApiOperation(value = "编辑产品", notes = "参数为ActivityModel", httpMethod = "POST")
    @RequestMapping(value = "/edit.json", method = RequestMethod.POST)
    public WebResult activityEdit(@Valid ActivityModel activityModel, HttpServletRequest request) throws Exception {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        ActivityWithBLOBs activity = activityService.getActivityById(activityModel.getActivityId());

        if (!activity.getOrganizerId().equals(organizer.getOrganizerId())) {
            return WebResult.getErrorResult(WebResult.Code.NO_PERMISSION);
        } else {
            activity.setTitle(activityModel.getTitle());
            activity.setIntroduction(activityModel.getIntroduction());
            activity.setHelp(activityModel.getHelp());
            activity.setLegalStatement(activityModel.getLegalStatement());
            activity.setDescription(activityModel.getDescription());
            activity.setPosterUrl(activityModel.getPosterUrl());
            activity.setOperator(WebUpmsContext.getUserName(request));
            activity.setShowMess(activityModel.getShowMess());
            if (activityService.updateActivity(activity)) {
                /*修改活动的时候,更新openSearch的索引*/
//                List<OpenSearch> openSearches = activityService.getOpenSearchByOption(activity.getActivityId(), null, null);
//                for (OpenSearch openSearch : openSearches) {
//                    OpenSearchUtil.updateDoc(OpenSearchUtil.tableName, OpenSearchUtil.index, OpenSearchUtil.beanToMap(openSearch));
//                }
                return WebResult.getSuccessResult();
            } else {
                return WebResult.getErrorResult(WebResult.Code.ADD_FAILED);
            }
        }
    }


    @ApiOperation(value = "处理产品状态", notes = "测试产品数据统计时不统计", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "activityId", value = "产品ID", required = true, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "status", value = "状态", required = true, dataType = "integer", paramType = "query")
    })
    @RequestMapping(value = "/status.json", method = RequestMethod.POST)
    public WebResult dealStatus(@RequestParam("activityId") Integer activityId,
                                @RequestParam("status") Integer status) {

        ActivityWithBLOBs activity = activityService.getActivityById(activityId);
        activity.setStatus(status);
        activityService.updateActivity(activity);
        return WebResult.getSuccessResult();
    }

    @ApiOperation(value = "处理线下状态", notes = "处理线下状态", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "activityId", value = "产品ID", required = true, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "offline", value = "线下状态", required = true, dataType = "integer", paramType = "query")
    })
    @RequestMapping(value = "/offline.json", method = RequestMethod.POST)
    public Object updateOffline(@RequestParam("activityId") Integer activityId,
                                @RequestParam("offline") Integer offline,
                                HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer != null) {
            if (activityService.updateAcitivtyOfflie(activityId, offline) > 0) {
                return WebResult.getSuccessResult();
            } else {
                return WebResult.getErrorResult(WebResult.Code.UPDATE_FAILED);
            }
        } else {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
    }

    /**
     * 每个产品是否可以使用 储值金额 进行购买的修改
     *
     * @param storage 0：不可用  1：可用
     * @return
     */
    @ApiOperation(value = "处理储值状态", notes = "处理储值状态", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "activityId", value = "产品ID", required = true, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "storage", value = "储值状态", required = true, dataType = "integer", paramType = "query")
    })
    @RequestMapping(value = "/storage.json", method = RequestMethod.POST)
    public Object updateAllowUseStorage(@RequestParam(value = "activityId", required = true) Integer activityId,
                                        @RequestParam(value = "storage", defaultValue = "0") Integer storage) {

        ActivityWithBLOBs activity = new ActivityWithBLOBs();
        activity.setActivityId(activityId);
        activity.setAllowUseStorage(storage);

        if (activityService.updateActivity(activity)) {
            return WebResult.getSuccessResult();
        } else {
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }

    }

    @RequestMapping(value = "/visible.json", method = RequestMethod.POST)
    public Object updateRender(@RequestParam("activityId") Integer activityId,
                               @RequestParam("visible") Integer status, HttpServletRequest request
    ) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer != null) {
            if (activityService.updateRenderActivity(activityId, organizer.getOrganizerId(), status) > 0) {
                activityService.updateRenderActivity(activityId, 0, status);
                return WebResult.getSuccessResult();
            } else {
                return WebResult.getErrorResult(WebResult.Code.UPDATE_FAILED);
            }
        } else {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
    }

    @ApiOperation(value = "独立售卖", notes = "处理独立售卖", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "activityId", value = "产品ID", required = true, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "offline", value = "线下状态", required = true, dataType = "integer", paramType = "query")
    })
    @RequestMapping(value = "/rentAvailable.json", method = RequestMethod.POST)
    public Object updateRentAvailable(@RequestParam("activityId") Integer activityId,
                                      @RequestParam("rentAvailable") Integer rentAvailable,
                                      HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer != null) {
            ActivityWithBLOBs activity = new ActivityWithBLOBs();
            activity.setActivityId(activityId);
            activity.setRentAvailable(rentAvailable);
            if (activityService.updateActivity(activity)) {
                return WebResult.getSuccessResult();
            } else {
                return WebResult.getErrorResult(WebResult.Code.ERROR);
            }
        } else {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
    }

    @RequestMapping(value = "/refresh.json", method = RequestMethod.POST)
    public Object updateRender(HttpServletRequest request
    ) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer != null) {
            redisService.refreshRedisData("ProductInfo", organizer.getOrganizerId());
            return WebResult.getSuccessResult();
        } else {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
    }

    @RequestMapping(value = "/memberCardVisible.json")
    public Object memberCardVisible(@RequestParam("activityId") Integer activityId, @RequestParam("visible") Integer visible, HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer != null) {
            ActivityWithBLOBs activity = new ActivityWithBLOBs();
            activity.setActivityId(activityId);
            activity.setVisible(visible);
            if (activityService.updateActivity(activity)) {
                return WebResult.getSuccessResult();
            } else {
                return WebResult.getErrorResult(WebResult.Code.ERROR);
            }
        } else {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
    }

    @ApiOperation(value = "产品售卖情况", notes = "产品售卖情况", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "startTime", value = "开始时间", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "endTime", value = "结束时间", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "sellType", value = "售卖模式（1：通用 2：日历）", required = true, dataType = "integer", paramType = "query")
    })
    @GetMapping(value = "/activitySellDetail.json")
    public Object activitySellDetail(@RequestParam String startTime, @RequestParam String endTime, @RequestParam Integer sellType, HttpServletRequest request){
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if(organizer == null) return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        List<Map<String, Object>> data = activityService.activitySellDetail(startTime, endTime, sellType, organizer.getOrganizerId());
        return WebResult.getSuccessResult("data", data);
    }


}
