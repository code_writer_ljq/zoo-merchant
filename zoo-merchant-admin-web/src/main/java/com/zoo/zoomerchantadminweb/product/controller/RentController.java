package com.zoo.zoomerchantadminweb.product.controller;

import com.zoo.activity.dao.model.Activity;
import com.zoo.activity.dao.model.ActivityWithBLOBs;
import com.zoo.activity.dao.model.Organizer;
import com.zoo.activity.service.ActivityService;
import com.zoo.web.bean.WebResult;
import com.zoo.zoomerchantadminweb.context.WebUpmsContext;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;

@Api(value = "api/server/rent", tags = {"主办方租赁产品接口"}, description = "主办方租赁产品接口描述")
@RestController
@RequestMapping("api/server/rent")
public class RentController {

    @Resource(name = "activityService")
    ActivityService activityService;

    @ApiOperation(value = "主办方租赁产品", notes = "主办方租赁产品", httpMethod = "GET")
    @RequestMapping(value = "/activity.json", method = RequestMethod.GET)
    public WebResult rentManage(HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        Activity activity = activityService.getRentActivity(organizer.getOrganizerId());
        return WebResult.getSuccessResult("data", activity);
    }

    @ApiOperation(value = "主办方租赁产品添加", notes = "主办方租赁产品添加、修改", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "rentActivityId", value = "ID", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "rentActivityTitle", value = "标题", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "rentActivityPostUrl", value = "海报", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "rentAvailable", value = "是否独立销售", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "offline", value = "是否线下购买", required = true, dataType = "int", paramType = "query"),
    })
    @RequestMapping(value = "/activity/save.json", method = RequestMethod.POST)
    public Object addRentActivity(@RequestParam(value = "activityId", required = false) Integer rentActivityId,
                                  @RequestParam("title") String rentActivityTitle,
                                  @RequestParam("posterUrl") String rentActivityPostUrl,
                                  @RequestParam("rentAvailable") Integer rentAvailable,
                                  @RequestParam("offline") Integer offline,
                                  @RequestParam(value = "description", required = false) String description,
                                  @RequestParam(value = "introduction", required = false) String introduction,
                                  @RequestParam(value = "legalStatement", required = false) String legalStatement,
                                  @RequestParam(value = "help", required = false) String help,
                                  HttpServletRequest request) {
        ActivityWithBLOBs activity = new ActivityWithBLOBs();
        activity.setTitle(rentActivityTitle);
        activity.setPosterUrl(rentActivityPostUrl);
        activity.setOrganizerId(WebUpmsContext.getOrgnization(request).getOrganizerId());
        activity.setOperator(WebUpmsContext.getUserName(request));
        activity.setTypeId(6);
        activity.setVisible(1);
        activity.setStatus(1);
        activity.setRent(1);
        activity.setRentAvailable(rentAvailable);
        activity.setOffline(offline);
        activity.setHelp(help);
        activity.setIntroduction(introduction);
        activity.setLegalStatement(legalStatement);
        activity.setDescription(description);
        boolean success;
        if (null == rentActivityId || rentActivityId == 0) {
            activity.setActivityId(activityService.getNextActivityId());
            activity.setCreateTime(new Date());
            success = activityService.addActivity(activity, null);
        } else {
            activity.setActivityId(rentActivityId);
            activity.setUpdateTime(new Date());
            success = activityService.updateActivity(activity);
        }
        if (success) {
            return WebResult.getSuccessResult("data", activity.getActivityId());
        } else {
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }
    }

    @ApiOperation(value = "主办方租赁产品列表", notes = "后台强c端使用的产品列表,会显示这个机构下面的所有的活动", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "size", value = "条", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "status", value = "状态", required = false, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "activityId", value = "产品ID", required = false, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "distribution", value = "是否为分销产品", required = false, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "startTime", value = "开始时间", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "endTime", value = "结束类型", required = false, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "offline", value = "是否开启线下", required = false, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "rentAvailable", value = "是否独立售卖", required = false, dataType = "integer", paramType = "query")
    })
    @RequestMapping(value = "/list.json", method = RequestMethod.GET)
    public WebResult activityList(@RequestParam(value = "page", required = false, defaultValue = "1") String page,
                                  @RequestParam(value = "size", required = false, defaultValue = "10") String size,
                                  @RequestParam(value = "status", required = false) Integer status,
                                  @RequestParam(value = "activityId", required = false) Integer activityId,
                                  @RequestParam(value = "distribution", required = false) Integer distribution,
                                  @RequestParam(value = "activityType", required = false) Integer activityType,
                                  @RequestParam(value = "startTime", required = false) String startTime,
                                  @RequestParam(value = "endTime", required = false) String endTime,
                                  @RequestParam(value = "offline", required = false) Integer offline,
                                  @RequestParam(value = "rentAvailable", required = false) Integer rentAvailable,
                                  HttpServletRequest request) {


        Organizer organizer = WebUpmsContext.getOrgnization(request);

        if (null == organizer) {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        } else {
            WebResult result = WebResult.getSuccessResult();
            result.putResult("data", activityService.getRentalActivityList(activityId, null, organizer.getOrganizerId(), distribution, status, Integer.parseInt(size), Integer.parseInt(page), activityType, startTime, endTime, offline, rentAvailable));
            return result;
        }

    }
}
