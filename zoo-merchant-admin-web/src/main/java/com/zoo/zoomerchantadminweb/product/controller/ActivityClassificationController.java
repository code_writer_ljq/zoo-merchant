package com.zoo.zoomerchantadminweb.product.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.zoo.activity.dao.model.ActivityClassification;
import com.zoo.activity.dao.model.ActivityClassificationExample;
import com.zoo.activity.dao.model.ActivityClassificationRelation;
import com.zoo.activity.dao.model.Organizer;
import com.zoo.activity.dao.nutz.pojo.CommonAccount;
import com.zoo.activity.service.ActivityClassificationRelationService;
import com.zoo.activity.service.ActivityClassificationService;
import com.zoo.web.bean.WebResult;
import com.zoo.zoomerchantadminweb.context.WebUpmsContext;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Api(value = "api/server/activity/classification", tags = {"产品分类接口"}, description = "产品分类接口描述")
@RestController
@RequestMapping("api/server/activity/classification")
public class ActivityClassificationController {

    @Autowired
    private ActivityClassificationService activityClassificationService;

    @Autowired
    private ActivityClassificationRelationService activityClassificationRelationService;

    @ApiOperation(value = "创建或修改产品分类", notes = "创建或修改产品分类", httpMethod = "POST")
    @RequestMapping(value = "/insertOrUpdate.json", method = RequestMethod.POST)
    public WebResult insertOrUpdate(ActivityClassification activityClassification, HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        CommonAccount account = WebUpmsContext.getAccount(request);
        if(null == organizer) return WebResult.getErrorResult("主办方不存在");
        if(null == account) return WebResult.getErrorResult("请先登录");
        Date date = new Date();
        if (activityClassification.getActivityClassificationId() == null) {
            activityClassification.setCreateUserId(account.getCustomerId());
            activityClassification.setOperatorId(account.getCustomerId());
            activityClassification.setCreateTime(date);
            activityClassification.setUpdateTime(date);
            activityClassification.setStatus(1);
            activityClassification.setOrganizerId(organizer.getOrganizerId());
            int insert = activityClassificationService.add(activityClassification);
        } else {
            activityClassification.setOperatorId(account.getCustomerId());
            activityClassification.setUpdateTime(date);
            activityClassificationService.updateByPrimaryKeySelective(activityClassification);
        }
        return WebResult.getSuccessResult("data", activityClassification.getActivityClassificationId());
    }

    @ApiOperation(value = "产品分类列表", notes = "产品分类列表", httpMethod = "GET")
    @RequestMapping(value = "/list.json", method = RequestMethod.GET)
    public WebResult list(HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        ActivityClassificationExample activityClassificationExample = new ActivityClassificationExample();
        activityClassificationExample.createCriteria().andOrganizerIdEqualTo(organizer.getOrganizerId()).andStatusEqualTo(1);
        List<ActivityClassification> activityClassifications = activityClassificationService.selectByExample(activityClassificationExample);
        return WebResult.getSuccessResult("data", activityClassifications);
    }

    @ApiOperation(value = "产品分类添加产品接口", notes = "产品分类添加产品接口", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "activities", value = "产品ID集合JsonArray字符串", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "activityClassificationId", value = "产品分类ID", required = true, dataType = "int", paramType = "query")
    })
    @RequestMapping(value = "/select.json", method = RequestMethod.POST)
    public WebResult addActivity(@RequestParam("activities") String activities, @RequestParam(name = "activityClassificationId") Integer activityClassificationId, HttpServletRequest request) {
        JSONArray activityJson = JSON.parseArray(activities);
        if (null == activityJson) {
            activityJson = new JSONArray();
        }
        activityClassificationRelationService.deleteByActivityClassificationId(activityClassificationId);
        List<ActivityClassificationRelation> relations = new ArrayList<>();
        for (Object obj : activityJson) {
            ActivityClassificationRelation relation = new ActivityClassificationRelation();
            Integer activityId = Integer.parseInt(((Map) obj).get("id").toString());
            relation.setActivityClassificationId(activityClassificationId);
            relation.setActivityId(activityId);
            relations.add(relation);
        }
        activityClassificationRelationService.insertBatch(relations);
        return WebResult.getSuccessResult();
    }

}
