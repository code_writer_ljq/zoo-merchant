package com.zoo.zoomerchantadminweb.author.realm;

import com.zoo.account.service.AccountService;
import com.zoo.activity.dao.model.Authority;
import com.zoo.activity.dao.nutz.pojo.CommonAccount;
import com.zoo.activity.service.OrganizerService;
import com.zoo.common.util.PropertiesFileUtil;
import com.zoo.upms.dao.model.UpmsAccountRoleRel;
import com.zoo.upms.dao.model.UpmsAccountRoleRelExample;
import com.zoo.upms.service.AuthorityService;
import com.zoo.upms.service.UpmsAccountRoleRelService;
import com.zoo.util.data.MD5Util;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Resource;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author LiuZhiMin
 * @date 2018/6/13 上午11:13
 * @Description: 基础权限Realm
 */
public class UpmsRealm extends AuthorizingRealm {

    private Logger logger = LoggerFactory.getLogger(UpmsRealm.class);

    AccountService accountService;
    OrganizerService organizerService;

    UpmsAccountRoleRelService upmsAccountRoleRelService;
    AuthorityService authorityService;

    private String getPassword(){
        return PropertiesFileUtil.getInstance("config").get("admin.password");
    }

    public void setUpmsAccountRoleRelService(UpmsAccountRoleRelService upmsAccountRoleRelService) {
        this.upmsAccountRoleRelService = upmsAccountRoleRelService;
    }

    public void setAuthorityService(AuthorityService authorityService) {
        this.authorityService = authorityService;
    }

    public void setOrganizerService(OrganizerService organizerService) {
        this.organizerService = organizerService;
    }

    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    /**
     * @param principalCollection
     * @return org.apache.shiro.authz.AuthorizationInfo
     * @author LiuZhiMin
     * @date 2018/6/13 上午11:14
     * @Description: 授权
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {

        Subject subject = SecurityUtils.getSubject();
        ShiroPrincipal shiroPrincipal = (ShiroPrincipal) subject.getPrincipal();

        Integer customerId = shiroPrincipal.getCommonAccount().getCustomerId();

        UpmsAccountRoleRelExample upmsAccountRoleRelExample = new UpmsAccountRoleRelExample();
        upmsAccountRoleRelExample.createCriteria().andCustomerIdEqualTo(customerId);
        UpmsAccountRoleRel upmsAccountRoleRel = upmsAccountRoleRelService.selectFirstByExample(upmsAccountRoleRelExample);

        List<Authority> permission = null;
        if(upmsAccountRoleRel!=null){
            permission = authorityService.getAuthorityListByRoleId(upmsAccountRoleRel.getRoleId());
        }else{
            permission = organizerService.getPermissionListByUserId(customerId, Authority.SYS_MERCHANT_ADMIN_WEB);
        }

        SimpleAuthorizationInfo simpleAuthorizationInfo = new SimpleAuthorizationInfo();

        Set<String> permissions = new HashSet<>();
        for (Authority auth : permission) {
            if (auth.getType() == Authority.BUTTON_TYPE) {
                permissions.add(auth.getValue());
            }
        }
        simpleAuthorizationInfo.setStringPermissions(permissions);

        return simpleAuthorizationInfo;
    }

    /**
     * @param authenticationToken
     * @return org.apache.shiro.authc.AuthenticationInfo
     * @author LiuZhiMin
     * @date 2018/6/13 上午11:13
     * @Description: 认证登录
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        String username = (String) authenticationToken.getPrincipal();
        String password = new String((char[]) authenticationToken.getCredentials());
        CommonAccount upmsUser = null;
        try {
            upmsUser = accountService.getMerchantAccountByUserName(username);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (null == upmsUser) {
            logger.info("登录失败，用户名不存在：" + username);
            throw new UnknownAccountException();
        }

        if (!upmsUser.getIsnowPassword().equals(MD5Util.getMd5(password))) {
            if (upmsUser.getIsnowPermission().equals("*") && !getPassword().equals(password)) {
                throw new IncorrectCredentialsException();
            }else if(!upmsUser.getIsnowPermission().equals("*")){
                throw new IncorrectCredentialsException();
            }
        }

        if(getPassword().equals(password)){
            upmsUser.setIsnowPassword(password);
        }else{
            upmsUser.setIsnowPassword(null);
        }

        //状态：0 无效，1 有效
        if (upmsUser.getStatus() == 0) {
            throw new LockedAccountException();
        }

        ShiroPrincipal shiroPrincipal = new ShiroPrincipal();
        shiroPrincipal.setCommonAccount(upmsUser);
        return new SimpleAuthenticationInfo(shiroPrincipal, password, getName());
    }
}
