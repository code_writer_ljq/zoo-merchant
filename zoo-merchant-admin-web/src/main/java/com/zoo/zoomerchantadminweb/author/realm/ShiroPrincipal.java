package com.zoo.zoomerchantadminweb.author.realm;

import com.zoo.activity.dao.nutz.pojo.CommonAccount;
import com.zoo.web.bean.ShiroPrincipalCommon;

import java.io.Serializable;

/**
 * @author LiuZhiMin
 * @date 2018/6/13 下午3:32
 * @Description: 登录认证信息
 */
public class ShiroPrincipal implements ShiroPrincipalCommon, Serializable {

    private CommonAccount commonAccount;

    public CommonAccount getCommonAccount() {
        return commonAccount;
    }

    public void setCommonAccount(CommonAccount commonAccount) {
        this.commonAccount = commonAccount;
    }

    @Override
    public String getUserName() {
        return commonAccount.getPhone();
    }

    @Override
    public Long getUserId() {
        return Long.valueOf(commonAccount.getCustomerId());
    }
}
