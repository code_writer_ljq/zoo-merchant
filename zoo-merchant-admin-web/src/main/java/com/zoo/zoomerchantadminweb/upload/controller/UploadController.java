package com.zoo.zoomerchantadminweb.upload.controller;

import com.alibaba.fastjson.JSON;
import com.zoo.activity.constant.ResponseData;
import com.zoo.activity.core.Status;
import com.zoo.activity.dao.model.Organizer;
import com.zoo.activity.util.Common;
import com.zoo.web.bean.WebResult;
import com.zoo.wechat.common.Config;
import com.zoo.zoomerchantadminweb.context.WebUpmsContext;
import io.swagger.annotations.Api;
import net.coobird.thumbnailator.Thumbnails;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Api(value = "api/upload", tags = {"主办方上传文件接口"}, description = "这是为前台UE提供服务的controller")
@RestController
@RequestMapping("api/upload")
public class UploadController {

    private String url = "http://resource.huaxuezoo.com/upload";


    @RequestMapping(value = "/image.json", method = RequestMethod.POST)
    @ResponseBody
    public WebResult imageUpload(@RequestParam("file") MultipartFile[] tmpFiles, HttpServletRequest req, HttpSession session) throws Exception {

        Organizer organizer = WebUpmsContext.getOrgnization(req);
        ResponseData resp = new ResponseData();
        if (organizer == null) {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
        /*只做单次上传处理*/
        if (tmpFiles.length > 0) {

            String contentType = tmpFiles[0].getContentType();
            String result = httpSubmitImage(url, tmpFiles[0].getInputStream(), contentType);

            if (StringUtils.isNotBlank(result)) {

                List<Map> resultInfo = JSON.parseArray(result, Map.class);
                resp.setStatus(Status.success);

                Map data = new HashMap();
                data.put("url", resultInfo.get(0).get("url").toString());
                data.put("contentType", contentType);
                return WebResult.getSuccessResult("data", data);

            }

        } else {
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }
        return WebResult.getErrorResult(WebResult.Code.ERROR);
    }


    @RequestMapping(value = "/schedule/image.json", method = RequestMethod.POST)
    public WebResult scheduleUpload(@RequestParam("file") MultipartFile[] tmpFiles,
                                 @RequestParam(value = "x", required = false) String x,
                                 @RequestParam(value = "y", required = false) String y,
                                 @RequestParam(value = "w", required = false) String width,
                                 @RequestParam(value = "h", required = false) String height,
                                 HttpServletRequest req, HttpSession session) throws Exception {

        Organizer organizer = WebUpmsContext.getOrgnization(req);
        ResponseData resp = new ResponseData();
        if (organizer == null) {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
        /*只做单次上传处理*/
        if (tmpFiles.length > 0) {
            String result;

            String contentType = tmpFiles[0].getContentType();

            String cropPath = Config.instance().getTmpPath() + File.separator + Common.uniqueKey() + "." + contentType.split("/")[1];

            if (StringUtils.isNotBlank(x) && StringUtils.isNotBlank(y) && StringUtils.isNotBlank(width) && StringUtils.isNotBlank(height)) {
                Integer xInt = handlerCutData(x);
                Integer yInt = handlerCutData(y);
                Integer widthInt = handlerCutData(width);
                Integer heightInt = handlerCutData(height);

                Thumbnails.of(tmpFiles[0].getInputStream()).sourceRegion(xInt, yInt, widthInt, heightInt).size(widthInt > 400 ? widthInt : 400, widthInt > 400 ? heightInt : 450).keepAspectRatio(false).toFile(cropPath);


                result = httpSubmitImage(url, new FileInputStream(cropPath), contentType);
            } else {
                result = httpSubmitImage(url, tmpFiles[0].getInputStream(), contentType);
            }


            if (StringUtils.isNotBlank(result)) {

                List<Map> resultInfo = JSON.parseArray(result, Map.class);
                resp.setStatus(Status.success);

                Map data = new HashMap();
                data.put("url", resultInfo.get(0).get("url").toString());
                data.put("contentType", contentType);


                File cropFile = new File(cropPath);
                if (cropFile.exists())
                    cropFile.delete();
                return WebResult.getSuccessResult("data", data);
            }

        } else {
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }
        return WebResult.getErrorResult(WebResult.Code.ERROR);
    }


    /**
     * 模拟表单上传文件的方式,用于将图片上传到图片服务器
     *
     * @throws Exception
     */
    public static String httpSubmitImage(String url, InputStream in, String contentType) throws Exception {
        CloseableHttpClient httpClient = null;
        CloseableHttpResponse response = null;
        /*发送请求,返回的结果数据*/
        String resultString = null;
        try {
            httpClient = HttpClients.createDefault();

            // 把一个普通参数和文件上传给下面这个地址 是一个servlet
            HttpPost httpPost = new HttpPost(url);

            // 把文件转换成流对象FileBody
            HttpEntity reqEntity = MultipartEntityBuilder.create()
                    // 相当于<input type="file" name="file"/>
                    .addBinaryBody("file", in, ContentType.create(contentType), Common.uniqueKey())
                    .build();
            httpPost.setEntity(reqEntity);

            // 发起请求 并返回请求的响应
            response = httpClient.execute(httpPost);

            // 获取响应对象
            HttpEntity resEntity = response.getEntity();
            if (resEntity != null) {
                resultString = EntityUtils.toString(resEntity, Charset.forName("UTF-8"));
            }
            // 销毁
            EntityUtils.consume(resEntity);
        } finally {
            try {
                if (response != null) {
                    response.close();
                }
            } catch (IOException e) {

            }

            try {
                if (httpClient != null) {
                    httpClient.close();
                }
            } catch (IOException e) {

            }
        }
        return resultString;
    }

    /**
     * 根据cropper的坐标可能不是Int类型</br>
     * Thumbnails 切割图片需要使用的int 需要在这个地方进行转换操作
     */
    private Integer handlerCutData(String s) {
        if (!s.contains(".")) {
            return Integer.parseInt(s);
        } else {
            return Integer.parseInt(s.substring(0, s.lastIndexOf(".")));
        }
    }
}
