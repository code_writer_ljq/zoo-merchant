package com.zoo.zoomerchantadminweb.comments.controller;

import com.zoo.common.page.PageList;
import com.zoo.activity.dao.model.Activity;
import com.zoo.activity.dao.model.Comment;
import com.zoo.activity.dao.model.Organizer;
import com.zoo.activity.service.ActivityService;
import com.zoo.activity.service.CommentService;
import com.zoo.activity.vo.CommentModel;
import com.zoo.util.data.BeanUtilsExtends;
import com.zoo.web.bean.WebResult;
import com.zoo.zoomerchantadminweb.context.WebUpmsContext;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.Map;

@Api(value = "/api/server/activity/comment", tags = "产品后台评价管理相关接口", description = "产品后台评价管理相关接口")
@RestController
@RequestMapping("/api/server/activity/comment")
public class ActivityCommentsController {

    @Autowired
    private CommentService commentService;
    @Autowired
    private ActivityService activityService;

    @ApiOperation(value = "产品评价列表", notes = "产品评价列表", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码，默认值1", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "size", value = "页量(一页显示的数量)，默认值10", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "activityId", value = "活动ID(activityId)", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "username", value = "用户名称", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "code", value = "订单号", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "startTime", value = "评论开始时间", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "endTime", value = "评论结束时间", required = false, dataType = "string", paramType = "query")
    })
    @RequestMapping(value = "/list.json")
    public WebResult getCommentListByActivityId(@RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
                                                @RequestParam(value = "size", required = false, defaultValue = "10") Integer size,
                                                @RequestParam(value = "username", required = false) String username,
                                                @RequestParam(value = "code", required = false) String code,
                                                @RequestParam(value = "startTime", required = false) String startTime,
                                                @RequestParam(value = "endTime", required = false) String endTime,
                                                @RequestParam(value = "activityId", required = false) Integer activityId, HttpServletRequest request) {
        PageList resultPageList;

        if (null == activityId) {
            Organizer organizer = WebUpmsContext.getOrgnization(request);
            if (null == organizer) {
                return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
            }
            resultPageList = commentService.getCommentList(null, organizer.getOrganizerId(), username, code, startTime, endTime, page, size);
        } else {
            resultPageList = commentService.getCommentList(activityId, null, username, code, startTime, endTime, page, size);
        }

        return WebResult.getSuccessResult("data", resultPageList);
    }

    @ApiOperation(value = "删除评价", notes = "删除评价", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "commentId", value = "评论ID", required = true, dataType = "int", paramType = "query")
    })
    @RequestMapping(value = "/delete.json", method = RequestMethod.POST)
    public WebResult delete(@RequestParam("commentId") Integer commentId, HttpServletRequest request) {

        Comment comment = commentService.getCommentById(commentId);
        int result = commentService.deleteComment(commentId, WebUpmsContext.getUserName(request));
        if (result == 1) {

            if (null != comment.getActivityId() && 0 != comment.getActivityId()) {
                Activity activity = activityService.getActivityById(comment.getActivityId());

                Map<String, Integer> commentMap = commentService.getActivityBasicComment(activity.getActivityId());
                if (commentMap.get("commentCount") == 0) {
                    activity.setAvgStar(5);
                } else {
                    activity.setAvgStar((int) Math.ceil((double) commentMap.get("sumStar") / commentMap.get("commentCount")));
                }
                activityService.updateActivityBySelective(activity);
            }
            return WebResult.getSuccessResult();

        } else {
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }
    }

    @ApiOperation(value = "回复", notes = "回复", httpMethod = "POST")
    @RequestMapping(value = "/add.json", method = RequestMethod.POST)
    public WebResult doCommentAdd(@ModelAttribute CommentModel commentModel, HttpServletRequest request)
            throws Exception {
        Comment comment = new Comment();
        BeanUtilsExtends.copyProperties(comment, commentModel);
        comment.setCreateTime(new Date());
        comment.setType(2);
        comment.setValid(1);
        comment.setTargetId(comment.getCommentId());
        comment.setCommentId(null);
        comment.setOperator(WebUpmsContext.getOrgnization(request).getName());

        int result = commentService.createComment(comment);

        if (result == 1) {
            return WebResult.getSuccessResult();
        } else {
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }
    }
}
