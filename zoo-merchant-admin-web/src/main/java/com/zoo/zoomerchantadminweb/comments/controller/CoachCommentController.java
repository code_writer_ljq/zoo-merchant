package com.zoo.zoomerchantadminweb.comments.controller;

import com.zoo.common.page.PageList;
import com.zoo.activity.dao.model.CoachComment;
import com.zoo.activity.dao.model.CommentPhoto;
import com.zoo.activity.dao.model.Organizer;
import com.zoo.activity.dao.nutz.pojo.CommonAccount;
import com.zoo.activity.service.CoachCommentService;
import com.zoo.web.bean.WebResult;
import com.zoo.zoomerchantadminweb.base.BaseController;
import com.zoo.zoomerchantadminweb.context.WebUpmsContext;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Arrays;
import java.util.List;

/**
 * Created by ljq on 2018/8/24.
 */
@Api(value = "/api/server/coach/course/comment", tags = "【教练系统】教练后台评价管理相关接口", description = "教练评价的增删改查")
@RestController
@RequestMapping("/api/server/coach/course/comment")
public class CoachCommentController extends BaseController {
    @Autowired
    private CoachCommentService coachCommentService;

    @ApiOperation(value = "查询评论列表", notes = "查询评论列表", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码，默认值1", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "size", value = "页量(一页显示的数量)，默认值10", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "episodeId", value = "活动ID(episodeId)", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "coachId", value = "教练ID", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "hasPhoto", value = "有图片", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "hasReply", value = "有回复", required = false, dataType = "int", paramType = "query"),
    })
    @RequestMapping(value = "/list.json", method = RequestMethod.GET)
    public Object courseCommentList(@RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
                                    @RequestParam(value = "size", required = false, defaultValue = "10") Integer size,
                                    @RequestParam(value = "episodeId", required = false) Integer episodeId,
                                    @RequestParam(value = "coachId", required = false) Integer coachId,
                                    @RequestParam(value = "hasPhoto", required = false) Integer hasPhoto,
                                    @RequestParam(value = "hasReply", required = false) Integer hasReply,
                                    HttpServletRequest request, HttpSession session) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null && organizer.getOrganizerId() != null) {
            return WebResult.getErrorResult(WebResult.Code.COACH_GET_ORGANIZER_INFO_ERROR);
        }
        PageList coachCommentResult = coachCommentService.getCommentListByOrganizerId(episodeId, organizer.getOrganizerId(), coachId, hasPhoto, hasReply, page, size);
        return WebResult.getSuccessResult("data", coachCommentResult);
    }


    /**
     * 教练评论置为 有效或无效
     * @param commentId
     * @param valid
     * @param request
     * @param session
     * @return
     */
    @ApiOperation(value = "将教练评论置为有效或失效接口", notes = "教练评论置为 有效或无效", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "commentId", value = "评论ID", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "valid", value = "是否有效", required = true, dataType = "int", paramType = "query"),
    })
    @RequestMapping(value = "/valid.json", method = RequestMethod.POST)
    public Object validComment(@RequestParam("commentId") Integer commentId,
                               @RequestParam("valid") Integer valid,
                               HttpServletRequest request, HttpSession session) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null && organizer.getOrganizerId() != null) {
            return WebResult.getErrorResult(WebResult.Code.COACH_GET_ORGANIZER_INFO_ERROR);
        }

        CoachComment coachComment = new CoachComment();
        coachComment.setCommentId(commentId);
        coachComment.setValid(valid);
        if (coachCommentService.updateCommentValid(coachComment) > 0) {
            return WebResult.getSuccessResult();
        }
        return WebResult.getErrorResult(WebResult.Code.ERROR);
    }

    /**
     * 将教练评论的图片置为 有效或无效
     * @param photoId
     * @param valid
     * @param session
     * @return
     */
    @ApiOperation(value = "教练评论图片置为有效或失效接口", notes = "将教练评论的图片置为 有效或无效", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "photoId", value = "图片ID", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "valid", value = "是否有效", required = true, dataType = "int", paramType = "query"),
    })
    @RequestMapping(value = "/photo/valid.json", method = RequestMethod.POST)
    @ResponseBody
    public Object validCommentPhoto(@RequestParam("photoId") Integer photoId,
                                    @RequestParam("valid") Integer valid,
                                    HttpServletRequest request, HttpSession session) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null && organizer.getOrganizerId() != null) {
            return WebResult.getErrorResult(WebResult.Code.COACH_GET_ORGANIZER_INFO_ERROR);
        }
        CommentPhoto commentPhoto = new CommentPhoto();
        commentPhoto.setPhotoId(photoId);
        commentPhoto.setValid(valid);
        if (coachCommentService.updatePhotoValid(commentPhoto) > 0) {
            return WebResult.getSuccessResult();
        }
        return WebResult.getErrorResult(WebResult.Code.ERROR);
    }

    /**
     * 后台回复教练和用户的评论接口
     * @param replyId
     * @param content
     * @param request
     * @param session
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "后台回复教练和用户的评论接口", notes = "后台回复教练和用户的评论接口", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "replyId", value = "回复ID", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "content", value = "回复内容", required = true, dataType = "string", paramType = "query"),
    })
    @RequestMapping(value = "/reply/save.json", method = RequestMethod.POST)
    public Object addCoachCommentReply(@RequestParam(value = "replyId") Integer replyId,
                                       @RequestParam(value = "content") String content,
                                       HttpServletRequest request, HttpSession session) throws Exception {
        CoachComment coachComment = new CoachComment();
        CommonAccount account = WebUpmsContext.getAccount(request);
        coachComment.setValid(1);
        coachComment.setNickname(WebUpmsContext.getOrgnization(request).getName());
        coachComment.setAgent(request.getHeader("user-agent"));
        coachComment.setCommentUser(account.getCustomerId());
        coachComment.setContent(content);
        coachComment.setParentId(replyId);
        coachComment.setCommentType(1);
        coachComment.setStatus(1);
        if (coachCommentService.saveCoachComment(coachComment, null)) {
            // res.setData(coachComment.getNickname());
            return WebResult.getSuccessResult("data", coachComment.getNickname());
        }
        return WebResult.getErrorResult(WebResult.Code.ERROR);
    }

    @ApiOperation(value = "教练评价审核接口", notes = "教练评价审核接口", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "commentId", value = "评论ID", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "status", value = "审批状态(1：审批通过 2：审批未通过)", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "type", value = "审批类型(1：单个审批 2：全部审批通过)", required = true, dataType = "int", paramType = "query"),
    })
    @PostMapping("/approve.json")
    public Object approve(@RequestParam(value = "commentId", required = false) Integer commentId, @RequestParam(value = "status", required = false) Integer status, @RequestParam("type") Integer type, HttpServletRequest request){
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null || organizer.getOrganizerId() == null) {
            return WebResult.getErrorResult(WebResult.Code.COACH_GET_ORGANIZER_INFO_ERROR);
        }
        if(type == 1) {
            coachCommentService.approveCoachComment(commentId, status);
        } else if (type == 2) {
            coachCommentService.approveAll(organizer.getOrganizerId());
        }
//        if (StringUtils.isEmpty(commentId)){
//            return WebResult.getSuccessResult();
//        }
//        String[] split = commentIds.split(",");
//        List<String> commentIdList = Arrays.asList(split);
//        coachCommentService.approve(commentIdList, status);
        return WebResult.getSuccessResult();
    }
}
