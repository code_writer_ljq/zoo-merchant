package com.zoo.zoomerchantadminweb.comments.controller;

import com.zoo.account.constant.AccountEnum;
import com.zoo.account.service.AccountService;
import com.zoo.activity.dao.model.ArticleComment;
import com.zoo.activity.dao.model.Organizer;
import com.zoo.activity.dao.nutz.pojo.CommonAccount;
import com.zoo.activity.service.ArticleCommentService;
import com.zoo.activity.service.ArticleService;
import com.zoo.activity.vo.ArticleCommentModel;
import com.zoo.util.data.BeanUtilsExtends;
import com.zoo.web.bean.WebResult;
import com.zoo.zoomerchantadminweb.context.WebUpmsContext;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

import java.util.Date;

@Api(value = "/api/server/article/comment", tags = "资讯后台评价管理相关接口", description = "资讯后台评价管理相关接口")
@RestController
@RequestMapping("/api/server/article/comment")
public class ArticleCommentsController {

    @Autowired
    private ArticleService articleService;

    @Autowired
    private ArticleCommentService articleCommentService;

    @Autowired
    private AccountService accountService;

    @ApiOperation(value = "文章评论列表", notes = "文章评论列表", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码，默认值1", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "size", value = "页量(一页显示的数量)，默认值10", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "title", value = "标题", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "nick", value = "用户名", required = false, dataType = "string", paramType = "query"),
    })
    @RequestMapping(value = "list.json")
    public WebResult getArticleCommentList(@RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
                                           @RequestParam(value = "size", required = false, defaultValue = "10") Integer size,
                                           @RequestParam(value = "title", required = false) String title,
                                           @RequestParam(value = "nick", required = false) String nick,
                                           HttpServletRequest request) throws Exception {

        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (null != organizer) {
            return WebResult.getSuccessResult("data", articleService.getArticleCommentListByOrgId(organizer.getOrganizerId(), title, nick, page, size));
        } else {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }

    }

    @ApiOperation(value = "文章评论回复", notes = "文章评论回复", httpMethod = "POST")
    @RequestMapping(value = "add.json", method = RequestMethod.POST)
    public Object addCommentReply(@ModelAttribute ArticleCommentModel articleCommentModel, HttpServletRequest request) throws Exception {
        ArticleComment comment = new ArticleComment();
        BeanUtilsExtends.copyProperties(comment, articleCommentModel);
        Integer customerId = WebUpmsContext.getAccount(request).getCustomerId();
        if (customerId != null) {
            comment.setCustomerId(customerId);
            CommonAccount account = (CommonAccount) accountService.getAccount(AccountEnum.FetchAccountTypeEnum.FETCH_BY_CUSTOMER.getValue(), String.valueOf(customerId)).getData();
            if (account != null) {
                String operator = account.getNickname();
                if (operator == null || "".equals(operator)) {
                    operator = account.getRealname();
                }
                if (operator == null || "".equals(operator)) {
                    String phone = account.getPhone();
                    if (phone != null && !"".equals(phone)) {
                        StringBuffer sbPhone = new StringBuffer(phone);
                        sbPhone = sbPhone.replace(3, 7, "****");
                        operator = sbPhone.toString();
                    } else {
                        operator = customerId.toString();
                    }
                }
                comment.setNickname(operator);
                comment.setAvatar(account.getAvatarUrl());
            }

        }
        Date currentTime = new Date();
        comment.setCreateTime(currentTime);
        comment.setUpdateTime(currentTime);
        //设置回复评论的ID
        if (articleCommentService.addComment(comment) != null) {
            return WebResult.getSuccessResult();
        } else {
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }
    }

    @ApiOperation(value = "文章评论回复删除", notes = "文章评论回复删除", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "commentId", value = "评论ID", required = true, dataType = "int", paramType = "query")
    })
    @RequestMapping(value = "delete.json", method = RequestMethod.POST)
    public WebResult deleteCommentReply(@RequestParam(value = "commentId", required = true) String commentId) {
        if (articleCommentService.deleteCommentReply(commentId)) {
            return WebResult.getSuccessResult();
        } else {
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }
    }

    @ApiOperation(value = "文章评论删除", notes = "文章评论删除", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "commentId", value = "评论ID", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "articleId", value = "文章ID", required = true, dataType = "int", paramType = "query")
    })
    @RequestMapping(value = "remove.json", method = RequestMethod.POST)
    public WebResult removeArticleComment(@RequestParam(value = "commentId", required = true) int commentId,
                                       @RequestParam(value = "articleId", required = true) int articleId) throws Exception {

        try {
            if (articleService.removeComment(commentId, articleId)) {
                return WebResult.getSuccessResult();
            } else {
                return WebResult.getErrorResult(WebResult.Code.ERROR);
            }
        } catch (Exception e) {
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }
    }
}
