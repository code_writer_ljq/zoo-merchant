package com.zoo.zoomerchantadminweb.distribution.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zoo.activity.dao.model.*;
import com.zoo.activity.dao.nutz.pojo.CommonAccount;
import com.zoo.activity.service.*;
import com.zoo.activity.util.DateUtil;
import com.zoo.common.page.PageList;
import com.zoo.common.util.IdcardValidator;
import com.zoo.common.util.MD5Util;
import com.zoo.web.bean.WebResult;
import com.zoo.zoomerchantadminweb.context.WebUpmsContext;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.util.*;
import java.util.regex.Pattern;

/**
 * Created by ljq on 2019/4/11.
 */
@Api(value = "api/server/distribution/account", tags = {"【分销后台】分销员后台管理接口"}, description = "后台管理分销员和分销产品")
@RestController
@RequestMapping("api/server/distribution/account")
public class DistributionAccountController {
    private Pattern phonePattern = Pattern.compile("^(0|86|17951)?(13[0-9]|15[0-9]|16[0-9]|17[0-9]|18[0-9]|19[0-9]|14[0-9])[0-9]{8}$");

    @Autowired
    private DistributionAccountService distributionAccountService;

    @Autowired
    private DistributionAccountActivityRelationService distributionAccountActivityRelationService;

    @Autowired
    private DistributionAccountOrganizerRelationService distributionAccountOrganizerRelationService;

    @Autowired
    private DistributionSettlementRecordService distributionSettlementRecordService;

    @Autowired
    private VerifyService verifyService;

    @ApiOperation(value = "分销员列表", notes = "分销员列表", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "name", value = "分销员姓名", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "phone", value = "分销员手机号", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "role", value = "分销员角色{0:员工，1：分销商}", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "orgRelationRemove", value = "分销员状态{1:无效，0：有效}", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "page", value = "页码", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "size", value = "每一页数量", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "sortProp", value = "按照某一列值进行排序", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "sortType", value = "asc 升序，desc 降序", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "startTime", value = "下单开始时间", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "endTime", value = "下单结束时间", required = false, dataType = "string", paramType = "query"),
    })
    @RequestMapping(value = "/list.json", method = RequestMethod.GET)
    public WebResult distributionAccountList(@RequestParam(name = "name", required = false) String name,
                                  @RequestParam(name = "phone", required = false) String phone,
                                  @RequestParam(name = "role", required = false) Integer role,
                                  @RequestParam(name = "orgRelationRemove", required = false) Integer orgRelationRemove,
                                  @RequestParam(name = "page", required = false, defaultValue = "1") Integer page,
                                  @RequestParam(name = "size", required = false, defaultValue = "10") Integer size,
                                  @RequestParam(name = "sortProp", required = false, defaultValue = "sales_total_price") String sortProp,
                                  @RequestParam(name = "sortType", required = false, defaultValue = "desc") String sortType,
                                 @RequestParam(value = "startTime", required = false) String startTime,
                                 @RequestParam(value = "endTime", required = false) String endTime,
                                  HttpServletRequest request, HttpSession session) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null) {
            return WebResult.getErrorResult(WebResult.Code.COACH_GET_ORGANIZER_INFO_ERROR);
        }

        if (StringUtils.isBlank(name))
            name = null;

        if (StringUtils.isBlank(phone))
            phone = null;

        // 统计查询分销员
        return WebResult.getSuccessResult("data", distributionAccountService.findDistributionAccountListAndSales(organizer.getOrganizerId(), name, phone, role, orgRelationRemove, sortProp, sortType, startTime, endTime, page, size));
    }

    @ApiOperation(value = "保存分销员信息", notes = "保存分销员信息", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "editMode", value = "编辑模式（0:添加分销员，1：编辑分销员或分销商，2：分配分销商）", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "distributionAccountId", value = "分销账户ID", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "name", value = "分销员姓名", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "phone", value = "分销员手机号", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "role", value = "分销员角色{0:员工，1：分销商}", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "idCard", value = "身份证号", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "organizationName", value = "组织名称", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "organizationAddress", value = "组织所在地", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "organizationQualifications", value = "组织资质（格式：{}）", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "note", value = "备注", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "activityInfos", value = "关联产品信息（[{\"activityId\":12,\"title\":\"哈哈\"},{\"activityId\":13,\"title\":\"666\"}]）", required = false, dataType = "string", paramType = "query")
    })
    @RequestMapping(value = "/save.json", method = RequestMethod.POST)
    public WebResult saveDistributionAccountInfo(@RequestParam(name = "editMode", required = true) Integer editMode,
                                              @RequestParam(name = "distributionAccountId", required = false) Integer distributionAccountId,
                                              @RequestParam(name = "name", required = true) String name,
                                              @RequestParam(name = "phone", required = true) String phone,
                                              @RequestParam(name = "role", required = true) Integer role,
                                              @RequestParam(name = "idCard", required = true) String idCard,
                                              @RequestParam(name = "organizationName", required = false) String organizationName,
                                              @RequestParam(name = "organizationAddress", required = false) String organizationAddress,
                                              @RequestParam(name = "organizationQualifications", required = false) String organizationQualifications,
                                              @RequestParam(name = "note", required = false) String note,
                                              @RequestParam(name = "activityInfos", required = false, defaultValue = "[]") String activityInfos,
                                              HttpServletRequest request, HttpSession session) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        CommonAccount account = WebUpmsContext.getAccount(request);
        if (organizer == null) {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }

        if (!phonePattern.matcher(phone).matches()) {
            return WebResult.getErrorResult(WebResult.Code.PHONE_INVALID);
        }

        if (!IdcardValidator.build().isValidatedAllIdcard(idCard)) {
            return WebResult.getErrorResult(WebResult.Code.DISTRIBUTION_IDCARD_INVALID);
        }

        DistributionAccount distributionAccount = new DistributionAccount();
        DistributionAccountOrganizerRelation accountOrganizerRelation = null;
        Date now = new Date();
        //手机号账号是否在本雪场注册、申请过
        DistributionAccount queryDistributionAccount = distributionAccountService.findDistributionAccountByPhone(organizer.getOrganizerId(), phone, role);
        DistributionAccount entityAccount = null;
        if (editMode != 0) {
            entityAccount = distributionAccountService.selectByPrimaryKey(distributionAccountId);
            if (!phone.equals(entityAccount.getPhone()) && queryDistributionAccount != null) {
                return WebResult.getErrorResult(WebResult.Code.PHONE_HAS_REGISTER);
            }
        }
        if (editMode == 2) {
            distributionAccount.setAccountId(distributionAccountId);
        } else {
            if (editMode == 0) {
                if (queryDistributionAccount != null) {
                    return WebResult.getErrorResult(WebResult.Code.PHONE_HAS_REGISTER);
                }
                if (role == 0) {
                    // 手机号账户是否已有启用
                    List<DistributionAccountOrganizerRelation> organizerRelations = distributionAccountOrganizerRelationService.getAccountOrganizerRelationListByPhone(null, phone, 0, 0);
                    if (organizerRelations.size() >= 1) {
                        return WebResult.getErrorResult(WebResult.Code.DISTRIBUTION_REGISTER_PHONE_VALID_EXCEPTION);
                    }
                }
                accountOrganizerRelation = new DistributionAccountOrganizerRelation();
                accountOrganizerRelation.setOrganizerId(organizer.getOrganizerId());
                accountOrganizerRelation.setValid(1);
                accountOrganizerRelation.setCreateTime(now);
                accountOrganizerRelation.setUpdateTime(now);
                accountOrganizerRelation.setRemove(0);
                accountOrganizerRelation.setOrganizerName(organizer.getName());

                distributionAccount.setCreateTime(now);
                distributionAccount.setHeadUrl("http://static.huaxuezoo.com/image/ef2e5c1e1db0439b92747dc9117d2a58");
                distributionAccount.setOrganizerId(0);
                distributionAccount.setPassword(MD5Util.getMd5(phone)); // 默认密码为手机号
                distributionAccount.setValid(1);
            } else {
                distributionAccount.setAccountId(distributionAccountId);
                role = entityAccount.getRole();
            }
        }

        distributionAccount.setPhone(phone);
        distributionAccount.setEmail(null);
        distributionAccount.setName(name);
        distributionAccount.setIdCard(idCard);
        distributionAccount.setNote(note);
        distributionAccount.setRole(role);
        distributionAccount.setUpdateTime(now);
        if (role == 1) {
            distributionAccount.setOrganizationName(organizationName);
            distributionAccount.setOrganizationAddress(organizationAddress);
            distributionAccount.setOrganizationQualifications(organizationQualifications);
        }

        if (!distributionAccountService.saveOrUpdateDistributionAccount(editMode, distributionAccount, accountOrganizerRelation, activityInfos, organizer, account)) {
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }
        return WebResult.getSuccessResult();
    }


    @ApiOperation(value = "分配分销商为雪场分销员", notes = "分配分销商为雪场分销员", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "distributionAccountId", value = "分销账户ID", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "activityInfos", value = "关联产品信息（[{\"activityId\":12,\"title\":\"哈哈\"},{\"activityId\":13,\"title\":\"666\"}]）", required = false, dataType = "string", paramType = "query")
    })
    @RequestMapping(value = "/division.json", method = RequestMethod.POST)
    public WebResult divisionDistributionAccountInfo(@RequestParam(name = "distributionAccountId", required = true) Integer distributionAccountId,
                                              @RequestParam(name = "activityInfos", required = false, defaultValue = "[]") String activityInfos,
                                              HttpServletRequest request, HttpSession session) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        CommonAccount account = WebUpmsContext.getAccount(request);
        if (organizer == null) {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }

        if (!distributionAccountService.divisionOrganizerDistributionAccount(distributionAccountId, activityInfos, organizer, account)) {
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }
        return WebResult.getSuccessResult();
    }

    @ApiOperation(value = "编辑分销员信息", notes = "编辑分销员信息", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "distributionAccountId", value = "分销员ID", required = true, dataType = "int", paramType = "query")
    })
    @RequestMapping(value = "/detail.json", method = RequestMethod.GET)
    public WebResult distributionAccountDetail(@RequestParam(name = "distributionAccountId", required = true) Integer distributionAccountId,
                                            HttpServletRequest request, HttpSession session) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null) {
            return WebResult.getErrorResult(WebResult.Code.COACH_GET_ORGANIZER_INFO_ERROR);
        }
        Map<String, Object> map = new HashMap();
        map.put("distributionAccount", distributionAccountService.selectByPrimaryKey(distributionAccountId));
        DistributionAccountActivityRelationExample activityRelationExample = new DistributionAccountActivityRelationExample();
        activityRelationExample.createCriteria().andOrganizerIdEqualTo(organizer.getOrganizerId())
                .andDistributionAccountIdEqualTo(distributionAccountId)
                .andValidEqualTo(1);
        map.put("accountActivityRelation", distributionAccountActivityRelationService.findDistributionAccountActivityRelationByOption(activityRelationExample));
        return WebResult.getSuccessResult("data", map);
    }

    @ApiOperation(value = "查看分销员详情", notes = "查看分销员详情", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "distributionAccountId", value = "分销员ID", required = true, dataType = "int", paramType = "query")
    })
    @RequestMapping(value = "/info.json", method = RequestMethod.GET)
    public WebResult distributionAccountInfo(@RequestParam(name = "distributionAccountId", required = true) Integer distributionAccountId,
                                          HttpServletRequest request, HttpSession session) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null) {
            return WebResult.getErrorResult(WebResult.Code.COACH_GET_ORGANIZER_INFO_ERROR);
        }

        return WebResult.getSuccessResult("data", distributionAccountService.findDistributionAccountAndSalesInfo(distributionAccountId.longValue(), organizer.getOrganizerId()));
    }

    @ApiOperation(value = "禁用启用分销员", notes = "禁用启用分销员", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "distributionAccountId", value = "分销员ID", required = true, dataType = "int", paramType = "query")
    })
    @RequestMapping(value = "/status.json", method = RequestMethod.POST)
    public WebResult distributionAccount(@RequestParam(name = "distributionAccountId", required = true) Integer distributionAccountId,
                                          HttpServletRequest request, HttpSession session) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null) {
            return WebResult.getErrorResult(WebResult.Code.COACH_GET_ORGANIZER_INFO_ERROR);
        }

        // 查询分销账户信息
        DistributionAccount distributionAccount = distributionAccountService.selectByPrimaryKey(distributionAccountId);
        if (distributionAccount == null) {
            return WebResult.getErrorResult(WebResult.WechatCode.WECHAT_DISTRIBUTION_NOT_ACCOUNT);
        }

        // 查询分销账户与主办方的关系
        DistributionAccountOrganizerRelationExample orgRelationExample = new DistributionAccountOrganizerRelationExample();
        orgRelationExample.createCriteria().andDistributionAccountIdEqualTo(distributionAccountId).andOrganizerIdEqualTo(organizer.getOrganizerId());
        DistributionAccountOrganizerRelation organizerRelation = distributionAccountOrganizerRelationService.selectFirstByExample(orgRelationExample);
        if (organizerRelation == null) {
            return WebResult.getErrorResult(WebResult.WechatCode.WECHAT_DISTRIBUTION_ACCOUNT_EXCEPTION);
        }

        // 判断分销账户状态
        Date now = new Date();
        DistributionAccountOrganizerRelation orgRelation = new DistributionAccountOrganizerRelation();
        if (organizerRelation.getRemove() == 0) {
            orgRelation.setRemove(1);
            //distributionAccount.setValid(1);
        } else {
            //distributionAccount.setValid(0);
            // 判断分销员是否已经存在已经启用
            List<DistributionAccountOrganizerRelation> organizerRelations = new ArrayList<>();
            if (distributionAccount.getRole() == 0) {
                organizerRelations = distributionAccountOrganizerRelationService.getAccountOrganizerRelationListByPhone(null, distributionAccount.getPhone(), distributionAccount.getRole(), 0);
                if (organizerRelations.size() >= 1)
                    return WebResult.getErrorResult(WebResult.WechatCode.WECHAT_DISTRIBUTION_ACCOUNT_NOT_WORK);
            } else {
                organizerRelations = distributionAccountOrganizerRelationService.getAccountOrganizerRelationListByPhone(organizer.getOrganizerId(), distributionAccount.getPhone(), distributionAccount.getRole(), 0);
                if (organizerRelations.size() > 1)
                    return WebResult.getErrorResult(WebResult.WechatCode.WECHAT_DISTRIBUTION_ACCOUNT_EXCEPTION);
            }
            orgRelation.setRemove(0);
        }

        // 判断是否更新分销账户前置条件
//        Boolean isUpdateAccount = false;
//        if (distributionAccount.getRole() == 1) {
//            DistributionAccountOrganizerRelationExample example = new DistributionAccountOrganizerRelationExample();
//            example.createCriteria().andDistributionAccountIdEqualTo(distributionAccountId)
//                    .andValidEqualTo(1).andRemoveEqualTo(0);
//            Integer distributeOrgCount = distributionAccountOrganizerRelationService.countByExample(example);
//            if (distributeOrgCount <= 1)
//                isUpdateAccount = true;
//        } else {
//            isUpdateAccount = true;
//        }

        // 更新主办方关系
//        organizerRelationExample = new DistributionAccountOrganizerRelationExample();
//        organizerRelationExample.createCriteria().andDistributionAccountIdEqualTo(distributionAccountId)
//                .andOrganizerIdEqualTo(organizer.getOrganizerId());
        orgRelation.setUpdateTime(now);
        distributionAccountOrganizerRelationService.updateByExampleSelective(orgRelation, orgRelationExample);

        // 更新分销账户信息
//        if (isUpdateAccount) {
//            distributionAccountService.updateByPrimaryKeySelective(distributionAccount);
//            return WebResult.getErrorResult(WebResult.Code.ERROR);
//        }
        return WebResult.getSuccessResult();
    }

    @ApiOperation(value = "分销员结算", notes = "分销员结算按钮", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "startTime", value = "结算开始时间", required = false, paramType = "long", dataType = "query"),
            @ApiImplicitParam(name = "endTime", value = "结算结束时间", required = false, paramType = "long", dataType = "query"),
            @ApiImplicitParam(name = "settlementInfo", value = "结算信息，参数格式：[{\"distributionAccountId\":23, \"settlementAmount\":100},{...}]", required = false, paramType = "string", dataType = "query"),
            @ApiImplicitParam(name = "note", value = "结算备注", required = false, paramType = "string", dataType = "query"),
    })
    @RequestMapping(value = "/sales/settlement.json", method = RequestMethod.POST)
    public WebResult distributionSettlement(@RequestParam(name = "startTime", required = false) Long startTime,
                                            @RequestParam(name = "endTime", required = false) Long endTime,
                                            @RequestParam(name = "settlementInfo", required = true) String settlementInfo,
                                            @RequestParam(name = "note", required = false) String note,
                                            HttpServletRequest request, HttpSession session) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        CommonAccount account = WebUpmsContext.getAccount(request);
        if (organizer == null || account == null) {
            return WebResult.getErrorResult(WebResult.Code.COACH_GET_ORGANIZER_INFO_ERROR);
        }
        if (StringUtils.isBlank(settlementInfo))
            return WebResult.getErrorResult(WebResult.Code.DISTRIBUTION_SALES_SETTLEMENT_AMOUNT_IS_NOT_NULL);

        Date now = new Date();
        String startDate = null;
        String endDate = null;
        if (endTime == null)
            endTime = now.getTime();
        endDate = DateUtil.dateToDateString(new Date(endTime));

        JSONArray settlementInfos = JSONArray.parseArray(settlementInfo);
        DistributionSettlementRecord record = null;
        for (Object obj : settlementInfos) {
            JSONObject info = (JSONObject) obj;
            Integer distributionAccountId = info.getInteger("distributionAccountId");
            if (distributionAccountId == null) {
                continue;
            }
            BigDecimal settlementAmount = info.getBigDecimal("settlementAmount");
            BigDecimal zero = BigDecimal.ZERO;
            if (settlementAmount == null)
                settlementAmount = zero;

            if (settlementAmount.compareTo(zero) <= 0)
                continue;

            if (record == null) {
                record = new DistributionSettlementRecord();
                record.setCreateTime(now);
                record.setUpdateTime(now);
                record.setNote(note);
                record.setOperatorId(account.getCustomerId());
                record.setOperatorName(account.getRealname() != null ? account.getRealname() : account.getNickname());
                record.setOperatorTime(now);
                record.setSettlementTime(now);
                record.setOrganizerId(organizer.getOrganizerId());
                record.setType(0);
                record.setValid(1);
                record.setEndTime(new Date(endTime));
            }

            if (startTime == null) {
                /*DistributionSettlementRecordExample example = new DistributionSettlementRecordExample();
                example.createCriteria().andOrganizerIdEqualTo(organizer.getOrganizerId()).andDistributionAccountIdEqualTo(distributionAccountId)
                        .andTypeEqualTo(0).andValidEqualTo(1);*/
                DistributionSettlementRecord settlementRecord = distributionSettlementRecordService.getLastDistributionSettlementRecord(organizer.getOrganizerId(), distributionAccountId, 0, 1);
                if (settlementRecord != null) {
                    startDate = DateUtil.dateToDateString(settlementRecord.getEndTime());
                    record.setStartTime(settlementRecord.getEndTime());
                }
            }

            BigDecimal shouldSettlementAmount = verifyService.getDistributionAccountShouldSettlementAmount(organizer.getOrganizerId(), distributionAccountId, startDate, endDate);
            if (shouldSettlementAmount == null) {
                shouldSettlementAmount = zero;
            }

            if (settlementAmount.compareTo(shouldSettlementAmount) <= 0) {
                settlementAmount = shouldSettlementAmount;
            } else {
                continue;
            }

            record.setId(null);
            record.setDistributionAccountId(distributionAccountId);
            record.setSettlementAmount(settlementAmount);
            distributionAccountService.distributionSalesAmountSettlement(record);
        }

        return WebResult.getSuccessResult();
    }

    @ApiOperation(value = "获取结算记录", notes = "获取结算记录", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "distributionAccountId", value = "分销员ID", required = false, paramType = "int", dataType = "query"),
            @ApiImplicitParam(name = "startTime", value = "开始时间", required = false, paramType = "long", dataType = "query"),
            @ApiImplicitParam(name = "endTime", value = "结束时间", required = false, paramType = "long", dataType = "query"),
            @ApiImplicitParam(name = "page", value = "页码", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "size", value = "每一页数量", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "sortProp", value = "按照某一列值进行排序", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "sortType", value = "asc 升序，desc 降序", required = false, dataType = "int", paramType = "query")
    })
    @RequestMapping(value = "/settlement/record.json", method = RequestMethod.GET)
    public WebResult getDistributionSettlementRecord(@RequestParam(name = "startTime", required = false) Long startTime,
                                            @RequestParam(name = "endTime", required = false) Long endTime,
                                            @RequestParam(name = "distributionAccountId", required = false) Integer distributionAccountId,
                                            @RequestParam(name = "page", required = false, defaultValue = "1") Integer page,
                                            @RequestParam(name = "size", required = false, defaultValue = "10") Integer size,
                                            @RequestParam(name = "sortProp", required = false, defaultValue = "operator_time") String sortProp,
                                            @RequestParam(name = "sortType", required = false, defaultValue = "desc") String sortType,
                                            HttpServletRequest request, HttpSession session) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null) {
            return WebResult.getErrorResult(WebResult.Code.COACH_GET_ORGANIZER_INFO_ERROR);
        }

        String startDate = null;
        String endDate = null;
        if (startTime != null)
            startDate = DateUtil.dateToDateString(new Date(startTime));

        if (endTime != null)
            endDate = DateUtil.dateToDateString(new Date(endTime));

        PageList pageList = distributionSettlementRecordService.getDistributionSettlementRecordList(organizer.getOrganizerId(), distributionAccountId, startDate, endDate, page, size, sortProp, sortType);
        return WebResult.getSuccessResult("data", pageList);
    }
}
