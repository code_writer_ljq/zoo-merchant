package com.zoo.zoomerchantadminweb.distribution.controller;

import com.zoo.common.page.PageList;
import com.zoo.activity.dao.model.Organizer;
import com.zoo.activity.service.OrdersService;
import com.zoo.web.bean.WebResult;
import com.zoo.zoomerchantadminweb.context.WebUpmsContext;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Created by ljq on 2019/4/17.
 */
@Api(value = "api/server/distribution/order", tags = {"【分销后台】分销订单管理"}, description = "分销订单相关接口")
@RestController
@RequestMapping("api/server/distribution/order")
public class DistributionOrderController {

    @Autowired
    private OrdersService ordersService;

    @ApiOperation(value = "分销订单列表", notes = "获取分销列表数据", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "activityId", value = "产品ID", required = false, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "episodeId", value = "场次ID", required = false, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "status", value = "状态", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "phone", value = "手机号", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "code", value = "订单号", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "startTime", value = "开始时间", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "endTime", value = "结束时间", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "payType", value = "订单来源{\"微信支付\", \"线上储值支付\"}", required = false, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "role", value = "分销角色", required = false, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "distributionAccountId", value = "分销员ID: distributionAccountId", required = false, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "isRemark", value = "备注 0 无备注， 1有备注", required = false, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "page", value = "页", required = false, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "size", value = "条", required = false, dataType = "integer", paramType = "query")
    })
    @RequestMapping(value = "/list.json", method = RequestMethod.GET)
    public WebResult getDistributionOrders(@RequestParam(value = "activityId", required = false) Integer activityId,
                                           @RequestParam(value = "episodeId", required = false) Integer episodeId,
                                           @RequestParam(value = "status", required = false) String status,
                                           @RequestParam(value = "phone", required = false) String phone,
                                           @RequestParam(value = "code", required = false) String code,
                                           @RequestParam(value = "startTime", required = false) String startTime,
                                           @RequestParam(value = "endTime", required = false) String endTime,
                                           @RequestParam(value = "payType", required = false) Integer payTypeId,
                                           @RequestParam(value = "role", required = false) Integer role,
                                           @RequestParam(value = "distributionAccountId", required = false) Integer distributionAccountId,
                                           @RequestParam(value = "isRemark", required = false) Integer isRemark,
                                           @RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
                                           @RequestParam(value = "size", required = false, defaultValue = "10") Integer size,
                                           HttpServletRequest request, HttpSession session) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null) {
            return  WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }

        if (code != null) {
            code = code.trim();
        }

        if (StringUtils.isBlank(status) || status.equals("[]")) {
            status = null;
        } else {
            status = "(" + status + ")";
        }

        PageList pageList = ordersService.getDistributionOrderListByOption(organizer.getOrganizerId(), activityId, episodeId, status, phone, code, startTime, endTime, payTypeId, role, distributionAccountId, isRemark, page, size);
        return WebResult.getSuccessResult("data", pageList);
    }

    @ApiOperation(value = "分销订单列表", notes = "获取分销列表数据", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "activityId", value = "产品ID", required = false, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "episodeId", value = "场次ID", required = false, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "status", value = "状态", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "phone", value = "手机号", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "code", value = "订单号", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "startTime", value = "开始时间", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "endTime", value = "结束时间", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "verifyStartTime", value = "开始时间", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "verifyEndTime", value = "结束时间", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "payType", value = "订单来源{\"微信支付\", \"线上储值支付\"}", required = false, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "role", value = "分销角色", required = false, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "distributionAccountId", value = "分销员ID: distributionAccountId", required = false, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "isRemark", value = "备注 0 无备注， 1有备注", required = false, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "page", value = "页", required = false, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "size", value = "条", required = false, dataType = "integer", paramType = "query")
    })
    @RequestMapping(value = "/verifyList.json", method = RequestMethod.GET)
    public WebResult getDistributionVerifyOrders(@RequestParam(value = "activityId", required = false) Integer activityId,
                                           @RequestParam(value = "episodeId", required = false) Integer episodeId,
                                           @RequestParam(value = "status", required = false) String status,
                                           @RequestParam(value = "phone", required = false) String phone,
                                           @RequestParam(value = "code", required = false) String code,
                                           @RequestParam(value = "startTime", required = false) String startTime,
                                           @RequestParam(value = "endTime", required = false) String endTime,
                                           @RequestParam(value = "verifyStartTime", required = false) String verifyStartTime,
                                           @RequestParam(value = "verifyEndTime", required = false) String verifyEndTime,
                                           @RequestParam(value = "payType", required = false) Integer payTypeId,
                                           @RequestParam(value = "role", required = false) Integer role,
                                           @RequestParam(value = "distributionAccountId", required = false) Integer distributionAccountId,
                                           @RequestParam(value = "isRemark", required = false) Integer isRemark,
                                           @RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
                                           @RequestParam(value = "size", required = false, defaultValue = "10") Integer size,
                                           HttpServletRequest request, HttpSession session) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null) {
            return  WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }

        if (code != null) {
            code = code.trim();
        }

        if (StringUtils.isBlank(status) || status.equals("[]")) {
            status = null;
        } else {
            status = "(" + status + ")";
        }

        PageList pageList = ordersService.getDistributionVerifyOrderListByOption(organizer.getOrganizerId(), activityId, episodeId, status, phone, code, startTime, endTime, verifyStartTime, verifyEndTime, payTypeId, role, distributionAccountId, isRemark, page, size);
        return WebResult.getSuccessResult("data", pageList);
    }
}
