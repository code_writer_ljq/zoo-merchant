package com.zoo.zoomerchantadminweb.web.interceptor;

import com.zoo.activity.dao.model.Organizer;
import com.zoo.activity.dao.nutz.pojo.CommonAccount;
import com.zoo.activity.service.OrganizerService;
import com.zoo.finance.dao.model.ZooOperatorLogin;
import com.zoo.finance.dao.model.ZooOperatorLoginExample;
import com.zoo.finance.service.ZooOperatorLoginService;
import com.zoo.web.bean.WebResult;
import com.zoo.web.context.ThreadContext;
import com.zoo.zoomerchantadminweb.author.realm.ShiroPrincipal;
import com.zoo.zoomerchantadminweb.context.WebUpmsContext;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.json.JSONObject;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;

public class ContextInterceptor implements HandlerInterceptor {

    private OrganizerService organizerService;

    private ZooOperatorLoginService zooOperatorLoginService;

    public void setOrganizerService(OrganizerService organizerService) {
        this.organizerService = organizerService;
    }

    public void setZooOperatorLoginService(ZooOperatorLoginService zooOperatorLoginService) {
        this.zooOperatorLoginService = zooOperatorLoginService;
    }

    @Override
    public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o) throws Exception {

        ThreadContext.setLocalName(httpServletRequest);

        Subject subject = SecurityUtils.getSubject();
        if(subject.isAuthenticated()){
            //登录后初始化数据
            ShiroPrincipal shiroPrincipal = (ShiroPrincipal)subject.getPrincipal();
            CommonAccount commonAccount = shiroPrincipal.getCommonAccount();
            Organizer organizer = organizerService.getOrganizerDetail(commonAccount.getOrganizerId());

            //if(commonAccount.getZooOperatorLoginId()==null){
                ZooOperatorLoginExample example = new ZooOperatorLoginExample();
                ZooOperatorLoginExample.Criteria criteria = example.createCriteria();
                criteria.andEndTimeIsNull();
                criteria.andOperatorIdEqualTo(commonAccount.getCustomerId());
                ZooOperatorLogin operatorLogin = zooOperatorLoginService.selectFirstByExample(example);
                if(operatorLogin!=null){
                    commonAccount.setZooOperatorLoginId(operatorLogin.getId());
                } else if(!httpServletRequest.getRequestURI().contains("login")){

                    WebResult result = WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
                    httpServletResponse.setCharacterEncoding("utf-8");
                    PrintWriter writer = httpServletResponse.getWriter();
                    JSONObject jsonObject = new JSONObject(result);
                    writer.write(jsonObject.toString());
                    writer.close();

                    return false;
                }
            //}

            WebUpmsContext.setAccount(httpServletRequest,commonAccount);
            WebUpmsContext.setOrganization(httpServletRequest,organizer);
        }

        return true;
    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {

    }
}
