package com.zoo.zoomerchantadminweb.web.util;

import com.zoo.activity.dao.model.Authority;
import org.apache.commons.lang3.StringUtils;

import java.util.Iterator;
import java.util.List;

/**
 * 权限工具类
 */
public class AuthorityUtil {

    public static void parasMenus(List<Authority> permission, List<Authority> topMenu, String sysvalue, int parentId) {
        //获取顶级菜单，并移除其他权限
        if (permission != null && permission.size() > 0) {
            Iterator<Authority> iterator = permission.iterator();
            while (iterator.hasNext()) {
                Authority next = iterator.next();
                if ((next.getParentId().intValue() == parentId) && (sysvalue==null || sysvalue.equals(next.getValue()))) {
                    topMenu.add(next);
                    iterator.remove();
                }
            }
        }
    }

    public static Authority getAuthority(List<Authority> sys,String sysvalue){
        if(sys==null || StringUtils.isEmpty(sysvalue)){
            return null;
        }
        for (Authority a:sys) {
            if(sysvalue.equals(a.getValue())){
                return a;
            }
        }
        return null;
    }

    /**
     * 查询权限string
     * @param parent
     * @param permission
     * @return
     */
    public static String parseChildMenuToString(Authority parent, List<Authority> permission) {
        StringBuffer menus = new StringBuffer();
        if (permission != null && permission.size() > 0) {
            Iterator<Authority> iterator = permission.iterator();
            while (iterator.hasNext()) {
                Authority next = iterator.next();
                if(next.getSysValue()!=null && next.getSysValue().equals(parent.getValue())){
                    if(menus.length()==0){
                        menus.append(next.getValue());
                    }else{
                        menus.append("," + next.getValue());
                    }
                }
                /*if (next.getParentId().intValue() == parent.getAuthorityId().intValue()) {
                    if(menus.length()==0){
                        menus.append(next.getValue());
                    }else{
                        menus.append("," + next.getValue());
                    }
                    //iterator.remove();
                    String childstring = parseChildMenuToString(next, permission);
                    if(StringUtils.isNotEmpty(childstring)){
                        menus.append("," + childstring);
                    }
                }*/
            }
        }
        return menus.toString();
    }

}
