package com.zoo.zoomerchantadminweb.account.controller;

import com.zoo.activity.dao.model.Authority;
import com.zoo.activity.dao.nutz.pojo.CommonAccount;
import com.zoo.upms.dao.model.UpmsModule;
import com.zoo.upms.dao.model.UpmsRole;
import com.zoo.upms.dao.model.UpmsRoleExample;
import com.zoo.upms.service.AuthorityService;
import com.zoo.upms.service.UpmsModuleService;
import com.zoo.upms.service.UpmsRoleService;
import com.zoo.web.bean.WebResult;
import com.zoo.zoomerchantadminweb.context.WebUpmsContext;
import com.zoo.zoomerchantadminweb.web.util.AuthorityUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

@Api(value = "api/role", tags = {"账户角色接口"}, description = "账户角色接口")
@RestController
@RequestMapping("api/role")
public class RoleController {

    @Autowired
    private UpmsRoleService upmsRoleService;

    @Autowired
    private AuthorityService authorityService;

    @Autowired
    private UpmsModuleService upmsModuleService;

    @ApiOperation(value = "获取主办方所有角色", notes = "获取主办方所有角色", httpMethod = "GET")
    @RequestMapping(value = "getRoles", method = RequestMethod.GET)
    public WebResult getRoles(HttpServletRequest request){
        CommonAccount account = WebUpmsContext.getAccount(request);
        UpmsRoleExample upmsRoleExample = new UpmsRoleExample();
        upmsRoleExample.createCriteria().andOrganizerIdEqualTo(account.getOrganizerId());
        List<UpmsRole> upmsRoles = upmsRoleService.selectByExample(upmsRoleExample);
        WebResult<Object> result = WebResult.getSuccessResult();
        result.putResult("list",upmsRoles);
        return result;
    }

    @ApiOperation(value = "获取角色权限列表", notes = "获取角色权限列表", httpMethod = "GET")
    @ApiImplicitParam(name = "roleId", value = "角色ID", required = true, dataType = "int", paramType = "query")
    @RequestMapping(value = "getRolesAuthority", method = RequestMethod.GET)
    public WebResult getRolesAuthority(HttpServletRequest request,Long roleId){
        List<Authority> list = authorityService.getAuthorityListByRoleId(roleId);
        List<UpmsModule> upmsModules = upmsModuleService.selectModuleListByRoleId(roleId);
        WebResult<Object> result = WebResult.getSuccessResult();

        //获取系统
        List<Authority> sys = authorityService.getAuthorityListByParentId(0L);
        AuthorityUtil.parasMenus(list, sys,null,0);

        Map<String,String> premissionlist = new HashMap<String,String>();
        String sysBuffer = new String();
        if(sys!=null){
            for(Authority au : sys){
                sysBuffer += "," + au.getValue();
                premissionlist.put(au.getValue(),AuthorityUtil.parseChildMenuToString(au,list));
            }
        }

        //查询模块
        Map<String,String> moduls = new HashMap<String,String>();
        if(upmsModules!=null){
            for (UpmsModule upm : upmsModules){
                String s = moduls.get(upm.getSysValue());
                if(StringUtils.isEmpty(s)){
                    s = upm.getUpmsModuleId()+"";
                }else{
                    s += "," + upm.getUpmsModuleId();
                }
                moduls.put(upm.getSysValue(),s);
            }
        }

        result.putResult("authorityIds",premissionlist);
        result.putResult("moduleIds",moduls);
        result.putResult("sys",sysBuffer);
        return result;
    }

}
