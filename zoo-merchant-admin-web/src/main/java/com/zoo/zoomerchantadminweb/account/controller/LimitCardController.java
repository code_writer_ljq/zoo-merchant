package com.zoo.zoomerchantadminweb.account.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.pay.unionpay.api.UnionPayApi;
import com.pay.unionpay.api.WXPaymentCodeApi;
import com.zoo.activity.util.ImageUtil;
import com.zoo.activity.util.RegexUtil;
import com.zoo.bean.util.ActivityBeanUtils;
import com.zoo.common.page.PageList;
import com.zoo.activity.dao.model.*;
import com.zoo.activity.dao.nutz.pojo.CommonAccount;
import com.zoo.activity.service.*;
import com.zoo.activity.util.DateUtil;
import com.zoo.icenow.dao.model.Entry;
import com.zoo.icenow.service.EnterService;
import com.zoo.web.bean.WebResult;
import com.zoo.wechat.common.Config;
import com.zoo.wechat.pay.util.HttpClientUtil;
import com.zoo.zoomerchantadminweb.base.BaseController;
import com.zoo.zoomerchantadminweb.context.WebUpmsContext;
import com.zoo.zoomerchantadminweb.order.controller.SweepPayUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import jodd.util.StringUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

/**
 * server 时限卡相关
 * Created by jmf on 2018/12/3.
 */
@Api(value = "api/server/limit-card", tags = {"主办方时限卡相关接口"}, description = "主办方时限卡相关接口描述")
@RestController
@RequestMapping("api/server/limit-card")
public class LimitCardController extends BaseController {

    @Autowired
    private LimitCardService limitCardService;

    @Autowired
    private OrdersService ordersService;

    @Autowired
    private ChargeService chargeService;

    @Autowired
    private DepositService depositService;

    @Autowired
    private VerifyService verifyService;

    @Autowired
    private EpisodeService episodeService;

    @Autowired
    private EnterService enterService;

    @Autowired
    private PlayerService playerService;


    @Resource(name = "organizerPayConfigService")
    private OrganizerPayConfigService organizerPayConfigService;

    /**
     * 扫码订单支付记录
     */
    @Autowired
    private OrderSweepCodePayService orderSweepCodePayService;

    /**
     * 支付失败
     */
    final String PAY_ERROR = "PAY_ERROR";

    /**
     * 支付成功
     */
    final String PAY_SUCCESS = "PAY_SUCCESS";

    @ApiOperation(value = "查询会员信息列表", notes = "查询会员信息列表功能", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页", required = false, dataType = "int", paramType = "query", defaultValue = "1"),
            @ApiImplicitParam(name = "size", value = "条", required = false, dataType = "int", paramType = "query", defaultValue = "10"),
            @ApiImplicitParam(name = "phone", value = "会员手机号", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "name", value = "会员名", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "id", value = "id", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "startTime", value = "开始时间", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "endTime", value = "结束时间", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "idCard", value = "身份证号", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "number", value = "卡号", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "valid", value = "禁用/启用", required = false, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "keyword", value = "关键字查询", required = false, dataType = "string", paramType = "query")

    })
    @RequestMapping(value = "/list.json", method = RequestMethod.GET)
    public Object getCustomerList(@RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
                                  @RequestParam(value = "size", required = false, defaultValue = "10") Integer size,
                                  @RequestParam(value = "phone", required = false) String phone,
                                  @RequestParam(value = "name", required = false) String name,
                                  @RequestParam(value = "idCard", required = false) String idCard,
                                  @RequestParam(value = "startTime", required = false) String startTime,
                                  @RequestParam(value = "endTime", required = false) String endTime,
                                  @RequestParam(value = "number", required = false) String number,
                                  @RequestParam(value = "id", required = false) Integer id,
                                  @RequestParam(value = "valid", required = false) Integer valid,
                                  @RequestParam(value = "keyword", required = false) String keyword,
                                  HttpServletRequest request) {

        CommonAccount account = WebUpmsContext.getAccount(request);
        if (account == null) {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
        if (StringUtils.isNotEmpty(keyword)) {
            if (RegexUtil.isPhoneNumber(keyword)) {
                phone = keyword;
            } else {
                name = keyword;
            }
        }
        PageList pageList;
        try {
            pageList = limitCardService.getCustomerListByOrgId(account.getOrganizerId(), name, phone, idCard, startTime, endTime, id, number, valid, page, size);
        } catch (Exception e) {
            e.printStackTrace();
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }

        return WebResult.getSuccessResult("data", pageList);
    }

    @RequestMapping(value = "/save.json", method = RequestMethod.POST)
    public WebResult saveCustomer(@ModelAttribute LimitCardMember limitCardMember, HttpServletRequest request) {
        CommonAccount account = WebUpmsContext.getAccount(request);
        if (null == account) {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
        Organizer orgnization = WebUpmsContext.getOrgnization(request);

        Integer orderId = limitCardMember.getOrderId();
        LimitCardMember limitCardMemberByOrderId = limitCardService.getLimitCardMemberByOrderId(orderId);
        if(limitCardMemberByOrderId != null) return WebResult.getErrorResult("该订单已开卡！");
        OrderView orderView = ordersService.getOrderDetail(orderId);
        limitCardMember.setChargeId(orderView.getChargeId());
        limitCardMember.setChargeName(orderView.getChargeName());
        limitCardMember.setOrganizerId(account.getOrganizerId());
        limitCardMember.setOperatorId(account.getCustomerId());
        limitCardMember.setOperatorName(account.getRealname());
        limitCardMember.setStatus(1);
        limitCardMember.setValid(1);
        Charge charge = chargeService.getChargeById(limitCardMember.getChargeId());
        Episode episode = episodeService.getEpisodeDetailById(charge.getEpisodeId());
        Date now = new Date();
        Date endDate = DateUtil.addDay(now, charge.getValidRange().intValue());

        limitCardMember.setStartTime(now);
        limitCardMember.setCreateTime(now);

        limitCardMember.setEndTime(endDate.before(episode.getEndTime()) ? endDate : episode.getEndTime());
        Material material = depositService.selectByPrimaryKey(limitCardMember.getMaterialId());
        material.setStatus(1);
        depositService.updateMaterial(material);
        limitCardService.saveLimitCardMember(limitCardMember);

        CompletableFuture.runAsync(() -> {
            Map<String, Object> map = new HashMap<>();

            String img = ImageUtil.encodeImgageToBase64(limitCardMember.getPhoto());
            map.put("dataType", 2);
            map.put("requestID", System.currentTimeMillis());
            map.put("typeNumber", "");
            map.put("img", img);
            String response = HttpClientUtil.httpPostJson4QiShan(Config.instance().getFaceUrl(), JSON.toJSONString(map));
            JSONObject jsonObject = JSONObject.parseObject(response);
            JSONObject data = jsonObject.getJSONObject("data");
            String faceFeature = data.getString("faceFeature");
            if (StringUtils.isNotEmpty(faceFeature)) {
                LimitCardFace limitCardFace = new LimitCardFace();
                limitCardFace.setFaceId(faceFeature);
                LimitCardMember limitCardMemberByCard = limitCardService.getLimitCardMemberByCard(limitCardMember.getMaterialId());
                limitCardFace.setLimitCardId(limitCardMemberByCard.getId());
                limitCardFace.setOrganizerId(orgnization.getOrganizerId());
                Date date = new Date();
                limitCardFace.setCreateTime(date);
                limitCardFace.setUpdateTime(date);
                limitCardService.saveLimitCardFace(limitCardFace);
            }


        });
        if (orderView.getOrderStatus() == 1) {
            try {
                verifyService.verify(orderView.getOrderId(), orderView.getAvailableCount(), orderView.getTypeId(), orderView.getSellerId(), account.getCustomerId().toString(), 1, null);
            } catch (Exception e) {
                return WebResult.getErrorResult(WebResult.Code.ERROR);
            }
        }

        return WebResult.getSuccessResult();
    }

    @RequestMapping(value = "/checkOrder.json", method = RequestMethod.POST)
    public WebResult checkOrder(@RequestParam("code") String code, HttpServletRequest request) {
        CommonAccount account = WebUpmsContext.getAccount(request);
        if (null == account) {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }

        Orders orders = ordersService.getOrdersByCode(code);

        if (null == orders) {
            return WebResult.getErrorMsgResult(WebResult.Code.ERROR, "订单号错误");
        }

        if (!orders.getSellerId().equals(account.getOrganizerId()) || null == orders.getChargeId() || (orders.getStatus() != 1 && orders.getStatus() != 5)) {
            return WebResult.getErrorMsgResult(WebResult.Code.ERROR, "无效订单");
        }

        if (orders.getParentId() == null) {
            return WebResult.getErrorMsgResult(WebResult.Code.ERROR, "有租赁物或增值服务的订单无法绑定");
        }

        if (orders.getChargeNum() > 1) {
            return WebResult.getErrorMsgResult(WebResult.Code.ERROR, "订单数量大于1");
        }

        Charge charge = chargeService.getChargeById(orders.getChargeId());

        if (charge.getChargeMode() != 4) {
            return WebResult.getErrorMsgResult(WebResult.Code.ERROR, "购买产品必须为时限卡产品");
        }

        if (orders.getParentId() != 0) {
            return WebResult.getErrorMsgResult(WebResult.Code.ERROR, "无效订单");
        }

        LimitCardMember limitCardMember = limitCardService.getLimitCardMemberByOrderId(orders.getOrderId());

        if (null == limitCardMember) {

            Map<String, Object> resultMap = new HashMap<>();
            List<Player> playerList = playerService.getPlayersByOrder(orders.getOrderId());

            if (null != playerList && playerList.size() > 0) {
                Player player = playerList.get(0);
                resultMap.put("idCard", player.getIdCardNo());
                resultMap.put("phone", player.getPhone());
                resultMap.put("name", player.getRealName());
            }

            resultMap.put("orderId", orders.getOrderId());

            return WebResult.getSuccessResult("data", resultMap);
        }
        return WebResult.getErrorMsgResult(WebResult.Code.ERROR, "订单已使用");
    }

    @RequestMapping(value = "/detail.json", method = RequestMethod.GET)
    public WebResult getLimitCardMember(@RequestParam("id") Integer id, HttpServletRequest request) {
        CommonAccount account = WebUpmsContext.getAccount(request);
        if (null == account) {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
        Map map = limitCardService.getLimitCardInfo(id);
        if (!Integer.valueOf(map.get("organizerId").toString()).equals(account.getOrganizerId())) {
            return WebResult.getErrorResult(WebResult.Code.NO_PERMISSION);
        }
        WebResult webResult = WebResult.getSuccessResult();
        webResult.putResult("data", map);
        return webResult;
    }

    @RequestMapping(value = "/detailByCard.json", method = RequestMethod.GET)
    public WebResult getLimitCardMemberByCard(@RequestParam(value = "cardNumber", required = false) Long cardNumber, HttpServletRequest request) {
        CommonAccount account = WebUpmsContext.getAccount(request);
        if (null == account) {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
        String hex = null;
        if (cardNumber != null) {
            hex = Long.toHexString(cardNumber);
            hex = transform(hex, "");
        }
        Material material = depositService.selectByGateWaferNum(hex, account.getOrganizerId(), 1);

        if (material == null) {
            return WebResult.getErrorMsgResult(WebResult.Code.ERROR, "目标卡无效");
        }

        LimitCardMember limitCardMember = limitCardService.getLimitCardMemberByCard(material.getMaterialId());
        if (limitCardMember == null) {
            return WebResult.getErrorResult(WebResult.Code.CASHIER_NOT_SEASON_CARD);
        }

        if (!limitCardMember.getOrganizerId().equals(account.getOrganizerId())) {
            return WebResult.getErrorResult(WebResult.Code.NO_PERMISSION);
        }
        WebResult webResult = WebResult.getSuccessResult();
        webResult.putResult("data", limitCardMember);
        return webResult;
    }

    @RequestMapping(value = "/detailByGateWaferNum.json", method = RequestMethod.GET)
    public WebResult detailByGateWaferNum(@RequestParam(value = "cardNumber", required = false) Long cardNumber, HttpServletRequest request) {
        CommonAccount account = WebUpmsContext.getAccount(request);
        if (null == account) {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
        String hex = null;
        if (cardNumber != null) {
            hex = Long.toHexString(cardNumber);
            hex = transform(hex, "");
        }
        Material material = depositService.selectByGateWaferNum(hex, account.getOrganizerId(), 1);

        if (material == null) {
            return WebResult.getErrorMsgResult(WebResult.Code.ERROR, "目标卡无效");
        }

        WebResult webResult = WebResult.getSuccessResult();
        webResult.putResult("data", material);
        return webResult;
    }

    @RequestMapping(value = "/activation.json", method = RequestMethod.GET)
    public WebResult getActivationRecordsByMemberId(@RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
                                                    @RequestParam(value = "size", required = false, defaultValue = "10") Integer size,
                                                    @RequestParam(value = "memberId", required = false) Integer memberId, HttpServletRequest request) {
        CommonAccount account = WebUpmsContext.getAccount(request);
        if (null == account) {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
        PageList pageList;
        try {
            pageList = limitCardService.getActivationRecordsByMemberId(memberId, page, size);
        } catch (Exception e) {
            e.printStackTrace();
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }

        return WebResult.getSuccessResult("data", pageList);
    }

    @RequestMapping(value = "/activate.json", method = RequestMethod.POST)
    public WebResult activate(@RequestParam("id") Integer id, HttpServletRequest request) {
        CommonAccount account = WebUpmsContext.getAccount(request);
        if (null == account) {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
        return limitCardService.activate(id, account);
    }

    @RequestMapping(value = "/indemnity.json", method = RequestMethod.POST)
    public WebResult indemnity(@ModelAttribute LimitCardIndemnity LimitCardIndemnity, HttpServletRequest request) {
        CommonAccount account = WebUpmsContext.getAccount(request);
        if (null == account) {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
        return limitCardService.indemnity(LimitCardIndemnity, account);
    }

    @RequestMapping(value = "/sweepIndemnity.json", method = RequestMethod.POST)
    public WebResult sweepIndemnity(@ModelAttribute LimitCardIndemnity limitCardIndemnity, HttpServletRequest request,
                                    @RequestParam(value = "authorcode", required = false) String authorcode) {
        CommonAccount account = WebUpmsContext.getAccount(request);
        if (null == account) {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }

        Organizer organizer = WebUpmsContext.getOrgnization(request);

        OrganizerPayConfig organizerPayConfigByOrgId = organizerPayConfigService.getOrganizerPayConfigByOrgId(organizer.getOrganizerId());

        // String unipayOpShopId = organizerPayConfigService.getUnipayOpShopId(organizer.getOrganizerId());
        /*if (organizerPayConfigByOrgId == null) {
            return WebResult.getErrorMsgResult(WebResult.Code.ERROR, "没有支付配置");
        }*/
        SweepPayUtils.clearResult(organizer.getOrganizerId(), limitCardIndemnity.getNewCard().longValue(), account.getCustomerId());
        new Thread() {
            @Override
            public void run() {
                String extSweepCodeWay = null;
                OrderSweepCodePay sweepCodePay = null;
                Map<String, String> responseResult = null;
                Map<String, String> paramMap = null;
                String code = limitCardIndemnity.getMemberId() + "" + limitCardIndemnity.getNewCard() + account.getCustomerId() + organizer.getOrganizerId();
                try {
                    sweepCodePay = orderSweepCodePayService.findOrderSweepCodePaySuccess(code);

                    if (sweepCodePay != null) {
                        if (sweepCodePay.getPayType() == 13) {
                            if (org.apache.commons.lang.StringUtils.isEmpty(organizerPayConfigByOrgId.getUnipayOpenapiMchId())) {
                                responseResult = UnionPayApi.tradeQuery(sweepCodePay.getPayCode(), organizerPayConfigByOrgId.getUnipayOpShopId(), organizerPayConfigByOrgId.getUnipayOpenapiMchId(), organizerPayConfigByOrgId.getUnipayOpenapiKey());
                            } else {
                                responseResult = UnionPayApi.openApiUnifiedTradeQuery(sweepCodePay.getPayCode(), organizerPayConfigByOrgId.getUnipayOpenapiMchId(),
                                        organizerPayConfigByOrgId.getUnipayOpenapiKey());
                            }
                        } else {
                            paramMap = new HashMap<>();
                            if (organizerPayConfigByOrgId != null && org.apache.commons.lang.StringUtils.isNotBlank(organizerPayConfigByOrgId.getSubmchid())) {
                                paramMap.put("sub_mch_id", organizerPayConfigByOrgId.getSubmchid());
                            }
                            paramMap.put("out_trade_no", sweepCodePay.getPayCode());
                            responseResult = WXPaymentCodeApi.orderQuery(paramMap);
                        }
                    } else {
                        Orders orders = new Orders();
                        orders.setOrderId(limitCardIndemnity.getNewCard());
                        orders.setPayTypeId(limitCardIndemnity.getPayType());
                        orders.setCode(code);
//                        orders.setOrderId(limitCardIndemnity.getNewCard());
                        //创建支付记录
                        sweepCodePay = orderSweepCodePayService.createPay(orders);

                        int totalPrice = limitCardIndemnity.getAmount().multiply(BigDecimal.valueOf(100)).intValue();

                        if (limitCardIndemnity.getPayType() == 13) {
                            if (org.apache.commons.lang.StringUtils.isEmpty(organizerPayConfigByOrgId.getUnipayOpenapiMchId())) {
                                responseResult = UnionPayApi.pay(sweepCodePay.getPayCode(), totalPrice,
                                        "时限卡换卡", organizerPayConfigByOrgId.getUnipayOpShopId(), authorcode);
                            } else {
                                responseResult = UnionPayApi.openApiPay(sweepCodePay.getPayCode(), totalPrice,
                                        "时限卡换卡", organizerPayConfigByOrgId.getUnipayOpenapiMchId(),
                                        organizerPayConfigByOrgId.getUnipayOpenapiKey(), authorcode);
                            }
                            sweepCodePay.setPayWay(UnionPayApi.getPayWay(responseResult));
                            sweepCodePay.setTransactionId(UnionPayApi.getTransactionId(responseResult));
                            sweepCodePay.setTime(new Date());
                            if (!UnionPayApi.isSuccess(responseResult) && !UnionPayApi.isNeedQuery(responseResult)) {
                                paySweepCodeError(sweepCodePay, responseResult, organizerPayConfigByOrgId, limitCardIndemnity, organizerPayConfigByOrgId.getUnipayOpShopId(), account, organizer, UnionPayApi.getErrMsg(responseResult));
                                return;
                            } else if (UnionPayApi.isResultSuccess(responseResult)) {
                                sweepCodePay.setTradeStat(ActivityBeanUtils.SweepPayTradeStat.SUCCESS.toString());
                            } else if (UnionPayApi.isNeedQuery(responseResult)) {
                                int count = 0;
                                Map<String, String> post = null;
                                while (post == null) {
                                    try {
                                        post = null;
                                        Thread.sleep(1000);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }

                                    if (org.apache.commons.lang.StringUtils.isEmpty(organizerPayConfigByOrgId.getUnipayOpenapiMchId())) {
                                        post = UnionPayApi.tradeQuery(sweepCodePay.getPayCode(), organizerPayConfigByOrgId.getUnipayOpShopId(), organizerPayConfigByOrgId.getUnipayOpenapiMchId(), organizerPayConfigByOrgId.getUnipayOpenapiKey());
                                    } else {
                                        post = UnionPayApi.openApiUnifiedTradeQuery(sweepCodePay.getPayCode(), organizerPayConfigByOrgId.getUnipayOpenapiMchId(),
                                                organizerPayConfigByOrgId.getUnipayOpenapiKey());
                                    }


                                    if (!UnionPayApi.isContinueTradeQuery(post)) {
                                        break;
                                    }
                                    if (UnionPayApi.isSuccessTradeQuery(post)) {
                                        break;
                                    }
                                    count++;
                                    if (count > 60) {
                                        break;
                                    }
                                    post = null;
                                }
                                if (!UnionPayApi.isSuccessTradeQuery(post)) {
                                    // 撤销订单 (银联支付失败)
                                    // 撤销支付
                                    paySweepCodeError(sweepCodePay, responseResult, organizerPayConfigByOrgId, limitCardIndemnity, organizerPayConfigByOrgId.getUnipayOpShopId(), account, organizer, UnionPayApi.getErrMsg(post));
                                    return;
                                } else {
                                    responseResult = post;
                                    sweepCodePay.setTradeStat(ActivityBeanUtils.SweepPayTradeStat.SUCCESS.toString());
                                }
                            }
                            // extSweepCodeWay = UnionPayApi.getPayWay(responseResult);
                            // transactionId = UnionPayApi.getTransactionId(responseResult);
                        } else {
                            // 组装支付业务数据
                            if (organizerPayConfigByOrgId != null && org.apache.commons.lang.StringUtils.isNotBlank(organizerPayConfigByOrgId.getSubmchid())) {
                                paramMap.put("sub_mch_id", organizerPayConfigByOrgId.getSubmchid());
                            }
                            paramMap.put("out_trade_no", sweepCodePay.getPayCode());
                            paramMap.put("device_info", String.valueOf(organizer.getOrganizerId()));
                            paramMap.put("body", "时限卡换卡");
                            paramMap.put("total_fee", String.valueOf(totalPrice));
                            paramMap.put("fee_type", "CNY");
                            paramMap.put("auth_code", authorcode);
                            responseResult = WXPaymentCodeApi.microPay(paramMap);

                            if (!WXPaymentCodeApi.isMicroPaySuccess(responseResult)) {
                                if (!WXPaymentCodeApi.isSuccess(responseResult)) {
                                    paySweepCodeError(sweepCodePay, responseResult, organizerPayConfigByOrgId, limitCardIndemnity, organizerPayConfigByOrgId.getUnipayOpShopId(), account, organizer, WXPaymentCodeApi.getErrMsg(responseResult));
                                    return;
                                }

                                int count = 0;
                                Map<String, String> wxPost = null;
                                // 组装线下微信支付查询订单api业务数据
                                paramMap = new HashMap<>();
                                if (organizerPayConfigByOrgId != null && org.apache.commons.lang.StringUtils.isNotBlank(organizerPayConfigByOrgId.getSubmchid())) {
                                    paramMap.put("sub_mch_id", organizerPayConfigByOrgId.getSubmchid());
                                }
                                paramMap.put("out_trade_no", sweepCodePay.getPayCode());
                                while (wxPost == null) {
                                    try {
                                        Thread.sleep(1000);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }

                                    wxPost = WXPaymentCodeApi.orderQuery(paramMap);

                                    if (!WXPaymentCodeApi.isSuccess(wxPost)) {
                                        break;
                                    }
                                /*if (!WXPaymentCodeApi.resultCode(wxPost)) {
                                    break;
                                }*/

                                    if (WXPaymentCodeApi.isSuccessTradeStateQueryBySuccess(wxPost)) {
                                        break;
                                    }

                                    if (!WXPaymentCodeApi.isContinueTradeStateQueryByUserpaying(wxPost)) {
                                        break;
                                    }
                                    count++;
                                    if (count > 60) {
                                        break;
                                    }
                                    wxPost = null;
                                }
                                if (!WXPaymentCodeApi.isQueryPayResultSuccess(wxPost)) {
                                    // 撤销订单 (微信线下扫码支付失败)
                                    // 撤销支付
                                    paySweepCodeError(sweepCodePay, responseResult, organizerPayConfigByOrgId, limitCardIndemnity, organizerPayConfigByOrgId.getUnipayOpShopId(), account, organizer, WXPaymentCodeApi.getErrMsg(responseResult));
                                    return;
                                } else {
                                    responseResult = wxPost;
                                    sweepCodePay.setTradeStat(ActivityBeanUtils.SweepPayTradeStat.SUCCESS.toString());
                                }
                            }else {
                                sweepCodePay.setTradeStat(ActivityBeanUtils.SweepPayTradeStat.SUCCESS.toString());
                            }
                            sweepCodePay.setPayWay(WXPaymentCodeApi.getPayWay(responseResult));
                            sweepCodePay.setTransactionId(WXPaymentCodeApi.getTransactionId(responseResult));
                            sweepCodePay.setTime(new Date());
                            System.out.println(DateUtil.getDateTimeStr(new Date()) + "订单号：" + sweepCodePay.getPayCode() + "----------------- 线下微信扫码支付花费时间：");
                        }
                    }
                    limitCardIndemnity.setPayOrderCode(sweepCodePay.getOrderCode());
                    WebResult webResult = limitCardService.indemnity(limitCardIndemnity, account);
                    if (webResult.isSuccess()) {
                        if (limitCardIndemnity.getPayType() == 13) {
                            //订单支付完成
                            pushPayMessage(sweepCodePay, organizer.getOrganizerId(), account.getCustomerId(), "支付成功", PAY_SUCCESS);
                        }
                    } else {
                        if (limitCardIndemnity.getPayType() == 13) {
                            //撤销支付
                            paySweepCodeError(sweepCodePay, responseResult, organizerPayConfigByOrgId, limitCardIndemnity, organizerPayConfigByOrgId.getUnipayOpShopId(), account, organizer, "支付失败");
                        }
                    }
                } catch (Exception e) {
                    //扫码支付
                    if (limitCardIndemnity.getPayType() == 13) {
                        //撤销支付
                        paySweepCodeError(sweepCodePay, responseResult, organizerPayConfigByOrgId, limitCardIndemnity, organizerPayConfigByOrgId.getUnipayOpShopId(), account, organizer, "支付失败");
                    }
                }
            }
        }.start();

        return WebResult.getSuccessResult();
    }

    /**
     * 支付失败，撤销订单
     *
     * @param LimitCardIndemnity
     * @param unipayOpShopId
     * @param commonAccount
     * @param organizer
     */
    private void paySweepCodeError(OrderSweepCodePay sweepCodePay, Map<String, String> responseMaps, OrganizerPayConfig organizerPayConfigByOrgId, LimitCardIndemnity LimitCardIndemnity, String unipayOpShopId, CommonAccount commonAccount, Organizer organizer, String payMessage) {
        //撤销支付
        try {
            // order.setStatus(4);
            // ordersService.updateOrders(order);
            if (LimitCardIndemnity.getAmount().doubleValue() != 0) {

                if (sweepCodePay.getPayType() == 13) {
                    if (StringUtil.isEmpty(organizerPayConfigByOrgId.getUnipayOpenapiMchId())) {
                        UnionPayApi.cancelPay(sweepCodePay.getPayCode(), unipayOpShopId);
                    } else {
                        UnionPayApi.openApiCancelPay(sweepCodePay.getPayCode(), organizerPayConfigByOrgId.getUnipayOpenapiMchId(), organizerPayConfigByOrgId.getUnipayOpenapiKey());
                    }
                } else {
                    Map<String, String> map = new HashMap<>();
                    if (organizerPayConfigByOrgId != null && org.apache.commons.lang.StringUtils.isNotBlank(organizerPayConfigByOrgId.getSubmchid())) {
                        map.put("sub_mch_id", organizerPayConfigByOrgId.getSubmchid());
                    }
                    map.put("out_trade_no", sweepCodePay.getPayCode());
                    map = WXPaymentCodeApi.reverse(map);
                }

                sweepCodePay.setTradeStat(ActivityBeanUtils.SweepPayTradeStat.REVOKED.toString());
            } else {
                sweepCodePay.setTradeStat(ActivityBeanUtils.SweepPayTradeStat.PAYERROR.toString());
            }
            if (responseMaps != null) {
                String transactionId;
                String payWay;
                if (sweepCodePay.getPayType() == 13) {
                    transactionId = UnionPayApi.getTransactionId(responseMaps);
                } else {
                    transactionId = WXPaymentCodeApi.getTransactionId(responseMaps);
                }
                if (org.apache.commons.lang.StringUtils.isNotEmpty(transactionId)) {
                    sweepCodePay.setTransactionId(transactionId);
                }

                if (sweepCodePay.getPayType() == 13) {
                    payWay = UnionPayApi.getPayWay(responseMaps);
                } else {
                    payWay = WXPaymentCodeApi.getPayWay(responseMaps);
                }
                if (org.apache.commons.lang.StringUtils.isNotEmpty(payWay)) {
                    sweepCodePay.setPayWay(payWay);
                }

                String errorMsg = null;
                if (sweepCodePay.getPayType() == 13) {
                    errorMsg = UnionPayApi.getErrMsg(responseMaps);
                } else {
                    errorMsg = WXPaymentCodeApi.getErrMsg(responseMaps);
                }
                sweepCodePay.setErrorMsg(errorMsg);
            }
            pushPayMessage(sweepCodePay, organizer.getOrganizerId(), commonAccount.getCustomerId(), payMessage, PAY_ERROR);
        } catch (UnknownHostException ex) {
            ex.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 通知支付信息 (前端接收：PAY_ERROR、PAY_SUCCESS、PAY_RETRY)
     *
     * @param orgId
     * @param userId
     * @param payMessage
     */
    private void pushPayMessage(OrderSweepCodePay sweepCodePay, long orgId, long userId, String payMessage, String status) {

        orderSweepCodePayService.updateByPrimaryKey(sweepCodePay);

        SweepPayUtils.putResult(orgId, sweepCodePay.getOrderId(), userId, payMessage, status);

        /*Map<String, String> parasMap = new HashMap<String, String>();
        parasMap.put("orgId", "" + orgId);
        parasMap.put("userId", "" + userId);
        parasMap.put("orderCode", "" + sweepCodePay.getOrderCode());
        WebsocketSender.send(payMessage, parasMap);*/

        /*Map<String, WebSocketSession> sessionMap = webSocketEndPoint.getSocketSessionMap(orgId , userId);
        if (sessionMap != null && sessionMap.size() > 0) {
            TextMessage textMessage = new TextMessage(message);

            for (Map.Entry<String, WebSocketSession> entry : sessionMap.entrySet()) {
                try {
                    entry.getValue().sendMessage(textMessage);
                } catch (Exception e) {
                    // 异常信息
                    e.printStackTrace();
                }
            }
        } else {
            System.out.println("连接丢失，支付通知失败....");
        }*/

    }

    @RequestMapping(value = "/indemnity/list.json", method = RequestMethod.GET)
    public WebResult getIndemnityListByMemberId(@RequestParam(value = "memberId", required = false) Integer memberId, HttpServletRequest request) {
        CommonAccount account = WebUpmsContext.getAccount(request);
        if (null == account) {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
        PageList pageList;
        try {
            pageList = limitCardService.getIndemnityListByMemberId(memberId, null, null);
        } catch (Exception e) {
            e.printStackTrace();
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }

        return WebResult.getSuccessResult("data", pageList);
    }

    @RequestMapping(value = "/valid.json", method = RequestMethod.POST)
    public WebResult valid(@RequestParam("valid") Integer valid, @RequestParam("id") Integer id, HttpServletRequest request) {
        CommonAccount account = WebUpmsContext.getAccount(request);
        if (null == account) {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
        LimitCardMember limitCardMember = limitCardService.getLimitCardMemberById(id);
        limitCardMember.setValid(valid);

        Orders orders = ordersService.getSimpleOrdersById(limitCardMember.getOrderId());
        Entry entry = enterService.getLastEntry(orders.getOrderId());
        Charge charge = chargeService.getChargeById(orders.getChargeId());

        if (valid == 0) {

            if (null == entry || entry.getStatus() == 3) {
                limitCardService.updateLimitCardMember(limitCardMember);
                return WebResult.getSuccessResult();
            }

            if (charge.getChargeMode() != 4 && entry.getStatus() == 4) {
                limitCardService.updateLimitCardMember(limitCardMember);
                return WebResult.getSuccessResult();
            }

            entry.setStatus(5);

            enterService.update(entry);

        } else {

            if (null == entry || entry.getStatus() != 5 || (null != entry.getMaxTime() && entry.getMaxTime().before(new Date()))) {
                limitCardService.updateLimitCardMember(limitCardMember);
                return WebResult.getSuccessResult();
            }
            if (entry.getStartTime() != null) {
                entry.setStatus(1);
            } else {
                entry.setStatus(0);
            }
            enterService.update(entry);

        }

        limitCardService.updateLimitCardMember(limitCardMember);

        return WebResult.getSuccessResult();
    }


    private String transform(String origin, String des) {
        if (StringUtils.isBlank(des) && origin.length() < 8) {
            int len = 8 - origin.length();
            for (int i = 0; i < len; i++)
                origin = "0" + origin;
        }
        if (origin.length() <= des.length()) {
            return des.toUpperCase();
        } else {
            String last = origin.substring(origin.length() - 2 - des.length(), origin.length() - des.length());
            return transform(origin, des + last);
        }
    }

}
