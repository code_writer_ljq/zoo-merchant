package com.zoo.zoomerchantadminweb.account.controller;


import com.zoo.account.constant.AccountConstant;
import com.zoo.account.constant.AccountEnum;
import com.zoo.account.service.AccountService;
import com.zoo.account.support.AccountResult;
import com.zoo.activity.dao.model.OrganizerPayConfig;
import com.zoo.activity.service.OrganizerPayConfigService;
import com.zoo.common.page.PageList;
import com.zoo.activity.dao.model.Authority;
import com.zoo.activity.dao.model.Organizer;
import com.zoo.activity.dao.nutz.pojo.CommonAccount;
import com.zoo.activity.service.MailService;
import com.zoo.activity.service.OrganizerService;
import com.zoo.activity.util.Common;
import com.zoo.activity.util.MD5Util;
import com.zoo.common.util.RedisUtil;
import com.zoo.common.util.StringUtil;
import com.zoo.finance.dao.model.ZooOperatorLogin;
import com.zoo.finance.dao.model.ZooOperatorLoginExample;
import com.zoo.finance.service.ZooOperatorLoginService;
import com.zoo.orgnizer.dao.model.OrganizerContract;
import com.zoo.orgnizer.dao.model.OrganizerContractExample;
import com.zoo.orgnizer.service.OrganizerContractService;
import com.zoo.sms.util.VerifyCodeUtils;
import com.zoo.upms.dao.model.*;
import com.zoo.upms.service.AuthorityService;
import com.zoo.upms.service.UpmsAccountRoleRelService;
import com.zoo.upms.service.UpmsModuleService;
import com.zoo.web.bean.WebResult;
import com.zoo.wechat.common.Config;
import com.zoo.zoomerchantadminweb.author.realm.ShiroPrincipal;
import com.zoo.zoomerchantadminweb.context.WebUpmsContext;
import com.zoo.zoomerchantadminweb.web.util.AuthorityUtil;
import io.swagger.annotations.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.Serializable;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.*;


@Api(value = "api/account", tags = {"主办方账户接口"}, description = "账户管理接口描述")
@RestController
@RequestMapping("api/account")
public class AccountController {

    private Logger logger = LoggerFactory.getLogger(AccountController.class);

    @Autowired
    AccountService accountService;

    @Autowired
    OrganizerService organizerService;

    @Autowired
    OrganizerContractService organizerContractService;

    @Autowired
    UpmsAccountRoleRelService upmsAccountRoleRelService;

    @Autowired
    MailService mailService;

    @Autowired
    UpmsModuleService upmsModuleService;

    @Autowired
    private AuthorityService authorityService;

    @Autowired
    private ZooOperatorLoginService zooOperatorLoginService;

    @Autowired
    private OrganizerPayConfigService organizerPayConfigService;

    public static String lockKey = "SMS_VERIFY_CODE_FOR_REGISTER";

    public static void main(String[] args) {
    }

    @RequestMapping(value = "setRedis", method = RequestMethod.GET)
    public WebResult setRedis(String key, String value, int seconds) {
        System.out.println(Thread.currentThread().getId() + " - key:" + key);
        RedisUtil.set(key, value, seconds);
        System.out.println("out-" + Thread.currentThread().getId() + " - key:" + key);
        return WebResult.getSuccessResult();
    }

    @RequestMapping(value = "getRedis", method = RequestMethod.GET)
    public WebResult getRedis(String key) {
        System.out.println(Thread.currentThread().getId() + " - key:" + key);
        String s = RedisUtil.get(key);
        WebResult result = WebResult.getSuccessResult();
        result.put("d", s);
        System.out.println("out-" + Thread.currentThread().getId() + " - key:" + key);
        return result;
    }

    @RequestMapping(value = "setRedisByte", method = RequestMethod.GET)
    public WebResult setRedisByte(String key, String value, int seconds) {
        System.out.println(Thread.currentThread().getId() + " - key:" + key);
        RedisUtil.set(key.getBytes(), value.getBytes(), seconds);
        System.out.println("out-" + Thread.currentThread().getId() + " - key:" + key);
        return WebResult.getSuccessResult();
    }

    @RequestMapping(value = "getRedisByte", method = RequestMethod.GET)
    public WebResult getRedisByte(String key) {
        System.out.println(Thread.currentThread().getId() + " - key:" + key);
        byte[] s = RedisUtil.get(key.getBytes());
        WebResult result = WebResult.getSuccessResult();
        result.put("d", new String(s));
        System.out.println("out-" + Thread.currentThread().getId() + " - key:" + key);
        return result;
    }


    @ApiOperation(value = "启动或禁用用户", notes = "启动或禁用用户", httpMethod = "POST")
    @ApiImplicitParam(name = "customerId", value = "用户ID", required = true, dataType = "string", paramType = "query")
    @RequestMapping(value = "stopOrStartAccount", method = RequestMethod.POST)
    public WebResult stopOrStartAccount(HttpServletRequest request,
                                        @RequestParam(required = true) Integer customerId) {
        AccountResult account = accountService.getAccount(AccountEnum.FetchAccountTypeEnum.FETCH_BY_CUSTOMER.getValue(), customerId + "");
        CommonAccount commonAccount = (CommonAccount) account.getData();
        if ("1".equals(commonAccount.getStatus().toString())) {
            return statusAccount(0, customerId);
        } else {
            return statusAccount(1, customerId);
        }
    }

    @ApiOperation(value = "启用用户", notes = "启用用户", httpMethod = "POST")
    @ApiImplicitParam(name = "customerId", value = "用户ID", required = true, dataType = "string", paramType = "query")
    @RequestMapping(value = "startAccount", method = RequestMethod.POST)
    public WebResult startAccount(HttpServletRequest request,
                                  @RequestParam(required = true) Integer customerId) {
        return statusAccount(1, customerId);
    }

    private WebResult statusAccount(Integer status, Integer customerId) {
        CommonAccount account = new CommonAccount();
        account.setCustomerId(customerId);
        account.setStatus(status);
        accountService.saveOrUpdate(AccountEnum.OperateTypeEnum.UPDATE.getValue(), account, null);
        WebResult<Object> result = WebResult.getSuccessResult();
        return result;
    }

    @ApiOperation(value = "获取主办方用户列表", notes = "获取主办方用户列表", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "size", value = "分页大小", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "userName", value = "用户名", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "phone", value = "手机号", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "name", value = "职位名称", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "isUsing", value = "是否启用", required = false, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "startTime", value = "下单开始时间", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "endTime", value = "下单结束时间", required = false, dataType = "string", paramType = "query")
    })
    @RequestMapping(value = "getCommonAccounts", method = RequestMethod.GET)
    public WebResult getCommonAccounts(HttpServletRequest request,
                                       @RequestParam(required = false, defaultValue = "1") Integer page,
                                       @RequestParam(required = false, defaultValue = "10") Integer size,
                                       @RequestParam(value = "userName", required = false) String userName,
                                       @RequestParam(value = "phone", required = false) String phone,
                                       @RequestParam(value = "name", required = false) String name,
                                       @RequestParam(value = "isUsing", required = false) Integer isUsing,
                                       @RequestParam(value = "startTime", required = false) String startTime,
                                       @RequestParam(value = "endTime", required = false) String endTime) {
        CommonAccount account = WebUpmsContext.getAccount(request);

        if (null == account) {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        } else {
            PageList list = accountService.getAccountListByOrganizerAndType(account.getOrganizerId(), 2, page, size, userName, AccountEnum.AccountTypeEnum.ISNOW_SERVER.getValue(), phone, name, isUsing, startTime, endTime);

            WebResult<Object> result = WebResult.getSuccessResult();
            result.putResult("list", list);
            return result;
        }
    }

    @ApiOperation(value = "获取主办方用户下拉框数据", notes = "获取主办方用户下拉框数据", httpMethod = "GET")
    @RequestMapping(value = "getAllAccountsAndRoleForSelect", method = RequestMethod.GET)
    public WebResult getCommonAccounts(HttpServletRequest request) {
        CommonAccount account = WebUpmsContext.getAccount(request);

        if (null == account) {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        } else {
            List<Map> list = accountService.getAllAccountListByOrganizerAndType(account.getOrganizerId(), 2, AccountEnum.AccountTypeEnum.ISNOW_SERVER.getValue());
            return WebResult.getSuccessResult("data", list);
        }
    }

    /**
     * 获取主办方用户详情
     * 1、获取主办方用户基本信息
     * 2、获取主办方用户模块信息，以逗号分隔返回
     * 3、获取主办方用户权限信息，需要返回对应模块下的对应权限
     * ---主办方用户isnow_permission值*时返回所有权限信息
     * ---用户有角色分配角色时返回角色对应权限信息
     *
     * @param request
     * @param customerId
     * @return
     */
    @ApiOperation(value = "获取主办方用户详情", notes = "获取主办方用户详情", httpMethod = "GET")
    @ApiImplicitParam(name = "customerId", value = "用户名ID", required = false, dataType = "int", paramType = "query")
    @RequestMapping(value = "getAccount", method = RequestMethod.GET)
    public WebResult getAccount(HttpServletRequest request,
                                Integer customerId) {
        AccountResult account = accountService.getAccount(AccountEnum.FetchAccountTypeEnum.FETCH_BY_CUSTOMER.getValue(), customerId + "");

        UpmsAccountRoleRelExample upmsAccountRoleRelExample = new UpmsAccountRoleRelExample();
        upmsAccountRoleRelExample.createCriteria().andCustomerIdEqualTo(customerId);
        UpmsAccountRoleRel upmsAccountRoleRel = upmsAccountRoleRelService.selectFirstByExample(upmsAccountRoleRelExample);
        List<Authority> permission = null;


        if (upmsAccountRoleRel == null) {
            //没有创建角色
            CommonAccount commonAccount = (CommonAccount) account.getData();
            String isnowPermission = commonAccount.getIsnowPermission();
            if (isnowPermission.equals("*")) {
                permission = authorityService.getPermissionList(null);
            } else {
                String[] values = isnowPermission.split(",");
                permission = authorityService.getAuthorityListByValues(values);
            }
        } else {
            //有分配角色
            permission = authorityService.getAuthorityListByRoleId(upmsAccountRoleRel.getRoleId());
        }

        //获取系统
        List<Authority> sys = authorityService.getAuthorityListByParentId(0L);

        Map<String, String> premissionlist = new HashMap<String, String>();
        String sysBuffer = new String();
        if (sys != null) {
            for (Authority au : sys) {
                sysBuffer += "," + au.getValue();
                premissionlist.put(au.getValue(), AuthorityUtil.parseChildMenuToString(au, permission));
            }
        }

        CommonAccount commonAccount = (CommonAccount) account.getData();
        //查询模块
        List<UpmsModule> upmsModules = null;
        Map<String, String> moduls = new HashMap<String, String>();
        if (upmsAccountRoleRel != null) {
            upmsModules = upmsModuleService.selectModuleListByRoleId(upmsAccountRoleRel.getRoleId());
        } else {
            upmsModules = upmsModuleService.selectByExample(new UpmsModuleExample());
        }
        if (upmsModules != null) {
            String camodule = commonAccount.getIsnowModulePermission();
            if (StringUtils.isNotEmpty(camodule)) {
                for (UpmsModule upm : upmsModules) {
                    String s = moduls.get(upm.getSysValue());
                    if (StringUtils.isEmpty(s) && camodule.contains(upm.getUpmsModuleId() + "")) {
                        s = upm.getUpmsModuleId() + "";
                    } else if (camodule.contains(upm.getUpmsModuleId() + "")) {
                        s += "," + upm.getUpmsModuleId();
                    }
                    if (s != null) {
                        moduls.put(upm.getSysValue(), s);
                    }
                }
            }
        }

        WebResult<Object> result = WebResult.getSuccessResult();
        result.putResult("account", account.getData());
        result.putResult("permision", premissionlist);
        result.putResult("moduls", moduls);
        result.putResult("sys", sysBuffer);
        if (upmsAccountRoleRel != null) {
            result.putResult("roleId", upmsAccountRoleRel.getRoleId());
        }
        return result;
    }

    @ApiOperation(value = "修改主办方用户", notes = "修改主办方用户，选择角色ID时必须上传角色名称和角色描述", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "customerId", value = "用户Id", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "name", value = "姓名", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "phone", value = "手机号", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "moduleIds", value = "模块,格式(ID1,ID2)", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "permission", value = "权限,格式（格式：系统值1,系统值2,权限值1,权限值2，使用逗号分隔）", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "roleId", value = "角色ID", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "roleName", value = "角色名称", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "roleRemark", value = "角色描述", required = false, dataType = "int", paramType = "query"),
    })
    @RequestMapping(value = "editCommonAccount", method = RequestMethod.POST)
    public WebResult editCommonAccount(HttpServletRequest request,
                                       int customerId,
                                       String name,
                                       String phone,
                                       String password,
                                       String repassword,
                                       String permission,
                                       String moduleIds,
                                       Long roleId,
                                       String roleName,
                                       String roleRemark) {
        CommonAccount account = WebUpmsContext.getAccount(request);

        if (customerId == 0) {
            return WebResult.getErrorResult(WebResult.Code.REQUEST_ERROR);
        }
        if (StringUtils.isEmpty(name)) {
            return WebResult.getErrorResult(WebResult.Code.USER_NAME_MUST_ERROR);
        }
        if (StringUtils.isEmpty(phone)) {
            return WebResult.getErrorResult(WebResult.Code.PHONE_IS_EMPTY);
        }
        if (StringUtils.isNotEmpty(password) && StringUtils.isNotEmpty(repassword)) {
            if (!password.equals(repassword)) {
                return WebResult.getErrorResult(WebResult.Code.RE_PASSWORD_ERROR);
            }
        } /*else {
            return WebResult.getErrorResult(WebResult.Code.LOGIN_PASSWORD_NOT_NULL_ERROR);
        }*/
        AccountResult account1 = accountService.getAccount(3, phone + "");
        CommonAccount user = null;
        if (account1 != null) {
            user = (CommonAccount) account1.getData();
            //手机号已存在
            if (user != null && user.getCustomerId() != customerId) {
                return WebResult.getErrorResult(WebResult.Code.PHONE_HAS_REGISTER);
            }
        }

        if (user == null) {
            account1 = accountService.getAccount(7, customerId + "");
            user = (CommonAccount) account1.getData();
        }
        if (StringUtils.isEmpty(password) || StringUtils.isEmpty(repassword)) {
            user.setIsnowPassword(null);
        } else {
            user.setIsnowPassword(password);
        }
        user.setPhone(phone);
        user.setRealname(name);
        user.setIsnowPermission(permission);

        accountService.editCommonAccount(user, moduleIds, permission, roleId, roleName, roleRemark);
        return WebResult.getSuccessResult();
    }

    @ApiOperation(value = "添加主办方用户", notes = "添加主办方用户，选择角色ID时必须上传角色名称和角色描述", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "name", value = "姓名", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "phone", value = "手机号", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "moduleIds", value = "模块,格式(ID1,ID2)", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "permission", value = "权限,格式（格式：系统权限值,权限值1,权限值2，使用逗号分隔）", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "roleId", value = "角色ID", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "roleName", value = "角色名称", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "roleRemark", value = "角色描述", required = false, dataType = "int", paramType = "query"),
    })
    @RequestMapping(value = "addCommonAccount", method = RequestMethod.POST)
    public WebResult addCommonAccount(HttpServletRequest request,
                                      String name,
                                      String phone,
                                      String password,
                                      String repassword,
                                      String moduleIds,
                                      String permission,
                                      Long roleId,
                                      String roleName,
                                      String roleRemark) {
        CommonAccount account = WebUpmsContext.getAccount(request);

        if (StringUtils.isEmpty(name)) {
            return WebResult.getErrorResult(WebResult.Code.USER_NAME_MUST_ERROR);
        }
        if (StringUtils.isEmpty(phone)) {
            return WebResult.getErrorResult(WebResult.Code.PHONE_IS_EMPTY);
        }
        if (StringUtils.isEmpty(password)) {
            return WebResult.getErrorResult(WebResult.Code.LOGIN_PASSWORD_NOT_NULL_ERROR);
        }
        if (!password.equals(repassword)) {
            return WebResult.getErrorResult(WebResult.Code.RE_PASSWORD_ERROR);
        }

        //手机号已存在
        if (accountService.getMerchantAccountByUserName(phone) != null) {
            return WebResult.getErrorResult(WebResult.Code.PHONE_HAS_REGISTER);
        }

        AccountResult account1 = accountService.getAccount(3, phone + "");

        CommonAccount user = null;
        if (account1 != null) {
            user = (CommonAccount) account1.getData();
        }
        if (user != null) {
            user.setSource(user.getSource() + "," + AccountEnum.AccountTypeEnum.ISNOW_SERVER.getValue());
            //isnow中的账户类型：1 主账户，2 子账户。3.微信绑定的主办方用户。4.微信绑定的教练用户 \\n0或NULL表示没有权限。
            user.setIsnowType(2);
            user.setRegisterType(user.getRegisterType() + "," + AccountEnum.RegisterTypeEnum.PHONE.getValue());
        } else {
            user = new CommonAccount();
            user.setUserId(Common.getUserId());
            user.setSource(AccountEnum.AccountTypeEnum.ISNOW_SERVER.getValue());
            user.setIsnowType(2);
            user.setRegisterType(AccountEnum.RegisterTypeEnum.PHONE.getValue());
        }
        user.setOrganizerId(account.getOrganizerId());
        user.setRealname(name);
        user.setNickname(name);
        user.setIsnowPermission(permission);
        user.setIsnowPassword(password);
        user.setPhone(phone);


        //状态：0 无效，1 有效
        user.setStatus(1);

        user.setCreateTime(new Date());

        accountService.addCommonAccount(user, moduleIds, permission, roleId, roleName, roleRemark);
        return WebResult.getSuccessResult();
    }

    @ApiOperation(value = "获取用户菜单和权限", notes = "获取用户菜单和权限", httpMethod = "GET")
    @ApiImplicitParam(name = "customerId", value = "用户ID", required = true, dataType = "int", paramType = "query")
    @RequestMapping(value = "getMenusByCustomerId", method = RequestMethod.GET)
    public WebResult getMenusByCustomerId(HttpServletRequest request, int customerId) {
        CommonAccount commonAccount = WebUpmsContext.getAccount(request);

        List<Authority> permission = organizerService.getPermissionListByUserId(customerId, Authority.SYS_MERCHANT_ADMIN_WEB);

        //获取系统
        List<Authority> sys = authorityService.getAuthorityListByParentId(0L);
        Authority sysAuthority = AuthorityUtil.getAuthority(sys, Authority.SYS_VALUE_MANAGER);

        List<Authority> topMenu = new ArrayList<Authority>();

        //获取顶级菜单，并移除其他权限
        AuthorityUtil.parasMenus(permission, topMenu, null, sysAuthority.getAuthorityId());

        //菜单分层
        for (Authority authority : topMenu) {
            authority.setChilds(parseChildMenu(authority, permission, commonAccount));
        }

        Comparator<Authority> parentId = Comparator.comparing(Authority::getParentId);
        Comparator<Authority> sort = Comparator.comparing(Authority::getSort);
        topMenu.sort(parentId.thenComparing(sort));

        WebResult result = WebResult.getSuccessResult();
        result.putResult("menus", topMenu);
        return result;
    }

    /**
     * @param
     * @return
     * @author LiuZhiMin
     * @date 2018/6/19 下午3:22
     * @Description: 获取当前登录用户的菜单
     */
    @ApiOperation(value = "主办方登录用户菜单", notes = "主办方登录用户菜单以及菜单页面中的权限，返回对象type(0：系统，1：菜单，2：按钮)", httpMethod = "POST")
    @RequestMapping(value = "getMenus", method = RequestMethod.POST)
    public WebResult getMenus(HttpServletRequest request) {
        return getMenusCommon(request, Authority.SYS_VALUE_MANAGER);
    }

    @ApiOperation(value = "获取教练系统菜单", notes = "获取教练系统菜单", httpMethod = "GET")
    @RequestMapping(value = "getCoachMenus", method = RequestMethod.GET)
    public WebResult getCoachMenus(HttpServletRequest request) {
        return getMenusCommon(request, Authority.SYS_VALUE_COACH);
    }

    @ApiOperation(value = "获取主办方信息", notes = "获取主办方信息", httpMethod = "GET")
    @RequestMapping(value = "/refreshAuth.json", method = RequestMethod.GET)
    public WebResult refreshAuth(HttpServletRequest request, HttpSession session) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        CommonAccount commonAccount = WebUpmsContext.getAccount(request);
        if (organizer == null || commonAccount == null) {
            // 获取登录信息失败，请重新登录
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }

        Map<String, String> data = new HashMap<String, String>();
        data.put("orgId", organizer.getOrganizerId().toString());
        data.put("orgName", organizer.getName());
        data.put("logo", organizer.getLogoUrl());
        data.put("user", 1 == commonAccount.getIsnowType() ? "总管理员" : commonAccount.getRealname());
        data.put("bussinessStart", organizer.getBusinessStart().toString());
        data.put("bussinessEnd", organizer.getBusinessEnd().toString());
        data.put("bussinessTime", organizer.getBusinessTime().toString());
        data.put("scheduleCoach", organizer.getScheduleCoach().toString());
        data.put("permission", commonAccount.getIsnowPermission());
        data.put("customerId", commonAccount.getCustomerId().toString());
        data.put("realname", 1 == commonAccount.getIsnowType() ? "总管理员" : commonAccount.getRealname());
        data.put("orgCategory", organizer.getCategory().toString()); // 主办方种类
        return WebResult.getSuccessResult("data", data);
    }

    /**
     * 获取菜单公共方法
     *
     * @param request
     * @param sysvalue
     * @return
     */
    private WebResult getMenusCommon(HttpServletRequest request, String sysvalue) {
        CommonAccount account = WebUpmsContext.getAccount(request);
        Organizer orgnization = WebUpmsContext.getOrgnization(request);

        //List<Authority> permission = organizerService.getPermissionListByUserId(account.getCustomerId(), Authority.SYS_MERCHANT_ADMIN_WEB);

        UpmsAccountRoleRelExample upmsAccountRoleRelExample = new UpmsAccountRoleRelExample();
        upmsAccountRoleRelExample.createCriteria().andCustomerIdEqualTo(account.getCustomerId());
        UpmsAccountRoleRel upmsAccountRoleRel = upmsAccountRoleRelService.selectFirstByExample(upmsAccountRoleRelExample);

        List<Authority> permission = null;
        if (upmsAccountRoleRel != null) {
            permission = authorityService.getAuthorityListByRoleId(upmsAccountRoleRel.getRoleId());
        } else {
            permission = organizerService.getPermissionListByUserId(account.getCustomerId(), Authority.SYS_MERCHANT_ADMIN_WEB);
        }

        OrganizerContractExample contractExample = new OrganizerContractExample();
        OrganizerContractExample.Criteria contractExampleCriteria = contractExample.createCriteria();
        contractExampleCriteria.andOrganizerIdEqualTo(orgnization.getOrganizerId());
        contractExampleCriteria.andStatusEqualTo(1);
        OrganizerContract organizerContract = organizerContractService.selectFirstByExample(contractExample);

        String weixinSubmchId = organizerPayConfigService.getWeixinSubmchId(orgnization.getOrganizerId());

        //判断转账类型
        if (StringUtils.isEmpty(weixinSubmchId)) {
            Iterator<Authority> iterator = permission.iterator();
            int removeCount = 0;
            while (iterator.hasNext()) {
                Authority next = iterator.next();
                if (organizerContract == null &&
                        ("sys_manager:finance:transfer".equals(next.getValue())
                                || "sys_manager:finance:cashdrawal".equals(next.getValue()))) {
                    iterator.remove();
                    removeCount++;
                    if (removeCount == 2) {
                        break;
                    }
                } else if ("sys_manager:finance:transfer".equals(next.getValue()) && organizerContract.getTransferType().intValue() == 2) {
                    iterator.remove();
                    break;
                } else if ("sys_manager:finance:cashdrawal".equals(next.getValue()) && organizerContract.getTransferType().intValue() == 1) {
                    iterator.remove();
                    break;
                }
            }
        } else {
            Iterator<Authority> iterator = permission.iterator();
            while (iterator.hasNext()) {
                Authority next = iterator.next();
                if ("sys_manager:finance:transfer".equals(next.getValue())) {
                    iterator.remove();
                } else if ("sys_manager:finance:cashdrawal".equals(next.getValue())) {
                    iterator.remove();
                }
            }
        }

        //获取系统
        List<Authority> sys = authorityService.getAuthorityListByParentId(0L);
        Authority sysAuthority = AuthorityUtil.getAuthority(sys, sysvalue);

        List<Authority> topMenu = new ArrayList<Authority>();

        //获取顶级菜单，并移除其他权限
        AuthorityUtil.parasMenus(permission, topMenu, null, sysAuthority.getAuthorityId());

        //菜单分层
        for (Authority authority : topMenu) {
            authority.setChilds(parseChildMenu(authority, permission, account));
        }

        List<Authority> exportMenu = authorityService.getAuthorityListByValues(new String[]{"sys_manager:export"});
        if (exportMenu != null && exportMenu.size() > 0) {
            topMenu.add(exportMenu.get(0));
        }

        List<Authority> childExportMenu = authorityService.getAuthorityListByValues(new String[]{"sys_manager:export:list"});
        if (exportMenu != null && exportMenu.size() > 0 && childExportMenu != null) {
            exportMenu.get(0).setChilds(childExportMenu);
        }

        Comparator<Authority> parentId = Comparator.comparing(Authority::getParentId);
        Comparator<Authority> sort = Comparator.comparing(Authority::getSort);
        topMenu.sort(parentId.thenComparing(sort));

        WebResult result = WebResult.getSuccessResult();
        result.putResult("menus", topMenu);
        result.putResult("url", WebUpmsContext.getOrgnization(request).getLogoUrl());
        result.putResult("name", WebUpmsContext.getOrgnization(request).getName());
        result.putResult("userName", account.getIsnowPermission().equals("*") ? "总管理员" : account.getRealname());
        return result;
    }

    /**
     * @param
     * @return
     * @author LiuZhiMin
     * @date 2018/6/19 下午3:35
     * @Description: 查询下降菜单
     */
    private List<Authority> parseChildMenu(Authority parent, List<Authority> permission, CommonAccount account) {
        List<Authority> menus = new ArrayList<Authority>();
        if (permission != null && permission.size() > 0) {
            Iterator<Authority> iterator = permission.iterator();
            while (iterator.hasNext()) {
                Authority next = iterator.next();
                if (next.getParentId().intValue() == parent.getAuthorityId().intValue()) {
                    if (next.getValue().equals("sys_manager:setting:userList") && !account.getIsnowPermission().equals("*")) {
                        //iterator.remove();
                        continue;
                    }
                    if (next.getValue().equals("sys_manager:member:memberCard")) {
                        Organizer organizer = organizerService.getAllInfoById(account.getOrganizerId());
                        if (organizerService.getOrganizerActivityTypeAllow(organizer.getType()).substring(4, 5).equals("0")) {
                            continue;
                        }
                    }
                    if (next.getValue().equals("sys_manager:setting:updatePassword")) {
                        //iterator.remove();
                        continue;
                    }
                    next.setDisable(false);
                    // 教练团课权限设置
                    if ("sys_manager:course:league:list".equalsIgnoreCase(next.getValue()) || "sys_manager:course:league:create".equalsIgnoreCase(next.getValue())
                            || "sys_manager:course:league:schedule".equalsIgnoreCase(next.getValue()) || "sys_manager:course:league:guide:record".equalsIgnoreCase(next.getValue())) {

                        if (account.getOrganizerId() != 220 && account.getOrganizerId() != 492 && account.getOrganizerId() != 493 && account.getOrganizerId() != 495 && account.getOrganizerId() != 496 && account.getOrganizerId() != 498
                                && account.getOrganizerId() != 47 && account.getOrganizerId() != 86 && account.getOrganizerId() != 261 && account.getOrganizerId() != 477 && account.getOrganizerId() != 400 && account.getOrganizerId() != 517
                                && account.getOrganizerId() != 520 && account.getOrganizerId() != 537 && account.getOrganizerId() != 529 && account.getOrganizerId() != 254 && account.getOrganizerId() != 534 && account.getOrganizerId() != 459
                                && account.getOrganizerId() != 71 && account.getOrganizerId() != 544 && account.getOrganizerId() != 319 && account.getOrganizerId() != 570 && account.getOrganizerId() != 571
                                && account.getOrganizerId() != 331 && account.getOrganizerId() != 460 && account.getOrganizerId() != 475 && account.getOrganizerId() != 443 && account.getOrganizerId() != 12
                                && account.getOrganizerId() != 108 && account.getOrganizerId() != 101 && account.getOrganizerId() != 237 && account.getOrganizerId() != 232 && account.getOrganizerId() != 239
                                && account.getOrganizerId() != 240 && account.getOrganizerId() != 556 && account.getOrganizerId() != 554 && account.getOrganizerId() != 561 && account.getOrganizerId() != 219
                                && account.getOrganizerId() != 521 && account.getOrganizerId() != 217 && account.getOrganizerId() != 575 && account.getOrganizerId() != 579 && account.getOrganizerId() != 580
                                && account.getOrganizerId() != 583
                        ) {
                            next.setDisable(true);
                        }
                    }

                    menus.add(next);
                    //iterator.remove();
                    next.setChilds(parseChildMenu(next, permission, account));
                }
            }
        }
        // paixu
        Comparator<Authority> parentId = Comparator.comparing(Authority::getParentId);
        Comparator<Authority> sort = Comparator.comparing(Authority::getSort);
        menus.sort(parentId.thenComparing(sort));
        return menus;
    }

    /**
     * @param username, password
     * @return com.zoo.web.bean.WebResult
     * @author LiuZhiMin
     * @date 2018/6/13 下午12:02
     * @Description: 主办方用户登录
     */
    @ApiOperation(value = "主办方用户登录", notes = "主办方用户登录，登录后客户端需要保持token信息", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "username", value = "用户名", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "password", value = "密码", required = true, dataType = "string", paramType = "query"),
    })
    @RequestMapping(value = "login", method = RequestMethod.POST)
    public WebResult login(String username, String password) {

        if (StringUtils.isEmpty(username)) {
            return WebResult.getErrorResult(WebResult.Code.USER_NAME_MUST_ERROR);
        }
        if (StringUtils.isEmpty(password)) {
            return WebResult.getErrorResult(WebResult.Code.LOGIN_PASSWORD_NOT_NULL_ERROR);
        }

        WebResult result = WebResult.getSuccessResult();
        Subject subject = SecurityUtils.getSubject();
        try {
            username = username.trim();
            subject.login(new UsernamePasswordToken(username, password));
        } catch (IncorrectCredentialsException e) {
            return WebResult.getErrorResult(WebResult.Code.LOGIN_AUTH_ERROR);
        } catch (AuthenticationException e) {
            return WebResult.getErrorResult(WebResult.Code.LOGIN_AUTH_ERROR);
        } catch (Exception e) {
            logger.error("登录失败", e);
            return WebResult.getErrorResult(WebResult.Code.LOGIN_AUTH_ERROR);
        }
        ShiroPrincipal shiroPrincipal = (ShiroPrincipal) subject.getPrincipal();
        Serializable sessionId = subject.getSession().getId();
        result.putResult("token", sessionId);
        result.putResult("domain", Config.instance().getDomain());

        //记录登录日志
        CommonAccount account = shiroPrincipal.getCommonAccount();
        Organizer organizer = organizerService.getOrganizerDetail(account.getOrganizerId());
        if (organizer.getType() == null || organizer.getType() <= 0) {
            return WebResult.getErrorResult(WebResult.Code.LOGIN_ORGANIZER_INVALID);
        }

        result.putResult("customerId", account.getCustomerId());
        result.putResult("orgCategory", organizer.getCategory().toString()); // 主办方种类
        result.putResult("orgName", organizer.getName()); // 主办方种类
        result.putResult("orgId", organizer.getOrganizerId().toString());
        result.putResult("allowMemberCard", organizerService.getOrganizerActivityTypeAllow(organizer.getType()).substring(4, 5).equals("1"));
        result.putResult("isnowType", account.getIsnowType());
        if (organizer.getIsSupportSweepCode() != null) {
            result.putResult("isSupportSweepCode", organizer.getIsSupportSweepCode().toString());
        } else {
            result.putResult("isSupportSweepCode", "1");
        }


        ZooOperatorLoginExample example = new ZooOperatorLoginExample();
        ZooOperatorLoginExample.Criteria criteria = example.createCriteria();
        criteria.andOperatorIdEqualTo(account.getCustomerId());
        criteria.andEndTimeIsNull();
        List<ZooOperatorLogin> logins = zooOperatorLoginService.selectByExample(example);
        if (logins == null || logins.size() == 0) {
            ZooOperatorLogin login = new ZooOperatorLogin();
            login.setStartTime(new Date());
            login.setOperatorId(account.getCustomerId());
            login.setOperatorName(StringUtils.isEmpty(account.getRealname()) ? account.getNickname() : account.getRealname());
            zooOperatorLoginService.insert(login);
        }

        //最后登录时间
        CommonAccount a = new CommonAccount();
        a.setCustomerId(account.getCustomerId());
        a.setLastLogin(new Date());
        accountService.saveOrUpdate(AccountEnum.OperateTypeEnum.UPDATE.getValue(), a, null);

        return result;
    }

    @ApiOperation(value = "主办方用户退出登录", notes = "主办方用户退出登录", httpMethod = "POST")
    @RequestMapping(value = "logout", method = RequestMethod.POST)
    public WebResult logout() {

        WebResult result = WebResult.getSuccessResult();
        try {
            Subject subject = SecurityUtils.getSubject();
            subject.logout();
        } catch (AuthenticationException e) {
            throw e;
        }
        return result;
    }

    @ApiOperation(value = "主办方用户注册", notes = "主办方用户注册", httpMethod = "POST")
    @RequestMapping(value = "register", method = RequestMethod.POST)
    public WebResult register(@RequestParam(value = "phone", required = false) String phone,
                              @RequestParam(value = "mail", required = false) String mail,
                              @RequestParam(value = "password", required = true) String password,
                              @RequestParam(value = "code", required = false) String code,
                              @RequestParam(value = "type", required = true) Integer type,
                              HttpSession session, HttpServletRequest request) {
        if (type == 0) {
            //手机
            if (accountService.getAccount(AccountEnum.FetchAccountTypeEnum.FETCH_BY_PHONE.getValue(), phone).getStatus().equals("1")) {
                return WebResult.getErrorResult(WebResult.Code.USER_NAME_HAVED_ERROR);
            }
            if (!VerifyCodeUtils.verificationSmsCode(phone, code)) {
                return WebResult.getErrorResult(WebResult.Code.CAPTCHA_WRONG);
            }

            String userId = Common.uniqueKey();
            CommonAccount account = new CommonAccount();
            account.setUserId(userId);
            account.setRegisterType("phone");
            account.setSource(AccountEnum.AccountTypeEnum.MAIN_SITE.getValue());
            account.setPhone(phone);
            account.setPassword(password);
            account.setNickname("小ZOO" + Common.randomMessageCode() + Common.randomMessageCode());
            account.setStatus(1);

            AccountResult result = accountService.saveOrUpdate(1, account, AccountConstant.ENCRYPTED);

            if ("1".equals(result.getStatus())) {
                return WebResult.getSuccessResult();
            } else {
                return WebResult.getErrorResult(WebResult.Code.UPDATE_FAILED);
            }

        } else if (type == 1) {

            if (accountService.getAccount(AccountEnum.FetchAccountTypeEnum.FETCH_BY_EMAIL.getValue(), mail).getStatus().equals("1")) {
                return WebResult.getErrorResult(WebResult.Code.USER_NAME_HAVED_ERROR);
            }
            String userId = Common.uniqueKey();
            CommonAccount account = new CommonAccount();
            account.setUserId(userId);
            account.setRegisterType("email");
            account.setEmailValid(1);
            account.setSource(AccountEnum.AccountTypeEnum.MAIN_SITE.getValue());
            account.setEmail(mail);
            account.setPassword(password);
            account.setNickname("小ZOO" + Common.randomMessageCode() + Common.randomMessageCode());
            account.setStatus(1);

            AccountResult result = accountService.saveOrUpdate(1, account, AccountConstant.ENCRYPTED);

            if (result.getStatus().equals("1")) {
//                try {
//                    sendVerifyEmail("/api/account/email/verify", userId, mail);
//                } catch (MailException e) {
//                    return WebResult.getErrorResult(WebResult.Code.ERROR);
//                }
                return WebResult.getSuccessResult();
            } else {
                return WebResult.getErrorResult(WebResult.Code.UPDATE_FAILED);
            }
        } else {
            return WebResult.getErrorResult(WebResult.Code.THIRD_TYPE_ERROR);
        }
    }

    @ApiOperation(value = "主办方邮箱认证", notes = "主办方邮箱认证", httpMethod = "GET")
    @RequestMapping(value = "/verifyCode", method = RequestMethod.GET)
    public WebResult getVerifyCode(@RequestParam(value = "mobile", required = true) String phone, HttpSession session) {
        String key = lockKey + session.getId();
        if (StringUtils.isBlank(phone)) {
            return WebResult.getErrorResult(WebResult.Code.PHONE_IS_EMPTY);
        }

        //获取验证码并发送短信
        WebResult result = VerifyCodeUtils.getRegisterVerifyCode(phone);

        if (result.getCode() == WebResult.Code.SUCCESS.code) {
            return WebResult.getSuccessResult();
        } else {
            return result;
        }

    }

    @ApiOperation(value = "主办方邮箱认证", notes = "主办方邮箱认证", httpMethod = "GET")
    @RequestMapping(value = "/email/verify", method = RequestMethod.GET)
    public ModelAndView updateUserEmailVerify(@RequestParam("userId") String userid,
                                              @RequestParam("code") String code) {
        userid = URLDecoder.decode(userid);
        AccountResult accountResult = accountService.getAccount(2, userid);
        CommonAccount account = (CommonAccount) accountResult.getData();
//        code = URLDecoder.decode(code);
        if (account == null) {
//            model.addAttribute("message", "用户不存在。。");
//            model.addAttribute("status", 0);
            return new ModelAndView("failed");
        }

        if (account.getEmailValid() == 1) {
//            model.addAttribute("message", "邮件已验证！");
//            model.addAttribute("status", 0);
            return new ModelAndView("failed");
        }
        String md5Code = MD5Util.getMd5(account.getEmail());

        if (code != null &&
                account.getEmailValid() == 0 &&
                md5Code.equalsIgnoreCase(code)) {

            CommonAccount accountData = new CommonAccount();
            accountData.setCustomerId(account.getCustomerId());
            accountData.setUserId(account.getUserId());
            accountData.setEmailValid(1);
            AccountResult saveAccountResult = accountService.saveOrUpdate(2, accountData, AccountConstant.UNENCRYPTED);
            if (saveAccountResult.getStatus().equals("1")) {
                return new ModelAndView("success");
            }
        }
        return new ModelAndView("failed");
    }

    private void sendVerifyEmail(String uri, String userId, String toAddress) {
        // 注册成功，发送验证邮件
        String md5Code = MD5Util.getMd5(toAddress);

        md5Code = URLEncoder.encode(md5Code);
        userId = URLEncoder.encode(userId);

        StringBuffer e = new StringBuffer("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">");
        e.append("<html xmlns=\"http://www.w3.org/1999/xhtml\">");
        e.append("<head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" /><title>Email验证</title>");
        e.append("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"/></head>");
        e.append("<body><div><table align=\"center\" border=\"0\"  cellpadding=\"0\" cellspacing=\"0\" width=\"720px\" style=\"position:relative; border-collapse: collapse;\">");
        e.append("<tr><td align=\"center\"><img width=\"720px\" border=\"0\" align=\"center\" style=\"display:block;width:720px;height:auto;\" src=\"http://resource.huaxuezoo.com/image/4a7d1289f4934da89c4b0c893fe2adb9\">");
        e.append("<div style=\"margin:0 auto;width:100%;min-width:720px;font-weight:bold;color:#5D5E59;position:absolute;top:150px;\"><p>欢迎你加入滑雪族</p><p>点击下方按钮即刻完成注册</p>");
        e.append("<a href=\"http://houtai.huaxuezoo.com" + uri + "?userId=" + userId + "&code=" + md5Code + "\" type=\"button\" name=\"验证\"  style=\"display:block;width:100px;height:30px;line-height:30px;border-radius:20px;background-color:#3A9BF9;color:#fff;border:0;text-decoration:none;\">验证</a>");
        e.append("<p style=\"font-size:13px;\">若按钮无法显示，请将以下链接复制并粘贴至新的浏览器窗口中打开</p><p>http://houtai.huaxuezoo.com" + uri + "?userId=" + userId + "&code=" + md5Code + "</p></div>");

//        e.append("<a href=\"http://zoo-merchant.test.huaxuezoo.com" + uri + "?userId=" + userId + "&code=" + md5Code + "\" type=\"button\" name=\"验证\"  style=\"display:block;width:100px;height:30px;line-height:30px;border-radius:20px;background-color:#3A9BF9;color:#fff;border:0;text-decoration:none;\">验证</a>");
//        e.append("<p style=\"font-size:13px;\">若按钮无法显示，请将以下链接复制并粘贴至新的浏览器窗口中打开</p><p>http://zoo-merchant.test.huaxuezoo.com" + uri + "?userId=" + userId + "&code=" + md5Code + "</p></div>");


        e.append("<div style=\"margin:0 auto;width:100%;min-width:720px;position:absolute;top:380px;\">");
        e.append("<img src=\"http://resource.huaxuezoo.com/image/929f3046c98b4009a4b765c5198ce1d8\" style=\"width:720px;height:200px;\"></div>");
        e.append("<div style=\"background-color:#F0EEEF;margin:0 auto;line-height:16px;width:100%;min-width:720px;position:absolute;top:580px;\">");
        e.append("<div style=\"width:720px\"><p>扫描二维码关注滑雪族公众号</p><p>随时随地获取优惠活动和滑雪资讯</p><img src=\"http://resource.huaxuezoo.com/image/64ce4951e99b4a90b9fa08a2c380caef\" style=\"display:block\">");
        e.append("<p>如有疑问请联系我们: 010-8454-9822</p><p style=\"font-size:11px;color:#5D5E59;padding:0 15px;\">本邮件是用户注册滑雪族系统自动发出，如果你并未注册滑雪族，可能是其他用户误输入了你的邮箱地址而使你收到了这封邮件，你可以忽略</p>");
        e.append("</div></div></td></tr></table></div></body></html>");
//        emailService.sendMail("滑雪族邮箱验证", e.toString(), toAddress, true);
        mailService.sendMail(toAddress, "滑雪族邮箱验证", e.toString());
    }

    @ApiOperation(value = "总管理员修改密码", notes = "总管理员修改密码", httpMethod = "POST")
    @RequestMapping(value = "/update/password.json", method = RequestMethod.POST)
    public WebResult updatePassword(@RequestParam("oldPassword") String oldPassword, @RequestParam("newPassword") String newPassword, HttpServletRequest request) {
        CommonAccount account = WebUpmsContext.getAccount(request);
        if (account == null) {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }

        account = (CommonAccount) accountService.getAccount(AccountEnum.FetchAccountTypeEnum.FETCH_BY_CUSTOMER.getValue(), account.getCustomerId().toString()).getData();

        if (!account.getIsnowPassword().equals(MD5Util.getMd5(oldPassword))) {
            return WebResult.getErrorMsgResult(WebResult.Code.ERROR, "原始密码错误");
        }

        account.setIsnowPassword(newPassword);

        accountService.saveOrUpdate(AccountEnum.OperateTypeEnum.UPDATE.getValue(), account, AccountConstant.ENCRYPTED);

        return WebResult.getSuccessResult();
    }

}
