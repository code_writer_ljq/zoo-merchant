package com.zoo.zoomerchantadminweb.account.controller;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zoo.account.constant.AccountEnum;
import com.zoo.account.service.AccountService;
import com.zoo.account.service.MemberService;
import com.zoo.account.support.AccountResult;
import com.zoo.activity.core.Status;
import com.zoo.activity.dao.model.*;
import com.zoo.activity.dao.nutz.pojo.CommonAccount;
import com.zoo.activity.service.*;
import com.zoo.activity.util.Excel.ImportExcelUtil;
import com.zoo.activity.util.Excel.model.ImportMemberModel;
import com.zoo.activity.util.ExcelUtil;
import com.zoo.common.page.PageList;
import com.zoo.icenow.dao.model.CommonAccountMoney;
import com.zoo.web.bean.WebResult;
import com.zoo.zoomerchantadminweb.context.WebUpmsContext;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * server 会员相关
 * Created by jmf on 2019/9/26.
 */
@Api(value = "api/server/member", tags = {"[会员系统]主办方会员相关接口"}, description = "[会员系统]主办方会员相关接口描述")
@RestController
@RequestMapping("api/server/member")
public class MemberController {

    private static Logger logger = Logger.getLogger(MemberController.class);

    @Autowired
    private CourseService courseService;

    @Autowired
    private MemberService memberService;

    @Autowired
    private NextIdService nextIdService;

    @Autowired
    private ActivityService activityService;

    @Autowired
    private EpisodeService episodeService;

    @Autowired
    private OrganizerService organizerService;

    @Autowired
    private AccountService accountService;

    @Autowired
    private ChargeService chargeService;


    @ApiOperation(value = "会员卡列表", notes = "会员卡列表", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页", required = false, dataType = "int", paramType = "query", defaultValue = "1"),
            @ApiImplicitParam(name = "size", value = "条", required = false, dataType = "int", paramType = "query", defaultValue = "10"),
    })
    @RequestMapping(value = "/member-card/list.json", method = RequestMethod.GET)
    public Object getMemberCardList(@RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
                                    @RequestParam(value = "size", required = false, defaultValue = "10") Integer size,
                                    HttpServletRequest request) {
        // 主办方信息
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null) {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
        PageList pageList = memberService.getMemberCardList(organizer.getOrganizerId(), page, size);
        return WebResult.getSuccessResult("data", pageList);
    }

    @ApiOperation(value = "会员卡详情", notes = "会员卡详情", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "activityId", value = "产品id", required = false, dataType = "int", paramType = "query", defaultValue = "0"),
    })
    @RequestMapping(value = "/member-card/detail.json", method = RequestMethod.GET)
    public Object getMemberCardDetail(@RequestParam(value = "activityId") Integer activityId,
                                      HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null) {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
        return WebResult.getSuccessResult("data", memberService.getMemberCardDetail(organizer.getOrganizerId(), activityId));
    }

    @ApiOperation(value = "添加会员卡", notes = "添加会员卡", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "name", value = "会员卡名称", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "activityId", value = "产品id", required = false, dataType = "int", paramType = "query", defaultValue = "0"),
            @ApiImplicitParam(name = "chargeList", value = "种类(json: [{rangeType: day, rangeValue: 60, price: 60}, {rangeType: day, rangeValue: 30, price: 30}])", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "note", value = "权益说明", required = false, dataType = "string", paramType = "query"),
    })
    @RequestMapping(value = "/member-card/add.json", method = RequestMethod.POST)
    public Object getMemberCardList(@RequestParam(value = "name", required = true) String name,
                                    @RequestParam(value = "chargeList", required = true) String chargeList,
                                    @RequestParam(value = "note", required = false) String note,
                                    @RequestParam(value = "activityId", required = false, defaultValue = "0") Integer activityId,
                                    HttpServletRequest request) throws Exception {
        // 主办方信息
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null) {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
        OrganizerWithScenic organizerWithScenic = organizerService.getAllInfoById(organizer.getOrganizerId());

        List<Charge> charges = new ArrayList<>();
        List<Charge> updateCharges = new ArrayList<>();
        JSONArray jsonArray = JSON.parseArray(chargeList);
        for (Object obj : jsonArray) {
            JSONObject json = (JSONObject) obj;
            Charge charge = new Charge();
            Integer chargeId = json.getInteger("chargeId");
            charge.setChargeId(chargeId);
            charge.setValidRange(json.getDouble("rangeValue"));
            charge.setPrice(json.getBigDecimal("price"));
            charge.setChargeName(name + "-" + charge.getValidRange().intValue() + "天");
            charge.setChargeLimit(0);
            charge.setIsPayback(0);
            charge.setBuyOrderLimit(1);
            charge.setBuyLimitType(1);
            charge.setBuyLimitCount(1);
            charges.add(charge);
            if (activityId > 0 && chargeId == null) {
                updateCharges.add(charge);
            }
        }

        ActivityWithBLOBs activity = new ActivityWithBLOBs();
        if (null == activityId || activityId == 0) {
            Date now = new Date();
            activity.setTitle(name);
            activity.setOperator(WebUpmsContext.getUserName(request));
            activity.setOrganizerId(WebUpmsContext.getOrgnization(request).getOrganizerId());
            activity.setCreateTime(now);
            activity.setUpdateTime(now);
            activity.setTypeId(4);
            activity.setVisible(1);
            activity.setStatus(1);
            activity.setRent(0);
            activity.setRentAvailable(0);
            activity.setIntroduction(note);
//            success = activityService.addActivity(activity, null);

            //todo 如果新增加的activity 需要创建新的episode
            Episode episode = new Episode();
            episode.setVerifyFlag(1);
            episode.setVerifyCode("");
            // 邀请码设置
            episode.setInviteCode(null);
            episode.setOperator(WebUpmsContext.getUserName(request));
            episode.setActivityId(activity.getActivityId());
            episode.setEpisodeId(null);
            episode.setName(null);
            episode.setRefundDeadline(null); // 不可退
            episode.setVoucherSend(0);
            episode.setPlayerLimit(0);
            episode.setNeedPlayerCount(0);

            //创建课程时，去掉了地图，地址
            episode.setProvinceId((null != organizerWithScenic.getProvince() && organizerWithScenic.getProvince() != 0 && organizerWithScenic.getProvince() != 1) ? organizerWithScenic.getProvince() : 2);
            episode.setCityId((null != organizerWithScenic.getCity() && organizerWithScenic.getCity() != 0 && organizerWithScenic.getCity() != 1) ? organizerWithScenic.getCity() : 2);
            episode.setDistrictId((null != organizerWithScenic.getDistrict() && organizerWithScenic.getDistrict() != 0 && organizerWithScenic.getDistrict() != 1) ? organizerWithScenic.getDistrict() : 4);
            episode.setLocation(organizer.getAddress());
            episode.setLatitude(organizer.getLatitude());
            episode.setLongitude(organizer.getLongitude());

//            episode.setNote(note);
            episode.setPlayerMeta("[{\"id\":\"real_name\",\"placeHolder\":\"请填写真实姓名\"," +
                    "\"title\":\"姓名\",\"type\":\"text\",\"values\":[]},{\"id\":\"phone\",\"placeHolder\":\"手机号\"," +
                    "\"title\":\"手机号\",\"type\":\"text\",\"values\":[]}]");

            episode.setStartTime(now);
            episode.setEndTime(new Date(Long.parseLong("32503651200000")));
            episode.setRegisterStart(episode.getStartTime());
            episode.setRegisterDeadline(episode.getEndTime());

            episode.setCharges(charges);

            synchronized (this) {
                activity.setActivityId(nextIdService.getNextId("activity"));
                episode.setActivityId(activity.getActivityId());
                episode.setStatus(0);
                episode.setEpisodeId(nextIdService.getNextId("episode"));
                courseService.addCourse(activity, episode, null, null);
            }
        } else {
            activity = activityService.getActivityById(activityId);
            activity.setOperator(WebUpmsContext.getUserName(request));
            activity.setUpdateTime(new Date());
            activity.setIntroduction(note);
            activity.setTitle(name);
            List<Episode> updateEpisodes = activityService.getEpisodeListByActId(activityId);
            Episode updateEpisode = updateEpisodes.get(0);
//            updateEpisode.setNote(note);
            updateEpisode.setCharges(updateCharges);

            activityService.updateActivity(activity);
            episodeService.updateEpisode(updateEpisode, null, null);
        }

        return WebResult.getSuccessResult("data", activity.getActivityId());
    }

    @ApiOperation(value = "关闭会员", notes = "关闭会员", httpMethod = "get")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "customerId", value = "会员id", required = true, dataType = "int", paramType = "query")
    })
    @RequestMapping(value = "/close.json", method = RequestMethod.GET)
    public Object memberClose(@RequestParam(value = "customerId") Integer customerId,
                              HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null) {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
        return memberService.memberClose(customerId, organizer.getOrganizerId());
    }


    @PostMapping("/importMemberExcel")
    public Object importMemberExcel(@RequestParam("file") MultipartFile file, HttpServletRequest request, HttpServletResponse response){
        AtomicInteger atomicInteger = new AtomicInteger(0);
        int count = 0;
        try {
            Organizer organizer = WebUpmsContext.getOrgnization(request);
            if (organizer == null) {
                return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
            }
            List<ImportMemberModel> importMemberModels = ImportExcelUtil.importExcel(ImportMemberModel.class, file.getInputStream());
            count = importMemberModels.size();
            Date date = new Date();

            for (ImportMemberModel model : importMemberModels) {
                AccountResult accountResult = accountService.getAccount(AccountEnum.FetchAccountTypeEnum.FETCH_BY_PHONE.getValue(), model.getPhone());
                if(Status.success.equals(accountResult.getStatus())) {
                    CommonAccount account = (CommonAccount) accountResult.getData();
                    if (account != null) {
                        Charge charge = chargeService.getChargeByActivityId(model.getActivityId());
                        ActivityWithBLOBs activityWithBLOBs = activityService.getActivityById(Integer.parseInt(model.getActivityId()));
                        if(activityWithBLOBs == null || !organizer.getOrganizerId().equals(activityWithBLOBs.getOrganizerId())) {
                            logger.info("会员操作：----> " + model.getPhone() + "-----> 产品不存在！");
                            continue;
                        }
                        if(null == activityWithBLOBs.getTypeId() || 4 != activityWithBLOBs.getTypeId()) {
                            logger.info("会员操作：----> " + model.getPhone() + "-----> 产品非会员卡！");
                            continue;
                        }

                        CommonAccountMoney commonAccountMoney = memberService.selectCommonAccountMoneyDetail(account.getCustomerId(), organizer.getOrganizerId());
                        if(commonAccountMoney == null) {
                            CommonAccountMoney cam = new CommonAccountMoney();

                            cam.setBalance(BigDecimal.ZERO);
                            cam.setLastUpdatetIme(date);
                            cam.setCreateTime(date);
                            cam.setCustomerId(account.getCustomerId());
                            cam.setOrganizerId(organizer.getOrganizerId());
                            cam.setMemberBalance(BigDecimal.ZERO);
                            memberService.insertCommonAccountMoney(cam);
                        }



                        MsMember member = memberService.getCustomerMemberByCustomerIdAndOrganizerId(account.getCustomerId(), organizer.getOrganizerId());
                        if(member != null) {
                            int i = memberService.deleteMember(member.getMemberId());
//                            atomicInteger.getAndIncrement();
//                            continue;
                        }

                        MsMember msMember = new MsMember();
                        msMember.setOrganizerId(organizer.getOrganizerId());

                        msMember.setType(1);
                        msMember.setActivityId(activityWithBLOBs.getActivityId());
                        msMember.setEpisodeId(charge.getEpisodeId());
                        msMember.setChargeId(charge.getChargeId());
                        msMember.setCreateTime(new Date());
                        msMember.setUpdateTime(new Date());
                        msMember.setExpireTime(com.zoo.activity.util.DateUtil.addDay(new Date(), charge.getValidRange().intValue()));
                        msMember.setTitle(activityWithBLOBs.getTitle());
                        msMember.setCustomerId(account.getCustomerId());
//                        System.out.println("MsMember: " + JSON.toJSONString(msMember));
                        int i = memberService.insertMsmember(msMember);
                        if(i > 0) {
                            atomicInteger.getAndIncrement();
                        }

                    } else {
                        logger.info("会员操作：----> " + model.getPhone() + "-----> 用户不存在！");
                    }
                } else {
                    logger.info("会员操作：----> " + model.getPhone() + "-----> 用户不存在！");
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return WebResult.getSuccessResult("data", "插入或更新成功" + atomicInteger.get() + "条数据！插入或更新失败" + (count - atomicInteger.get()) + "条数据！");
    }

    @GetMapping("/downloadMemberTemplate")
    public void downloadMemberTemplate(HttpServletResponse response, HttpServletRequest request) {
        try {
            //获取要下载的模板名称
            String fileName = "模板.xlsx";
            String filePath = getClass().getResource("/" + fileName).getPath();
            System.out.println(filePath);


            response.setContentType("application/vnd.ms-excel;charset=UTF-8");
            response.setCharacterEncoding("UTF-8");
            response.addHeader("Cache-Control", "no-cache");

            final String userAgent = request.getHeader("USER-AGENT");
            if(StringUtils.contains(userAgent, "MSIE")){//IE浏览器
                fileName = URLEncoder.encode(fileName,"UTF-8");
            }else if(StringUtils.contains(userAgent, "Mozilla")){//google,火狐浏览器
                fileName = new String(fileName.getBytes(), "ISO8859-1");
            }else{
                fileName = URLEncoder.encode(fileName,"UTF-8");//其他浏览器
            }

            response.addHeader("Content-Disposition", "attachment;filename=" + fileName);
            ServletOutputStream outputStream = response.getOutputStream();
            FileInputStream in = new FileInputStream(filePath);
            byte[] b = new byte[1024];
            int length = 0;
            while ((length = in.read(b))>0){
                outputStream.write(b,0,length);
            }
            response.flushBuffer();


//            downLoadFile(filePath, response, fileName);

        } catch (Exception e) {
            logger.error("getApplicationTemplate :", e);
        }

    }

    protected void downLoadFile(String filePath, HttpServletResponse response, String fileTitle) throws Exception {
        File f = null;
        BufferedInputStream in = null;
        BufferedOutputStream out = null;
        if (StringUtils.isBlank(filePath)) {
            injectResponse(response, "请检查有无信息或者稍后在尝试");
        } else {
            String fileName = new String(fileTitle.getBytes("UTF-8"), "iso-8859-1");
            try {
                f = new File(filePath);
                response.setContentType("application/octet-stream;charset=utf-8");
                response.setCharacterEncoding("UTF-8");
                response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
                response.setHeader("Content-Length", String.valueOf(f.length()));
                in = new BufferedInputStream(new FileInputStream(f));
                out = new BufferedOutputStream(response.getOutputStream());
                byte[] data = new byte[1024];
                int len;
                while (-1 != (len = in.read(data, 0, data.length))) {
                    out.write(data, 0, len);
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (in != null) {
                    in.close();
                }
                if (out != null) {
                    out.close();
                }
            }
        }
    }

    protected void injectResponse(HttpServletResponse response, String info) throws Exception {
        response.setContentType("text/html;charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        response.setHeader("Cache-Control", "no-cache");
        PrintWriter pw = response.getWriter();
        pw.write(info);
        pw.flush();
        pw.close();
    }
}
