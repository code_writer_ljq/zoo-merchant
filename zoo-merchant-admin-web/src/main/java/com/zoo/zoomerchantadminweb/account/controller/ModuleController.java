package com.zoo.zoomerchantadminweb.account.controller;

import com.zoo.activity.dao.model.Authority;
import com.zoo.activity.dao.nutz.pojo.CommonAccount;
import com.zoo.upms.dao.model.AuthorityValue;
import com.zoo.upms.dao.model.UpmsModule;
import com.zoo.upms.service.AuthorityService;
import com.zoo.upms.service.UpmsModuleService;
import com.zoo.web.bean.WebResult;
import com.zoo.zoomerchantadminweb.context.WebUpmsContext;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.collections.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

@Api(value = "api/module", tags = {"系统模块接口"}, description = "系统模块接口")
@RestController
@RequestMapping("api/module")
public class ModuleController {


    @Autowired
    private UpmsModuleService upmsModuleService;

    @Autowired
    AuthorityService authorityService;

    @ApiOperation(value = "获取模块列表", notes = "获取模块列表", httpMethod = "GET")
    @RequestMapping(value = "getModule", method = RequestMethod.GET)
    public WebResult getModule(HttpServletRequest request){
        CommonAccount account = WebUpmsContext.getAccount(request);
        List<UpmsModule> upmsModules = upmsModuleService.selectModuleList(account.getOrganizerId());

        Map<String,List<UpmsModule>> sysModuleMap = new HashMap<String,List<UpmsModule>>();
        if(upmsModules!=null){
            for (UpmsModule upm : upmsModules){
                List<UpmsModule> ll = sysModuleMap.get(upm.getSysValue());
                if(ll == null){
                    ll = new ArrayList<UpmsModule>();
                    sysModuleMap.put(upm.getSysValue(),ll);
                }
                ll.add(upm);
                List<AuthorityValue> authorityValueList = upm.getAuthorityValueList();
                for(AuthorityValue a : authorityValueList){
                    if(a.getValue().equals("sys_manager:setting:userList")){
                        authorityValueList.remove(a);
                        break;
                    }
                }
            }
        }

        List<Authority> authorityListByValues = authorityService.getAuthorityListByValues(sysModuleMap.keySet().toArray(new String[sysModuleMap.keySet().size()]));

        WebResult<Object> result = WebResult.getSuccessResult();
        result.putResult("list",sysModuleMap);
        result.putResult("sys",authorityListByValues);
        return result;
    }

}
