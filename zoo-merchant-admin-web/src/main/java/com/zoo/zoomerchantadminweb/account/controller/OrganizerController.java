package com.zoo.zoomerchantadminweb.account.controller;

import com.zoo.activity.dao.model.OrganizerInventory;
import com.zoo.activity.dao.model.OrganizerWithTime4Redis;
import com.zoo.activity.service.*;
import com.zoo.activity.util.DateTypeUtil;
import com.zoo.activity.dao.model.Organizer;
import com.zoo.activity.dao.model.OrganizerWithScenic;
import com.zoo.activity.util.DesUtils;
import com.zoo.activity.vo.OrganizerModel;
import com.zoo.util.data.BeanUtilsExtends;
import com.zoo.web.bean.WebResult;
import com.zoo.zoomerchantadminweb.context.WebUpmsContext;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

@Api(value = "api/server/organizer", tags = {"主办方信息接口"}, description = "主办方信息接口描述")
@RestController
@RequestMapping("api/server/organizer")
public class OrganizerController {


    @Autowired
    OrganizerService organizerService;

    @Autowired
    private ActivityService activityService;

    @Autowired
    private RegionService regionService;

    @Autowired
    private DateTypeService dateTypeService;

    @Autowired
    private RedisService redisService;

    @Autowired
    private OrganizerInventoryService organizerInventoryService;

    /**
     * 查看customer所在的机构信息
     *
     * @throws Exception
     */
    @ApiOperation(value = "主办方信息", notes = "查看当前用户所在的主办方信息", httpMethod = "POST")
    @RequestMapping(value = "/info.json", method = RequestMethod.POST)
    public Object userProfile(HttpServletRequest request) throws Exception {
        Organizer sessionOrganizer = WebUpmsContext.getOrgnization(request);
        OrganizerWithScenic realOrganizer = organizerService.getAllInfoById(sessionOrganizer.getOrganizerId());

//        List<Map> otaList = new ArrayList<>();
//        String[] otaIds = StringUtils.isNotBlank(realOrganizer.getOtaIds()) ? realOrganizer.getOtaIds().split(",") : new String[]{};
//        for (OtaConstant.OtaIdEnum e : OtaConstant.OtaIdEnum.values()) {
//            Map<String, Object> map = new HashMap<>();
//            map.put("otaId", e.getOtaId());
//            map.put("otaName", e.getName());
//            Integer checked = 0;
//            for (String s : otaIds) {
//                if (Integer.parseInt(s) == e.getOtaId()) {
//                    checked = 1;
//                    break;
//                }
//            }
//            map.put("checked", checked);
//            otaList.add(map);
//        }
        Map resultMap = new HashMap();
//        resultMap.put("otaList", otaList);
        resultMap.put("organizer", realOrganizer);
        resultMap.put("province", regionService.getRegionListByParentId(1));
        if (null != realOrganizer.getProvince()) {
            resultMap.put("city", regionService.getRegionListByParentId(realOrganizer.getProvince()));
        }
        if (null != realOrganizer.getCity()) {
            resultMap.put("district", regionService.getRegionListByParentId(realOrganizer.getCity()));
        }
        return WebResult.getSuccessResult("data", resultMap);
    }


    @ApiOperation(value = "主办方信息更新", notes = "主办方信息更新", httpMethod = "POST")
    @RequestMapping(value = "/update.json", method = RequestMethod.POST)
    public Object doUpdateProfile(@Valid OrganizerModel organizerModel, HttpServletRequest request) throws Exception {

        Organizer organizer = WebUpmsContext.getOrgnization(request);

        /**
         * 用户前台传入的信息
         */
        OrganizerWithScenic orgByUser = new OrganizerWithScenic();
        BeanUtilsExtends.copyProperties(orgByUser, organizerModel);
        orgByUser.setOrganizerId(organizer.getOrganizerId());

        //bug fix
        /*orgByUser.setWxFee(organizer.getWxFee());
        orgByUser.setType(organizer.getType());
        orgByUser.setShowRegionFilter(organizer.getShowRegionFilter());
        orgByUser.setCategory(organizer.getCategory());
        orgByUser.setUseArticle(organizer.getUseArticle());
        orgByUser.setShowTop(organizer.getShowTop());*/

        //这些字段的值都不从sesson中获取，可能会出现server端和manage端同时修改主办方信息，导致manage端修改信息被重置
        orgByUser.setWxFee(null);
        orgByUser.setType(null);
        orgByUser.setShowRegionFilter(null);
        orgByUser.setCategory(null);
        orgByUser.setUseArticle(null);
        orgByUser.setShowTop(null);

        orgByUser.setDistributor(organizer.getDistributor());
        orgByUser.setVisitEntry(null);
        orgByUser.setVersion(null);
        orgByUser.setInsurance(null);
        orgByUser.setCoachEncourage(null);
        orgByUser.setScheduleCoach(null);
        orgByUser.setCoachApprovalLeave(null);
        orgByUser.setBusinessTime(null);
        // orgByUser.setMaxDelay(null);
        orgByUser.setDealer(null);
        orgByUser.setWithdrawalsType(null);
        orgByUser.setWithdrawalsPeriod(null);
        orgByUser.setWithdrawalsPeriodType(null);
        orgByUser.setPattern(null);

        if (StringUtils.isBlank(orgByUser.getHoliday()) || orgByUser.getHoliday().equals("[]")) {
            orgByUser.setHoliday(dateTypeService.getJsonArray());
        }
        organizerService.updateOrganizerHoliday(orgByUser.getOrganizerId(), orgByUser.getHoliday());

        if (organizerService.updateOrganizerAllInfo(orgByUser)) {
            /*修改俱乐部信息的时候,更新openSearch的索引*/
//            List<OpenSearch> openSearches = activityService.getOpenSearchByOption(null, orgByUser.getOrganizerId(), null);
//            List<Map> searchMap = JSON.parseArray(JSON.toJSONString(openSearches), Map.class);
//            OpenSearchUtil.updateDoc(OpenSearchUtil.tableName, OpenSearchUtil.index, searchMap);
            OrganizerWithTime4Redis organizerWithTime4Redis = new OrganizerWithTime4Redis(orgByUser.getOrganizerId(), orgByUser.getDaytimeStart(), orgByUser.getDaytimeEnd(),
                    orgByUser.getNightStart(), orgByUser.getNightEnd(), orgByUser.getDaytimeStartMin(), orgByUser.getDaytimeEndMin(),
                    orgByUser.getNightStartMin(), orgByUser.getNightEndMin(), orgByUser.getTimeInterval(), orgByUser.getMorningTimeStart(), orgByUser.getAfternoonTimeStart(), orgByUser.getMorningTimeStartMin(), orgByUser.getAfternoonTimeStartMin(),
                    orgByUser.getMorningTimeEnd(), orgByUser.getAfternoonTimeEnd(), orgByUser.getMorningTimeEndMin(), orgByUser.getAfternoonTimeEndMin());
            redisService.valueSet("organizerWithTime4Redis" + organizer.getOrganizerId(), organizerWithTime4Redis);
            return WebResult.getSuccessResult();
        } else {
            return WebResult.getErrorResult(WebResult.Code.UPDATE_FAILED);
        }
    }

    @ApiOperation(value = "获取主办方酒店ID", notes = "查看当前用户所在的主办方信息", httpMethod = "POST")
    @RequestMapping(value = "/hotel.json", method = RequestMethod.POST)
    public WebResult getHotelId(HttpServletRequest request) throws Exception {
        return WebResult.getSuccessResult("data", URLEncoder.encode(DesUtils.hotelEncrypt(WebUpmsContext.getOrgnization(request).getOrganizerId().toString()), "UTF-8"));
//        return WebResult.getSuccessResult("data", URLEncoder.encode(DesUtils.hotelEncrypt("331"), "UTF-8"));
    }

    @ApiOperation(value = "获取主办方库存信息", notes = "获取主办方库存信息", httpMethod = "GET")
    @GetMapping(value = "/organizerInventory.json")
    public WebResult getOrganizerInventory(HttpServletRequest request){
        Organizer orgnization = WebUpmsContext.getOrgnization(request);
        OrganizerInventory organizerInventory = organizerInventoryService.oneByOrganizerIdAndType(orgnization.getOrganizerId(), 0);
        WebResult webResult = WebResult.getSuccessResult();
        if(null == organizerInventory){
            organizerInventory = new OrganizerInventory();
            organizerInventory.setCount(0);
            webResult.put("data", organizerInventory);
        }else {
            webResult.put("data", organizerInventory);
        }
        return webResult;
    }
}
