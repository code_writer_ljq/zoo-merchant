package com.zoo.zoomerchantadminweb.account.controller;

import com.zoo.account.service.CustomerOrgService;
import com.zoo.common.page.PageList;
import com.zoo.activity.dao.model.CustomerWithConcernInfo;
import com.zoo.activity.dao.model.Fingerprint;
import com.zoo.activity.dao.model.Organizer;
import com.zoo.web.bean.WebResult;
import com.zoo.zoomerchantadminweb.base.BaseController;
import com.zoo.zoomerchantadminweb.context.WebUpmsContext;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.Date;

/**
 * server 会员相关
 * Created by soul on 2018/7/8.
 */
@Api(value = "api/server/customer", tags = {"主办方会员相关接口"}, description = "主办方会员相关接口描述")
@RestController
@RequestMapping("api/server/customer")
public class CustomerController extends BaseController {

    @Resource(name = "customerOrgService")
    private CustomerOrgService customerOrgService;


    @ApiOperation(value = "查询会员信息列表", notes = "查询会员信息列表功能", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页", required = false, dataType = "int", paramType = "query", defaultValue = "1"),
            @ApiImplicitParam(name = "size", value = "条", required = false, dataType = "int", paramType = "query", defaultValue = "10"),
            @ApiImplicitParam(name = "phone", value = "会员手机号", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "name", value = "会员名", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "startAmount", value = "查询金额起始", required = false, dataType = "bigDecimal", paramType = "query"),
            @ApiImplicitParam(name = "endAmount", value = "查询金额结束", required = false, dataType = "bigDecimal", paramType = "query"),
            @ApiImplicitParam(name = "startCreateTime", value = "查询注册时间起始", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "endCreateTime", value = "查询注册时间结束", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "payedPriceDesc", value = "累计消费金额排序 0.降序 1.升序", required = false, dataType = "int", paramType = "query", defaultValue = "0"),
            @ApiImplicitParam(name = "storageAmountDesc", value = "储值金额排序 0.降序 1.升序", required = false, dataType = "int", paramType = "query", defaultValue = "0"),
            @ApiImplicitParam(name = "memberType", value = "会员类别 0.全部 1.储值会员 2.普通会员 3.储值且普通", required = false, dataType = "int", paramType = "query", defaultValue = "0"),
            @ApiImplicitParam(name = "memberActivity", value = "会员产品种类id", required = false, dataType = "int", paramType = "query")
    })
    @RequestMapping(value = "/list.json", method = RequestMethod.GET)
    public Object getCustomerList(@RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
                                  @RequestParam(value = "size", required = false, defaultValue = "10") Integer size,
                                  @RequestParam(value = "phone", required = false) String phone,
                                  @RequestParam(value = "name", required = false) String name,
                                  @RequestParam(value = "startAmount", required = false) BigDecimal startAmount,
                                  @RequestParam(value = "endAmount", required = false) BigDecimal endAmount,
                                  @RequestParam(value = "startCreateTime", required = false) String startCreateTime,
                                  @RequestParam(value = "endCreateTime", required = false) String endCreateTime,
                                  @RequestParam(value = "payedPriceDesc", required = false, defaultValue = "0") Integer payedPriceDesc,
                                  @RequestParam(value = "storageAmountDesc", required = false, defaultValue = "0") Integer storageAmountDesc,
                                  @RequestParam(value = "memberType", required = false, defaultValue = "0") Integer memberType,
                                  @RequestParam(value = "memberActivity", required = false) Integer memberActivity,
                                  HttpServletRequest request) {

        // 主办方信息
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null) {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
        PageList pageList = null;
        try {
            pageList = customerOrgService.getCustomerListByOrgId(organizer.getOrganizerId(), name, phone, startAmount, endAmount, startCreateTime, endCreateTime, payedPriceDesc, storageAmountDesc, memberType, memberActivity, page, size);
        } catch (Exception e) {
            e.printStackTrace();
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }

        return WebResult.getSuccessResult("data", pageList);
    }

    @ApiOperation(value = "获取会员详情信息", notes = "获取会员详情信息", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "customerId", value = "用户id", required = true, dataType = "int", paramType = "query")
    })
    @RequestMapping(value = "/getMemberInfo.json", method = RequestMethod.GET)
    public Object getMemberInfo(@RequestParam(value = "customerId", required = true) Integer customerId,
                                HttpServletRequest request) {
        // 主办方信息
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null) {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
        CustomerWithConcernInfo customerWithConcernInfo = null;
        try {
            customerWithConcernInfo = customerOrgService.getMemberInfo(customerId, organizer.getOrganizerId());
        } catch (Exception e) {
            e.printStackTrace();
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }

        return WebResult.getSuccessResult("data", customerWithConcernInfo);
    }

    @ApiOperation(value = "获取会员消费记录列表", notes = "获取会员消费记录列表", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页", required = false, dataType = "int", paramType = "query", defaultValue = "1"),
            @ApiImplicitParam(name = "size", value = "条", required = false, dataType = "int", paramType = "query", defaultValue = "10"),
            @ApiImplicitParam(name = "customerId", value = "用户id", required = true, dataType = "int", paramType = "query")
    })
    @RequestMapping(value = "/getMemberPayMoneyRecordList.json", method = RequestMethod.GET)
    public Object getMemberPayMoneyRecordList(@RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
                                              @RequestParam(value = "size", required = false, defaultValue = "10") Integer size,
                                              @RequestParam(value = "customerId", required = true) Integer customerId,
                                              @RequestParam(value = "isAll", required = false, defaultValue = "0") Integer isAll,
                                              HttpServletRequest request) {
        // 主办方信息
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null) {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
        PageList pageList = null;
        try {
            pageList = customerOrgService.getMemberPayMoneyRecordList(customerId, organizer.getOrganizerId(), isAll, page, size);
        } catch (Exception e) {
            e.printStackTrace();
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }
        return WebResult.getSuccessResult("data", pageList);
    }

    @ApiOperation(value = "查询会员信息列表", notes = "查询会员信息列表功能", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页", required = false, dataType = "int", paramType = "query", defaultValue = "1"),
            @ApiImplicitParam(name = "size", value = "条", required = false, dataType = "int", paramType = "query", defaultValue = "10"),
            @ApiImplicitParam(name = "phone", value = "会员手机号", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "name", value = "会员名", required = false, dataType = "string", paramType = "query")})
    @RequestMapping(value = "/fingerprint/list.json", method = RequestMethod.GET)
    public Object getFingerprintList(@RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
                                     @RequestParam(value = "size", required = false, defaultValue = "10") Integer size,
                                     @RequestParam(value = "phone", required = false) String phone,
                                     @RequestParam(value = "name", required = false) String name,
                                     HttpServletRequest request) {

        // 主办方信息
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null) {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
        PageList pageList = null;
        try {
            pageList = customerOrgService.getFingerprintListByOption(organizer.getOrganizerId(), name, phone, page, size, null);
        } catch (Exception e) {
            e.printStackTrace();
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }

        return WebResult.getSuccessResult("data", pageList);
    }

    @ApiOperation(value = "查询会员信息列表", notes = "查询会员信息列表功能", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "phone", value = "会员手机号", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "idCard", value = "会员手机号", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "title", value = "会员手机号", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "startTime", value = "会员手机号", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "endTime", value = "会员手机号", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "fingerprint", value = "会员手机号", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "name", value = "会员名", required = false, dataType = "string", paramType = "query")})
    @RequestMapping(value = "/fingerprint/add.json", method = RequestMethod.POST)
    public Object addFingerprint(@RequestParam(value = "phone", required = false) String phone,
                                 @RequestParam(value = "idCard", required = false) String idCard,
                                 @RequestParam(value = "title", required = false) String title,
                                 @RequestParam(value = "startTime", required = false) String startTime,
                                 @RequestParam(value = "endTime", required = false) String endTime,
                                 @RequestParam(value = "name", required = false) String name,
                                 @RequestParam(value = "fingerprint", required = false) String fingerprint,
                                 HttpServletRequest request) {

        // 主办方信息
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null) {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
        Fingerprint newFingerprint = new Fingerprint();
        newFingerprint.setName(name);
        newFingerprint.setPhone(phone);
        newFingerprint.setIdCard(idCard);
        newFingerprint.setTitle(title);
        newFingerprint.setStartTime(new Date(Long.parseLong(startTime)));
        newFingerprint.setEndTime(new Date(Long.parseLong(endTime)));
        newFingerprint.setValid(1);
        newFingerprint.setFingerprint(fingerprint);
        newFingerprint.setOrganizerId(organizer.getOrganizerId());
        customerOrgService.addFingerprint(newFingerprint);

        return WebResult.getSuccessResult();
    }

}
