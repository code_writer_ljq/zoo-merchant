package com.zoo.zoomerchantadminweb.test.controller;

import com.pay.unionpay.api.UnionPayApi;
import com.zoo.account.constant.AccountEnum;
import com.zoo.account.service.AccountService;
import com.zoo.account.support.AccountDispatcher;
import com.zoo.account.support.AccountResult;
import com.zoo.activity.dao.model.*;
import com.zoo.activity.dao.nutz.pojo.CommonAccount;
import com.zoo.activity.service.*;
import com.zoo.activity.util.*;
import com.zoo.activity.util.bean.OrderResultBean;
import com.zoo.common.util.DistributedLock;
import com.zoo.icenow.dao.model.Entry;
import com.zoo.icenow.service.EnterService;
import com.zoo.zoomerchantadminweb.context.WebUpmsContext;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.*;

/**
 * Created by lihao on 15/8/14.
 */
@Api(value = "api/test", tags = {"【测试接口】"}, description = "【测试接口】订单支付测试使用")
@RestController
@RequestMapping("api/test")
public class WxPayController{

    private OrdersService ordersService;
    private EpisodeService episodeService;
    private ChargeService chargeService;
    private ActivityService activityService;
    private OrganizerService organizerService;
    private OrderOperateService orderOperateService;

    @Autowired
    private AccountService accountService;

    @Autowired
    private EnterService enterService;

    @Autowired
    public void setOrganizerService(OrganizerService organizerService) {
        this.organizerService = organizerService;
    }

    @Autowired
    public void setActivityService(ActivityService activityService) {
        this.activityService = activityService;
    }

    @Autowired
    public void setChargeService(ChargeService chargeService) {
        this.chargeService = chargeService;
    }

    @Autowired
    public void setEpisodeService(EpisodeService episodeService) {
        this.episodeService = episodeService;
    }

    @Autowired
    public void setOrdersService(OrdersService ordersService) {
        this.ordersService = ordersService;
    }

    @Autowired
    public void setOrderOperateService(OrderOperateService orderOperateService) {
        this.orderOperateService = orderOperateService;
    }

    @Autowired
    private DepositService depositService;

    @Autowired
    private VerifyService verifyService;

    @Autowired
    private OrderSweepCodePayService orderSweepCodePayService;

    @Autowired
    private OrganizerPayConfigService organizerPayConfigService;

    private Logger logger = LoggerFactory.getLogger(WxPayController.class);


    @RequestMapping(value = "/order/wxrefund",method = RequestMethod.GET)
    public String refundUnionPayMoney(HttpServletRequest request,Integer orgId,String code,int refundFee,int totalFee){
        CommonAccount operatorAccount = WebUpmsContext.getAccount(request);
        if(!"18200000003".equals(operatorAccount.getPhone()) && !"17610259833".equals(operatorAccount.getPhone())){
            return "failed login user, please login by 18200000003";
        }
        ordersService.wexinRefundCommon(orgId,code,refundFee,totalFee);
        return code;
    }

    @RequestMapping(value = "/order/refund",method = RequestMethod.GET)
    public String refundUnionPayMoney(HttpServletRequest request,String code,Double m,String store){
        CommonAccount operatorAccount = WebUpmsContext.getAccount(request);
        if(!"18200000003".equals(operatorAccount.getPhone()) && !"13552213327".equals(operatorAccount.getPhone()) && !"17610259833".equals(operatorAccount.getPhone())){
            return "failed login user, please login by 18200000003";
        }
        Orders orders = ordersService.getOrdersByCode(code);
        //String unipayOpShopId = organizerPayConfigService.getUnipayOpShopId(orders.getSellerId());

        OrderSweepCodePay orderSweepCodePaySuccess = orderSweepCodePayService.findOrderSweepCodePaySuccess(code);

        String payCode = null;

        if(orderSweepCodePaySuccess != null){
            payCode = orderSweepCodePaySuccess.getPayCode();
        }else{
            payCode = code;
        }

        String refundCode = payCode + "REF";
        int refundCent = 0;
        if(m == null){
            refundCent = orders.getTotalPrice().multiply(BigDecimal.valueOf(100)).intValue();
        }else{
            refundCent = BigDecimal.valueOf(m).multiply(BigDecimal.valueOf(100)).intValue();
        }

        if(orders == null){
            UnionPayApi.refund(payCode,refundCode,refundCent,refundCent,store);
        }else{
            OrganizerPayConfig organizerPayConfigByOrgId = organizerPayConfigService.getOrganizerPayConfigByOrgId(orders.getSellerId());

            if(org.apache.commons.lang.StringUtils.isEmpty(organizerPayConfigByOrgId.getUnipayOpenapiMchId())){
                UnionPayApi.refund(payCode,refundCode,refundCent,refundCent,organizerPayConfigByOrgId.getUnipayOpShopId());
            }else{
                UnionPayApi.openApiRefund(payCode,refundCode,refundCent,refundCent,organizerPayConfigByOrgId.getUnipayOpenapiMchId(),organizerPayConfigByOrgId.getUnipayOpenapiKey());
            }
        }




        return refundCode;
    }

    /**
     * 支付回调
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/order/notify.json",method = RequestMethod.GET)
    public String execute(HttpServletRequest request,String orderCode) throws Exception {

        CommonAccount operatorAccount = WebUpmsContext.getAccount(request);
        if(!"18200000003".equals(operatorAccount.getPhone()) && !"17610259833".equals(operatorAccount.getPhone())){
            return "failed please login by 18200000003";
        }

        Orders order = ordersService.getOrderDetailByCode(orderCode);

        String lockKey = "notify" + order.getCode() + "lock";

        logger.info("notifyLock :" + lockKey);

        String requestId = UUID.randomUUID().toString();

        DistributedLock lock = DistributedLock.createLock(requestId);
        lock.lock();

        logger.info("lock success !");

        //将判断订单状态是否为已支付放在加锁之后，也是为了避免微信支付成功后多次回调产生的脏数据。
        if (order.getStatus() != 0) {
            logger.error("WechatPay callback error: Order status is not '0'.");
            //bug ==> 支付回调时  订单状态如果是已经支付过了就应该告诉微信已经支付成功了，不用在回调发请求了。
            //return failed;
            return "success";
        }

        int mainOrderIndex = 0;

        List<Charge> chargeList = new ArrayList<>();
        List<Orders> ordersList = new ArrayList<>();
        List<Orders> childOrders = ordersService.getChildOrders(order.getOrderId());
        List<Entry> entryList = new ArrayList<>();
        List<EquipmentRelationships> equipmentRelationshipsList = new ArrayList<>();
        List<RentDetail> rentDetailList = new ArrayList<>();
        Integer entryCount = 0;
        Integer entryOrderId = 0;
        Integer entryOrgId = 0;
        if (null == childOrders || childOrders.size() == 0) {
            Charge charge = chargeService.getChargeById(order.getChargeId());
            chargeList.add(charge);
            ordersList.add(order);
            entryOrderId = order.getOrderId();
            entryOrgId = order.getSellerId();
            if (charge.getChargeMode() == -1) {
                entryCount = order.getChargeNum();
            } else if (charge.getChargeMode() == 3) {
                entryCount = order.getChargeNum() * charge.getFrequency();
            } else {
                entryCount = 1;
            }
        } else {
            for (Orders childOrder : childOrders) {
                if (childOrder.getBuyerId() < 0) {
                    Charge multiCharge = chargeService.getChargeById(childOrder.getChargeId());
                    if (multiCharge.getIncrement() == 0) {
                        entryOrderId = childOrder.getOrderId();
                        entryOrgId = childOrder.getSellerId();
                        if (multiCharge.getChargeMode() == -1) {
                            entryCount = childOrder.getChargeNum();
                        } else if (multiCharge.getChargeMode() == 3) {
                            entryCount = childOrder.getChargeNum() * multiCharge.getFrequency();
                        } else {
                            entryCount = 1;
                        }
                    }
                }
                if (childOrder.getBuyerId() > 0) {
                    Charge multiCharge = chargeService.getChargeById(childOrder.getChargeId());
                    chargeList.add(multiCharge);
                    ordersList.add(childOrder);
                    if (multiCharge.getIncrement() == 0) {
                        mainOrderIndex = childOrders.indexOf(childOrder);
                        entryOrderId = childOrder.getOrderId();
                        entryOrgId = childOrder.getSellerId();
                        if (multiCharge.getChargeMode() == -1) {
                            entryCount = childOrder.getChargeNum();
                        } else if (multiCharge.getChargeMode() == 3) {
                            entryCount = childOrder.getChargeNum() * multiCharge.getFrequency();
                        } else {
                            entryCount = 1;
                        }
                    }
                    if (null != multiCharge.getEquipmentId() && depositService.checkExisitEquipmentRelationships(childOrder.getOrderId()) == 0) {
                        Integer relationId = NextIdUtil.getNextId("relation_id");
                        String name = episodeService.getEquipmentName(multiCharge.getEquipmentId());
                        EquipmentRelationships equipmentRelationships = new EquipmentRelationships();
                        equipmentRelationships.setRelationId(relationId);
                        equipmentRelationships.setEquipmentId(multiCharge.getEquipmentId());
                        equipmentRelationships.setName(name);
                        equipmentRelationships.setOrderId(childOrder.getOrderId());
                        equipmentRelationships.setSize(childOrder.getChargeNum());
                        equipmentRelationships.setStatus(0);
                        equipmentRelationshipsList.add(equipmentRelationships);
                        for (int i = 0; i < equipmentRelationships.getSize(); i++) {
                            RentDetail rentDetail = new RentDetail();
                            rentDetail.setRelationId(relationId);
                            rentDetail.setStatus(0);
                            rentDetail.setIndemnity(new BigDecimal(0));
                            rentDetail.setOrderId(childOrder.getOrderId());
                            rentDetail.setName(name);
                            rentDetail.setEquipmentId(multiCharge.getEquipmentId());
                            rentDetailList.add(rentDetail);
                        }
                    }
                }
            }
        }

        Episode episode = episodeService.getEpisodeDetailById(chargeList.get(0).getEpisodeId());
        Activity activity = activityService.getActivityById(episode.getActivityId());
        Organizer organizer = organizerService.getOrganizerDetail(activity.getOrganizerId());

        AccountResult account = accountService.getAccount(AccountEnum.FetchAccountTypeEnum.FETCH_BY_CUSTOMER.getValue(), String.valueOf(order.getBuyerId()));
        // 判断储值充值订单的支付回调，是否有其他地方的操作已经锁上了操作
        if (activity.getTypeId() == 9) {
            lock.release();
            logger.info("storage lock success.");
        }


        String resultCode = "SUCCESS";
        boolean indemnityAutoVerify = resultCode.toUpperCase().equals("SUCCESS") && (activity.getTypeId() == 10 || activity.getTypeId() == 9);
        logger.info("WechatPay callback: [result_code=" + resultCode + "]");
        try {
            if (resultCode.toUpperCase().equals("SUCCESS")) {

                if ((activity.getTypeId() == 3 || activity.getTypeId() == 6) && !(activity.getCardType() == 2 || activity.getCardType() == 3)) {
                    for (int i = 0; i < entryCount; i++) {
                        Integer entryId = NextIdUtil.getNextId("entry_id");
                        Entry entry = new Entry();
                        entry.setStatus(0);
                        entry.setEntryId(entryId);
                        entry.setOrganizerId(entryOrgId);
                        entry.setqCode(MD5Util.getMd5(entryId.toString() + entryOrgId.toString()));
                        entry.setOrderId(entryOrderId);
                        entryList.add(entry);
                    }
                    if (entryList.size() > 0) {
                        enterService.saveList(entryList);
                    }
                }
                if (equipmentRelationshipsList.size() > 0)
                    depositService.saveEquipmentRelationshipsList(equipmentRelationshipsList);
                if (rentDetailList.size() > 0)
                    depositService.saveRentDetailList(rentDetailList);

//                if (activity.getActivityId().equals(1888)) { // 测试服务器用
//                if (activity.getActivityId().equals(Integer.valueOf(Config.instance().getWechatHighsnowActivityId()))) {
//                    String phone = ordersService.getOrderPlayers(ordersService.getOrderDetailByCode(orderCode).getOrderId()).get(0).getPhone();
//                    List<BasicNameValuePair> params = new ArrayList<>();
//                    params.add(new BasicNameValuePair("phone", phone));
//                    params.add(new BasicNameValuePair("price", ordersService.getOrderDetailByCode(orderCode).getPrice().toString()));
//                    UrlEncodedFormEntity paramEntity = new UrlEncodedFormEntity(params, "UTF-8");
////                    this.asyncSendHighSnowPush("http://highsnow.huaxuezoo.com/notify.json", paramEntity);
//                    this.asyncSendHighSnowPush(Config.instance().getWechatighsnowUrl() + "/notify.json", paramEntity);
//                }

                String transactionId = System.currentTimeMillis() + "";

                if (null != order.getDeposit() && order.getDeposit().compareTo(BigDecimal.valueOf(0)) > 0) {
                    Deposit deposit = depositService.getDepositDetailByCode(order.getCode());
                    deposit.setStatus(1);
                    deposit.setTransactionId(transactionId);
                    Date now = new Date();
                    deposit.setPayTime(now);
                    deposit.setUpdateTime(now);
                    depositService.updateDeposit(deposit);
                }

                if (activity.getTypeId() == 10) {
                    OrganizerWithScenic organizerWithScenic = organizerService.getAllInfoById(organizer.getOrganizerId());
                    enterService.dealIndemnity(order.getOrderId(), organizerWithScenic);
                }

                int processNum = 0;
                for (int i = 0; i < ordersList.size(); i++) {
                    OrderResultBean orderResult = ordersService.payOrder("test", null,null,null,null,
                            operatorAccount,null,"test", order.getTotalPrice(),transactionId,ordersList.get(i),activity,episode,chargeList.get(0),(CommonAccount) account.getData(),resultCode,
                            1,null,null,null,null,null
                    ,null,null,null,null,null);
                    logger.info(orderResult.getMsg());
                    processNum += orderResult.getResultCount();

                    //删除 Quartz Task
                    /*List<Quartz> quartzList = quartzService.getQuartzByTypeAndTargetId(5, ordersList.get(i).getOrderId());
                    logger.info("WechatPay callback: Delete quartz task. QuartzList: " + JSON.toJSONString(quartzList));
                    for (Quartz quartz : quartzList) {
                        QuartzManager.removeJob(quartz.getQuartzName());
                        quartzService.deleteQuartzTask(quartz.getQuartzId());
                    }*/
                }

                if (processNum == ordersList.size()) {
                    order = ordersService.getOrderDetailByCode(orderCode);
                    if (null == order.getParentId()) {
                        order.setPayTime(new Date());
                        order.setStatus(1);
                        order.setPayTypeId(1);
                        ordersService.updateOrders(order);
                    }

                    Date appointmentTime = null;
                    List<Player> players = ordersService.getOrderPlayers(ordersList.get(mainOrderIndex).getOrderId());
                    if (players.size() > 0) {
                        appointmentTime = players.get(0).getAppointmentTime();
                    }

                    //给购买者发送微信消息
//                    if (!activity.getActivityId().equals(1888)) {
//                    if (!activity.getActivityId().equals(Integer.valueOf(Config.instance().getWechatHighsnowActivityId()))) {
                        //NoticeService.asyncWechatPush((CommonAccount) account.getData(), activity, episode, chargeList.get(mainOrderIndex), order, null, null, null, organizer, appointmentTime, null, NoticeService.PURCHASE_SUCCESS, 1);
//                    }
                    //获取该主办方绑定的微信账户
//                    List<AccountResult> accountList = AccountDispatcher.getAccountByOrgId(String.valueOf(organizer.getOrganizerId()));
//
//                    if (accountList != null && accountList.size() > 0) {
//                        for (AccountResult ar : accountList) {
//                            //判断该CommonAccount是否有订单管理权限
//                            String permission = ((CommonAccount) ar.getData()).getIsnowPermission();
//                            if (null != permission && (permission.equals("*") || permission.contains("/order/*") ||
//                                    (permission.contains("/order/search") && permission.contains("/order/refund")))) {
//                                //推送微信消息
//                                NoticeService.purchaseSuccess((CommonAccount) ar.getData(),
//                                        activity, episode, chargeList.get(mainOrderIndex), order, organizer, appointmentTime, 2);
//
//                            }
//                        }
//                    }

//                    String noticeEmail = organizer.getEmail();
//                    if (StringUtils.isNotBlank(episode.getNoticeEmail())) {
//                        noticeEmail = episode.getNoticeEmail();
//                    }
//                    List<String> notifyTargets = new ArrayList<>();
//                    notifyTargets.add(noticeEmail);
//                    zooNotify.notifyByEmail(notifyTargets, "滑雪族新订单", ("您的" + activity.getTitle() + "产品有新的支付订单，请前往 http://" + Config.instance().getDomain() + "/server/order-detail?orderId=" + order.getOrderId() + "  进行查看。"));

                }
            } else {

                for (int i = 0; i < ordersList.size(); i++) {
                    //删除 Quartz Task
                    //List<Quartz> quartzList = quartzService.getQuartzByTypeAndTargetId(5, order.getOrderId());
                    //logger.info("WechatPay callback: Delete quartz task. QuartzList: " + JSON.toJSONString(quartzList));
                    //for (Quartz quartz : quartzList) {
                        //QuartzManager.removeJob(quartz.getQuartzName());
                        //quartzService.deleteQuartzTask(quartz.getQuartzId());
                    //}
                    orderOperateService.occupyProcWithRollBack(episode.getEpisodeId(), chargeList.get(i).getChargeId(), order.getOrderId(), 0 - order.getChargeNum(), false);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "failed";
        } finally {
            lock.release();
            if (indemnityAutoVerify) {
                verifyService.verify(order.getOrderId(), 1, 6, organizer.getOrganizerId(), null, 0, null);
            }
        }
        StringBuffer buffer = new StringBuffer();
        for (Orders orders : ordersList) {
            buffer.append(orders.getOrderId().toString() + ",");
        }
        return buffer.toString().substring(0, buffer.length() - 1);
    }

}
