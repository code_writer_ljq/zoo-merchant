package com.zoo.zoomerchantadminweb.coach.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.zoo.activity.util.DateUtil;
import com.zoo.common.page.PageList;
import com.zoo.activity.dao.model.*;
import com.zoo.activity.dao.nutz.pojo.CommonAccount;
import com.zoo.activity.service.*;
import com.zoo.activity.vo.ScheduleCoach;
import com.zoo.web.bean.WebResult;
import com.zoo.zoomerchantadminweb.base.BaseController;
import com.zoo.zoomerchantadminweb.context.WebUpmsContext;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.net.URLDecoder;
import java.text.ParseException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by lsy on 17/4/28.
 */
@Api(value = "/api/server/coach/schedule", tags = "【教练系统】schedule接口", description = "排班顺序,出导,学员档案统计相关接口")
@RestController
@RequestMapping("/api/server/coach/schedule")
public class ScheduleCoachController extends BaseController {
    @Autowired
    private CoachService coachService;

    @Autowired
    private OrdersService ordersService;

    @Autowired
    private OrderCoachExtendService orderCoachExtendService;

    @Autowired
    private CoachGuideRelationshipsService coachGuideRelationshipsService;

    @Autowired
    private CourseService courseService;

    @Autowired
    private ScheduleCoachService scheduleCoachService;

    @Autowired
    private CoachStatisticsService coachStatisticsService;

    @Autowired
    private CoachGroupService coachGroupService;

    @Autowired
    private PlayerService playerService;

    @Autowired
    private ChargeService chargeService;

    @Autowired
    private CoachRestService coachRestService;

    @ApiOperation(value = "", notes = "教练系统", httpMethod = "GET")
    @RequestMapping(value = "", method = RequestMethod.GET)
    public Object schedule(HttpSession session, Model model) throws Exception {
        return "/coach/schedule/index";
    }

    /**
     * 出导接口（目前只有教练课时卡用此接口， 普通课程出导还是调用和小接口）
     * @param extendId  教练拓展订单表ID
     * @param session
     * @param request
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "出导按钮接口", notes = "点击出导按钮调用接口", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "extendId", value = "出导ID(extendId)", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "type", value = "出导类型（0课程、课时卡，团课2）", required = true, dataType = "int", paramType = "query"),
    })
    @RequestMapping(value = "/startGuide.json", method = RequestMethod.POST)
    public Object startSchedule(@RequestParam("extendId") Integer extendId,
                                @RequestParam(value = "type", required = false, defaultValue = "0") Integer type,
                                HttpSession session, HttpServletRequest request) throws Exception {
        //String userId = SystemSessionUtil.getHxzUserId(session);
        //userId = ((CommonAccount) (AccountDispatcher.getAccount(AccountEnum.FetchAccountTypeEnum.FETCH_BY_USER.getValue(), userId).getData())).getCustomerId().toString();
        CommonAccount account = WebUpmsContext.getAccount(request);
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (account == null) {
            return WebResult.getErrorResult(WebResult.Code.LOGIN_USER_NO_EXIST_ERROR);
        }
        if (organizer == null) {
            return WebResult.getErrorResult(WebResult.Code.COACH_GET_ORGANIZER_INFO_ERROR);
        }
        OrderCoachExtend orderCoachExtend = coachService.getCoachExtendByExtendId(extendId);
        OrderView orderView = null;
        List<Integer> coachIdList = new ArrayList<>();
        if (type == 0) {
            coachIdList.add(orderCoachExtend.getTransferCoach() != null ? orderCoachExtend.getTransferCoach() : orderCoachExtend.getOrderCoach());
            orderView = ordersService.getOrderDetail(orderCoachExtend.getOrderId());
        } else {
            orderCoachExtend.setActivityMode(type);
            CoachGuideRelationshipsExample example = new CoachGuideRelationshipsExample();
            example.createCriteria().andSourceIdEqualTo(orderCoachExtend.getExtendId()).andTypeEqualTo(0).andValidEqualTo(1);
            List<CoachGuideRelationships> coachGuideRelationships = coachGuideRelationshipsService.selectByExample(example);
            for (CoachGuideRelationships relationships : coachGuideRelationships) {
                coachIdList.add(relationships.getTargetId());
            }
        }

        Date now = new Date();

        Date start = coachService.getCourseStartTime(now, orderCoachExtend.getDuration(), organizer);
        Date end = coachService.getCourseEndTime(start, orderCoachExtend.getDuration(), organizer, 1);
        List<CoachRest> allCoachRest = coachRestService.getAllCoachRest(coachIdList, start, end);

        for (Integer coachId : coachIdList) {
            List<CoachRest> coachRests = allCoachRest.stream().filter(e -> e.getCoachId().equals(coachId)).collect(Collectors.toList());
            if (scheduleCoachService.getGuideCoach(organizer.getOrganizerId(), coachId) != null) {
                // 教练不能连续出导 (预约教练不能同时出导不同时段的预约订单)
                return WebResult.getErrorResult(WebResult.Code.COACH_NOT_CONTINUOUS_GUIDE);
            } else if (!coachService.checkCoachRestCanAppoint(coachRests, start, end)) {
                return WebResult.getErrorResult(WebResult.Code.COACH_APPOINTMENT_TIME_HAS_LEACE_0);
            }
        }

        if (courseService.updateJustGuideAndOrderVerify(orderView, orderCoachExtend, account, type)) {
            //控制教练卡的状态，DB插入guide记录
            if (type == 2) {
                orderCoachExtend.setNumber(ordersService.getCourseLeaguePlayerCount(orderCoachExtend.getChargeId()));
            }
            courseService.initCoachGuide(orderCoachExtend, coachIdList);
            for (Integer coachId : coachIdList) {
                if (type == 2) {
                    orderCoachExtend.setOrderCoach(coachId);
                }
                if (!DateUtil.isToday(orderCoachExtend.getAppointmentTime())) {
                    scheduleCoachService.updateScheduleCoachOrder(organizer, orderCoachExtend, 0);
                }
                orderCoachExtend.setAppointmentTime(now);
                //更新本次出导教练的redis中出导订单信息(一个教练在redis中只存一个出导订单，所以存的是最后一个点击出导按钮的订单)
                scheduleCoachService.assignScheduleCoach(organizer, orderCoachExtend);
            }
            return WebResult.getSuccessResult();
        } else {
            //出导失败
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }
    }

    /**
     * 结束出导
     * @param extendId
     * @param session
     * @param request
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "【出导日程页面】停止出导按钮接口", notes = "点击停止出导按钮调用接口", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "extendId", value = "出导ID(extendId)", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "type", value = "出导类型（0课程、课时卡，团课2）", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "coachId", value = "教练ID", required = false, dataType = "int", paramType = "query")
    })
    @RequestMapping(value = "/stopGuide.json", method = RequestMethod.POST)
    public Object stopSchedule(@RequestParam("extendId") Integer extendId,
                               @RequestParam(value = "type", required = false, defaultValue = "0") Integer type,
                               @RequestParam(value = "coachId", required = false) Integer coachId,
                               HttpSession session, HttpServletRequest request) throws Exception {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null) {
            return WebResult.getErrorResult(WebResult.Code.COACH_GET_ORGANIZER_INFO_ERROR);
        }

        OrderCoachExtend orderCoachExtend = coachService.getCoachExtendByExtendId(extendId);
        List<Integer> extendList = new ArrayList<>();
        extendList.add(extendId);
        // 更新出导记录中的结束出导时间， 并释放教练卡
        if (courseService.updateAlreadyGuideOrders(extendList, new Date()) > 0) {
            if (type == 2) {
                orderCoachExtend.setOrderCoach(coachId);
            }
            //reids中移除当前预约教练所在的出导队列，并设置该预约订单为已出导，重新放回排班队列
            scheduleCoachService.stopSchedule(organizer, orderCoachExtend);
            return WebResult.getSuccessResult();
        } else {
            //停止出导失败
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }
    }

    /**
     * 获取所有教练的出导信息
     * @param session
     * @param request
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "【出导日程】获取主办方所有出导教练的信息", notes = "获取主办方所有出导教练的信息", httpMethod = "GET")
    @RequestMapping(value = "/scheduleInfo.json", method = RequestMethod.GET)
    public Object scheduleInfo(HttpSession session, HttpServletRequest request) throws Exception {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null) {
            return WebResult.getErrorResult(WebResult.Code.COACH_GET_ORGANIZER_INFO_ERROR);
        }
        Map<String, List> info = scheduleCoachService.getScheduleInfo(organizer.getOrganizerId());
        return WebResult.getSuccessResult("data", info);
    }

    /**
     * 获取主办方所有教练请假信息
     * @param session
     * @param request
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "获取主办方所有教练请假信息", notes = "获取主办方教练请假信息", httpMethod = "GET")
    @RequestMapping(value = "/scheduleInfoWithRest.json", method = RequestMethod.GET)
    public Object scheduleInfoWithRest(HttpSession session, HttpServletRequest request) throws Exception {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null) {
            return WebResult.getErrorResult(WebResult.Code.COACH_GET_ORGANIZER_INFO_ERROR);
        }
        Map<String, List> info = scheduleCoachService.getScheduleInfoWithRest(organizer.getOrganizerId());
        return WebResult.getSuccessResult("data", info);
    }

    /**
     * 【排班页面】获取主办方所有教练-进行分组
     * @param session
     * @param request
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "【排班页面】获取分组教练信息", notes = "后台处理教练的分组情况", httpMethod = "GET")
    @RequestMapping(value = "/scheduleInfoWithGroup.json", method = RequestMethod.GET)
    @ResponseBody
    public Object scheduleInfoWithGroup(HttpSession session, HttpServletRequest request) throws Exception {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null) {
            return WebResult.getErrorResult(WebResult.Code.COACH_GET_ORGANIZER_INFO_ERROR);
        }

        List coaches = scheduleCoachService.getScheduleInfoWithoutGuide(organizer.getOrganizerId());
        List<CoachGroup> groups = coachGroupService.getAllCoachGroup(organizer.getOrganizerId());
        Map<Integer, CoachGroup> groupMap = new HashMap<>();
        CoachGroup noTeam = new CoachGroup();
        noTeam.setDeleted(0);
        noTeam.setGroupId(-1);
        noTeam.setName("未分组教练");
        List<ScheduleCoach> noTeamScheduleCoaches = new LinkedList<>();
        noTeam.setScheduleCoaches(noTeamScheduleCoaches);
        groupMap.put(-1, noTeam);
        for (CoachGroup group : groups) {
            List<ScheduleCoach> scheduleCoaches = new LinkedList<>();
            group.setScheduleCoaches(scheduleCoaches);
            groupMap.put(group.getGroupId(), group);
        }
        for (Object o : coaches) {
            ScheduleCoach scheduleCoach = (ScheduleCoach) o;
            if (scheduleCoach.getGroupId() != null && groupMap.get(scheduleCoach.getGroupId()) != null) {
                groupMap.get(scheduleCoach.getGroupId()).getScheduleCoaches().add(scheduleCoach);
            } else {
                groupMap.get(-1).getScheduleCoaches().add(scheduleCoach);
            }
        }
        return WebResult.getSuccessResult("data", groupMap);
    }

    /**
     * 教练周排名数据接口
     * @param date
     * @param size
     * @param sortType
     * @param sortProp
     * @param session
     * @param request
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "【首页】本周出导排行数据接口", notes = "教练周排名数据接口,取5条", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "date", value = "日期（格式为字符串：20180821）", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "size", value = "一次请求获取的数量(默认5)", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "sortType", value = "排序字段", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "sortProp", value = "排序方式(升序：asc，降序：desc)", required = false, dataType = "string", paramType = "query"),
    })
    @RequestMapping(value ="/coachWeekRankTop.json", method = RequestMethod.GET)
    public Object getCoachWeekRank(@RequestParam(value = "date", required = false) String date,
                                   @RequestParam(value = "size", required = false, defaultValue = "5") Integer size,
                                   @RequestParam(value = "sortType", required = false) String sortType,
                                   @RequestParam(value = "sortProp", required = false) String sortProp,
                                   HttpSession session, HttpServletRequest request) throws Exception {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null) {
            return WebResult.getErrorResult(WebResult.Code.COACH_GET_ORGANIZER_INFO_ERROR);
        }
        PageList resultData = coachStatisticsService.getCoachWeekRankTop(organizer.getOrganizerId(), date, sortProp, sortType, 1, size);
        return WebResult.getSuccessResult("data", resultData);
    }

    /**
     * 出导统计页面-教练排序统计数据接口
     * @param start
     * @param end
     * @param size
     * @param page
     * @param sortType
     * @param sortProp
     * @param activityId
     * @param coachId
     * @param session
     * @param request
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "【出导统计页面】教练排序统计数据接口", notes = "教练排序统计数据(一次请求拿5条)", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "start", value = "开始时间格式：20180723", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "end", value = "结束时间（格式为：20180821）", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "size", value = "一页几条数据（默认5条）", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "page", value = "第几页(默认第一页)", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "sortType", value = "排序字段", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "sortProp", value = "排序方式(升序：asc，降序：desc)", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "activityId", value = "活动ID(activityId)", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "episodeId", value = "活动ID(episodeId)", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "coachId", value = "教练ID(coachId)", required = false, dataType = "int", paramType = "query"),
    })
    @RequestMapping(value ="/coachRankWithTimeRange.json", method = RequestMethod.GET)
    public Object getCoachRankWithTimeRange(@RequestParam(value = "start", required = false) String start,
                                            @RequestParam(value = "end", required = false) String end,
                                            @RequestParam(value = "size", required = false, defaultValue = "5") Integer size,
                                            @RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
                                            @RequestParam(value = "sortType", required = false, defaultValue = "positive_percent") String sortType,
                                            @RequestParam(value = "sortProp", required = false, defaultValue = "desc") String sortProp,
                                            @RequestParam(value = "activityId", required = false) Integer activityId,
                                            @RequestParam(value = "episodeId", required = false) Integer episodeId,
                                            @RequestParam(value = "coachId", required = false) Integer coachId,
                                            HttpSession session, HttpServletRequest request) throws Exception {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null) {
            return WebResult.getErrorResult(WebResult.Code.COACH_GET_ORGANIZER_INFO_ERROR);
        }
        PageList resultData = coachStatisticsService.getCoachRankTimeRange(organizer.getOrganizerId(), start, end, activityId, episodeId, coachId, sortProp, sortType, page, size);
        return WebResult.getSuccessResult("data", resultData);
    }

    /**
     * 【首页】近30天出导订单变化趋势数据接口
     * @param start
     * @param end
     * @param dateGroupType
     * @param guideGroupType
     * @param activityId
     * @param coachId
     * @param session
     * @param request
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "【首页】近30天出导订单变化趋势数据接口", notes = "近30天出导订单变化趋势数据接口", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "start", value = "开始时间格式：20180723", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "end", value = "结束时间（格式为：20180821）", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "dateGroupType", value = "1", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "guideGroupType", value = "0", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "activityId", value = "活动ID(activityId)", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "episodeId", value = "活动ID(episodeId)", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "coachId", value = "教练ID(coachId)", required = false, dataType = "int", paramType = "query"),
    })
    @RequestMapping(value = "/recentData.json", method = RequestMethod.GET)
    public Object getRecentData(@RequestParam(value = "start", required = false) String start,
                                @RequestParam(value = "end", required = false) String end,
                                @RequestParam(value = "dateGroupType", required = true) Integer dateGroupType,
                                @RequestParam(value = "guideGroupType", required = true) Integer guideGroupType,
                                @RequestParam(value = "activityId", required = false) Integer activityId,
                                @RequestParam(value = "episodeId", required = false) Integer episodeId,
                                @RequestParam(value = "coachId", required = false) Integer coachId,
                                HttpSession session, HttpServletRequest request) throws Exception {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null) {
            return WebResult.getErrorResult(WebResult.Code.COACH_GET_ORGANIZER_INFO_ERROR);
        }
        List<CoachStatistics> resultData = coachStatisticsService.getCoachSumInfo(organizer.getOrganizerId(), start, end, dateGroupType, guideGroupType, activityId, episodeId, coachId);
        return WebResult.getSuccessResult("data", resultData);
    }

    /**
     * 排班保存接口
     * @param coachIds
     * @param session
     * @return
     */
    @ApiOperation(value = "【教练排班】排班接口", notes = "排班保存接口", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "coachIds", value = "教练ID 格式：[164,156,162,278,165,129,166,285,316,284,157]", required = true, dataType = "string", paramType = "query")
    })
    @RequestMapping(value = "/sort.json", method = RequestMethod.POST)
    public Object sort(@RequestParam("coachIds") String coachIds, HttpServletRequest request, HttpSession session) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null) {
            // 您没有权限进行该操作
            return WebResult.getErrorResult(WebResult.Code.COACH_GET_ORGANIZER_INFO_ERROR);
        }
        JSONArray idArray = JSON.parseArray(coachIds);
        Map<Integer, Integer> sortMap = new HashMap<>();
        for (int i = 0; i < idArray.size(); i++) {
            sortMap.put((Integer) idArray.get(i), i);
        }
        scheduleCoachService.sortSchedule(organizer, sortMap);
        // 排序成功
        return WebResult.getSuccessResult("data", "排班成功");
    }

    /**
     * [出导日程]-手动刷新按钮
     * @param session
     * @return
     */
    @ApiOperation(value = "【出导日程】手动刷新接口", notes = "手动刷新按钮调用的接口", httpMethod = "POST")
    @RequestMapping(value = "/refresh.json", method = RequestMethod.POST)
    public Object refresh(HttpServletRequest request, HttpSession session) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null) {
            return WebResult.getErrorResult(WebResult.Code.COACH_GET_ORGANIZER_INFO_ERROR);
        }
        scheduleCoachService.refreshCoachQueue(organizer, new Date(), null);
        Map<String, List> map = scheduleCoachService.getScheduleInfo(organizer.getOrganizerId());
        // 更新成功");
        return WebResult.getSuccessResult("data", map);
    }

    /**
     * 学员档案接口
     * @param phone
     * @param realName
     * @param size
     * @param page
     * @param session
     * @param request
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "【教学反馈】学员档案数据接口", notes = "学员档案数据接口", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "phone", value = "手机号", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "realName", value = "姓名", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "page", value = "第几页（默认为1）", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "size", value = "默认10", required = false, dataType = "int", paramType = "query"),
    })
    @RequestMapping(value ="/studentList.json", method = RequestMethod.GET)
    public Object getStudentList(@RequestParam(value = "phone", required = false) String phone,
                                 @RequestParam(value = "realName", required = false) String realName,
                                 @RequestParam(value = "size", required = false, defaultValue = "10") Integer size,
                                 @RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
                                 HttpSession session, HttpServletRequest request) throws Exception {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null || organizer.getOrganizerId() == null) {
            return WebResult.getErrorResult(WebResult.Code.COACH_GET_ORGANIZER_INFO_ERROR);
        }
        if (StringUtils.isNotBlank(realName))
            realName = URLDecoder.decode(realName);

        PageList result = playerService.getStudentsByOrg(organizer.getOrganizerId(), phone, realName, page, size);
        return WebResult.getSuccessResult("data", result);
    }

    /**
     * 学员档案 - 查看详情接口
     * @param phone
     * @param realName
     * @param session
     * @param request
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "【学员档案】查看学员详情接口", notes = "查看学员详情接口", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "phone", value = "手机号", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "realName", value = "姓名", required = true, dataType = "string", paramType = "query"),
    })
    @RequestMapping(value ="/student/detail.json", method = RequestMethod.GET)
    public Object getStudentOrderList(@RequestParam(value = "phone") String phone,
                                      @RequestParam(value = "realName") String realName,
                                      HttpSession session, HttpServletRequest request) throws Exception {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null || organizer.getOrganizerId() == null) {
            return WebResult.getErrorResult(WebResult.Code.COACH_GET_ORGANIZER_INFO_ERROR);
        }
        realName = URLDecoder.decode(realName);
        List<OrderCoachExtendList> orderCoachExtendListList = coachService.getStudentsOrders(organizer.getOrganizerId(), phone, realName);
        return WebResult.getSuccessResult("data", orderCoachExtendListList);
    }


    /**
     *  获取雪场统计信息(性别，年龄分布，单双板分布,支付方式)
     *
     * @param sendByType 由哪个页面发送的请求（0:非雪场页面，可进行全部雪场查询；1:雪场页面，举办方由session提供）（之后写成枚举？）
     * @param coachId 目前只在 教导方式 和 教学时间 上用作查询条件
     * @return
     * @author kongweichen
     * @date 2017.07.10
     */
    @ApiOperation(value = "【出导统计-出导详情】获取雪场统计信息接口", notes = "获取雪场统计信息(性别，年龄分布，单双板分布,支付方式)", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "organizerId", value = "主办方ID", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "sendByType", value = "由哪个页面发送的请求（0:非雪场页面，可进行全部雪场查询；1:雪场页面，举办方由session提供）", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "startTime", value = "筛选开始时间", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "endTime", value = "筛选结束时间", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "activityId", value = "活动ID(activityId)", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "episodeId", value = "活动ID(episodeId)", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "orderStatus", value = "订单状态", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "activityType", value = "活动类型", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "coachId", value = "教练ID", required = false, dataType = "int", paramType = "query"),
    })
    @RequestMapping(value = "/statisticsInfo.json", method = RequestMethod.GET)
    public Object getStatisticsInfo(HttpServletRequest request, HttpSession session,
                                    @RequestParam(value = "organizerId", required = false) Integer organizerId,
                                    @RequestParam(value = "sendByType", required = true) Integer sendByType,
                                    @RequestParam(value = "startTime", required = false) String startTime,
                                    @RequestParam(value = "endTime", required = false) String endTime,
                                    @RequestParam(value = "activityId", required = false) Integer activityId,
                                    @RequestParam(value = "episodeId", required = false) Integer episodeId,
                                    @RequestParam(value = "orderStatus", required = false) Integer orderStatus,
                                    @RequestParam(value = "activityType", required = false) Integer activityType,
                                    @RequestParam(value = "coachId", required = false) Integer coachId) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null && organizerId == null) {
            return WebResult.getErrorResult(WebResult.Code.COACH_GET_ORGANIZER_INFO_ERROR);
        }
        if (sendByType == 0) {//雪场相关页面中查
            organizerId = organizer.getOrganizerId();
        } else if (sendByType == 1) {//费雪场页面，可以查询不同雪场
            if (organizerId == -1) {
                organizerId = null;
            }
        }

        HashMap<String, Object> reDate = new HashMap<>();
        Map<String, Integer> genderInfoMap = coachStatisticsService.getGenderInfoByOthers(organizerId, activityId, episodeId, startTime, endTime, orderStatus, activityType, null);//性别
        Map<String, Integer> ageInfoMap = coachStatisticsService.getAgeInfoByOthers(organizerId, activityId, episodeId, startTime, endTime, orderStatus, activityType);//年龄
        Map<String, Integer> boardInfoMap = coachStatisticsService.getBoardInfoByOthers(organizerId, activityId, episodeId, startTime, endTime, orderStatus, activityType, null);//单双板
        Map<String, Integer> payTypeInfoMap = coachStatisticsService.getPayTypeInfoByOthers(organizerId, activityId, episodeId, startTime, endTime, orderStatus, activityType);//支付方式

        reDate.put("genderInfoMap", genderInfoMap);
        reDate.put("ageInfoMap", ageInfoMap);
        reDate.put("boardInfoMap", boardInfoMap);
        reDate.put("payTypeInfoMap", payTypeInfoMap);

        return WebResult.getSuccessResult("data", reDate);
    }

    /**
     *  获取雪场统计信息(教导方式,教学时间)
     *
     * @param sendByType 由哪个页面发送的请求（0:非雪场页面，可进行全部雪场查询；1:雪场页面，举办方由session提供）（之后写成枚举？）
     * @param coachId 目前只在 教导方式 和 教学时间 上用作查询条件
     * @return
     * @author kongweichen
     * @date 2017.07.14
     */
    @ApiOperation(value = "【出导统计-教导相关】获取雪场统计信息接口", notes = "获取雪场统计信息(教导方式,教学时间)", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "organizerId", value = "主办方ID", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "sendByType", value = "由哪个页面发送的请求（0:非雪场页面，可进行全部雪场查询；1:雪场页面，举办方由session提供）", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "startTime", value = "筛选开始时间", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "endTime", value = "筛选结束时间", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "activityId", value = "活动ID(activityId)", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "episodeId", value = "活动ID(episodeId)", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "orderStatus", value = "订单状态", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "activityType", value = "活动类型", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "coachId", value = "教练ID", required = false, dataType = "int", paramType = "query"),
    })
    @RequestMapping(value = "/teachStatisticsInfo.json", method = RequestMethod.GET)
    public Object getTeachStatisticsInfo(HttpServletRequest request, HttpSession session,
                                         @RequestParam(value = "organizerId", required = false) Integer organizerId,
                                         @RequestParam(value = "sendByType", required = true) Integer sendByType,
                                         @RequestParam(value = "startTime", required = false) String startTime,
                                         @RequestParam(value = "endTime", required = false) String endTime,
                                         @RequestParam(value = "activityId", required = false) Integer activityId,
                                         @RequestParam(value = "episodeId", required = false) Integer episodeId,
                                         @RequestParam(value = "orderStatus", required = false) Integer orderStatus,
                                         @RequestParam(value = "activityType", required = false) Integer activityType,
                                         @RequestParam(value = "coachId", required = false) Integer coachId) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null && organizerId == null) {
            return WebResult.getErrorResult(WebResult.Code.COACH_GET_ORGANIZER_INFO_ERROR);
        }
        if (sendByType == 0) {//雪场相关页面中查
            organizerId = organizer.getOrganizerId();
        } else if (sendByType == 1) {//费雪场页面，可以查询不同雪场
            if (organizerId == -1) {
                organizerId = null;
            }
        }

        HashMap<String, Object> reDate = new HashMap<>();
        List<Map<String, Object>> teachMethodsInfoList = coachStatisticsService.getTeachMethodsInfo(coachId, orderStatus, organizerId, activityId, episodeId, startTime, endTime);//教导方式
        List<Map<String, Object>> teachDurationInfoList = coachStatisticsService.getTeachDurationInfo(coachId, orderStatus, organizerId, activityId, episodeId, startTime, endTime);//教学时间

        reDate.put("teachMethodsInfoList", teachMethodsInfoList);
        reDate.put("teachDurationInfoList", teachDurationInfoList);

        return WebResult.getSuccessResult("data", reDate);
    }

    /**
     * 预约修改
     * @param session
     * @param request
     * @param responseContent
     * @param coachId
     * @param appointTime
     * @param orderId
     * @param extendId
     * @return
     */
    @ApiOperation(value = "出导记录列表页修改预约接口", notes = "出导记录列表修改预约接口", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "responseContent", value = "修改预约原因", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "coachId", value = "教练ID", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "appointTime", value = "预约时间（时间戳）", required = true, dataType = "long", paramType = "query"),
            @ApiImplicitParam(name = "orderId", value = "订单ID", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "extendId", value = "出导ID(extendId)", required = true, dataType = "int", paramType = "query"),
    })
    @RequestMapping(value = "/appointment/modify.json", method = RequestMethod.POST)
    public Object modifyCourseOrderInfo(HttpSession session, HttpServletRequest request,
                                        @RequestParam(value = "responseContent") String responseContent,
                                        @RequestParam(value = "coachId") Integer coachId,
                                        @RequestParam(value = "appointTime") long appointTime,
                                        @RequestParam(value = "orderId") Integer orderId,
                                        @RequestParam(value = "extendId") Integer extendId) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null || organizer.getOrganizerId() == null) {
            //无法从session中获取organizer信息
            return WebResult.getErrorResult(WebResult.Code.COACH_GET_ORGANIZER_INFO_ERROR);
        }
        Integer orgId = organizer.getOrganizerId();
        OrderView orderView = ordersService.getOrderDetail(orderId);
        if (!orgId.equals(orderView.getSellerId())) {
            //只能修改属于自己主办方的课程订单信息
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }
        Date time = new Date(appointTime);
        if (time.before(new Date())) {
            //预约时间不能小于当前时间
            return WebResult.getErrorResult(WebResult.Code.COACH_APPOINTMENT_TIME_BEFORE_NOW);
        }
//        List<CoachRest> coachRests = coachService.getCoachRest(coachId, time);
//        if (coachRests.size() > 0) {
//            //该教练在预约时间正处于请假状态,无法预约
//            return WebResult.getErrorResult(WebResult.Code.COACH_APPOINTMENT_TIME_HAS_LEACE);
//        }

        WebResult result = new WebResult();
        // service层包含了许多返回错误
        result = courseService.updateCourseOrder(orderId, extendId, time, coachId, responseContent, result);
        return result;
    }

    /**
     * 管理员--课时卡预约取消接口
     * @param session
     * @param orderId
     * @param extendId
     * @param responseContent
     * @return
     */
    @ApiOperation(value = "管理员--课时卡预约取消接口", notes = "管理员--课时卡预约取消接口", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "responseContent", value = "修改预约原因", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "orderId", value = "订单ID", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "extendId", value = "出导ID(extendId)", required = true, dataType = "int", paramType = "query"),
    })
    @RequestMapping(value = "/appointment/cancel.json", method = RequestMethod.POST)
    public Object cancelCourseOrderInfo(HttpSession session, HttpServletRequest request,
                                        @RequestParam(value = "orderId") Integer orderId,
                                        @RequestParam(value = "extendId") Integer extendId,
                                        @RequestParam(value = "responseContent", required = false) String responseContent) {
        CommonAccount customer = WebUpmsContext.getAccount(request);
        if (courseService.cancelCourseOrder(customer, orderId, extendId, responseContent)) {
            return WebResult.getSuccessResult();
        } else {
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }
    }

    @ApiOperation(value = "团课指派教练确认接口", notes = "团课指派教练确认接口", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "responseContent", value = "指派备注", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "coachId", value = "教练ID", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "cancelCoachIds", value = "取消教练ID（数组）", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "chargeId", value = "票券ID", required = true, dataType = "int", paramType = "query")
    })
    @RequestMapping(value = "/league/assign.json", method = RequestMethod.POST)
    public WebResult courseLeagueAssignCoach(HttpSession session, HttpServletRequest request,
                                                     @RequestParam(value = "responseContent", required = false) String responseContent,
                                                     @RequestParam(value = "coachId", required = false) Integer coachId,
                                                     @RequestParam(value = "cancelCoachIds", required = false, defaultValue = "[]") String cancelCoachs,
                                                     @RequestParam(value = "chargeId") Integer chargeId) throws ParseException {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        CommonAccount account = WebUpmsContext.getAccount(request);
        if (organizer == null || organizer.getOrganizerId() == null) {
            //无法从session中获取organizer信息
            return WebResult.getErrorResult(WebResult.Code.COACH_GET_ORGANIZER_INFO_ERROR);
        }
        Date now = new Date();
        List<Integer> cancelCoachIds = JSONArray.parseArray(cancelCoachs, Integer.class);
        OrderCoachExtend orderCoachExtend = orderCoachExtendService.getOrderCoachExtendByChargeId(chargeId);
        CourseLeagueCharge leagueCharge = chargeService.getCourseLeagueDetailByChargeId(chargeId);
        // 计算预约时间
        Date appointmentDateTime = DateUtil.convertSimpleStringToDateTime(leagueCharge.getAlias() + " " + DateUtil.convertDateToString(leagueCharge.getCourseStartTime(), "HH:mm:ss"));
        // 计算教学时长
        long endTime = leagueCharge.getCourseEndTime().getTime();
        long startTime = leagueCharge.getCourseStartTime().getTime();
        BigDecimal divisor = new BigDecimal(endTime - startTime);
        BigDecimal dividend = new BigDecimal(60 * 60 * 1000);
        Double duration = divisor.divide(dividend, 1, BigDecimal.ROUND_HALF_UP).doubleValue();
        if (orderCoachExtend == null) {
            orderCoachExtend = new OrderCoachExtend();
            orderCoachExtend.setChargeId(chargeId);
            orderCoachExtend.setAppointmentTime(appointmentDateTime);
            orderCoachExtend.setOrderId(-1);
            orderCoachExtend.setCreateTime(now);
            orderCoachExtend.setUpdateTime(now);
            orderCoachExtend.setDuration(duration);
            orderCoachExtend.setNumber(leagueCharge.getMinNumber());
            orderCoachExtend.setExtendStatus(0);
            orderCoachExtend.setCreateType(1);
            orderCoachExtend.setCourseType(2);
            courseService.addCoachExtendWithoutChargeInfo(orderCoachExtend);
        }
        if (StringUtils.isNotBlank(responseContent)) {
            orderCoachExtend.setResponseContent(responseContent);
            orderCoachExtend.setResponseTime(now);
        }

        if (coachId != null) {
            if (!cancelCoachIds.contains(coachId)) {
                List<Map> coachesMap = coachGuideRelationshipsService.getOrderOrCoachInfoByOption(orderCoachExtend.getExtendId(), 0);
                for (Map map : coachesMap) {
                    if (coachId.equals(map.get("coachId"))) {
                        return WebResult.getErrorResult(WebResult.Code.COACH_COURSE_LEAGUE_ASSIGN_COACH_REPEAT_ERROR);
                    }
                }
            }

            if (!coachService.checkCoachCanAppoint(coachId, appointmentDateTime, duration, organizer, null, 1)) {
                // 您排导的教练在该时段已有订单,请重新下单
                return WebResult.getErrorResult(WebResult.Code.COACH_APPOINTMENT_TIME_HAS_ORDER);
            }

//            List<CoachRest> coachRests = coachService.getCoachRest(coachId, appointmentDateTime);
            if (!coachService.checkCoachRestCanAppoint(coachId, appointmentDateTime, duration, organizer, null, 1)) {
                // 该教练在该时段处于请假状态,无法排导
                return WebResult.getErrorResult(WebResult.Code.COACH_APPOINTMENT_TIME_HAS_LEACE);
            }
        } else {
            if (cancelCoachIds.size() >= coachGuideRelationshipsService.getOrderOrCoachInfoByOption(orderCoachExtend.getExtendId(), 0).size()) {
                return WebResult.getErrorResult(WebResult.Code.COACH_COURSE_LEAGUE_NOT_HAS_COACH);
            }
        }

        Boolean flag = scheduleCoachService.courseLeagueAssignCoach(organizer, orderCoachExtend, coachId, cancelCoachIds, account);
        if (flag) {
            return WebResult.getSuccessResult();
        } else {
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }
    }
}
