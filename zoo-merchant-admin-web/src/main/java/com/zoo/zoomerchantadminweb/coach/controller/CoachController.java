package com.zoo.zoomerchantadminweb.coach.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.zoo.activity.dao.nutz.pojo.CommonAccount;
import com.zoo.common.page.PageList;
import com.zoo.activity.dao.model.*;
import com.zoo.activity.service.*;
import com.zoo.activity.service.TagService;
import com.zoo.web.bean.WebResult;
import com.zoo.zoomerchantadminweb.base.BaseController;
import com.zoo.zoomerchantadminweb.context.WebUpmsContext;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.models.auth.In;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.*;

/**
 * Created by ljq on 2018/8/25.
 */
@Api(value = "/api/server/coach", tags = "【教练系统】教练相关接口", description = "教练信息")
@RestController
@RequestMapping("/api/server/coach")
public class CoachController extends BaseController {
    @Autowired
    private CoachService coachService;

    @Autowired
    private TagService tagService;

    @Autowired
    private NextIdService nextIdService;

    @Autowired
    private CourseService courseService;

    @Autowired
    private ScheduleCoachService scheduleCoachService;

    @Autowired
    private OrganizerEnumService organizerEnumService;

    /**
     * 获取主办方教练下拉框
     *
     * @param session
     * @return
     */
    @ApiOperation(value = "获取主办方教练下拉框数据接口", notes = "获取主办方教练下拉框数据", httpMethod = "GET")
    @RequestMapping(value = "/option.json", method = RequestMethod.GET)
    public Object getCoachListOption(HttpServletRequest request, HttpSession session) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null || organizer.getOrganizerId() == null) {
            //无法从session中获取organizer信息
            return WebResult.getErrorResult(WebResult.Code.COACH_GET_ORGANIZER_INFO_ERROR);
        }

        List<Map> coachOptionMap = coachService.findCoachListOptionByOrganizerId4PC(organizer.getOrganizerId());
        return WebResult.getSuccessResult("data", coachOptionMap);
    }

    @ApiOperation(value = "教练信息管理获取所有课程信息（只有课程）", notes = "教练关联课程用", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "type", value = "获取主办方下课程或课时卡（0：课程， 1：课时卡）", required = true, dataType = "int", paramType = "query"),
    })
    @RequestMapping(value = "/relation/course/option.json", method = RequestMethod.GET)
    public Object getCoachRelationOption(@RequestParam(value = "type", required = true, defaultValue = "0") Integer type,
                                         HttpServletRequest request, HttpSession session) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null || organizer.getOrganizerId() == null) {
            //无法从session中获取organizer信息
            return WebResult.getErrorResult(WebResult.Code.COACH_GET_ORGANIZER_INFO_ERROR);
        }

        List<Map> optionMap = courseService.getCourseOrFrequencyOption(organizer.getOrganizerId(), type);
        return WebResult.getSuccessResult("data", optionMap);
    }

    /**
     * 创建自定义资质
     *
     * @param tagName
     * @return
     */
    @ApiOperation(value = "【教练信息】编辑教练按钮接口", notes = "获取教练详情及教练的tags", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "tagName", value = "自定义资质名称", required = true, dataType = "string", paramType = "query"),
    })
    @RequestMapping(value = "/add-tag.json", method = RequestMethod.POST)
    public Object addCoachTag(@RequestParam(value = "tagName") String tagName,
                              HttpServletRequest request, HttpSession session) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null || organizer.getOrganizerId() == null) {
            //无法从session中获取organizer信息
            return WebResult.getErrorResult(WebResult.Code.COACH_GET_ORGANIZER_INFO_ERROR);
        }

        Integer version = 2;
        if (organizer.getCategory() > 4) {
            version = organizer.getCategory();
        }

        Tag searchTag = tagService.getTagsByName(tagName, 1);
        if (searchTag != null) {
            // 自定义资质名称已经存在
            return WebResult.getErrorResult(WebResult.Code.COACH_CUSYOM_TAG_NAME_EXIST);
        } else {
            Tag tag = new Tag();
            Integer tagId = nextIdService.getNextId("tag");
            tag.setTagId(tagId);
            tag.setType(3);
            tag.setName(tagName);
            tag.setValid(1);
            tag.setVersion(version);
            if (tagService.addTag(tag) > 0) {
                return WebResult.getSuccessResult("data", tag);
            } else {
                return WebResult.getErrorResult(WebResult.Code.ADD_FAILED);
            }
        }
    }

    /**
     * 教练后台，添加教练信息
     *
     * @param realname
     * @param sex
     * @param phone
     * @param teachAge
     * @param skiAge
     * @param board
     * @param editStatus
     * @param headerUrl
     * @param description
     * @param tags
     * @param request
     * @param session
     * @return
     */
    @ApiOperation(value = "【教练管理】添加教练", notes = "添加教练", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "realname", value = "教练姓名", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "sex", value = "性别(女0，男1)", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "phone", value = "手机号", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "teachAge", value = "教龄", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "skiAge", value = "雪龄或冰龄", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "board", value = "技术类型(单板或冰球：1，双板或花滑：2  单板&双板或冰球&花滑：3)", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "editStatus", value = "是否允许自己编辑", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "headerUrl", value = "正面照", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "description", value = "描述信息(富文本内容)", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "tags", value = "标签(资质和擅长)", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "level", value = "教练级别", required = false, dataType = "int", paramType = "query"),
    })
    @RequestMapping(value = "/add.json", method = RequestMethod.POST)
    public Object addCoach(@RequestParam(value = "realname", required = true) String realname,
                           @RequestParam(value = "sex", required = true) Integer sex,
                           @RequestParam(value = "phone", required = true) String phone,
                           @RequestParam(value = "teachAge", required = false, defaultValue = "0") Integer teachAge,
                           @RequestParam(value = "skiAge", required = false, defaultValue = "0") Integer skiAge,
                           @RequestParam(value = "board", required = true) Integer board,
                           @RequestParam(value = "editStatus", required = true) Integer editStatus,
                           @RequestParam(value = "headerUrl", required = false) String headerUrl,
                           @RequestParam(value = "description", required = false) String description,
                           @RequestParam(value = "tags", required = false) String tags,
                           @RequestParam(value = "level", required = false, defaultValue = "0") Integer level,
                           HttpServletRequest request, HttpSession session) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null || organizer.getOrganizerId() == null) {
            //无法从session中获取organizer信息
            return WebResult.getErrorResult(WebResult.Code.COACH_GET_ORGANIZER_INFO_ERROR);
        }

        if (phone != null) {
            if (coachService.checkHasCoachByPhone(organizer.getOrganizerId(), phone)) {
                // 主办方该手机号已经添加,请勿重复添加;
                return WebResult.getErrorResult(WebResult.Code.COACH_PHONE_REPEAT_ERROR);
            } else {
                Coach coach = new Coach();
                coach.setOrganizerId(organizer.getOrganizerId());
                coach.setRealname(realname);
                coach.setSex(sex);
                coach.setPhone(phone);
                coach.setTeachAge(teachAge);
                coach.setSkiAge(skiAge);
                coach.setBoard(board);
                coach.setLevel(level);
                coach.setEditStatus(editStatus);
                coach.setHeaderUrl(headerUrl);
                description = description.replace("&quot;", "\"");
                coach.setDescription(description);
                coach.setCreateTime(new Date());

                if (!coachService.addCoach(coach, tags)) {
                    return WebResult.getErrorResult(WebResult.Code.ADD_FAILED);
                }
            }
        } else {
            return WebResult.getErrorResult(WebResult.Code.PHONE_IS_EMPTY);
        }

        return WebResult.getSuccessResult();
    }

    /**
     * 获取教练详情及教练的tags
     *
     * @param coachId
     * @param request
     * @param session
     * @return
     */
    @ApiOperation(value = "【教练信息】编辑教练按钮接口", notes = "获取教练详情及教练的tags", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "coachId", value = "教练ID", required = false, dataType = "int", paramType = "query"),
    })
    @RequestMapping(value = "/detail.json", method = RequestMethod.GET)
    public Object getCoachTag(@RequestParam(value = "coachId", required = false) Integer coachId,
                              HttpServletRequest request, HttpSession session) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null || organizer.getOrganizerId() == null) {
            return WebResult.getErrorResult(WebResult.Code.COACH_GET_ORGANIZER_INFO_ERROR);
        }

        Integer version = 2;
        if (organizer.getCategory() > 4) {
            version = organizer.getCategory();
        }

        Map<String, Object> result = new HashMap<>();
        Map<String, List<Tag>> tagMap = coachService.findCoachTagListByVersion(coachId, version);
        result.put("tag", tagMap);
        if (coachId != null) {
            Coach coach = coachService.findCoach(coachId);
            if (coach != null) {
                OrganizerEnumExample example = new OrganizerEnumExample();
                example.createCriteria().andValidEqualTo(1).andTypeEqualTo(0).andIsLabelEqualTo(0)
                        .andOrganizerIdEqualTo(organizer.getOrganizerId()).andSystemEqualTo(1);
                List<OrganizerEnum> orgExtends = organizerEnumService.selectByExample(example);
                if (orgExtends.size() <= 0) {
                    coach.setLevel(-1);
                }
            }
            result.put("coach", coach);
        }
        return WebResult.getSuccessResult("data", result);
    }

    /**
     * 教练信息 - 按人显示
     *
     * @return
     */
    @ApiOperation(value = "【教练管理】按人显示(通过条件筛选教练信息)接口", notes = "点击按人显示按钮调用的接口", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码，默认1", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "size", value = "页量，默认10", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "coachName", value = "教练姓名", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "phone", value = "手机(电话)", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "teachAge", value = "教龄", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "board", value = "技术类型(单板或冰球：1，双板或花滑：2  单板&双板或冰球&花滑：3)", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "sex", value = "性别(女0，男1)", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "valid", value = "是否有效(有效1，失效0)", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "startTime", value = "开始时间", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "endTime", value = "结束时间", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "timeType", value = "时段类型,默认值为0(0：有请假，1：有订单，2可出导[既没请假也没订单])", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "level", value = "教练级别", required = false, dataType = "int", paramType = "query"),
    })
    @RequestMapping(value = "/list.json", method = RequestMethod.GET)
    public Object getOrganizerCoachList(HttpServletRequest request, HttpSession session,
                                        @RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
                                        @RequestParam(value = "size", required = false, defaultValue = "10") Integer size,
                                        @RequestParam(value = "coachName", required = false) String coachName,
                                        @RequestParam(value = "phone", required = false) String phone,
                                        @RequestParam(value = "teachAge", required = false) Integer teachAge,
                                        @RequestParam(value = "board", required = false) Integer board,
                                        @RequestParam(value = "sex", required = false) Integer sex,
                                        @RequestParam(value = "valid", required = false) Integer valid,
                                        @RequestParam(value = "startTime", required = false) String startTime,
                                        @RequestParam(value = "endTime", required = false) String endTime,
                                        @RequestParam(value = "timeType", required = false, defaultValue = "0") Integer timeType,
                                        @RequestParam(value = "level", required = false) Integer level) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null || organizer.getOrganizerId() == null) {
            return WebResult.getErrorResult(WebResult.Code.COACH_GET_ORGANIZER_INFO_ERROR);
        }

        Coach coach = new Coach();
        coach.setOrganizerId(organizer.getOrganizerId());
//        try {
//            //get中文处理乱码
//            if (realname != null)
//                // realname = new String(realname.getBytes("iso8859-1"));
//                realname = new String(realname.getBytes(), "UTF-8");
//            if (phone != null)
//                phone = new String(phone.getBytes("iso8859-1"));
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//        }

//        if (valid != null) {
//            coach.setValid(valid);
//        }
//        if (level != null)
//            coach.setLevel(level);

        if (StringUtils.isNotBlank(coachName))
            coach.setRealname(coachName);

        if (StringUtils.isNotBlank(phone))
            coach.setPhone(phone);

        coach.setValid(valid);
        coach.setLevel(level);
        coach.setTeachAge(teachAge);
        coach.setBoard(board);
        coach.setSex(sex);
        PageList pageList = coachService.findCoachList(coach, startTime, endTime, timeType, page, size);
        return WebResult.getSuccessResult("data", pageList);
    }

    /**
     * 教练值为失效或有效
     *
     * @param coachId
     * @param request
     * @param session
     * @return
     */
    @ApiOperation(value = "教练置为失效或有效接口", notes = "教练置为失效时值为有效，有效状态时值为失效", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "coachId", value = "教练ID", required = true, dataType = "int", paramType = "query")
    })
    @RequestMapping(value = "/valid.json", method = RequestMethod.POST)
    public Object updateCoachValid1(@RequestParam(value = "coachId") Integer coachId,
                                    HttpServletRequest request, HttpSession session) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null || organizer.getOrganizerId() == null) {
            //无法从session中获取organizer信息
            return WebResult.getErrorResult(WebResult.Code.COACH_GET_ORGANIZER_INFO_ERROR);
        }

        Coach coach = coachService.findCoach(coachId);
        if (coach == null) {
            // 教练信息不存在
            return WebResult.getErrorResult(WebResult.Code.COACH_NO_INFO_ERROR);
        }

        if (!organizer.getOrganizerId().equals(coach.getOrganizerId())) {
            // 不是一个主办方，没有操作
            return WebResult.getErrorResult(WebResult.Code.NO_PERMISSION);
        }

        Integer result = coachService.modifyCoachValid(coach, organizer.getOrganizerId());
        if (result == 0) {
            if (coach.getValid() == 1) {
                scheduleCoachService.refreshCoachQueue(organizer, new Date(), null);
            }
            return WebResult.getSuccessResult();
        } else if (result == 2) {
            return WebResult.getErrorResult(WebResult.Code.COACH_BIND_COURSE_INVALID_FAIL);
        } else {// 返回1 和其他情况都是请求失败
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }
    }

    /**
     * 修改教练信息
     *
     * @param session
     * @return
     */
    @ApiOperation(value = "【教练管理】编辑保存教练接口", notes = "修改教练信息", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "coachId", value = "教练ID", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "realname", value = "教练姓名", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "sex", value = "性别(女0，男1)", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "phone", value = "手机号", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "teachAge", value = "教龄", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "skiAge", value = "雪龄或冰龄", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "board", value = "技术类型(单板或冰球：1，双板或花滑：2  单板&双板或冰球&花滑：3)", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "editStatus", value = "是否允许自己编辑", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "headerUrl", value = "正面照", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "description", value = "描述信息(富文本内容)", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "tags", value = "标签(资质和擅长)", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "level", value = "教练级别", required = false, dataType = "int", paramType = "query"),
    })
    @RequestMapping(value = "/update.json", method = RequestMethod.POST)
    public Object updateCoachNew(@RequestParam(value = "coachId") Integer coachId,
                                 @RequestParam(value = "realname", required = true) String realname,
                                 @RequestParam(value = "sex", required = true) Integer sex,
                                 @RequestParam(value = "phone", required = true) String phone,
                                 @RequestParam(value = "teachAge", required = false) Integer teachAge,
                                 @RequestParam(value = "skiAge", required = false) Integer skiAge,
                                 @RequestParam(value = "board", required = true) Integer board,
                                 @RequestParam(value = "editStatus", required = true) Integer editStatus,
                                 @RequestParam(value = "headerUrl", required = false) String headerUrl,
                                 @RequestParam(value = "description", required = false) String description,
                                 @RequestParam(value = "tags", required = false) String tags,
                                 @RequestParam(value = "level", required = false) Integer level,
                                 HttpServletRequest request, HttpSession session) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null || organizer.getOrganizerId() == null) {
            //无法从session中获取organizer信息
            return WebResult.getErrorResult(WebResult.Code.COACH_GET_ORGANIZER_INFO_ERROR);
        }

        Coach searchCoach = coachService.findCoach(coachId);
        if (phone != null && !phone.equals(searchCoach.getPhone())) {
            //查询到的phone和修改的phone不一样,说明phone字段有修改,需要再次校验是否重复
            //if (coachService.findCoachSizeByPhone(phone) > 0) {
            if (coachService.checkHasCoachByPhone(organizer.getOrganizerId(), phone)) {
                //说明修改后的电话号码有重复
                //data.setMsg("修改失败,该手机号已经关联了其他教练");
                return WebResult.getErrorResult(WebResult.Code.COACH_PHONE_MODIFY_FAIL);
            }
            //}
        }
        Coach coach = new Coach();
        coach.setCoachId(coachId);
        coach.setRealname(realname);
        coach.setSex(sex);
        coach.setPhone(phone);
        coach.setTeachAge(teachAge);
        coach.setSkiAge(skiAge);
        coach.setBoard(board);
        coach.setLevel(level);
        coach.setEditStatus(editStatus);
        coach.setHeaderUrl(headerUrl);
        description = description.replace("&quot;", "\"");
        coach.setDescription(description);
        if (coachService.updateCoach(organizer.getOrganizerId(), coach, tags)) {
            return WebResult.getSuccessResult();
        } else {
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }
    }

    /**
     * 更新教练与课程的关联关系
     *
     * @param parameter
     * @return
     */
    @ApiOperation(value = "更新教练与课程的关联关系", notes = "更新教练与课程的关联关系", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "parameter", value = "教练关联课程时传的参数,格式为json  parameter: {\"coachId\":\"278\",\"courseIds\":[756,757,792,793,841,879]}", required = true, dataType = "string", paramType = "query")
    })
    @RequestMapping(value = "/relation.json", method = RequestMethod.POST)
    public Object updateCoachCourse(@RequestParam(value = "parameter") String parameter, HttpServletRequest request) {
        JSONObject jsonPara = JSONObject.parseObject(parameter);
        Integer coachId = jsonPara.getInteger("coachId");
        if (coachId == null) {
            // 请选择教练在进行操作
            return WebResult.getErrorResult(WebResult.Code.COACH_RELATION_COURSE_FAIL);
        }
        List<Integer> courseList = JSONArray.parseArray(jsonPara.getString("courseIds"), Integer.class);
        if (coachService.modifyCoachCourseRelation(coachId, courseList)) {
            return WebResult.getSuccessResult();
        } else {
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }
    }

//    @ApiOperation(value = "获取教练的核销记录(server用)", notes = "获取教练的核销记录", httpMethod = "GET")
//    @ApiImplicitParams({
//            @ApiImplicitParam(name = "orderId", value = "订单ID", required = true, dataType = "int", paramType = "query")
//    })
//    @RequestMapping(value = "/order.json", method = RequestMethod.GET)
//    @ResponseBody
//    public Object getOrderExtendByOrderId(@RequestParam(value = "orderId") Integer orderId) {
//        JSONObject jsonObject = (JSONObject) courseService.getOrderExtendAndOrderViewByOrderId(orderId);
//        return WebResult.getSuccessResult("data", jsonObject);
//    }

    @ApiOperation(value = "教练系统获取主办方枚举", notes = "根据系统号，枚举类别去查询，默认查询全部", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "system", value = "系统（0：isnow   1：教练）", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "type", value = "枚举类型（教练：0，教练级别）", required = false, dataType = "int", paramType = "query"),
    })
    @RequestMapping(value = "/organizerEnum/list.json", method = RequestMethod.GET)
    public Object getOrganizerCustomEnum (@RequestParam(value = "system", required = false, defaultValue = "1") Integer system,
                                          @RequestParam(value = "type", required = false) Integer type,
                                          HttpServletRequest request) {
        Map<String,Object> resultMap = new HashMap<>();
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null) {
            return WebResult.getErrorResult(WebResult.Code.COACH_GET_ORGANIZER_INFO_ERROR);
        }
        List<OrganizerEnum> enumTypeList = organizerEnumService.getOrganizerEnumTypesInfo(organizer.getOrganizerId(), 1, 1, 0);
        Map<Integer,String> enumTypeMap = new HashMap<>();
        for (OrganizerEnum organizerEnum : enumTypeList) {
            if (type != null) {
                if (organizerEnum.getType().equals(type)) {
                    enumTypeMap.put(organizerEnum.getType(), organizerEnum.getCategoryTitle());
                    break;
                }
            } else {
                enumTypeMap.put(organizerEnum.getType(), organizerEnum.getCategoryTitle());
            }
        }

        OrganizerEnumExample example = new OrganizerEnumExample();
        for (Map.Entry<Integer,String> map : enumTypeMap.entrySet()) {
            example.createCriteria().andOrganizerIdEqualTo(organizer.getOrganizerId()).andSystemEqualTo(system)
                    .andValidEqualTo(1).andIsLabelEqualTo(0).andTypeEqualTo(map.getKey());
            example.setOrderByClause("sort");
            resultMap.put(map.getValue(), organizerEnumService.selectByExample(example));
            example.clear();
        }

        return WebResult.getSuccessResult("data", resultMap);
    }

    @ApiOperation(value = "教练系统保存主办方枚举", notes = "保存", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "system", value = "系统（0：isnow   1：教练）", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "data", value = "（json数据）", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "type", value = "保存类型（0，教练级别）", required = true, dataType = "int", paramType = "query"),
    })
    @RequestMapping(value = "/organizerEnum/save.json", method = RequestMethod.POST)
    public Object saveOrganizerCustomEnum (@RequestParam(value = "system", required = false, defaultValue = "1") Integer system,
                                           @RequestParam(value = "data", required = false) String data,
                                           @RequestParam(value = "type") Integer type,
                                           HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        CommonAccount account = WebUpmsContext.getAccount(request);
        if (organizer == null) {
            // responseData.setMsg("无法获取主办方信息，请尝试重新登录");
            return WebResult.getErrorResult(WebResult.Code.COACH_GET_ORGANIZER_INFO_ERROR);
        }
        List<OrganizerEnum> organizerEnumList = new ArrayList<>();
        Date now = new Date();
        OrganizerEnumExample example = new OrganizerEnumExample();
        OrganizerEnum organizerEnum = new OrganizerEnum();
        organizerEnum.setValid(0);
        organizerEnum.setOperatorId(account.getCustomerId());
        organizerEnum.setUpdateTime(now);
        if (type == 0) {
            example.createCriteria().andOrganizerIdEqualTo(organizer.getOrganizerId()).andSystemEqualTo(system)
                    .andValidEqualTo(1).andTypeEqualTo(0).andIsLabelEqualTo(0);
            organizerEnumService.updateByExampleSelective(organizerEnum, example);
            JSONArray coachLevel = JSONArray.parseArray(data);
            for (Object o : coachLevel) {
                organizerEnum = new OrganizerEnum();
                JSONObject item = (JSONObject) o;
                organizerEnum.setOrganizerId(organizer.getOrganizerId());
                organizerEnum.setSystem(1);
                organizerEnum.setType(type);
                organizerEnum.setValid(1);
                organizerEnum.setName(item.getString("name"));
                organizerEnum.setValue(item.getInteger("value"));
                organizerEnum.setSort(item.getInteger("value"));
                organizerEnum.setOperatorId(account.getCustomerId());
                organizerEnum.setCreateTime(now);
                organizerEnum.setUpdateTime(now);
                organizerEnum.setLabel("board" + organizerEnum.getValue());
                organizerEnum.setIsLabel(0);
                organizerEnum.setEnglish(null);
                organizerEnum.setCategoryTitle("level");
                organizerEnum.setAlmighty(0);
                organizerEnumList.add(organizerEnum);
            }
        }

        if (organizerEnumService.insertBatch(organizerEnumList) > 0) {
            return WebResult.getSuccessResult();
        }
        return WebResult.getErrorResult(WebResult.Code.ERROR);
    }

    /**
     * 根据卡号查询是否存在卡
     *
     * @return
     */
    @ApiOperation(value = "根据教练ID 获取 绑定的卡信息", notes = "根据教练ID 获取 绑定的卡信息", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "cardNumber", value = "刷出来的卡号", required = true, dataType = "long", paramType = "query"),
    })
    @RequestMapping(value = "/adddMaterialInfoByCard.json", method = RequestMethod.POST)
    public Object getMaterialInfoByCard(@RequestParam("cardNumber") Long cardNumber, HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null) {
            // responseData.setMsg("无法获取主办方信息，请尝试重新登录");
            return WebResult.getErrorResult(WebResult.Code.COACH_GET_ORGANIZER_INFO_ERROR);
        }
        try {
            Material material = coachService.selectByMaterialNum(String.valueOf(cardNumber));
            // responseData.setData(resultMap);
            if (material != null) {
                return WebResult.getErrorResult(WebResult.Code.COACH_CARD_MATERIAL_NUM_HAVE_ERROR);
            }

            String hex = Long.toHexString(cardNumber);
            hex = transform(hex, "");

            material = new Material();
            material.setMaterialNum(String.valueOf(cardNumber));
            material.setGateWaferNum(hex);
            material.setOrganizerId(organizer.getOrganizerId());
            material.setStatus(0); // 可用
            material.setMaterialType(0);
            material.setEquipmentId(200); // 教练卡

            if (coachService.insertMaterial(material) > 0) {
                return WebResult.getSuccessResult();
            } else {
                return WebResult.getErrorResult(WebResult.Code.ERROR);
            }

        } catch (Exception e) {
            e.printStackTrace();
            // responseData.setMsg("查询卡信息出错，请重新尝试");
            return WebResult.getErrorResult(WebResult.Code.COACH_CARD_FIND_ERROR);
        }
    }

    /**
     * 根据卡号查询是否存在卡
     *
     * @return
     */
    @ApiOperation(value = "刷卡查询教练卡绑定教练的信息", notes = "刷卡查询教练卡绑定教练的信息", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "cardNumber", value = "刷出来的卡号", required = true, dataType = "long", paramType = "query"),
    })
    @RequestMapping(value = "/infoByCard.json", method = RequestMethod.POST)
    public Object getCoachInfoByCard(@RequestParam("cardNumber") Long cardNumber, HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null) {
            // responseData.setMsg("无法获取主办方信息，请尝试重新登录");
            return WebResult.getErrorResult(WebResult.Code.COACH_GET_ORGANIZER_INFO_ERROR);
        }
        try {
            String hex = Long.toHexString(cardNumber);
            hex = transform(hex, "");
            Material material = coachService.selectByGateWaferNum(hex, organizer.getOrganizerId(), null);
            // responseData.setData(resultMap);
            if (material == null) {
                return WebResult.getErrorResult(WebResult.Code.COACH_CARD_NO_ERROR);
            }

            Coach coach = coachService.getCoachByMaterial(material.getMaterialId());
            if (coach == null) {
                return WebResult.getErrorResult(WebResult.Code.COACH_MATERIAL_NO_BIND_COACH);
            }

            return WebResult.getSuccessResult("data", coach);
        } catch (Exception e) {
            e.printStackTrace();
            // responseData.setMsg("查询卡信息出错，请重新尝试");
            return WebResult.getErrorResult(WebResult.Code.COACH_CARD_FIND_ERROR);
        }
    }


    /**
     * 根据教练ID 获取 绑定的卡信息
     *
     * @param coachId
     * @return
     */
    @ApiOperation(value = "根据教练ID 获取 绑定的卡信息", notes = "根据教练ID 获取 绑定的卡信息", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "coachId", value = "教练ID", required = true, dataType = "int", paramType = "query"),
    })
    @RequestMapping(value = "/getMaterialByCoachId.json", method = RequestMethod.GET)
    public Object getMaterialByCoachId(@RequestParam("coachId") Integer coachId) {
        try {
            Map<String, Object> resultMap = coachService.getMaterialByCoachId(coachId);
            // responseData.setData(resultMap);
            return WebResult.getSuccessResult("data", resultMap);
        } catch (Exception e) {
            e.printStackTrace();
            // responseData.setMsg("查询卡信息出错，请重新尝试");
            return WebResult.getErrorResult(WebResult.Code.COACH_CARD_FIND_ERROR);
        }
    }

    /**
     * 教练卡绑定(对未帮过卡的教练的操作)
     *
     * @return
     */
    @ApiOperation(value = "教练卡绑定(对未帮过卡的教练的操作)", notes = "教练卡绑定(对未帮过卡的教练的操作)", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "coachId", value = "教练ID", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "cardNo", value = "卡号", required = true, dataType = "long", paramType = "query"),
    })
    @RequestMapping(value = "/cardBind.json", method = RequestMethod.POST)
    public Object coachCardBind(@RequestParam("coachId") Integer coachId, @RequestParam("cardNo") Long cardNo,
                                HttpSession session, HttpServletRequest request) {
        try {
            if (cardNo == null) {
                // responseData.setMsg("读卡失败，请重新刷卡");
                return WebResult.getErrorResult(WebResult.Code.COACH_CARD_READ_FAIL);
            }
            if (coachId == null) {
                // responseData.setMsg("读取教练信息失败，请尝试刷新页面");
                return WebResult.getErrorResult(WebResult.Code.COACH_NO_INFO_ERROR);
            }

            Organizer organizer = WebUpmsContext.getOrgnization(request);
            if (organizer == null) {
                // responseData.setMsg("无法获取主办方信息，请尝试重新登录");
                return WebResult.getErrorResult(WebResult.Code.COACH_GET_ORGANIZER_INFO_ERROR);
            }

            String hex = Long.toHexString(cardNo);
            hex = transform(hex, "");
            // 检查卡信息
            Material material = coachService.selectByGateWaferNum(hex, organizer.getOrganizerId(), null);
            if (material == null) {
                // responseData.setMsg("当前卡未录制到系统中、或卡芯损坏，请尝试换卡");
                return WebResult.getErrorResult(WebResult.Code.COACH_CARD_NO_ERROR);
            }
            if (material.getStatus() != null && material.getStatus().equals(1)) {
                // responseData.setMsg("当前卡正在被使用中，请尝试换卡、或等卡使用完毕");
                return WebResult.getErrorResult(WebResult.Code.COACH_CARD_CAKE_UP_ERROR);
            }
            Coach coach = coachService.getCoachByMaterial(material.getMaterialId());
            if (coach != null) {
                // responseData.setMsg("当前卡已经与“" + coach.getRealname() + "”教练绑定，请尝试换卡");
                return WebResult.getErrorMsgResult(WebResult.Code.COACH_APPOINTMENT_TIME_ERROR, "当前卡已经与“" + coach.getRealname() + "”教练绑定，请尝试换卡");
            }
            // 卡信息检查完毕

            // 检查教练信息
            coach = coachService.selectByPrimaryKey(coachId);
            if (coach == null) {
                // responseData.setMsg("查找教练信息失败，请尝试刷新页面");
                return WebResult.getErrorResult(WebResult.Code.COACH_NO_INFO_ERROR);
            }
            if (coach.getMaterialId() != null) {
                // responseData.setMsg("当前教练已绑定教练卡，请尝试刷新页面进行确认");
                return WebResult.getErrorResult(WebResult.Code.COACH_CARD_ALREADY_BIND);
            }
            // 教练信息检查完毕

            // 进行绑卡操作
            boolean resultBool = coachService.coachCardBind(coachId, hex, material.getMaterialId());
            if (resultBool) {
                // responseData.setMsg("绑卡成功");
                // responseData.setData(material.getMaterialNum());
                return WebResult.getSuccessResult("data", material.getMaterialNum());
            } else {
                // responseData.setMsg("绑卡出错，请重新尝试");
                return WebResult.getErrorResult(WebResult.Code.COACH_CARD_BIND_FAIL);
            }

        } catch (Exception e) {
            e.printStackTrace();
            // responseData.setMsg("绑卡出错，请重新尝试，或换卡尝试");
            return WebResult.getErrorResult(WebResult.Code.COACH_CARD_BIND_ERROR);
        }
    }

    /**
     * 教练卡取消绑定
     *
     * @return
     */
    @ApiOperation(value = "教练卡取消绑定", notes = "教练卡取消绑定", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "coachId", value = "教练ID", required = true, dataType = "int", paramType = "query"),
    })
    @RequestMapping(value = "/coachCardUnbind.json", method = RequestMethod.POST)
    public Object coachCardUnbind(@RequestParam("coachId") Integer coachId, HttpServletRequest request, HttpSession session) {
        try {
            if (coachId == null) {
                // responseData.setMsg("读取教练信息失败，请尝试刷新页面");
                return WebResult.getErrorResult(WebResult.Code.COACH_CARD_UNBIND_FAIL);
            }

            Organizer organizer = WebUpmsContext.getOrgnization(request);
            if (organizer == null) {
                return WebResult.getErrorResult(WebResult.Code.COACH_GET_ORGANIZER_INFO_ERROR);
            }

            // 检查教练信息
            Coach coach = coachService.selectByPrimaryKey(coachId);
            if (coach == null) {
                return WebResult.getErrorResult(WebResult.Code.COACH_NO_INFO_ERROR);
            }
            if (coach.getMaterialId() == null) {
                // responseData.setMsg("当前教练未绑卡，请尝试刷新页面进行确认");
                return WebResult.getErrorResult(WebResult.Code.COACH_CARD_NO_BIND_FAIL);
            }
            // 教练信息检查完毕

            // 检查卡信息
            Material material = coachService.selectMaterialByPrimaryKey(coach.getMaterialId());
            if (material == null) {
                // responseData.setMsg("未找到当前教练绑定的卡，请尝试刷新页面");
                return WebResult.getErrorResult(WebResult.Code.COACH_CARD_NO_ALREADY_BIND_FAIL);

            }
            if (material.getStatus() != null && material.getStatus().equals(1)) {
                //responseData.setMsg("当前教练的卡 正在使用中，请等待出导结束再进行解绑操作");
                return WebResult.getErrorResult(WebResult.Code.COACH_CARD_USE_UNBIND_FAIL);
            }
            // 检查卡信息 end

            // 进行解绑操作
            boolean resultBool = coachService.coachCardUnbind(coach);
            if (resultBool) {
                return WebResult.getSuccessResult("data", material.getMaterialNum());
            } else {
                // responseData.setMsg("解绑出错，请重新尝试");
                return WebResult.getErrorResult(WebResult.Code.COACH_CARD_UNBIND_ERROR);
            }

        } catch (Exception e) {
            e.printStackTrace();
            // responseData.setMsg("教练卡取消绑定失败，请重新尝试");
            return WebResult.getErrorResult(WebResult.Code.COACH_CARD_UNBIND_FAIL);
        }
    }

    /**
     * 教练换卡
     *
     * @param materialNumNow 当前绑定的卡号    例如：3504370
     * @param cardNo         需要更换为 的 卡number    例如：2925913099
     */
    @ApiOperation(value = "教练换卡", notes = "教练换卡", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "coachId", value = "教练ID", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "cardNo", value = "需要更换为的卡number", required = true, dataType = "long", paramType = "query"),
            @ApiImplicitParam(name = "materialNumNow", value = "当前绑定的卡号", required = true, dataType = "string", paramType = "query"),
    })
    @RequestMapping(value = "/coachCardCharge.json", method = RequestMethod.POST)
    public Object coachCardCharge(@RequestParam("coachId") Integer coachId,
                                  @RequestParam("cardNo") Long cardNo,
                                  @RequestParam("materialNumNow") String materialNumNow,
                                  HttpSession session, HttpServletRequest request) {
        try {
            Organizer organizer = WebUpmsContext.getOrgnization(request);
            if (organizer == null) {
                // responseData.setMsg("无法获取主办方信息，请尝试重新登录");
                return WebResult.getErrorResult(WebResult.Code.COACH_GET_ORGANIZER_INFO_ERROR);
            }

            if (cardNo == null) {
                // responseData.setMsg("读卡失败，请重新刷卡");
                return WebResult.getErrorResult(WebResult.Code.COACH_CARD_READ_FAIL);
            }
            if (coachId == null) {
                // responseData.setMsg("读取教练信息失败，请尝试刷新页面");
                return WebResult.getErrorResult(WebResult.Code.COACH_NO_INFO_ERROR);
            }
            if (materialNumNow == null) {
                // responseData.setMsg("读取已绑卡信息失败，请尝试刷新页面");
                return WebResult.getErrorResult(WebResult.Code.COACH_CARD_ALEADY_UNBIND_FAIL);
            }

            String hex = Long.toHexString(cardNo);
            hex = transform(hex, "");
            // 检查新卡信息
            Material materialNew = coachService.selectByGateWaferNum(hex, organizer.getOrganizerId(), null);
            if (materialNew == null) {
                // responseData.setMsg("当前卡未录制到系统中、或卡芯损坏，请尝试换卡");
                return WebResult.getErrorResult(WebResult.Code.COACH_CARD_NO_ERROR);
            }
            if (materialNew.getStatus() != null && materialNew.getStatus().equals(1)) {
                // responseData.setMsg("当前卡正在被使用中，请尝试换卡、或等卡使用完毕");
                return WebResult.getErrorResult(WebResult.Code.COACH_CARD_CAKE_UP_ERROR);
            }
            Coach materialNewCoach = coachService.getCoachByMaterial(materialNew.getMaterialId());
            if (materialNewCoach != null) {
                // responseData.setMsg("当前卡已经与“" + materialNewCoach.getRealname() + "”教练绑定，请尝试换卡");
                return WebResult.getErrorMsgResult(WebResult.Code.COACH_APPOINTMENT_TIME_ERROR, "当前卡已经与“" + materialNewCoach.getRealname() + "”教练绑定，请尝试换卡");
            }
            // 新卡信息检查完毕

            // 检查教练信息
            Coach coach = coachService.selectByPrimaryKey(coachId);
            if (coach == null) {
                return WebResult.getErrorResult(WebResult.Code.COACH_NO_INFO_ERROR);
            }
            if (coach.getMaterialId() == null) {
                // responseData.setMsg("当前教练未绑定卡，请尝试刷新页面");
                return WebResult.getErrorResult(WebResult.Code.COACH_CARD_NO_BIND_FAIL);
            }
            // 教练信息检查完毕

            // 检查当前绑定的卡信息
            Material materialNow = coachService.selectByMaterialNum(materialNumNow);

            if (materialNow == null) {
                // responseData.setMsg("查找当前绑定卡信息失败，请尝试刷新页面");
                return WebResult.getErrorResult(WebResult.Code.COACH_CARD_SELECT_FAIL);
            }
            if (!materialNow.getMaterialId().equals(coach.getMaterialId())) {
                // responseData.setMsg("教练与已绑定卡数据不匹配，请尝试刷新页面");
                return WebResult.getErrorResult(WebResult.Code.COACH_CARD_COUNT_FAIL);
            }
            if (materialNow.getStatus() != null && materialNow.getStatus().equals(1)) {
                // responseData.setMsg("当前教练正在使用（" + materialNumNow + "）此卡出导中，请等待出导结束再进行换卡操作");
                return WebResult.getErrorMsgResult(WebResult.Code.COACH_APPOINTMENT_TIME_ERROR, "当前教练正在使用（" + materialNumNow + "）此卡出导中，请等待出导结束再进行换卡操作");
            }
            // 检查当前绑定的卡信息end

            // 进行换卡操作
            boolean resultBool = coachService.coachCardCharge(coachId, materialNew.getMaterialId(), materialNow.getMaterialId());
            if (resultBool) {
                // responseData.setMsg("换卡成功");
                return WebResult.getSuccessResult("data", materialNew.getMaterialNum());
            } else {
                // responseData.setMsg("换卡出错，请重新尝试");
                return WebResult.getErrorResult(WebResult.Code.COACH_CARD_CHANGE_FAIL);
            }
        } catch (Exception e) {
            e.printStackTrace();
            // responseData.setMsg("教练换卡出错，请重新尝试，或换张卡尝试");
            return WebResult.getErrorResult(WebResult.Code.COACH_CARD_CHANGE_ERROR);
        }
    }

    private String transform(String origin, String des) {
        if (StringUtils.isBlank(des) && origin.length() < 8) {
            int len = 8 - origin.length();
            for (int i = 0; i < len; i++)
                origin = "0" + origin;
        }
        if (origin.length() <= des.length()) {
            return des.toUpperCase();
        } else {
            String last = origin.substring(origin.length() - 2 - des.length(), origin.length() - des.length());
            return transform(origin, des + last);
        }
    }

    @GetMapping("/coachList.json")
    public Object selectAcceptCoach(HttpServletRequest request){
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null) {
            // responseData.setMsg("无法获取主办方信息，请尝试重新登录");
            return WebResult.getErrorResult(WebResult.Code.COACH_GET_ORGANIZER_INFO_ERROR);
        }
        List<Coach> acceptList = coachService.seletByAccept(1, organizer.getOrganizerId());
        List<Coach> notAcceptList = coachService.seletByAccept(0, organizer.getOrganizerId());
        Map<String, Object> data = new HashMap<>();
        data.put("acceptList", acceptList);
        data.put("notAcceptList", notAcceptList);

        return WebResult.getSuccessResult("data", data);
    }

    @PostMapping("/select.json")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "coachIds", value = "教练ID集合(,分隔)", required = false, dataType = "string", paramType = "query"),
    })
    public Object updateCoachAccept(@RequestParam String coachIds, HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null) {
            // responseData.setMsg("无法获取主办方信息，请尝试重新登录");
            return WebResult.getErrorResult(WebResult.Code.COACH_GET_ORGANIZER_INFO_ERROR);
        }

        coachService.updateCoachAcceptByOrganizerId(organizer.getOrganizerId());

        if(!StringUtils.isEmpty(coachIds)){
            String[] split = coachIds.split(",");
            List<String> coachIdList = Arrays.asList(split);


            coachService.updateCoachAccept(0, coachIdList);
        }
        return WebResult.getSuccessResult();
    }
}
