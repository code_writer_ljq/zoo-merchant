package com.zoo.zoomerchantadminweb.coach.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.zoo.activity.dao.model.Coach;
import com.zoo.activity.dao.model.CoachGroup;

import com.zoo.activity.dao.model.Organizer;
import com.zoo.activity.service.CoachGroupService;

import com.zoo.web.bean.WebResult;
import com.zoo.wechat.common.Config;
import com.zoo.zoomerchantadminweb.base.BaseController;
import com.zoo.zoomerchantadminweb.context.WebUpmsContext;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import sun.misc.BASE64Encoder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ljq on 2017/5/5.
 */
@Api(value = "/api/server/coach/group", tags = {"【教练系统】教练分组相关接口"}, description = "教练分组相关接口")
@RestController
@RequestMapping("/api/server/coach/group")
public class CoachGroupController extends BaseController {

    @Autowired
    private CoachGroupService coachGroupService;

    /**
     * 保存分组信息（创建分组和更新分组）
     *
     * @param name
     * @param description
     * @return
     */
    @ApiOperation(value = "添加分组信息接口", notes = "保存分组信息", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "groupId", value = "组ID", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "name", value = "组名", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "description", value = "组描述", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "coachId", value = "分组中所有教练ID,包含队长ID(格式:[129,138])", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "captainId", value = "队长ID（格式:[129]）", required = false, dataType = "string", paramType = "query"),
    })
    @RequestMapping(value = "/save.json", method = RequestMethod.POST)
    public Object saveGroup(@RequestParam(value = "groupId", required = false) Integer groupId,
                            @RequestParam(value = "name") String name,
                            @RequestParam(value = "description", required = false) String description,
                            String coachId, String captainId, HttpServletRequest request, HttpSession session) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null && organizer.getOrganizerId() != null) {
            return WebResult.getErrorResult(WebResult.Code.COACH_GET_ORGANIZER_INFO_ERROR);
        }
        Integer organizerId = organizer.getOrganizerId();

        JSONArray coachIdArray = JSON.parseArray(coachId);
        JSONArray captainIdArray = JSON.parseArray(captainId);
        coachIdArray.addAll(captainIdArray);
        List<Integer> coachIds = new ArrayList<>();
        List<Integer> captainIds = new ArrayList<>();
        for (int i = 0; i < coachIdArray.size(); i++) {
            coachIds.add((Integer) coachIdArray.get(i));
        }

        for (int i = 0; i < captainIdArray.size(); i++) {
            captainIds.add((Integer) captainIdArray.get(i));
        }

        CoachGroup coachGroup = new CoachGroup();
        //groupId为null：创建分组操作，   groupId有值：为更新分组操作
        if (groupId == null) {
            coachGroup = new CoachGroup();
            coachGroup.setName(name);
            coachGroup.setDescription(description);
            coachGroup.setOrganizerId(organizerId);
        } else {
            //更新分组操作
            coachGroup = coachGroupService.getCoachGroupById(groupId);
            if (!organizerId.equals(coachGroup.getOrganizerId())) {
                // 您不能更改其他主办方的分组信息
                return WebResult.getErrorResult(WebResult.Code.ERROR);
            }
            coachGroup.setName(name);
            coachGroup.setDescription(description);
        }

        //创建分组和更新操作
        if (!coachGroupService.saveCoachGroup(coachGroup, coachIds, captainIds)) {
            if (groupId == null) {
                return WebResult.getErrorResult(WebResult.Code.ADD_FAILED);
            } else {
                return WebResult.getErrorResult(WebResult.Code.UPDATE_FAILED);
            }
        }
        // 保存或更新成功
        return WebResult.getSuccessResult();
    }

    /**
     * 删除分组
     *
     * @param groupId
     * @return
     */
    @ApiOperation(value = "删除分组接口", notes = "删除分组操作", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "groupId", value = "组ID", required = true, dataType = "int", paramType = "query"),
    })
    @RequestMapping(value = "/delete.json", method = RequestMethod.POST)
    public Object deleteGroupById(@RequestParam(value = "groupId") Integer groupId,
                                  HttpServletRequest request, HttpSession session) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null && organizer.getOrganizerId() != null) {
            return WebResult.getErrorResult(WebResult.Code.COACH_GET_ORGANIZER_INFO_ERROR);
        }
        CoachGroup coachGroup = coachGroupService.getCoachGroupById(groupId);
        if (coachGroup != null && coachGroup.getOrganizerId() != null && !organizer.getOrganizerId().equals(coachGroup.getOrganizerId())) {
            return WebResult.getErrorResult(WebResult.Code.NO_PERMISSION);
        }

        if (coachGroupService.deleteGroup(groupId)) {
            return WebResult.getSuccessResult();
        }
        return WebResult.getErrorResult(WebResult.Code.DELTED_FAILED);
    }

    /**
     * 查询所有分组信息，并且将第一个分组的成员查询出来
     *
     * @param session
     * @returnode
     */
    @ApiOperation(value = "查询所有分组信息接口", notes = "查询所有分组信息,并且将第一个分组的成员查询出来", httpMethod = "GET")
    @RequestMapping(value = "/list.json", method = RequestMethod.GET)
    public Object groupList(HttpServletRequest request, HttpSession session) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null) {
            return WebResult.getErrorResult(WebResult.Code.COACH_GET_ORGANIZER_INFO_ERROR);
        }

        Map<String, Object> coachGroupMap = new HashMap<>();
        List<CoachGroup> coachGroupList = coachGroupService.getAllCoachGroup(organizer.getOrganizerId());
        coachGroupMap.put("groupList", coachGroupList);
        if (coachGroupList != null & coachGroupList.size() != 0) {
            List<Coach> firstGroupCoachList = coachGroupService.findCoachListByGroupId(coachGroupList.get(0).getGroupId(), organizer.getOrganizerId());
            coachGroupMap.put("firstGroupCoachList", firstGroupCoachList);
        }
        coachGroupMap.put("encodeClientId", base64Encode(organizer.getOrganizerId().toString(), ENCODE_KEY));
        coachGroupMap.put("encodeCode", base64Encode(organizer.getOrganizerId().toString(), ENCODE_CODE_KEY));
        coachGroupMap.put("domain", Config.instance().getDomain());

        return WebResult.getSuccessResult("data", coachGroupMap);
    }

    public static String base64Encode(String s,String key) {
        s=key+s;
        BASE64Encoder base64 = new BASE64Encoder();
        return base64.encode(s.getBytes());
    }

    /**
     * 根据分组id查询分组下的所有教练
     *
     * @param session
     * @param groupId 分组的id（如果为空的话查询未分组教练，，如果有值得话查询该分组下的所有教练信息）
     * @return
     */
    @ApiOperation(value = "根据分组id查询分组下的所有教练信息接口", notes = "查询分组下的所有教练", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "groupId", value = "组ID(groupId为空查询主办方的所有未分组教练,不为空查询主办方指定分组下的所有教练)", required = false, dataType = "int", paramType = "query"),
    })
    @RequestMapping(value = "/coachListByGroupId.json", method = RequestMethod.GET)
    @ResponseBody
    public Object getCoachListByGroupId(HttpServletRequest request, HttpSession session,
                                        @RequestParam(value = "groupId", required = false) Integer groupId) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null && organizer.getOrganizerId() != null) {
            return WebResult.getErrorResult(WebResult.Code.COACH_GET_ORGANIZER_INFO_ERROR);
        }

        //获取指定分组下有教练或未分组教练（根据主办方id查询）:  groupId为空查询所属主办方的所有未分组教练，，不为空查询所属主办方指定分组下的所有教练
        List<Coach> coachList = coachGroupService.findCoachListByGroupId(groupId, organizer.getOrganizerId());
        return WebResult.getSuccessResult("data", coachList);
    }
}
