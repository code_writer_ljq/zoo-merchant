package com.zoo.zoomerchantadminweb.coach.controller;

import com.zoo.common.page.PageList;
import com.zoo.activity.dao.model.Coach;
import com.zoo.activity.dao.model.CoachRest;
import com.zoo.activity.dao.model.Organizer;
import com.zoo.activity.dao.nutz.pojo.CommonAccount;
import com.zoo.activity.service.CoachRestService;
import com.zoo.activity.service.CoachService;
import com.zoo.util.data.DateUtil;
import com.zoo.web.bean.WebResult;
import com.zoo.zoomerchantadminweb.base.BaseController;
import com.zoo.zoomerchantadminweb.context.WebUpmsContext;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by ljq on 2018/8/25.
 */
@Api(value = "/api/server/coach/rest", tags = "【教练系统】请假管理相关接口", description = "教练请假")
@RestController
@RequestMapping("/api/server/coach/rest")
public class CoachRestController extends BaseController {
    @Autowired
    private CoachRestService coachRestService;

    @Autowired
    private CoachService coachService;

    /**
     * 后台给教练请假
     * @param coachId
     * @param startTimeStr
     * @param endTimeStr
     * @param reason
     * @param restType
     * @param request
     * @param session
     * @return
     */
    @ApiOperation(value = "后台给教练请假接口", notes = "教练请假", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "coachId", value = "教练ID", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "startTime", value = "开始时间（格式：字符串）", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "endTime", value = "结束时间（格式：字符串）", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "reason", value = "请假原因", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "restType", value = "请假类型", required = true, dataType = "int", paramType = "query"),
    })
    @RequestMapping(value = "/save.json", method = RequestMethod.POST)
    public Object addCoachRest(@RequestParam(value = "coachId") Integer coachId,
                               @RequestParam(value = "startTime") String startTimeStr,
                               @RequestParam(value = "endTime") String endTimeStr,
                               @RequestParam(value = "reason", required = false) String reason,
                               @RequestParam(value = "restType") Integer restType,
                               HttpServletRequest request, HttpSession session) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null || organizer.getOrganizerId() == null) {
            //无法从session中获取organizer信息
            return WebResult.getErrorResult(WebResult.Code.COACH_GET_ORGANIZER_INFO_ERROR);
        }
        try {
            Date startTime = DateUtil.convertSimpleStringToDateTimeWithoutSecond(startTimeStr);
            Date endTime = DateUtil.convertSimpleStringToDateTimeWithoutSecond(endTimeStr);
            long startDate = startTime.getTime();
            long endDate = endTime.getTime();
            BigDecimal divisor = new BigDecimal(endDate - startDate);
            BigDecimal dividend = new BigDecimal(60 * 60 * 1000);
            Double duration = divisor.divide(dividend, 1, BigDecimal.ROUND_HALF_UP).doubleValue();
            if (startDate >= endDate) {
                // 请假开始时间必须小于结束时间
                return WebResult.getErrorResult(WebResult.Code.COACH_REST_START_TIME_AFTER_END_TIME);
            } else if (reason.length() > 200) {
                // 请假原因不能超过200个字
                return WebResult.getErrorResult(WebResult.Code.COACH_REST_REASON_TOO_LONG);
            } else if (!coachService.checkCoachCanAppoint(coachId, startTime, duration, organizer, null, 1)) {
                return WebResult.getErrorResult(WebResult.Code.COACH_HAS_ORDER_REST_FAIL);
            } else {
                CoachRest rest = new CoachRest();
                rest.setCoachId(coachId);
                // schedule端 发起请假无需审批
                rest.setStatus(2);
                rest.setRestStart(startTime);
                rest.setRestEnd(endTime);
                rest.setReason(reason);
                Date now = new Date();
                rest.setCreateTime(now);
                rest.setDuration(getDistanceDay(startTime, endTime));
                rest.setRestType(restType);
                if (coachRestService.addCoachRest(rest)) {
                    return WebResult.getSuccessResult();
                } else {
                    // 添加请假失败
                    return WebResult.getErrorResult(WebResult.Code.ERROR);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            // 请假日期有误
            return WebResult.getErrorResult(WebResult.Code.COACH_REST_FORMAT_DATE_ERROR);
        }
    }

    /**
     * 请假列表接口 - 分页
     * @param request
     * @param session
     * @return
     */
    @ApiOperation(value = "请假列表数据接口", notes = "请假列表数据(分页)", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码，默认1", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "size", value = "页量，默认10", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "startTime", value = "开始时间(格式：时间戳)", required = false, dataType = "long", paramType = "query"),
            @ApiImplicitParam(name = "endTime", value = "结束时间（格式L:时间戳）", required = false, dataType = "long", paramType = "query"),
            @ApiImplicitParam(name = "duration", value = "请假时长(单位：天)", required = false, dataType = "float", paramType = "query"),
            @ApiImplicitParam(name = "status", value = "请假状态(1，提交 0，取消（不准假，自己取消） 2，审批通过)", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "coachId", value = "教练ID", required = false, dataType = "int", paramType = "query"),
    })
    @RequestMapping(value = "/list.json", method = RequestMethod.GET)
    public Object getAllCoachRestList(@RequestParam(value = "coachId",required = false) Integer coachId,
                                      @RequestParam(value = "startTime",required = false) Long startTime,
                                      @RequestParam(value = "endTime",required = false) Long endTime,
                                      @RequestParam(value = "duration",required = false)Float duration,
                                      @RequestParam(value = "status",required = false)Integer status,
                                      @RequestParam(value = "page",required = false,defaultValue = "1")Integer page,
                                      @RequestParam(value = "size",required = false,defaultValue = "10")Integer size,
                                      HttpServletRequest request, HttpSession session) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null || organizer.getOrganizerId() == null) {
            //无法从session中获取organizer信息
            return WebResult.getErrorResult(WebResult.Code.COACH_GET_ORGANIZER_INFO_ERROR);
        }

        CoachRest rest = new CoachRest();
        rest.setCoachId(coachId);
        rest.setRestStart(startTime != null ? new Date(startTime) : null);
        rest.setRestEnd(endTime != null ? new Date(endTime) : null);
        rest.setDuration(duration);
        rest.setStatus(status);
        rest.setOrganizerId(organizer.getOrganizerId());
        PageList resultList = coachRestService.findCoachRestByCondition(rest,page,size);

        return WebResult.getSuccessResult("data", resultList);
    }

    /**
     * 后台管理员对教练进行请假审批
     * @param session
     * @param restId
     * @param status
     * @return
     */
        @ApiOperation(value = "【请假管理】列表操作栏请假审批接口", notes = "后台管理员对教练进行请假审批", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "restId", value = "请假ID", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "status", value = "请假状态（0不准假， 1 审批通过）", required = true, dataType = "int", paramType = "query"),
    })
    @RequestMapping(value = "/audit.json", method = RequestMethod.POST)
    public Object modifyCoachRestInfo(HttpServletRequest request, HttpSession session,
                                      @RequestParam(value = "restId") Integer restId,
                                      @RequestParam(value = "status") Integer status) {
        CommonAccount account = WebUpmsContext.getAccount(request);
        String userName = WebUpmsContext.getUserName(request);
        if (account == null) {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        } else {
            // 进行微信推送
            CoachRest rest = coachRestService.getCoachRestByRestId(restId);
            Coach coach = coachService.findCoach(rest.getCoachId());
            if (status.equals(2)) {
                if (coachRestService.confirmCoachRest(restId, account.getCustomerId(), userName)) {

                    //修改推送bug
//                    Integer customerId = assignCoachService.getCustomerIdByCoachId(coach.getCoachId());
//                    if (customerId != null && customerId > 0) {
//                        AccountResult coachAccount = AccountDispatcher.getAccount(AccountEnum.FetchAccountTypeEnum.FETCH_BY_CUSTOMER.getValue(), String.valueOf(customerId));
//                        String information = "您的请假申请被批准，请及时查看";
//                        String remark = "备注：审批通过";
//                        NoticeService.asyncWechatPush((CommonAccount) coachAccount.getData(), information, rest, coach, remark, NoticeService.LEAVE_APPROVAL_SUCCESS, 2);
//                    }
//
                    return WebResult.getSuccessResult();
                }
            }
            if (status.equals(0)) {
                if (coachRestService.cancelCoachRest(restId, account.getCustomerId(), userName)) {

                    //修改推送bug
//                    Integer customerId = assignCoachService.getCustomerIdByCoachId(coach.getCoachId());
//                    if (customerId != null && customerId > 0) {
//                        AccountResult coachAccount = AccountDispatcher.getAccount(AccountEnum.FetchAccountTypeEnum.FETCH_BY_CUSTOMER.getValue(), String.valueOf(customerId));
//                        String information = "您的请假申请不通过，请及时查看";
//                        String remark = "备注：审批不通过";
//                        NoticeService.asyncWechatPush((CommonAccount) coachAccount.getData(), information, rest, coach, remark, NoticeService.LEAVE_APPROVAL_SUCCESS, 2);
//                    }
//
                    return WebResult.getSuccessResult();
                }
            }

            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }
    }

    /**
     * @param startDate 开始时间
     * @param endDate   结束时间
     * @return float 持续了多少天,半天为0.5
     */

    private float getDistanceDay(Date startDate, Date endDate) {
        float time = 0;
        try {
            long startTime = startDate.getTime();
            long entTime = endDate.getTime();
            Calendar calStart = Calendar.getInstance();
            calStart.setTime(startDate);
            Calendar calEnd = Calendar.getInstance();
            calEnd.setTime(endDate);
            boolean isSameDay = calStart.get(Calendar.YEAR) == calEnd.get(Calendar.YEAR);
            isSameDay = isSameDay && calStart.get(Calendar.MONTH) == calEnd.get(Calendar.MONTH);
            isSameDay = isSameDay && calStart.get(Calendar.DAY_OF_MONTH) == calEnd.get(Calendar.DAY_OF_MONTH);
            float hours = (entTime - startTime) / (60 * 60 * 1000);//持续多少小时;
            if (isSameDay) {
                //同一天直接计算是整天还是半天
                if (hours >= 8) {
                    time = 1;
                } else {
                    time = 0.5f;
                }
            } else {
                //不是同一天需要细分
                if (hours <= 24) {
                    time = 1;
                } else {
                    int n = (int) (hours / 24);
                    int restHours = (int) (hours - n * 24);
                    if (restHours == 0) {
                        time = n;
                    } else {
                        if (restHours < 8) {
                            time = n + 0.5f;
                        } else {
                            time = n + 1;
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return time;
    }
}
