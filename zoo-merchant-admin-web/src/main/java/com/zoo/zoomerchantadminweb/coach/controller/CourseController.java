package com.zoo.zoomerchantadminweb.coach.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.zoo.activity.vo.CourseLeagueModel;
import com.zoo.common.page.PageList;
import com.zoo.activity.dao.model.*;
import com.zoo.activity.service.*;
import com.zoo.activity.vo.CourseFrequencyModel;
import com.zoo.activity.vo.CourseModel;
import com.zoo.common.util.CoachTechnologyTypeUtil;
import com.zoo.partner.service.PartnerSystemService;
import com.zoo.util.data.BeanUtilsExtends;
import com.zoo.util.data.DateUtil;
import com.zoo.web.bean.WebResult;
import com.zoo.wechat.common.Config;
import com.zoo.zoomerchantadminweb.base.BaseController;
import com.zoo.zoomerchantadminweb.context.WebUpmsContext;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.util.*;

@Api(value = "/api/server/course", tags = {"【教练系统】课程管理相关接口"}, description = "课程列表,创建,编辑,复制")
@RestController
@RequestMapping("/api/server/course")
public class CourseController extends BaseController {
    @Autowired
    private ActivityService activityService;

    @Autowired
    private CourseService courseService;

    @Autowired
    private com.zoo.skifield.service.SkiFieldService skiFieldService;

    @Autowired
    private com.zoo.activity.service.SkiFieldService znsSkiFieldService;

    @Autowired
    private PartnerSystemService partnerSystemService;

    @Autowired
    private EpisodeService episodeService;

    @Autowired
    private MailService mailService;

    @Autowired
    private CoachService coachService;
//
//    @Autowired
//    private TypographyService typographyService;

    @Autowired
    private OrdersService ordersService;

    @Autowired
    private OrganizerEnumService organizerEnumService;

    @Autowired
    private OrganizerService organizerService;

    @Autowired
    private ChargeService chargeService;

    @Autowired
    private OrderCoachExtendService orderCoachExtendService;

    @Autowired
    private NextIdService nextIdService;

    @Autowired
    private RedisService redisService;

    @Autowired
    private CoachGuideRelationshipsService coachGuideRelationshipsService;

    @Autowired
    private EpisodeExtendService episodeExtendService;


    @ApiOperation(value = "获取主办方评级表接口", notes = "获取主办方评级表", httpMethod = "GET")
    @RequestMapping(value = "/gradeMeta.json", method = RequestMethod.GET)
    public Object courseList( HttpServletRequest request, HttpSession session) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        Map<String,Object> resultMap = new HashMap<>();
        resultMap.put("clientId", organizer.getOrganizerId());
        resultMap.put("courseGradeMeta", organizer.getCourseGradeMeta());
        return WebResult.getSuccessResult("data", resultMap);
    }

    /**
     *保存主办方评价表
     * @param courseGradeMeta
     * @param request
     * @param session
     * @return
     */
    @ApiOperation(value = "保存主办方评级表接口", notes = "根据parentId 获取 chargeRules 的相关教练规则", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "courseGradeMeta", value = "提交评级表的内容", required = true, dataType = "string", paramType = "query")
    })
    @RequestMapping(value = "/saveGradeMeta.json", method = RequestMethod.POST)
    public Object saveGradeMeta(@RequestParam(value = "courseGradeMeta") String courseGradeMeta,
                                HttpServletRequest request, HttpSession session) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer != null) {
            organizer.setCourseGradeMeta(courseGradeMeta);
            if (organizerService.updateOrganizer(organizer)) {
                return WebResult.getSuccessResult("data", courseGradeMeta);
            } else {
                //保存失败
                return WebResult.getErrorResult(WebResult.Code.ERROR);
            }
        } else {
            //无法从session中获取organizer信息
            return WebResult.getErrorResult(WebResult.Code.COACH_NO_INFO_ERROR);
        }
    }

    /**
     * @param coachId 如果传了coachId 说明是通过教练获取课程,如果教练没有绑定,返回status为error
     * @param session
     * @return
     */
    @ApiOperation(value = "课程列表页-课程名称下拉框 和 教练关联课程接口", notes = " 如果不传coachId，则获取课程名称下拉框的数据， 如果传coachId 说明是获取教练关联的所有课程,如果教练失效，则返回错误信息。", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "coachId", value = "教练ID", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "type", value = "下拉框数据类型0：1   课程和课时卡 2：团课", required = false, dataType = "int", paramType = "query")
    })
    @RequestMapping(value = "/option.json", method = RequestMethod.GET)
    public Object getCourceOption(@RequestParam(value = "coachId", required = false) Integer coachId,
                                  @RequestParam(value = "type", required = false) Integer type,
                                  HttpServletRequest request, HttpSession session) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer != null && organizer.getOrganizerId() != null) {
            if (coachId == null) {
                List<Map> optionMap = courseService.getCourseOption(organizer.getOrganizerId(), type);
                return WebResult.getSuccessResult("data", optionMap);
            } else {
                // 存在coachId
                Coach coach = coachService.findCoach(coachId);
                if (coach != null) {
                    if (1 == coach.getValid()) {
                        //查询该教练是否已经绑定
//                        if (!coachService.isCoachRelation(coachId)) {
//                            //该教练未绑定
//                            data.setStatus(Status.error);
//                            data.setMsg("该教练未绑定,请先让教练在微信端进行绑定操作");
//                        } else {
                        //已绑定
                        List<Map> chooseCourseMap = courseService.getChooseCourseOption(coachId, organizer.getOrganizerId());
                        return WebResult.getSuccessResult("data", chooseCourseMap);
//                        }
                    } else {
                        return WebResult.getErrorResult(WebResult.Code.COACH_INVALID_ERROR);
                    }
                } else {
                    return WebResult.getErrorResult(WebResult.Code.COACH_NO_INFO_ERROR);
                }
            }
        } else {
            return WebResult.getErrorResult(WebResult.Code.COACH_GET_ORGANIZER_INFO_ERROR);
        }
    }

    /**
     * 获取主办方场次状态为1、2的所有课程 （typeId=8)
     * 返回结果为（episode_id: title）activity和episode组合的一个字段标题
     * @param request
     * @param session
     * @return
     */
    @ApiOperation(value = "[首页排班下单按钮]获取主办方场次状态为1(有名额)、2(已满员)的所有课程", notes = "返回结果为（episode_id: title）activity.title和episode.episodeId组合的一个字段标题)", httpMethod = "GET")
    @RequestMapping(value = "/activeOption.json", method = RequestMethod.GET)
    public Object getCourceOption(HttpServletRequest request, HttpSession session) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer != null && organizer.getOrganizerId() != null) {
            List<Map> activeCourseMap = courseService.getActiveCourseOption(organizer.getOrganizerId());
           return WebResult.getSuccessResult("data", activeCourseMap);
        } else {
            return WebResult.getErrorResult(WebResult.Code.COACH_GET_ORGANIZER_INFO_ERROR);
        }
    }

    @ApiOperation(value = "保存或编辑课程", notes = "保存或编辑课程", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "editMode", value = "编辑类型(0 创建，1 编辑，2 暂停时编辑，3 部分编辑), 后台接口默认值为0", required = false, dataType = "string", paramType = "query")
    })
    @RequestMapping(value = "/save.json", method = RequestMethod.POST)
    public Object courseSave(@RequestParam(value = "editMode", required = false, defaultValue = "0") String editMode,
                             CourseModel episodeModel,
                             HttpServletRequest request, HttpSession session) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null || organizer.getOrganizerId() == null) {
            return WebResult.getErrorResult(WebResult.Code.COACH_GET_ORGANIZER_INFO_ERROR);
        }
        OrganizerWithScenic organizerWithScenic = organizerService.getAllInfoById(organizer.getOrganizerId());
//        Skifield skifield = skiFieldService.getSkiFieldByOrganizerId(organizer.getOrganizerId());
//        if (skifield == null) {
//            return WebResult.getErrorResult(WebResult.Code.SKIFEILD_NO_BIND_ERROR);
//        }
//        ZNSSkiField znsSkiField = znsSkiFieldService.getSkiFieldById(skifield.getZnsId());

        ActivityWithBLOBs activity = new ActivityWithBLOBs();
        Episode episode = new Episode();
        Date now = new Date();
        if (null != organizer) {
            try {
                BeanUtilsExtends.copyProperties(activity, episodeModel);
                activity.setOrganizerId(organizer.getOrganizerId());
                activity.setTypeId(8);
                activity.setActivityMode(0);// 普通课程为0  课时卡为1
                activity.setUpdateTime(now);
                activity.setOperator(WebUpmsContext.getUserName(request));
                activity.setRent(0);
                activity.setRentAvailable(0);
                activity.setVisible(1);
                System.out.println("编辑模式：" + editMode);
                if ("0".equals(editMode)) {
                    activity.setCreateTime(now);
                    activity.setStatus(1);
                }

                /**
                 * 将前台的episodeModel转换成一个episode对象</br>
                 * 用于前台传入的时间参数是String类型  使用的是String，与后台Date类型不一致
                 * 导致目前的copyProperties()不能够支持，后期需要扩展copyProperties()方法
                 * */
                //新项目
                // 设置为手动核销
                episode.setVerifyFlag(1);
                episode.setVerifyCode("");
                // 邀请码设置
                episode.setInviteCode(null);
                episode.setOperator(WebUpmsContext.getUserName(request));
                episode.setActivityId(episodeModel.getActivityId());
                episode.setEpisodeId(episodeModel.getEpisodeId());
                episode.setName(episodeModel.getName());
                episode.setVoucherSend(episodeModel.getVoucherSend());

                //创建课程时，去掉了地图，地址
                episode.setProvinceId((null != organizerWithScenic.getProvince() && organizerWithScenic.getProvince() != 0 && organizerWithScenic.getProvince() != 1) ? organizerWithScenic.getProvince() : 2);
                episode.setCityId((null != organizerWithScenic.getCity() && organizerWithScenic.getCity() != 0 && organizerWithScenic.getCity() != 1) ? organizerWithScenic.getCity() : 2);
                episode.setDistrictId((null != organizerWithScenic.getDistrict() && organizerWithScenic.getDistrict() != 0 && organizerWithScenic.getDistrict() != 1) ? organizerWithScenic.getDistrict() : 4);
                episode.setLocation(organizer.getAddress());
                episode.setLatitude(organizer.getLatitude());
                episode.setLongitude(organizer.getLongitude());

                episode.setNote(episodeModel.getNote());
                episode.setPlayerMeta(episodeModel.getPlayerMeta());
                episode.setPlayerLimit(episodeModel.getPlayerLimit());
                episode.setWechatGroupUrl(episodeModel.getWechatGroupUrl());
                episode.setAdvanceBookingTime(episodeModel.getAdvanceBookingTime());
                episode.setAdvanceBookingType(episodeModel.getAdvanceBookingType());
                episode.setAdvanceRefundTime(episodeModel.getAdvanceRefundTime());
                episode.setAdvanceRefundType(episodeModel.getAdvanceRefundType());

                //不限制课程期间的处理
                if (episodeModel.getDateOfUse() == null || episodeModel.getDateOfUse() == 1) {
                    episode.setStartTime(StringUtils.isBlank(episodeModel.getStartTime()) ? now : new Date(Long.parseLong(episodeModel.getStartTime())));
                    episode.setEndTime(new Date(Long.parseLong("32503651200000")));
                } else {
                    episode.setStartTime(new Date(Long.parseLong(episodeModel.getStartTime())));
                    episode.setEndTime(new Date(Long.parseLong(episodeModel.getEndTime())));
                }

                episode.setNoticePhone(episodeModel.getNoticePhone());
                episode.setNoticeEmail(episodeModel.getNoticeEmail());
                episode.setSkiFieldId(episodeModel.getSkiFieldId());
                episode.setNeedPlayerCount(episodeModel.getNeedPlayerCount());
                episode.setNote(episodeModel.getNote());

                episode.setNeedConfirm(episodeModel.getNeedConfirm());

                //处理报名开始时间和报名截止时间
                if (StringUtils.isNotBlank(episodeModel.getRegisterStart())) {
                    episode.setRegisterStart(new Date(Long.parseLong(episodeModel.getRegisterStart())));
                } else {
                    episode.setRegisterStart(null);
                }
                episode.setRegisterDeadline(new Date(Long.parseLong(episodeModel.getRegisterDeadline())));

                //是否可取消报名（退款标记字段）
                if (StringUtils.isNotBlank(episodeModel.getRefundDeadline())) {
                    episode.setRefundDeadline(new Date(Long.parseLong(episodeModel.getRefundDeadline())));
                } else if (StringUtils.isNotBlank(episodeModel.getAdvanceRefundType())) {
                    episode.setRefundDeadline(episode.getEndTime());
                } else {
                    episode.setRefundDeadline(null);
                }

                /**
                 *  编辑模式判断
                 *  0 创建，1 编辑，2 暂停时编辑，3 部分编辑
                 */
                if ("0".equals(editMode) || "1".equals(editMode)) {
                    Date startTime = episode.getStartTime();
                    Date endTime = episode.getEndTime();
                    if (startTime != null && startTime.getTime() < now.getTime()) {
                        // 开始时间必须大于当前时间
                        return WebResult.getErrorResult(WebResult.Code.EPISODE_START_TIME_BEFORE_NOW);
                    }
                    if (endTime != null && endTime.getTime() < now.getTime()) {
                        //结束时间必须大于当前时间
                        return WebResult.getErrorResult(WebResult.Code.EPISODE_END_TIME_BEFORE_NOW);
                    }
                    if (episode.getRegisterStart() != null && episode.getRegisterStart().getTime() < now.getTime()) {
                        //报名开始时间不能小于当前时间
                        return WebResult.getErrorResult(WebResult.Code.EPISODE_REGISTER_STAR_TIME_BEFORE_NOW);
                    }
                    if (episode.getRegisterDeadline().getTime() < now.getTime()) {
                        //报名截止时间不能小于当前时间
                        return WebResult.getErrorResult(WebResult.Code.EPISODE_REGISTER_END_TIME_BEFORE_NOW);
                    }
                    if (episode.getRegisterStart() != null && episode.getRegisterStart().getTime() > episode.getRegisterDeadline().getTime()) {
                        //报名开始时间不能大于报名截止时间
                        return WebResult.getErrorResult(WebResult.Code.EPISODE_REGISTER_TIME_ERROR);
                    }
                    if (episode.getRegisterDeadline().getTime() >= episode.getEndTime().getTime()) {
                        //报名截止时间不能大于结束时间
                        return WebResult.getErrorResult(WebResult.Code.EPISODE_REGISTER_END_TIME_BEFORE_END);
                    }
                    if (startTime != null && endTime != null && startTime.getTime() > endTime.getTime()) {
                        //开始时间不能大于结束时间
                        return WebResult.getErrorResult(WebResult.Code.EPISODE_END_TIME_BEFORE_START_TIME);
                    }
                }

                if (StringUtils.isNotBlank(episodeModel.getCharges())) {
                    List<Charge> charges = JSON.parseArray(episodeModel.getCharges(), Charge.class);
                    /**
                     * 用于判断原价是否为空，若为空则置为0
                     */
                    for (Charge ch : charges) {
                        if (ch.getOriginPrice() == null) {
                            ch.setOriginPrice(new BigDecimal(0));
                        }
                    }
                    /**
                     * 保存场次信息的时候保证 至少有一种票卷
                     */
                    if (StringUtils.isBlank(episodeModel.getChargeRules()) && charges.size() == 0) {
                        //请至少添加一种课程价格
                        return WebResult.getErrorResult(WebResult.Code.COACH_NO_COURSE_PRICE_ERROR);
                    }
                    episode.setCharges(charges);
                }

                //短信模板
                if (StringUtils.isNotBlank(episodeModel.getMessages())) {
                    List<Message> messages = JSON.parseArray(episodeModel.getMessages(), Message.class);
                    episode.setMessages(messages);
                }

                //关联教练
                List<CoachRelationships> coaches = null;
                if (StringUtils.isNotBlank(episodeModel.getCoaches())) {
                    coaches = JSON.parseArray(episodeModel.getCoaches(), CoachRelationships.class);
                }

                //创建或修改DB
                if ("0".equals(editMode) && (episode.getEpisodeId() == null || episode.getEpisodeId() == 0)) {
                    /*同步代码，保证线程安全*/
                    synchronized (this) {
                        activity.setActivityId(nextIdService.getNextId("activity"));
                        episode.setEpisodeId(nextIdService.getNextId("episode"));
                        episode.setActivityId(activity.getActivityId());
                        /*创建episode的时候，将状态置为草稿*/
                        episode.setStatus(0);
                        if (courseService.addCourse(activity, episode, null, coaches)) {
                            /*当类型是教练的时候保存用户的选中规则*/
                            List<String> chargeRules = JSON.parseArray(episodeModel.getChargeRules(), String.class);
                            if (chargeRules.size() > 0) {
                                if (!episodeService.insertChargeRules(chargeRules, episode.getEpisodeId())) {
                                    return WebResult.getErrorResult(WebResult.Code.COACH_CREATE_COURSE_FAIL);
                                }
                            }
                        }
                    }
                } else {
                    Integer status = episodeService.getEpisodeStatusById(episode.getEpisodeId());
                    episode.setStatus(status == -1 ? 0 : status);
                    courseService.updateCourse(activity, episode, null, coaches);
                    //更新任务job
                    episodeService.updateTask(episode);
                    /*当类型是教练的时候保存用户的选中规则*/
                    List<String> chargeRules = JSON.parseArray(episodeModel.getChargeRules(), String.class);
                    if (chargeRules.size() > 0) {
                        if (!episodeService.updateChargeRules(chargeRules, episode.getEpisodeId())) {
                            return WebResult.getErrorResult(WebResult.Code.UPDATE_FAILED);
                        }
                    }
                }
            } catch (Exception e) {
                //logger.error(AssUtils.parse(e));
                e.printStackTrace();
                //logger.info("---------创建课程出现异常---------");
                //logger.info("---------创建课程出现异常---------", e.fillInStackTrace());
                //logger.info(e.getMessage());
                //创建课程失败
                return WebResult.getErrorResult(WebResult.Code.COACH_CREATE_COURSE_FAIL);
            }
        } else {
            //logger.info("session中获取不到organizer信息");
            //session中获取不到organizer信息
            return WebResult.getErrorResult(WebResult.Code.COACH_GET_ORGANIZER_INFO_ERROR);
        }
        //创建或更新成功
        return WebResult.getSuccessResult("data", episode.getEpisodeId());
    }

    @ApiOperation(value = "课程列表数据接口", notes = "通过筛选条件查询课程列表", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码(默认值:1)", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "size", value = "每页数量(默认值:10)", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "activityStatus", value = "课程状态(包含：正常、测试、下架)", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "activityMode", value = "产品类型（0，课程，2团课， 默认为0）", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "episodeId", value = "活动ID(episodeId)", required = false, dataType = "int", paramType = "query"),
    })
    @RequestMapping(value = "/list.json", method = RequestMethod.GET)
    public Object courseList(@RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
                             @RequestParam(value = "size", required = false, defaultValue = "10") Integer size,
                             @RequestParam(value = "activityStatus", required = false) Integer activityStatus,
                             @RequestParam(value = "episodeId", required = false) Integer episodeId,
                             @RequestParam(value = "activityMode", required = false, defaultValue = "0") Integer activityMode,
                             HttpServletRequest request, HttpSession session) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);

        if (null != organizer) {
            PageList courseList = courseService.getCourseListByOrgId(organizer.getOrganizerId(), episodeId, activityStatus, activityMode, page, size);
            return WebResult.getSuccessResult("data", courseList);
        } else {
            //session中获取不到organizer信息
            return WebResult.getErrorResult(WebResult.Code.COACH_GET_ORGANIZER_INFO_ERROR);
        }
    }

    @ApiOperation(value = "团课列表数据接口", notes = "团课列表数据接口", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码(默认值:1)", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "size", value = "每页数量(默认值:10)", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "activityStatus", value = "课程状态(包含：正常、测试、下架)", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "episodeId", value = "活动ID(episodeId)", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "startTime", value = "产品开始时间", required = false, dataType = "long", paramType = "query"),
            @ApiImplicitParam(name = "endTime", value = "产品结束时间", required = false, dataType = "long", paramType = "query")
    })
    @RequestMapping(value = "/league/list.json", method = RequestMethod.GET)
    public Object courseLeagueList(@RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
                             @RequestParam(value = "size", required = false, defaultValue = "10") Integer size,
                             @RequestParam(value = "activityStatus", required = false) Integer activityStatus,
                             @RequestParam(value = "episodeId", required = false) Integer episodeId,
                             @RequestParam(value = "startTime", required = false) Long start,
                             @RequestParam(value = "endTime", required = false) Long end,
                             HttpServletRequest request, HttpSession session) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);

        String startTime = null;
        String endTime = null;
        if (start != null) {
            startTime = DateUtil.convertDateToString(new Date(start), "yyyy-MM-dd HH:mm:ss");
        }
        if (end != null) {
            endTime = DateUtil.convertDateToString(new Date(end), "yyyy-MM-dd HH:mm:ss");
        }

        if (null != organizer) {
            PageList courseList = courseService.getCourseLeagueListByOption(organizer.getOrganizerId(), episodeId, activityStatus, startTime, endTime, page, size);
            return WebResult.getSuccessResult("data", courseList);
        } else {
            //session中获取不到organizer信息
            return WebResult.getErrorResult(WebResult.Code.COACH_GET_ORGANIZER_INFO_ERROR);
        }
    }

    @ApiOperation(value = "团课日程表数据接口", notes = "团课日程表数据接口", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "startTime", value = "产品开始时间（时间戳）", required = false, dataType = "long", paramType = "query"),
            @ApiImplicitParam(name = "endTime", value = "产品结束时间（时间戳）", required = false, dataType = "long", paramType = "query"),
            @ApiImplicitParam(name = "time", value = "某一天的时间（时间戳）", required = false, dataType = "long", paramType = "query")
    })
    @RequestMapping(value = "/league/schedule.json", method = RequestMethod.GET)
    public Object courseLeagueScheduleList(@RequestParam(value = "startTime", required = false) Long start,
                                           @RequestParam(value = "endTime", required = false) Long end,
                                           @RequestParam(value = "time", required = false) Long times,
                                           HttpServletRequest request, HttpSession session) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        String startTime = null;
        String endTime = null;
        String time = null;
        if (start != null) {
            startTime = DateUtil.convertDateToString(new Date(start), "yyyy-MM-dd");
        }
        if (end != null) {
            endTime = DateUtil.convertDateToString(new Date(end), "yyyy-MM-dd");
        }
        if (times != null) {
            time = DateUtil.convertDateToString(new Date(times), "yyyy-MM-dd");
        }

        if (null != organizer) {
            Date now = new Date();
            List<CourseLeagueCharge> chargeList =  chargeService.getCourseLeagueScheduleByOption(organizer.getOrganizerId(), startTime, endTime, time);
            for (CourseLeagueCharge leagueCharge : chargeList) {
                leagueCharge.setCourseLeagueStatus(0);
                // 过滤不显示团课
                /*if (leagueCharge.getCloudsType() == 1) {
                    *//*try {
                        Date courseStartTime = DateUtil.convertSimpleStringToDateTime(leagueCharge.getAlias() + " " + com.zoo.activity.util.DateUtil.convertDateToString(leagueCharge.getCourseStartTime(), "HH:mm:ss"));
                        if (now.before(DateUtil.addHour(courseStartTime, -1))) {
                            chargeList.remove(leagueCharge);
                            continue;
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }*//*
                    //强制成团
                    leagueCharge.setCourseLeagueStatus(1);
                }*/
                // 团课增加属性
                CoachGuideRecordView orderCoachExtend = orderCoachExtendService.getCourseLeagueAppointmentByChargeId(leagueCharge.getChargeId());
                if (orderCoachExtend != null) {
                    leagueCharge.setGuideNote(orderCoachExtend.getResponseContent());
                    leagueCharge.setAppointmentTime(orderCoachExtend.getAppointmentTime());
                    leagueCharge.setGuideId(orderCoachExtend.getGuideId());
                    leagueCharge.setExtendStatus(orderCoachExtend.getExtendStatus());

                    List<Map> relationships = coachGuideRelationshipsService.getOrderOrCoachInfoByOption(orderCoachExtend.getExtendId(), 0);
                    leagueCharge.setCoachGuideRelationships(relationships);
                    if (relationships.size() > 0) {
                        leagueCharge.setCourseLeagueStatus(2);
                    } else {
                        if (leagueCharge.getCloudsType() > 1)
                            leagueCharge.setCourseLeagueStatus(0);
                        else
                            //强制成团
                            leagueCharge.setCourseLeagueStatus(1);
                    }
                }
            }

            // 团课分组
            Map<String, List<CourseLeagueCharge>> resultMap = new HashMap<>();
            for (CourseLeagueCharge leagueCharge : chargeList) {
                List<CourseLeagueCharge> resultList = new ArrayList<>();
                if (resultMap.get(leagueCharge.getAlias()) != null) {
                    resultList = resultMap.get(leagueCharge.getAlias());
                    resultList.add(leagueCharge);
                } else {
                    resultList.add(leagueCharge);
                    resultMap.put(leagueCharge.getAlias(), resultList);
                }
            }

            return WebResult.getSuccessResult("data", resultMap);
        } else {
            //session中获取不到organizer信息
            return WebResult.getErrorResult(WebResult.Code.COACH_GET_ORGANIZER_INFO_ERROR);
        }
    }

    @ApiOperation(value = "获取团课成团但为指派教练的团课数量", notes = "获取团课成团但为指派教练的团课数量", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "startTime", value = "起始时间(2019-07-18 11:11:11)", required = false, dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "endTime", value = "结束时间(格式同上)", required = false, dataType = "String", paramType = "query")
    })
    @RequestMapping(value = "/league/clouds/notAssignCoach/count.json", method = RequestMethod.POST)
    public WebResult notAssignCoachCount4CourseLeague(@RequestParam(value = "startTime", required = false) String startTime,
                                                      @RequestParam(value = "endTime", required = false) String endTime,
                                                      HttpServletRequest request, HttpSession session) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (null == organizer) {
            //session中获取不到organizer信息
            return WebResult.getErrorResult(WebResult.Code.COACH_GET_ORGANIZER_INFO_ERROR);
        }

        if (StringUtils.isNotBlank(startTime))
            startTime = null;

        if (StringUtils.isNotBlank(endTime))
            endTime = null;

        return WebResult.getSuccessResult("data", courseService.getNotAssignCoachCount4CourseLeague(organizer.getOrganizerId(), startTime, endTime));

    }

    /**
     * 根据episodeId查询课程详情(举办者只能看到自己举办场次的详情)
     *
     * @param episodeId 场次id
     */
    @ApiOperation(value = "课程详情接口", notes = "根据episodeId查询课程详情", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "episodeId", value = "场次ID(episodeId)", required = true, dataType = "int", paramType = "query"),
    })
    @RequestMapping(value = "/detail.json", method = RequestMethod.GET)
    public Object courseDetail(@RequestParam("episodeId") Integer episodeId,
                               HttpServletRequest request, HttpSession session) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        Map<String, Object> courseDetailInfoMap = new HashMap<>();

        if (null == organizer) {
            //session中获取不到organizer信息
            return WebResult.getErrorResult(WebResult.Code.COACH_GET_ORGANIZER_INFO_ERROR);
        }

        //查询教练信息并处理
        /*Map<Integer, String> coachListMap = new HashMap<>();
        List<Coach> coachList = coachService.getAllCoachList(organizer.getOrganizerId());
        for (Coach coach : coachList) {
            coach.setRealname(coach.getRealname() + " (" + CoachTechnologyTypeUtil.boardConverterToString(coach.getBoard(), organizer.getCategory()) + ")");
            coachListMap.put(coach.getCoachId(), coach.getRealname());
        }
        courseDetailInfoMap.put("coachListMap", JSON.toJSONString(coachListMap));*/

        //B端改版创建和编辑课程时，教练不分组
        /*String coachArray = JSON.toJSONString(coachList);
        coachArray = coachArray.replace("'", "&apos;");
        courseEditInfoMap.put("coachList", coachArray);
        Map<String, CoachGroup> groupMap = new HashMap<>();
        try {
            //logger.error("------ 获取分组教练信息start -------");
            // 教练多选框，变成根据分队进行显示
            //logger.info("------ 开始查询getScheduleInfoWithoutGuide -------");
//            List coachesWithGroup = scheduleCoachService.getScheduleInfoWithoutGuide(organizer.getOrganizerId());
            //logger.info("------ 开始查询getAllCoachGroup -------");
            List<CoachGroup> groups = coachGroupService.getAllCoachGroup(organizer.getOrganizerId());
            CoachGroup noTeam = new CoachGroup();
            noTeam.setDeleted(0);
            noTeam.setGroupId(-1);
            noTeam.setName("未分组教练");
            List<ScheduleCoach> noTeamScheduleCoaches = new LinkedList<>();
            noTeam.setScheduleCoaches(noTeamScheduleCoaches);
            //logger.info("------ groupMap.put(-1, noTeam); -------");
            groupMap.put("-1", noTeam);
            //logger.info("------ groupMap.put(-1, noTeam); end -------");
            //logger.info("------ for (CoachGroup group : groups) -------");
            for (CoachGroup group : groups) {
                List<ScheduleCoach> scheduleCoaches = new LinkedList<>();
                group.setScheduleCoaches(scheduleCoaches);
                groupMap.put(group.getGroupId().toString(), group);
            }
            //logger.info("------ for (Object o : coachesWithGroup) -------");
            for (Coach one : coachList) {
                ScheduleCoach scheduleCoach = new ScheduleCoach();
                scheduleCoach.setRealname(one.getRealname());
                scheduleCoach.setCoachId(one.getCoachId());
                scheduleCoach.setGroupId(one.getGroupId());
                if (one.getGroupId() != null && groupMap.get(scheduleCoach.getGroupId().toString()) != null) {
                    groupMap.get(scheduleCoach.getGroupId().toString()).getScheduleCoaches().add(scheduleCoach);
                } else {
                    groupMap.get("-1").getScheduleCoaches().add(scheduleCoach);
                }
            }
            //logger.error("------ 获取分组教练信息end -------");
        } catch (Exception e) {
            e.printStackTrace();
            //logger.error("------ 获取分组教练信息出错 -------");
            //logger.error(e.getMessage());
        }
        courseEditInfoMap.put("coachListWithGroup", JSON.toJSONString(groupMap));*/

        // 查询课程关联的教练
        List<CoachRelationships> coaches = courseService.getCourseListOrCoachs(episodeId, 0);
        courseDetailInfoMap.put("coachRelationshipsJson", JSON.toJSONString(coaches));
        //查询课程详情detail
        Course courseDetail = episodeService.getCourseDetailWithChargesAndMessagesById(episodeId, organizer.getOrganizerId());
        courseDetailInfoMap.put("courseDetail", courseDetail);

        PartnerSystem partnerSystem = partnerSystemService.getPartnerSystemByOrgId(organizer.getOrganizerId());
        if (null != partnerSystem && partnerSystem.getInterfaceType() > 0 && courseDetail.getTypeId() == 6) {
            courseDetailInfoMap.put("partner", "true");
        } else {
            courseDetailInfoMap.put("partner", "false");
        }
        if (null != partnerSystem && partnerSystem.getInterfaceType() > 0) {
            courseDetailInfoMap.put("messagePartner", "true");
        } else {
            courseDetailInfoMap.put("messagePartner", "false");
        }
        courseDetailInfoMap.put("clientId", organizer.getOrganizerId());
        courseDetailInfoMap.put("insurance", "0");
        courseDetailInfoMap.put("insuranceNum", 0);

        return WebResult.getSuccessResult("data", courseDetailInfoMap);
    }

    @ApiOperation(value = "课程删除接口", notes = "根据episodeId删除课程", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "episodeId", value = "场次ID(episodeId)", required = true, dataType = "int", paramType = "query"),
    })
    @RequestMapping(value = "/delete.json", method = RequestMethod.POST)
    public Object courseDelete(@RequestParam("episodeId") Integer episodeId,
                               HttpServletRequest request, HttpSession session) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null || organizer.getOrganizerId() == null) {
            return WebResult.getErrorResult(WebResult.Code.COACH_GET_ORGANIZER_INFO_ERROR);
        }
        Episode episode = episodeService.getEpisodeDetailById(episodeId);
        Activity activity = activityService.getActivityById(episode.getActivityId());

        if (!organizer.getOrganizerId().equals(activity.getOrganizerId())) {
            // 没有权限
            return WebResult.getErrorResult(WebResult.Code.NO_PERMISSION);
        }

        // 当前课程或课时卡已产生订单信息,无法删除.
        Integer orderCount = ordersService.getCourseOrderCountByEpisodeId(episodeId);
        Integer frequencyOrderCount = orderCoachExtendService.getFrequencyOrderCountByEpisodeId(episodeId);
        if (episode.getPlayerCount() > 0 || (orderCount != null && orderCount > 0) || (frequencyOrderCount != null && frequencyOrderCount > 0)) {
            return WebResult.getErrorResult(WebResult.Code.COACH_COURSE_HAS_ORDER_DELETE_FAIL);
        }
        if (!activityService.deleteActivity(episode.getActivityId(), episode.getEpisodeId())) {
            return WebResult.getErrorResult(WebResult.Code.DELTED_FAILED);
        }
        //删除成功
        return WebResult.getSuccessResult();
    }

    @ApiOperation(value = "清空课程规则接口", notes = "清空课程规则接口", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "courseId", value = "场次ID(episodeId)", required = true, dataType = "int", paramType = "query"),
    })
    @RequestMapping(value = "/chargeRules/delete.json", method = RequestMethod.POST)
    public Object deleteChargeAndRules(@RequestParam(value = "courseId") Integer episodeId) {
        Episode episode = episodeService.getEpisodeDetailById(episodeId);
        if (episode != null) {
            if (episode.getStatus() <= 0) {
                episodeService.deleteChargeRules(episodeId);
                return WebResult.getSuccessResult();
            } else {
                return WebResult.getErrorResult(WebResult.Code.COACH_COURSE_EMPTY_PRICING_RULES_FAIL);
            }
        } else {
            return WebResult.getErrorResult(WebResult.Code.COACH_COURSE_NO_HAVE_ERROR);
        }
    }

    /**
     * 发布课程
     * @return
     */
    @ApiOperation(value = "发布课程接口", notes = "根据episodeId发布课程", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "episodeId", value = "场次ID(episodeId)", required = true, dataType = "int", paramType = "query"),
    })
    @RequestMapping(value = "/publish.json", method = RequestMethod.POST)
    public Object coursePublish(@RequestParam(value = "episodeId") Integer episodeId,
                                HttpServletRequest request, HttpSession session) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        Episode episode = episodeService.getEpisodeDetailById(episodeId);
        if (episode == null) {
            //网络拥堵，请联系滑雪族！(返回新系统错误码：100，系统异常)
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }

        if (!activityService.checkIsOrganizer(organizer.getOrganizerId(), episode.getActivityId())) {
            //产品主办方与登录的主办方不一致。没有权限`
            return WebResult.getErrorResult(WebResult.Code.NO_PERMISSION);
        }

        Integer coachCount = coachService.getCoachListCount(episodeId, 0);
        if (coachCount <= 0) {
            //发布失败,该课程没有添加教练
            return WebResult.getErrorResult(WebResult.Code.COURSE_NO_COACH_FAIL);
        }
        if (episode.getStatus() == 0) {
            episode.setStatus(1);
            if (episode.getRegisterStart() == null) {
                episode.setRegisterStart(new Date());
            }
            if (episodeService.saveEpisode(episode)) {
                Activity activity = activityService.getActivityById(episode.getActivityId());
                activity.setPublishTime(new Date());
                activityService.updateActivityBySelective(activity);
                episodeService.registTask(episode);
                //List<String> notifyTarget = new ArrayList<>();
                //notifyTarget.add("isnowadmin@huaxuezoo.com");
                //zooNotify.notifyByEmail(notifyTarget, "发布-滑雪族", "有新的课程发布，请前往http://" + Config.instance().getDomain() + "/manage/activity-detail?activityId=" + episode.getActivityId() + "  进行处理。");
                mailService.sendMail("isnowadmin@huaxuezoo.com", "发布-滑雪族", "有新的课程发布，请前往http://" + Config.instance().getDomain() + "/manage/activity-detail?activityId=" + episode.getActivityId() + "  进行处理。");
                // 发布成功
                return WebResult.getSuccessResult();
            } else {
                // 发布失败
                return WebResult.getErrorResult(WebResult.Code.ERROR);
            }
        } else if (episode.getStatus() == 5) {//已报名状态（暂停状态下发布，操作就是继续报名）
            if (episode.getPlayerLimit() > 0 && episode.getPlayerCount() >= episode.getPlayerLimit()) {
                // 已满员
                episode.setStatus(2);
            } else {
                // 有名额
                episode.setStatus(1);
            }

            if (episodeService.saveEpisode(episode)) {
                return WebResult.getSuccessResult();
            } else {
                // 返回系统错误
                return WebResult.getErrorResult(WebResult.Code.ERROR);
            }
        } else if (episode.getStatus() == -1) {  //状态为-1，说明是复制的课程，必须进入编辑页面晚上信息后才能发布
            return WebResult.getErrorResult(WebResult.Code.COACH_COURSE_NOT_EDIT);
        } else if (episode.getStatus() == 1) {
            /*将活动置为已发布状态*/
            //请不要重复发布！
            return WebResult.getErrorResult(WebResult.Code.EPISODE_PUBLISH_AGAIN);
        } else if (episode.getStatus() == 2) {
            return WebResult.getErrorResult(WebResult.Code.EPISODE_NOT_PLAYER_COUNT);
        } else {
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }
    }

    /**
     * 课程报名暂停
     * @return
     */
    @ApiOperation(value = "课程报名暂停接口", notes = "根据episodeId发布课程", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "episodeId", value = "场次ID(episodeId)", required = true, dataType = "int", paramType = "query"),
    })
    @RequestMapping(value = "/stopEnroll.json", method = RequestMethod.POST)
    public Object courseStopEnroll(@RequestParam(value = "episodeId") Integer episodeId,
                                   HttpSession session) {
        Episode episode = episodeService.getEpisodeDetailById(episodeId);

        if (episode.getStatus() == 1 || episode.getStatus() == 2) {
            episode.setStatus(5);
            if (episodeService.saveEpisode(episode)) {
                //暂停报名成功
                return WebResult.getSuccessResult();
            } else {
                //暂停报名失败
                return WebResult.getErrorResult(WebResult.Code.ERROR);
            }
        } else {
            // 请不要重复暂停报名！
            return WebResult.getErrorResult(WebResult.Code.EPISODE_STOP_AGAIN);
        }
    }

    /**
     * 课程报名继续  (目前继续报名和发布接口合并成一个接口)
     *
     * @return
     *//*
    @ApiOperation(value = "课程继续报名接口", notes = "课程根据episodeId继续报名", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "episodeId", value = "场次ID(episodeId)", required = true, dataType = "int", paramType = "query"),
    })
    @RequestMapping(value = "/continueEnroll.json")
    public Object courseContinueEnroll(@RequestParam(value = "episodeId") Integer episodeId,
                                       HttpSession session) {
        Episode episode = episodeService.getEpisodeDetailById(episodeId);
        if (episode.getStatus() == 5) {
            if (episode.getPlayerLimit() > 0 && episode.getPlayerCount() >= episode.getPlayerLimit()) {
                // 已满员
                episode.setStatus(2);
            } else {
                // 有名额
                episode.setStatus(1);
            }

            if (episodeService.saveEpisode(episode)) {
                return WebResult.getSuccessResult();
            } else {
                // 返回系统错误
                return WebResult.getErrorResult(WebResult.Code.ERROR);
            }
        } else {
            // 请不要重复继续报名！
            return WebResult.getErrorResult(WebResult.Code.EPISODE_PUBLISH_AGAIN);
        }
    }*/

    /**
     * 获取课程定价的生成规则
     */
    @ApiOperation(value = "获取课程定价的生成规则", notes = "获取课程定价的生成规则", httpMethod = "GET")
    @RequestMapping(value = "/charge/rules.json", method = RequestMethod.GET)
    public Object chargeRules() {
        Map chargeRulesMap = episodeService.getChargeRulesMap();
        return WebResult.getSuccessResult("data", chargeRulesMap);
    }

    /**
     * 根据课程获取所有选择的规则
     *
     * @return
     */
    @ApiOperation(value = "获取课程已选规则", notes = "根据episodeId获取课程所有选择的规则", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "episodeId", value = "场次ID(episodeId)", required = true, dataType = "int", paramType = "query"),
    })
    @RequestMapping(value = "/chargeRules/checkedRules.json", method = RequestMethod.GET)
    public Object getCheckedRules(@RequestParam(value = "episodeId") Integer episodeId) {
        return WebResult.getSuccessResult("data", episodeService.getCheckedRules(episodeId));
    }

//    //@Auth(authPath = "/assign/appointment-management")
//    @RequestMapping(value = "/course/order/list", method = RequestMethod.GET)
//    public String toCoachOrderList(HttpServletRequest request, HttpSession session, Model model) {
//        //Organizer organizer = getOrganizer(session);
//        Organizer organizer = WebUpmsContext.getOrgnization(request);
//        injectOrgInfo(session, model);
//        model.addAttribute("clientId", organizer.getOrganizerId());
//        return forwardToCustomerPage("pages/admin-course-order-list");
//    }

    /**
     * 【首页】排班下单mode中选择下拉框课程获取对应课程预约相关信息
     * @param courseId
     * @return
     */
    @ApiOperation(value = "【首页】排班下单选择下拉框课程获取对应课程预约相关信息", notes = "首页】排班下单mode中选择下拉框课程获取对应课程预约相关信息", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "courseId", value = "课程ID（episodeId）", required = true, dataType = "int", paramType = "query")
    })
    @RequestMapping(value = "/ruleInfo.json", method = RequestMethod.GET)
    public Object ruleInfo(@RequestParam(value = "courseId") Integer courseId) {
        Map checkedChargeRules = episodeService.getCheckedChargeRulesMap(courseId);
        Episode episode = episodeService.getEpisodeDetailById(courseId);
        Set keySet = checkedChargeRules.entrySet();
        Iterator i = keySet.iterator();
        int mainRulesCount = 0;
        Map dateRules = new HashMap();
        Map numberRules = new HashMap();
        Map durationRules = new HashMap();
        while (i.hasNext()) {
            if (mainRulesCount > 0) {
                Map.Entry<Object, Object> entry1 = (Map.Entry<Object, Object>) i.next();
                Map main = (Map) entry1.getValue();
                Map children = (Map) main.get("children");
                if (children.size() > 0) {
                    if (mainRulesCount == 1) {
                        dateRules = children;
                    } else if (mainRulesCount == 2) {
                        numberRules = children;
                    } else if (mainRulesCount == 3) {
                        durationRules = children;
                    }
                }
            }
            mainRulesCount++;
        }
        List<Map> chargeListMap = chargeService.getCoachChargeListMap(courseId);

        Map<String, Object> result = new HashMap<>();
        result.put("dateRules", dateRules);
        result.put("numberRules", numberRules);
        result.put("durationRules", durationRules);
        result.put("charges", chargeListMap);
        result.put("advanceType", episode.getAdvanceBookingType());
        result.put("advanceTime", episode.getAdvanceBookingTime());
        result.put("playerMeta", JSON.parseArray(episode.getPlayerMeta()));
        result.put("needPlayerCount", episode.getNeedPlayerCount());
        return WebResult.getSuccessResult("data", result);
    }

    /**
     * 更新 教学课程的 微信端显示状态 (当没有数据时，插入一条记录)
     * @param session
     * @param activityId
     * @return
     */
    @ApiOperation(value = "【课程列表】设置课程或课时卡开启对外开放按钮对应接口", notes = "设置课程或课时卡在商城产品列表页是否显示", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "courseId", value = "课程ID（episodeId）", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "status", value = "在商城产品列表页是否显示(0：显示，1: 不显示)，默认值为1", required = false, dataType = "int", paramType = "query"),
    })
    @RequestMapping(value = "/updateHidCourse.json", method = RequestMethod.POST)
    @ResponseBody
    public Object updateHidCourse(HttpSession session, HttpServletRequest request,
                                  @RequestParam(value = "activityId") Integer activityId,
                                  @RequestParam(value = "hidStatus", required = false, defaultValue = "1") Integer hidStatus) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null || organizer.getOrganizerId() == null) {
            //无法从session中获取organizer信息
            return WebResult.getErrorResult(WebResult.Code.COACH_GET_ORGANIZER_INFO_ERROR);
        }

        try {
            if (courseService.updateHidCourse(organizer.getOrganizerId(), activityId, hidStatus)) {
                return WebResult.getSuccessResult();
            } else {
                // 更新失败
                return WebResult.getErrorResult(WebResult.Code.ERROR);
            }
        } catch (Exception e) {
            //logger.error("/updateHidCourse.json 更新教练课程隐藏显示 出现异常");
            e.printStackTrace();
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }
    }


    /**
     * 根据parentId 获取 chargeRules 的相关教练规则
     * @param parentId
     * @return
     */
    @ApiOperation(value = "根据parentId获取相关教练规则", notes = "根据parentId 获取 chargeRules 的相关教练规则", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "parentId", value = "教学价格的父亲ID", required = true, dataType = "int", paramType = "query")
    })
    @RequestMapping(value = "/getChargeRulesByParent.json", method = RequestMethod.GET)
    public Object getChargeRulesByParent(@RequestParam(value = "parentId") Integer parentId) {
        try {
            List<ChargeRules> chargeRulesList = courseService.getChargeRulesByParent(parentId);
            return WebResult.getSuccessResult("data", chargeRulesList);
        } catch (Exception e) {
            e.printStackTrace();
            //获取信息出错,请尝试重新请求
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }
    }

    /**
     * 根据课时卡规则获取对应规则的课程
     * @author LiuZhiMin
     * @date 2018/7/12 上午11:28
     * @Description: TODO
     * @param
     * @return
     */
    @ApiOperation(value = "根据课时卡规则获取对应规则的课程接口", notes = "根据课时卡规则获取对应规则的课程", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "alias", value = "课时卡规则(教学人数ID-教学时长ID)", required = true, dataType = "string", paramType = "query")
    })
    @RequestMapping(value = "/courseEpisodeList.json", method = RequestMethod.GET)
    public Object courseEpisodeList(HttpServletRequest request, HttpSession session,
                                    @RequestParam(value = "alias") String alias){
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        // 所有普通课程 （不要未发布的课程）
        List<Map<String, Object>> courseEpisodeList = episodeService.getCourseInfoWithoutFrequencyAndAlias(organizer.getOrganizerId(), alias);
        return WebResult.getSuccessResult("data", courseEpisodeList);
    }

    /**
     * 创建/修改 次卡课程
     * @param session
     * @param editMode
     * @return
     */
    @ApiOperation(value = "创建或修改教练课时卡产品接口", notes = "创建或修改教练课时卡产品", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "editMode", value = "编辑类型(0 创建，1 编辑，2 暂停时编辑，3 部分编辑)", required = false, dataType = "string", paramType = "query")
    })
    @RequestMapping(value = "/frequency/save.json", method = RequestMethod.POST)
    public Object courseFrequencySave(HttpServletRequest request, HttpSession session,
                                      @RequestParam(value = "editMode", required = false, defaultValue = "0") String editMode,
                                      CourseFrequencyModel courseFrequencyModel) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null || organizer.getOrganizerId() == null) {
            return WebResult.getErrorResult(WebResult.Code.COACH_GET_ORGANIZER_INFO_ERROR);
        }

        OrganizerWithScenic organizerWithScenic = organizerService.getAllInfoById(organizer.getOrganizerId());
//        Skifield skifield = skiFieldService.getSkiFieldByOrganizerId(organizer.getOrganizerId());
//        if (skifield == null) {
//            return WebResult.getErrorResult(WebResult.Code.SKIFEILD_NO_BIND_ERROR);
//        }
//        ZNSSkiField znsSkiField = znsSkiFieldService.getSkiFieldById(skifield.getZnsId());

        ActivityWithBLOBs activity = new ActivityWithBLOBs();
        System.out.println("create   activity   info：" + JSONObject.toJSONString(activity));

        Episode episode = new Episode();

        if (organizer != null) {
            try {
                Date now = new Date();
                String operator = WebUpmsContext.getUserName(request);
                System.out.println("activity信息00000000  status：" + courseFrequencyModel.getStatus());

                BeanUtilsExtends.copyProperties(activity, courseFrequencyModel);
                System.out.println("activity信息copy status：" + activity.getStatus());


                activity.setOrganizerId(organizer.getOrganizerId());
                activity.setTypeId(8);
                activity.setActivityMode(1); // 0.普通教练课程 1.教练次卡课程
                activity.setUpdateTime(now);
                activity.setOperator(operator);
                activity.setRent(0);
                activity.setRentAvailable(0);
                activity.setVisible(1);
                if ("0".equals(editMode)) {
                    activity.setStatus(1);
                    activity.setCreateTime(now);
                }
                System.out.println("activity信息1111111：" + JSONObject.toJSONString(activity));

                // episode model set
                episode.setOperator(operator); // 记录操作人
                episode.setActivityId(courseFrequencyModel.getActivityId());
                episode.setEpisodeId(courseFrequencyModel.getEpisodeId());
                episode.setName(null);
                episode.setVoucherSend(courseFrequencyModel.getVoucherSend());

                //设置地里位置
                episode.setProvinceId((null != organizerWithScenic.getProvince() && organizerWithScenic.getProvince() != 0 && organizerWithScenic.getProvince() != 1) ? organizerWithScenic.getProvince() : 2);
                episode.setCityId((null != organizerWithScenic.getCity() && organizerWithScenic.getCity() != 0 && organizerWithScenic.getCity() != 1) ? organizerWithScenic.getCity() : 2);
                episode.setDistrictId((null != organizerWithScenic.getDistrict() && organizerWithScenic.getDistrict() != 0 && organizerWithScenic.getDistrict() != 1) ? organizerWithScenic.getDistrict() : 4);
                episode.setLocation(organizer.getAddress());
                episode.setLatitude(organizer.getLatitude());
                episode.setLongitude(organizer.getLongitude());

                episode.setNote(courseFrequencyModel.getNote());
                episode.setPlayerMeta(courseFrequencyModel.getPlayerMeta());
                episode.setPlayerLimit(courseFrequencyModel.getPlayerLimit());
                episode.setWechatGroupUrl(courseFrequencyModel.getWechatGroupUrl());
                // 设置为没有邀请码
                episode.setInviteCode(null);
                // 设置为手动核销
                episode.setVerifyFlag(1);
                episode.setVerifyCode("");

                // 不限制课程期间的处理
                if (courseFrequencyModel.getDateOfUse() == null || courseFrequencyModel.getDateOfUse() == 1) {
                    episode.setStartTime(StringUtils.isBlank(courseFrequencyModel.getStartTime()) ? now : new Date(Long.parseLong(courseFrequencyModel.getStartTime())));
                    episode.setEndTime(new Date(Long.parseLong("32503651200000")));
                } else {
                    episode.setStartTime(new Date(Long.parseLong(courseFrequencyModel.getStartTime())));
                    episode.setEndTime(new Date(Long.parseLong(courseFrequencyModel.getEndTime())));
                }

                episode.setNeedPlayerCount(courseFrequencyModel.getNeedPlayerCount());
                episode.setAdvanceBookingTime(0); // 为了能存下来AdvanceRefundTime的值，所以给AdvanceBookingTime一个默认值
                episode.setAdvanceRefundTime(courseFrequencyModel.getAdvanceRefundTime());

                if (StringUtils.isNotBlank(courseFrequencyModel.getRegisterStart())) {
                    episode.setRegisterStart(new Date(Long.parseLong(courseFrequencyModel.getRegisterStart())));
                } else {
                    episode.setRegisterStart(null);
                }
                episode.setRegisterDeadline(new Date(Long.parseLong(courseFrequencyModel.getRegisterDeadline())));

                if (StringUtils.isNotBlank(courseFrequencyModel.getRefundDeadline())) {
                    episode.setRefundDeadline(new Date(Long.parseLong(courseFrequencyModel.getRefundDeadline())));
                } else {
                    episode.setRefundDeadline(null);
                }

                /**
                 *  编辑模式判断
                 *  0 创建，1 编辑，2 暂停时编辑，3 部分编辑
                 */
                if ("0".equals(editMode) || "1".equals(editMode)) {
                    Date startTime = episode.getStartTime();
                    Date endTime = episode.getEndTime();
                    if (startTime != null && startTime.getTime() < now.getTime()) {
                        // 开始时间必须大于当前时间
                        return WebResult.getErrorResult(WebResult.Code.EPISODE_START_TIME_BEFORE_NOW);
                    }
                    if (endTime != null && endTime.getTime() < now.getTime()) {
                        //结束时间必须大于当前时间
                        return WebResult.getErrorResult(WebResult.Code.EPISODE_END_TIME_BEFORE_NOW);
                    }
                    if (episode.getRegisterStart() != null && episode.getRegisterStart().getTime() < now.getTime()) {
                        //报名开始时间不能小于当前时间
                        return WebResult.getErrorResult(WebResult.Code.EPISODE_REGISTER_STAR_TIME_BEFORE_NOW);
                    }
                    if (episode.getRegisterDeadline().getTime() < now.getTime()) {
                        //报名截止时间不能小于当前时间
                        return WebResult.getErrorResult(WebResult.Code.EPISODE_REGISTER_END_TIME_BEFORE_NOW);
                    }
                    if (episode.getRegisterStart() != null && episode.getRegisterStart().getTime() > episode.getRegisterDeadline().getTime()) {
                        //报名开始时间不能大于报名截止时间
                        return WebResult.getErrorResult(WebResult.Code.EPISODE_REGISTER_TIME_ERROR);
                    }
                    if (episode.getRegisterDeadline().getTime() >= episode.getEndTime().getTime()) {
                        //报名截止时间不能大于结束时间
                        return WebResult.getErrorResult(WebResult.Code.EPISODE_REGISTER_END_TIME_BEFORE_END);
                    }
                    if (startTime != null && endTime != null && startTime.getTime() > endTime.getTime()) {
                        //开始时间不能大于结束时间
                        return WebResult.getErrorResult(WebResult.Code.EPISODE_END_TIME_BEFORE_START_TIME);
                    }
                    if (episode.getRefundDeadline() != null && episode.getRegisterDeadline().getTime() > episode.getRefundDeadline().getTime()) {
                        //退款截止时间不能小于报名截止时间"
                        return WebResult.getErrorResult(WebResult.Code.EPISODE_END_TIME_BEFORE_REFUND_TIME);
                    }
                }

                if (StringUtils.isNotBlank(courseFrequencyModel.getCharges())) {
                    List<Charge> charges = JSON.parseArray(courseFrequencyModel.getCharges(), Charge.class);

                    /**
                     * 保存场次信息的时候保证 至少有一种票卷
                     */
                    if (StringUtils.isBlank(courseFrequencyModel.getChargeRules()) && charges.size() == 0) {
                        //请至少添加一个票劵
                        return WebResult.getErrorResult(WebResult.Code.EPISODE_CREATE_NO_CHARGE);
                    }

                    /**
                     * 用于判断原价是否为空，若为空则置为0
                     */
                    for (Charge ch : charges) {
                        // 给所有Charge的 chargemode置为3
                        ch.setChargeMode(3);
                        if (ch.getOriginPrice() == null) {
                            ch.setOriginPrice(new BigDecimal(0));
                        }
                    }
                    episode.setCharges(charges);
                }
                // 短信模板
                if (StringUtils.isNotBlank(courseFrequencyModel.getMessages())) {
                    List<Message> messages = JSON.parseArray(courseFrequencyModel.getMessages(), Message.class);
                    episode.setMessages(messages);
                }
                // 关联课程
                List<CoachRelationships> coachRelationshipsList = null;
                if (StringUtils.isNotBlank(courseFrequencyModel.getCourses())) {
                    coachRelationshipsList = JSON.parseArray(courseFrequencyModel.getCourses(), CoachRelationships.class);
                }

                System.out.println("activity信息2222222：" + JSONObject.toJSONString(activity));

                //创建和修改
                if ("0".equals(editMode) && (episode.getEpisodeId() == null || episode.getEpisodeId() == 0)) { // 创建
                    /* 同步代码，保证线程安全 */
                    synchronized (this) {
                        activity.setActivityId(nextIdService.getNextId("activity"));
                        episode.setEpisodeId(nextIdService.getNextId("episode"));
                        episode.setActivityId(activity.getActivityId());
                        /* 创建episode的时候，将状态置为草稿 */
                        episode.setStatus(0);
                        if (courseService.addCourse(activity, episode, null, coachRelationshipsList)) {
                            return WebResult.getSuccessResult();
                        } else {
                            //创建失败
                            return WebResult.getErrorResult(WebResult.Code.COACH_CREATE_FREQUENCY_FAIL);
                        }
                    }
                } else { // 修改
                    Integer status = episodeService.getEpisodeStatusById(episode.getEpisodeId());
                    episode.setStatus(status == -1 ? 0 : status);
                    courseService.updateCourse(activity, episode, null, coachRelationshipsList);
                    episodeService.updateTask(episode);

                    return WebResult.getSuccessResult();
                }
            } catch (Exception e) {
                //logger.error(AssUtils.parse(e));
                e.printStackTrace();
                //logger.info("---------创建次卡课程出现异常---------");
                //logger.info("---------创建次卡课程出现异常---------", e.fillInStackTrace());
                //logger.info(e.getMessage());
                //创建失败
                return WebResult.getErrorResult(WebResult.Code.COACH_CREATE_FREQUENCY_FAIL);
            }
        } else {
            //logger.info("session中获取不到organizer信息
            return WebResult.getErrorResult(WebResult.Code.COACH_GET_ORGANIZER_INFO_ERROR);
        }
    }

    /**
     * 根据episodeId查询次卡课程详情(举办者只能看到自己举办场次的详情)
     *
     * @param episodeId 场次id
     */
    @ApiOperation(value = "查询课时卡详情接口", notes = "根据episodeId查询次卡课程详情", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "episodeId", value = "场次ID(episodeId)", required = true, dataType = "int", paramType = "query")
    })
    @RequestMapping(value = "/frequency/detail.json", method = RequestMethod.GET)
    public Object courseFrequencyDetail(@RequestParam("episodeId") Integer episodeId,
                                        HttpServletRequest request, HttpSession session) {
        Map<String, Object> resultMap = new HashMap<>();
        Organizer organizer = WebUpmsContext.getOrgnization(request);

        if (null == organizer) {
            return WebResult.getErrorResult(WebResult.Code.COACH_GET_ORGANIZER_INFO_ERROR);
        }

        // 所有普通课程 （不要未发布的课程）
        //List<Map<String, Object>> courseEpisodeList = episodeService.getCourseInfoWithoutFrequency(organizer.getOrganizerId());
        //model.addAttribute("courseEpisodeList", JSON.toJSONString(courseEpisodeList));

        // 查询教练课时卡产品详情
        Course courseFrequency = episodeService.getCourseFrequencyDetailWithChargesAndMessagesById(episodeId, organizer.getOrganizerId());
        resultMap.put("courseFrequency", courseFrequency);

        List<Charge> chargeList = chargeService.getChargeList(courseFrequency.getEpisodeId());
        Charge charge = chargeList.size() > 0 ? chargeList.get(0) : null;
        if (charge == null) {
            // 系统出现问题，找不到对应的charge信息
            return WebResult.getErrorResult(WebResult.Code.EPISODE_CREATE_NO_CHARGE);
        }

        //查询所有符合课时卡规则的课程
        ChargeRules number = chargeService.getChargeRuleByParentAliasAndVal(Double.valueOf(charge.getTeachingNumber()), "number");
        ChargeRules duration = chargeService.getChargeRuleByParentAliasAndVal(Double.valueOf(charge.getValidRange()), "duration");
        // alias 拼接规则： 平日-1对3-2小时
        String alias = number.getRuleId() + "-" + duration.getRuleId();
        List<Map<String, Object>> courseEpisodeList = episodeService.getCourseInfoWithoutFrequencyAndAlias(organizer.getOrganizerId(), alias);
        resultMap.put("courseEpisodeList", courseEpisodeList);

        // 教练课时卡关联的基础课程
        List<CoachRelationships> courseRelationships = courseService.getCourseListOrCoachs(episodeId, 1);
        resultMap.put("courseRelationships", courseRelationships);

        return WebResult.getSuccessResult("data", resultMap);
    }

    /**
     * 创建/修改 团课
     * @param session
     * @param editMode
     * @return
     */
    @ApiOperation(value = "创建或修改团课接口", notes = "创建或修改团课接口", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "editMode", value = "编辑类型(0 创建，1 编辑，2 暂停时编辑，3 部分编辑)", required = false, dataType = "string", paramType = "query")
    })
    @RequestMapping(value = "/league/save.json", method = RequestMethod.POST)
    public Object courseLeagueSave(HttpServletRequest request, HttpSession session,
                                   @RequestParam(value = "editMode", required = false, defaultValue = "0") String editMode,
                                   CourseLeagueModel courseLeagueModel) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null || organizer.getOrganizerId() == null) {
            return WebResult.getErrorResult(WebResult.Code.COACH_GET_ORGANIZER_INFO_ERROR);
        }

        OrganizerWithScenic organizerWithScenic = organizerService.getAllInfoById(organizer.getOrganizerId());
        ActivityWithBLOBs activity = new ActivityWithBLOBs();
        Episode episode = new Episode();

        if (organizer != null) {
            try {
                Date now = new Date();
                String operator = WebUpmsContext.getUserName(request);

                BeanUtilsExtends.copyProperties(activity, courseLeagueModel);

                activity.setOrganizerId(organizer.getOrganizerId());
                activity.setTypeId(8);
                activity.setActivityMode(2);
                activity.setUpdateTime(now);
                activity.setOperator(operator);
                activity.setRent(0);
                activity.setRentAvailable(0);
                activity.setVisible(1);
                if ("0".equals(editMode)) {
                    activity.setStatus(1);
                    activity.setCreateTime(now);
                }
                System.out.println("activity信息1111111：" + JSONObject.toJSONString(activity));

                // episode model set
                episode.setOperator(operator); // 记录操作人
                episode.setActivityId(courseLeagueModel.getActivityId());
                episode.setEpisodeId(courseLeagueModel.getEpisodeId());
                episode.setName(null);
                episode.setVoucherSend(courseLeagueModel.getVoucherSend());

                //设置地里位置
                episode.setProvinceId((null != organizerWithScenic.getProvince() && organizerWithScenic.getProvince() != 0 && organizerWithScenic.getProvince() != 1) ? organizerWithScenic.getProvince() : 2);
                episode.setCityId((null != organizerWithScenic.getCity() && organizerWithScenic.getCity() != 0 && organizerWithScenic.getCity() != 1) ? organizerWithScenic.getCity() : 2);
                episode.setDistrictId((null != organizerWithScenic.getDistrict() && organizerWithScenic.getDistrict() != 0 && organizerWithScenic.getDistrict() != 1) ? organizerWithScenic.getDistrict() : 4);
                episode.setLocation(organizer.getAddress());
                episode.setLatitude(organizer.getLatitude());
                episode.setLongitude(organizer.getLongitude());

                episode.setNote(courseLeagueModel.getNote());
                episode.setPlayerMeta(courseLeagueModel.getPlayerMeta());
                episode.setWechatGroupUrl(courseLeagueModel.getWechatGroupUrl());
                // 设置为没有邀请码
                episode.setInviteCode(null);
                // 设置为手动核销
                episode.setVerifyFlag(1);
                episode.setVerifyCode("");

                episode.setUsableTime(courseLeagueModel.getUsableTime());
                episode.setPlayerLimit(0);

                // 不限制课程期间的处理
                if (courseLeagueModel.getDateOfUse() == null || courseLeagueModel.getDateOfUse() == 1) {
                    episode.setStartTime(StringUtils.isBlank(courseLeagueModel.getStartTime()) ? now : new Date(Long.parseLong(courseLeagueModel.getStartTime())));
                    episode.setEndTime(new Date(Long.parseLong("32503651200000")));
                } else {
                    episode.setStartTime(new Date(Long.parseLong(courseLeagueModel.getStartTime())));
                    episode.setEndTime(new Date(Long.parseLong(courseLeagueModel.getEndTime())));
                }

                if (StringUtils.isNotBlank(courseLeagueModel.getRegisterStart())) {
                    episode.setRegisterStart(new Date(Long.parseLong(courseLeagueModel.getRegisterStart())));
                } else {
                    episode.setRegisterStart(null);
                }
                episode.setRegisterDeadline(new Date(Long.parseLong(courseLeagueModel.getRegisterDeadline())));

                episode.setNeedPlayerCount(courseLeagueModel.getNeedPlayerCount());
                episode.setAdvanceBookingTime(0); // 为了能存下来AdvanceRefundTime的值，所以给AdvanceBookingTime一个默认值
                episode.setAdvanceBookingType(courseLeagueModel.getAdvanceBookingType());
                episode.setAdvanceRefundTime(courseLeagueModel.getAdvanceRefundTime());
                episode.setAdvanceRefundType(courseLeagueModel.getAdvanceRefundType());

                if (StringUtils.isNotBlank(courseLeagueModel.getRefundDeadline())) {
                    episode.setRefundDeadline(new Date(Long.parseLong(courseLeagueModel.getRefundDeadline())));
                } else {
                    episode.setRefundDeadline(episode.getEndTime());
                }

                /**
                 *  编辑模式判断
                 *  0 创建，1 编辑，2 暂停时编辑，3 部分编辑
                 */
                if ("0".equals(editMode) || "1".equals(editMode)) {
                    Date startTime = episode.getStartTime();
                    Date endTime = episode.getEndTime();
                    if (startTime != null && startTime.getTime() < now.getTime()) {
                        // 开始时间必须大于当前时间
                        return WebResult.getErrorResult(WebResult.Code.EPISODE_START_TIME_BEFORE_NOW);
                    }
                    if (endTime != null && endTime.getTime() < now.getTime()) {
                        //结束时间必须大于当前时间
                        return WebResult.getErrorResult(WebResult.Code.EPISODE_END_TIME_BEFORE_NOW);
                    }
                    if (startTime != null && endTime != null && startTime.getTime() > endTime.getTime()) {
                        //开始时间不能大于结束时间
                        return WebResult.getErrorResult(WebResult.Code.EPISODE_END_TIME_BEFORE_START_TIME);
                    }
                    if (episode.getRegisterStart() != null && episode.getRegisterStart().getTime() < now.getTime()) {
                        //报名开始时间不能小于当前时间
                        return WebResult.getErrorResult(WebResult.Code.EPISODE_REGISTER_STAR_TIME_BEFORE_NOW);
                    }
                    if (episode.getRegisterDeadline().getTime() < now.getTime()) {
                        //报名截止时间不能小于当前时间
                        return WebResult.getErrorResult(WebResult.Code.EPISODE_REGISTER_END_TIME_BEFORE_NOW);
                    }
                    if (episode.getRegisterStart() != null && episode.getRegisterStart().getTime() > episode.getRegisterDeadline().getTime()) {
                        //报名开始时间不能大于报名截止时间
                        return WebResult.getErrorResult(WebResult.Code.EPISODE_REGISTER_TIME_ERROR);
                    }
                    if (episode.getRegisterDeadline().getTime() >= episode.getEndTime().getTime()) {
                        //报名截止时间不能大于结束时间
                        return WebResult.getErrorResult(WebResult.Code.EPISODE_REGISTER_END_TIME_BEFORE_END);
                    }
                    if (episode.getRefundDeadline() != null && episode.getRegisterDeadline().getTime() > episode.getRefundDeadline().getTime()) {
                        //退款截止时间不能小于报名截止时间"
                        return WebResult.getErrorResult(WebResult.Code.EPISODE_END_TIME_BEFORE_REFUND_TIME);
                    }
                }

                if (courseLeagueModel.getMinNumber() > courseLeagueModel.getMaxNumber()) {
                    return WebResult.getErrorResult(WebResult.Code.COACH_COURSE_LEAGUE_OPEN_CLOUDS_NUMBER_ERROR);
                }

                EpisodeExtend extend = null;
                if ("0".equals(editMode)) {
                    extend = new EpisodeExtend();
                    extend.setCreateTime(now);
                    extend.setType(0);
                } else {
                    EpisodeExtendExample extendExample = new EpisodeExtendExample();
                    extendExample.createCriteria().andEpisodeIdEqualTo(episode.getEpisodeId()).andTypeEqualTo(0);
                    extend = episodeExtendService.selectFirstByExample(extendExample);
                    extend.setUpdateTime(now);
                }
                extend.setStartTime(new Date(Long.parseLong(courseLeagueModel.getCourseStartTime())));
                extend.setEndTime(new Date(Long.parseLong(courseLeagueModel.getCourseEndTime())));
                extend.setMaxNumber(courseLeagueModel.getMaxNumber());
                extend.setMinNumber(courseLeagueModel.getMinNumber());
                extend.setPrice(courseLeagueModel.getPrice());

                Long endTime = Long.parseLong(courseLeagueModel.getCourseEndTime());
                Long startTime = Long.valueOf(courseLeagueModel.getCourseStartTime());
                BigDecimal divisor = new BigDecimal(endTime - startTime);
                BigDecimal dividend = new BigDecimal(60 * 60 * 1000);
                Double duration = divisor.divide(dividend, 1, BigDecimal.ROUND_HALF_UP).doubleValue();
                extend.setDuration(duration);

                // 短信模板
                if (StringUtils.isNotBlank(courseLeagueModel.getMessages())) {
                    List<Message> messages = JSON.parseArray(courseLeagueModel.getMessages(), Message.class);
                    episode.setMessages(messages);
                }
                // coach
                List<CoachRelationships> coachRelationshipsList = null;
                if (StringUtils.isNotBlank(courseLeagueModel.getCoaches())) {
                    coachRelationshipsList = JSON.parseArray(courseLeagueModel.getCoaches(), CoachRelationships.class);
                    for (CoachRelationships coachRelationships : coachRelationshipsList) {
                        coachRelationships.setType(2);
                    }
                }

                //创建和修改
                if ("0".equals(editMode) && (episode.getEpisodeId() == null || episode.getEpisodeId() == 0)) { // 创建
                    /* 同步代码，保证线程安全 */
                    synchronized (this) {
                        activity.setActivityId(nextIdService.getNextId("activity"));
                        episode.setEpisodeId(nextIdService.getNextId("episode"));
                        episode.setActivityId(activity.getActivityId());
                        /* 创建episode的时候，将状态置为草稿 */
                        episode.setStatus(0);
                        extend.setEpisodeId(episode.getEpisodeId());
                        if (courseService.addCourse(activity, episode, extend, coachRelationshipsList)) {
                            return WebResult.getSuccessResult();
                        } else {
                            //创建失败
                            return WebResult.getErrorResult(WebResult.Code.COACH_COURSE_LEAGUE_CREATE_FAIL);
                        }
                    }
                } else { // 修改
                    Integer status = episodeService.getEpisodeStatusById(episode.getEpisodeId());
                    episode.setStatus(status == -1 ? 0 : status);
                    courseService.updateCourse(activity, episode, extend, coachRelationshipsList);
                    episodeService.updateTask(episode);

                    return WebResult.getSuccessResult();
                }
            } catch (Exception e) {
                e.printStackTrace();
                //创建失败
                return WebResult.getErrorResult(WebResult.Code.COACH_COURSE_LEAGUE_CREATE_FAIL);
            }
        } else {
            return WebResult.getErrorResult(WebResult.Code.COACH_GET_ORGANIZER_INFO_ERROR);
        }
    }

    /**
     * @param episodeId 场次id
     */
    @ApiOperation(value = "查询团课详情接口", notes = "查询团课详情接口", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "episodeId", value = "场次ID(episodeId)", required = true, dataType = "int", paramType = "query")
    })
    @RequestMapping(value = "/league/detail.json", method = RequestMethod.GET)
    public Object courseLeagueDetail(@RequestParam("episodeId") Integer episodeId,
                                        HttpServletRequest request, HttpSession session) {
        Map<String, Object> resultMap = new HashMap<>();
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (null == organizer) {
            return WebResult.getErrorResult(WebResult.Code.COACH_GET_ORGANIZER_INFO_ERROR);
        }

        Course leagueCourse = episodeService.getLeagueCourseDetailById(organizer.getOrganizerId(), episodeId);
        List<CoachRelationships> coaches = courseService.getCourseListOrCoachs(episodeId, 2);

        resultMap.put("coaches", coaches);
        resultMap.put("course", leagueCourse);
        return WebResult.getSuccessResult("data", resultMap);
    }


    @ApiOperation(value = "发布团课接口", notes = "根据episodeId发布团课", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "episodeId", value = "场次ID(episodeId)", required = true, dataType = "int", paramType = "query"),
    })
    @RequestMapping(value = "/league/publish.json", method = RequestMethod.POST)
    public Object leagueCoursePublish(@RequestParam(value = "episodeId") Integer episodeId,
                                HttpServletRequest request, HttpSession session) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        Episode episode = episodeService.getEpisodeDetailById(episodeId);
        if (episode == null) {
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }

        if (!activityService.checkIsOrganizer(organizer.getOrganizerId(), episode.getActivityId())) {
            return WebResult.getErrorResult(WebResult.Code.NO_PERMISSION);
        }

        if (episode.getStatus() == 0) {
            episode.setStatus(1);
            if (episode.getRegisterStart() == null) {
                episode.setRegisterStart(new Date());
            }
            if (episodeService.saveEpisode(episode)) {
                Activity activity = activityService.getActivityById(episode.getActivityId());
                activity.setPublishTime(new Date());
                activityService.updateActivityBySelective(activity);
                episodeService.registTask(episode);
                //List<String> notifyTarget = new ArrayList<>();
                //notifyTarget.add("isnowadmin@huaxuezoo.com");
                //zooNotify.notifyByEmail(notifyTarget, "发布-滑雪族", "有新的课程发布，请前往http://" + Config.instance().getDomain() + "/manage/activity-detail?activityId=" + episode.getActivityId() + "  进行处理。");
                mailService.sendMail("isnowadmin@huaxuezoo.com", "发布-滑雪族", "有新的课程发布，请前往http://" + Config.instance().getDomain() + "/manage/activity-detail?activityId=" + episode.getActivityId() + "  进行处理。");
                // 发布成功
                return WebResult.getSuccessResult();
            } else {
                // 发布失败
                return WebResult.getErrorResult(WebResult.Code.ERROR);
            }
        } else if (episode.getStatus() == 5) {//已报名状态（暂停状态下发布，操作就是继续报名）
            if (episode.getPlayerLimit() > 0 && episode.getPlayerCount() >= episode.getPlayerLimit()) {
                // 已满员
                episode.setStatus(2);
            } else {
                // 有名额
                episode.setStatus(1);
            }

            if (episodeService.saveEpisode(episode)) {
                return WebResult.getSuccessResult();
            } else {
                // 返回系统错误
                return WebResult.getErrorResult(WebResult.Code.ERROR);
            }
        } else if (episode.getStatus() == -1) {  //状态为-1，说明是复制的课程，必须进入编辑页面晚上信息后才能发布
            return WebResult.getErrorResult(WebResult.Code.COACH_COURSE_NOT_EDIT);
        }  else if (episode.getStatus() == 1) {
            /*将活动置为已发布状态*/
            //请不要重复发布！
            return WebResult.getErrorResult(WebResult.Code.EPISODE_PUBLISH_AGAIN);
        } else if (episode.getStatus() == 2) {
            return WebResult.getErrorResult(WebResult.Code.EPISODE_NOT_PLAYER_COUNT);
        } else {
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }
    }

    /**
     * 发布次卡课程 产品
     * @return
     */
    @ApiOperation(value = "课时卡产品发布接口", notes = "根据episodeId发布", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "episodeId", value = "场次ID(episodeId)", required = true, dataType = "int", paramType = "query")
    })
    @RequestMapping(value = "/frequency/publish.json", method = RequestMethod.POST)
    public Object courseFrequencyPublish(@RequestParam(value = "episodeId") Integer episodeId,
                                         HttpSession session) {
        Episode episode = episodeService.getEpisodeDetailById(episodeId);
        if (episode == null) {
            //网络拥堵，请联系滑雪族！(返回新系统错误码：100，系统异常)
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }
        // 获得次卡产品绑定的课程个数
        Integer courseCount = coachService.getCoachListCount(episodeId, 1);
        if (courseCount <= 0) {
            //发布失败、该次卡课程没有绑定任何课程
            return WebResult.getErrorResult(WebResult.Code.COACH_FREQUENCY_NO_COURSE_FAIL);
        }
        if (episode.getStatus() == 0) {
            episode.setStatus(1);
            if (episode.getRegisterStart() == null) {
                episode.setRegisterStart(new Date());
            }
            if (episodeService.saveEpisode(episode)) {
                Activity activity = activityService.getActivityById(episode.getActivityId());
                activity.setPublishTime(new Date());
                activityService.updateActivityBySelective(activity);
                episodeService.registTask(episode);
//                List<String> notifyTarget = new ArrayList<>();
//                notifyTarget.add("isnowadmin@huaxuezoo.com");
                mailService.sendMail("isnowadmin@huaxuezoo.com", "发布-滑雪族", "有新的次卡课程发布，请前往http://" + Config.instance().getDomain() + "/manage/activity-detail?activityId=" + episode.getActivityId() + "  进行处理。");
                return WebResult.getSuccessResult();
            } else {
                //发布失败
                return WebResult.getErrorResult(WebResult.Code.ERROR);
            }
        } else if (episode.getStatus() == 5) {//已报名状态（暂停状态下发布，操作就是继续报名）
            if (episode.getPlayerLimit() > 0 && episode.getPlayerCount() >= episode.getPlayerLimit()) {
                // 已满员
                episode.setStatus(2);
            } else {
                // 有名额
                episode.setStatus(1);
            }

            if (episodeService.saveEpisode(episode)) {
                return WebResult.getSuccessResult();
            } else {
                // 返回系统错误
                return WebResult.getErrorResult(WebResult.Code.ERROR);
            }
        } else if (episode.getStatus() == -1) {  //状态为-1，说明是复制的课程，必须进入编辑页面晚上信息后才能发布
            return WebResult.getErrorResult(WebResult.Code.COACH_COURSE_NOT_EDIT);
        }  else if (episode.getStatus() == 1) {
            /*将活动置为已发布状态*/
            //请不要重复发布！
            return WebResult.getErrorResult(WebResult.Code.EPISODE_PUBLISH_AGAIN);
        } else if (episode.getStatus() == 2) {
            return WebResult.getErrorResult(WebResult.Code.EPISODE_NOT_PLAYER_COUNT);
        } else {
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }
    }

    /**
     * 获取可用课程 或 次卡产品下拉框
     * @param activityMode 0.普通课程 1.课时卡
     * @return
     */
    @ApiOperation(value = "获取可用课程 或 课时卡产品下拉框数据接口", notes = "获取可用课程 或 课时卡产品下拉框", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "activityMode", value = "是否为课时卡(0不是,1是)", required = false, dataType = "int", paramType = "query")
    })
    @RequestMapping(value = "/getCourseListOption.json", method = RequestMethod.GET)
    public Object getCourseListOption(HttpServletRequest request, HttpSession session,
                                      @RequestParam(value = "activityMode", required = false) Integer activityMode) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer != null && organizer.getOrganizerId() != null) {
            List<Map> map = courseService.getCourseOptionByActivityMode(organizer.getOrganizerId(), activityMode);
            return WebResult.getSuccessResult("data", map);
        } else {
            // session中获取不到organizer信息
            return WebResult.getErrorResult(WebResult.Code.COACH_GET_ORGANIZER_INFO_ERROR);
        }
    }

    /**
     * 根据episode获取charge信息list
     * @param session
     * @param episodeId
     * @return
     */
    @ApiOperation(value = "根据episodeId获取所有票信息接口", notes = "根据episodeId获取所有定价规则的票劵信息", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "episodeId", value = "场次ID(episodeId)", required = true, dataType = "int", paramType = "query")
    })
    @RequestMapping(value = "/getChargesByEpisodeId.json", method = RequestMethod.GET)
    public Object getChargesByEpisodeId(HttpSession session,
                                        @RequestParam(value = "episodeId", required = true) Integer episodeId) {
        try {
            List<Charge> chargeList = chargeService.getChargeList(episodeId);
            return WebResult.getSuccessResult("data", chargeList);
        } catch (Exception e) {
            e.printStackTrace();
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }
    }

    /**
     * 获取有效教练 - 用来绑定课程
     * @param session
     * @return
     */
    @ApiOperation(value = "[课程绑定教练]获取可绑课程所有教练信息接口", notes = "获取可绑课程所有教练信息", httpMethod = "GET")
    @RequestMapping(value = "/coach/list.json", method = RequestMethod.GET)
    public Object getValidCoachListByOrganizer(HttpServletRequest request, HttpSession session) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null) {
            // session中获取不到organizer信息
            return WebResult.getErrorResult(WebResult.Code.COACH_GET_ORGANIZER_INFO_ERROR);
        }

        List<Coach> coachList = coachService.getAllCoachList(organizer.getOrganizerId());

        OrganizerEnumExample example = new OrganizerEnumExample();
        example.createCriteria().andOrganizerIdEqualTo(organizer.getOrganizerId()).andSystemEqualTo(1)
                .andValidEqualTo(1).andTypeEqualTo(1).andIsLabelEqualTo(0);
        List<OrganizerEnum> technicalTypeList = organizerEnumService.selectByExample(example);
        Map technicalTypeMap = new HashMap();
        for (OrganizerEnum en : technicalTypeList) {
            technicalTypeMap.put(en.getValue(), en.getName());
        }

        for (Coach coach : coachList) {
            coach.setRealname(coach.getRealname() + " (" + technicalTypeMap.get(coach.getBoard()) + ")");
        }
        return WebResult.getSuccessResult("data", coachList);
    }

    /**
     * 课程或课时卡复制接口
     * @param copyEpisodeId
     * @param startTime
     * @param endTime
     * @param registerStart
     * @param registerDeadline
     * @param courseType
     * @param request
     * @param session
     * @return
     */
    @ApiOperation(value = "课程或课时卡复制接口", notes = "目前只有指定教练预约支持复制功能（typeId=8）", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "copyEpisodeId", value = "被复制的场次ID", required = true, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "startTime", value = "开始时间", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "endTime", value = "结束时间", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "registerStart", value = "报名开始时间", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "registerDeadline", value = "报名结束时间", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "courseType", value = "课程类型(0:课程, 1:课时卡, 2:团课)", required = false, dataType = "int", paramType = "query"),
    })
    @RequestMapping(value = "/copy.json", method = RequestMethod.POST)
    public Object episodeCopy(@RequestParam(value = "copyEpisodeId", required = true) Integer copyEpisodeId,
                              @RequestParam(value = "startTime", required = false) String startTime,
                              @RequestParam(value = "endTime", required = false) String endTime,
                              @RequestParam(value = "registerStart", required = false) String registerStart,
                              @RequestParam(value = "registerDeadline", required = true) String registerDeadline,
                              @RequestParam(value = "courseType", required = false, defaultValue = "0") Integer courseType,
                              HttpServletRequest request, HttpSession session) {
        // TODO 校验时间
        if (startTime == null || startTime.length() == 0) {
            return WebResult.getErrorResult(WebResult.Code.EPISODE_NEED_START_TIME);
        }
        if (endTime == null || endTime.length() == 0) {
            return WebResult.getErrorResult(WebResult.Code.EPISODE_NEED_END_TIME);
        }
        if (registerDeadline == null || registerDeadline.length() == 0) {
            return WebResult.getErrorResult(WebResult.Code.EPISODE_NEED_REGISTER_END_TIME);
        }

        List<Charge> copyChargeList = null;
        List<String> checkedChargeRules = null;
        // 获取需要复制的activity episode  charge信息
        EpisodeExtend episodeExtend = null;
        Episode copyEpiosde = episodeService.getEpisodeDetailById(copyEpisodeId);
        ActivityWithBLOBs copyActivity = activityService.getActivityById(copyEpiosde.getActivityId());
        if (courseType != 2) {
            copyChargeList = chargeService.getChargeList(copyEpisodeId);
            if (courseType == 0) {
                checkedChargeRules = episodeService.getCheckedRules(copyEpisodeId);
            }
        } else {
            EpisodeExtendExample extendExample = new EpisodeExtendExample();
            extendExample.createCriteria().andEpisodeIdEqualTo(copyEpisodeId);
            List<EpisodeExtend> episodeExtendList = episodeService.getEpisodeExtendDetailByEpisodeId(extendExample);
            if (episodeExtendList.size() != 1)
                return WebResult.getErrorResult(WebResult.Code.EPISODE_EXTEND_COUNT_EXCEPTION);
            else
                episodeExtend = episodeExtendList.get(0);
        }

        if (copyEpiosde == null) {
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }

        // 教练只有typeId = 8的产品支持复制功能
        if (copyActivity.getTypeId() != 8) {
            return WebResult.getErrorResult(WebResult.Code.COURSE_ACTIVITY_TYPE_ERROR);
        }

        // TODO 开始真正的复制操作
        Date now = new Date();
        Integer newEpisodeId = episodeService.getNextEpisodeId();
        Integer newActivityId = activityService.getNextActivityId();

        // 重置activity信息
        ActivityWithBLOBs newActivity = copyActivity;
        newActivity.setCreateTime(now);
        newActivity.setUpdateTime(now);
        newActivity.setActivityId(newActivityId);
        newActivity.setRent(0);
        newActivity.setRentAvailable(0);
        newActivity.setVisible(1);
        newActivity.setStatus(1);

        if (courseType != 2) {
            // 重置charge信息
            ListIterator<Charge> chargeListIterator = copyChargeList.listIterator();
            while (chargeListIterator.hasNext()) {
                Charge charge = chargeListIterator.next();
                charge.setChargeCount(0);
                charge.setChargeId(null);
                charge.setEpisodeId(newEpisodeId);
                charge.setActivityId(newActivityId);
            }
        }

        // 重置episode信息
        Episode newEpisode = copyEpiosde;
        newEpisode.setEpisodeId(newEpisodeId);
        newEpisode.setActivityId(newActivityId);
        newEpisode.setShare(0);
        newEpisode.setHint(0);
        newEpisode.setPlayerCount(0);
        newEpisode.setCharges(copyChargeList);

        //设置产品期间
        newEpisode.setStartTime(new Date(Long.parseLong(startTime)));
        newEpisode.setEndTime(new Date(Long.parseLong(endTime)));

        //设置报名时间
        if (StringUtils.isNotBlank(registerStart)) {
            newEpisode.setRegisterStart(new Date(Long.parseLong(registerStart)));
        } else {
            newEpisode.setRegisterStart(null);
        }
        newEpisode.setRegisterDeadline(new Date(Long.parseLong(registerDeadline)));

        //设置退款时间
        if (copyEpiosde.getRefundDeadline() != null) {
            newEpisode.setRefundDeadline(newEpisode.getRefundDeadline());
        } else {
            newEpisode.setRefundDeadline(null);
        }

        // 设置操作人和状态
        newEpisode.setOperator(WebUpmsContext.getUserName(request));
        newEpisode.setStatus(-1);// 复制状态，必须对产品进行修改才能发布

        List<CoachRelationships> coachRelationshipsList = new ArrayList<>();
        coachRelationshipsList = courseService.getCourseListOrCoachs(copyEpisodeId, courseType);
        for (CoachRelationships coachRelationships : coachRelationshipsList) {
            coachRelationships.setMapId(null);
        }

        if (courseType == 2) {
            //重置extend
            episodeExtend.setExtendId(null);
            episodeExtend.setEpisodeId(newEpisodeId);
            episodeExtend.setCreateTime(now);
            episodeExtend.setUpdateTime(now);
            episodeExtend.setType(0);
        }

        // TODO 执行复制功能
        try {
            if (courseService.addCourse(newActivity, newEpisode, episodeExtend, coachRelationshipsList)) {
                if (courseType == 0) { // 复制教练课程要插入对应定价规则
                    if (!episodeService.insertChargeRules(checkedChargeRules, newEpisodeId)) {
                        return WebResult.getErrorResult(WebResult.Code.COURSE_OR_FREQUENCY_COPY_FAIL);
                    }
                }
            } else {
                //复制异常
                return WebResult.getErrorResult(WebResult.Code.COURSE_OR_FREQUENCY_COPY_FAIL);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }

        // 复制成功
        return WebResult.getSuccessResult("data", newEpisodeId);
    }

    @RequestMapping(value = "/refresh.json", method = RequestMethod.POST)
    public Object updateRender(HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer != null) {
            redisService.refreshRedisData("ProductInfo", organizer.getOrganizerId());
            return WebResult.getSuccessResult();
        } else {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
    }
}
