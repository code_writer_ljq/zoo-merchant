package com.zoo.zoomerchantadminweb.base;

import com.zoo.activity.util.AssUtils;
import com.zoo.activity.util.DateUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.util.Date;

public abstract class BaseController {
    protected static final String ENCODE_KEY = "QWERTYUIOP";
    protected static final String ENCODE_CODE_KEY = "ABCDEFG";

    protected void injectResponse(HttpServletResponse response, String info) throws Exception {
        response.setContentType("text/html;charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        response.setHeader("Cache-Control", "no-cache");
        PrintWriter pw = response.getWriter();
        pw.write(info);
        pw.flush();
        pw.close();
    }

    protected File generateDataToExcelFile(HSSFWorkbook wb, String baseFilePath) {
        File file = null;
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        try {
            wb.write(os);

            byte[] content = os.toByteArray();
            file = new File(baseFilePath);//Excel文件生成后存储的位置。
            if (!file.exists()) {
                file.mkdirs();
            }
            file = new File(baseFilePath + File.separator + 12 + "-" + DateUtil.convertDateToString(new Date()) + ".xls");
            OutputStream fos;
            fos = new FileOutputStream(file);
            fos.write(content);
            os.close();
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return file;
    }

    protected void downLoadFile(String filePath, HttpServletResponse response, String fileTitle) throws Exception {
        File f = null;
        BufferedInputStream in = null;
        BufferedOutputStream out = null;
        if (StringUtils.isBlank(filePath)) {
            injectResponse(response, "请检查有无信息或者稍后在尝试");
        } else {
            String fileName = new String(fileTitle.getBytes("UTF-8"), "iso-8859-1");
            try {
                f = new File(filePath);
                response.setContentType("application/octet-stream;charset=utf-8");
                response.setCharacterEncoding("UTF-8");
                response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
                response.setHeader("Content-Length", String.valueOf(f.length()));
                in = new BufferedInputStream(new FileInputStream(f));
                out = new BufferedOutputStream(response.getOutputStream());
                byte[] data = new byte[1024];
                int len;
                while (-1 != (len = in.read(data, 0, data.length))) {
                    out.write(data, 0, len);
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (in != null) {
                    in.close();
                }
                if (out != null) {
                    out.close();
                }
                if (f != null) {
                /*删除生成的excel文件*/
                    f.delete();
                }
            }
        }
    }

    /**
     * 从资源服务器去下载文件
     *
     * @param filePath
     * @param response
     * @param fileTitle
     * @throws Exception
     */
    protected void newDownLoadFile(String filePath, HttpServletResponse response, String fileTitle, String baseFilePath) throws Exception {
        // 首先下载资源服务器上的文件
        int bytesum = 0;
        int byteread = 0;
        URL url = new URL(filePath);

        InputStream inStream = null;
        FileOutputStream fs = null;
        String baseStr = baseFilePath + File.separator + DateUtil.convertDateToString(new Date()) + ".xls";
        try {
            URLConnection conn = url.openConnection();
            inStream = conn.getInputStream();
            fs = new FileOutputStream(baseStr);

            byte[] buffer = new byte[1204];
            //int length;
            while ((byteread = inStream.read(buffer)) != -1) {
                bytesum += byteread;
                //System.out.println(bytesum);
                fs.write(buffer, 0, byteread);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (inStream != null) {
                inStream.close();
            }
            if (fs != null) {
                fs.close();
            }
        }

        //其次将下载到本地的文件通过resopnse返回给用户
        downLoadFile(baseStr, response, fileTitle);
    }

    protected Date calculateBefore3MonthDate (Date date) {
        // 默认导出3个月以前的数据
        Date now = new Date();
        Date calculateDate = now;
        if (date == null) {
            calculateDate = DateUtil.addMonth(now, -3);
        } else {
            calculateDate = DateUtil.addMonth(date, 3);
            if (calculateDate.before(now)) {
                calculateDate = DateUtil.addMonth(now, -3);
            } else {
                calculateDate = date;
            }
        }
        return calculateDate;
    }
}
