package com.zoo.zoomerchantadminweb.statistics.controller;

import com.zoo.activity.dao.model.Organizer;
import com.zoo.activity.service.ActivityService;
import com.zoo.activity.service.OrdersService;
import com.zoo.web.bean.WebResult;
import com.zoo.zoomerchantadminweb.context.WebUpmsContext;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@Api(value = "api/server/statistics/user", tags = {"主办方用户分析接口"}, description = "主办方用户分析接口描述")
@RestController
@RequestMapping("api/server/statistics/user")
public class UserDataController {

    @Autowired
    private ActivityService activityService;

    @Autowired
    private OrdersService ordersService;

    @ApiOperation(value = "基础数据", notes = "基础数据", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "startTime", value = "开始时间", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "endTime", value = "结束时间", required = true, dataType = "string", paramType = "query")
    })
    @RequestMapping(value = "/base.json", method = RequestMethod.POST)
    public WebResult baseData(@RequestParam("startTime") String startTime, @RequestParam("endTime") String endTime, HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        return WebResult.getSuccessResult("data", activityService.getUserBaseData(startTime, endTime, organizer.getOrganizerId()));
    }

    @ApiOperation(value = "用户性别", notes = "用户性别", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "startTime", value = "开始时间", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "endTime", value = "结束时间", required = true, dataType = "string", paramType = "query")
    })
    @RequestMapping(value = "/gender.json", method = RequestMethod.POST)
    public WebResult genderStatistics(@RequestParam("startTime") String startTime, @RequestParam("endTime") String endTime, HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        return WebResult.getSuccessResult("data", activityService.getGenderInfoByOrganizerId(organizer.getOrganizerId(), startTime, endTime));
    }

    @ApiOperation(value = "用户年龄", notes = "用户年龄", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "startTime", value = "开始时间", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "endTime", value = "结束时间", required = true, dataType = "string", paramType = "query")
    })
    @RequestMapping(value = "/age.json", method = RequestMethod.POST)
    public WebResult ageStatistics(@RequestParam("startTime") String startTime, @RequestParam("endTime") String endTime, HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        return WebResult.getSuccessResult("data", activityService.getAgeInfoByOrganizerId(organizer.getOrganizerId(), startTime, endTime));
    }

    @ApiOperation(value = "用户地域", notes = "用户地域", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "startTime", value = "开始时间", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "endTime", value = "结束时间", required = true, dataType = "string", paramType = "query")
    })
    @RequestMapping(value = "/location.json", method = RequestMethod.POST)
    public WebResult locationStatistics(@RequestParam("startTime") String startTime, @RequestParam("endTime") String endTime, HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        return WebResult.getSuccessResult("data", activityService.locationStatistics(startTime, endTime, organizer.getOrganizerId()));
    }

    @ApiOperation(value = "访问时间", notes = "访问时间", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "startTime", value = "开始时间", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "endTime", value = "结束时间", required = true, dataType = "string", paramType = "query")
    })
    @RequestMapping(value = "/access.json", method = RequestMethod.POST)
    public WebResult accessStatistics(@RequestParam("startTime") String startTime, @RequestParam("endTime") String endTime, HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        return WebResult.getSuccessResult("data", activityService.accessTimeStatistics(startTime, endTime, organizer.getOrganizerId()));
    }

    @ApiOperation(value = "分享种类", notes = "分享种类", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "startTime", value = "开始时间", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "endTime", value = "结束时间", required = true, dataType = "string", paramType = "query")
    })
    @RequestMapping(value = "/shareType.json", method = RequestMethod.POST)
    public WebResult shareTypeStatistics(@RequestParam("startTime") String startTime, @RequestParam("endTime") String endTime, HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        return WebResult.getSuccessResult("data", activityService.shareTypeStatistics(startTime, endTime, organizer.getOrganizerId()));
    }

    @ApiOperation(value = "分享时间", notes = "分享时间", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "startTime", value = "开始时间", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "endTime", value = "结束时间", required = true, dataType = "string", paramType = "query")
    })
    @RequestMapping(value = "/shareDate.json", method = RequestMethod.POST)
    public WebResult shareDateStatistics(@RequestParam("startTime") String startTime, @RequestParam("endTime") String endTime, HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        return WebResult.getSuccessResult("data", activityService.shareDateStatistics(startTime, endTime, organizer.getOrganizerId()));
    }

    @ApiOperation(value = "复购次数", notes = "复购次数", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "startTime", value = "开始时间", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "endTime", value = "结束时间", required = true, dataType = "string", paramType = "query")
    })
    @RequestMapping(value = "/repurchase.json", method = RequestMethod.POST)
    public WebResult repurchaseStatistics(@RequestParam("startTime") String startTime, @RequestParam("endTime") String endTime, HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        return WebResult.getSuccessResult("data", ordersService.repurchaseStatistics(organizer.getOrganizerId(), startTime, endTime));
    }


}
