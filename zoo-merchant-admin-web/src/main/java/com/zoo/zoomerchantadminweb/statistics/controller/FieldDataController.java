package com.zoo.zoomerchantadminweb.statistics.controller;

import com.zoo.activity.dao.model.Organizer;
import com.zoo.activity.service.VerifyService;
import com.zoo.web.bean.WebResult;
import com.zoo.zoomerchantadminweb.context.WebUpmsContext;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@Api(value = "api/server/statistics/field", tags = {"主办方场地分析接口"}, description = "主办方场地分析接口描述")
@RestController
@RequestMapping("api/server/statistics/field")
public class FieldDataController {

    @Autowired
    private VerifyService verifyService;

    @ApiOperation(value = "基础数据", notes = "基础数据", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "startTime", value = "开始时间", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "endTime", value = "结束时间", required = true, dataType = "string", paramType = "query")
    })
    @RequestMapping(value = "/base.json", method = RequestMethod.POST)
    public WebResult baseData(@RequestParam("startTime") String startTime, @RequestParam("endTime") String endTime, HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        return WebResult.getSuccessResult("data", verifyService.getFieldBaseData(startTime, endTime, organizer.getOrganizerId()));
    }

    @ApiOperation(value = "时间段分析", notes = "时间段分析", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "startTime", value = "开始时间", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "endTime", value = "结束时间", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "type", value = "1.自助机数据 2.租赁数据 3闸机数据", required = false, dataType = "int", paramType = "query", defaultValue = "1")
    })
    @RequestMapping(value = "/hour.json", method = RequestMethod.POST)
    public WebResult hourData(@RequestParam("startTime") String startTime, @RequestParam("endTime") String endTime,
                              @RequestParam(value = "type", required = false, defaultValue = "1") Integer type,
                              HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        return WebResult.getSuccessResult("data", verifyService.hourStatistics(startTime, endTime, organizer.getOrganizerId(), type));
    }

    @ApiOperation(value = "次数分析", notes = "次数分析", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "startTime", value = "开始时间", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "endTime", value = "结束时间", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "type", value = "1.自助机数据 2.租赁数据 3闸机数据", required = false, dataType = "int", paramType = "query", defaultValue = "1")
    })
    @RequestMapping(value = "/times.json", method = RequestMethod.POST)
    public WebResult timesData(@RequestParam("startTime") String startTime, @RequestParam("endTime") String endTime,
                               @RequestParam(value = "type", required = false, defaultValue = "1") Integer type,
                               HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        return WebResult.getSuccessResult("data", verifyService.timesStatistics(startTime, endTime, organizer.getOrganizerId(), type));
    }

    @RequestMapping(value = "/pay.json", method = RequestMethod.POST)
    public WebResult payData(@RequestParam("startTime") String startTime, @RequestParam("endTime") String endTime,
                               @RequestParam(value = "type", required = false, defaultValue = "1") Integer type,
                               HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        return WebResult.getSuccessResult("data", verifyService.payStatistics(startTime, endTime, organizer.getOrganizerId(), type));
    }


}
