package com.zoo.zoomerchantadminweb.statistics.controller;

import com.zoo.activity.dao.model.Organizer;
import com.zoo.activity.service.OrdersService;
import com.zoo.web.bean.WebResult;
import com.zoo.zoomerchantadminweb.context.WebUpmsContext;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * Created by soul on 2018/7/17.
 */
@Api(value = "api/server/statistics/order", tags = {"主办方订单分析接口"}, description = "主办方订单分析接口描述")
@RestController
@RequestMapping("api/server/statistics/order")
public class OrderDataController {

    @Resource(name = "orderService")
    private OrdersService orderService;

    @ApiOperation(value = "基础数据", notes = "基础数据", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "startTime", value = "开始时间", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "endTime", value = "结束时间", required = true, dataType = "string", paramType = "query")
    })
    @RequestMapping(value = "/base.json", method = RequestMethod.POST)
    public WebResult baseData(@RequestParam("startTime") String startTime,
                              @RequestParam("endTime") String endTime,
                              HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null) {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
        try {
            return WebResult.getSuccessResult("data", orderService.orderBaseStatistics(organizer.getOrganizerId(), startTime, endTime));
        } catch (Exception e) {
            e.printStackTrace();
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }
    }

    @ApiOperation(value = "每日订单数信息", notes = "每日订单数信息", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "startTime", value = "开始时间", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "endTime", value = "结束时间", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "type", value = "1.每日订单数量 2.每日订单销售额 3.下单人数", required = false, dataType = "int", paramType = "query", defaultValue = "1")
    })
    @RequestMapping(value = "/orderNumberEveryday.json", method = RequestMethod.POST)
    public WebResult orderNumberEveryday(@RequestParam("startTime") String startTime,
                                         @RequestParam("endTime") String endTime,
                                         @RequestParam(value = "type", required = false, defaultValue = "1") Integer type,
                                         HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null) {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
        try {
            return WebResult.getSuccessResult("data", orderService.orderNumberEverydayStatistics(type, organizer.getOrganizerId(), startTime, endTime));
        } catch (Exception e) {
            e.printStackTrace();
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }
    }

    @ApiOperation(value = "订单状态分布", notes = "订单状态分布", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "startTime", value = "开始时间", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "endTime", value = "结束时间", required = true, dataType = "string", paramType = "query")
    })
    @RequestMapping(value = "/orderStatus.json", method = RequestMethod.POST)
    public WebResult orderStatus(@RequestParam("startTime") String startTime,
                                 @RequestParam("endTime") String endTime,
                                 HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null) {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
        try {
            return WebResult.getSuccessResult("data", orderService.getOrderStatusToStatistics(organizer.getOrganizerId(), startTime, endTime));
        } catch (Exception e) {
            e.printStackTrace();
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }
    }

    @ApiOperation(value = "产品种类销售排名", notes = "产品种类销售排名", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "startTime", value = "开始时间", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "endTime", value = "结束时间", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "limit", value = "查询数量（前x名）", required = false, dataType = "int", paramType = "query")
    })
    @RequestMapping(value = "/orderActivitySell.json", method = RequestMethod.POST)
    public WebResult orderActivitySell(@RequestParam("startTime") String startTime,
                                       @RequestParam("endTime") String endTime,
                                       @RequestParam(value = "limit", required = false) Integer limit,
                                       HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null) {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
        try {
            return WebResult.getSuccessResult("data", orderService.getOrderActivitySell(limit, organizer.getOrganizerId(), startTime, endTime));
        } catch (Exception e) {
            e.printStackTrace();
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }
    }

    @ApiOperation(value = "产品种类的销售情况", notes = "产品种类的销售情况", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "startTime", value = "开始时间", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "endTime", value = "结束时间", required = true, dataType = "string", paramType = "query")
    })
    @RequestMapping(value = "/orderActivityTypeGroup.json", method = RequestMethod.POST)
    public WebResult orderActivityTypeGroup(@RequestParam("startTime") String startTime,
                                            @RequestParam("endTime") String endTime,
                                            HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null) {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
        try {
            return WebResult.getSuccessResult("data", orderService.getOrderActivityTypeGroup(organizer.getOrganizerId(), startTime, endTime));
        } catch (Exception e) {
            e.printStackTrace();
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }
    }

    @ApiOperation(value = "订单支付分类情况", notes = "订单支付分类情况", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "startTime", value = "开始时间", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "endTime", value = "结束时间", required = true, dataType = "string", paramType = "query")
    })
    @RequestMapping(value = "/orderPayTypeGroup.json", method = RequestMethod.POST)
    public WebResult orderPayTypeGroup(@RequestParam("startTime") String startTime,
                                       @RequestParam("endTime") String endTime,
                                       HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null) {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
        try {
            return WebResult.getSuccessResult("data", orderService.getOrderPayTypeGroup(organizer.getOrganizerId(), startTime, endTime));
        } catch (Exception e) {
            e.printStackTrace();
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }
    }

    @ApiOperation(value = "复购分析查询", notes = "复购分析查询", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "startTime", value = "开始时间", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "endTime", value = "结束时间", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "type", value = "1.产品的复购人数 2.主办方的每天复购人数", required = false, dataType = "int", paramType = "query", defaultValue = "1")
    })
    @RequestMapping(value = "/orderRePurchase.json", method = RequestMethod.POST)
    public WebResult orderRePurchase(@RequestParam("startTime") String startTime,
                                     @RequestParam("endTime") String endTime,
                                     @RequestParam(value = "type", required = false, defaultValue = "1") Integer type,
                                     HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null) {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
        try {
            return WebResult.getSuccessResult("data", orderService.getOrderRePurchaseStatistics(type, organizer.getOrganizerId(), startTime, endTime));
        } catch (Exception e) {
            e.printStackTrace();
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }
    }

    @ApiOperation(value = "主办方每日退款单数", notes = "当前主办方 每日退款单数", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "startTime", value = "开始时间", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "endTime", value = "结束时间", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "type", value = "1.退款单数 2.退款原因", required = false, dataType = "int", paramType = "query", defaultValue = "1")
    })
    @RequestMapping(value = "/orderRefundEveryDay.json", method = RequestMethod.POST)
    public WebResult orderRefundEveryDay(@RequestParam("startTime") String startTime,
                                         @RequestParam("endTime") String endTime,
                                         @RequestParam(value = "type", required = false, defaultValue = "1") Integer type,
                                         HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null) {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
        try {
            return WebResult.getSuccessResult("data", orderService.getOrderRefundEveryDay(type, organizer.getOrganizerId(), startTime, endTime));
        } catch (Exception e) {
            e.printStackTrace();
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }
    }

    @ApiOperation(value = "渠道订单总销售查询", notes = "渠道订单总销售查询", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "startTime", value = "开始时间", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "endTime", value = "结束时间", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "type", value = "1.销售额 2.订单数量", required = false, dataType = "int", paramType = "query", defaultValue = "1")
    })
    @RequestMapping(value = "/orderOTAGroup.json", method = RequestMethod.POST)
    public WebResult orderOTAGroup(@RequestParam("startTime") String startTime,
                                         @RequestParam("endTime") String endTime,
                                         @RequestParam(value = "type", required = false, defaultValue = "1") Integer type,
                                         HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null) {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
        try {
            return WebResult.getSuccessResult("data", orderService.getOrderOTAGroup(type, organizer.getOrganizerId(), startTime, endTime));
        } catch (Exception e) {
            e.printStackTrace();
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }
    }
}
