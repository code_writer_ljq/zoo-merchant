package com.zoo.zoomerchantadminweb.statistics.controller;

import com.zoo.activity.dao.model.Organizer;
import com.zoo.activity.service.ActivityService;
import com.zoo.web.bean.WebResult;
import com.zoo.zoomerchantadminweb.context.WebUpmsContext;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@Api(value = "api/server/statistics/activity", tags = {"主办方产品分析接口"}, description = "主办方产品分析接口描述")
@RestController
@RequestMapping("api/server/statistics/activity")
public class ActivityDataController {
    @Autowired
    private ActivityService activityService;

    @ApiOperation(value = "基础数据", notes = "基础数据", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "startTime", value = "开始时间", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "endTime", value = "结束时间", required = true, dataType = "string", paramType = "query")
    })
    @RequestMapping(value = "/base.json", method = RequestMethod.POST)
    public WebResult baseData(@RequestParam("startTime") String startTime, @RequestParam("endTime") String endTime, HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        return WebResult.getSuccessResult("data", activityService.getActivityBaseData(startTime, endTime, organizer.getOrganizerId()));
    }

    @ApiOperation(value = "产品统计表", notes = "产品统计表", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "startTime", value = "开始时间", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "endTime", value = "结束时间", required = true, dataType = "string", paramType = "query")
    })
    @RequestMapping(value = "/activity.json", method = RequestMethod.POST)
    public WebResult activityStatistics(@RequestParam("startTime") String startTime, @RequestParam("endTime") String endTime, HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        return WebResult.getSuccessResult("data", activityService.activityStatistics(startTime, endTime, organizer.getOrganizerId()));
    }

    @ApiOperation(value = "产品状态分析", notes = "产品状态分析", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "startTime", value = "开始时间", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "endTime", value = "结束时间", required = true, dataType = "string", paramType = "query")
    })
    @RequestMapping(value = "/status.json", method = RequestMethod.POST)
    public WebResult statusStatistics(@RequestParam("startTime") String startTime, @RequestParam("endTime") String endTime, HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        return WebResult.getSuccessResult("data", activityService.statusStatistics(startTime, endTime, organizer.getOrganizerId()));
    }

    @ApiOperation(value = "产品种类占比", notes = "产品种类占比", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "startTime", value = "开始时间", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "endTime", value = "结束时间", required = true, dataType = "string", paramType = "query")
    })
    @RequestMapping(value = "/type.json", method = RequestMethod.POST)
    public WebResult typeStatistics(@RequestParam("startTime") String startTime, @RequestParam("endTime") String endTime, HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        return WebResult.getSuccessResult("data", activityService.typeStatistics(startTime, endTime, organizer.getOrganizerId()));
    }

    @ApiOperation(value = "租赁物分析", notes = "租赁物分析", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "startTime", value = "开始时间", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "endTime", value = "结束时间", required = true, dataType = "string", paramType = "query")

    })
    @RequestMapping(value = "/equipment.json", method = RequestMethod.POST)
    public WebResult equipmentStatistics(@RequestParam("startTime") String startTime, @RequestParam("endTime") String endTime, HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        return WebResult.getSuccessResult("data", activityService.equipmentStatistics(startTime, endTime, organizer.getOrganizerId()));
    }

    @ApiOperation(value = "产品浏览统计", notes = "产品浏览统计", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "size", value = "条", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "activityId", value = "产品ID", required = false, dataType = "int", paramType = "query")
    })
    @RequestMapping(value = "/productChartList.json", method = RequestMethod.POST)
    public WebResult productStatistics(@RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
                                       @RequestParam(value = "size", required = false, defaultValue = "10") Integer size,
                                       @RequestParam(value = "activityId", required = false) Integer activityId, HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        return WebResult.getSuccessResult("data", activityService.getProductChartList(activityId, organizer.getOrganizerId(), page, size));
    }
}
