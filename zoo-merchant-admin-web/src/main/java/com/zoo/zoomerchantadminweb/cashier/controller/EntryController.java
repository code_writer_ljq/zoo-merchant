package com.zoo.zoomerchantadminweb.cashier.controller;

import com.zoo.common.page.PageList;
import com.zoo.activity.dao.model.Charge;
import com.zoo.activity.dao.model.Orders;
import com.zoo.activity.dao.model.Organizer;
import com.zoo.activity.dao.model.OrganizerWithScenic;
import com.zoo.activity.dao.nutz.pojo.CommonAccount;
import com.zoo.activity.service.ChargeService;
import com.zoo.activity.service.OrdersService;
import com.zoo.activity.service.OrganizerService;
import com.zoo.icenow.dao.model.Entry;
import com.zoo.icenow.dao.model.Indemnity;
import com.zoo.icenow.service.EnterService;
import com.zoo.web.bean.WebResult;
import com.zoo.zoomerchantadminweb.context.WebUpmsContext;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Api(value = "api/server/entry", tags = {"主办方入园接口"}, description = "主办方入园接口描述")
@RestController
@RequestMapping("api/server/entry")
public class EntryController {
    @Autowired
    private EnterService enterService;

    @Autowired
    private OrdersService ordersService;

    @Autowired
    private OrganizerService organizerService;

    @Autowired
    private ChargeService chargeService;

    @ApiOperation(value = "主办方入园记录列表", notes = "主办方入园记录列表", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "size", value = "条", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "status", value = "状态", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "activityId", value = "产品ID", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "episodeId", value = "场次ID", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "code", value = "订单号", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "phone", value = "手机号", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "payType", value = "支付方式", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "enterTimeStart", value = "入园开始时间", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "enterTimeEnd", value = "入园结束时间", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "outTimeStart", value = "到期开始时间", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "outTimeEnd", value = "到期结束时间", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "entryId", value = "入园ID", required = false, dataType = "int", paramType = "query"),
    })
    @RequestMapping("/list.json")
    public WebResult getEntryList(@RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
                                  @RequestParam(value = "size", required = false, defaultValue = "10") Integer size,
                                  @RequestParam(value = "activityId", required = false) Integer activityId,
                                  @RequestParam(value = "episodeId", required = false) Integer episodeId,
                                  @RequestParam(value = "status", required = false) Integer status,
                                  @RequestParam(value = "phone", required = false) String phone,
                                  @RequestParam(value = "code", required = false) String code,
                                  @RequestParam(value = "enterTimeStart", required = false) String enterTimeStart,
                                  @RequestParam(value = "enterTimeEnd", required = false) String enterTimeEnd,
                                  @RequestParam(value = "outTimeStart", required = false) String outTimeStart,
                                  @RequestParam(value = "outTimeEnd", required = false) String outTimeEnd,
                                  @RequestParam(value = "payType", required = false) Integer payType,
                                  @RequestParam(value = "entryId", required = false) Integer entryId,
                                  HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (null != organizer) {
            if (code != null) {
                code = code.trim();
            }
            Integer organizerId = organizer.getOrganizerId();
            PageList pageList = enterService.getEntryViewListByOption(activityId, episodeId, status, code, phone, organizerId, enterTimeStart, enterTimeEnd, outTimeStart, outTimeEnd, payType, entryId, page, size);
            return WebResult.getSuccessResult("data", pageList);
        } else {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
    }

    @ApiOperation(value = "清冰续时", notes = "清冰续时", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "duration", value = "时间", required = true, dataType = "double", paramType = "query")
    })
    @RequestMapping("/clean-up.json")
    public WebResult cleanUp(@RequestParam("duration") Double duration, HttpServletRequest request) {
        CommonAccount account = WebUpmsContext.getAccount(request);
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (null == organizer) {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        } else {
            OrganizerWithScenic organizerWithScenic = organizerService.getAllInfoById(organizer.getOrganizerId());
            Map<String, Object> resultMap = enterService.cleanUp(organizerWithScenic, duration, account.getCustomerId());
            return WebResult.getSuccessResult("data", resultMap);
        }
    }

    @ApiOperation(value = "续时产品列表", notes = "续时产品列表", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "entryId", value = "入园ID", required = true, dataType = "int", paramType = "query")
    })
    @RequestMapping(value = "/product-info.json", method = RequestMethod.GET)
    public WebResult indemnityProductInfo(@RequestParam("entryId") Integer entryId, HttpServletRequest request) {
        Map<String, Object> resultMap = new HashMap<>();

        CommonAccount customer = WebUpmsContext.getAccount(request);
        Entry entry = enterService.getById(entryId);

        if (null == entry || entry.getStatus() != 3) {
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }

        Indemnity indemnity = enterService.getIndemnityByEntryIdAndStatus(entryId, 0);

        if (null == indemnity || indemnity.getStatus() != 0) {
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }

        resultMap.put("indemnityId", indemnity.getIndemnityId());
        Date now = new Date();

        Integer range = (int) (now.getTime() - entry.getMaxTime().getTime()) / 1000 / 60;



        /* 获取充值产品list */
        List<Map<String, Object>> productList = enterService.getIndemnityProductList(customer.getOrganizerId(), range);
        resultMap.put("productList", productList);


        return WebResult.getSuccessResult("data", resultMap);
    }

    @ApiOperation(value = "续时", notes = "续时", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "indemnityId", value = "续时ID", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "chargeId", value = "续时票ID", required = true, dataType = "int", paramType = "query")
    })
    @RequestMapping(value = "/deal.json", method = RequestMethod.POST)
    public WebResult dealIndemnity(@RequestParam("indemnityId") Integer indemintyId, @RequestParam("chargeId") Integer chargeId, HttpServletRequest request) {

        Organizer organizer = WebUpmsContext.getOrgnization(request);

        OrganizerWithScenic organizerWithScenic = organizerService.getAllInfoById(organizer.getOrganizerId());

        if (enterService.dealIndemnityByPc(indemintyId, chargeId, organizerWithScenic)) {
            return WebResult.getSuccessResult();
        } else {
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }
    }

    @ApiOperation(value = "启用", notes = "启用", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "entryId", value = "入园ID", required = true, dataType = "int", paramType = "query")
    })
    @RequestMapping("/enable.json")
    public WebResult enable(@RequestParam(value = "entryId", required = false) Integer entryId, HttpServletRequest request, HttpSession session) {
        Entry entry = enterService.getById(entryId);
        if (null == entry || entry.getStatus() != 5 || (null != entry.getMaxTime() && entry.getMaxTime().before(new Date()))) {
            return WebResult.getErrorResult(WebResult.Code.NOT_NEED_ENABLE);
        }
        if (entry.getStartTime() != null) {
            entry.setStatus(1);
        } else {
            entry.setStatus(0);
        }
        enterService.update(entry);
        return WebResult.getSuccessResult();
    }

    @ApiOperation(value = "禁用", notes = "禁用", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "entryId", value = "入园ID", required = true, dataType = "int", paramType = "query")
    })
    @RequestMapping("/disable.json")
    public WebResult disable(@RequestParam(value = "entryId", required = false) Integer entryId, HttpServletRequest request) {
        Entry entry = enterService.getById(entryId);
        if (null == entry || entry.getStatus() == 3) {
            return WebResult.getErrorResult(WebResult.Code.NOT_NEED_DISABLE);
        }
        Orders orders = ordersService.getSimpleOrdersById(entry.getOrderId());
        Charge charge = chargeService.getChargeById(orders.getChargeId());

        if (charge.getChargeMode() != 4 && entry.getStatus() == 4) {
            return WebResult.getErrorResult(WebResult.Code.NOT_NEED_DISABLE);
        }

        if (charge.getChargeMode() == 4) {
            entry = enterService.getLastEntry(orders.getOrderId());
            entry.setStatus(5);
        } else {
            entry.setStatus(5);
        }
        enterService.update(entry);
        return WebResult.getSuccessResult();
    }

    @ApiOperation(value = "刷卡记录", notes = "刷卡记录", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "entryId", value = "入园ID", required = true, dataType = "int", paramType = "query")
    })
    @RequestMapping("/records.json")
    public WebResult getAccessRecordByEntryId(@RequestParam(value = "entryId", required = false) Integer entryId, HttpServletRequest request) {

        CommonAccount account = WebUpmsContext.getAccount(request);
        if (null == account) {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }

        return WebResult.getSuccessResult("data", enterService.getAccessRecordByEntryId(entryId));
    }

}
