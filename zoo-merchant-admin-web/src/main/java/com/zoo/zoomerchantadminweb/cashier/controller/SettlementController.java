package com.zoo.zoomerchantadminweb.cashier.controller;

import com.zoo.activity.dao.model.Organizer;
import com.zoo.activity.dao.model.OrganizerSettlementLog;
import com.zoo.activity.dao.nutz.pojo.CommonAccount;
import com.zoo.activity.service.SettlementService;
import com.zoo.web.bean.WebResult;
import com.zoo.zoomerchantadminweb.base.BaseController;
import com.zoo.zoomerchantadminweb.context.WebUpmsContext;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by soul on 2018/8/1.
 */
@Api(value = "api/server/settlement", tags = {"主办方收银台结算日志接口"}, description = "主办方收银台结算日志相关接口")
@RestController
@RequestMapping("api/server/settlement")
public class SettlementController extends BaseController {

    @Resource(name = "settlementService")
    private SettlementService settlementService;

    @ApiOperation(value = "获取结算日志列表", notes = "获取某个收银员的所有结算日志列表", httpMethod = "GET")
    @RequestMapping(value = "/getLogList.json", method = RequestMethod.GET)
    public WebResult getList(HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        CommonAccount operateAccount = WebUpmsContext.getAccount(request);

        // 验证基本信息
        if (organizer == null || operateAccount == null) {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }

        try {
            return WebResult.getSuccessResult("data", settlementService.getSettlementLogList(operateAccount.getCustomerId(), organizer.getOrganizerId()));
        } catch (Exception e) {
            e.printStackTrace();
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }
    }
}
