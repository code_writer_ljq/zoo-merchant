package com.zoo.zoomerchantadminweb.cashier.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alipay.api.AlipayClient;
import com.alipay.api.domain.AlipayTradeCancelModel;
import com.alipay.api.domain.AlipayTradePayModel;
import com.alipay.api.request.AlipayTradeCancelRequest;
import com.alipay.api.request.AlipayTradePayRequest;
import com.alipay.api.response.AlipayTradeCancelResponse;
import com.alipay.api.response.AlipayTradePayResponse;
import com.alipay.api.response.AlipayTradeQueryResponse;
import com.pay.unionpay.api.AliPayApi;
import com.pay.unionpay.api.UnionPayApi;
import com.pay.unionpay.api.WXPaymentCodeApi;
import com.pay.unionpay.sdk.alipay.AliPayConstants;
import com.zoo.account.service.AccountService;
import com.zoo.activity.dao.model.*;
import com.zoo.activity.dao.nutz.pojo.CommonAccount;
import com.zoo.activity.service.*;
import com.zoo.activity.util.*;
import com.zoo.activity.vo.OrderModel;
import com.zoo.bean.util.ActivityBeanUtils;
import com.zoo.common.util.RedisUtil;
import com.zoo.finance.dao.model.ZooOnAccountCompany;
import com.zoo.finance.dao.model.ZooOnAccountCompanyExample;
import com.zoo.finance.service.ZooOnAccountCompanyService;
import com.zoo.icenow.service.EnterService;
import com.zoo.icenow.service.StorageService;
import com.zoo.web.bean.WebResult;
import com.zoo.zoomerchantadminweb.config.websocket.WebSocketEndPoint;
import com.zoo.zoomerchantadminweb.context.WebUpmsContext;
import com.zoo.zoomerchantadminweb.order.controller.SweepPayUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import jodd.util.StringUtil;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.util.*;

@Api(value = "api/server/cashier", tags = {"主办方收银台接口"}, description = "主办方收银台接口描述")
@RestController
@RequestMapping("api/server/cashier")
public class CashierController {

    @Resource(name = "episodeService")
    private EpisodeService episodeService;

    @Resource(name = "chargeService")
    private ChargeService chargeService;

    @Resource(name = "activityService")
    private ActivityService activityService;

    @Resource(name = "orderService")
    private OrdersService ordersService;

    @Resource(name = "playerService")
    private PlayerService playerService;

    @Resource(name = "nextIdService")
    private NextIdService nextIdService;

    @Resource(name = "storageService")
    private StorageService storageService;

    @Resource(name = "enterService")
    private EnterService enterService;

    @Resource(name = "depositService")
    private DepositService depositService;

    @Resource(name = "orderMarkService")
    private OrderMarkService orderMarkService;

    @Resource(name = "AccountService")
    private AccountService accountService;

    @Resource(name = "quartzService")
    private QuartzService quartzService;

    @Resource(name = "organizerPayConfigService")
    private OrganizerPayConfigService organizerPayConfigService;

    @Autowired
    private ZooOnAccountCompanyService zooOnAccountCompanyService;

    @Autowired
    private WebSocketEndPoint webSocketEndPoint;

    /**
     * 扫码订单支付记录
     */
    @Autowired
    private OrderSweepCodePayService orderSweepCodePayService;

    /**
     * 支付失败
     */
    final String PAY_ERROR = "PAY_ERROR";

    /**
     * 支付成功
     */
    final String PAY_SUCCESS = "PAY_SUCCESS";

    /**
     * 重新支付
     */
    final String PAY_RETRY = "PAY_RETRY";


    /**
     * b端显示的所有 线下售卖 的activity信息,前台下拉列表使用
     */
    @ApiOperation(value = "产品列表", notes = "收银台显示所有下线售卖产品", httpMethod = "GET")
    @RequestMapping(value = "/activity.json", method = RequestMethod.GET)
    public WebResult offlineActivityOption(@RequestParam(value = "cardType", required = false, defaultValue = "0") Integer cardType,
                                           @RequestParam(value = "activityClassificationId", required = false) Integer activityClassificationId,
                                           HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer != null) {
            return WebResult.getSuccessResult("data", activityService.getOfflineActivityOptionByOrgId(organizer.getOrganizerId(), cardType, activityClassificationId));
        } else {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
    }

    @ApiOperation(value = "场次列表", notes = "收银台根据产品ID获取某个产品下所有场次列表", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "activityId", value = "产品ID", required = true, dataType = "integer", paramType = "query"),
    })
    @RequestMapping(value = "/episode.json", method = RequestMethod.GET)
    public WebResult getEpisodeSimplelist(@RequestParam("activityId") Integer activityid, HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (null != organizer) {
            List<Episode> episodes = episodeService.getEpisodeDetailWithChargesByActivityId(activityid, organizer.getOrganizerId(), null);

            Map<Integer, List<Charge>> rentChargesMap = new HashMap<>();
            Map<Integer, List<Charge>> incrementChargesMap = new HashMap<>();
            for (Episode episode : episodes) {
                List<Charge> rentTmp = chargeService.getValidRentChargeList(episode.getEpisodeId());
                rentChargesMap.put(episode.getEpisodeId(), rentTmp);
                List<Charge> incrementTmp = chargeService.getIncrementChargesByEpisode(episode.getEpisodeId());
                incrementChargesMap.put(episode.getEpisodeId(), incrementTmp);

            }
            Map<String, Object> result = new HashMap<>();
            result.put("episodeList", episodes);
            result.put("rentChargesMap", rentChargesMap);
            result.put("incrementChargesMap", incrementChargesMap);
            result.put("depositGradient", organizer.getDepositGradient());
            return WebResult.getSuccessResult("data", result);
        } else {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
    }

    /**
     * 创建订单
     * 统一的事务处理-->createOrder()   /enroll/confirmOrder.json?episodeId=1&&chargeNum=2&&chargeId=1
     */
    @RequestMapping(value = "/confirmOrder.json", method = RequestMethod.POST)
    public WebResult confirmOrder(OrderModel orderModel,
                                  @RequestParam(value = "chargeNumList", required = false) String chargeNumJson,
                                  @RequestParam(value = "appointmentTime", required = false) String appointmentTime,
                                  @RequestParam(value = "incrementInfoList", required = false) String incrementInfoList,
                                  HttpServletRequest request) throws Exception {
        Integer episodeId = orderModel.getEpisodeId();
        Integer rent = orderModel.getRent();

        Integer chargeNum = orderModel.getChargeNum();
        List<Integer> chargeIdList = new ArrayList<>();

        JSONArray chargeIdArray = JSON.parseArray(orderModel.getChargeId());
        for (Object chargeId : chargeIdArray) {
            chargeIdList.add(Integer.parseInt(chargeId.toString()));
        }
        String items = orderModel.getItems();
        CommonAccount customer = WebUpmsContext.getAccount(request);

        List<Integer> chargeNumList = new ArrayList<>();
        if (org.apache.commons.lang3.StringUtils.isNotBlank(chargeNumJson)) {
            JSONArray chargeNumArray = JSON.parseArray(chargeNumJson);
            for (Object chargeNumObject : chargeNumArray) {
                chargeNumList.add(Integer.parseInt(chargeNumObject.toString()));
            }
        }

        Date appointmentDateTime = null;
        if (StringUtils.isNotBlank(appointmentTime)) {
            appointmentDateTime = DateUtil.convertSimpleStringToDate(appointmentTime);
        }

        //for (Integer chargeId : chargeIdList) {
        /*判断该票种是否限制购买人数*/
        //Charge charge = chargeService.getChargeWithBlobsById(chargeId);
//            //如果该票种限制购买数量
//            if (charge.getBuyLimitType() == 1) {
//                //当前提交的订单的购买数量大于限制购买数量
//                if (charge.getBuyLimitCount() < chargeNum) {
//                    return WebResult.getErrorResult(WebResult.Code.CASHIER_CHARGE_LIMIT);
//                } else {
//                    //当前用户已经购买成功的数量
//                    Integer buyCount = ordersService.getOrderCount(customer.getCustomerId(), chargeId, 1);
//                    if (null != buyCount && charge.getBuyLimitCount() < (chargeNum + buyCount)) {
//                        return WebResult.getErrorResult(WebResult.Code.CASHIER_CHARGE_LIMIT);
//                    }
//                }
//            }

        //if (StringUtils.isNotBlank(appointmentTime)) {
        //appointmentDateTime = DateUtil.convertSimpleStringToDate(appointmentTime);
        //}

//            if (appointmentDateTime != null && StringUtils.isNotBlank(charge.getBuyExceptLimit())) {
//                List<String> buyExceptStr = JSON.parseArray(charge.getBuyExceptLimit(), String.class);
//                String appointStr = appointmentTime.substring(0, 10);
//                if (buyExceptStr.size() > 0 && buyExceptStr.contains(appointStr)) {
//                    return WebResult.getErrorResult(WebResult.Code.CASHIER_INVALID_TIME);
//                }
//            }
        //}

        Boolean isMultiOrder = false;
        String players = request.getParameter("playerEnrollInfos");
//        logger.info("== 报名信息(playerEnrollInfos): " + players);
        List<Player> playerModelList = PlayerUtil.formatPlayerFromString(players);
//        logger.info("== formatPlayerFromString: " + JSON.toJSONString(playerModelList));

        Map<Integer, Integer> integerIntegerMap = new HashMap<>();

        JSONArray incrementArray = new JSONArray();

        if (StringUtils.isNotBlank(incrementInfoList)) {
            JSONArray jsonArray = JSONArray.parseArray(incrementInfoList);
            if (null != jsonArray) {
                Iterator<Object> iterator = jsonArray.iterator();
                while (iterator.hasNext()) {
                    JSONObject jsonObject = (JSONObject) iterator.next();
                    Integer chargeId = jsonObject.getInteger("id");
                    Integer count = jsonObject.getInteger("number");
                    if (null != count && count > 0) {
                        isMultiOrder = true;
                        Charge incrementCharge = chargeService.getChargeById(chargeId);
//                        if (incrementCharge.getBuyLimitType() == 1) {
//                            if (incrementCharge.getBuyLimitCount() < count) {
//                                return WebResult.getErrorResult(WebResult.Code.CASHIER_CHARGE_LIMIT);
//                            } else {
//                                //当前用户已经购买成功的数量
//                                Integer buyCount = ordersService.getOrderCount(customer.getCustomerId(), chargeId, 1);
//                                if (null != buyCount && incrementCharge.getBuyLimitCount() < (count + buyCount)) {
//                                    return WebResult.getErrorResult(WebResult.Code.CASHIER_CHARGE_LIMIT);
//                                }
//                            }
//                        }
                        integerIntegerMap.put(chargeId, count);
                        Map<String, Object> incrementNameMap = new HashMap<>();
                        incrementNameMap.put("name", incrementCharge.getChargeName());
                        incrementNameMap.put("count", count);
                        incrementArray.add(incrementNameMap);
                    }
                }
            }
        }
        try {

            if (playerModelList.size() == 0) {
//                logger.error("报名信息为空！");
                return WebResult.getErrorResult(WebResult.Code.CASHIER_INVALID_REGISTRATION_FORM);
            }

            List<Player> playerList = new ArrayList<>();

            int defaultPlayerIndex = 0;
//            for (int i = 0; i < playerModelList.size(); i++) {
//                PlayerModel playerModel = playerModelList.get(i);
//                Player player = new Player();
//                BeanUtilsExtends.copyProperties(player, playerModel);
//                playerList.add(player);
//                // 标记默认联系人
//            }

            Integer client = customer.getOrganizerId();

            OrdersResult result;
            if (!isMultiOrder) {
                result = ordersService.createOrder(client, customer, episodeId, chargeNum, chargeIdList, playerModelList, appointmentDateTime, defaultPlayerIndex, rent, items, null, chargeNumList, "线下支付", null, null);
            } else {
                result = ordersService.createMultiOrder(client, customer, episodeId, chargeNum, chargeIdList.get(0), playerModelList, integerIntegerMap, appointmentDateTime, defaultPlayerIndex, "线下支付", null);
            }
            if (result.getOrders() == null) {
                return WebResult.getErrorMsgResult(WebResult.Code.CASHIER_CREATE_ORDER_ERROR, result.getMessage());
            } else {
                Map<String, Object> resultMap = new HashMap<>();
                resultMap.put("orderId", result.getOrders().getOrderId().toString());
                resultMap.put("code", result.getOrders().getCode());
                resultMap.put("multi", isMultiOrder ? "1" : "0");
                BigDecimal ticketPrice = result.getOrders().getTotalPrice().subtract(result.getOrders().getDeposit() == null ? new BigDecimal(0) : result.getOrders().getDeposit());
                resultMap.put("totalPrice", ticketPrice.doubleValue());
                resultMap.put("payPrice", result.getOrders().getTotalPrice().doubleValue());
                resultMap.put("deposit", (result.getOrders().getDeposit() == null ? new BigDecimal(0) : result.getOrders().getDeposit()).doubleValue());
                resultMap.put("increment", incrementArray);
                resultMap.put("activity", result.getResultMap());
                return WebResult.getSuccessResult("data", resultMap);
            }
        } catch (IllegalAccessException e) {
//            logger.error("player从model转换失败：" + e.getMessage());
            return WebResult.getErrorResult(WebResult.Code.CASHIER_CREATE_ORDER_ERROR);
        } catch (InvocationTargetException e) {
            return WebResult.getErrorResult(WebResult.Code.CASHIER_CREATE_ORDER_ERROR);
        } catch (Exception e) {
//            System.out.println(AssUtils.parse(e));
            return WebResult.getErrorResult(WebResult.Code.CASHIER_CREATE_ORDER_ERROR);
        }
    }

    /**
     * 通知支付信息 (前端接收：PAY_ERROR、PAY_SUCCESS、PAY_RETRY)
     *
     * @param orgId
     * @param userId
     * @param payMessage
     */
    private void pushPayMessage(OrderSweepCodePay sweepCodePay, long orgId, long userId, String payMessage, String status) {

        orderSweepCodePayService.updateByPrimaryKey(sweepCodePay);

        if (org.apache.commons.lang3.StringUtils.isNotBlank(status) && PAY_ERROR.equalsIgnoreCase(status)) {
            ordersService.updateOrdersByParentId(Math.toIntExact(sweepCodePay.getOrderId()));
        }

        SweepPayUtils.putResult(orgId, sweepCodePay.getOrderId(), userId, payMessage, status);

        /*Map<String,String> parasMap = new HashMap<String,String>();
        parasMap.put("orgId","" + orgId);
        parasMap.put("userId","" + userId);
        parasMap.put("orderId","" + sweepCodePay.getOrderId());
        WebsocketSender.send(status,parasMap);*/

        /*Map<String, WebSocketSession> sessionMap = webSocketEndPoint.getSocketSessionMap(orgId , userId);
        if (sessionMap != null && sessionMap.size() > 0) {
            TextMessage textMessage = new TextMessage(message);

            for (Map.Entry<String, WebSocketSession> entry : sessionMap.entrySet()) {
                try {
                    entry.getValue().sendMessage(textMessage);
                } catch (Exception e) {
                    // 异常信息
                    e.printStackTrace();
                }
            }
        } else {
            System.out.println("连接丢失，支付通知失败....");
        }*/

    }

    @RequestMapping(value = "/offlineSweepCodePay.json", method = RequestMethod.POST)
    public Object offlineSweepCodePay(@RequestParam(value = "orderId", required = false) Integer orderId,
                                      @RequestParam(value = "payType", required = false) Integer payType,
                                      @RequestParam(value = "receiptCode", required = false, defaultValue = "") String receiptCode,
                                      @RequestParam(value = "payInCashMoney", required = false) BigDecimal payInCashMoney,
                                      @RequestParam(value = "payByCardMoney", required = false) BigDecimal payByCardMoney,
                                      @RequestParam(value = "payByWechatMoney", required = false) BigDecimal payByWechatMoney,
                                      @RequestParam(value = "payByAlipayMoney", required = false) BigDecimal payByAlipayMoney,
                                      @RequestParam(value = "payByPreSaleMoney", required = false) BigDecimal payByPreSaleMoney,
                                      @RequestParam(value = "payByBullMoney", required = false) BigDecimal payByBullMoney,
                                      @RequestParam(value = "depositOfflineMoney", required = false) BigDecimal depositOfflineMoney,
                                      @RequestParam(value = "depositOfflineType", required = false) Integer depositOfflineType,
                                      @RequestParam(value = "companyId", required = false) Integer companyId,
                                      @RequestParam(value = "trustees", required = false) String trustees,
                                      @RequestParam(value = "trusteesPhone", required = false) String trusteesPhone,
                                      @RequestParam(value = "entertainPersonName", required = false) String entertainPersonName,
                                      @RequestParam(value = "entertainPersonPhone", required = false) String entertainPersonPhone,
                                      @RequestParam(value = "companyName", required = false) String companyName,
                                      @RequestParam(value = "authorcode", required = false) String authorcode,
                                      HttpSession session, HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        CommonAccount commonAccount = WebUpmsContext.getAccount(request);
        if (commonAccount == null) {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
        if (organizer == null) {
            return WebResult.getErrorResult(WebResult.Code.COACH_GET_ORGANIZER_INFO_ERROR);
        }
        if (StringUtils.isBlank(authorcode) && (payType == 2 || payType == 4 || payType == 13)) {
            //需要重新扫码
            return WebResult.getErrorResult(WebResult.Code.CASHIER_NO_AUTHOR_CODE);
        }
        /*String unipayOpShopId = organizerPayConfigService.getUnipayOpShopId(organizer.getOrganizerId());
        if(StringUtil.isEmpty(unipayOpShopId)){
            return WebResult.getErrorMsgResult(WebResult.Code.ERROR,"没有支付配置");
        }*/
//        SweepPayUtils.clearResult(organizer.getOrganizerId(), orderId, commonAccount.getCustomerId());
        new Thread() {
            @Override
            public void run() {
                offlinePay(orderId,
                        payType,
                        receiptCode,
                        payInCashMoney,
                        payByCardMoney,
                        payByWechatMoney,
                        payByAlipayMoney,
                        payByPreSaleMoney,
                        payByBullMoney,
                        depositOfflineMoney,
                        depositOfflineType,
                        companyId,
                        trustees,
                        trusteesPhone,
                        entertainPersonName,
                        entertainPersonPhone,
                        companyName,
                        authorcode,
                        organizer,
                        commonAccount);
            }
        }.start();
        return WebResult.getSuccessResult();
    }

    @ApiOperation(value = "线下支付", notes = "线下支付", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "companyId", value = "企业ID-(挂账业务)", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "companyName", value = "招待单位-(招待业务)", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "trustees", value = "经办人-(挂账和招待业务)", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "trusteesPhone", value = "经办人电话-(挂账和招待业务)", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "entertainPersonName", value = "招待人-(挂账和招待业务)", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "entertainPersonPhone", value = "招到人电话-(挂账和招待业务)", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "authorcode", value = "授权码：扫码支付时使用", required = false, dataType = "string", paramType = "query"),
    })
    @RequestMapping(value = "/offlinePay.json", method = RequestMethod.POST)
    public Object offlinePay(@RequestParam(value = "orderId", required = false) Integer orderId,
                             @RequestParam(value = "payType", required = false) Integer payType,
                             @RequestParam(value = "receiptCode", required = false, defaultValue = "") String receiptCode,
                             @RequestParam(value = "payInCashMoney", required = false) BigDecimal payInCashMoney,
                             @RequestParam(value = "payByCardMoney", required = false) BigDecimal payByCardMoney,
                             @RequestParam(value = "payByWechatMoney", required = false) BigDecimal payByWechatMoney,
                             @RequestParam(value = "payByAlipayMoney", required = false) BigDecimal payByAlipayMoney,
                             @RequestParam(value = "payByPreSaleMoney", required = false) BigDecimal payByPreSaleMoney,
                             @RequestParam(value = "payByBullMoney", required = false) BigDecimal payByBullMoney,
                             @RequestParam(value = "depositOfflineMoney", required = false) BigDecimal depositOfflineMoney,
                             @RequestParam(value = "depositOfflineType", required = false) Integer depositOfflineType,
                             @RequestParam(value = "companyId", required = false) Integer companyId,
                             @RequestParam(value = "trustees", required = false) String trustees,
                             @RequestParam(value = "trusteesPhone", required = false) String trusteesPhone,
                             @RequestParam(value = "entertainPersonName", required = false) String entertainPersonName,
                             @RequestParam(value = "entertainPersonPhone", required = false) String entertainPersonPhone,
                             @RequestParam(value = "companyName", required = false) String companyName,
                             @RequestParam(value = "authorcode", required = false) String authorcode,
                             HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        CommonAccount account = WebUpmsContext.getAccount(request);
        return offlinePay(orderId,
                payType,
                receiptCode,
                payInCashMoney,
                payByCardMoney,
                payByWechatMoney,
                payByAlipayMoney,
                payByPreSaleMoney,
                payByBullMoney,
                depositOfflineMoney,
                depositOfflineType,
                companyId,
                trustees,
                trusteesPhone,
                entertainPersonName,
                entertainPersonPhone,
                companyName,
                authorcode,
                organizer,
                account);
    }

    /**
     * 支付失败，撤销订单
     *
     * @param order
     * @param commonAccount
     * @param organizer
     */
    private void paySweepCodeError(OrderSweepCodePay sweepCodePay, Map<String, String> responseMaps, Orders order, OrganizerPayConfig organizerPayConfigByOrgId, CommonAccount commonAccount, Organizer organizer, String payMessage) {
        //撤销支付
        try {
            if (order.getPayTypeId() == 2) {
                String errorMsg = "";
                if (order.getTotalPrice().doubleValue() != 0) {
                    AlipayClient alipayClient = AliPayApi.alipayClient(organizerPayConfigByOrgId.getAlipayAppId()
                            , organizerPayConfigByOrgId.getPrivateKey(), organizerPayConfigByOrgId.getAlipayPublicKey()
                            , organizerPayConfigByOrgId.getAlipayEncryptKey(), organizerPayConfigByOrgId.getAlipayEncryptType());


                    //创建API对应的request类
                    AlipayTradeCancelRequest alipayTradeCancelRequest = new AlipayTradeCancelRequest();
                    AlipayTradeCancelModel alipayTradeCancelModel = new AlipayTradeCancelModel();
                    alipayTradeCancelModel.setOutTradeNo(order.getParentCode());
                    alipayTradeCancelRequest.setBizModel(alipayTradeCancelModel);
                    //request.putOtherTextParam("app_auth_token",organizerPayConfigByOrgId.getAppAuthToken); //设置商户的应用授权token
                    //通过alipayClient调用API，获得对应的response类
                    alipayTradeCancelRequest.setNeedEncrypt(true);
                    alipayTradeCancelRequest.putOtherTextParam("app_auth_token", organizerPayConfigByOrgId.getAppAuthToken());
                    AlipayTradeCancelResponse alipayTradeCancelResponse = alipayClient.execute(alipayTradeCancelRequest);

                    System.out.println("-----------------------订单模式收银台系统支付宝撤销接口返回body体信息：" + alipayTradeCancelResponse);
                    if (alipayTradeCancelResponse.isSuccess()) {
                        System.out.println("-----------------------订单模式收银台系统支付宝撤销接口支付成功,撤销订单号：" + sweepCodePay.getPayCode());
                    } else {
                        System.out.println("-----------------------订单模式收银台系统支付宝撤销支付失败, msg原因：" + alipayTradeCancelResponse.getMsg() + ", subMsg原因业务参数返回信息：" + alipayTradeCancelResponse.getSubMsg());
                        //errorMsg = errorMsg + alipayTradeCancelResponse.getSubMsg() + order.getParentCode();
                    }
                }
                /*if (StringUtils.isNotEmpty(responseMaps.get("out_trade_no"))) {
                    sweepCodePay.setTransactionId(responseMaps.get("out_trade_no"));
                }*/
                //设置PayWay
                System.out.println("订单模式收银台线下支付宝------------撤销操作，打印支付宝支付接口返回对象转成map的信息：" + JSONObject.toJSONString(responseMaps));
                Set keyset = responseMaps.keySet();
                Iterator iterator = keyset.iterator();
                while (iterator.hasNext()) {
                    Object key = iterator.next();
                    if (!key.equals("sign")) {
                        JSONObject jsonObject = JSON.parseObject(JSONObject.toJSONString(responseMaps.get(key)));
                        System.out.println("订单模式线下支付宝撤销接口，打印key值：" + key + " ,value值:" + JSONObject.toJSONString(jsonObject));
                        /*if (jsonObject.containsKey("code")) {
                            errorMsg = errorMsg + jsonObject.getString("code");
                        }
                        if (jsonObject.containsKey("msg")) {
                            errorMsg = errorMsg + jsonObject.getString("msg");
                        }*/
                        if (jsonObject.containsKey("sub_code")) {
                            errorMsg = errorMsg + "业务错误码：" + jsonObject.getString("sub_code");
                        }
                        if (jsonObject.containsKey("sub_msg")) {
                            errorMsg = errorMsg + "业务错Msg：" + jsonObject.getString("sub_msg");
                        }
                    }
                }
                System.out.println("-----------------------订单模式收银台系统--支付宝撤销接口错误信息拼接字符串：：" + errorMsg);
                if (StringUtils.isNotBlank(AliPayApi.getSubCode(responseMaps)) || StringUtils.isNotBlank(AliPayApi.getSubSMsg(responseMaps))) {
                    sweepCodePay.setErrorMsg(AliPayApi.getSubCode(responseMaps) + ":" + AliPayApi.getSubSMsg(responseMaps));
                }
                if (StringUtils.isBlank(sweepCodePay.getErrorMsg())) {
                    sweepCodePay.setErrorMsg(errorMsg);
                }
            } else if (order.getPayTypeId() == 4) {
                if (order.getTotalPrice().doubleValue() != 0) {
                    Map<String, String> map = new HashMap<>();
                    if (organizerPayConfigByOrgId != null && StringUtils.isNotBlank(organizerPayConfigByOrgId.getSubmchid())) {
                        map.put("sub_mch_id", organizerPayConfigByOrgId.getSubmchid());
                    }
                    map.put("out_trade_no", order.getParentCode());
                    map = WXPaymentCodeApi.reverse(map);
                }
                if (responseMaps != null) {
                    String transactionId = WXPaymentCodeApi.getTransactionId(responseMaps);
                    if (StringUtils.isNotEmpty(transactionId)) {
                        sweepCodePay.setTransactionId(transactionId);
                    }

                    String payWay = WXPaymentCodeApi.getPayWay(responseMaps);
                    if (StringUtils.isNotEmpty(payWay)) {
                        sweepCodePay.setPayWay(payWay);
                    }

                    sweepCodePay.setErrorMsg(WXPaymentCodeApi.getErrMsg(responseMaps));
                }
            } else if (order.getPayTypeId() == 13) {
                // order.setStatus(4);
                // ordersService.updateOrders(order);
                if (order.getTotalPrice().doubleValue() != 0) {
                    if (StringUtil.isEmpty(organizerPayConfigByOrgId.getUnipayOpenapiMchId())) {
                        Map<String, String> cancelPayResult = UnionPayApi.cancelPay(sweepCodePay.getPayCode(), organizerPayConfigByOrgId.getUnipayOpShopId());
                    } else {
                        Map<String, String> cancelPayResult = UnionPayApi.openApiCancelPay(sweepCodePay.getPayCode(), organizerPayConfigByOrgId.getUnipayOpenapiMchId(), organizerPayConfigByOrgId.getUnipayOpenapiKey());
                    }
//                    sweepCodePay.setTradeStat(ActivityBeanUtils.SweepPayTradeStat.REVOKED.toString());
                }
                /*else {
                    sweepCodePay.setTradeStat(ActivityBeanUtils.SweepPayTradeStat.PAYERROR.toString());
                }*/
                if (responseMaps != null) {
                    String transactionId = UnionPayApi.getTransactionId(responseMaps);
                    if (StringUtils.isNotEmpty(transactionId)) {
                        sweepCodePay.setTransactionId(transactionId);
                    }

                    String payWay = UnionPayApi.getPayWay(responseMaps);
                    if (StringUtils.isNotEmpty(payWay)) {
                        sweepCodePay.setPayWay(payWay);
                    }

                    sweepCodePay.setErrorMsg(UnionPayApi.getErrMsg(responseMaps));
                }

            }

            if (order.getTotalPrice().doubleValue() == 0) {
                sweepCodePay.setTradeStat(ActivityBeanUtils.SweepPayTradeStat.PAYERROR.toString());
            } else {
                sweepCodePay.setTradeStat(ActivityBeanUtils.SweepPayTradeStat.REVOKED.toString());
            }
            // 推送消息
            pushPayMessage(sweepCodePay, organizer.getOrganizerId(), commonAccount.getCustomerId(), payMessage, PAY_ERROR);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Object offlinePay(Integer orderId,
                             Integer payType,
                             String receiptCode,
                             BigDecimal payInCashMoney,
                             BigDecimal payByCardMoney,
                             BigDecimal payByWechatMoney,
                             BigDecimal payByAlipayMoney,
                             BigDecimal payByPreSaleMoney,
                             BigDecimal payByBullMoney,
                             BigDecimal depositOfflineMoney,
                             Integer depositOfflineType,
                             Integer companyId,
                             String trustees,
                             String trusteesPhone,
                             String entertainPersonName,
                             String entertainPersonPhone,
                             String companyName,
                             String authorcode,
                             Organizer organizer,
                             CommonAccount operatorAccount
    ) {
        if (payType == 9 && companyId == null) {
            //挂账
            return WebResult.getErrorMsgResult(WebResult.Code.ERROR, "没有选择挂账企业");
        } else if (payType == 10 && (StringUtils.isEmpty(trustees) || StringUtils.isEmpty(entertainPersonName))) {
            //招待
            return WebResult.getErrorMsgResult(WebResult.Code.ERROR, "请输入经办人与招待人信息");
        }

        Orders simpleOrdersById = ordersService.getSimpleOrdersById(orderId);
        if (simpleOrdersById.getStatus() != 0) {
            return WebResult.getErrorResult(WebResult.Code.CASHIER_CANT_PAY);
        }

        //挂账招待押金和租赁、增值服务金额都为0才可以下单，不为0报错
        if (payType == 9 || payType == 10) {
            //押金判断
            if (simpleOrdersById.getDeposit() != null && simpleOrdersById.getDeposit().doubleValue() != 0) {
                return WebResult.getErrorResult(WebResult.Code.HUNG_UP_MONEY_DEPOSIT_MUST_ZERO);
            }

            //租赁、增值服务金额判断
                /*List<Orders> childOrders = ordersService.getChildOrders(orderId);
                if (childOrders != null) {
                    for (Orders orders : childOrders) {
                        //判断是否是租赁物
                        if (orders.getDeposit()!=null && orders.getDeposit().doubleValue() > 0) {
                            return WebResult.getErrorResult(WebResult.Code.HUNG_UP_MONEY_RENT_MUST_ZERO);
                        }

                        //增值服务判断
                        *//*Charge charge = chargeService.getChargeById(orders.getChargeId());
                        if (charge.getIncrement() != null && charge.getIncrement().intValue() == 1 && orders.getTotalPrice() != null && orders.getTotalPrice().doubleValue() > 0) {
                            return WebResult.getErrorResult(WebResult.Code.HUNG_UP_MONEY_INCREMENT_MUST_ZERO);
                        }*//*

                    }``
                }*/

            if (payType == 9) {
                ZooOnAccountCompanyExample example = new ZooOnAccountCompanyExample();
                example.createCriteria().andIdEqualTo(new Long(companyId));
                ZooOnAccountCompany company = zooOnAccountCompanyService.selectFirstByExample(example);
                if (simpleOrdersById.getDeposit() != null) {
                    if (company.getBalance().doubleValue() < simpleOrdersById.getTotalPrice().subtract(simpleOrdersById.getDeposit()).doubleValue()) {
                        return WebResult.getErrorResult(WebResult.Code.HUNG_UP_MONEY_NO_FULL);
                    }
                } else {
                    if (company.getBalance().doubleValue() < simpleOrdersById.getTotalPrice().doubleValue()) {
                        return WebResult.getErrorResult(WebResult.Code.HUNG_UP_MONEY_NO_FULL);
                    }
                }
                companyName = company.getCompanyName();
            }
        }

//      JedisConnectionFactory jedisConnectionFactory = (JedisConnectionFactory) SpringContextUtil.getBean("jedisConnectionFactory");
//      Jedis jedis = jedisConnectionFactory.getShardInfo().createResource();
        String requestId = UUID.randomUUID().toString();
        String offlinePayLockKey = "order_mode_offline_pay" + "_" + simpleOrdersById.getOrderId();
        OrderSweepCodePay sweepCodePay = null;
        Map<String, String> responseResult = null;
        OrganizerPayConfig organizerPayConfigByOrgId = null;
        Date now = new Date();
        try {
            // 支付加分布式锁
            if (!com.zoo.common.util.RedisUtil.tryGetDistributedLock(offlinePayLockKey, requestId, 70000L)) {
                return WebResult.getErrorResult("订单支付中，请稍后重试！");
            }

            //扫码支付
            String transactionId = null;
            String unipayOpShopId = null;
            String extSweepCodeWay = null;
            // 组装支付业务参数
            Map<String, String> paramMap = new HashMap<>();
            if ((payType == 2 || payType == 4 || payType == 13) && simpleOrdersById.getTotalPrice().doubleValue() > 0) {
//                if (organizer.getIsSupportSweepCode() == 1) {
//                    return WebResult.getErrorResult(WebResult.Code.CASHIER_NO_AUTHOR_CODE);
//                }

                SweepPayUtils.clearResult(organizer.getOrganizerId(), orderId, operatorAccount.getCustomerId());

                sweepCodePay = orderSweepCodePayService.findOrderSweepCodePaySuccess(simpleOrdersById.getCode());
                organizerPayConfigByOrgId = organizerPayConfigService.getOrganizerPayConfigByOrgId(organizer.getOrganizerId());
                if (organizerPayConfigByOrgId == null) {
                    // 释放支付分布式锁
                    RedisUtil.releaseDistributedLock(offlinePayLockKey, requestId);
                    return WebResult.getErrorResult(WebResult.Code.CASHIER_ORGANIZER_SCAN_PAY_NO_CONFIG);
                }

                if (payType == 2) {
                    //支付状态
                    Boolean aliPayStatus = false;
                    //支付失败备注
                    String remarks = "";
                    AlipayClient alipayClient = AliPayApi.alipayClient(organizerPayConfigByOrgId.getAlipayAppId()
                            , organizerPayConfigByOrgId.getPrivateKey(), organizerPayConfigByOrgId.getAlipayPublicKey()
                            , organizerPayConfigByOrgId.getAlipayEncryptKey(), organizerPayConfigByOrgId.getAlipayEncryptType());


                    if (sweepCodePay != null) {
                        //查询支付宝订单
                        responseResult = BeanUtils.describe(AliPayApi.query(simpleOrdersById.getParentCode(), organizerPayConfigByOrgId.getAppAuthToken(), alipayClient));
                    } else {
                        long d = System.currentTimeMillis();
                        simpleOrdersById.setPayTypeId(payType);
                        //创建支付记录
                        sweepCodePay = orderSweepCodePayService.createPay(simpleOrdersById);
                        // 组装支付业务数据
                        AlipayTradePayRequest request = new AlipayTradePayRequest();
                        AlipayTradePayModel alipayTradePayModel = new AlipayTradePayModel();
                        // 商品标题
                        alipayTradePayModel.setSubject(organizer.getOrganizerId() + "-" + simpleOrdersById.getChargeName() + "x" + simpleOrdersById.getChargeNum());
                        // 商家订单编号
                        alipayTradePayModel.setOutTradeNo(simpleOrdersById.getParentCode());
                        // 订单总金额
                        alipayTradePayModel
                                .setTotalAmount(String.valueOf(
                                        simpleOrdersById.getTotalPrice() != null ? simpleOrdersById.getTotalPrice().setScale(2) : 0));
                        // 付款码
                        alipayTradePayModel.setAuthCode(authorcode);

                        request.setBizModel(alipayTradePayModel);
                        request.setNeedEncrypt(true);
                        request.putOtherTextParam("app_auth_token", organizerPayConfigByOrgId.getAppAuthToken());
                        System.out.println("----------------------订单模式收银台支付宝支付金额" + simpleOrdersById.getTotalPrice().setScale(2, BigDecimal.ROUND_HALF_UP));
                        AlipayTradePayResponse response = alipayClient.execute(request);

                        System.out.println(response.getBody());
                        sweepCodePay.setPayWay("alipay");//支付方式需要确认
                        sweepCodePay.setTransactionId(response.getTradeNo());
                        sweepCodePay.setTime(now);
                        if (response.isSuccess()) {
                            String code = response.getCode();
                            System.out.println(response.getCode());
                            if (AliPayConstants.ALI_PAY_SUCCESS.equals(code)) {
                                System.out.println("----------------------订单模式收银台支付宝支付成功!  订单号：" + simpleOrdersById.getParentCode());
//                                aliPayStatus = true;
                                sweepCodePay.setTradeStat(ActivityBeanUtils.SweepPayTradeStat.SUCCESS.toString());
                            } else if (AliPayConstants.ALI_PAY_USERPAYING.equals(code)) {//用户正在支付
                                AlipayTradeQueryResponse alipayTradeQueryResponse = AliPayApi.query(simpleOrdersById.getParentCode(), organizerPayConfigByOrgId.getAppAuthToken(), alipayClient);
                                for (int i = 1; i < 30; i++) {
                                    Thread.sleep(2000);
                                    alipayTradeQueryResponse = AliPayApi.query(simpleOrdersById.getParentCode(), organizerPayConfigByOrgId.getAppAuthToken(), alipayClient);
                                    String tradeStatus = alipayTradeQueryResponse.getTradeStatus();
                                    //查询订单状态
                                    if (AliPayConstants.ALI_PAY_SUCCESS.equals(alipayTradeQueryResponse.getCode()) && "TRADE_SUCCESS".equals(tradeStatus)) {
                                        System.out.println("----------------------订单模式收银台支付宝支付成功!  订单号：" + simpleOrdersById.getParentCode());
                                        aliPayStatus = true;
                                        sweepCodePay.setTradeStat(ActivityBeanUtils.SweepPayTradeStat.SUCCESS.toString());
                                        break;
                                    } else {
                                        System.out.println("----------------------订单模式收银台支付宝查询订单接口返回订单还未支付!  订单号：" + simpleOrdersById.getParentCode());
                                        remarks = alipayTradeQueryResponse.getSubMsg();
                                    }
                                }
                                //轮询结束，未支付成功，撤销订单
                                if (!aliPayStatus) {
                                    System.out.println("----------------------订单模式收银台支付宝支付，1分钟之内用户未支付，撤销订单！订单号：" + simpleOrdersById.getParentCode());
                                    remarks = "1分钟之内用户未支付,撤销订单!";
                                    // 撤销订单
                                    // 撤销支付
                                    paySweepCodeError(sweepCodePay, JSONObject.parseObject(JSONObject.toJSONString(response), Map.class)
                                            , simpleOrdersById, organizerPayConfigByOrgId, operatorAccount, organizer, remarks);

                                    // 释放支付分布式锁
                                    RedisUtil.releaseDistributedLock(offlinePayLockKey, requestId);
                                    return WebResult.getErrorMsgResult(WebResult.Code.CASHIER_NO_PAY_ERROR, alipayTradeQueryResponse.getSubMsg());
                                } else {
                                    //支付成功
                                    sweepCodePay.setTradeStat(ActivityBeanUtils.SweepPayTradeStat.SUCCESS.toString());
                                }
                            }
                        } else {
                            System.out.println("----------------------订单模式收银台支付宝支付接口通讯失败，返回信息：" + JSON.parseObject(response.getBody(), Map.class));
                            //撤销订单
                            paySweepCodeError(sweepCodePay, JSONObject.parseObject(JSONObject.toJSONString(response), Map.class)
                                    , simpleOrdersById, organizerPayConfigByOrgId, operatorAccount, organizer, response.getSubMsg());

                            // 释放支付分布式锁
                            RedisUtil.releaseDistributedLock(offlinePayLockKey, requestId);
                            return WebResult.getErrorMsgResult(WebResult.Code.CASHIER_NO_PAY_ERROR, response.getSubMsg());
                        }

                        extSweepCodeWay = "alipay";
                        transactionId = response.getTradeNo();

                        System.out.println(DateUtil.getDateTimeStr(new Date()) + "----------------------订单模式收银台支付宝支付, 订单号：" + simpleOrdersById.getCode() + "----------------- 线下支付宝扫码支付花费时间：" + (System.currentTimeMillis() - d));
                    }
                } else if (payType == 4) {
                    //微信支付
                    // 组装订单号&商户号
                    if (organizerPayConfigByOrgId != null && StringUtils.isNotBlank(organizerPayConfigByOrgId.getSubmchid())) {
                        paramMap.put("sub_mch_id", organizerPayConfigByOrgId.getSubmchid());
                    }
                    paramMap.put("out_trade_no", simpleOrdersById.getParentCode());

                    if (sweepCodePay != null) {
                        responseResult = WXPaymentCodeApi.orderQuery(paramMap);
                    } else {
                        long d = System.currentTimeMillis();
                        simpleOrdersById.setPayTypeId(payType);
                        //创建支付记录
                        sweepCodePay = orderSweepCodePayService.createPay(simpleOrdersById);

                        // 组装支付业务数据
                        paramMap.put("device_info", String.valueOf(organizer.getOrganizerId()));
                        paramMap.put("body", organizer.getOrganizerId() + "-" + simpleOrdersById.getChargeName() + "x" + simpleOrdersById.getChargeNum());
                        paramMap.put("total_fee", String.valueOf(simpleOrdersById.getTotalPrice() != null ? simpleOrdersById.getTotalPrice().multiply(new BigDecimal(100)).intValue() : 0));
                        paramMap.put("fee_type", "CNY");
                        paramMap.put("auth_code", authorcode);
                        responseResult = WXPaymentCodeApi.microPay(paramMap);

                        sweepCodePay.setPayWay(WXPaymentCodeApi.getPayWay(responseResult));
                        sweepCodePay.setTransactionId(WXPaymentCodeApi.getTransactionId(responseResult));
                        sweepCodePay.setTime(now);
                        if (!WXPaymentCodeApi.isMicroPaySuccess(responseResult)) {
                            if (!WXPaymentCodeApi.isSuccess(responseResult)) {
                                paySweepCodeError(sweepCodePay, responseResult, simpleOrdersById, organizerPayConfigByOrgId, operatorAccount, organizer, WXPaymentCodeApi.getErrMsg(responseResult));

                                // 释放支付分布式锁
                                RedisUtil.releaseDistributedLock(offlinePayLockKey, requestId);
                                return WebResult.getErrorMsgResult(WebResult.Code.CASHIER_NO_PAY_ERROR, WXPaymentCodeApi.getErrMsg(responseResult));
                            }

                            int count = 0;
                            Map<String, String> wxPost = null;
                            // 组装线下微信支付查询订单api业务数据
                            paramMap = new HashMap<>();
                            if (organizerPayConfigByOrgId != null && StringUtils.isNotBlank(organizerPayConfigByOrgId.getSubmchid())) {
                                paramMap.put("sub_mch_id", organizerPayConfigByOrgId.getSubmchid());
                            }
                            paramMap.put("out_trade_no", simpleOrdersById.getParentCode());
                            while (wxPost == null) {
                                try {
                                    Thread.sleep(1000);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }

                                wxPost = WXPaymentCodeApi.orderQuery(paramMap);

                                if (!WXPaymentCodeApi.isSuccess(wxPost)) {
                                    break;
                                }
                                /*if (!WXPaymentCodeApi.resultCode(wxPost)) {
                                    break;
                                }*/

                                if (WXPaymentCodeApi.isSuccessTradeStateQueryBySuccess(wxPost)) {
                                    break;
                                }

                                if (!WXPaymentCodeApi.isContinueTradeStateQueryByUserpaying(wxPost)) {
                                    break;
                                }
                                count++;
                                if (count > 60) {
                                    break;
                                }
                                wxPost = null;
                            }
                            if (!WXPaymentCodeApi.isQueryPayResultSuccess(wxPost)) {
                                // 撤销订单 (微信线下扫码支付失败)
                                // 撤销支付
                                paySweepCodeError(sweepCodePay, responseResult, simpleOrdersById, organizerPayConfigByOrgId, operatorAccount, organizer, WXPaymentCodeApi.getErrMsg(wxPost));

                                // 释放支付分布式锁
                                RedisUtil.releaseDistributedLock(offlinePayLockKey, requestId);
                                return WebResult.getErrorMsgResult(WebResult.Code.CASHIER_NO_PAY_ERROR, WXPaymentCodeApi.getErrMsg(responseResult));
                            } else {
                                responseResult = wxPost;
                                sweepCodePay.setTradeStat(ActivityBeanUtils.SweepPayTradeStat.SUCCESS.toString());
                            }
                        } else {
                            sweepCodePay.setTradeStat(ActivityBeanUtils.SweepPayTradeStat.SUCCESS.toString());
                        }

                        extSweepCodeWay = WXPaymentCodeApi.getPayWay(responseResult);
                        transactionId = WXPaymentCodeApi.getTransactionId(responseResult);

                        System.out.println(DateUtil.getDateTimeStr(new Date()) + "订单号：" + simpleOrdersById.getCode() + "----------------- 线下微信扫码支付花费时间：" + (System.currentTimeMillis() - d));
                    }
                } else if (payType == 13) { // 扫码支付
                    if (organizerPayConfigByOrgId == null) {
                        // 释放支付分布式锁
                        RedisUtil.releaseDistributedLock(offlinePayLockKey, requestId);
                        return WebResult.getErrorMsgResult(WebResult.Code.ERROR, "没有配置支付参数");
                    }
                    unipayOpShopId = organizerPayConfigByOrgId.getUnipayOpShopId();
                    if (unipayOpShopId == null &&
                            (StringUtils.isEmpty(organizerPayConfigByOrgId.getUnipayOpenapiMchId()) ||
                                    StringUtils.isEmpty(organizerPayConfigByOrgId.getUnipayOpenapiKey()))) {

                        // 释放支付分布式锁
                        RedisUtil.releaseDistributedLock(offlinePayLockKey, requestId);
                        return WebResult.getErrorMsgResult(WebResult.Code.ERROR, "支付配置不正确");
                    }

                    if (sweepCodePay != null) {
                        if (StringUtils.isEmpty(organizerPayConfigByOrgId.getUnipayOpenapiMchId())) {
                            responseResult = UnionPayApi.tradeQuery(sweepCodePay.getPayCode(), unipayOpShopId, organizerPayConfigByOrgId.getUnipayOpenapiMchId(),
                                    organizerPayConfigByOrgId.getUnipayOpenapiKey());
                        } else {
                            responseResult = UnionPayApi.openApiUnifiedTradeQuery(sweepCodePay.getPayCode(), organizerPayConfigByOrgId.getUnipayOpenapiMchId(),
                                    organizerPayConfigByOrgId.getUnipayOpenapiKey());
                        }
                    } else {

                        long d = System.currentTimeMillis();

                        simpleOrdersById.setPayTypeId(payType);
                        //创建支付记录
                        sweepCodePay = orderSweepCodePayService.createPay(simpleOrdersById);

                        int totalPrice = simpleOrdersById.getTotalPrice().multiply(BigDecimal.valueOf(100)).intValue();

                        if (StringUtils.isEmpty(organizerPayConfigByOrgId.getUnipayOpenapiMchId())) {
                            responseResult = UnionPayApi.pay(sweepCodePay.getPayCode(), totalPrice,
                                    simpleOrdersById.getActivityName(), unipayOpShopId, authorcode);
                        } else {
                            responseResult = UnionPayApi.openApiPay(sweepCodePay.getPayCode(), totalPrice,
                                    simpleOrdersById.getActivityName(), organizerPayConfigByOrgId.getUnipayOpenapiMchId(),
                                    organizerPayConfigByOrgId.getUnipayOpenapiKey(), authorcode);
                        }

                        sweepCodePay.setPayWay(UnionPayApi.getPayWay(responseResult));
                        sweepCodePay.setTransactionId(UnionPayApi.getTransactionId(responseResult));
                        sweepCodePay.setTime(now);
                        if (!UnionPayApi.isSuccess(responseResult) && !UnionPayApi.isNeedQuery(responseResult)) {
                            paySweepCodeError(sweepCodePay, responseResult, simpleOrdersById, organizerPayConfigByOrgId, operatorAccount, organizer, UnionPayApi.getErrMsg(responseResult));

                            // 释放支付分布式锁
                            RedisUtil.releaseDistributedLock(offlinePayLockKey, requestId);
                            return WebResult.getErrorMsgResult(WebResult.Code.CASHIER_NO_PAY_ERROR, UnionPayApi.getErrMsg(responseResult));
                        } else if (UnionPayApi.isResultSuccess(responseResult)) {
                            sweepCodePay.setTradeStat(ActivityBeanUtils.SweepPayTradeStat.SUCCESS.toString());
                        } else if (UnionPayApi.isNeedQuery(responseResult)) {
                            int count = 0;
                            Map<String, String> post = null;
                            while (post == null) {
                                try {
                                    Thread.sleep(1000);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }

                                post = UnionPayApi.tradeQuery(sweepCodePay.getPayCode(), unipayOpShopId, organizerPayConfigByOrgId.getUnipayOpenapiMchId(),
                                        organizerPayConfigByOrgId.getUnipayOpenapiKey());

                                if (!UnionPayApi.isContinueTradeQuery(post)) {
                                    break;
                                }
                                if (UnionPayApi.isSuccessTradeQuery(post)) {
                                    break;
                                }
                                count++;
                                if (count > 60) {
                                    break;
                                }
                                post = null;
                            }
                            if (!UnionPayApi.isSuccessTradeQuery(post)) {
                                // 撤销订单 (银联支付失败)
                                // 撤销支付
                                paySweepCodeError(sweepCodePay, responseResult, simpleOrdersById, organizerPayConfigByOrgId, operatorAccount, organizer, UnionPayApi.getErrMsg(post));

                                // 释放支付分布式锁
                                RedisUtil.releaseDistributedLock(offlinePayLockKey, requestId);
                                return WebResult.getErrorResult(WebResult.Code.CASHIER_NO_PAY_ERROR);
                            } else {
                                responseResult = post;
                                sweepCodePay.setTradeStat(ActivityBeanUtils.SweepPayTradeStat.SUCCESS.toString());
                            }
                        }
                        extSweepCodeWay = UnionPayApi.getPayWay(responseResult);
                        transactionId = UnionPayApi.getTransactionId(responseResult);

                        System.out.println(DateUtil.getDateTimeStr(new Date()) + "订单号：" + simpleOrdersById.getCode() + "----------------- 支付花费时间：" + (System.currentTimeMillis() - d));
                    }
                }
            }

            WebResult webResult = ordersService.offlinePay(receiptCode, trustees, trusteesPhone, entertainPersonName, entertainPersonPhone, operatorAccount, companyId, orderId, payType, receiptCode, payInCashMoney, payByCardMoney, payByWechatMoney, payByAlipayMoney, payByPreSaleMoney, payByBullMoney, depositOfflineMoney, depositOfflineType, organizer, companyName, transactionId, extSweepCodeWay);

            if (webResult.isSuccess()) {
                if (payType == 13 || payType == 4 || payType == 2) {
                    //订单支付完成
                    pushPayMessage(sweepCodePay, organizer.getOrganizerId(), operatorAccount.getCustomerId(), "支付成功", PAY_SUCCESS);
                }
            } else {
                if (payType == 13 || payType == 4 || payType == 2) {
                    //撤销支付
                    paySweepCodeError(sweepCodePay, responseResult, simpleOrdersById, organizerPayConfigByOrgId, operatorAccount, organizer, "offlinePay执行失败,支付失败");
                }
            }

            // 释放支付分布式锁
            RedisUtil.releaseDistributedLock(offlinePayLockKey, requestId);
            return webResult;
        } catch (Exception e) {
            e.printStackTrace();
            //扫码支付
            if (payType == 13 || payType == 4 || payType == 2) {
                //撤销支付
                paySweepCodeError(sweepCodePay, responseResult, simpleOrdersById, organizerPayConfigByOrgId, operatorAccount, organizer, "代码异常报错,支付失败");
            }
            // 释放支付分布式锁
            RedisUtil.releaseDistributedLock(offlinePayLockKey, requestId);
            return WebResult.getErrorMsgResult(WebResult.Code.ERROR, "系统异常");
        }
    }

    @ApiOperation(value = "每日结算出卡情况", notes = "每日结算出卡情况", httpMethod = "GET")
    @GetMapping("/statistics.json")
    public WebResult cashierDetail(HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        CommonAccount account = WebUpmsContext.getAccount(request);
        Date date = new Date();
        Date startTime = DateUtil.getStartTime(date);
        Date endTime = DateUtil.getEndTime(date);
        Integer cashierOrderCount = ordersService.getCashierOrderCount(organizer.getOrganizerId(), account.getCustomerId(), startTime, endTime);
        Integer todayOutCardCount = ordersService.getCashierOutCardCount(account.getCustomerId(), null, startTime, endTime);
        Integer notReturnCardCount = ordersService.getCashierOutCardCount(account.getCustomerId(), 1, startTime, endTime);
        Map<String, Integer> result = new HashMap<>();
        result.put("cashierOrderCount", cashierOrderCount);
        result.put("todayOutCardCount", todayOutCardCount);
        result.put("notReturnCardCount", notReturnCardCount);
        return WebResult.getSuccessResult("data", result);
    }

    public static void main(String[] args) {
        BigDecimal bigDecimal = new BigDecimal(0.1000);

        System.out.println(bigDecimal);
        System.out.println(bigDecimal.setScale(2, BigDecimal.ROUND_HALF_UP));
//        AlipayClient alipayClient= new DefaultAlipayClient("https://openapi.alipay.com/gateway.do","2021001182696591","MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCCudeKSTO8JTRhzgf2SjosSsbYU6nvgTFa/jN89Bhh9n6WOlelqG87WLXlHXsAYwDE5DSR8aSFTt/YFq8CB0/M2zTkepMM6K48KfjECq9zuEfewl0DwIC7ajQ4qH7M0XBViue7nfPIBF7Xy9XhiNfvXZDYIRNTg9M/bokNCOBCTXHTsPYZcf61xyXkwwizBJeKHpl3JyK/X6LpMbcBWkuVwGAOt7XOTjLP2FkuqsawAf7IbahK3hfoWDV127620ciMvXyMizv3X9YPIfr/X9b7yTCY+ymerorA9GD6F78c1oBnfyxWr/1FeohLZGYijTpGwiBOjQQt3PRZGkkGhS65AgMBAAECggEAU9MnSx01d2fD9xNON7fRJaWta/n59gI5y8dE7NL475hIA7zVhvxClyODP69vuAfD7n0G2K7CeFjjYITrI7ncidfmSrvun1B0qU+gnNTdPK7mJwkq68T8xzCK2+nvpRaHmdLAVaYrNA7ZYjbqpvxAutSclHphEiRtyTbv2tPeOaTUYiQtKVO848NEfLjTExJN5woQnsJc8sHRmT0abZfx2gu+X5cJzwig/duXN7g48zXoKwHt0pavD6DuHaAQqIJuFp0wJ03bTubOBhycf7qcQWiDcijO1NcEdKuy/zkBEINbE4CaE0vGZUg5lCbm782v2Z7XhXgO1IZ+tIhxwllLoQKBgQDBsOPZdkK0z7gAZ2L581hBMh99BEGA5YHjjqMC4MixXlBJFeptPULnDQ+zWgIv4aodkanVyIdoPJzyfgtYb58e9REEMENFcGN5ZED27hAGVXZeYLDpYfT/a7GqlfZQ8epJX8BCaOXuwySbUrlJWFveswVq/cP/0U0XE2DSi8el6wKBgQCsx5FRxdRDqeueJ8QPpqPNif90T7Fd4nNulTFCEra9u0czBbtwMvj4xqeTPivTaQg6S1FjwgGvGKb0BxQy9SjgHpkt0ZiaGTlMXEE/73Jw0GaT32wEgOq1g6eLlbes+gP5rlhX5dvVlNBzNP+MvWV039GoLgpX3SOCRHZqR2eg6wKBgB/akX4//rRxvNcEKbjllkwSjC8pNgSWugfMe26LvTW+RwHeAtG4U3pgIYbjdJyfRjTasc2gzvQLk9wlcoQ2xzp2HzhLH6UZ2KIsJKr3d9cy0uaujCm1nG8tLOjbBMlHH7q1bo79ZWMg4LJSpnozd9hmJKv10/gMWdtDqpa1uj3FAoGBAJOyeag0e7Jn0/G7vp8+bTIr8EKmSVGfXdu/D3+S7cIdEM0t7kAIYsqPTvrQn7g2RgFHF0k2tCfQ5A8g6gRxjEKiBYGkdVXtkAyVpHWaI/MKQ1XPHNBwv7Lw2024a24jvzg18HEbHiRi9Ro2yOmvkcYp+lhRGyhm8o7OJYfOiGiHAoGAK/cp3YoUdj7/JiOAoUpvJ4O2g/nZunikoYLCvX9HZ7fjgg1TasF3ZewlrHQIYOp6CyqVcJmooxIq7OkbTRcipxs1/wxRjYWyFt3X838MWs8hku1T9qdNdN8nPvk85IbYI6TzxjCt6UhjOSLCI0+0Gu0yVdBg9PVv1W9c0T2eyNs="
//                ,"JSON","UTF8",
//                "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAhKpCweTqg6cSyfbQWth+wknvNONLJ6JzH69xF2dslH5BdKhd73Gwyp1LoAAK4cHlh+GCIep3mfpf+Fc3+aW2zUp17oDrswi4OZdjhC5p5TCbNFP+5AGE6/3tSGE7Xtx/1HsaoHtNSsQk5s+NQ70u43nGO/xL2L0NnMBpmGNJUC3YbHzLCL077IF+RJ6cYtlkQNHZQWv7j1HwEc8rlSsFWTNum8TDt7EoPPUbb9auPe31M9kl5KMzNz70O7v7xFKDxqrRl8UTJlamEJC7n2MZCYyGmC+k5eh6m/6Bw/pbDMkFcmDPMpZWVqNJT1rmmAi+Swhu4J4gH9/pblcfWajhVwIDAQAB"
//                ,"RSA2","E5q7qgXltk/pcT/ln+T0TA==","AES");
//        AlipayTradeRefundRequest request=new AlipayTradeRefundRequest();
//        AlipayTradeRefundModel model=new AlipayTradeRefundModel();
//        model.setRefundAmount(String.valueOf(1));
//        model.setOutTradeNo("x11138224505092463232");
//        request.setBizModel(model);
//        request.setNeedEncrypt(true);
//        request.putOtherTextParam("app_auth_token","202209BB4e74f8ce2fcf49cea89c3f8cc2417F85");
//        AlipayTradeRefundResponse response= null;
//        try {
//            response = alipayClient.execute(request);
//        } catch (AlipayApiException e) {
//            throw new RuntimeException(e);
//        }
//        System.out.println(response.getBody());
        String body = "{\"sign\":\"FCudpVF3JgXVy5+ptYk8c3Zwe2GzUjUepv8tYJci03tEv2pw7E1WPDTjFKm7hVengj4rp9+KquW/SsGsD8v3Zrez/YhqKxqr6ysUtzqY1RmrPITjXE4Udif7EYsNvvm8WvcGzktzlxtFuU+Ptu3tNA69TwTDDLEAMCgeXxb8o8R35HtSwTXh2TMJWMyONwjtzJdmBKK72kUgxxse0QI9lHTn35OCa11GnGO8Bm77oQ7G0ZuF+d8f86bu5uyPIwM/r9S8J48oIlvOBZjmE0dXCxVlaEP5IBPu+tk2SXLvuBYO4OsM8JT5Cv8gT8v02g95Tn8vKI4On6ZknOFTIKinvA==\",\"alipay_trade_pay_response\":{\"msg\":\"Business Failed\",\"code\":\"40004\",\"sub_msg\":\"支付失败，获取顾客账户信息失败，请顾客刷新付款码后重新收款，如再次收款失败，请联系管理员处理。[SOUNDWAVE_PARSER_FAIL]\",\"sub_code\":\"ACQ.PAYMENT_AUTH_CODE_INVALID\",\"receipt_amount\":\"0.00\",\"point_amount\":\"0.00\",\"buyer_pay_amount\":\"0.00\",\"invoice_amount\":\"0.00\"}}";
        Map<String, String> responseMap = JSON.parseObject(body, Map.class);
        Set keyset = responseMap.keySet();
        Iterator iterator = keyset.iterator();
        String errorMsg = "";
        while (iterator.hasNext()) {
            Object key = iterator.next();
            if (!key.equals("sign")) {
                JSONObject jsonObject = JSON.parseObject(JSONObject.toJSONString(responseMap.get(key)));
                if (jsonObject.containsKey("code")) {
                    errorMsg = errorMsg + jsonObject.getString("code");
                }
                if (jsonObject.containsKey("msg")) {
                    errorMsg = errorMsg + jsonObject.getString("msg");
                }
                if (jsonObject.containsKey("sub_code")) {
                    errorMsg = errorMsg + jsonObject.getString("sub_code");
                }
                if (jsonObject.containsKey("sub_msg")) {
                    errorMsg = errorMsg + jsonObject.getString("sub_msg");
                }
            }

        }
        System.out.println(errorMsg);
    }
}
