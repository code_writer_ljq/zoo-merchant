package com.zoo.zoomerchantadminweb.cashier.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alipay.api.AlipayClient;
import com.alipay.api.domain.AlipayTradeCancelModel;
import com.alipay.api.domain.AlipayTradePayModel;
import com.alipay.api.request.AlipayTradeCancelRequest;
import com.alipay.api.request.AlipayTradePayRequest;
import com.alipay.api.response.AlipayTradeCancelResponse;
import com.alipay.api.response.AlipayTradePayResponse;
import com.alipay.api.response.AlipayTradeQueryResponse;
import com.pay.unionpay.api.AliPayApi;
import com.pay.unionpay.api.UnionPayApi;
import com.pay.unionpay.api.WXPaymentCodeApi;
import com.pay.unionpay.sdk.alipay.AliPayConstants;
import com.zoo.account.constant.AccountEnum;
import com.zoo.account.service.AccountService;
import com.zoo.account.support.AccountResult;
import com.zoo.activity.dao.model.*;
import com.zoo.activity.dao.nutz.pojo.CommonAccount;
import com.zoo.activity.service.*;
import com.zoo.activity.util.DateUtil;
import com.zoo.activity.util.PlayerUtil;
import com.zoo.activity.util.XMLUtil;
import com.zoo.activity.util.bean.OrderResultBean;
import com.zoo.activity.vo.OrderModel;
import com.zoo.activity.vo.ScheduleCoach;
import com.zoo.bean.util.ActivityBeanUtils;
import com.zoo.common.util.RedisUtil;
import com.zoo.finance.dao.model.ZooOnAccountCompany;
import com.zoo.finance.dao.model.ZooOnAccountCompanyExample;
import com.zoo.finance.service.ZooOnAccountCompanyService;
import com.zoo.icenow.service.StorageService;
import com.zoo.web.bean.WebResult;
import com.zoo.wechat.common.Config;
import com.zoo.wechat.pay.WxPayApi;
import com.zoo.zoomerchantadminweb.config.websocket.WebSocketEndPoint;
import com.zoo.zoomerchantadminweb.context.WebUpmsContext;
import com.zoo.zoomerchantadminweb.order.controller.SweepPayUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import jodd.util.StringUtil;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.net.UnknownHostException;
import java.util.*;

/**
 * Created by ljq on 2018/8/27.
 */
@Api(value = "api/server/coach/cashier", tags = {"【教练系统】收银台接口"}, description = "收银员相关(教练系统线下下单、支付操作相关)")
@RestController
@RequestMapping("api/server/coach/cashier")
public class CoachCashierController {

    @Autowired
    private OrdersService ordersService;

    @Autowired
    private CoachService coachService;

    @Autowired
    private EpisodeService episodeService;

    @Autowired
    private ChargeService chargeService;

    @Autowired
    private ChargeRulesService chargeRulesService;

    @Autowired
    private CourseService courseService;

    @Autowired
    private ScheduleCoachService scheduleCoachService;

    @Autowired
    private OrderHistoryService orderHistoryService;

    @Autowired
    private OrderCoachExtendService orderCoachExtendService;

    @Autowired
    private CoachGuideRelationshipsService coachGuideRelationshipsService;

    @Autowired
    private ActivityService activityService;

    @Autowired
    private MailService mailService;

    @Autowired
    private OrderCoachExtendListViewService orderCoachExtendListViewService;

    @Autowired
    private StorageService storageService;

    @Autowired
    private PlayerService playerService;

    @Autowired
    private AccountService accountService;

    @Autowired
    private ZooOnAccountCompanyService zooOnAccountCompanyService;

    @Autowired
    private VerifyService verifyService;

    @Autowired
    private OrganizerPayConfigService organizerPayConfigService;

    @Autowired
    private WebSocketEndPoint webSocketEndPoint;

    /**
     * 扫码订单支付记录
     */
    @Autowired
    private OrderSweepCodePayService orderSweepCodePayService;

    /**
     * 支付失败
     */
    final String PAY_ERROR = "PAY_ERROR";

    /**
     * 支付成功
     */
    final String PAY_SUCCESS = "PAY_SUCCESS";

    /**
     * 重新支付
     */
    final String PAY_RETRY = "PAY_RETRY";

    /**
     * 时间段已有订单
     */
    final String COACH_APPOINTMENT_TIME_HAS_ORDER = "COACH_APPOINTMENT_TIME_HAS_ORDER";

    /**
     * 教练请假
     */
    final String COACH_APPOINTMENT_TIME_HAS_LEACE = "COACH_APPOINTMENT_TIME_HAS_LEACE";

    /**
     * 首页 - 排班下单
     *
     * @param orderModel
     * @param appointmentTime
     * @param coachId
     * @param gender
     * @param board
     * @param session
     * @param request
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "首页排班下单接口", notes = "点击下单调用接口", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "episodeId", value = "场次ID(episodeId)", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "chargeId", value = "票劵ID(chargeId)", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "chargeNum", value = "下单数量,默认值传1", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "playerEnrollInfos", value = "报名人信息([{\"real_name\":\"sad \",\"is_default_player\":false,\"phone\":\"15011490302\"},{},{}])", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "appointmentTime", value = "预约时间", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "coachId", value = "教练ID", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "gender", value = "性别(如果不选择性别就值为''，否则女0，男1)", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "board", value = "技术类型(单板或冰球：1，双板或花滑：2  单板&双板或冰球&花滑：3)", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "number", value = "教学人数ID", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "duration", value = "教学时长ID", required = true, dataType = "int", paramType = "query"),
    })
    @RequestMapping(value = "/addManualOrder.json", method = RequestMethod.POST)
    public Object addManualOrder(OrderModel orderModel,
                                 @RequestParam(value = "appointmentTime", required = false) String appointmentTime,
                                 @RequestParam(value = "coachId", required = false) Integer coachId,
                                 @RequestParam(value = "gender", required = false) Integer gender,
                                 @RequestParam(value = "board", required = false) Integer board,
                                 HttpSession session, HttpServletRequest request) throws Exception {
        boolean isAssignTime = true;
        int episodeId = orderModel.getEpisodeId();
        int chargeNum = orderModel.getChargeNum();
        int chargeId = Integer.parseInt(orderModel.getChargeId());

        Organizer organizer = WebUpmsContext.getOrgnization(request);
        CommonAccount customer = WebUpmsContext.getAccount(request);
        if (customer == null) {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
        if (organizer == null) {
            return WebResult.getErrorResult(WebResult.Code.COACH_GET_ORGANIZER_INFO_ERROR);
        }

        // TODO 报名人处理
        String players = request.getParameter("playerEnrollInfos");
        // logger.info("== 报名信息(playerEnrollInfos): " + players);
        List<Player> playerModelList = PlayerUtil.formatPlayerFromString(players);
        // logger.info("== formatPlayerFromString: " + JSON.toJSONString(playerModelList));
        if (playerModelList.size() == 0) {
            // 报名信息有误，请重新下单
            return WebResult.getErrorResult(WebResult.Code.CASHIER_INVALID_REGISTRATION_FORM);
        }

        OrderCoachExtend orderCoachExtend = new OrderCoachExtend();
        ScheduleCoach scheduleCoach;
        try {
            // TODO 处理预约时间
            Date appointmentDateTime;
            if (StringUtils.isBlank(appointmentTime)) {
                isAssignTime = false;
                appointmentDateTime = new Date();
                appointmentTime = DateUtil.getDateTimeStr(appointmentDateTime);
            } else {
                appointmentDateTime = DateUtil.convertSimpleStringToDateTimeWithoutSecond(appointmentTime);
            }

            // TODO: 处理报名人和主办方ID 开始
//            List<Player> playerList = new ArrayList<>();
            int defaultPlayerIndex = 0;
//            for (int i = 0; i < playerModelList.size(); i++) {
//                PlayerModel playerModel = playerModelList.get(i);
//                Player player = new Player();
//                BeanUtilsExtends.copyProperties(player, playerModel);
//                playerList.add(player);
//            }
            Integer client = 0;
            if (organizer.getOrganizerId() != null) {
                client = organizer.getOrganizerId();
            }

            // TODO 设置orderCoachExtend信息
            orderCoachExtend.setAppointmentTime(appointmentDateTime);
            orderCoachExtend.setBoard(board);
            orderCoachExtend = courseService.getExtendChargeInfo(orderCoachExtend, chargeId);

            // TODO: 处理教练出导记录 开始
            if (coachId != null) {
                if (!coachService.checkCoachCanAppoint(coachId, appointmentDateTime, orderCoachExtend.getDuration(), organizer, null, 1)) {
                    // 您排导的教练在该时段已有订单,请重新下单
                    return WebResult.getErrorResult(WebResult.Code.COACH_APPOINTMENT_TIME_HAS_ORDER);
                }

//                List<CoachRest> coachRests = coachService.getCoachRest(coachId, appointmentDateTime);
                if (!coachService.checkCoachRestCanAppoint(coachId, appointmentDateTime, orderCoachExtend.getDuration(), organizer, null, 1)) {
                    // 该教练在该时段处于请假状态,无法排导
                    return WebResult.getErrorResult(WebResult.Code.COACH_APPOINTMENT_TIME_HAS_LEACE);
                }

                Coach coach = coachService.findCoach(coachId);
                scheduleCoach = new ScheduleCoach();
                scheduleCoach.setRealname(coach.getRealname());
                scheduleCoach.setCoachId(coachId);
            } else {
                //排导(包含设置出导教练ID)
                scheduleCoach = scheduleCoachService.scheduleCoach(organizer, gender, episodeId, orderCoachExtend, isAssignTime);
            }

            if (scheduleCoach == null) {
                // 快捷下单没有找到合适的教练
                return WebResult.getErrorResult(WebResult.Code.COURSE_SCHEDULE_NO_COACH);
            } else {
                orderCoachExtend.setOrderCoach(scheduleCoach.getCoachId());
            }

            // TODO: 创建订单以及教练出导记录
            OrdersResult result;
            List<Integer> chargeIdList = new ArrayList<>();
            chargeIdList.add(chargeId);
            result = ordersService.createOrder(client, customer, episodeId, chargeNum, chargeIdList, playerModelList, appointmentDateTime, defaultPlayerIndex, 0, null, null, null, "线下支付", orderCoachExtend, null);
            if (result.getOrders() == null) {
                return WebResult.getErrorMsgResult(WebResult.Code.COACH_APPOINTMENT_TIME_ERROR, result.getMessage());
            } else {
                //防止指定教练免费订单发送短信失败
                if (result.getOrders().getStatus() == 1) {
                    saveOrderHistory("create", customer, result.getOrders());
                }
                OrderCoachExtend coachExtend = (OrderCoachExtend) result.getResultMap().get("orderCoachExtend");

                Map<String, Object> resultMap = new HashMap<>();
                resultMap.put("orderId", result.getOrders().getOrderId().toString());
                resultMap.put("code", result.getOrders().getCode());
                resultMap.put("payPrice", result.getOrders().getTotalPrice());
                resultMap.put("coachName", scheduleCoach.getRealname());
                resultMap.put("extendId", coachExtend != null ? coachExtend.getExtendId() : null);
                return WebResult.getSuccessResult("data", resultMap);
            }
        } catch (IllegalAccessException e) {
            //logger.error("player从model转换失败：" + e.getMessage());
            // 排导出现问题，请重新尝试!
            e.printStackTrace();
            return WebResult.getErrorResult(WebResult.Code.COACH_SCHEDULE_FAIL);
        } catch (InvocationTargetException e) {
            //logger.error("player从model转换失败：" + e.getMessage());
            // 排导出现问题，请重新尝试!
            e.printStackTrace();
            return WebResult.getErrorResult(WebResult.Code.COACH_SCHEDULE_FAIL);
        } catch (Exception e) {
            //logger.error(AssUtils.parse(e));
            // 排导出现问题，请重新尝试!
            e.printStackTrace();
            return WebResult.getErrorResult(WebResult.Code.COACH_SCHEDULE_FAIL);
        }
    }

    private void saveOrderHistory(String action, CommonAccount account, Orders orders) {
        OrderHistory createHistory = new OrderHistory();
        createHistory.setAction(action);
        createHistory.setCustomerId(account.getCustomerId());
        createHistory.setMessage("Create order @ " + DateUtil.dateToDateString(new Date()) + " code: " + orders.getCode() + " by " + account.getCustomerId());
        createHistory.setOrderId(orders.getOrderId());
        createHistory.setResponse(1);
        createHistory.setTime(new Date());
        orderHistoryService.saveOrderHistory(createHistory);
    }

    @ApiOperation(value = "【首页】排班下单时跳过(切换)教练", notes = "排班下单过程中点击跳过(切换)教练", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "extendId", value = "出导ID(extendId)", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "gender", value = "性别(如果不选择性别就值为''，否则女0，男1)", required = false, dataType = "int", paramType = "query"),
    })
    @RequestMapping(value = "/skipSchedule.json", method = RequestMethod.POST)
    public Object skipSchedule(@RequestParam("extendId") Integer extendId,
                               @RequestParam(value = "gender", required = false) Integer gender,
                               HttpSession session, HttpServletRequest request) throws Exception {
        Organizer organizer = WebUpmsContext.getOrgnization(request);

        OrderCoachExtend orderCoachExtend = coachService.getCoachExtendByExtendId(extendId);
        OrderView orderView = ordersService.getOrderDetail(orderCoachExtend.getOrderId());
        ScheduleCoach scheduleCoach = scheduleCoachService.skipScheduleCoach(organizer, gender, orderView.getEpisodeId(), orderCoachExtend);
        if (scheduleCoach != null) {
            orderCoachExtend.setOrderCoach(scheduleCoach.getCoachId());
            if (orderCoachExtendService.updateByPrimaryKeySelective(orderCoachExtend)) {
                Map<String, Object> resultMap = new HashMap<>();
                resultMap.put("coachName", scheduleCoach.getRealname());

                return WebResult.getSuccessResult("data", resultMap);
            }
        }

        // 切换教练失败
        return WebResult.getErrorResult(WebResult.Code.COURSE_SKIP_COACH_FAIL);
    }

    /**
     * 排班下单-教练确认预约订单
     *
     * @param extendId
     * @param session
     * @param request
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "【排班下单】确认预约接口", notes = "教练确认预约订单", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "extendId", value = "出导ID(extendId)", required = true, dataType = "int", paramType = "query")
    })
    @RequestMapping(value = "/comfirmSchedule.json", method = RequestMethod.POST)
    public Object comfirmSchedule(@RequestParam(value = "extendId", required = true) Integer extendId,
                                  HttpSession session, HttpServletRequest request) throws Exception {
        if (coachService.updateCoach(extendId)) {
            return WebResult.getSuccessResult();
        } else {
            //确认排导失败
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }
    }

    /**
     * 【首页】排班下单，根据选择预约时间、教学时长、教学人数、预约课程ID，去筛选可排导的教练
     *
     * @param courseId
     * @param appointTime
     * @param board
     * @param session
     * @return
     */
    @ApiOperation(value = "【首页】排班下单，根据选择预约时间、教学时长、教学人数、预约课程ID，去筛选可排导的教练", notes = "根据选择预约时间、教学时长、教学人数、预约课程ID，去筛选可排导的教练", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "courseId", value = "课程ID(episodeId)", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "duration", value = "教学时长", required = false, dataType = "double", paramType = "query"),
            @ApiImplicitParam(name = "appointTime", value = "预约时间(时间戳)", required = false, dataType = "long", paramType = "query"),
            @ApiImplicitParam(name = "board", value = "技术类型(值为：1、2、3)", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "type", value = "课程类型（2：团课）", required = false, dataType = "int", paramType = "query")
    })
    @RequestMapping(value = "/coach-status-order.json", method = RequestMethod.GET)
    public Object getCoaches(@RequestParam(value = "courseId", required = false) Integer courseId,
                             @RequestParam(value = "duration", required = false) Double durat,
                             @RequestParam(value = "appointTime", required = false) long appointTime,
                             @RequestParam(value = "board", required = false) Integer board,
                             @RequestParam(value = "type", required = false, defaultValue = "0") Integer type,
                             HttpServletRequest request, HttpSession session) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null) {
            return WebResult.getErrorResult(WebResult.Code.COACH_GET_ORGANIZER_INFO_ERROR);
        }

        Double duration = durat;
        if (type == 0) {
            if (durat != null) {
                ChargeRules chargeRule = chargeRulesService.selectByPrimaryKey(durat.intValue());
                if (chargeRule != null) {
                    duration = chargeRule.getValue();
                }
            }
        }

        Date time = new Date(appointTime);
        List<Map> resultMap = coachService.getCoachStatusAndOrderCountMap(courseId, time, duration, 1, organizer, board, type);
        return WebResult.getSuccessResult("data", resultMap);
    }

    /**
     * 教练系统 - frequency线下创建订单
     * 统一的事务处理-->createOrder()   episodeId=1&&chargeNum=2&&chargeId=1
     */
    @ApiOperation(value = "线下创建订单(下单按钮调用接口)", notes = "教练系统 - 线下创建订单", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "appointmentTime", value = "预约时间", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "incrementInfoList", value = "租赁物信息", required = false, dataType = "string", paramType = "query"),
    })
    @RequestMapping(value = "/confirmOrder.json", method = RequestMethod.POST)
    public Object confirmOrder(OrderModel orderModel,
                               @RequestParam(value = "appointmentTime", required = false) String appointmentTime,
                               @RequestParam(value = "incrementInfoList", required = false) String incrementInfoList,
                               HttpSession session, HttpServletRequest request) throws Exception {
        CommonAccount customer = WebUpmsContext.getAccount(request);
        if (customer == null) {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
        Integer episodeId = orderModel.getEpisodeId();
        Integer rent = orderModel.getRent();
        Integer chargeNum = orderModel.getChargeNum();

        List<Integer> chargeIdList = new ArrayList<>();
        JSONArray chargeIdArray = JSON.parseArray(orderModel.getChargeId());
        for (Object chargeId : chargeIdArray) {
            chargeIdList.add(Integer.parseInt(chargeId.toString()));
        }
        String items = orderModel.getItems();

        Date appointmentDateTime = null;
        for (Integer chargeId : chargeIdList) {
            /*判断该票种是否限制购买人数*/
            Charge charge = chargeService.getChargeWithBlobsById(chargeId);
            //如果该票种限制购买数量
            if (charge.getBuyLimitType() == 1) {
                //当前提交的订单的购买数量大于限制购买数量
                if (charge.getBuyLimitCount() < chargeNum) {
                    // 该票种限制购买用户数量，对于每个微信用户最多购买" + charge.getBuyLimitCount() + "张"
                    return WebResult.getErrorResult(WebResult.Code.CASHIER_CHARGE_LIMIT);
                } else {
                    //当前用户已经购买成功的数量
                    Integer buyCount = ordersService.getOrderCount(customer.getCustomerId(), chargeId, 1);
                    if (null != buyCount && charge.getBuyLimitCount() < (chargeNum + buyCount)) {
                        // 该票种限制购买用户数量，对于每个微信用户最多购买" + charge.getBuyLimitCount() + "张"
                        return WebResult.getErrorResult(WebResult.Code.CASHIER_CHARGE_LIMIT);
                    }
                }
            }

            if (StringUtils.isNotBlank(appointmentTime)) {
                appointmentDateTime = com.zoo.util.data.DateUtil.convertSimpleStringToDate(appointmentTime);
            }

            if (appointmentDateTime != null && StringUtils.isNotBlank(charge.getBuyExceptLimit())) {
                List<String> buyExceptStr = JSON.parseArray(charge.getBuyExceptLimit(), String.class);
                String appointStr = appointmentTime.substring(0, 10);
                if (buyExceptStr.size() > 0 && buyExceptStr.contains(appointStr)) {
                    // 该产品无法在您所选的时间预定"
                    // res.setData("/wechat/activity/episode-detail?episodeId=" + episodeId);
                    return WebResult.getErrorResult(WebResult.Code.CASHIER_INVALID_TIME);
                }
            }
        }

        Boolean isMultiOrder = false;
        String players = request.getParameter("playerEnrollInfos");
        // logger.info("== 报名信息(playerEnrollInfos): " + players);
        List<Player> playerModelList = PlayerUtil.formatPlayerFromString(players);
        // logger.info("== formatPlayerFromString: " + JSON.toJSONString(playerModelList));

        Map<Integer, Integer> integerIntegerMap = new HashMap<>();

        if (StringUtils.isNotBlank(incrementInfoList)) {
            JSONArray jsonArray = JSONArray.parseArray(incrementInfoList);
            if (null != jsonArray) {
                Iterator<Object> iterator = jsonArray.iterator();
                while (iterator.hasNext()) {
                    JSONObject jsonObject = (JSONObject) iterator.next();
                    Integer chargeId = jsonObject.getInteger("id");
                    Integer count = jsonObject.getInteger("number");
                    if (null != count && count > 0) {
                        isMultiOrder = true;
                        Charge incrementCharge = chargeService.getChargeById(chargeId);
                        if (incrementCharge.getBuyLimitType() == 1) {
                            if (incrementCharge.getBuyLimitCount() < count) {
                                // "限制购买用户数量，对于每个微信用户最多购买" + incrementCharge.getBuyLimitCount() + "张"
                                return WebResult.getErrorResult(WebResult.Code.CASHIER_CHARGE_LIMIT);
                            } else {
                                //当前用户已经购买成功的数量
                                Integer buyCount = ordersService.getOrderCount(customer.getCustomerId(), chargeId, 1);
                                if (null != buyCount && incrementCharge.getBuyLimitCount() < (count + buyCount)) {
                                    // "限制购买用户数量，对于每个微信用户最多购买" + incrementCharge.getBuyLimitCount() + "张"
                                    return WebResult.getErrorResult(WebResult.Code.CASHIER_CHARGE_LIMIT);
                                }
                            }
                        }
                        integerIntegerMap.put(chargeId, count);
                    }
                }
            }
        }
//        Map<Integer, List> incrementInfoMap = new HashMap<>();
//        for (PlayerModel playerModel : playerModelList) {
//            if (null != playerModel.getIncrementChargeId()) {
//                isMultiOrder = true;
//                String[] incrementIds = playerModel.getIncrementChargeId().split(",");
//                for (String incrementId : incrementIds) {
//                    if (incrementInfoMap.containsKey(Integer.parseInt(incrementId))) {
//                        incrementInfoMap.get(Integer.parseInt(incrementId)).add(playerModel);
//                    } else {
//                        List<PlayerModel> playerModels = new ArrayList<>();
//                        playerModels.add(playerModel);
//                        incrementInfoMap.put(Integer.parseInt(incrementId), playerModels);
//                    }
//                }
//            }
//        }
//
//        for (Integer incrementChargeId : incrementInfoMap.keySet()) {
//            Charge incrementCharge = chargeService.getChargeById(incrementChargeId);
//            if (incrementCharge.getBuyLimitType() == 1) {
//                if (incrementCharge.getBuyLimitCount() < incrementInfoMap.get(incrementChargeId).size()) {
//                    res.setStatus(Status.error);
//                    res.setMsg(incrementCharge.getChargeName() + "限制购买用户数量，对于每个微信用户最多购买" + incrementCharge.getBuyLimitCount() + "张");
//                    return res;
//                } else {
//                    //当前用户已经购买成功的数量
//                    Integer buyCount = ordersService.getOrderCount(customer.getCustomerId(), incrementChargeId, 1);
//                    if (null != buyCount && incrementCharge.getBuyLimitCount() < (incrementInfoMap.get(incrementChargeId).size() + buyCount)) {
//                        res.setStatus(Status.error);
//                        res.setMsg(incrementCharge.getChargeName() + "限制购买用户数量，对于每个微信用户最多购买" + incrementCharge.getBuyLimitCount() + "张");
//                        return res;
//                    }
//                }
//            }
//        }

        try {
            if (playerModelList.size() == 0) {
                // 报名信息有误，请重新下单
                return WebResult.getErrorResult(WebResult.Code.CASHIER_INVALID_REGISTRATION_FORM);
            }

//            List<Player> playerList = new ArrayList<>();

            int defaultPlayerIndex = 0;
//            for (int i = 0; i < playerModelList.size(); i++) {
//                PlayerModel playerModel = playerModelList.get(i);
//                Player player = new Player();
//                BeanUtilsExtends.copyProperties(player, playerModel);
//                playerList.add(player);
//                // 标记默认联系人
//                if (playerModel.getIsDefaultPlayer()) {
//                    defaultPlayerIndex = i;
//                }
//            }

            Integer client = 0;
            if (customer.getOrganizerId() != null) {
                client = customer.getOrganizerId();
            }

            OrdersResult result;
            if (!isMultiOrder) {
                result = ordersService.createOrder(client, customer, episodeId, chargeNum, chargeIdList, playerModelList, appointmentDateTime, defaultPlayerIndex, rent, items, null, null, "线下支付", null, null);
            } else {
                result = ordersService.createMultiOrder(client, customer, episodeId, chargeNum, chargeIdList.get(0), playerModelList, integerIntegerMap, appointmentDateTime, defaultPlayerIndex, "线下支付", null);
            }
            if (result.getOrders() == null) {
                // 老系统返回具体下单错误信息 ==> result.getMessage()
                return WebResult.getErrorResult(WebResult.Code.CASHIER_CREATE_ORDER_ERROR);
            } else {
                Map<String, Object> resultMap = new HashMap<>();
                resultMap.put("orderId", result.getOrders().getOrderId().toString());
                resultMap.put("code", result.getOrders().getCode());
                resultMap.put("multi", isMultiOrder ? "1" : "0");
                BigDecimal ticketPrice = result.getOrders().getTotalPrice().subtract(result.getOrders().getDeposit() == null ? new BigDecimal(0) : result.getOrders().getDeposit());
                resultMap.put("totalPrice", ticketPrice.doubleValue());
                resultMap.put("payPrice", result.getOrders().getTotalPrice().doubleValue());
                resultMap.put("deposit", (result.getOrders().getDeposit() == null ? new BigDecimal(0) : result.getOrders().getDeposit()).doubleValue());
                return WebResult.getSuccessResult("data", resultMap);
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            return WebResult.getErrorResult(WebResult.Code.CASHIER_CREATE_ORDER_ERROR);
        } catch (InvocationTargetException e) {
            e.printStackTrace();
            return WebResult.getErrorResult(WebResult.Code.CASHIER_CREATE_ORDER_ERROR);
        } catch (Exception e) {
            e.printStackTrace();
            return WebResult.getErrorResult(WebResult.Code.CASHIER_CREATE_ORDER_ERROR);
        }
    }

    /**
     * 查询支付结果
     *
     * @param orderId
     * @param request
     * @return
     */
    @RequestMapping(value = "/offlineSweepCodePayResult.json", method = RequestMethod.POST)
    public Object offlineSweepCodePayResult(@RequestParam(value = "orderId", required = false) Integer orderId,
                                            @RequestParam(value = "payType", required = false) Integer payType,
                                            @RequestParam(value = "receiptCode", required = false, defaultValue = "") String receiptCode,
                                            @RequestParam(value = "payInCashMoney", required = false) BigDecimal payInCashMoney,
                                            @RequestParam(value = "payByCardMoney", required = false) BigDecimal payByCardMoney,
                                            @RequestParam(value = "payByWechatMoney", required = false) BigDecimal payByWechatMoney,
                                            @RequestParam(value = "payByAlipayMoney", required = false) BigDecimal payByAlipayMoney,
                                            @RequestParam(value = "payByPreSaleMoney", required = false) BigDecimal payByPreSaleMoney,
                                            @RequestParam(value = "payByBullMoney", required = false) BigDecimal payByBullMoney,
                                            @RequestParam(value = "depositOfflineMoney", required = false) BigDecimal depositOfflineMoney,
                                            @RequestParam(value = "depositOfflineType", required = false) Integer depositOfflineType,
                                            @RequestParam(value = "companyId", required = false) Integer companyId,
                                            @RequestParam(value = "trustees", required = false) String trustees,
                                            @RequestParam(value = "trusteesPhone", required = false) String trusteesPhone,
                                            @RequestParam(value = "entertainPersonName", required = false) String entertainPersonName,
                                            @RequestParam(value = "entertainPersonPhone", required = false) String entertainPersonPhone,
                                            @RequestParam(value = "companyName", required = false) String companyName,
                                            @RequestParam(value = "authorcode", required = false) String authorcode,
                                            HttpSession session, HttpServletRequest request) {
        Orders order = ordersService.getSimpleOrdersById(orderId);
        CommonAccount commonAccount = WebUpmsContext.getAccount(request);
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        //String unipayOpShopId = organizerPayConfigService.getUnipayOpShopId(organizer.getOrganizerId());

        OrganizerPayConfig organizerPayConfigByOrgId = organizerPayConfigService.getOrganizerPayConfigByOrgId(organizer.getOrganizerId());

        if (organizerPayConfigByOrgId == null) {
            return WebResult.getErrorMsgResult(WebResult.Code.ERROR, "没有配置支付参数");
        }

        Map<String, String> post = UnionPayApi.tradeQuery(order.getCode(), organizerPayConfigByOrgId.getUnipayOpShopId(), organizerPayConfigByOrgId.getUnipayOpenapiMchId(),
                organizerPayConfigByOrgId.getUnipayOpenapiKey());
        if (post != null) {
            if (order.getStatus() == 1) {
                return WebResult.getSuccessResult("payResult", 1);
            } else {
                // 银联已经支付成功，修改订单状态
                return WebResult.getSuccessResult("payResult", 2);
                /*try{
                    //扫码支付
                    String extSweepCodeWay = UnionPayApi.getPayWay(post);
                    String transactionId = UnionPayApi.getTransactionId(post);

                    Charge charge = chargeService.getChargeById(order.getChargeId());
                    Episode episode = episodeService.getEpisodeDetailById(charge.getEpisodeId());
                    Activity activity = activityService.getActivityById(episode.getActivityId());

                    CommonAccount operatorAccount = WebUpmsContext.getAccount(request);
                    OrderResultBean orderResult = ordersService.payOrder(receiptCode,trustees,trusteesPhone,entertainPersonName,entertainPersonPhone,operatorAccount,companyId,null, order.getTotalPrice(), transactionId, order, activity, episode, charge, commonAccount, receiptCode, payType,
                            payInCashMoney, payByCardMoney, payByWechatMoney, payByAlipayMoney, payByPreSaleMoney, payByBullMoney, depositOfflineMoney, depositOfflineType,companyName,extSweepCodeWay);
                    // logger.info(orderResult.getMsg());
                    if (orderResult.getResultCount().equals(1)) {
                        // 下单即核销，改为同步
                        verifyService.verify(order.getOrderId(), 1, activity.getTypeId(), order.getSellerId(), String.valueOf(operatorAccount.getCustomerId()), 1, null);

                        episodeService.confirmPay(episode, charge, order, null);

                        //删除 Quartz Task
                        List<Quartz> quartzList = quartzService.getQuartzByTypeAndTargetId(5, order.getOrderId());
                        // logger.info("WechatPay callback: Delete quartz task. QuartzList: " + JSON.toJSONString(quartzList));
                        for (Quartz quartz : quartzList) {
                            QuartzManager.removeJob(quartz.getQuartzName());
                            quartzService.deleteQuartzTask(quartz.getQuartzId());
                        }

                        String noticeEmail = organizer.getEmail();
                        if (StringUtils.isNotBlank(episode.getNoticeEmail())) {
                            noticeEmail = episode.getNoticeEmail();
                        }
                        mailService.sendMail(noticeEmail, "滑雪族新订单", ("您的" + activity.getTitle() + "产品有新的支付订单，请前往 http://" + Config.instance().getDomain() + "/server/order-detail?orderId=" + order.getOrderId() + "  进行查看。"));

                        return WebResult.getSuccessResult("payResult",1);
                    } else {

                        //撤销支付
                        try {
                            UnionPayApi.cancelPay(order.getCode(),unipayOpShopId);
                        } catch (UnknownHostException ex) {
                            ex.printStackTrace();
                        }
                        order.setStatus(4);
                        ordersService.updateOrders(order);

                        return WebResult.getSuccessResult("payResult",3);
                    }*/
                /*}catch (Exception e){
                    //撤销支付
                    try {
                        UnionPayApi.cancelPay(order.getCode(),unipayOpShopId);
                    } catch (UnknownHostException ex) {
                        ex.printStackTrace();
                    }
                    order.setStatus(4);
                    ordersService.updateOrders(order);
                    return WebResult.getSuccessResult("payResult",3);
                }*/
            }
        } else {
            Integer sweep_pay_result_query = RedisUtil.getInt("sweep_pay_result_query_" + orderId, 0);
            if (sweep_pay_result_query < 10) {
                sweep_pay_result_query++;
                RedisUtil.set("sweep_pay_result_query_" + orderId, sweep_pay_result_query);
            } else {
                //撤销支付
                try {

                    if (StringUtil.isEmpty(organizerPayConfigByOrgId.getUnipayOpenapiMchId())) {
                        UnionPayApi.cancelPay(order.getCode(), organizerPayConfigByOrgId.getUnipayOpShopId());
                    } else {
                        UnionPayApi.openApiCancelPay(order.getCode(), organizerPayConfigByOrgId.getUnipayOpenapiMchId(), organizerPayConfigByOrgId.getUnipayOpenapiKey());
                    }

                } catch (UnknownHostException ex) {
                    ex.printStackTrace();
                }
                order.setStatus(4);
                ordersService.updateOrders(order);
                return WebResult.getSuccessResult("payResult", 3);
            }
            return WebResult.getSuccessResult("payResult", 2);
        }
    }

    @RequestMapping(value = "/offlineSweepCodePay.json", method = RequestMethod.POST)
    public Object offlineSweepCodePay(@RequestParam(value = "orderId", required = false) Integer orderId,
                                      @RequestParam(value = "payType", required = false) Integer payType,
                                      @RequestParam(value = "receiptCode", required = false, defaultValue = "") String receiptCode,
                                      @RequestParam(value = "payInCashMoney", required = false) BigDecimal payInCashMoney,
                                      @RequestParam(value = "payByCardMoney", required = false) BigDecimal payByCardMoney,
                                      @RequestParam(value = "payByWechatMoney", required = false) BigDecimal payByWechatMoney,
                                      @RequestParam(value = "payByAlipayMoney", required = false) BigDecimal payByAlipayMoney,
                                      @RequestParam(value = "payByPreSaleMoney", required = false) BigDecimal payByPreSaleMoney,
                                      @RequestParam(value = "payByBullMoney", required = false) BigDecimal payByBullMoney,
                                      @RequestParam(value = "depositOfflineMoney", required = false) BigDecimal depositOfflineMoney,
                                      @RequestParam(value = "depositOfflineType", required = false) Integer depositOfflineType,
                                      @RequestParam(value = "companyId", required = false) Integer companyId,
                                      @RequestParam(value = "trustees", required = false) String trustees,
                                      @RequestParam(value = "trusteesPhone", required = false) String trusteesPhone,
                                      @RequestParam(value = "entertainPersonName", required = false) String entertainPersonName,
                                      @RequestParam(value = "entertainPersonPhone", required = false) String entertainPersonPhone,
                                      @RequestParam(value = "companyName", required = false) String companyName,
                                      @RequestParam(value = "authorcode", required = false) String authorcode,
                                      HttpSession session, HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        CommonAccount commonAccount = WebUpmsContext.getAccount(request);
        if (commonAccount == null) {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
        if (organizer == null) {
            return WebResult.getErrorResult(WebResult.Code.COACH_GET_ORGANIZER_INFO_ERROR);
        }
        if ((payType == 13 || payType == 4 || payType == 2) && StringUtils.isBlank(authorcode)) {
            //需要重新扫码
            return WebResult.getErrorResult(WebResult.Code.CASHIER_NO_AUTHOR_CODE);
        }
        /*String unipayOpShopId = organizerPayConfigService.getUnipayOpShopId(organizer.getOrganizerId());
        if(StringUtil.isEmpty(unipayOpShopId)){
            return WebResult.getErrorMsgResult(WebResult.Code.ERROR,"没有支付配置");
        }*/
//        SweepPayUtils.clearResult(organizer.getOrganizerId(), orderId, commonAccount.getCustomerId());
        new Thread() {
            @Override
            public void run() {
                offlinePay(orderId,
                        payType,
                        receiptCode,
                        payInCashMoney,
                        payByCardMoney,
                        payByWechatMoney,
                        payByAlipayMoney,
                        payByPreSaleMoney,
                        payByBullMoney,
                        depositOfflineMoney,
                        depositOfflineType,
                        companyId,
                        trustees,
                        trusteesPhone,
                        entertainPersonName,
                        entertainPersonPhone,
                        companyName,
                        authorcode,
                        commonAccount,
                        organizer);
            }
        }.start();
        return WebResult.getSuccessResult();
    }

    @ApiOperation(value = "教练系统线下支付按钮接口", notes = "点击教练系统线下支付按钮调用的接口", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "orderId", value = "订单ID", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "payType", value = "支付方式(0：线下现金，1：线上， 2：线下支付宝， 3：线下刷卡， 4：线下微信，5：线下预售，6：线下挂账，7：线上储值支付，8：线下储值支付，9：线， 10， 线下招待，999：混合支付)", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "receiptCode", value = "收据号", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "payInCashMoney", value = "线下 现金支付的 金额", required = false, dataType = "BigDecimal", paramType = "query"),
            @ApiImplicitParam(name = "payByCardMoney", value = "线下 刷卡支付的 金额", required = false, dataType = "BigDecimal", paramType = "query"),
            @ApiImplicitParam(name = "payByWechatMoney", value = "线下 微信支付的 金额", required = false, dataType = "BigDecimal", paramType = "query"),
            @ApiImplicitParam(name = "payByAlipayMoney", value = "线下 支付宝支付的 金额", required = false, dataType = "BigDecimal", paramType = "query"),
            @ApiImplicitParam(name = "payByPreSaleMoney", value = "线下 预售支付的 金额", required = false, dataType = "BigDecimal", paramType = "query"),
            @ApiImplicitParam(name = "payByBullMoney", value = "线下 挂账支付的 金额", required = false, dataType = "BigDecimal", paramType = "query"),
            @ApiImplicitParam(name = "depositOfflineMoney", value = "线下 押金 金额", required = false, dataType = "BigDecimal", paramType = "query"),
            @ApiImplicitParam(name = "depositOfflineType", value = "线下 押金 类型", required = false, dataType = "BigDecimal", paramType = "query"),

            @ApiImplicitParam(name = "companyId", value = "企业ID-(挂账业务)", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "companyName", value = "招待单位-(招待业务)", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "trustees", value = "经办人-(挂账和招待业务)", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "trusteesPhone", value = "经办人电话-(挂账和招待业务)", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "entertainPersonName", value = "招待人-(挂账和招待业务)", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "entertainPersonPhone", value = "招到人电话-(挂账和招待业务)", required = false, dataType = "string", paramType = "query"),
    })
    @RequestMapping(value = "/offlinePay.json", method = RequestMethod.POST)
    public Object offlinePay(@RequestParam(value = "orderId", required = false) Integer orderId,
                             @RequestParam(value = "payType", required = false) Integer payType,
                             @RequestParam(value = "receiptCode", required = false, defaultValue = "") String receiptCode,
                             @RequestParam(value = "payInCashMoney", required = false) BigDecimal payInCashMoney,
                             @RequestParam(value = "payByCardMoney", required = false) BigDecimal payByCardMoney,
                             @RequestParam(value = "payByWechatMoney", required = false) BigDecimal payByWechatMoney,
                             @RequestParam(value = "payByAlipayMoney", required = false) BigDecimal payByAlipayMoney,
                             @RequestParam(value = "payByPreSaleMoney", required = false) BigDecimal payByPreSaleMoney,
                             @RequestParam(value = "payByBullMoney", required = false) BigDecimal payByBullMoney,
                             @RequestParam(value = "depositOfflineMoney", required = false) BigDecimal depositOfflineMoney,
                             @RequestParam(value = "depositOfflineType", required = false) Integer depositOfflineType,
                             @RequestParam(value = "companyId", required = false) Integer companyId,
                             @RequestParam(value = "trustees", required = false) String trustees,
                             @RequestParam(value = "trusteesPhone", required = false) String trusteesPhone,
                             @RequestParam(value = "entertainPersonName", required = false) String entertainPersonName,
                             @RequestParam(value = "entertainPersonPhone", required = false) String entertainPersonPhone,
                             @RequestParam(value = "companyName", required = false) String companyName,
                             @RequestParam(value = "authorcode", required = false) String authorcode,
                             HttpSession session, HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        CommonAccount account = WebUpmsContext.getAccount(request);
        return offlinePay(orderId,
                payType,
                receiptCode,
                payInCashMoney,
                payByCardMoney,
                payByWechatMoney,
                payByAlipayMoney,
                payByPreSaleMoney,
                payByBullMoney,
                depositOfflineMoney,
                depositOfflineType,
                companyId,
                trustees,
                trusteesPhone,
                entertainPersonName,
                entertainPersonPhone,
                companyName,
                authorcode,
                account,
                organizer);
    }

    /**
     * 点击线下支付按钮
     *
     * @param orderId
     * @param payType
     * @param receiptCode
     * @param payInCashMoney
     * @param payByCardMoney
     * @param payByWechatMoney
     * @param payByAlipayMoney
     * @param payByPreSaleMoney
     * @param payByBullMoney
     * @param depositOfflineMoney
     * @param depositOfflineType
     * @return
     */
    public Object offlinePay(Integer orderId,
                             Integer payType,
                             String receiptCode,
                             BigDecimal payInCashMoney,
                             BigDecimal payByCardMoney,
                             BigDecimal payByWechatMoney,
                             BigDecimal payByAlipayMoney,
                             BigDecimal payByPreSaleMoney,
                             BigDecimal payByBullMoney,
                             BigDecimal depositOfflineMoney,
                             Integer depositOfflineType,
                             Integer companyId,
                             String trustees,
                             String trusteesPhone,
                             String entertainPersonName,
                             String entertainPersonPhone,
                             String companyName,
                             String authorcode,
                             CommonAccount commonAccount,
                             Organizer organizer) {
        Orders order = ordersService.getSimpleOrdersById(orderId);
        if (organizer == null) {
            return WebResult.getErrorResult(WebResult.Code.COACH_GET_ORGANIZER_INFO_ERROR);
        }
        if (!organizer.getOrganizerId().equals(order.getSellerId())) {
            // 您没有权限操作该订单
            return WebResult.getErrorResult(WebResult.Code.NO_PERMISSION);
        }
        if (commonAccount == null) {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }

        if (order.getStatus() != 0) {
            return WebResult.getErrorResult(WebResult.Code.CASHIER_CANT_PAY);
        }

        // 招待挂账
        if (payType == 9 || payType == 10) {
            Orders simpleOrdersById = ordersService.getSimpleOrdersById(orderId);

            if (simpleOrdersById.getDeposit() != null && simpleOrdersById.getDeposit().doubleValue() != 0) {
                return WebResult.getErrorResult(WebResult.Code.HUNG_UP_MONEY_DEPOSIT_MUST_ZERO);
            }
            if (payType == 9) {
                if (companyId == null) {
                    //挂账
                    return WebResult.getErrorMsgResult(WebResult.Code.ERROR, "没有选择挂账企业");
                } else {
                    ZooOnAccountCompanyExample example = new ZooOnAccountCompanyExample();
                    example.createCriteria().andIdEqualTo(new Long(companyId));
                    ZooOnAccountCompany company = zooOnAccountCompanyService.selectFirstByExample(example);
                    BigDecimal depositMoney = simpleOrdersById.getDeposit();
                    if (depositMoney == null) {
                        depositMoney = BigDecimal.ZERO;
                    }
                    if (company.getBalance().doubleValue() < simpleOrdersById.getTotalPrice().subtract(depositMoney).doubleValue()) {
                        return WebResult.getErrorResult(WebResult.Code.HUNG_UP_MONEY_NO_FULL);
                    }
                }
            } else if (payType == 10 && (org.apache.commons.lang.StringUtils.isEmpty(trustees) || org.apache.commons.lang.StringUtils.isEmpty(entertainPersonName))) {
                //招待
                return WebResult.getErrorMsgResult(WebResult.Code.ERROR, "请输入经办人与招待人信息");
            }
        }

        // 储值判断
        if (payType == 7 || payType == 8) {
            // 进行相关的确认操作（如什么样的产品不能买之类的）
            /* 获取player表的phone字段的 common_account 的用户信息 */
            List<Player> players = playerService.getPlayersByOrder(order.getOrderId());
            if (players.size() <= 0) {
                //获取报名人信息有误
                return WebResult.getErrorResult(WebResult.Code.CASHIER_INVALID_REGISTRATION_FORM);
            }
            String custoemrPhone = players.get(0).getPhone(); // 储值会员的手机号
            AccountResult accountResult = accountService.getAccount(AccountEnum.FetchAccountTypeEnum.FETCH_BY_PHONE.getValue(), custoemrPhone);
            if (accountResult != null && accountResult.getData() != null) {
                commonAccount = (CommonAccount) accountResult.getData();
            } else {
                // 填写报名人信息不是储值用户,请核实信息
                return WebResult.getErrorResult(WebResult.Code.CASHIER_ORDER_PAY_NOT_STORAGE_ACCOUNT);
            }

            /* 获取 储值余额 */
            BigDecimal balanceAmount = storageService.getStorageAmountByCustomerIdAndOrganizerId(commonAccount.getCustomerId(), organizer.getOrganizerId());
            if (balanceAmount.subtract(order.getTotalPrice()).doubleValue() <= 0) {
                // 储值余额不足，请进行充值~
                return WebResult.getErrorResult(WebResult.Code.CASHIER_STORAGE_NOT_ENOUGH);
            }
        }

        String requestId = UUID.randomUUID().toString();
        String offlinePayLockKey = "coach_order_mode_offline_pay" + "_" + order.getOrderId();

        OrderSweepCodePay sweepCodePay = null;
        Map<String, String> responseResult = null;
        OrganizerPayConfig organizerPayConfigByOrgId = null;
        try {
            if (!RedisUtil.tryGetDistributedLock(offlinePayLockKey, requestId, 70000L)) {
                return WebResult.getErrorResult("订单支付中，请稍后重试！");
            }

            Charge charge = chargeService.getChargeById(order.getChargeId());
            Episode episode = episodeService.getEpisodeDetailById(charge.getEpisodeId());
            Activity activity = activityService.getActivityById(episode.getActivityId());
            if (activity.getActivityMode() == 0) {
                OrderCoachExtend orderCoachExtend = orderCoachExtendService.selectByOrderId(orderId).get(0);
                Integer coachId = orderCoachExtend.getTransferCoach() != null ? orderCoachExtend.getTransferCoach() : orderCoachExtend.getOrderCoach();
                if (!coachService.checkCoachCanAppoint(coachId, orderCoachExtend.getAppointmentTime(), orderCoachExtend.getDuration(), organizer, orderId, 1)) {
                    // 时段已有订单,请重新下单
                    payError(organizer.getOrganizerId(), order.getOrderId().longValue(), commonAccount.getCustomerId(), COACH_APPOINTMENT_TIME_HAS_ORDER);

                    // 释放支付分布式锁
                    RedisUtil.releaseDistributedLock(offlinePayLockKey, requestId);
                    return WebResult.getErrorResult(WebResult.Code.COACH_APPOINTMENT_TIME_HAS_ORDER);
                }

//            List<CoachRest> coachRests = coachService.getCoachRest(coachId, orderCoachExtend.getAppointmentTime());
                if (!coachService.checkCoachRestCanAppoint(coachId, orderCoachExtend.getAppointmentTime(), orderCoachExtend.getDuration(), organizer, orderId, 1)) {
                    // 教练有请假
                    payError(organizer.getOrganizerId(), order.getOrderId().longValue(), commonAccount.getCustomerId(), COACH_APPOINTMENT_TIME_HAS_LEACE);

                    // 释放支付分布式锁
                    RedisUtil.releaseDistributedLock(offlinePayLockKey, requestId);
                    return WebResult.getErrorResult(WebResult.Code.COACH_APPOINTMENT_TIME_HAS_LEACE);
                }
            }

            //扫码支付组装支付业务参数
            String transactionId = null;
            String unipayOpShopId = null;
            String extSweepCodeWay = null;
            Map<String, String> paramMap = new HashMap<>();
            if (payType == 2 || payType == 4 || payType == 13) {
                /*if (StringUtils.isEmpty(authorcode)) {
                    //需要重新扫码
                    paySweepCodeError(sweepCodePay,responseResult,order,organizerPayConfigByOrgId,commonAccount,organizer,"扫码失败");
                    return WebResult.getErrorResult(WebResult.Code.CASHIER_NO_AUTHOR_CODE);
                }*/

                // 发生扫码支付时，清除订单支付结果
                SweepPayUtils.clearResult(organizer.getOrganizerId(), orderId, commonAccount.getCustomerId());

                organizerPayConfigByOrgId = organizerPayConfigService.getOrganizerPayConfigByOrgId(organizer.getOrganizerId());
                sweepCodePay = orderSweepCodePayService.findOrderSweepCodePaySuccess(order.getCode());

                if (organizerPayConfigByOrgId == null) {
                    // 释放支付分布式锁
                    RedisUtil.releaseDistributedLock(offlinePayLockKey, requestId);
                    return WebResult.getErrorResult(WebResult.Code.CASHIER_ORGANIZER_SCAN_PAY_NO_CONFIG);
                }

                if (payType == 2) {
                    //支付状态
                    Boolean aliPayStatus = false;
                    //支付失败备注
                    String remarks = "";
                    AlipayClient alipayClient = AliPayApi.alipayClient(organizerPayConfigByOrgId.getAlipayAppId()
                            , organizerPayConfigByOrgId.getPrivateKey(), organizerPayConfigByOrgId.getAlipayPublicKey()
                            , organizerPayConfigByOrgId.getAlipayEncryptKey(), organizerPayConfigByOrgId.getAlipayEncryptType());


                    if (sweepCodePay != null) {
                        //查询支付宝订单
                        responseResult = BeanUtils.describe(AliPayApi.query(order.getParentCode(), organizerPayConfigByOrgId.getAppAuthToken(), alipayClient));
                    } else {
                        long d = System.currentTimeMillis();
                        order.setPayTypeId(payType);
                        //创建支付记录
                        sweepCodePay = orderSweepCodePayService.createPay(order);
                        // 组装支付业务数据
                        AlipayTradePayRequest request = new AlipayTradePayRequest();
                        AlipayTradePayModel alipayTradePayModel = new AlipayTradePayModel();
                        // 商品标题
                        alipayTradePayModel.setSubject(organizer.getOrganizerId() + "-" + order.getChargeName() + "x" + order.getChargeNum());
                        // 商家订单编号
                        alipayTradePayModel.setOutTradeNo(order.getParentCode());
                        // 订单总金额
                        alipayTradePayModel
                                .setTotalAmount(String.valueOf(
                                        order.getTotalPrice() != null ? order.getTotalPrice().setScale(2) : 0));
                        // 付款码
                        alipayTradePayModel.setAuthCode(authorcode);

                        request.setBizModel(alipayTradePayModel);
                        request.setNeedEncrypt(true);
                        request.putOtherTextParam("app_auth_token", organizerPayConfigByOrgId.getAppAuthToken());
                        System.out.println("----------------------教练系统支付宝支付金额：" + order.getTotalPrice().setScale(2, BigDecimal.ROUND_HALF_UP));
                        AlipayTradePayResponse response = alipayClient.execute(request);

                        System.out.println(response.getBody());
                        sweepCodePay.setPayWay("alipay");//支付方式需要确认
                        sweepCodePay.setTransactionId(response.getTradeNo());
                        sweepCodePay.setTime(new Date());
                        if (response.isSuccess()) {
                            String code = response.getCode();
                            System.out.println("----------------------教练系统支付宝支付接口返回信息：" + response.getCode());
                            if (AliPayConstants.ALI_PAY_SUCCESS.equals(code)) {
                                System.out.println("----------------------教练系统支付宝支付成功!  订单号：" + order.getParentCode());
//                                aliPayStatus = true;
                                sweepCodePay.setTradeStat(ActivityBeanUtils.SweepPayTradeStat.SUCCESS.toString());
                            } else if (AliPayConstants.ALI_PAY_USERPAYING.equals(code)) {//用户正在支付
                                AlipayTradeQueryResponse alipayTradeQueryResponse = AliPayApi.query(order.getParentCode(), organizerPayConfigByOrgId.getAppAuthToken(), alipayClient);
                                for (int i = 1; i < 30; i++) {
                                    Thread.sleep(2000);
                                    alipayTradeQueryResponse = AliPayApi.query(order.getParentCode(), organizerPayConfigByOrgId.getAppAuthToken(), alipayClient);
                                    String tradeStatus = alipayTradeQueryResponse.getTradeStatus();
                                    //查询订单状态
                                    if (AliPayConstants.ALI_PAY_SUCCESS.equals(alipayTradeQueryResponse.getCode()) && "TRADE_SUCCESS".equals(tradeStatus)) {
                                        System.out.println("----------------------教练系统支付宝支付成功!  订单号：" + order.getParentCode());
                                        aliPayStatus = true;
                                        sweepCodePay.setTradeStat(ActivityBeanUtils.SweepPayTradeStat.SUCCESS.toString());
                                        break;
                                    } else {
                                        System.out.println("----------------------教练系统支付宝支付宝查询订单接口返回订单还未支付!  订单号：" + order.getParentCode());
                                        remarks = alipayTradeQueryResponse.getSubMsg();
                                    }
                                }
                                //轮询结束，未支付成功，撤销订单
                                if (!aliPayStatus) {
                                    System.out.println("----------------------教练系统支付宝支付1分钟之内用户未支付，撤销订单！ 订单号：" + order.getParentCode());
                                    remarks = "1分钟之内用户未支付,撤销订单!";
                                    // 撤销订单
                                    // 撤销支付
                                    paySweepCodeError(sweepCodePay, JSONObject.parseObject(JSONObject.toJSONString(response), Map.class)
                                            , order, organizerPayConfigByOrgId, commonAccount, organizer, remarks);

                                    // 释放支付分布式锁
                                    RedisUtil.releaseDistributedLock(offlinePayLockKey, requestId);
                                    return WebResult.getErrorMsgResult(WebResult.Code.CASHIER_NO_PAY_ERROR, alipayTradeQueryResponse.getSubMsg());
                                } else {
                                    //支付成功
                                    sweepCodePay.setTradeStat(ActivityBeanUtils.SweepPayTradeStat.SUCCESS.toString());
                                }
                            }
                        } else {
                            System.out.println("----------------------教练系统支付宝支付接口通讯失败，返回信息：" + JSON.parseObject(response.getBody(), Map.class));
                            //撤销订单
                            paySweepCodeError(sweepCodePay, JSONObject.parseObject(JSONObject.toJSONString(response), Map.class)
                                    , order, organizerPayConfigByOrgId, commonAccount, organizer, response.getSubMsg());

                            // 释放支付分布式锁
                            RedisUtil.releaseDistributedLock(offlinePayLockKey, requestId);
                            return WebResult.getErrorMsgResult(WebResult.Code.CASHIER_NO_PAY_ERROR, response.getSubMsg());
                        }

                        extSweepCodeWay = "alipay";
                        transactionId = response.getTradeNo();

                        System.out.println(DateUtil.getDateTimeStr(new Date()) + "----------------------教练系统支付宝, 订单号：" + order.getCode() + "----------------- 线下支付宝扫码支付花费时间：" + (System.currentTimeMillis() - d));
                    }
                } else if (payType == 4) {
                    // 组装订单号&商户号
                    if (organizerPayConfigByOrgId != null && org.apache.commons.lang.StringUtils.isNotBlank(organizerPayConfigByOrgId.getSubmchid())) {
                        paramMap.put("sub_mch_id", organizerPayConfigByOrgId.getSubmchid());
                    }
                    paramMap.put("out_trade_no", order.getParentCode());

                    if (sweepCodePay != null) {
                        responseResult = WXPaymentCodeApi.orderQuery(paramMap);
                    } else {
                        long d = System.currentTimeMillis();
                        order.setPayTypeId(payType);
                        //创建支付记录
                        sweepCodePay = orderSweepCodePayService.createPay(order);

                        // 组装支付业务数据
                        paramMap.put("device_info", String.valueOf(organizer.getOrganizerId()));
                        paramMap.put("body", organizer.getOrganizerId() + "-" + order.getChargeName() + "x" + order.getChargeNum());
                        paramMap.put("total_fee", String.valueOf(order.getTotalPrice() != null ? order.getTotalPrice().multiply(new BigDecimal(100)).intValue() : 0));
                        paramMap.put("fee_type", "CNY");
                        paramMap.put("auth_code", authorcode);
                        responseResult = WXPaymentCodeApi.microPay(paramMap);

                        sweepCodePay.setPayWay(WXPaymentCodeApi.getPayWay(responseResult));
                        sweepCodePay.setTransactionId(WXPaymentCodeApi.getTransactionId(responseResult));
                        sweepCodePay.setTime(new Date());
                        if (!WXPaymentCodeApi.isMicroPaySuccess(responseResult)) {
                            if (!WXPaymentCodeApi.isSuccess(responseResult)) {
                                paySweepCodeError(sweepCodePay, responseResult, order, organizerPayConfigByOrgId, commonAccount, organizer, WXPaymentCodeApi.getErrMsg(responseResult));

                                // 释放支付分布式锁
                                RedisUtil.releaseDistributedLock(offlinePayLockKey, requestId);
                                return WebResult.getErrorMsgResult(WebResult.Code.CASHIER_NO_PAY_ERROR, WXPaymentCodeApi.getErrMsg(responseResult));
                            }

                            int count = 0;
                            Map<String, String> wxPost = null;
                            // 组装线下微信支付查询订单api业务数据
                            paramMap = new HashMap<>();
                            if (organizerPayConfigByOrgId != null && org.apache.commons.lang.StringUtils.isNotBlank(organizerPayConfigByOrgId.getSubmchid())) {
                                paramMap.put("sub_mch_id", organizerPayConfigByOrgId.getSubmchid());
                            }
                            paramMap.put("out_trade_no", order.getParentCode());
                            while (wxPost == null) {
                                try {
                                    Thread.sleep(1000);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }

                                wxPost = WXPaymentCodeApi.orderQuery(paramMap);

                                if (!WXPaymentCodeApi.isSuccess(wxPost)) {
                                    break;
                                }
                                /*if (!WXPaymentCodeApi.resultCode(wxPost)) {
                                    break;
                                }*/

                                if (WXPaymentCodeApi.isSuccessTradeStateQueryBySuccess(wxPost)) {
                                    break;
                                }

                                if (!WXPaymentCodeApi.isContinueTradeStateQueryByUserpaying(wxPost)) {
                                    break;
                                }
                                count++;
                                if (count > 60) {
                                    break;
                                }
                                wxPost = null;
                            }
                            if (!WXPaymentCodeApi.isQueryPayResultSuccess(wxPost)) {
                                // 撤销订单 (微信线下扫码支付失败)
                                // 撤销支付
                                paySweepCodeError(sweepCodePay, responseResult, order, organizerPayConfigByOrgId, commonAccount, organizer, WXPaymentCodeApi.getErrMsg(wxPost));

                                // 释放支付分布式锁
                                RedisUtil.releaseDistributedLock(offlinePayLockKey, requestId);
                                return WebResult.getErrorMsgResult(WebResult.Code.CASHIER_NO_PAY_ERROR, WXPaymentCodeApi.getErrMsg(responseResult));
                            } else {
                                responseResult = wxPost;
                                sweepCodePay.setTradeStat(ActivityBeanUtils.SweepPayTradeStat.SUCCESS.toString());
                            }
                        } else {
                            sweepCodePay.setTradeStat(ActivityBeanUtils.SweepPayTradeStat.SUCCESS.toString());
                        }

                        extSweepCodeWay = WXPaymentCodeApi.getPayWay(responseResult);
                        transactionId = WXPaymentCodeApi.getTransactionId(responseResult);

                        System.out.println(DateUtil.getDateTimeStr(new Date()) + "订单号：" + order.getCode() + "----------------- 线下微信扫码支付花费时间：" + (System.currentTimeMillis() - d));
                    }
                } else if (payType == 13) {

                    long d = System.currentTimeMillis();

                    if (organizerPayConfigByOrgId == null) {

                        // 释放支付分布式锁
                        RedisUtil.releaseDistributedLock(offlinePayLockKey, requestId);
                        return WebResult.getErrorMsgResult(WebResult.Code.ERROR, "没有配置支付参数");
                    }
                    unipayOpShopId = organizerPayConfigByOrgId.getUnipayOpShopId();
                    if (unipayOpShopId == null &&
                            (org.apache.commons.lang.StringUtils.isEmpty(organizerPayConfigByOrgId.getUnipayOpenapiMchId()) ||
                                    org.apache.commons.lang.StringUtils.isEmpty(organizerPayConfigByOrgId.getUnipayOpenapiKey()))) {

                        // 释放支付分布式锁
                        RedisUtil.releaseDistributedLock(offlinePayLockKey, requestId);
                        return WebResult.getErrorMsgResult(WebResult.Code.ERROR, "支付配置不正确");
                    }

                    if (sweepCodePay != null) {
                        responseResult = UnionPayApi.tradeQuery(sweepCodePay.getPayCode(), unipayOpShopId, organizerPayConfigByOrgId.getUnipayOpenapiMchId(),
                                organizerPayConfigByOrgId.getUnipayOpenapiKey());
                    } else {
                        order.setPayTypeId(payType);
                        //创建支付记录
                        sweepCodePay = orderSweepCodePayService.createPay(order);

                        int totalPrice = order.getTotalPrice().multiply(BigDecimal.valueOf(100)).intValue();

                        if (org.apache.commons.lang.StringUtils.isEmpty(organizerPayConfigByOrgId.getUnipayOpenapiMchId())) {
                            responseResult = UnionPayApi.pay(sweepCodePay.getPayCode(), totalPrice,
                                    order.getActivityName(), unipayOpShopId, authorcode);
                        } else {
                            responseResult = UnionPayApi.openApiPay(sweepCodePay.getPayCode(), totalPrice,
                                    order.getActivityName(), organizerPayConfigByOrgId.getUnipayOpenapiMchId(),
                                    organizerPayConfigByOrgId.getUnipayOpenapiKey(), authorcode);
                        }

                        sweepCodePay.setPayWay(UnionPayApi.getPayWay(responseResult));
                        sweepCodePay.setTransactionId(UnionPayApi.getTransactionId(responseResult));
                        sweepCodePay.setTime(new Date());
                        if (!UnionPayApi.isSuccess(responseResult) && !UnionPayApi.isNeedQuery(responseResult)) {
                            paySweepCodeError(sweepCodePay, responseResult, order, organizerPayConfigByOrgId, commonAccount, organizer, UnionPayApi.getErrMsg(responseResult));

                            // 释放支付分布式锁
                            RedisUtil.releaseDistributedLock(offlinePayLockKey, requestId);
                            return WebResult.getErrorMsgResult(WebResult.Code.CASHIER_NO_PAY_ERROR, UnionPayApi.getErrMsg(responseResult));
                        } else if (UnionPayApi.isResultSuccess(responseResult)) {
                            sweepCodePay.setTradeStat(ActivityBeanUtils.SweepPayTradeStat.SUCCESS.toString());
                        } else if (UnionPayApi.isNeedQuery(responseResult)) {
                            int count = 0;
                            Map<String, String> post = null;
                            while (post == null) {

                                try {
                                    Thread.sleep(1000);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }

                                post = UnionPayApi.tradeQuery(sweepCodePay.getPayCode(), unipayOpShopId, organizerPayConfigByOrgId.getUnipayOpenapiMchId(),
                                        organizerPayConfigByOrgId.getUnipayOpenapiKey());
                                if (!UnionPayApi.isContinueTradeQuery(post)) {
                                    break;
                                }
                                if (UnionPayApi.isSuccessTradeQuery(post)) {
                                    break;
                                }
                                count++;
                                if (count > 60) {
                                    break;
                                }

                                post = null;
                            }
                            if (!UnionPayApi.isSuccessTradeQuery(post)) {
                                // 撤销订单 (银联支付失败)
                                // 撤销支付
                                paySweepCodeError(sweepCodePay, responseResult, order, organizerPayConfigByOrgId, commonAccount, organizer, UnionPayApi.getErrMsg(post));

                                // 释放支付分布式锁
                                RedisUtil.releaseDistributedLock(offlinePayLockKey, requestId);
                                return WebResult.getErrorResult(WebResult.Code.CASHIER_NO_PAY_ERROR);
                            } else {
                                responseResult = post;
                                sweepCodePay.setTradeStat(ActivityBeanUtils.SweepPayTradeStat.SUCCESS.toString());
                            }
                        }
                        extSweepCodeWay = UnionPayApi.getPayWay(responseResult);
                        transactionId = UnionPayApi.getTransactionId(responseResult);
                    }

                    System.out.println(DateUtil.getDateTimeStr(new Date()) + " 教练订单号：" + order.getCode() + "----------------- 支付花费时间：" + (System.currentTimeMillis() - d));
                }
            }
//        } catch (Exception e) {
//            //扫码支付
//            if (payType == 2 || payType == 4 || payType == 13) {
//                //撤销支付
//                paySweepCodeError(sweepCodePay, responseResult, order, organizerPayConfigByOrgId, commonAccount, organizer, "支付失败");
//            }
//            e.printStackTrace();
//            return WebResult.getErrorResult(WebResult.Code.CASHIER_NO_PAY_ERROR);
//        }
//
//
//        try {


            OrderResultBean orderResult = ordersService.payOrder(receiptCode, trustees, trusteesPhone, entertainPersonName, entertainPersonPhone, commonAccount, companyId, null, order.getTotalPrice(), transactionId, order, activity, episode, charge, commonAccount, receiptCode, payType,
                    payInCashMoney, payByCardMoney, payByWechatMoney, payByAlipayMoney, payByPreSaleMoney, payByBullMoney, depositOfflineMoney, depositOfflineType, companyName, extSweepCodeWay);


//                WebResult webResult = ordersService.offlinePay(receiptCode, trustees, trusteesPhone, entertainPersonName, entertainPersonPhone, commonAccount, companyId, orderId, payType, receiptCode, payInCashMoney, payByCardMoney, payByWechatMoney, payByAlipayMoney, payByPreSaleMoney, payByBullMoney, depositOfflineMoney, depositOfflineType, organizer, companyName, transactionId, extSweepCodeWay);
            // logger.info(orderResult.getMsg());
            ;
            if (1 == orderResult.getResultCount()) {
//                if (Integer.valueOf(webResult.get("resultCount").toString()).equals(1)) {
                // 下单即核销，改为同步
//                    verifyService.verify(order.getOrderId(), 1, activity.getTypeId(), order.getSellerId(), String.valueOf(commonAccount.getCustomerId()), 1, null);
                // 线下取消占位
//                    episodeService.confirmPay(episode, charge, order, null);

                //删除 Quartz Task
//                    List<Quartz> quartzList = quartzService.getQuartzByTypeAndTargetId(5, order.getOrderId());
                // logger.info("WechatPay callback: Delete quartz task. QuartzList: " + JSON.toJSONString(quartzList));
//                    for (Quartz quartz : quartzList) {
//                        QuartzManager.removeJob(quartz.getQuartzName());
//                        quartzService.deleteQuartzTask(quartz.getQuartzId());
//                    }

                /*String noticeEmail = organizer.getEmail();
                if (StringUtils.isNotBlank(episode.getNoticeEmail())) {
                    noticeEmail = episode.getNoticeEmail();
                }*/
                // List<String> notifyTargets = new ArrayList<>();
                // notifyTargets.add(noticeEmail);
                // zooNotify.notifyByEmail(notifyTargets, "滑雪族新订单", ("您的" + activity.getTitle() + "产品有新的支付订单，请前往 http://" + Config.instance().getDomain() + "/server/order-detail?orderId=" + order.getOrderId() + "  进行查看。"));
//                mailService.sendMail(noticeEmail, "滑雪族新订单", ("您的" + activity.getTitle() + "产品有新的支付订单，请前往 http://" + Config.instance().getDomain() + "/server/order-detail?orderId=" + order.getOrderId() + "  进行查看。"));

                // 只有支付成功了，才会插入储值的消费记录
                if (activity.getTypeId() != 9 && (payType == 7 || payType == 8)) {
                    storageService.storageBuyDeal(payType, order.getBuyerId(), commonAccount.getCustomerId(), orderId, organizer.getOrganizerId(), order.getTotalPrice());
                }

//                    if (payType == 7 || payType == 8) {
//                        // 进行 储值消费 记录 (storage_operation_record)
//                        StorageOperationRecord storageOperationRecord = new StorageOperationRecord();
//                        storageOperationRecord.setOperation(payType == 7 ? 1 : 4); // 储值线下消费
//                        storageOperationRecord.setOperationCustomerId(order.getBuyerId());
//                        storageOperationRecord.setCustomerId(commonAccount.getCustomerId());
//                        storageOperationRecord.setChangeAmount(new BigDecimal(Common.sub(0, order.getTotalPrice().doubleValue())));
//                        storageOperationRecord.setOrderId(orderId);
//                        storageOperationRecord.setOrganizerId(organizer.getOrganizerId());
//                        storageOperationRecord.setCreateTime(new Date());
//                        storageService.insertSelective(storageOperationRecord);
//                    }
                if (payType == 2 || payType == 4 || payType == 13) {
                    //订单支付完成
                    pushPayMessage(sweepCodePay, organizer.getOrganizerId(), commonAccount.getCustomerId(), "支付成功", PAY_SUCCESS);
                }
                // 释放支付分布式锁
                RedisUtil.releaseDistributedLock(offlinePayLockKey, requestId);
                return WebResult.getSuccessResult();
            } else {
                if (payType == 2 || payType == 4 || payType == 13) {
                    //撤销支付
                    paySweepCodeError(sweepCodePay, responseResult, order, organizerPayConfigByOrgId, commonAccount, organizer, "payOrder方法执行失败,支付失败");
                }
                // 释放支付分布式锁
                RedisUtil.releaseDistributedLock(offlinePayLockKey, requestId);
                return WebResult.getErrorResult(WebResult.Code.ERROR);
            }
        } catch (Exception e) {
            e.printStackTrace();
            if (payType == 2 || payType == 4 || payType == 13) {
                //撤销支付
                paySweepCodeError(sweepCodePay, responseResult, order, organizerPayConfigByOrgId, commonAccount, organizer, "代码异常报错,支付失败");
            }
            // 释放支付分布式锁
            RedisUtil.releaseDistributedLock(offlinePayLockKey, requestId);
            return WebResult.getErrorResult(WebResult.Code.CASHIER_NO_PAY_ERROR);
        }
    }

    /**
     * 支付失败
     *
     * @param orgId
     * @param userId
     * @param message
     */
    private void payError(long orgId, long orderId, long userId, String message) {
        /*Map<String,String> parasMap = new HashMap<String,String>();
        parasMap.put("orgId","" + orgId);
        parasMap.put("userId","" + userId);
        parasMap.put("orderId","" + orderId);
        WebsocketSender.send(message,parasMap);*/

        SweepPayUtils.putResult(orgId, orderId, userId, "支付失败", message);
    }

    /**
     * 支付失败，撤销订单
     *
     * @param order
     * @param organizerPayConfigByOrgId
     * @param commonAccount
     * @param organizer
     */
    private void paySweepCodeError(OrderSweepCodePay sweepCodePay, Map<String, String> responseMaps, Orders order, OrganizerPayConfig organizerPayConfigByOrgId, CommonAccount commonAccount, Organizer organizer, String payMessage) {
        //撤销支付
        try {
            if (order.getPayTypeId() == 2) {
                String errorMsg = "";
                if (order.getTotalPrice().doubleValue() != 0) {
                    AlipayClient alipayClient = AliPayApi.alipayClient(organizerPayConfigByOrgId.getAlipayAppId()
                            , organizerPayConfigByOrgId.getPrivateKey(), organizerPayConfigByOrgId.getAlipayPublicKey()
                            , organizerPayConfigByOrgId.getAlipayEncryptKey(), organizerPayConfigByOrgId.getAlipayEncryptType());


                    //创建API对应的request类
                    AlipayTradeCancelRequest alipayTradeCancelRequest = new AlipayTradeCancelRequest();
                    AlipayTradeCancelModel alipayTradeCancelModel = new AlipayTradeCancelModel();
                    alipayTradeCancelModel.setOutTradeNo(order.getParentCode());
                    alipayTradeCancelRequest.setBizModel(alipayTradeCancelModel);
                    //request.putOtherTextParam("app_auth_token",organizerPayConfigByOrgId.getAppAuthToken); //设置商户的应用授权token
                    //通过alipayClient调用API，获得对应的response类
                    alipayTradeCancelRequest.setNeedEncrypt(true);
                    alipayTradeCancelRequest.putOtherTextParam("app_auth_token", organizerPayConfigByOrgId.getAppAuthToken());

                    AlipayTradeCancelResponse alipayTradeCancelResponse = alipayClient.execute(alipayTradeCancelRequest);
                    System.out.println("-----------------------教练系统支付宝执行撤销接口返回信息：" + JSONObject.toJSONString(alipayTradeCancelResponse));
                    if (alipayTradeCancelResponse.isSuccess()) {
                        System.out.println("-----------------------教练系统支付宝撤销支付成功，撤销订单号：" + sweepCodePay.getPayCode());
                    } else {
                        System.out.println("-----------------------教练系统支付宝支付宝撤销支付失败, msg失败原因：" + alipayTradeCancelResponse.getMsg() + ", subMsg原因业务参数返回信息：" + alipayTradeCancelResponse.getSubMsg());
//                        errorMsg = errorMsg + alipayTradeCancelResponse.getSubMsg() + order.getParentCode();
                    }
                }
                /*if (org.apache.commons.lang.StringUtils.isNotEmpty(responseMaps.get("out_trade_no"))) {
                    sweepCodePay.setTransactionId(responseMaps.get("out_trade_no"));
                }*/
                //设置PayWay
                System.out.println("-----------------------教练系统支付宝支付，撤销操作，打印支付宝支付接口返回对象转map打印信息：" + JSONObject.toJSONString(responseMaps));
                Set keyset = responseMaps.keySet();
                Iterator iterator = keyset.iterator();
                while (iterator.hasNext()) {
                    Object key = iterator.next();
                    if (!key.equals("sign")) {
                        JSONObject jsonObject = JSON.parseObject(JSONObject.toJSONString(responseMaps.get(key)));
                        System.out.println("-----------------------教练系统线下支付宝撤销接口返回信息，打印key值：" + key + " ,value值:" + JSONObject.toJSONString(jsonObject));
                        /*if (jsonObject.containsKey("code")) {
                            errorMsg = errorMsg + jsonObject.getString("code");
                        }
                        if (jsonObject.containsKey("msg")) {
                            errorMsg = errorMsg + jsonObject.getString("msg");
                        }*/
                        if (jsonObject.containsKey("sub_code")) {
                            errorMsg = errorMsg + "业务错误码："  + jsonObject.getString("sub_code");
                        }
                        if (jsonObject.containsKey("sub_msg")) {
                            errorMsg = errorMsg + "业务错Msg："  + jsonObject.getString("sub_msg");
                        }
                    }
                }
                System.out.println("-----------------------教练系统支付宝撤销接口错误信息拼接字符串：：" + errorMsg);
                if (StringUtils.isNotBlank(AliPayApi.getSubCode(responseMaps)) || StringUtils.isNotBlank(AliPayApi.getSubSMsg(responseMaps))) {
                    sweepCodePay.setErrorMsg(AliPayApi.getSubCode(responseMaps) + ":" + AliPayApi.getSubSMsg(responseMaps));
                }
                if (StringUtils.isBlank(sweepCodePay.getErrorMsg())) {
                    sweepCodePay.setErrorMsg(errorMsg);
                }
            } else if (order.getPayTypeId() == 4) {
                if (order.getTotalPrice().doubleValue() != 0) {
                    Map<String, String> map = new HashMap<>();
                    if (organizerPayConfigByOrgId != null && org.apache.commons.lang.StringUtils.isNotBlank(organizerPayConfigByOrgId.getSubmchid())) {
                        map.put("sub_mch_id", organizerPayConfigByOrgId.getSubmchid());
                    }
                    map.put("out_trade_no", order.getParentCode());
                    map = WXPaymentCodeApi.reverse(map);
                }
                if (responseMaps != null) {
                    String transactionId = WXPaymentCodeApi.getTransactionId(responseMaps);
                    if (org.apache.commons.lang.StringUtils.isNotEmpty(transactionId)) {
                        sweepCodePay.setTransactionId(transactionId);
                    }

                    String payWay = WXPaymentCodeApi.getPayWay(responseMaps);
                    if (org.apache.commons.lang.StringUtils.isNotEmpty(payWay)) {
                        sweepCodePay.setPayWay(payWay);
                    }

                    sweepCodePay.setErrorMsg(WXPaymentCodeApi.getErrMsg(responseMaps));
                }
            } else if (order.getPayTypeId() == 13) {
                // order.setStatus(4);
                // ordersService.updateOrders(order);
                if (order.getTotalPrice().doubleValue() != 0) {
                    //UnionPayApi.cancelPay(sweepCodePay.getPayCode(),unipayOpShopId);

                    if (StringUtil.isEmpty(organizerPayConfigByOrgId.getUnipayOpenapiMchId())) {
                        UnionPayApi.cancelPay(sweepCodePay.getPayCode(), organizerPayConfigByOrgId.getUnipayOpShopId());
                    } else {
                        UnionPayApi.openApiCancelPay(sweepCodePay.getPayCode(), organizerPayConfigByOrgId.getUnipayOpenapiMchId(), organizerPayConfigByOrgId.getUnipayOpenapiKey());
                    }

//                    sweepCodePay.setTradeStat(ActivityBeanUtils.SweepPayTradeStat.REVOKED.toString());
                }
                if (responseMaps != null) {
                    String transactionId = UnionPayApi.getTransactionId(responseMaps);
                    if (org.apache.commons.lang.StringUtils.isNotEmpty(transactionId)) {
                        sweepCodePay.setTransactionId(transactionId);
                    }

                    String payWay = UnionPayApi.getPayWay(responseMaps);
                    if (org.apache.commons.lang.StringUtils.isNotEmpty(payWay)) {
                        sweepCodePay.setPayWay(payWay);
                    }

                    sweepCodePay.setErrorMsg(UnionPayApi.getErrMsg(responseMaps));
                }
            }

            if (order.getTotalPrice().doubleValue() == 0) {
                sweepCodePay.setTradeStat(ActivityBeanUtils.SweepPayTradeStat.PAYERROR.toString());
            } else {
                sweepCodePay.setTradeStat(ActivityBeanUtils.SweepPayTradeStat.REVOKED.toString());
            }
            // 推送消息
            pushPayMessage(sweepCodePay, organizer.getOrganizerId(), commonAccount.getCustomerId(), payMessage, PAY_ERROR);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * 通知支付信息 (前端接收：PAY_ERROR、PAY_SUCCESS、PAY_RETRY)
     *
     * @param orgId
     * @param userId
     * @param payMessage
     */
    private void pushPayMessage(OrderSweepCodePay sweepCodePay, long orgId, long userId, String payMessage, String status) {

        orderSweepCodePayService.updateByPrimaryKey(sweepCodePay);

        if (StringUtils.isNotBlank(status) && PAY_ERROR.equalsIgnoreCase(status)) {
            ordersService.updateOrdersByParentId(Math.toIntExact(sweepCodePay.getOrderId()));
        }

        SweepPayUtils.putResult(orgId, sweepCodePay.getOrderId(), userId, payMessage, status);

        /*Map<String,String> parasMap = new HashMap<String,String>();
        parasMap.put("orgId","" + orgId);
        parasMap.put("userId","" + userId);
        parasMap.put("orderId","" + sweepCodePay.getOrderId());
        WebsocketSender.send(status,parasMap);*/

        /*Map<String, WebSocketSession> sessionMap = webSocketEndPoint.getSocketSessionMap(orgId , userId);
        if (sessionMap != null && sessionMap.size() > 0) {
            TextMessage textMessage = new TextMessage(message);

            for (Map.Entry<String, WebSocketSession> entry : sessionMap.entrySet()) {
                try {
                    entry.getValue().sendMessage(textMessage);
                } catch (Exception e) {
                    // 异常信息
                    e.printStackTrace();
                }
            }
        } else {
            System.out.println("连接丢失，支付通知失败....");
        }*/

    }

    /**
     * 扫码支付接口
     *
     * @param orderId
     * @param session
     * @param request
     * @return
     * @throws IOException
     */
    @ApiOperation(value = "线下扫码支付接口", notes = "线下扫码支付接口", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "orderId", value = "订单ID", required = false, dataType = "int", paramType = "query")
    })
    @RequestMapping(value = "/scanPay.json", method = RequestMethod.POST)
    public Object scanPay(@RequestParam(value = "orderId", required = false) Integer orderId,
                          HttpSession session, HttpServletRequest request) throws IOException {
        OrderView orderView = ordersService.getOrderDetail(orderId);
        CommonAccount commonAccount;
        AccountResult account = accountService.getAccount(AccountEnum.FetchAccountTypeEnum.FETCH_BY_CUSTOMER.getValue(), String.valueOf(orderView.getBuyerId()));
        commonAccount = (CommonAccount) account.getData();
        orderView.setBuyerName(commonAccount.getNickname());
        if (StringUtils.isNotBlank(commonAccount.getRealname())) {
            orderView.setBuyerName(commonAccount.getRealname());
        }
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null || !orderView.getSellerId().equals(organizer.getOrganizerId())) {
            // 您没有权限操作该订单"
            return WebResult.getErrorResult(WebResult.Code.NO_PERMISSION);
        } else {
            try {
                String unifiedOrder = WxPayApi.scanpayUnifiedOrder(orderView, request.getRemoteAddr(), "charge", String.valueOf(orderView.getChargeId()), new Date(), "0", null);

                // 错误处理
                if (StringUtils.isBlank(unifiedOrder) || unifiedOrder.contains("FAIL")) {
                    // 调用扫码失败
                    return WebResult.getErrorResult(WebResult.Code.SCANPAY_FAILED);
                }
                Map unifiedOrderResult = XMLUtil.xml2Map(unifiedOrder);

                Date triggerTime = new Date();
                Long longTriggerTime = triggerTime.getTime() + (Config.instance().getPayTimeout() * 60 * 1000 + 30 * 1000);
                Date expire = null;

                expire = ordersService.initOrderMarkJob(ordersService.getOrdersByCode(orderView.getCode()), triggerTime, longTriggerTime);

                // logger.info("order expire time: " + expire);

                List<OrderCoachExtendList> oceList = orderCoachExtendListViewService.selectByOrderIdAndCoachExtendId(orderView.getOrderId(), null, null);
                Map<String, Object> result = new HashMap<>();
                result.put("scanUrl", unifiedOrderResult.get("code_url"));
                result.put("expireTime", expire.getTime());
                result.put("extendOrder", oceList.size() > 0 ? oceList.get(0) : null);
                result.put("order", orderView);

                return WebResult.getSuccessResult("data", result);
            } catch (Exception e) {
                e.printStackTrace();
                return WebResult.getErrorResult(WebResult.Code.ERROR);
            }
        }
    }

    /**
     * 线下支付页面 - 获取打印小票信息
     *
     * @param orderId
     * @param request
     * @return
     * @throws IOException
     */
    @ApiOperation(value = "线下支付页面-获取打印小票信息", notes = "线下支付页面-获取打印小票信息", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "orderId", value = "订单ID", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "extendId", value = "预约ID", required = false, dataType = "int", paramType = "query")
    })
    @RequestMapping(value = "/print/PayInfo.json", method = RequestMethod.GET)
    public Object getPrintReceiptInfo(@RequestParam(value = "orderId", required = false) Integer orderId,
                                      @RequestParam(value = "extendId", required = false) Integer extendId,
                                      HttpServletRequest request) throws IOException {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null) {
            return WebResult.getErrorResult(WebResult.Code.COACH_GET_ORGANIZER_INFO_ERROR);
        }
        Map<String, Object> result = new HashMap<>();

        if (extendId != null) {
            OrderCoachExtend coachExtend = coachService.getCoachExtendByExtendId(extendId);
            if (coachExtend.getCourseType() == 2) {
                List<OrderCoachExtend> extendOrderList = new ArrayList<>();
                extendOrderList.add(coachExtend);
                result.put("extendOrder", extendOrderList);
//                result.put("charge", chargeService.getChargeById(coachExtend.getChargeId()));

                CourseLeagueCharge courseLeagueDetail = chargeService.getCourseLeagueDetailByChargeId(coachExtend.getChargeId());
                CoachGuideRecordView oce = orderCoachExtendService.getCourseLeagueAppointmentByChargeId(coachExtend.getChargeId());
                List<Map> coachList = coachGuideRelationshipsService.getOrderOrCoachInfoByOption(oce.getExtendId(), 0);
                List<Orders> ordersList = ordersService.getMainOrderListByChargeId(coachExtend.getChargeId());
                result.put("courseLeagueDetail", courseLeagueDetail);
                result.put("courseLeagueGuideDetail", oce);
                result.put("courseLeagueCoachList", coachList);
                result.put("courseLeagueOrderList", ordersList);

                BigDecimal courseLeagueTotalPrice = BigDecimal.ZERO;
                Integer playerCount = 0;
                for (Orders order : ordersList) {
                    if (order.getStatus() == 1 || order.getStatus() == 5) {
                        courseLeagueTotalPrice = courseLeagueTotalPrice.add(order.getTotalPrice());
                        playerCount = playerCount + order.getChargeNum();
                    }
                }
                result.put("courseLeagueTotalPrice", courseLeagueTotalPrice);
                result.put("courseLeaguePlayerCount", playerCount);

                return WebResult.getSuccessResult("data", result);
            }
        }

        extendId = null;
        OrderView orderView = ordersService.getOrderDetail(orderId);
        /*AccountResult account = accountService.getAccount(AccountEnum.FetchAccountTypeEnum.FETCH_BY_CUSTOMER.getValue(), String.valueOf(orderView.getBuyerId()));
        CommonAccount commonAccount = (CommonAccount) account.getData();
        orderView.setBuyerName(commonAccount.getNickname());
        if (StringUtils.isNotBlank(commonAccount.getRealname())) {
            orderView.setBuyerName(commonAccount.getRealname());
        }*/
        if (organizer == null || !orderView.getSellerId().equals(organizer.getOrganizerId())) {
            // 您没有权限操作该订单
            return WebResult.getErrorResult(WebResult.Code.NO_PERMISSION);
        }

        List<OrderCoachExtendList> oceList = orderCoachExtendListViewService.selectByOrderIdAndCoachExtendId(orderView.getOrderId(), extendId, 135);

        Integer todayAppointmentCount = 0;
        for (OrderCoachExtendList oce : oceList) {
            if (DateUtil.isToday(oce.getAppointmentTime())) {
                todayAppointmentCount++;
            }
        }
        List<Player> playerList = playerService.getPlayersByOption(orderId, extendId);
        result.put("extendOrder", oceList);
        result.put("order", orderView);
        result.put("players", playerList);
        if (orderView.getActivityMode() == 1) {
            Charge charge = chargeService.getChargeById(orderView.getChargeId());
            result.put("todayAppointmentCount", todayAppointmentCount);
            if (todayAppointmentCount > 0) {
                result.put("todayAppointmentAmount", orderView.getTotalPrice().divide(BigDecimal.valueOf(charge.getFrequency()), 2, BigDecimal.ROUND_HALF_UP).multiply(BigDecimal.valueOf(todayAppointmentCount)));
            } else {
                result.put("todayAppointmentAmount", 0.00);
            }

            result.put("charge", charge);
        }

        return WebResult.getSuccessResult("data", result);
    }

    /**
     * 出导订单页面 - 当日线下订单打印(根据具体收银员)
     *
     * @param request
     * @param session
     * @return CASHIER
     */
    @ApiOperation(value = "收银员 - 当日线下订单打印数据接口", notes = "出导订单页面 - 获取当日线下订单打印数据", httpMethod = "GET")
    @RequestMapping(value = "/getCashierTodayOrdersCount.json", method = RequestMethod.GET)
    public Object getOperaterTodayOrdersCount(HttpServletRequest request, HttpSession session) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        Integer operaterId = WebUpmsContext.getAccount(request).getCustomerId();
        if (organizer == null || operaterId == null) {
            // 获取信息失败
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }

        try {
            Map<String, Object> resultMap = ordersService.getOperaterTodayOrdersCount(organizer.getOrganizerId(), operaterId);
            return WebResult.getSuccessResult("data", resultMap);
        } catch (Exception e) {
            e.printStackTrace();
            // 获取信息失败
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }
    }
}
