package com.zoo.zoomerchantadminweb.cashier.controller;

import com.pay.unionpay.api.UnionPayApi;
import com.zoo.account.constant.AccountEnum;
import com.zoo.account.service.AccountService;
import com.zoo.account.support.AccountResult;
import com.zoo.bean.util.ActivityBeanUtils;
import com.zoo.common.page.PageList;
import com.zoo.activity.dao.model.*;
import com.zoo.activity.dao.nutz.pojo.CommonAccount;
import com.zoo.activity.service.DepositService;
import com.zoo.activity.service.OrdersService;
import com.zoo.activity.service.PlayerService;
import com.zoo.activity.service.VerifyService;
import com.zoo.activity.util.DateUtil;
import com.zoo.web.bean.WebResult;
import com.zoo.wechat.common.Config;
import com.zoo.zoomerchantadminweb.context.WebUpmsContext;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Api(value = "api/server/deposit", tags = {"主办方押金接口"}, description = "主办方押金接口描述")
@RestController
@RequestMapping("api/server/deposit")
public class DepositController {

    @Autowired
    private DepositService depositService;

    @Autowired
    private OrdersService ordersService;

    @Autowired
    private VerifyService verifyService;

    @Autowired
    private PlayerService playerService;

    @Autowired
    private AccountService accountService;

    @ApiOperation(value = "主办方押金列表", notes = "主办方押金列表", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "size", value = "条", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "status", value = "状态", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "code", value = "订单号", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "phone", value = "手机号", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "orgId", value = "主办方ID", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "payType", value = "支付方式", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "createTimeStart", value = "创建时间开始", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "createTimeEnd", value = "创建时间结束", required = false, dataType = "string", paramType = "query"),
    })
    @RequestMapping("/list.json")
    public WebResult getDepositList(@RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
                                    @RequestParam(value = "size", required = false, defaultValue = "10") Integer size,
                                    @RequestParam(value = "status", required = false) Integer status,
                                    @RequestParam(value = "payType", required = false) Integer payType,
                                    @RequestParam(value = "transactionId", required = false) String transactionId,
                                    @RequestParam(value = "code", required = false) String code,
                                    @RequestParam(value = "orgId", required = false) Integer orgId,
                                    @RequestParam(value = "randomCode", required = false) String randomCode,
                                    @RequestParam(value = "phone", required = false) String phone,
                                    @RequestParam(value = "createTimeStart", required = false) String createTimeStart,
                                    @RequestParam(value = "createTimeEnd", required = false) String createTimeEnd,
                                    @RequestParam(value = "refundTimeStart", required = false) String refundTimeStart,
                                    @RequestParam(value = "refundTimeEnd", required = false) String refundTimeEnd,
                                    HttpServletRequest request) {

        if (orgId == null) {
            Organizer organizer = WebUpmsContext.getOrgnization(request);
            orgId = organizer.getOrganizerId();
        }

        PageList pageList = depositService.getDepositListByOption(transactionId, status, code, orgId, randomCode, phone, page, size, payType, createTimeStart, createTimeEnd, refundTimeStart, refundTimeEnd);

        return WebResult.getSuccessResult("data", pageList);
    }

    @ApiOperation(value = "主办方押金详情", notes = "主办方押金详情", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "depositId", value = "押金ID", required = false, dataType = "int", paramType = "query")
    })
    @RequestMapping("/detail.json")
    public WebResult getDepositDetail(@RequestParam("depositId") Integer depositId) {
        Deposit deposit = depositService.getDepositById(depositId);
        if (null == deposit || deposit.getStatus() != 1) {
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }

        Map map = depositService.getDepositDetailAllByCode(deposit.getCode());
        Orders ordersByCode = ordersService.getOrdersByCode(deposit.getCode());
        if (ordersByCode != null) {
            map.put("payTypeId", ordersByCode.getPayTypeId());
        }

        return WebResult.getSuccessResult("data", map);
    }

    @ApiOperation(value = "主办方押金退还", notes = "主办方押金退还", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "depositId", value = "押金ID", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "code", value = "订单号", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "force", value = "是否强制退还", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "otherInfo", value = "其他信息", required = false, dataType = "string", paramType = "query")
    })
    @RequestMapping("/dealRefund.json")
    @RequiresPermissions(value = {"sys_manager:cashier:rent:read:dealRefund", "sys_manager:cashier:deposit:read:dealRefund", "sys_manager:rent:read:dealRefund", "sys_manager:deposit:read:dealRefund"}, logical = Logical.OR)
    public WebResult dealRefund(@RequestParam(value = "depositId", required = false) Integer depositId,
                                @RequestParam("amount") String refundAmount,
                                @RequestParam(value = "otherInfo", required = false) String otherInfo,
                                @RequestParam(value = "code", required = false) String code,
                                @RequestParam(value = "force", required = false) String force,
                                HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (code == null) {
            code = depositService.getDepositById(depositId).getCode();
        }
        Deposit deposit = depositService.getDepositDetailByCode(code);

        String opUserId = Config.instance().getOpUserId();
        CommonAccount account = WebUpmsContext.getAccount(request);

        int flag = 1;

        Orders parentOrder = null;
        if (deposit == null) {
            parentOrder = ordersService.getOrdersByCode(code);
            if((new Date().getTime() - parentOrder.getCreateTime().getTime()) / 1000 / 3600 / 24 >= 300 && parentOrder.getPayTypeId() == 1){
                return WebResult.getErrorResult("该笔订单已超过在线支付原路退款有效期（300天），如需退款请采用其它方式");
            }
            List<EquipmentRelationships> equipmentRelationships = depositService.getRelationshipsByParentOrderId(parentOrder.getOrderId());
            for (EquipmentRelationships e : equipmentRelationships) {
                List<RentDetail> rentDetails = depositService.getDetailsByRelationIdAndStatus(e.getRelationId(), null);
                for (RentDetail r : rentDetails) {
                    if (r.getStatus() == 4) {
                        return WebResult.getErrorResult(WebResult.Code.CASHIER_ABNORMAL_EQUIPMENT);
                    }
                }
            }

            if (null != force && force.equals("force")) {
                flag = 1;
            }

            if (null != force && flag != 0 && force.equals("force")) {
                Orders orders = ordersService.getOrdersByCode(code);
                depositService.returnAllByParentOrderId(orders.getOrderId(), account);
                List<OrderView> orderViews = ordersService.getChildOrderViews(orders.getOrderId());
                for (OrderView o : orderViews) {
                    if (o.getOrderStatus() == 1 && o.getBuyerId() > 0) {
                        try {
                            verifyService.verify(o.getOrderId(), o.getAvailableCount(), o.getTypeId(), o.getSellerId(), account.getCustomerId().toString(), 1, null);
                        } catch (Exception e) {
                            return WebResult.getErrorResult(WebResult.Code.ERROR);
                        }
                    }
                }
            }

            return WebResult.getSuccessResult();

        } else {
            //退押完成状态直接返回成功
            if (deposit.getStatus() == 3) {
                return WebResult.getSuccessResult();
            }
            if (deposit.getStatus() != 1) {
                return WebResult.getErrorResult(WebResult.Code.ERROR);
            }

            parentOrder = ordersService.getOrdersByCode(code);
            if((new Date().getTime() - parentOrder.getCreateTime().getTime()) / 1000 / 3600 / 24 >= 300 && parentOrder.getPayTypeId() == 1){
                return WebResult.getErrorResult("该笔订单已超过在线支付原路退款有效期（300天），如需退款请采用其它方式");
            }
            List<EquipmentRelationships> equipmentRelationships = depositService.getRelationshipsByParentOrderId(parentOrder.getOrderId());
            for (EquipmentRelationships e : equipmentRelationships) {
                List<RentDetail> rentDetails = depositService.getDetailsByRelationIdAndStatus(e.getRelationId(), null);
                for (RentDetail r : rentDetails) {
                    if (r.getStatus() == 4) {
                        return WebResult.getErrorResult(WebResult.Code.CASHIER_ABNORMAL_EQUIPMENT);
                    }
                }
            }

            if (!deposit.getOrganizerId().equals(organizer.getOrganizerId())) {
                return WebResult.getErrorResult(WebResult.Code.NO_PERMISSION);
            }

            Map depositMap = depositService.getDepositDetailAllByCode(deposit.getCode());


            //flag = depositService.dealRefundDeposit(deposit, account, (BigDecimal) depositMap.get("refund_amount"), otherInfo, opUserId);
            WebResult result = depositService.dealRefundDepositNew(deposit, account, (BigDecimal) depositMap.get("refund_amount"), otherInfo, opUserId);
            if(!result.isSuccess()){
                return result;
            }

            /*if (flag == 0) {
                return WebResult.getErrorMsgResult(WebResult.Code.ERROR, "退押失败");
            }*/
        }


        if (null != force && force.equals("force")) {
            flag = 1;
        }

        if (null != force && flag != 0 && force.equals("force")) {
            Orders orders = ordersService.getOrdersByCode(code);
            depositService.returnAllByParentOrderId(orders.getOrderId(), account);
            List<OrderView> orderViews = ordersService.getChildOrderViews(orders.getOrderId());
            for (OrderView o : orderViews) {
                if (o.getOrderStatus() == 1 && o.getBuyerId() > 0) {
                    try {
                        verifyService.verify(o.getOrderId(), o.getAvailableCount(), o.getTypeId(), o.getSellerId(), account.getCustomerId().toString(), 1, null);
                    } catch (Exception e) {
                        return WebResult.getErrorResult(WebResult.Code.ERROR);
                    }
                }
            }
        }

        return WebResult.getSuccessResult();
    }

    @RequestMapping("/print.json")
    public WebResult print(@RequestParam("code") String code, HttpServletRequest request) {
        WebResult webResult = WebResult.getSuccessResult();

        CommonAccount operator = WebUpmsContext.getAccount(request);

        if (null == operator) {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }

        Map deposit = depositService.getDepositDetailAllByCode(code);

        if (null == deposit || deposit.isEmpty()) {
            return WebResult.getErrorMsgResult(WebResult.Code.ERROR, "不存在押金，无法打印押金凭条");
        }

        Map<String, Object> resultMap = new HashMap<>();
        DepositRefund depositRefund = depositService.getDepositRefundByDepositId(Integer.valueOf(deposit.get("deposit_id").toString()));

        resultMap.put("refundTime", DateUtil.dateToDateString(depositRefund.getTime()));
        resultMap.put("amount", deposit.get("amount"));
        resultMap.put("indemnity", deposit.get("indemnity"));
        resultMap.put("refundAmount", depositRefund.getRefundAmount());
        resultMap.put("status", deposit.get("status"));

        OrderView parentOrder = ordersService.getOrderViewByCode(code);
        List<OrderView> incrementChildOrderView = ordersService.getIncrementChildOrderView(parentOrder.getOrderId());

        OrderView mainOrder = ordersService.getDefaultChildOrderView(parentOrder.getOrderId());
        Pay pay;

        if (parentOrder.getParentId() == null) {
            if (mainOrder == null) {
                mainOrder = incrementChildOrderView.get(0);
            }
        } else if (parentOrder.getParentId() == 0) {
            mainOrder = parentOrder;
        }
        pay = ordersService.getPayInfoByOrderId(mainOrder.getOrderId());

        resultMap.put("code", parentOrder.getCode());
        resultMap.put("payType", pay.getPayType());
        resultMap.put("createTime", DateUtil.dateToDateString(pay.getTime()));

        if (ActivityBeanUtils.OnlinePayNameEnum.containPayTypeId(pay.getPayType())) {
            AccountResult account = accountService.getAccount(AccountEnum.FetchAccountTypeEnum.FETCH_BY_CUSTOMER.getValue(), String.valueOf(mainOrder.getBuyerId()));
            CommonAccount commonAccount = (CommonAccount) account.getData();
            resultMap.put("buyerName", commonAccount.getNickname());
            resultMap.put("buyerPhone", commonAccount.getPhone());
        } else {
            AccountResult account = accountService.getAccount(AccountEnum.FetchAccountTypeEnum.FETCH_BY_CUSTOMER.getValue(), String.valueOf(mainOrder.getBuyerId()));
            CommonAccount commonAccount = (CommonAccount) account.getData();
            resultMap.put("cashier", commonAccount.getRealname());
            List players = playerService.getPlayersByOrder(parentOrder.getOrderId());
            if (null == players || players.size() == 0) {
                players = playerService.getPlayersByOrder(mainOrder.getOrderId());
                if (null != players && players.size() > 0) {
                    Player player = (Player) players.get(0);
                    resultMap.put("buyerName", player.getRealName());
                    resultMap.put("buyerPhone", player.getPhone());
                }
            } else {
                Player player = (Player) players.get(0);
                resultMap.put("buyerName", player.getRealName());
                resultMap.put("buyerPhone", player.getPhone());
            }
        }
        webResult.putResult("data", resultMap);
        return webResult;
    }

}
