package com.zoo.zoomerchantadminweb.cashier.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zoo.activity.dao.model.*;
import com.zoo.activity.dao.nutz.pojo.CommonAccount;
import com.zoo.activity.service.*;
import com.zoo.activity.util.DateUtil;
import com.zoo.activity.util.PlayerUtil;
import com.zoo.finance.dao.model.ZooOnAccountCompany;
import com.zoo.finance.dao.model.ZooOnAccountCompanyExample;
import com.zoo.finance.service.ZooOnAccountCompanyService;
import com.zoo.web.bean.WebResult;
import com.zoo.zoomerchantadminweb.context.WebUpmsContext;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.*;

@Api(value = "api/server/cashier", tags = {"主办方团队收银台接口"}, description = "主办方团队收银台接口描述")
@RestController
@RequestMapping("api/server/team/cashier")
public class TeamCashierController {

    @Autowired
    private TeamCredentialsService teamCredentialsService;

    @Autowired
    private OrdersService ordersService;

    @Autowired
    private ZooOnAccountCompanyService zooOnAccountCompanyService;

    @Autowired
    private ChargeService chargeService;


    @RequestMapping(value = "/confirmOrder.json", method = RequestMethod.POST)
    @ApiOperation(value = "线下团队下单接口", notes = "团队模块 - 线下创建订单", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "appointmentTime", value = "团队到场时间", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "rent", value = "是否是租赁物（0不是   1是）", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "episodeId", value = "场次ID", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "chargeIds", value = "票种ID（格式： [1]）", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "chargeNum", value = "下单总数量", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "playerEnrollInfos", value = "报名人信息（格式：[{\"real_name\":\"呵呵呵\",\"phone\":\"15011490302\",\"number\":\"10\"},{\"real_name\":\"哈哈哈\",\"phone\":\"15011490302\",\"number\":\"5\"}]）", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "companyName", value = "公司/旅行社名称", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "contactEmail", value = "联系邮箱", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "contactName", value = "联系人姓名", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "contactPhone", value = "联系人手机号", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "payType", value = "支付方式-(5预付，9挂账)", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "companyId", value = "企业ID-(挂账业务)", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "remark", value = "备注", required = false, dataType = "string", paramType = "query"),
    })
    public WebResult confirmOrder(@RequestParam(value = "appointmentTime", required = false) String appointmentTime,
                                  @RequestParam(value = "rent", required = false) Integer rent,
                                  @RequestParam(value = "episodeId", required = false) Integer episodeId,
                                  @RequestParam(value = "chargeIds", required = false) String chargeIds,
                                  @RequestParam(value = "chargeNum", required = false) Integer chargeNum,
                                  @RequestParam(value = "playerEnrollInfos", required = false) String playerEnrollInfos,
                                  @RequestParam(value = "companyName", required = false) String companyName,
                                  @RequestParam(value = "contactEmail", required = false) String contactEmail,
                                  @RequestParam(value = "contactName", required = false) String contactName,
                                  @RequestParam(value = "contactPhone", required = false) String contactPhone,
                                  @RequestParam(value = "payType", required = false) Integer payTypeId,
                                  @RequestParam(value = "companyId", required = false) Integer companyId,
                                  @RequestParam(value = "remark", required = false) String remark,
                                  HttpServletRequest request) throws Exception {
        CommonAccount account = WebUpmsContext.getAccount(request);
        Organizer organizer = WebUpmsContext.getOrgnization(request);

        try {
            // 统计报名数量
            Integer playerCount = 0;
            JSONArray playerArray = JSONArray.parseArray(playerEnrollInfos);
            for (Object playerObj : playerArray) {
                playerCount += JSONObject.parseObject(JSONObject.toJSONString(playerObj)).getInteger("number");
            }
            if (!playerCount.equals(chargeNum)) {
                return WebResult.getErrorResult(WebResult.Code.CASHIER_PLAYER_COUNT_ERROR);
            }

            // 转换报名人信息到实体类中
            List<Player> playerList = PlayerUtil.formatPlayerFromString(playerEnrollInfos);
            if (playerList.size() == 0) {
                return WebResult.getErrorResult(WebResult.Code.CASHIER_INVALID_REGISTRATION_FORM);
            }
            List<Integer> chargeIdList = JSONArray.parseArray(chargeIds, Integer.class);
            Charge charge = chargeService.selectByPrimaryKey(chargeIdList.get(0));
            BigDecimal totalPrice = charge.getPrice().multiply(BigDecimal.valueOf(chargeNum));
            if (BigDecimal.ZERO.compareTo(totalPrice) >= 0) {
                return WebResult.getErrorResult(WebResult.Code.ORDER_TOTAL_PRICE_LARGER_ZERO);
            }

            ZooOnAccountCompany company = new ZooOnAccountCompany();
            if(payTypeId == 9){
                ZooOnAccountCompanyExample example = new ZooOnAccountCompanyExample();
                example.createCriteria().andIdEqualTo(new Long(companyId));
                company = zooOnAccountCompanyService.selectFirstByExample(example);
                if (company.getBalance().doubleValue() < totalPrice.doubleValue()) {
                    return WebResult.getErrorResult(WebResult.Code.HUNG_UP_MONEY_NO_FULL);
                }
            }

            // 预约时间  == 到场时间
            Date appointmentDateTime = null;
            if (StringUtils.isNotBlank(appointmentTime)) {
                appointmentDateTime = DateUtil.convertSimpleStringToDate(appointmentTime);
            }

            Integer client = account.getOrganizerId();
            Map<String, Object> resultMap = ordersService.createTeamOrder(organizer.getOrganizerId(), client, account, episodeId, chargeNum, chargeIdList, playerList, appointmentDateTime, rent, companyName, contactName, contactPhone, contactEmail, remark);
            if (!(Boolean) resultMap.get("status")) {
                return WebResult.getErrorMsgResult(WebResult.Code.CASHIER_CREATE_ORDER_ERROR, String.valueOf(resultMap.get("message")));
            }

            System.out.println("=========fuck:" + JSONObject.toJSONString(resultMap));

            WebResult offlinePayResult = offlinePay((Integer) resultMap.get("teamId"), payTypeId, companyId, organizer, account, company);
//            return WebResult.getSuccessResult();
            if (offlinePayResult.getCode() != 0) {
                return offlinePayResult;
            }
            return WebResult.getSuccessResult("data", resultMap);
        } catch (Exception e) {
            e.printStackTrace();
            return WebResult.getErrorResult(WebResult.Code.CASHIER_CREATE_ORDER_ERROR);
        }
    }

    @ApiOperation(value = "团队线下支付", notes = "团队线下支付", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "teamId", value = "团队ID)", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "payTypeId", value = "支付方式-(5预付，9挂账)", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "companyId", value = "企业ID-(挂账业务)", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "companyName", value = "招待单位-(招待业务)", required = false, dataType = "string", paramType = "query"),
    })
    @RequestMapping(value = "/offlinePay.json", method = RequestMethod.POST)
    public Object offlinePay(@RequestParam(value = "teamId", required = false) Integer teamId,
                             @RequestParam(value = "payTypeId", required = false) Integer payTypeId,
                             @RequestParam(value = "companyId", required = false) Integer companyId,
                             @RequestParam(value = "companyName", required = false) String companyName,
                             HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        CommonAccount account = WebUpmsContext.getAccount(request);
        ZooOnAccountCompany company = new ZooOnAccountCompany();
        if(payTypeId == 9){
            ZooOnAccountCompanyExample example = new ZooOnAccountCompanyExample();
            example.createCriteria().andIdEqualTo(new Long(companyId));
            company = zooOnAccountCompanyService.selectFirstByExample(example);
        }

        return offlinePay(teamId,
                payTypeId,
                companyId,
                organizer,
                account,
                company);
    }

    private WebResult offlinePay(Integer teamId,
                             Integer payTypeId,
                             Integer companyId,
                             Organizer organizer,
                             CommonAccount operatorAccount,
                              ZooOnAccountCompany company
    ) {
        System.out.println("=======进入线下团队支付=====");
        TeamCredentials teamCredentials = teamCredentialsService.selectByPrimaryKey(teamId);

//        if (teamCredentials == null || teamCredentials.getPayStatus() == 1) {
//            return WebResult.getErrorResult(WebResult.Code.CASHIER_CANT_PAY);
//        }
        System.out.println("=======进入线下团队支付=====");

        Date now = new Date();
        String transactionId = null;
        String companyName = null;
        try {
            teamCredentials.setPayTypeId(payTypeId);
            teamCredentials.setUpdateTime(now);
            if (payTypeId == 9) { //挂账
                if (companyId == null) {
                    return WebResult.getErrorMsgResult(WebResult.Code.ERROR, "没有选择挂账企业");
                }

//                ZooOnAccountCompanyExample example = new ZooOnAccountCompanyExample();
//                example.createCriteria().andIdEqualTo(new Long(companyId));
//                ZooOnAccountCompany company = zooOnAccountCompanyService.selectFirstByExample(example);
                if (company.getBalance().doubleValue() < teamCredentials.getTicketAmount().doubleValue()) {
                    return WebResult.getErrorResult(WebResult.Code.HUNG_UP_MONEY_NO_FULL);
                }
                companyName = company.getCompanyName();

                teamCredentials.setPayStatus(1);
                teamCredentials.setPayTime(now);
                teamCredentials.setPayType("挂账");
                teamCredentials.setOriginalCompanyId(companyId);
                teamCredentials.setOriginalCompanyName(companyName);

                System.out.println("=======挂账线下团队支付开始=====");

                // 订单支付
                List<Orders> teamOrderList = ordersService.getTeamOrdersByTeamId(teamId);
                Integer count = 0;
                for (Orders orders : teamOrderList) {
                    ordersService.offlinePay(null, null, null, null, null
                            , operatorAccount, companyId, orders.getOrderId(), payTypeId, null, null, null
                            , null, null, null, null, null
                            , null, organizer, companyName, transactionId, null);
                    count++;
                    if (count >= 15) {
                        Thread.sleep(1000 * 10);
                        count = 0;
                    }
                }
                System.out.println("=======挂账线下团队支付结束=====");

                teamCredentials.setStatus(2);
            } else if (payTypeId == 5) { // 预付
                teamCredentials.setPayStatus(0);
                teamCredentials.setPayType("预付");
            } else {
                // 暂不支持其他支付方式
                return WebResult.getErrorMsgResult(WebResult.Code.ERROR, "支付方式错误，请重新选择支付方式");
            }
            // 处理团队单
            teamCredentialsService.updateByTeamId(teamCredentials);
        } catch (Exception e) {
            e.printStackTrace();
            return WebResult.getErrorMsgResult(WebResult.Code.ERROR, "系统异常");
        }
        return WebResult.getSuccessResult();
    }

    @ApiOperation(value = "团队挂账关闭订单", notes = "团队挂账关闭订单", httpMethod = "POST")
    @PostMapping("/closeOrder.json")
    public WebResult closeOrder(@RequestParam("orderId") Integer orderId, @RequestParam("note") String note, HttpServletRequest request){
        CommonAccount commonAccount = WebUpmsContext.getAccount(request);
        return ordersService.closeOrder(orderId, note, commonAccount);
    }
}
