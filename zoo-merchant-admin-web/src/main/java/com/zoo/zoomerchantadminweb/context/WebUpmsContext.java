package com.zoo.zoomerchantadminweb.context;

import com.zoo.activity.dao.model.Organizer;
import com.zoo.activity.dao.nutz.pojo.CommonAccount;

import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;

/**
 * @author LiuZhiMin
 * @date 2018/6/13 下午3:06
 * @Description: 上下文信息
 */
public class WebUpmsContext implements Serializable {

    private static final String ORGANIZATION = "user_organizer";

    private static final String ACCOUNT = "user_account";

    private static final String USER_NAME = "user_name";

    /**
     * @param request, account
     * @return void
     * @author LiuZhiMin
     * @date 2018/6/13 下午3:25
     * @Description: 设置登录后的账号信息
     */
    public static void setAccount(HttpServletRequest request, CommonAccount account) {
        request.setAttribute(ACCOUNT, account);
        request.setAttribute(USER_NAME, account.getIsnowPermission().equals("*") ? "总管理员" : account.getRealname());
    }

    /**
     * @param request
     * @return com.zoo.activity.dao.nutz.pojo.CommonAccount
     * @author LiuZhiMin
     * @date 2018/6/13 下午3:26
     * @Description: 获取登录后的账号信息
     */
    public static CommonAccount getAccount(HttpServletRequest request) {
        CommonAccount account = (CommonAccount) request.getAttribute(ACCOUNT);
        if(account.getIsnowPermission().equals("*")){
            account.setRealname("总管理员");
            account.setNickname("总管理员");
        }
        return account;
    }

    /**
     * @param request
     * @return com.zoo.activity.dao.model.Organizer
     * @author LiuZhiMin
     * @date 2018/6/13 下午3:21
     * @Description: 获取登录后用户的机构信息
     */
    public static Organizer getOrgnization(HttpServletRequest request) {
        Organizer organizer = (Organizer) request.getAttribute(ORGANIZATION);
        return organizer;
    }

    /**
     * @param request, organizer
     * @return com.zoo.activity.dao.model.Organizer
     * @author LiuZhiMin
     * @date 2018/6/13 下午3:22
     * @Description: 设置登录后用户的机构信息
     */
    public static void setOrganization(HttpServletRequest request, Organizer organizer) {
        request.setAttribute(ORGANIZATION, organizer);
    }

    public static String getUserName(HttpServletRequest request) {
        return (String) request.getAttribute(USER_NAME);
    }


}
