package com.zoo.zoomerchantadminweb.export.controller;

import com.zoo.activity.dao.model.*;
import com.zoo.activity.dao.model.CoachGuideRecordView;
import com.zoo.activity.dao.nutz.pojo.CommonAccount;
import com.zoo.activity.service.*;
import com.zoo.activity.util.DateUtil;
import com.zoo.web.bean.WebResult;
import com.zoo.wechat.common.Config;
import com.zoo.zoomerchantadminweb.base.BaseController;
import com.zoo.zoomerchantadminweb.context.WebUpmsContext;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by ljq on 2018/8/27.
 */
@Api(value = "/api/server/coach/export", tags = {"【教练系统】导出excel相关接口"}, description = "教练后台导出excel相关接口")
@RestController
@RequestMapping("/api/server/coach/export")
public class CoachExportController extends BaseController {

    @Autowired
    private OrdersService ordersService;

    @Autowired
    private CoachService coachService;

    @Autowired
    private CourseService courseService;

    @Autowired
    private ActivityService activityService;

    @Autowired
    private PlayerService playerService;

    @Autowired
    private OrganizerService organizerService;

    @Autowired
    private VerifyService verifyService;

    @Autowired
    private CoachCommentService coachCommentService;

    /**
     * 导出教练信息
     */
    @ApiOperation(value = "导出教练信息接口", notes = "导出教练信息", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "realname", value = "教练姓名", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "phone", value = "手机(电话)", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "teachAge", value = "教龄", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "board", value = "技术类型(单板或冰球：1，双板或花滑：2  单板&双板或冰球&花滑：3)", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "sex", value = "性别(女0，男1)", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "startTime", value = "开始时间", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "endTime", value = "结束时间", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "timeType", value = "时段类型,默认值为0(0：有请假，1：有订单，2可出导[既没请假也没订单])", required = false, dataType = "int", paramType = "query"),
    })
    @RequestMapping(value = "/coach-list-by-organizer", method = RequestMethod.GET)
    public void exportCoachList(@RequestParam(value = "realname", required = false) String realname,
                                @RequestParam(value = "phone", required = false) String phone,
                                @RequestParam(value = "teachAge", required = false) Integer teachAge,
                                @RequestParam(value = "board", required = false) Integer board,
                                @RequestParam(value = "sex", required = false) Integer sex,
                                @RequestParam(value = "startTime", required = false) String startTime,
                                @RequestParam(value = "endTime", required = false) String endTime,
                                @RequestParam(value = "timeType", required = false) Integer timeType,
                                HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (null != organizer) {
            Coach coach = new Coach();
            coach.setOrganizerId(organizer.getOrganizerId());
            /*try {
                //get中文处理乱码
                if (realname != null)
                    realname = new String(realname.getBytes("iso8859-1"));
                if (phone != null)
                    phone = new String(phone.getBytes("iso8859-1"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }*/
            coach.setRealname(realname);
            coach.setPhone(phone);
            coach.setTeachAge(teachAge);
            coach.setBoard(board);
            coach.setSex(sex);
            List<Coach> coacheList= coachService.findCoachListWithoutPage(coach,startTime,endTime,timeType);
            String baseFilePath = Config.instance().getExcelTmpPath();
            String filePath = coachService.exportAssignCoachExcelFile(coacheList, organizer, baseFilePath);
            downLoadFile(filePath, response, "教练信息列表.xls");
        }
    }


    /**
     * 导出课程出导订单
     *
     * @param activityId
     * @param episodeId
     * @param status
     * @param phone
     * @param code
     * @param startTime
     * @param endTime
     * @param request
     * @param response
     * @param session
     * @throws Exception
     */
    @ApiOperation(value = "导出课程出导订单", notes = "导出课程出导订单", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "activityId", value = "活动ID(activityId)", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "episodeId", value = "订单ID", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "status", value = "订单状态(0:未使用 1:已付款 2:退款中 3:已退款 4:已关闭 5:已完成)", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "phone", value = "用户联系电话", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "code", value = "订单号", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "startTime", value = "筛选条件的订单创建时间(开始)", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "endTime", value = "筛选条件的订单创建时间(结束)", required = false, dataType = "string", paramType = "query"),
    })
    @RequestMapping(value = "/assign-course-ordersInfo-by-organizer", method = RequestMethod.GET)
    public void exportOrdersInfo(@RequestParam(value = "activityId", required = false) Integer activityId,
                                 @RequestParam(value = "episodeId", required = false) Integer episodeId,
                                 @RequestParam(value = "status", required = false) Integer status,
                                 @RequestParam(value = "phone", required = false) String phone,
                                 @RequestParam(value = "code", required = false) String code,
                                 @RequestParam(value = "startTime", required = false) String startTime,
                                 @RequestParam(value = "endTime", required = false) String endTime,
                                 HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer != null) {
            if (code != null) {
                code = code.trim();
            }

            // 默认导出3个月以前的数据
            Date calculateDate = null;
            if (StringUtils.isBlank(startTime)) {
                calculateDate = calculateBefore3MonthDate(null);
            } else {
                calculateDate = calculateBefore3MonthDate(DateUtil.convertStringToDate(startTime, "yyyy-MM-dd HH:mm:ss"));
            }
            startTime = DateUtil.convertDateToString(calculateDate, "yyyy-MM-dd HH:mm:ss");

            List<Map> orderCoachExtendList = coachService.getOrderExtendListByOptionWithoutPage(activityId, episodeId, status, code, phone, organizer.getOrganizerId(), startTime, endTime, null);
            Map<String, BigDecimal> orderCoachEachPriceCount = coachService.getOrderExtendListPriceByOptionFromPay(activityId, episodeId, status, code, phone, organizer.getOrganizerId(), startTime, endTime, null);
            String baseFilePath = Config.instance().getExcelTmpPath();
            String filePath = ordersService.exportAssignCoachExcelFile(orderCoachExtendList, baseFilePath, orderCoachEachPriceCount);
            downLoadFile(filePath, response, "指定教练预约订单信息.xls");
        }
    }

    /**
     * 教练系统中出导记录导表excel接口
     * @param activityId
     * @param episodeId
     * @param extendStatus   出导状态
     * @param coachId        教练ID
     * @param coachPhone     教练手机号
     * @param code
     * @param appStartTime   预约开始时间
     * @param appEndTime     预约结束时间
     * @param request
     * @param response
     * @param session
     * @throws Exception
     */
    @ApiOperation(value = "出导记录导表excel接口", notes = "出导记录导表excel接口", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "activityId", value = "活动ID(activityId)", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "episodeId", value = "场次ID", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "status", value = "出导状态(0:待确认 1:已确认 2:已驳回 3:已转接 4:已取消 5:已出导)", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "coachId", value = "教练ID", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "phone", value = "教练联系电话", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "code", value = "订单号", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "startTime", value = "筛选条件的预约时间(开始)", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "endTime", value = "筛选条件的预约时间(结束)", required = false, dataType = "string", paramType = "query"),
    })
    @RequestMapping(value = "/assign-course-guideRecordInfo-by-organizer", method = RequestMethod.GET)
    public void exportGuideRecordInfo(@RequestParam(value = "activityId", required = false) Integer activityId,
                                      @RequestParam(value = "episodeId", required = false) Integer episodeId,
                                      @RequestParam(value = "status", required = false) Integer extendStatus,
                                      @RequestParam(value = "coachId", required = false) Integer coachId,
                                      @RequestParam(value = "phone", required = false) String coachPhone,
                                      @RequestParam(value = "code", required = false) String code,
                                      @RequestParam(value = "startTime", required = false) String appStartTime,
                                      @RequestParam(value = "endTime", required = false) String appEndTime,
                                      HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer != null) {
            if (code != null) {
                code = code.trim();
            }

            // 默认导出3个月以前的数据
            Date calculateDate = null;
            if (StringUtils.isBlank(appStartTime)) {
                calculateDate = calculateBefore3MonthDate(null);
            } else {
                calculateDate = calculateBefore3MonthDate(DateUtil.convertStringToDate(appStartTime, "yyyy-MM-dd HH:mm:ss"));
            }
            appStartTime = DateUtil.convertDateToString(calculateDate, "yyyy-MM-dd HH:mm:ss");

            List<CoachGuideRecordView> coachGuideRecordList = coachService.getCoachGuideRecordListByOptionWithoutPage(activityId, episodeId, extendStatus, code, coachPhone, organizer.getOrganizerId(), appStartTime, appEndTime, coachId);
            String baseFilePath = Config.instance().getExcelTmpPath();
            String filePath = ordersService.exportAssignCoachGuideRecordExcelFile(coachGuideRecordList, organizer, baseFilePath);
            downLoadFile(filePath, response, "教练出导记录信息.xls");
        } else {
            //logger.info("无法获取session中organizer信息--接口：" + "/export-assign-course-guideRecordInfo-by-organizer");
        }
    }

    /**
     * [教练系统] - 出导订单页面：导出报名表按钮接口
     */
    @ApiOperation(value = "[教练系统]-出导订单页面：导出报名表按钮接口", notes = "导出报名表excel接口", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "activityId", value = "活动ID(activityId)", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "episodeId", value = "场次ID", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "status", value = "订单状态", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "fromAssign", value = "是否指定课程", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "startTime", value = "订单创建时间(开始)", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "endTime", value = "订单创建时间(结束)", required = false, dataType = "string", paramType = "query"),
    })
    @RequestMapping(value = "/assign-course-playerInfo-by-activity", method = RequestMethod.GET)
    public Object ExportPlayerByActivity(@RequestParam(value = "activityId", required = false) Integer activityId,
                       @RequestParam(value = "episodeId", required = false) Integer episodeId,
                       @RequestParam(value = "status", required = false) Integer status,
                       @RequestParam(value = "fromAssign", required = false) Integer fromAssign,
                       @RequestParam(value = "startTime", required = false) String startTime,
                       @RequestParam(value = "endTime", required = false) String endTime,
                       HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        CommonAccount account = WebUpmsContext.getAccount(request);
        String permissionStr = account.getIsnowPermission();

        if (episodeId == null) {
            return WebResult.getErrorResult(WebResult.Code.COACH_COURSE_NO_HAVE_ERROR);
        }

        if (fromAssign != null) {
            activityId = courseService.getActivityIdByEpisodeId(episodeId);
        }
        String baseFilePath = Config.instance().getExcelTmpPath();
        if (status == null) {
            status = -1;
        }
        Boolean hasPermission = false;
        if (permissionStr.contains("player") || permissionStr.equals("*"))
            hasPermission = true;

        Boolean isOwner = activityService.checkIsOrganizer(organizer.getOrganizerId(), activityId);
        if (!isOwner || !hasPermission) {
            // injectResponse(response, "没有权限");
            return WebResult.getErrorResult(WebResult.Code.NO_PERMISSION);
        }

        String filePath;
        if (null != episodeId && 0 != episodeId) {
            List<PlayerWithEnrollInfo> players = playerService.getPlayersByEpisodeId(episodeId, status, startTime, endTime);
            filePath = playerService.exportExcelFile(players, episodeId, startTime, endTime, baseFilePath);
        } else {
            filePath = playerService.exportExcelFile2(activityId, status, startTime, endTime, baseFilePath);
        }
        downLoadFile(filePath, response, "报名表信息.xls");

        return WebResult.getSuccessResult();
    }

    /**
     * 导出核销记录列表excel
     *
     * @param activityId
     * @param episodeId
     * @param startTime
     * @param endTime
     * @param code
     * @param session
     */
    @ApiOperation(value = "[教练系统]-核销记录页面：导出核销记录列表excel", notes = "导出核销记录列表excel", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "activityId", value = "活动ID(activityId)", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "episodeId", value = "场次ID", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "startTime", value = "订单创建时间(开始) 时间戳", required = false, dataType = "long", paramType = "query"),
            @ApiImplicitParam(name = "endTime", value = "订单创建时间(结束) 时间戳", required = true, dataType = "long", paramType = "query"),
            @ApiImplicitParam(name = "code", value = "订单号", required = false, dataType = "string", paramType = "query")
    })
    @RequestMapping(value = "/verifyInfo-by-organizer", method = RequestMethod.GET)
    public void getCoachVerifyInfoByOrganizer(@RequestParam(value = "activityId", required = false) Integer activityId,
                                         @RequestParam(value = "episodeId", required = false) Integer episodeId,
                                         @RequestParam(value = "startTime", required = false) Long startTime,
                                         @RequestParam(value = "endTime", required = false) Long endTime,
                                         @RequestParam(value = "code", required = false) String code,
                                         @RequestParam(value = "isRefund", required = false) String isRefund,
                                         HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (null != organizer) {
            List<Integer> typeIds = new ArrayList<>();
            typeIds.add(2);
            typeIds.add(8);

            Date startDate = null;
            Date endDate = null;
            if (startTime != null) {
                startDate = new Date(startTime);
            }
            if (endTime != null) {
                endDate = new Date(endTime);
            }
            if (StringUtils.isNotBlank(code)) {
                code = code.trim();
            }

            // 默认导出3个月以前的数据
            Date calculateDate = null;
            if (startDate == null) {
                startDate = calculateBefore3MonthDate(null);
            } else {
                startDate = calculateBefore3MonthDate(startDate);
            }

            if (episodeId != null) {
                activityId = courseService.getActivityIdByEpisodeId(episodeId);
            }

            List<VerifyRecordWithInfo> verifyInfoList = verifyService.getVerifyRecordInfoByOrganizer(activityId, episodeId, code, startDate, endDate, typeIds, organizer.getOrganizerId(),isRefund);
            String baseFilePath = Config.instance().getExcelTmpPath();
            String filePath = verifyService.exportExcelFile(verifyInfoList, baseFilePath, organizer);
            downLoadFile(filePath, response, "教练订单核销信息.xls");
        }
    }

    /**
     * 导出列表excel - 评价管理
     */
    @ApiOperation(value = "导出评价列表", notes = "导出评价列表", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "activityId", value = "活动ID(activityId)", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "episodeId", value = "活动ID(episodeId)", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "coachId", value = "教练ID", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "hasPhoto", value = "有图片", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "hasReply", value = "有回复", required = false, dataType = "int", paramType = "query"),
    })
    @RequestMapping(value = "/commentInfo-by-organizer", method = RequestMethod.GET)
    public void getCoachCommentInfoByOrganizer(@RequestParam(value = "activityId", required = false) Integer activityId,
                                               @RequestParam(value = "episodeId", required = false) Integer episodeId,
                                               @RequestParam(value = "coachId", required = false) Integer coachId,
                                               @RequestParam(value = "hasPhoto", required = false) Integer hasPhoto,
                                               @RequestParam(value = "hasReply", required = false) Integer hasReply,
                                               HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (null != organizer) {
            List<CoachComment> coachCommentList = coachCommentService.getExcelData4CoachCommentListByOption(activityId, episodeId, coachId, hasPhoto, hasReply, organizer.getOrganizerId());
            String baseFilePath = Config.instance().getExcelTmpPath();
            String filePath = coachCommentService.exportExcelFile4CoachComment(coachCommentList, baseFilePath, organizer);
            downLoadFile(filePath, response, "教练评价信息表.xls");
        }
    }
}
