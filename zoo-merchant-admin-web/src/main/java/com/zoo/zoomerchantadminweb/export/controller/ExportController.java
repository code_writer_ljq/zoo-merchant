package com.zoo.zoomerchantadminweb.export.controller;

import com.zoo.account.service.CustomerOrgService;
import com.zoo.activity.dao.model.*;
import com.zoo.activity.service.*;
import com.zoo.activity.util.DateUtil;
import com.zoo.activity.util.MongoUtil;
import com.zoo.icenow.service.StorageService;
import com.zoo.rest.sms.sdk.utils.encoder.BASE64Encoder;
import com.zoo.web.bean.WebResult;
import com.zoo.wechat.common.Config;
import com.zoo.zoomerchantadminweb.base.BaseController;
import com.zoo.zoomerchantadminweb.context.WebUpmsContext;
import com.zoo.zoomerchantadminweb.finance.controller.poi.util.FinanceExcelUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CellStyle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;


@Api(value = "api/server/export", tags = {"主办方导出excel相关接口"}, description = "主办方导出excel相关接口描述")
@RestController
@RequestMapping("api/server/export")
public class ExportController extends BaseController {

    @Resource(name = "playerService")
    private PlayerService playerService;

    @Resource(name = "activityService")
    private ActivityService activityService;

    @Resource(name = "orderService")
    private OrdersService ordersService;

    @Resource(name = "customerOrgService")
    private CustomerOrgService customerOrgService;

    @Resource(name = "storageService")
    private StorageService storageService;

    @Autowired
    private EpisodeService episodeService;

    @Autowired
    private EquipmentTotalService equipmentTotalService;

    @Autowired
    private EquipmentDetailService equipmentDetailService;

    @Autowired
    private OrganizerService organizerService;

    @Autowired
    private VerifyService verifyService;

    @Autowired
    private DepositService depositService;

    @Autowired
    private LimitCardService limitCardService;

    /**
     * 导出报名人员的excel
     */
    @ApiOperation(value = "导出server后台报名人员excel", httpMethod = "GET")
    @RequestMapping(value = "/export-player-by-activity", method = RequestMethod.GET)
    public void Export(@RequestParam(value = "activityId", required = false) Integer activityId,
                       @RequestParam(value = "episodeId", required = false) Integer episodeId,
                       @RequestParam(value = "status", required = false) Integer status,
//                       @RequestParam(value = "fromAssign", required = false) Integer fromAssign,
                       @RequestParam(value = "startTime", required = false) String startTime,
                       @RequestParam(value = "endTime", required = false) String endTime,
                       HttpServletRequest request, HttpServletResponse response) throws Exception {
//        if (fromAssign != null) {
//            episodeId = courseService.getCourseIdByActivityId(activityId).getEpisodeId();
//        }
        String baseFilePath = Config.instance().getExcelTmpPath();
        if (status == null) {
            status = -1;
        }
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        Boolean hasPermission = false;
        String permissionStr = WebUpmsContext.getAccount(request).getIsnowPermission();
        if (permissionStr.contains("player") || permissionStr.equals("*")) hasPermission = true;
        Boolean isOwner = activityService.checkIsOrganizer(organizer.getOrganizerId(), activityId);
        if (!isOwner || !hasPermission) {
            response.setContentType("text/html;charset=UTF-8");
            response.setCharacterEncoding("UTF-8");
            response.setHeader("Cache-Control", "no-cache");
            PrintWriter pw = response.getWriter();
            pw.write(WebResult.Code.NO_PERMISSION.getMessage());
            pw.flush();
            pw.close();
        }

        String filePath;
        if (null != episodeId && 0 != episodeId) {
            List<PlayerWithEnrollInfo> players = playerService.getPlayersByEpisodeId(episodeId, status, startTime, endTime);
            filePath = playerService.exportExcelFile(players, episodeId, startTime, endTime, baseFilePath);
        } else {
            filePath = playerService.exportExcelFile2(activityId, status, startTime, endTime, baseFilePath);
        }
        downLoadFile(filePath, response, "报名信息.xls");
    }

    /**
     * 导出报名人员的excel
     */
    @ApiOperation(value = "通过episode导出server后台报名人员excel", httpMethod = "GET")
    @RequestMapping(value = "/export-player-by-episode", method = RequestMethod.GET)
    public void Export(@RequestParam(value = "episodeId", required = false) Integer episodeId,
                       @RequestParam(value = "status", required = false) Integer status,
                       @RequestParam(value = "startTime", required = false) String startTime,
                       @RequestParam(value = "endTime", required = false) String endTime,
                       HttpServletRequest request, HttpServletResponse response) throws Exception {
        String baseFilePath = Config.instance().getExcelTmpPath();
        if (status == null) {
            status = -1;
        }
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        Boolean hasPermission = false;
        String permissionStr = WebUpmsContext.getAccount(request).getIsnowPermission();
        if (permissionStr.contains("player") || permissionStr.equals("*")) hasPermission = true;
        Boolean isOwner = activityService.checkIsOrganizer(organizer.getOrganizerId(), episodeService.getEpisodeDetailById(episodeId).getActivityId());
        if (!isOwner || !hasPermission) {
            injectResponse(response, "没有权限");
        }

        List<PlayerWithEnrollInfo> players = playerService.getPlayersByEpisodeId(episodeId, status, startTime, endTime);
        String filePath = playerService.exportExcelFile(players, episodeId, startTime, endTime, baseFilePath);

        downLoadFile(filePath, response, "报名信息.xls");
    }

    /**
     * 导出server后台订单列表excel
     *
     * @param activityId
     * @param episodeId
     * @param status
     * @param phone
     * @param code
     * @param startTime
     * @param endTime
     * @param request
     * @param response
     * @throws Exception
     */
    @ApiOperation(value = "导出server后台订单列表excel", httpMethod = "GET")
    @RequestMapping(value = "/export-ordersInfo-by-organizer", method = RequestMethod.GET)
    public void exportOrdersInfo(@RequestParam(value = "activityId", required = false) Integer activityId,
                                 @RequestParam(value = "episodeId", required = false) Integer episodeId,
                                 @RequestParam(value = "status", required = false) Integer status,
                                 @RequestParam(value = "phone", required = false) String phone,
                                 @RequestParam(value = "code", required = false) String code,
                                 @RequestParam(value = "startTime", required = false) String startTime,
                                 @RequestParam(value = "endTime", required = false) String endTime,
                                 @RequestParam(value = "removeRepeat", required = false, defaultValue = "0") String removeRepeat,
                                 @RequestParam(value = "payType", required = false) Integer payType,
                                 HttpServletRequest request, HttpServletResponse response) throws Exception {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer != null) {
            if (code != null) {
                code = code.trim();
            }
            List<OrderViewActivity> orderViewList = ordersService.getOrderInfoByOrganizer(activityId, episodeId, status, code, phone, organizer.getOrganizerId(), startTime, endTime, payType);
            String baseFilePath = Config.instance().getExcelTmpPath();
            String filePath = ordersService.exportExcelFile(orderViewList, baseFilePath, removeRepeat);
            downLoadFile(filePath, response, "订单信息.xls");
        }
    }

    @ApiOperation(value = "导出异常退款订单", httpMethod = "GET")
    @RequestMapping(value = "/export-ordersInfo-by-remark", method = RequestMethod.GET)
    public void exportOrdersInfoRemark(@RequestParam(value = "activityId", required = false) Integer activityId,
                                       @RequestParam(value = "episodeId", required = false) Integer episodeId,
                                       @RequestParam(value = "status", required = false) Integer status,
                                       @RequestParam(value = "phone", required = false) String phone,
                                       @RequestParam(value = "code", required = false) String code,
                                       @RequestParam(value = "startTime", required = false) String startTime,
                                       @RequestParam(value = "endTime", required = false) String endTime,
                                       @RequestParam(value = "removeRepeat", required = false, defaultValue = "0") String removeRepeat,
                                       HttpServletRequest request, HttpServletResponse response) throws Exception {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer != null) {
            if (code != null) {
                code = code.trim();
            }
            List<Map> orderViewList = ordersService.selectOrdersInfoRemark(activityId, episodeId, status, code, phone, organizer.getOrganizerId(), startTime, endTime);

            HSSFWorkbook wb = new HSSFWorkbook();
            HSSFSheet sheet = wb.createSheet("异常退款订单表");

            response.reset();
            response.setContentType("application/x-excel");
            String agent = request.getHeader("User-Agent");
            String fileName = encodeDownloadFilename("异常退款订单表", agent);
            response.setHeader("Content-disposition", "attachment;filename=" + fileName + ".xls");
            ServletOutputStream outputStream = response.getOutputStream();

            int rowNum = 0;
            HSSFCellStyle headStyle = wb.createCellStyle(); // 样式对象
            HSSFFont font = wb.createFont();// 生成一个字体
            font.setColor(HSSFColor.BLACK.index);// HSSFColor.VIOLET.index // 字体颜色
            font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);// 字体增粗
            font.setFontHeightInPoints((short) 16);
            headStyle.setFont(font);// 把字体应用到当前的样式
            HSSFRow row = sheet.createRow(rowNum);
            HSSFCell cell = row.createCell(1);
            cell.setCellValue("时间：" + (startTime == null ? "" : startTime) + "-" + (endTime == null ? "" : endTime));
            cell.setCellStyle(headStyle);

            headStyle = wb.createCellStyle(); // 样式对象
            headStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
            headStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
            headStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
            headStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
            headStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
            headStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);//垂直居中
            rowNum++;
            createHead(wb, sheet, rowNum, new String[]{"订单号", "下单时间", "支付方式", "产品名称", "场次名称", "异常退款金额", "收银员", "备注"});
            rowNum++;
            if (orderViewList != null) {
                for (Map map : orderViewList) {
                    row = sheet.createRow(rowNum);
                    cell = row.createCell(1);
                    cell.setCellValue(MapUtils.getString(map, "code"));
                    cell.setCellStyle(headStyle);

                    cell = row.createCell(2);
                    Date d = (Date) map.get("create_time");
                    cell.setCellValue(DateUtil.convertDateToString(d, "yyyy-MM-dd HH:mm:ss"));
                    cell.setCellStyle(headStyle);

                    cell = row.createCell(3);
                    cell.setCellValue(FinanceExcelUtil.getPayName(MapUtils.getIntValue(map, "pay_type_id", -1)));
                    cell.setCellStyle(headStyle);

                    cell = row.createCell(4);
                    cell.setCellValue(MapUtils.getString(map, "activity_title"));
                    cell.setCellStyle(headStyle);

                    cell = row.createCell(5);
                    cell.setCellValue(MapUtils.getString(map, "episode_title"));
                    cell.setCellStyle(headStyle);

                    cell = row.createCell(6);
                    cell.setCellValue(new BigDecimal(MapUtils.getDouble(map, "ext_abnormal_refund_money", 0d)).setScale(2, RoundingMode.HALF_UP).doubleValue());
                    cell.setCellStyle(headStyle);

                    cell = row.createCell(7);
                    cell.setCellValue(MapUtils.getString(map, "realname"));
                    cell.setCellStyle(headStyle);

                    cell = row.createCell(8);
                    cell.setCellValue(MapUtils.getString(map, "remark"));
                    cell.setCellStyle(headStyle);

                    rowNum++;
                }

            }
            wb.write(outputStream);
            outputStream.close();

        }
    }

    private void createHead(HSSFWorkbook wb, HSSFSheet sheet, int rowNum, String[] head) {
        HSSFCellStyle style = wb.createCellStyle(); // 样式对象
        HSSFFont font = wb.createFont();// 生成一个字体
        font.setColor(HSSFColor.BLACK.index);// HSSFColor.VIOLET.index // 字体颜色
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);// 字体增粗
        style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        style.setFillForegroundColor(HSSFColor.GREY_40_PERCENT.index);
        style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        style.setBorderRight(HSSFCellStyle.BORDER_THIN);
        style.setBorderTop(HSSFCellStyle.BORDER_THIN);
        style.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);//垂直居中
        style.setFont(font);// 把字体应用到当前的样式
        //style.setFillBackgroundColor(HSSFColor.BLACK.index);
        HSSFRow row = sheet.createRow(rowNum);
        int index = 1;
        for (String s : head) {
            HSSFCell cell = row.createCell(index);
            cell.setCellValue(s);
            cell.setCellStyle(style);
            sheet.setColumnWidth(index, 16 * 200);
            index++;
        }
    }

    public static String encodeDownloadFilename(String filename, String agent)
            throws IOException {
        if (agent.contains("Firefox")) { // 火狐浏览器
            filename = "=?UTF-8?B?"
                    + new BASE64Encoder().encode(filename.getBytes("utf-8"))
                    + "?=";
            filename = filename.replaceAll("\r\n", "");
        } else { // IE及其他浏览器
            filename = URLEncoder.encode(filename, "utf-8");
            filename = filename.replace("+", " ");
        }
        return filename;
    }

    @ApiOperation(value = "导出server后台押金列表excel", httpMethod = "GET")
    @RequestMapping(value = "/export-deposit-by-organizer", method = RequestMethod.GET)
    public void exportOrdersInfo(@RequestParam(value = "status", required = false) Integer status,
                                 @RequestParam(value = "payType", required = false) Integer payType,
                                 @RequestParam(value = "code", required = false) String code,
                                 @RequestParam(value = "phone", required = false) String phone,
                                 @RequestParam(value = "createTimeStart", required = false) String createTimeStart,
                                 @RequestParam(value = "createTimeEnd", required = false) String createTimeEnd,
                                 @RequestParam(value = "refundTimeStart", required = false) String refundTimeStart,
                                 @RequestParam(value = "refundTimeEnd", required = false) String refundTimeEnd,
                                 HttpServletRequest request, HttpServletResponse response) throws Exception {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer != null) {
            if (code != null) {
                code = code.trim();
            }
            String baseFilePath = Config.instance().getExcelTmpPath();
            List list = depositService.getDepositListByOption(null, status, code, organizer.getOrganizerId(), null, phone, 1, 1000000, payType, createTimeStart, createTimeEnd, refundTimeStart, refundTimeEnd).getDataList();
            String filePath = depositService.exportDepositExcelFile(list, baseFilePath);
            downLoadFile(filePath, response, "押金信息.xls");
        }
    }

    @ApiOperation(value = "导出server后台指纹列表excel", httpMethod = "GET")
    @RequestMapping(value = "/export-fingerprint", method = RequestMethod.GET)
    public void exportFingerprintInfo(
            @RequestParam(value = "phone", required = false) String phone,
            @RequestParam(value = "name", required = false) String name,
            HttpServletRequest request, HttpServletResponse response) throws Exception {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer != null) {
//            List<OrderViewActivity> orderViewList = ordersService.getOrderInfoByOrganizer(activityId, episodeId, status, code, phone, organizer.getOrganizerId(), startTime, endTime);
            String baseFilePath = Config.instance().getExcelTmpPath();
            String filePath = customerOrgService.exportFingerprintExcelFile(phone, name, baseFilePath, organizer.getOrganizerId());
            downLoadFile(filePath, response, "指纹信息.xls");
        }
    }


    @ApiOperation(value = "导出server后台订单列表excel", notes = "导出server后台订单列表excel功能", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "exportUrl", value = "检查有没有文件可下载时存的是redis中的KEY 下载时是文件下载地址", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "fileTitle", value = "下载文件名字", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "checkOrDownload", value = "0.检查有没有可下载文件  1.下载excel文件", required = true, dataType = "int", paramType = "query", defaultValue = "0")
    })
    @RequestMapping(value = "/check-download-excel.json", method = RequestMethod.GET)
    public Object checkExcelDownload(@RequestParam(value = "exportUrl") String exportUrl,
                                     @RequestParam(value = "fileTitle", required = false) String fileTitle,
                                     @RequestParam(value = "checkOrDownload", defaultValue = "0") Integer checkOrDownload,
                                     HttpServletRequest request,
                                     HttpServletResponse response) throws Exception {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null) {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
        WebResult result = WebResult.getSuccessResult();
        if (checkOrDownload == 0) {
            result = customerOrgService.checkHasExcel(exportUrl, organizer.getOrganizerId());
        } else {
            // 下载excel文件
            if (StringUtils.isNotBlank(fileTitle)) {
                fileTitle = URLDecoder.decode(fileTitle, "UTF-8");
            }
//            String baseFilePath = request.getSession().getServletContext().getRealPath("/");
            String baseFilePath = Config.instance().getExcelTmpPath();
            newDownLoadFile(exportUrl, response, fileTitle, baseFilePath);
        }

        return result;
    }


    @ApiOperation(value = "导出会员信息excel", notes = "导出会员信息excel功能", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "phone", value = "会员手机号", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "name", value = "会员名", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "startAmount", value = "查询金额起始", required = false, dataType = "bigDecimal", paramType = "query"),
            @ApiImplicitParam(name = "endAmount", value = "查询金额结束", required = false, dataType = "bigDecimal", paramType = "query"),
            @ApiImplicitParam(name = "startCreateTime", value = "查询注册时间起始", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "endCreateTime", value = "查询注册时间结束", required = false, dataType = "string", paramType = "query"),
//            @ApiImplicitParam(name = "payedPriceDesc", value = "累计消费金额排序 0.降序 1.升序", required = false, dataType = "int", paramType = "query", defaultValue = "0"),
//            @ApiImplicitParam(name = "storageAmountDesc", value = "储值金额排序 0.降序 1.升序", required = false, dataType = "int", paramType = "query", defaultValue = "0"),
            @ApiImplicitParam(name = "memberType", value = "会员类别 0.全部 1.储值会员 2.普通会员 3.储值且普通", required = false, dataType = "int", paramType = "query", defaultValue = "0")
    })
    @RequestMapping(value = "/export-member-list.json", method = RequestMethod.GET)
    public Object memberExport(@RequestParam(value = "phone", required = false) String phone,
                               @RequestParam(value = "name", required = false) String name,
                               @RequestParam(value = "startAmount", required = false) BigDecimal startAmount,
                               @RequestParam(value = "endAmount", required = false) BigDecimal endAmount,
                               @RequestParam(value = "startCreateTime", required = false) String startCreateTime,
                               @RequestParam(value = "endCreateTime", required = false) String endCreateTime,
//                               @RequestParam(value = "payedPriceDesc", required = false, defaultValue = "0") Integer payedPriceDesc,
//                               @RequestParam(value = "storageAmountDesc", required = false, defaultValue = "0") Integer storageAmountDesc,
                               @RequestParam(value = "memberType", required = false, defaultValue = "0") Integer memberType,
                               HttpServletRequest request) {

        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null) {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }

        WebResult result = WebResult.getSuccessResult();
        try {
            // redis加锁控制多人同时生成一个业务的数据
            String filterKey = request.getRequestURI();
            filterKey += (StringUtils.isNotBlank(phone)) ? ("-phone=" + phone) : ("-phone=");
            filterKey += (StringUtils.isNotBlank(name)) ? ("-name=" + name) : ("-name=");
            filterKey += (startAmount != null) ? ("-startAmount=" + startAmount) : ("-startAmount=");
            filterKey += (endAmount != null) ? ("-endAmount=" + endAmount) : ("-endAmount=");
            filterKey += (StringUtils.isNotBlank(startCreateTime)) ? ("-startCreateTime=" + startCreateTime) : ("-startCreateTime=");
            filterKey += (StringUtils.isNotBlank(endCreateTime)) ? ("-endCreateTime=" + endCreateTime) : ("-endCreateTime=");
//            filterKey += (payedPriceDesc != null) ? ("-payedPriceDesc=" + payedPriceDesc) : ("-payedPriceDesc=");
//            filterKey += (storageAmountDesc != null) ? ("-storageAmountDesc=" + storageAmountDesc) : ("-storageAmountDesc=");
            filterKey += (memberType != null && memberType != 0) ? ("-memberType=" + memberType) : ("-memberType=");
            filterKey += "-organizerId=" + organizer.getOrganizerId();

//            String baseFilePath = request.getSession().getServletContext().getRealPath("/");
            String baseFilePath = Config.instance().getExcelTmpPath();
            result = customerOrgService.memberExport(filterKey, baseFilePath, organizer.getOrganizerId(), phone, name, startAmount, endAmount, startCreateTime, endCreateTime, 0, 0, memberType);

        } catch (Exception e) {
            e.printStackTrace();
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }
        return result;
    }

    @ApiOperation(value = "导出会员消费记录列表excel", notes = "导出会员详情中 消费记录列表excel", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "customerId", value = "用户id", required = true, dataType = "int", paramType = "query")
    })
    @RequestMapping(value = "/export-member-payMoney-list.json", method = RequestMethod.GET)
    public Object memberPayMoneyExport(@RequestParam(value = "customerId", required = true) Integer customerId,
                                       @RequestParam(value = "isAll", required = false, defaultValue = "0") Integer isAll,
                                       HttpServletRequest request) {
        // 主办方信息
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null) {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
        WebResult result = WebResult.getSuccessResult();
        try {
            // redis加锁控制多人同时生成一个业务的数据
            String filterKey = request.getRequestURI();
            filterKey += (customerId != null) ? ("-customerId=" + customerId) : ("-customerId=");
            filterKey += "-organizerId=" + organizer.getOrganizerId();
            String baseFilePath = Config.instance().getExcelTmpPath();
            result = customerOrgService.memberPayMoneyExport(filterKey, baseFilePath, customerId, organizer.getOrganizerId(), isAll);
        } catch (Exception e) {
            e.printStackTrace();
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }
        return result;
    }

    @ApiOperation(value = "导出会员储值消费记录列表excel", notes = "导出会员详情中 消费记录列表excel", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "customerId", value = "用户id", required = true, dataType = "int", paramType = "query")
    })
    @RequestMapping(value = "/export-member-storagePayMoney-list.json", method = RequestMethod.GET)
    public Object memberStoragePayMoneyExport(@RequestParam(value = "customerId", required = true) Integer customerId,
                                       @RequestParam(value = "isAll", required = false, defaultValue = "0") Integer isAll,
                                       HttpServletRequest request) {
        // 主办方信息
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null) {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
        WebResult result = WebResult.getSuccessResult();
        try {
            // redis加锁控制多人同时生成一个业务的数据
            String filterKey = request.getRequestURI();
            filterKey += (customerId != null) ? ("-customerId=" + customerId) : ("-customerId=");
            filterKey += "-organizerId=" + organizer.getOrganizerId();
            String baseFilePath = Config.instance().getExcelTmpPath();
            result = customerOrgService.memberPayMoneyExport(filterKey, baseFilePath, customerId, organizer.getOrganizerId(), isAll);
        } catch (Exception e) {
            e.printStackTrace();
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }
        return result;
    }

    @ApiOperation(value = "导出会员储值充值记录列表excel", notes = "导出会员详情中 储值充值记录列表excel", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "customerId", value = "用户id", required = true, dataType = "int", paramType = "query")
    })
    @RequestMapping(value = "/export-member-storageIncome-list.json", method = RequestMethod.GET)
    public Object memberStorageIncomeExport(@RequestParam(value = "customerId", required = true) Integer customerId,
                                            HttpServletRequest request) {
        // 主办方信息
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null) {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
        WebResult result = WebResult.getSuccessResult();
        try {
            // redis加锁控制多人同时生成一个业务的数据
            String filterKey = request.getRequestURI();
            filterKey += (customerId != null) ? ("-customerId=" + customerId) : ("-customerId=");
            filterKey += "-organizerId=" + organizer.getOrganizerId();
            String baseFilePath = Config.instance().getExcelTmpPath();
            result = storageService.memberStorageExport(filterKey, baseFilePath, customerId, organizer.getOrganizerId());
        } catch (Exception e) {
            e.printStackTrace();
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }
        return result;
    }

    @ApiOperation(value = "导出租赁物库存列表excel", notes = "导出租赁物库存列表excel", httpMethod = "GET")
    @RequestMapping(value = "/equipment-inventory-list.json", method = RequestMethod.GET)
    public void equipmentTotalExport(HttpServletRequest request, HttpServletResponse response) throws Exception {
        // 主办方信息
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer != null) {
            // 查询当前主办方下设置的所有租赁物的库存情况
            EquipmentTotalExample equipmentTotalExample = new EquipmentTotalExample();
            equipmentTotalExample.createCriteria().andOrganizerIdEqualTo(organizer.getOrganizerId()) // 主办方ID
                    .andValidEqualTo(1); // 有效的
            List<EquipmentTotal> equipmentTotalList = equipmentTotalService.selectByExample(equipmentTotalExample);
            String baseFilePath = Config.instance().getExcelTmpPath();
            String filePath = equipmentTotalService.exportEquipmentTotalExcelFile(equipmentTotalList, baseFilePath);
            downLoadFile(filePath, response, "租赁物库存信息.xls");
        }
    }

    @ApiOperation(value = "导出租赁物异常列表excel", notes = "导出租赁物异常列表excel", httpMethod = "GET")
    @RequestMapping(value = "/equipment-abnormal-statistics-list.json", method = RequestMethod.GET)
    public void equipmentDetailExport(HttpServletRequest request, HttpServletResponse response) throws Exception {
        // 主办方信息
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer != null) {
            // 查询当前主办方下所有库存租赁物的异常情况
            EquipmentDetailExample equipmentDetailExample = new EquipmentDetailExample();
            // 封装查询条件
            List<Integer> statusIdList = new ArrayList<>();
            statusIdList.add(3); // 超时未处理
            statusIdList.add(4); // 超时已处理
            statusIdList.add(5); // 损坏未处理
            statusIdList.add(6); // 损坏已处理
            equipmentDetailExample.createCriteria().andOrganizerIdEqualTo(organizer.getOrganizerId()) // 主办方ID
                    .andStatusIn(statusIdList)
                    .andValidEqualTo(1); // 有效的

            // 查询当前主办方下所有租赁物异常情况概览数据
            List<EquipmentDetail> equipmentDetailList = equipmentDetailService.selectByExample(equipmentDetailExample);
            List<EquipmentDetailStatisticsView> equipmentDetailStatisticsViews = equipmentDetailService.equipmentDetailStatisticsData(equipmentDetailList, false, false);
            String baseFilePath = Config.instance().getExcelTmpPath();
            String filePath = equipmentDetailService.exportEquipmentDetailExcelFile(equipmentDetailStatisticsViews, baseFilePath);
            downLoadFile(filePath, response, "租赁物异常信息.xls");
        }
    }

    @RequestMapping(value = "/export-verifyInfo-by-organizer", method = RequestMethod.GET)
    public void getVerifyInfoByOrganizer(@RequestParam(value = "activityId", required = false) Integer activityId,
                                         @RequestParam(value = "episodeId", required = false) Integer episodeId,
                                         @RequestParam(value = "startTime", required = false) Long startTime,
                                         @RequestParam(value = "typeId", required = true) Integer typeId,
                                         @RequestParam(value = "endTime", required = false) Long endTime,
                                         @RequestParam(value = "code", required = false) String code,
                                         @RequestParam(value = "isRefund", required = false) String isRefund,
                                         HttpServletRequest request, HttpServletResponse response) throws Exception {

        Organizer organizer = WebUpmsContext.getOrgnization(request);
        List<ActivityType> allowTypes = organizerService.getOrganizerActivityAuth(typeId);
        List<Integer> typeIds = new ArrayList<>();
        for (ActivityType activityType : allowTypes) {
            typeIds.add(activityType.getTypeId());
        }

        Date startDate = null;
        Date endDate = null;
        if (startTime != null) {
            startDate = new Date(startTime);
        }
        if (endTime != null) {
            endDate = new Date(endTime);
        }
        if (StringUtils.isNotBlank(code)) {
            code = code.trim();
        }

        //List<VerifyRecordWithInfo> verifyInfoList = verifyService.getVerifyRecordInfoByOrganizer(activityId, episodeId, code, startDate, endDate, typeIds, organizer.getOrganizerId(),isRefund);
        List<Map> verifyInfoList = verifyService.getVerifyRecordInfoByOrganizer2(activityId, episodeId, code, startDate, endDate, typeIds, organizer.getOrganizerId(), isRefund);
        //String baseFilePath = Config.instance().getExcelTmpPath();
        //String filePath = verifyService.exportExcelFile(verifyInfoList, baseFilePath, organizer);
        //downLoadFile(filePath, response, "核销信息.xls");


        HSSFWorkbook wb = new HSSFWorkbook();
        HSSFSheet sheet = wb.createSheet("核销信息");

        response.reset();
        response.setContentType("application/x-excel");
        String agent = request.getHeader("User-Agent");
        String fileName = encodeDownloadFilename("核销信息", agent);
        response.setHeader("Content-disposition", "attachment;filename=" + fileName + ".xls");
        ServletOutputStream outputStream = response.getOutputStream();

        int rowNum = 0;
        HSSFCellStyle headStyle = wb.createCellStyle(); // 样式对象
        HSSFFont font = wb.createFont();// 生成一个字体
        font.setColor(HSSFColor.BLACK.index);// HSSFColor.VIOLET.index // 字体颜色
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);// 字体增粗
        font.setFontHeightInPoints((short) 16);
        headStyle.setFont(font);// 把字体应用到当前的样式
        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = row.createCell(1);
        cell.setCellValue("时间：" + (startDate == null ? "" : DateUtil.convertDateToString(startDate, "yyyy-MM-dd")) + "-" + (endDate == null ? "" : DateUtil.convertDateToString(endDate, "yyyy-MM-dd")));
        cell.setCellStyle(headStyle);

        headStyle = wb.createCellStyle(); // 样式对象
        headStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        headStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        headStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        headStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        headStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
        headStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);//垂直居中
        rowNum++;
        createHead(wb, sheet, rowNum, new String[]{"订单ID", "订单号", "子订单号", "外部订单号", "产品", "子项目", "票种", "订单价格", "核销储值金额", "核销金额", "核销数量", "核销时间", "核销标识", "核销终端", "核销用户", "支付方式", "分销商", "是否可退款", "是否是租赁物"});
        rowNum++;
        if (verifyInfoList != null) {
            for (Map map : verifyInfoList) {
                row = sheet.createRow(rowNum);
                cell = row.createCell(1);
                cell.setCellValue(MapUtils.getString(map, "order_id"));
                cell.setCellStyle(headStyle);

                cell = row.createCell(2);
                String parentCode = MapUtils.getString(map, "parent_code");
                if (parentCode == null) {
                    parentCode = MapUtils.getString(map, "code");
                }
                cell.setCellValue(parentCode);
                cell.setCellStyle(headStyle);

                cell = row.createCell(3);
                cell.setCellValue(MapUtils.getString(map, "code"));
                cell.setCellStyle(headStyle);

                cell = row.createCell(4);
                cell.setCellValue(MapUtils.getString(map, "agent_order_code"));
                cell.setCellStyle(headStyle);

                cell = row.createCell(5);
                cell.setCellValue(MapUtils.getString(map, "activity_title"));
                cell.setCellStyle(headStyle);

                cell = row.createCell(6);
                cell.setCellValue(MapUtils.getString(map, "episode_title"));
                cell.setCellStyle(headStyle);

                cell = row.createCell(7);
                cell.setCellValue(MapUtils.getString(map, "charge_name"));
                cell.setCellStyle(headStyle);

                cell = row.createCell(8);
                cell.setCellValue(new BigDecimal(MapUtils.getDouble(map, "total_price", 0d)).setScale(2, RoundingMode.HALF_UP).doubleValue());
                cell.setCellStyle(headStyle);

                cell = row.createCell(9);
                int activityTypeId = MapUtils.getIntValue(map, "ext_activity_type_id", -1);
                if (activityTypeId == 9) {
                    cell.setCellValue(new BigDecimal(MapUtils.getDouble(map, "total_price", 0d)).setScale(2, RoundingMode.HALF_UP).doubleValue());
                } else {
                    cell.setCellValue(0);
                }
                cell.setCellStyle(headStyle);

                cell = row.createCell(10);
                cell.setCellValue(new BigDecimal(MapUtils.getDouble(map, "v_price", 0d)).setScale(2, RoundingMode.HALF_UP).doubleValue());
                cell.setCellStyle(headStyle);

                cell = row.createCell(11);
                cell.setCellValue(MapUtils.getIntValue(map, "verify_order_num", 0));
                cell.setCellStyle(headStyle);

                cell = row.createCell(12);
                Date d = (Date) map.get("verify_time");
                cell.setCellValue(DateUtil.convertDateToString(d, "yyyy-MM-dd HH:mm:ss"));
                cell.setCellStyle(headStyle);

                cell = row.createCell(13);
                cell.setCellValue(getVerifyFlagName(MapUtils.getIntValue(map, "verify_flag", 0)));
                cell.setCellStyle(headStyle);

                cell = row.createCell(14);
                cell.setCellValue(getTerminalName(MapUtils.getIntValue(map, "verify_terminal", 0)));
                cell.setCellStyle(headStyle);

                cell = row.createCell(15);
                cell.setCellValue(MapUtils.getString(map, "realname"));
                cell.setCellStyle(headStyle);

                cell = row.createCell(16);
                cell.setCellValue(FinanceExcelUtil.getPayName(MapUtils.getIntValue(map, "pay_type_id", 0)));
                cell.setCellStyle(headStyle);

                cell = row.createCell(17);
                cell.setCellValue(MapUtils.getString(map, "distributor_name"));
                cell.setCellStyle(headStyle);

                cell = row.createCell(18);
                cell.setCellValue(MapUtils.getString(map, "isRefund"));
                cell.setCellStyle(headStyle);

                cell = row.createCell(19);
                cell.setCellValue(MapUtils.getString(map, "isRent"));
                cell.setCellStyle(headStyle);

                rowNum++;
            }

        }
        wb.write(outputStream);
        outputStream.close();

    }

    private String getVerifyFlagName(int flag) {
        String[] flags = {"自动核销", "手动核销"};       // 标识
        return flags[flag];
    }

    private String getTerminalName(int t) {
        String[] terminal = {"系统自动核销", "用户pc端核销", "用户手机扫码核销", "外部接口核销", "商米核销", "自助机核销", "闸机核销"};    // 终端
        return terminal[t];
    }

    /**
     * 导出租赁订单列表信息
     *
     * @throws Exception
     */
    @RequestMapping(value = "/export-rent-equipment-by-organizer", method = RequestMethod.GET)
    public void exportRentEquipmentInfo(@RequestParam(value = "detailStatus", required = false) Integer detailStatus,
                                        @RequestParam(value = "equipmentId", required = false) Integer equipmentId,
                                        @RequestParam(value = "borrowStart", required = false) String borrowStart,
                                        @RequestParam(value = "borrowEnd", required = false) String borrowEnd,
                                        @RequestParam(value = "cardNumber", required = false) Long cardNumber,
                                        @RequestParam(value = "code", required = false) String code,
                                        HttpServletRequest request, HttpServletResponse response) throws Exception {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer != null) {
            if (code != null) {
                code = code.trim();
            }
            String hex = null;
            if (cardNumber != null) {
                hex = Long.toHexString(cardNumber);
                hex = transform(hex, "");
            }
            List<RentDetailView> rentDetailViewList = depositService.getRentEquipmentListByOrgIdForExport(organizer.getOrganizerId(), equipmentId, code, detailStatus, borrowStart, borrowEnd, hex);
            String baseFilePath = Config.instance().getExcelTmpPath();
            String filePath = depositService.exportExcelFile(rentDetailViewList, baseFilePath);
            downLoadFile(filePath, response, "租赁订单列表信息.xls");
        }
    }

    @RequestMapping(value = "/export-limit-card-member-by-organizer", method = RequestMethod.GET)
    public void getCustomerList(
            @RequestParam(value = "phone", required = false) String phone,
            @RequestParam(value = "name", required = false) String name,
            @RequestParam(value = "idCard", required = false) String idCard,
            @RequestParam(value = "startTime", required = false) String startTime,
            @RequestParam(value = "endTime", required = false) String endTime,
            @RequestParam(value = "number", required = false) String number,
            @RequestParam(value = "id", required = false) Integer id,
            HttpServletRequest request, HttpServletResponse response) throws Exception {
        Organizer organizer = WebUpmsContext.getOrgnization(request);

        List<LimitCardMember> limitCardMembers = limitCardService.getCustomerListByOrgId4Excel(organizer.getOrganizerId(), name, phone, idCard, startTime, endTime, id, number);

        HSSFWorkbook wb = new HSSFWorkbook();
        HSSFSheet sheet = wb.createSheet("时限卡会员列表");

        response.reset();
        response.setContentType("application/x-excel");
        String agent = request.getHeader("User-Agent");
        String fileName = encodeDownloadFilename("时限卡会员列表", agent);
        response.setHeader("Content-disposition", "attachment;filename=" + fileName + ".xls");
        ServletOutputStream outputStream = response.getOutputStream();

        int rowNum = 0;
        HSSFCellStyle headStyle = wb.createCellStyle(); // 样式对象
        HSSFFont font = wb.createFont();// 生成一个字体
        font.setColor(HSSFColor.BLACK.index);// HSSFColor.VIOLET.index // 字体颜色
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);// 字体增粗
        font.setFontHeightInPoints((short) 16);
        headStyle.setFont(font);// 把字体应用到当前的样式
        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = row.createCell(1);

        cell.setCellValue("时限卡会员列表");
        cell.setCellStyle(headStyle);
        headStyle = wb.createCellStyle(); // 样式对象
        headStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        headStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        headStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        headStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        headStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
        headStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);//垂直居中
        rowNum++;
        createHead(wb, sheet, rowNum, new String[]{"ID", "姓名", "手机号", "身份证", "创建时间", "种类", "开始时间", "结束时间", "卡号", "操作人"});
        rowNum++;
        if (limitCardMembers != null) {
            for (LimitCardMember limitCardMember : limitCardMembers) {
                row = sheet.createRow(rowNum);
                cell = row.createCell(1);
                cell.setCellValue(limitCardMember.getId());
                cell.setCellStyle(headStyle);

                cell = row.createCell(2);
                cell.setCellValue(limitCardMember.getName());
                cell.setCellStyle(headStyle);

                cell = row.createCell(3);
                cell.setCellValue(limitCardMember.getPhone());
                cell.setCellStyle(headStyle);

                cell = row.createCell(4);
                cell.setCellValue(limitCardMember.getIdCard());
                cell.setCellStyle(headStyle);

                cell = row.createCell(5);
                cell.setCellValue(DateUtil.dateToDateString(limitCardMember.getCreateTime()));
                cell.setCellStyle(headStyle);

                cell = row.createCell(6);
                cell.setCellValue(limitCardMember.getChargeName());
                cell.setCellStyle(headStyle);

                cell = row.createCell(7);
                cell.setCellValue(DateUtil.dateToDateString(limitCardMember.getStartTime()));
                cell.setCellStyle(headStyle);

                cell = row.createCell(8);
                cell.setCellValue(DateUtil.dateToDateString(limitCardMember.getEndTime()));
                cell.setCellStyle(headStyle);

                cell = row.createCell(9);
                cell.setCellValue(limitCardMember.getMaterialNum());
                cell.setCellStyle(headStyle);

                cell = row.createCell(10);
                cell.setCellValue(limitCardMember.getOperatorName());
                cell.setCellStyle(headStyle);

                rowNum++;
            }

        }
        wb.write(outputStream);
        outputStream.close();

    }

    @RequestMapping(value = "/export-access-data-list", method = RequestMethod.GET)
    public void getAccessDataList(
            @RequestParam(value = "startTime", required = false) String startTime,
            @RequestParam(value = "endTime", required = false) String endTime,
            HttpServletRequest request, HttpServletResponse response) throws Exception {
        Organizer organizer = WebUpmsContext.getOrgnization(request);

        List<AccessData> accessDataList = verifyService.getAccessDataRecords(startTime, endTime, organizer.getOrganizerId());

        HSSFWorkbook wb = new HSSFWorkbook();
        HSSFSheet sheet = wb.createSheet("刷卡记录");

        response.reset();
        response.setContentType("application/x-excel");
        String agent = request.getHeader("User-Agent");
        String fileName = encodeDownloadFilename("刷卡记录", agent);
        response.setHeader("Content-disposition", "attachment;filename=" + fileName + ".xls");
        ServletOutputStream outputStream = response.getOutputStream();

        int rowNum = 0;
        HSSFCellStyle headStyle = wb.createCellStyle(); // 样式对象
        HSSFFont font = wb.createFont();// 生成一个字体
        font.setColor(HSSFColor.BLACK.index);// HSSFColor.VIOLET.index // 字体颜色
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);// 字体增粗
        font.setFontHeightInPoints((short) 16);
        headStyle.setFont(font);// 把字体应用到当前的样式
        HSSFRow row = sheet.createRow(rowNum);
        HSSFCell cell = row.createCell(1);

        cell.setCellValue("刷卡记录");
        cell.setCellStyle(headStyle);
        headStyle = wb.createCellStyle(); // 样式对象
        headStyle.setAlignment(HSSFCellStyle.VERTICAL_CENTER);
        headStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        headStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        headStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        headStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
        headStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);//垂直居中
        rowNum++;
        createHead(wb, sheet, rowNum, new String[]{"订单号", "入园ID", "姓名", "手机号", "产品", "票种", "支付方式", "时间", "首次刷卡时间", "闸机ID", "卡号"});
        rowNum++;
        if (accessDataList != null) {
            for (AccessData accessData : accessDataList) {
                row = sheet.createRow(rowNum);
                cell = row.createCell(1);
                cell.setCellValue(accessData.getCode());
                cell.setCellStyle(headStyle);

                cell = row.createCell(2);
                cell.setCellValue(accessData.getEntryId());
                cell.setCellStyle(headStyle);

                cell = row.createCell(3);
                cell.setCellValue(accessData.getCustomerName());
                cell.setCellStyle(headStyle);

                cell = row.createCell(4);
                cell.setCellValue(accessData.getPhone());
                cell.setCellStyle(headStyle);

                cell = row.createCell(5);
                cell.setCellValue(accessData.getActivityName());
                cell.setCellStyle(headStyle);

                cell = row.createCell(6);
                cell.setCellValue(accessData.getChargeName());
                cell.setCellStyle(headStyle);

                cell = row.createCell(7);
                cell.setCellValue(accessData.getPayType());
                cell.setCellStyle(headStyle);

                cell = row.createCell(8);
                cell.setCellValue(DateUtil.dateToDateString(accessData.getTime()));
                cell.setCellStyle(headStyle);

                cell = row.createCell(9);
                cell.setCellValue(DateUtil.dateToDateString(accessData.getFirstTime()));
                cell.setCellStyle(headStyle);

                cell = row.createCell(10);
                cell.setCellValue(accessData.getPosId());
                cell.setCellStyle(headStyle);

                cell = row.createCell(11);
                cell.setCellValue(accessData.getMaterialNum());
                cell.setCellStyle(headStyle);

                rowNum++;
            }

        }
        wb.write(outputStream);
        outputStream.close();

    }

    private String transform(String origin, String des) {
        if (StringUtils.isBlank(des) && origin.length() < 8) {
            int len = 8 - origin.length();
            for (int i = 0; i < len; i++)
                origin = "0" + origin;
        }
        if (origin.length() <= des.length()) {
            return des.toUpperCase();
        } else {
            String last = origin.substring(origin.length() - 2 - des.length(), origin.length() - des.length());
            return transform(origin, des + last);
        }
    }
}
