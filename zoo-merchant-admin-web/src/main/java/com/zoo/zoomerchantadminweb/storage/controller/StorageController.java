package com.zoo.zoomerchantadminweb.storage.controller;

import com.zoo.account.constant.AccountConstant;
import com.zoo.account.constant.AccountEnum;
import com.zoo.account.service.AccountService;
import com.zoo.account.support.AccountResult;
import com.zoo.activity.core.Status;
import com.zoo.activity.dao.model.*;
import com.zoo.activity.dao.nutz.pojo.CommonAccount;
import com.zoo.activity.service.*;
import com.zoo.activity.util.Common;
import com.zoo.icenow.dao.model.CommonAccountMoney;
import com.zoo.icenow.service.StorageService;
import com.zoo.web.bean.WebResult;
import com.zoo.zoomerchantadminweb.base.BaseController;
import com.zoo.zoomerchantadminweb.context.WebUpmsContext;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.util.*;

/**
 * server储值相关
 * Created by soul on 2018/7/12.
 */
@Api(value = "api/server/storage", tags = {"主办方储值相关接口"}, description = "主办方储值相关接口描述")
@RestController
@RequestMapping("api/server/storage")
public class StorageController extends BaseController {

    protected Logger logger = Logger.getLogger(this.getClass());

    @Resource(name = "storageService")
    private StorageService storageService;

    @Resource(name = "AccountService")
    private AccountService accountService;

    @Autowired
    private ActivityService activityService;

    @Autowired
    private OrdersService ordersService;

    @Autowired
    private ChargeService chargeService;

    @Autowired
    private EpisodeService episodeService;

    @Autowired
    private PlayerService playerService;

    @Autowired
    private OrderCoachExtendService orderCoachExtendService;

    @Autowired
    private CoachService coachService;

    @ApiOperation(value = "主办方储值产品", notes = "主办方储值产品", httpMethod = "GET")
    @RequestMapping(value = "/activity.json", method = RequestMethod.GET)
    public WebResult rentManage(HttpServletRequest request) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        Activity activity = activityService.getStorageActicity(organizer.getOrganizerId());
        return WebResult.getSuccessResult("data", activity);
    }

    @ApiOperation(value = "主办方储值产品添加", notes = "主办方储值产品添加、修改", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "storageActivityId", value = "ID", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "storageActivityTitle", value = "标题", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "storageActivityPostUrl", value = "海报", required = true, dataType = "string", paramType = "query")
    })
    @RequestMapping(value = "/activity/save.json", method = RequestMethod.POST)
    public Object addRentActivity(@RequestParam(value = "storageActivityId", required = false) Integer storageActivityId,
                                  @RequestParam("storageActivityTitle") String storageActivityTitle,
                                  @RequestParam("storageActivityPostUrl") String storageActivityPostUrl,
                                  HttpServletRequest request) {
        ActivityWithBLOBs activity = new ActivityWithBLOBs();
        activity.setTitle(storageActivityTitle);
        activity.setOrganizerId(WebUpmsContext.getOrgnization(request).getOrganizerId());
        activity.setTypeId(9);
        activity.setVisible(1);
        activity.setStatus(1);
        activity.setRent(0);
        activity.setOffline(1);
        activity.setPosterUrl(storageActivityPostUrl);
        boolean success;
        if (null == storageActivityId || storageActivityId == 0) {
            Activity exist = activityService.getStorageActicity(WebUpmsContext.getOrgnization(request).getOrganizerId());
            if (null != exist) {
                return WebResult.getErrorMsgResult(WebResult.Code.ERROR, "已经添加租赁产品，请刷新后重试~");
            }
            activity.setActivityId(activityService.getNextActivityId());
            activity.setCreateTime(new Date());
            success = activityService.addActivity(activity, null);
        } else {
            activity.setActivityId(storageActivityId);
            activity.setUpdateTime(new Date());
            success = activityService.updateActivity(activity);
        }
        if (success) {
            return WebResult.getSuccessResult();
        } else {
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }
    }

    @ApiOperation(value = "获取会员储值列表", notes = "获取会员储值列表", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页", required = false, dataType = "int", paramType = "query", defaultValue = "1"),
            @ApiImplicitParam(name = "size", value = "条", required = false, dataType = "int", paramType = "query", defaultValue = "10"),
            @ApiImplicitParam(name = "customerId", value = "用户id", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "operationType", value = "查询类型 1.所有充值(0/3)；2.所有消费(1/4) (动作（0.充值；1.消费；2.线下退款；3.线下充值；4线下消费；5.其他(未想到未用上)）；999.默认)", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "isAllowRefundAmount", value = "是否查询 可退款储值余额 （0.否；1.是）", required = true, dataType = "int", paramType = "query")
    })
    @RequestMapping(value = "/getMemberStorageIncomeRecordList.json", method = RequestMethod.GET)
    public Object getMemberStorageIncomeRecordList(@RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
                                                   @RequestParam(value = "size", required = false, defaultValue = "10") Integer size,
                                                   @RequestParam(value = "customerId", required = true) Integer customerId,
                                                   @RequestParam(value = "operationType", required = false) Integer operationType,
                                                   @RequestParam(value = "isAllowRefundAmount", required = false, defaultValue = "0") Integer isAllowRefundAmount,
                                                   HttpServletRequest request) {
        // 主办方信息
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null) {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }
        Map<String, Object> resultMap = new HashMap<>();
        try {
            resultMap.put("storageDetailList", storageService.selectStorageIncomePage(customerId, organizer.getOrganizerId(), ((page - 1) * size), (page * size), operationType));
            resultMap.put("storageDetailListCount", storageService.getStorageIncomePageCount(customerId, organizer.getOrganizerId(), operationType));

            if (isAllowRefundAmount == 1) { // 查询储值可退款金额
                resultMap.put("allowRefundAmount", storageService.getStorageBalanceAmountRefund(organizer.getOrganizerId(), customerId));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }
        return WebResult.getSuccessResult("data", resultMap);
    }

    @ApiOperation(value = "保存储值线下用户", notes = "保存储值线下用户", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "realname", value = "用户名", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "phone", value = "手机号", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "wechatAccountNo", value = "微信号", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "idCardNo", value = "身份证号", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "email", value = "邮箱", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "address", value = "地址", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "userJob", value = "工作", required = false, dataType = "string", paramType = "query"),
    })
    @RequestMapping(value = "/saveStorageCustomer.json", method = RequestMethod.POST)
    public WebResult saveStorageCustomer(@RequestParam(value = "realname", required = false, defaultValue = "") String realname,
                                         @RequestParam(value = "phone") String phone,
                                         @RequestParam(value = "wechatAccountNo", required = false, defaultValue = "") String wechatAccountNo,
                                         @RequestParam(value = "idCardNo") String idCardNo,
                                         @RequestParam(value = "email", required = false, defaultValue = "") String email,
                                         @RequestParam(value = "address", required = false, defaultValue = "") String address,
                                         @RequestParam(value = "userJob", required = false, defaultValue = "") String userJob) {

        try {
            CommonAccount commonAccount = new CommonAccount();
            commonAccount.setUserId(Common.getUserId());
            commonAccount.setRealname(realname);
            commonAccount.setPhone(phone);
            commonAccount.setWechatAccountNo(wechatAccountNo);
            commonAccount.setIdCardNo(idCardNo);
            commonAccount.setEmail(email);
            commonAccount.setAddress(address);
            commonAccount.setUserJob(userJob);
            commonAccount.setSource(AccountEnum.AccountTypeEnum.ISNOW_SERVER.getValue());
            commonAccount.setRegisterType(AccountEnum.RegisterTypeEnum.PHONE.getValue());
            commonAccount.setStatus(1);
            commonAccount.setCreateTime(new Date());
            commonAccount.setLastModify(new Date());
            AccountResult validResult = accountService.saveOrUpdate(AccountEnum.OperateTypeEnum.SAVE.getValue(), commonAccount, AccountConstant.UNENCRYPTED);

            if (null != validResult && validResult.getStatus().equals(Status.success)) {
                return WebResult.getSuccessResult();
            } else {
                return WebResult.getErrorResult(WebResult.Code.USER_NO_BIND_ERROR);
            }

        } catch (Exception e) {
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }

    }

    /**
     * 通过手机号 获取用户相关信息
     *
     * @return
     */
    @ApiOperation(value = "获取储值用户信息", notes = "通过手机号获取储值用户信息", httpMethod = "GET")
    @RequestMapping(value = "/getCustomerInfoByPhone.json", method = RequestMethod.GET)
    public WebResult getCustomerInfoByPhone(HttpServletRequest request,
                                            @RequestParam(value = "phone") String phone) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);

        try {
            Map<String, Object> resultMap = new HashedMap();
            resultMap.put("isPhoneHasCommonAccount", 0);
            AccountResult loginResult = accountService.getAccount(AccountEnum.FetchAccountTypeEnum.FETCH_BY_PHONE.getValue(), phone);
            if (null != loginResult && loginResult.getStatus().equals(Status.success)) { // 获取到数据
                CommonAccount account = (CommonAccount) loginResult.getData();
                if (account != null) {
                    resultMap.put("isPhoneHasCommonAccount", 1);
                    resultMap.put("customerInfo", account);
//                    BigDecimal balanceAmount = storageService.getStorageAmountByCustomerIdAndOrganizerId(account.getCustomerId(), organizer.getOrganizerId());
                    // 更换获取储值余额方式
                    CommonAccountMoney balance = storageService.getBalance(organizer.getOrganizerId(), account.getCustomerId());
                    resultMap.put("balanceAmount", balance == null ? 0 : balance.getBalance());
                }
            }
            return WebResult.getSuccessResult("data", resultMap);
        } catch (Exception e) {
            e.printStackTrace();
            return WebResult.getErrorResult(WebResult.Code.CASHIER_GET_STORAGE_FAILED);
        }

    }


    @ApiOperation(value = "线下储值退款操作", notes = "线下储值退款操作 进行记录", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "customerId", value = "退款会员ID", required = true, dataType = "int", paramType = "query")
    })
    @RequestMapping(value = "/refund-storage-offline.json", method = RequestMethod.GET)
    public WebResult refundStorage(@RequestParam(value = "customerId", required = true) Integer customerId,
                                   HttpServletRequest request
    ) {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        CommonAccount operateAccount = WebUpmsContext.getAccount(request); // 操作人
        // 验证基本信息
        if (organizer == null || operateAccount == null) {
            return WebResult.getErrorResult(WebResult.Code.NOT_LOGIN);
        }

        // 查询可退款 储值余额
        // 根据可退款余额 判断是否为0 来进行是否退款判断
        BigDecimal allowRefundAmount = storageService.getStorageBalanceAmountRefund(organizer.getOrganizerId(), customerId);
        if (allowRefundAmount.doubleValue() <= 0) {
            return WebResult.getErrorResult(WebResult.Code.STORAGE_NO_REFUND_MONEY);
        }

        try {
            // 退款
            return storageService.storageIncomRefund(customerId, organizer.getOrganizerId());
        } catch (Exception e) {
            e.printStackTrace();
            return WebResult.getErrorResult(WebResult.Code.ERROR);
        }
    }

    /**
     * 根据orderid 获取订单相关信息(线下支付用，目前很多信息用来判断储值消费相关)
     *
     * @return
     */
    @ApiOperation(value = "线下支付获取填写报名人的储值信息", notes = "线下支付获取填写报名人的储值信息,用于线下储值支付使用", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "orderId", value = "订单ID", required = true, dataType = "int", paramType = "query")
    })
    @RequestMapping(value = "/offlineOrderInfo.json", method = RequestMethod.GET)
    public Object offlineOrderInfo(HttpServletRequest request, HttpSession session,
                                   @RequestParam(value = "orderId") Integer orderId) {
        Map<String, Object> resultMap = new HashMap<>();

        // 验证基本信息
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        if (organizer == null) {
            return WebResult.getErrorResult(WebResult.Code.COACH_GET_ORGANIZER_INFO_ERROR);
        }
        try {
            Orders order = ordersService.getSimpleOrdersById(orderId);
            List<Charge> chargeList = new ArrayList<>();
            List<Orders> childOrders = ordersService.getChildOrders(order.getOrderId());
            if (null == childOrders || childOrders.size() == 0) {
                Charge charge = chargeService.getChargeById(order.getChargeId());
                chargeList.add(charge);
            } else {
                for (Orders childOrder : childOrders) {
                    Charge multiCharge = chargeService.getChargeById(childOrder.getChargeId());
                    chargeList.add(multiCharge);
                }
            }
            Episode episode = episodeService.getEpisodeDetailById(chargeList.get(0).getEpisodeId());
            Activity activity = activityService.getActivityById(episode.getActivityId());

            if (activity != null && activity.getTypeId() == 8 && activity.getActivityMode() == 0) {
                List<OrderCoachExtend> orderCoachExtendList = orderCoachExtendService.selectByOrderId(orderId);
                if (orderCoachExtendList.size() == 1) {
                    Integer coachId = orderCoachExtendList.get(0).getTransferCoach() != null ? orderCoachExtendList.get(0).getTransferCoach() : orderCoachExtendList.get(0).getOrderCoach();
                    if (coachId != null) {
                        resultMap.put("coachName", coachService.findCoach(coachId).getRealname());
                    }
                }
            }

            /* 判断当前产品是否能用储值金额 支付 */
            int isAllowUseStorage = 0;
            if (organizer.getType() != null) {
                String typeStr = activityService.getOrganizerActivityTypeAllow(organizer.getType()); // 获取二进制
                // 主办方开放储值功能 并且 产品可以用储值金额购买 (并且目前，押金不为0的订单，也不让用用储值支付)
                if (typeStr.substring(9, 10).equals("1") && activity.getAllowUseStorage() == 1 && (order.getDeposit() == null || order.getDeposit().doubleValue() == 0)) {
                    isAllowUseStorage = 1;
                }
            }
            resultMap.put("isAllowUseStorage", isAllowUseStorage);

            resultMap.put("balanceAmount", 0);
            if (isAllowUseStorage == 1) {
                /* 获取player表的phone字段的 common_account 的用户信息 */
                List<Player> players = playerService.getPlayersByOrder((null == childOrders || childOrders.size() == 0) ? order.getOrderId() : childOrders.get(0).getOrderId());
                if (players.size() <= 0) {
                    // 获取订单信息失败
                    return WebResult.getErrorResult(WebResult.Code.CASHIER_GET_ORDER_FAILED);
                }
                String custoemrPhone = players.get(0).getPhone(); // 储值会员的手机号
                AccountResult accountResult = accountService.getAccount(AccountEnum.FetchAccountTypeEnum.FETCH_BY_PHONE.getValue(), custoemrPhone);
                CommonAccount commonAccount = null;
                if (accountResult != null && accountResult.getData() != null) {
                    commonAccount = (CommonAccount) accountResult.getData();
                    /* 获取 储值余额 */
                    //BigDecimal balanceAmount = storageService.getStorageAmountByCustomerIdAndOrganizerId(commonAccount.getCustomerId(), organizer.getOrganizerId());
                    // 更换获取储值余额方式
                    CommonAccountMoney balance = storageService.getBalance(organizer.getOrganizerId(), commonAccount.getCustomerId());
                    resultMap.put("balanceAmount", balance == null ? 0 : balance.getBalance());
                    resultMap.put("isStorageUser", 1); // 储值用户
                } else {
                    // 填写的第一个报名人手机号没有系统账号，所以更没有储值信息
                    resultMap.put("balanceAmount", 0); // 0元
                    resultMap.put("isStorageUser", 0); // 不是储值用户
                }

            }

            return WebResult.getSuccessResult("data", resultMap);
        } catch (Exception e) {
            // logger.error("/storage/offlineOrderInfo.json 获取线下订单信息 出现异常");
            e.printStackTrace();
            return WebResult.getErrorResult(WebResult.Code.CASHIER_GET_ORDER_FAILED);
        }
    }
}
