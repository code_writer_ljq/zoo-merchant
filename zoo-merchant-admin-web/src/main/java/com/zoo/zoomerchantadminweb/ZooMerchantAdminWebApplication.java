package com.zoo.zoomerchantadminweb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@EnableWebMvc
@ComponentScan(basePackages = {"com.zoo.zoomerchantadminweb", "com.zoo.log.mq.handler"})
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
public class ZooMerchantAdminWebApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(ZooMerchantAdminWebApplication.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        // 项目启动的时候创建导表所需的文件夹
        // String baseFilePath = request.getSession().getServletContext().getRealPath("/");

        return builder.sources(ZooMerchantAdminWebApplication.class);
    }
}
