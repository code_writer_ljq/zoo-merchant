package com.zoo.zoomerchantadminweb.camp.controller;

import com.zoo.common.page.PageList;
import com.zoo.activity.dao.model.Organizer;
import com.zoo.activity.service.EpisodeService;
import com.zoo.activity.service.OrdersService;
import com.zoo.web.bean.WebResult;
import com.zoo.zoomerchantadminweb.context.WebUpmsContext;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@Api(value = "api/server/camp", tags = {"主办方训练营相关接口"}, description = "主办方训练营相关接口描述")
@RestController
@RequestMapping("api/server/camp")
public class CampController {

    @Autowired
    private OrdersService ordersService;

    @Autowired
    private EpisodeService episodeService;

    @ApiOperation(value = "报名列表", notes = "训练营报名列表", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页", required = false, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "size", value = "条", required = false, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "activityId", value = "产品ID", required = false, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "episodeId", value = "场次ID", required = false, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "status", value = "状态", required = false, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "phone", value = "手机号", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "name", value = "姓名", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "startTime", value = "开始时间", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "endTime", value = "结束时间", required = false, dataType = "string", paramType = "query")
    })
    @RequestMapping(value = "/player/list.json", method = RequestMethod.GET)
    public WebResult getCampPlayerList(@RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
                                       @RequestParam(value = "size", required = false, defaultValue = "10") Integer size,
                                       @RequestParam(value = "activityId", required = false) Integer activityId,
                                       @RequestParam(value = "episodeId", required = false) Integer episodeId,
                                       @RequestParam(value = "name", required = false) String name,
                                       @RequestParam(value = "phone", required = false) String phone,
                                       @RequestParam(value = "status", required = false) Integer status,
                                       @RequestParam(value = "startTime", required = false) String startTime,
                                       @RequestParam(value = "endTime", required = false) String endTime,
                                       HttpServletRequest request) throws Exception {
        Organizer organizer = WebUpmsContext.getOrgnization(request);
        PageList orderViews = ordersService.getOrderCampPlayerList(activityId, episodeId, status, organizer.getOrganizerId(), name, phone, startTime, endTime, page, size);

        Map map = new HashMap<>();
//        map.put("episodePlayerMeta", "");
//
//        if (episodeId != null) {
//            Episode episodeDetail = episodeService.getEpisodeDetailById(episodeId);
//            if (episodeDetail.getPlayerMeta() != null && episodeDetail.getPlayerMeta().length() > 0) {
//                map.put("episodePlayerMeta", episodeDetail.getPlayerMeta());
//            }
//        }

        map.put("list", orderViews);
        return WebResult.getSuccessResult("data", map);
    }

    @ApiOperation(value = "报名列表打印", notes = "训练营报名列表打印", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页", required = false, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "size", value = "条", required = false, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "activityId", value = "产品ID", required = false, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "episodeId", value = "场次ID", required = false, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "status", value = "状态", required = false, dataType = "integer", paramType = "query"),
            @ApiImplicitParam(name = "phone", value = "手机号", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "name", value = "姓名", required = false, dataType = "string", paramType = "query"),
    })
    @RequestMapping("/print.json")
    public WebResult printerPlayerByEpisodeId(@RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
                                              @RequestParam(value = "size", required = false, defaultValue = "10") Integer size,
                                              @RequestParam(value = "activityId", required = false) Integer activityId,
                                              @RequestParam(value = "episodeId", required = false) Integer episodeId,
                                              @RequestParam(value = "name", required = false) String name,
                                              @RequestParam(value = "phone", required = false) String phone,
                                              @RequestParam(value = "status", required = false) Integer status,
                                              HttpServletRequest request
    ) throws Exception {

        size = 10000;

        Organizer organizer = WebUpmsContext.getOrgnization(request);

        PageList orderViews = ordersService.printOrderCampPlayerList(activityId, episodeId, status, organizer.getOrganizerId(), name, phone, page, size);

//        Episode episodeDetail = episodeService.getEpisodeDetailById(episodeId);

        Map map = new HashMap();
        map.put("list", orderViews.getDataList());
        map.put("organizer", organizer);
//        map.put("episode", episodeDetail);

        return WebResult.getSuccessResult("data", map);

    }
}
